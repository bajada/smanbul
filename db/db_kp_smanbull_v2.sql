-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 13 Nov 2021 pada 14.13
-- Versi server: 10.4.19-MariaDB
-- Versi PHP: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kp_smanbull_v2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `kp_iuran`
--

CREATE TABLE `kp_iuran` (
  `id_iuran` varchar(50) NOT NULL DEFAULT '',
  `id_siswa_iuran` varchar(50) NOT NULL DEFAULT '',
  `id_siswa_kelas_iuran` varchar(50) NOT NULL DEFAULT '',
  `jatuh_tempo_iuran` date DEFAULT NULL,
  `jenis_iuran` varchar(50) DEFAULT NULL,
  `jumlah_iuran` decimal(15,2) DEFAULT 0.00,
  `keterangan_iuran` varchar(100) DEFAULT NULL,
  `no_iuran` varchar(50) DEFAULT NULL,
  `status_iuran` varchar(100) DEFAULT NULL,
  `tanggal_iuran` datetime DEFAULT NULL,
  `besar_iuran` decimal(15,2) DEFAULT 0.00,
  `bulan_iuran` varchar(50) DEFAULT NULL,
  `catatan_iuran` varchar(100) DEFAULT NULL,
  `cicilan_iuran` int(11) NOT NULL,
  `user_added_iuran` varchar(50) DEFAULT NULL,
  `date_added_iuran` datetime DEFAULT NULL,
  `user_modified_iuran` varchar(50) DEFAULT NULL,
  `date_modified_iuran` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kp_iuran`
--

INSERT INTO `kp_iuran` (`id_iuran`, `id_siswa_iuran`, `id_siswa_kelas_iuran`, `jatuh_tempo_iuran`, `jenis_iuran`, `jumlah_iuran`, `keterangan_iuran`, `no_iuran`, `status_iuran`, `tanggal_iuran`, `besar_iuran`, `bulan_iuran`, `catatan_iuran`, `cicilan_iuran`, `user_added_iuran`, `date_added_iuran`, `user_modified_iuran`, `date_modified_iuran`) VALUES
('02417995-2983-409d-ad95-059911ce3591', '1636643729347', '1636643729381', NULL, 'TABUNGAN', '0.00', '', 'VXNZY0T7NK', 'BELUM_LUNAS', '2021-11-12 00:00:00', '100000.00', 'Dec 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 06:00:35', NULL, NULL),
('0a1a1401-01d7-4dc3-b041-3695b847a9c7', '1636643729347', '1636643729381', NULL, 'IURAN_PER_BULAN', '0.00', '', '0CIR556OXE', 'LUNAS', '2021-11-12 00:00:00', '250000.00', 'Nov 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 03:27:30', NULL, NULL),
('0f39da20-9545-4969-a725-ca2474477b35', '1636643729347', '1636643729381', NULL, 'TABUNGAN', '0.00', '', '5EQCPS7MXJ', 'LUNAS', '2021-11-12 00:00:00', '50000.00', 'Nov 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 06:00:15', NULL, NULL),
('1a976eb6-ade1-475a-8a74-5fc467b2b612', '1636643729347', '1636643729381', NULL, 'TABUNGAN', '0.00', '', 'C6DQGIU3HZ', 'LUNAS', '2021-11-12 00:00:00', '20000.00', 'Oct 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 05:55:06', NULL, NULL),
('1bf6581d-5765-49e4-8caf-833a720f9b90', 'ff23199f-e279-468d-9a4e-889f35c8be1e', '707ce69a-4f97-49fd-9b24-7ba5dd4c129f', NULL, 'IURAN_PER_BULAN', '0.00', 'REGISTRASI_ULANG', 'QEPY1UKETO', 'LUNAS', '2021-07-07 00:00:00', '250000.00', 'Jul 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL),
('22ed0d4c-8a89-4978-8d11-830a8e62ec5f', '1636643729347', '1636643729381', NULL, 'TABUNGAN', '0.00', '', 'EGLKBLCB8X', 'LUNAS', '2021-11-12 00:00:00', '150000.00', 'Sep 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 03:25:52', NULL, NULL),
('23749781-1b0e-488b-a706-4b608a0f4bb2', '1636643729347', '1636643729381', NULL, 'IURAN_PER_BULAN', '0.00', '', 'EGLKBLCB8X', 'LUNAS', '2021-11-12 00:00:00', '250000.00', 'Sep 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 03:25:52', NULL, NULL),
('2eb054a3-905d-4735-9b98-dfe82716c1ab', '1636643729347', '1636643729381', NULL, 'TABUNGAN', '0.00', '', 'BPK88LZ0VQ', 'LUNAS', '2021-11-12 00:00:00', '50000.00', 'Nov 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 06:00:04', NULL, NULL),
('365292df-7ed7-4182-92b8-c823d6de4f30', '1636643729347', '1636643729381', NULL, 'IURAN_PER_BULAN', '0.00', '', 'VXNZY0T7NK', 'LUNAS', '2021-11-12 00:00:00', '200000.00', 'Apr 2022', NULL, 0, 'Admin SMANBULL', '2021-11-12 06:00:35', NULL, NULL),
('474334e6-98d6-4157-b9e6-034c1181cd91', '1636643729347', '1636643729381', NULL, 'IURAN_PER_BULAN', '0.00', '', '7FR0VDG1F8', 'LUNAS', '2021-11-12 00:00:00', '250000.00', 'Feb 2022', NULL, 0, 'Admin SMANBULL', '2021-11-12 05:59:30', NULL, NULL),
('4ecd0fed-38dd-41d6-ae9d-a6de2f691741', '576d456d-dc10-41e5-9fa7-d5a4aaaf5bbc', 'bc0d2977-24cf-4431-b7b0-3d3b197f817c', NULL, 'DU', '0.00', 'REGISTRASI_ULANG', 'BZ2X7M7LXC', 'LUNAS', '2021-07-08 00:00:00', '300000.00', '', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL),
('5179ddc5-0484-4db0-8380-425042c3966b', '1636643729347', '1636643729381', NULL, 'IURAN_PER_BULAN', '0.00', '', 'EGLKBLCB8X', 'LUNAS', '2021-11-12 00:00:00', '250000.00', 'Jul 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 03:25:52', NULL, NULL),
('55dfa2fd-52f0-4f68-999f-3dda062e2ce7', '339818e8-42d6-4b68-9ee5-cd17688bcdee', '8a343a67-807a-4626-a24f-52bf5e07314f', NULL, 'IURAN_PER_BULAN', '0.00', 'REGISTRASI_ULANG', 'T8SXVPD497', 'LUNAS', '2021-07-03 00:00:00', '250000.00', 'Jul 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL),
('57d3e9ed-6da7-45ab-89c2-9bab665c73f0', '1636643729347', '1636643729381', NULL, 'TABUNGAN', '0.00', '', 'YWCMAJKI5B', 'LUNAS', '2021-11-12 00:00:00', '50000.00', 'Oct 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 05:54:53', NULL, NULL),
('59c245a9-e2a6-4d6a-a981-0354267cfbd4', '1636643729347', '1636643729381', NULL, 'TABUNGAN', '0.00', '', 'EGLKBLCB8X', 'LUNAS', '2021-11-12 00:00:00', '150000.00', 'Jul 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 03:25:52', NULL, NULL),
('5fb7f437-4cc3-425e-a876-e9b49a2169f1', '1636643729347', '1636643729381', NULL, 'TABUNGAN', '0.00', '', '7FR0VDG1F8', 'LUNAS', '2021-11-12 00:00:00', '50000.00', 'Nov 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 05:59:30', NULL, NULL),
('624dd4a7-3102-4efb-a24e-c547f2a63336', 'fc6dc06b-8485-4947-a24c-1a0f6a5c78a7', 'a567f842-499f-45bc-9d41-eab620e8ae87', NULL, 'DPMP', '0.00', 'REGISTRASI_ULANG', 'FJSM0IKCKS', 'BELUM_LUNAS', '2021-07-06 00:00:00', '50000.00', '', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL),
('69114b1c-5c3c-4db7-bab0-1de210d33470', '339818e8-42d6-4b68-9ee5-cd17688bcdee', '8a343a67-807a-4626-a24f-52bf5e07314f', NULL, 'DPMP', '0.00', 'REGISTRASI_ULANG', 'T8SXVPD497', 'BELUM_LUNAS', '2021-07-03 00:00:00', '500000.00', '', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL),
('69331f68-a1a5-4c22-bf9e-fba6e2a0ea46', 'ff23199f-e279-468d-9a4e-889f35c8be1e', '707ce69a-4f97-49fd-9b24-7ba5dd4c129f', NULL, 'DU', '0.00', 'REGISTRASI_ULANG', 'QEPY1UKETO', 'LUNAS', '2021-07-07 00:00:00', '300000.00', '', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL),
('7251ba9b-8753-4372-b746-b329ca6d6c87', '339818e8-42d6-4b68-9ee5-cd17688bcdee', '8a343a67-807a-4626-a24f-52bf5e07314f', NULL, 'DU', '0.00', 'REGISTRASI_ULANG', 'T8SXVPD497', 'LUNAS', '2021-07-03 00:00:00', '300000.00', '', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL),
('82e43120-563d-4a2d-a1c8-dfd34beb5c2c', '576d456d-dc10-41e5-9fa7-d5a4aaaf5bbc', 'bc0d2977-24cf-4431-b7b0-3d3b197f817c', NULL, 'TABUNGAN', '0.00', 'REGISTRASI_ULANG', 'BZ2X7M7LXC', 'LUNAS', '2021-07-08 00:00:00', '50000.00', 'Jul 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL),
('948b32ed-ec3f-4d35-a07a-e10c444505e7', '20165cf0-7ef7-46aa-85b1-410d8d7b1eb9', 'dece2547-fc3c-455a-92a1-c62bed376237', NULL, 'IURAN_PER_BULAN', '0.00', 'REGISTRASI_ULANG', '4LDPKYN1RT', 'LUNAS', '2021-07-05 00:00:00', '250000.00', 'Jul 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL),
('96296690-495d-4be9-8542-a28c015475f4', '1636643729347', '1636643729381', NULL, 'IURAN_PER_BULAN', '0.00', '', 'BPK88LZ0VQ', 'LUNAS', '2021-11-12 00:00:00', '50000.00', 'Apr 2022', NULL, 0, 'Admin SMANBULL', '2021-11-12 06:00:04', NULL, NULL),
('983323eb-2ad8-4e09-848e-46a67587654d', '1636643729347', '1636643729381', NULL, 'IURAN_PER_BULAN', '0.00', '', 'BPK88LZ0VQ', 'LUNAS', '2021-11-12 00:00:00', '250000.00', 'Mar 2022', NULL, 0, 'Admin SMANBULL', '2021-11-12 06:00:04', NULL, NULL),
('9d4f762b-6568-49db-9854-04537536a43e', 'fc6dc06b-8485-4947-a24c-1a0f6a5c78a7', 'a567f842-499f-45bc-9d41-eab620e8ae87', NULL, 'IURAN_PER_BULAN', '0.00', 'REGISTRASI_ULANG', 'FJSM0IKCKS', 'LUNAS', '2021-07-06 00:00:00', '250000.00', 'Jul 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL),
('a4b061a7-5789-4a84-8d3b-62335a1aabd2', 'fc6dc06b-8485-4947-a24c-1a0f6a5c78a7', 'a567f842-499f-45bc-9d41-eab620e8ae87', NULL, 'DU', '0.00', 'REGISTRASI_ULANG', 'FJSM0IKCKS', 'LUNAS', '2021-07-06 00:00:00', '300000.00', '', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL),
('aaed0b7f-ca04-445e-9706-7d6525ed8c5d', '20165cf0-7ef7-46aa-85b1-410d8d7b1eb9', 'dece2547-fc3c-455a-92a1-c62bed376237', NULL, 'DPMP', '0.00', 'REGISTRASI_ULANG', '4LDPKYN1RT', 'BELUM_LUNAS', '2021-07-05 00:00:00', '50000.00', '', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL),
('ad38fb5c-8510-4d7a-b6a4-3413bcd51b44', '1636643729347', '1636643729381', NULL, 'IURAN_PER_BULAN', '0.00', '', 'EGLKBLCB8X', 'LUNAS', '2021-11-12 00:00:00', '250000.00', 'Oct 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 03:25:52', NULL, NULL),
('b48d9e7f-2e81-4ee5-83e0-305ea885c629', '576d456d-dc10-41e5-9fa7-d5a4aaaf5bbc', 'bc0d2977-24cf-4431-b7b0-3d3b197f817c', NULL, 'IURAN_PER_BULAN', '0.00', 'REGISTRASI_ULANG', 'BZ2X7M7LXC', 'LUNAS', '2021-07-08 00:00:00', '250000.00', 'Jul 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL),
('b7ce4214-49fc-4d8b-9d7e-c87699d58e6c', 'bbfd9b2a-3efe-4fef-bbfa-b43098e6b70a', '152817e6-8ef6-45b2-8557-9122285b4107', NULL, 'DPMP', '0.00', 'REGISTRASI_ULANG', 'GMEIRZ3HLF', 'BELUM_LUNAS', '2021-07-04 00:00:00', '150000.00', '', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL),
('b9080ac8-000b-4bd1-9c19-254e36bc14a4', '1636643729347', '1636643729381', NULL, 'IURAN_PER_BULAN', '0.00', '', 'EGLKBLCB8X', 'LUNAS', '2021-11-12 00:00:00', '250000.00', 'Aug 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 03:25:52', NULL, NULL),
('c01ec5fb-510a-4d52-9588-c09c83f9847d', '576d456d-dc10-41e5-9fa7-d5a4aaaf5bbc', 'bc0d2977-24cf-4431-b7b0-3d3b197f817c', NULL, 'DPMP', '0.00', 'REGISTRASI_ULANG', 'BZ2X7M7LXC', 'BELUM_LUNAS', '2021-07-08 00:00:00', '50000.00', '', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL),
('c2de4a65-c7d6-4050-a63b-1a82fb0fac14', '1636643729347', '1636643729381', NULL, 'TABUNGAN', '0.00', '', 'EGLKBLCB8X', 'LUNAS', '2021-11-12 00:00:00', '150000.00', 'Aug 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 03:25:52', NULL, NULL),
('cbb54adf-29b8-4c2c-8bb5-19a839632eb4', '1636643729347', '1636643729381', NULL, 'TABUNGAN', '0.00', '', 'ZL67PJJ310', 'LUNAS', '2021-11-12 00:00:00', '50000.00', 'Oct 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 05:54:32', NULL, NULL),
('cdfc9d76-856e-4532-9005-689a81f142b9', '20165cf0-7ef7-46aa-85b1-410d8d7b1eb9', 'dece2547-fc3c-455a-92a1-c62bed376237', NULL, 'DU', '0.00', 'REGISTRASI_ULANG', '4LDPKYN1RT', 'LUNAS', '2021-07-05 00:00:00', '300000.00', '', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL),
('d9c12678-83bd-48d6-9604-0705703dc3f7', '1636643729347', '1636643729381', NULL, 'IURAN_PER_BULAN', '0.00', '', 'ZL67PJJ310', 'LUNAS', '2021-11-12 00:00:00', '250000.00', 'Jan 2022', NULL, 0, 'Admin SMANBULL', '2021-11-12 05:54:32', NULL, NULL),
('db94377a-b553-4b8a-97ae-2437c9a13ac4', 'bbfd9b2a-3efe-4fef-bbfa-b43098e6b70a', '152817e6-8ef6-45b2-8557-9122285b4107', NULL, 'DU', '0.00', 'REGISTRASI_ULANG', 'GMEIRZ3HLF', 'LUNAS', '2021-07-04 00:00:00', '300000.00', '', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL),
('de86879f-652c-42e6-94b8-da639bc3805b', '1636643729347', '1636643729381', NULL, 'TABUNGAN', '0.00', '', 'EWRMAU95VH', 'LUNAS', '2021-11-12 00:00:00', '30000.00', 'Oct 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 05:58:57', NULL, NULL),
('e2b14080-fd97-4516-b199-450f419e519a', '1636643729347', '1636643729381', NULL, 'IURAN_PER_BULAN', '0.00', '', 'MY56KD2OMY', 'LUNAS', '2021-11-12 00:00:00', '250000.00', 'Dec 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 04:55:28', NULL, NULL),
('fec8770b-fe01-4b47-9ffc-b7b4a826bff3', 'bbfd9b2a-3efe-4fef-bbfa-b43098e6b70a', '152817e6-8ef6-45b2-8557-9122285b4107', NULL, 'IURAN_PER_BULAN', '0.00', 'REGISTRASI_ULANG', 'GMEIRZ3HLF', 'LUNAS', '2021-07-04 00:00:00', '250000.00', 'Jul 2021', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL),
('ffe7e1bf-67a1-4971-b4cf-51b61d711063', 'ff23199f-e279-468d-9a4e-889f35c8be1e', '707ce69a-4f97-49fd-9b24-7ba5dd4c129f', NULL, 'DPMP', '0.00', 'REGISTRASI_ULANG', 'QEPY1UKETO', 'BELUM_LUNAS', '2021-07-07 00:00:00', '50000.00', '', NULL, 0, 'Admin SMANBULL', '2021-11-12 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kp_jenis_iuran`
--

CREATE TABLE `kp_jenis_iuran` (
  `id_jenis_iuran` int(11) NOT NULL,
  `nama_jenis_iuran` varchar(50) DEFAULT NULL,
  `tingkat_jenis_iuran` varchar(50) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `cicilan_jenis_iuran` int(11) NOT NULL,
  `besar_jenis_iuran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kp_jenis_iuran`
--

INSERT INTO `kp_jenis_iuran` (`id_jenis_iuran`, `nama_jenis_iuran`, `tingkat_jenis_iuran`, `keterangan`, `cicilan_jenis_iuran`, `besar_jenis_iuran`) VALUES
(1, 'IURAN_PER_BULAN', 'X', NULL, 0, 250000),
(2, 'DU', 'X', NULL, 0, 300000),
(3, 'DPMP', 'ALL', NULL, 0, 3000000),
(4, 'TABUNGAN', 'X', NULL, 0, 150000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kp_kelas`
--

CREATE TABLE `kp_kelas` (
  `id_kelas` varchar(50) NOT NULL DEFAULT '',
  `jurusan_kelas` varchar(50) DEFAULT NULL,
  `tingkat_kelas` varchar(50) DEFAULT NULL,
  `sub_tingkat_kelas` varchar(50) DEFAULT NULL,
  `wali_kelas` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kp_kelas`
--

INSERT INTO `kp_kelas` (`id_kelas`, `jurusan_kelas`, `tingkat_kelas`, `sub_tingkat_kelas`, `wali_kelas`) VALUES
('1636550199150', 'MIPA', 'X', '2', 'Rahmat'),
('1636550284970', 'MIPA', 'X', '3', 'Rahmat'),
('1636550294974', 'MIPA', 'X', '4', 'Rahmat'),
('1636550309773', 'MIPA', 'X', '5', 'Rahmat'),
('1636550483482', 'BAHASA', 'X', '1', 'Ahmad'),
('1636550483647', 'MIPA', 'X', '1', 'Rahmat'),
('1636732803672', 'MIPA', 'X', '6', 'Andis Nur Ikmah'),
('1636733095553', 'MIPA', 'XI', '1', ''),
('1636733114431', 'MIPA', 'XI', '2', ''),
('1636733120697', 'MIPA', 'XI', '3', ''),
('1636733127215', 'MIPA', 'XI', '4', ''),
('1636733132780', 'MIPA', 'XI', '5', ''),
('1636733138623', 'MIPA', 'XI', '6', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kp_kwitansi_iuran`
--

CREATE TABLE `kp_kwitansi_iuran` (
  `id_kwitansi_iuran` int(11) NOT NULL,
  `id_siswa_kelas_kwitansi_iuran` varchar(50) DEFAULT NULL,
  `keterangan_iuran` varchar(100) DEFAULT NULL,
  `kode_kwitansi_iuran` varchar(50) DEFAULT NULL,
  `tanggal_kwitansi_iuran` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kp_kwitansi_iuran`
--

INSERT INTO `kp_kwitansi_iuran` (`id_kwitansi_iuran`, `id_siswa_kelas_kwitansi_iuran`, `keterangan_iuran`, `kode_kwitansi_iuran`, `tanggal_kwitansi_iuran`) VALUES
(7, '1636643729381', NULL, 'INV-20211111222704', '2021-11-11 22:27:04'),
(8, '1636643729381', NULL, 'INV-20211111234807', '2021-11-11 23:48:07'),
(9, '1636643729381', NULL, 'INV-20211111234825', '2021-11-11 23:48:25'),
(10, '1636643729381', NULL, 'INV-20211111235659', '2021-11-11 23:56:59'),
(11, '1636643729381', NULL, 'INV-20211112001440', '2021-11-12 00:14:40'),
(12, '1636643729381', NULL, 'INV-20211112002111', '2021-11-12 00:21:11'),
(13, '1636643729381', NULL, 'INV-20211112002954', '2021-11-12 00:29:54'),
(14, '1636643729381', NULL, 'INV-20211112021448', '2021-11-12 02:14:48'),
(15, '1636643729381', NULL, 'INV-20211112031839', '2021-11-12 03:18:39'),
(16, '1636643729381', NULL, 'INV-20211112032552', '2021-11-12 03:25:52'),
(17, '1636643729381', NULL, 'INV-20211112032730', '2021-11-12 03:27:30'),
(18, '1636643729381', NULL, 'INV-20211112032820', '2021-11-12 03:28:20'),
(19, '1636643729381', NULL, 'INV-20211112045528', '2021-11-12 04:55:28'),
(20, '1636643729381', NULL, 'INV-20211112045607', '2021-11-12 04:56:07'),
(21, '1636643729381', NULL, 'INV-20211112050856', '2021-11-12 05:08:56'),
(22, '1636643729381', NULL, 'INV-20211112051506', '2021-11-12 05:15:06'),
(23, '1636643729381', NULL, 'INV-20211112051640', '2021-11-12 05:16:40'),
(24, '1636643729381', NULL, 'INV-20211112051934', '2021-11-12 05:19:34'),
(25, '1636643729381', NULL, 'INV-20211112052103', '2021-11-12 05:21:03'),
(26, '1636643729381', NULL, 'INV-20211112052230', '2021-11-12 05:22:30'),
(27, '1636643729381', NULL, 'INV-20211112052651', '2021-11-12 05:26:51'),
(28, '1636643729381', NULL, 'INV-20211112052944', '2021-11-12 05:29:44'),
(29, '1636643729381', NULL, 'INV-20211112053331', '2021-11-12 05:33:31'),
(30, '1636643729381', NULL, 'INV-20211112053716', '2021-11-12 05:37:16'),
(31, '1636643729381', NULL, 'INV-20211112053803', '2021-11-12 05:38:03'),
(32, '1636643729381', NULL, 'INV-20211112054718', '2021-11-12 05:47:18'),
(33, '1636643729381', NULL, 'INV-20211112054740', '2021-11-12 05:47:40'),
(34, '1636643729381', NULL, 'INV-20211112054809', '2021-11-12 05:48:09'),
(35, '1636643729381', NULL, 'INV-20211112055049', '2021-11-12 05:50:49'),
(36, '1636643729381', NULL, 'INV-20211112055108', '2021-11-12 05:51:08'),
(37, '1636643729381', NULL, 'INV-20211112055432', '2021-11-12 05:54:32'),
(38, '1636643729381', NULL, 'INV-20211112055453', '2021-11-12 05:54:53'),
(39, '1636643729381', NULL, 'INV-20211112055506', '2021-11-12 05:55:06'),
(40, '1636643729381', NULL, 'INV-20211112055518', '2021-11-12 05:55:18'),
(41, '1636643729381', NULL, 'INV-20211112055611', '2021-11-12 05:56:11'),
(42, '1636643729381', NULL, 'INV-20211112055817', '2021-11-12 05:58:17'),
(43, '1636643729381', NULL, 'INV-20211112055857', '2021-11-12 05:58:57'),
(44, '1636643729381', NULL, 'INV-20211112055930', '2021-11-12 05:59:30'),
(45, '1636643729381', NULL, 'INV-20211112060004', '2021-11-12 06:00:04'),
(46, '1636643729381', NULL, 'INV-20211112060015', '2021-11-12 06:00:15'),
(47, '1636643729381', NULL, 'INV-20211112060035', '2021-11-12 06:00:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kp_siswa`
--

CREATE TABLE `kp_siswa` (
  `id_siswa` varchar(50) NOT NULL DEFAULT '',
  `ref_siswa` varchar(50) DEFAULT NULL,
  `nama_lengkap_siswa` varchar(100) DEFAULT NULL,
  `jenis_kelamin_siswa` varchar(50) DEFAULT NULL,
  `keterangan_masuk_siswa` varchar(100) DEFAULT NULL,
  `status_aktif_siswa` tinyint(1) DEFAULT NULL,
  `tahun_angkatan_siswa` varchar(50) DEFAULT NULL,
  `user_added_siswa` varchar(50) DEFAULT NULL,
  `date_added_siswa` date DEFAULT NULL,
  `user_deleted_siswa` varchar(50) DEFAULT NULL,
  `date_deleted_siswa` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kp_siswa`
--

INSERT INTO `kp_siswa` (`id_siswa`, `ref_siswa`, `nama_lengkap_siswa`, `jenis_kelamin_siswa`, `keterangan_masuk_siswa`, `status_aktif_siswa`, `tahun_angkatan_siswa`, `user_added_siswa`, `date_added_siswa`, `user_deleted_siswa`, `date_deleted_siswa`) VALUES
('1636643729347', '1111', 'Kayum Munajir', 'LAKI_LAKI', 'JALUR NORMAL', 1, '2021', 'Admin SMANBULL', '2021-11-11', NULL, NULL),
('1636643729923', '2222', 'Cecep Supriatna', 'LAKI_LAKI', 'JALUR NORMAL', 1, '2021', 'Admin SMANBULL', '2021-11-11', NULL, NULL),
('1636643729957', '3333', 'Ali Ashabullah', 'LAKI_LAKI', 'JALUR NORMAL', 1, '2021', 'Admin SMANBULL', '2021-11-11', NULL, NULL),
('20165cf0-7ef7-46aa-85b1-410d8d7b1eb9', '13393', 'ADILA SHOLIHAH', 'PEREMPUAN', 'Zonasi Jarak', 1, '2021', 'Admin SMANBULL', '2021-11-12', NULL, NULL),
('339818e8-42d6-4b68-9ee5-cd17688bcdee', '13311', 'ABDULLATIF AZIS', 'LAKI_LAKI', 'Zonasi Jarak', 1, '2021', 'Admin SMANBULL', '2021-11-12', NULL, NULL),
('576d456d-dc10-41e5-9fa7-d5a4aaaf5bbc', '13426', 'ANISA YUNIANTI', 'PEREMPUAN', 'Zonasi Jarak', 1, '2021', 'Admin SMANBULL', '2021-11-12', NULL, NULL),
('bbfd9b2a-3efe-4fef-bbfa-b43098e6b70a', '13389', 'ADE LIA RUSDIANTI', 'PEREMPUAN', 'Zonasi Jarak', 1, '2021', 'Admin SMANBULL', '2021-11-12', NULL, NULL),
('fc6dc06b-8485-4947-a24c-1a0f6a5c78a7', '13406', 'AINAYA MULIA HAQ', 'PEREMPUAN', 'Zonasi Jarak', 1, '2021', 'Admin SMANBULL', '2021-11-12', NULL, NULL),
('ff23199f-e279-468d-9a4e-889f35c8be1e', '13412', 'ALVIN KURNIAWAN', 'LAKI_LAKI', 'Zonasi Jarak', 1, '2021', 'Admin SMANBULL', '2021-11-12', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kp_siswa_detil`
--

CREATE TABLE `kp_siswa_detil` (
  `id_siswa_detil` varchar(50) NOT NULL DEFAULT '',
  `header_id_siswa_detil` varchar(100) DEFAULT NULL,
  `header_id_siswa_kelas_detil` varchar(100) DEFAULT NULL,
  `jenis_iuran_siswa_detil` varchar(50) DEFAULT NULL,
  `besar_iuran_siswa_detil` int(11) DEFAULT NULL,
  `potongan_iuran_siswa_detil` int(11) DEFAULT NULL,
  `tingkat_siswa_detil` varchar(50) DEFAULT NULL,
  `user_added_siswa_detil` varchar(50) DEFAULT NULL,
  `date_added_siswa_detil` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kp_siswa_detil`
--

INSERT INTO `kp_siswa_detil` (`id_siswa_detil`, `header_id_siswa_detil`, `header_id_siswa_kelas_detil`, `jenis_iuran_siswa_detil`, `besar_iuran_siswa_detil`, `potongan_iuran_siswa_detil`, `tingkat_siswa_detil`, `user_added_siswa_detil`, `date_added_siswa_detil`) VALUES
('045d33d4-239b-4e0a-9ff7-0e3a036c0bab', '339818e8-42d6-4b68-9ee5-cd17688bcdee', NULL, 'DU', 300000, NULL, 'X', 'Admin SMANBULL', '2021-11-12'),
('08ea6949-a620-45be-a8da-9304e91ef2c6', 'ff23199f-e279-468d-9a4e-889f35c8be1e', NULL, 'DU', 300000, NULL, 'X', 'Admin SMANBULL', '2021-11-12'),
('0f12b060-2bfd-43bb-9af4-8fa9a9dc7e56', '576d456d-dc10-41e5-9fa7-d5a4aaaf5bbc', NULL, 'IURAN_PER_BULAN', 250000, NULL, 'X', 'Admin SMANBULL', '2021-11-12'),
('160daec6-c019-48c9-91d3-056e0144677d', 'fc6dc06b-8485-4947-a24c-1a0f6a5c78a7', NULL, 'DU', 300000, NULL, 'X', 'Admin SMANBULL', '2021-11-12'),
('1636643729132', '1636643729957', NULL, 'TABUNGAN', 150000, NULL, 'X', 'Admin SMANBULL', '2021-11-11'),
('1636643729135', '1636643729957', NULL, 'DPMP', 2500000, NULL, 'ALL', 'Admin SMANBULL', '2021-11-11'),
('1636643729170', '1636643729923', NULL, 'DU', 300000, NULL, 'X', 'Admin SMANBULL', '2021-11-11'),
('1636643729172', '1636643729957', NULL, 'IURAN_PER_BULAN', 150000, NULL, 'X', 'Admin SMANBULL', '2021-11-11'),
('1636643729285', '1636643729347', NULL, 'DU', 300000, NULL, 'X', 'Admin SMANBULL', '2021-11-11'),
('1636643729313', '1636643729347', NULL, 'TABUNGAN', 150000, NULL, 'X', 'Admin SMANBULL', '2021-11-11'),
('1636643729336', '1636643729347', NULL, 'IURAN_PER_BULAN', 250000, NULL, 'X', 'Admin SMANBULL', '2021-11-11'),
('1636643729402', '1636643729923', NULL, 'DPMP', 3000000, NULL, 'ALL', 'Admin SMANBULL', '2021-11-11'),
('1636643729681', '1636643729923', NULL, 'IURAN_PER_BULAN', 250000, NULL, 'X', 'Admin SMANBULL', '2021-11-11'),
('1636643729714', '1636643729957', NULL, 'DU', 250000, NULL, 'X', 'Admin SMANBULL', '2021-11-11'),
('1636643729926', '1636643729923', NULL, 'TABUNGAN', 150000, NULL, 'X', 'Admin SMANBULL', '2021-11-11'),
('1636643729969', '1636643729347', NULL, 'DPMP', 3000000, NULL, 'ALL', 'Admin SMANBULL', '2021-11-11'),
('1adc2cf4-9200-4754-80fe-00a49a6e54ed', '576d456d-dc10-41e5-9fa7-d5a4aaaf5bbc', NULL, 'DU', 300000, NULL, 'X', 'Admin SMANBULL', '2021-11-12'),
('1e704746-626f-4f44-bc22-e05de2fc740a', 'fc6dc06b-8485-4947-a24c-1a0f6a5c78a7', NULL, 'DPMP', 3000000, NULL, 'ALL', 'Admin SMANBULL', '2021-11-12'),
('2152f91f-0357-4c7f-82fa-3715accda179', '339818e8-42d6-4b68-9ee5-cd17688bcdee', NULL, 'DPMP', 3000000, NULL, 'ALL', 'Admin SMANBULL', '2021-11-12'),
('227e3668-5afd-44fd-a7d8-2a4d061f919c', '20165cf0-7ef7-46aa-85b1-410d8d7b1eb9', NULL, 'DU', 300000, NULL, 'X', 'Admin SMANBULL', '2021-11-12'),
('2458aca8-7888-4072-bd6f-67c059d48b51', '576d456d-dc10-41e5-9fa7-d5a4aaaf5bbc', NULL, 'DPMP', 3000000, NULL, 'ALL', 'Admin SMANBULL', '2021-11-12'),
('2e9f57c7-bcf5-48b7-abc6-61782202dcdc', 'bbfd9b2a-3efe-4fef-bbfa-b43098e6b70a', NULL, 'TABUNGAN', 50000, NULL, 'X', 'Admin SMANBULL', '2021-11-12'),
('581fdabb-4867-46ce-beb8-ad3d2111516e', '339818e8-42d6-4b68-9ee5-cd17688bcdee', NULL, 'IURAN_PER_BULAN', 250000, NULL, 'X', 'Admin SMANBULL', '2021-11-12'),
('6713a7a2-4736-4475-8dca-f8fd7e7eccdc', '20165cf0-7ef7-46aa-85b1-410d8d7b1eb9', NULL, 'DPMP', 3000000, NULL, 'ALL', 'Admin SMANBULL', '2021-11-12'),
('6aa8c1d7-b4db-488b-a626-b3e153cee3e4', 'bbfd9b2a-3efe-4fef-bbfa-b43098e6b70a', NULL, 'DPMP', 3000000, NULL, 'ALL', 'Admin SMANBULL', '2021-11-12'),
('763d77b4-7f4e-4210-9b0c-7daccff2bbd5', '339818e8-42d6-4b68-9ee5-cd17688bcdee', NULL, 'TABUNGAN', 50000, NULL, 'X', 'Admin SMANBULL', '2021-11-12'),
('96ccda70-b600-4386-a945-9514e4c0e31c', 'ff23199f-e279-468d-9a4e-889f35c8be1e', NULL, 'IURAN_PER_BULAN', 250000, NULL, 'X', 'Admin SMANBULL', '2021-11-12'),
('9f5f3ef3-07b8-46f1-bc54-b61c314cd776', '20165cf0-7ef7-46aa-85b1-410d8d7b1eb9', NULL, 'IURAN_PER_BULAN', 250000, NULL, 'X', 'Admin SMANBULL', '2021-11-12'),
('a2e6506b-191a-41cc-aa27-722d0f271a6c', 'ff23199f-e279-468d-9a4e-889f35c8be1e', NULL, 'TABUNGAN', 50000, NULL, 'X', 'Admin SMANBULL', '2021-11-12'),
('a84c2398-86c8-477e-ac2c-bc41fe2344c0', '20165cf0-7ef7-46aa-85b1-410d8d7b1eb9', NULL, 'TABUNGAN', 50000, NULL, 'X', 'Admin SMANBULL', '2021-11-12'),
('b2770d77-29ed-44bf-9c8d-57f9712d22ed', '576d456d-dc10-41e5-9fa7-d5a4aaaf5bbc', NULL, 'TABUNGAN', 50000, NULL, 'X', 'Admin SMANBULL', '2021-11-12'),
('b808696e-2fe8-41e2-8406-a29473007087', 'fc6dc06b-8485-4947-a24c-1a0f6a5c78a7', NULL, 'TABUNGAN', 50000, NULL, 'X', 'Admin SMANBULL', '2021-11-12'),
('d05082da-42d3-47c2-b924-29e189ab8b07', 'ff23199f-e279-468d-9a4e-889f35c8be1e', NULL, 'DPMP', 3000000, NULL, 'ALL', 'Admin SMANBULL', '2021-11-12'),
('d580d37c-5993-431f-ab1f-7af4c55f5a9b', 'fc6dc06b-8485-4947-a24c-1a0f6a5c78a7', NULL, 'IURAN_PER_BULAN', 250000, NULL, 'X', 'Admin SMANBULL', '2021-11-12'),
('ddadd206-b932-4f9c-becd-f6dac24bd344', 'bbfd9b2a-3efe-4fef-bbfa-b43098e6b70a', NULL, 'DU', 300000, NULL, 'X', 'Admin SMANBULL', '2021-11-12'),
('f36c18e6-1e86-4b5a-8b5e-4a5ff4fccda3', 'bbfd9b2a-3efe-4fef-bbfa-b43098e6b70a', NULL, 'IURAN_PER_BULAN', 250000, NULL, 'X', 'Admin SMANBULL', '2021-11-12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kp_siswa_kelas`
--

CREATE TABLE `kp_siswa_kelas` (
  `id_siswa_kelas` varchar(50) NOT NULL DEFAULT '',
  `header_id_siswa_kelas` varchar(100) DEFAULT NULL,
  `kelas_siswa_kelas` bigint(20) NOT NULL,
  `status_siswa_kelas` tinyint(1) DEFAULT NULL,
  `tahun_ajaran_siswa_kelas` varchar(50) DEFAULT NULL,
  `tanggal_registrasi_siswa_kelas` date DEFAULT NULL,
  `user_added_siswa_kelas` varchar(50) DEFAULT NULL,
  `date_added_siswa_kelas` date DEFAULT NULL,
  `user_mofied_siswa_kelas` varchar(50) DEFAULT NULL,
  `date_mofied_siswa_kelas` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kp_siswa_kelas`
--

INSERT INTO `kp_siswa_kelas` (`id_siswa_kelas`, `header_id_siswa_kelas`, `kelas_siswa_kelas`, `status_siswa_kelas`, `tahun_ajaran_siswa_kelas`, `tanggal_registrasi_siswa_kelas`, `user_added_siswa_kelas`, `date_added_siswa_kelas`, `user_mofied_siswa_kelas`, `date_mofied_siswa_kelas`) VALUES
('152817e6-8ef6-45b2-8557-9122285b4107', 'bbfd9b2a-3efe-4fef-bbfa-b43098e6b70a', 1636550294974, 1, '2021/2022', '2021-07-04', 'Admin SMANBULL', '2021-11-12', NULL, NULL),
('1636643729261', '1636643729923', 1636550483647, 1, '2021/2022', '2021-07-11', 'Admin SMANBULL', '2021-11-11', NULL, NULL),
('1636643729381', '1636643729347', 1636550483647, 1, '2021/2022', '2021-07-10', 'Admin SMANBULL', '2021-11-11', NULL, NULL),
('1636643729770', '1636643729957', 1636550483647, 1, '2021/2022', '2021-07-11', 'Admin SMANBULL', '2021-11-11', NULL, NULL),
('707ce69a-4f97-49fd-9b24-7ba5dd4c129f', 'ff23199f-e279-468d-9a4e-889f35c8be1e', 1636550294974, 1, '2021/2022', '2021-07-07', 'Admin SMANBULL', '2021-11-12', NULL, NULL),
('8a343a67-807a-4626-a24f-52bf5e07314f', '339818e8-42d6-4b68-9ee5-cd17688bcdee', 1636550483647, 1, '2021/2022', '2021-07-03', 'Admin SMANBULL', '2021-11-12', NULL, NULL),
('a567f842-499f-45bc-9d41-eab620e8ae87', 'fc6dc06b-8485-4947-a24c-1a0f6a5c78a7', 1636732803672, 1, '2021/2022', '2021-07-06', 'Admin SMANBULL', '2021-11-12', NULL, NULL),
('bc0d2977-24cf-4431-b7b0-3d3b197f817c', '576d456d-dc10-41e5-9fa7-d5a4aaaf5bbc', 1636550483482, 1, '2021/2022', '2021-07-08', 'Admin SMANBULL', '2021-11-12', NULL, NULL),
('dece2547-fc3c-455a-92a1-c62bed376237', '20165cf0-7ef7-46aa-85b1-410d8d7b1eb9', 1636550284970, 1, '2021/2022', '2021-07-05', 'Admin SMANBULL', '2021-11-12', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_role_user`
--

CREATE TABLE `tbl_role_user` (
  `code_role_user` varchar(50) DEFAULT NULL,
  `name_role_user` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_role_user`
--

INSERT INTO `tbl_role_user` (`code_role_user`, `name_role_user`) VALUES
('ADMIN', 'Admin'),
('TU', 'TU'),
('STAFF_TU', 'Staff TU'),
('KEPALA_SEKOLAH', 'Kepala Sekolah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` int(11) NOT NULL,
  `firstname_user` varchar(50) DEFAULT NULL,
  `lastname_user` varchar(50) DEFAULT NULL,
  `username_user` varchar(50) DEFAULT NULL,
  `password_user` varchar(50) DEFAULT NULL,
  `role_user` varchar(50) DEFAULT NULL,
  `status_user` varchar(50) DEFAULT NULL,
  `user_added_user` varchar(50) DEFAULT NULL,
  `date_added_user` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `firstname_user`, `lastname_user`, `username_user`, `password_user`, `role_user`, `status_user`, `user_added_user`, `date_added_user`) VALUES
(1, 'Admin', 'SMANBULL', 'admin', 'c4ca4238a0b923820dcc509a6f75849b', 'ADMIN', '1', NULL, NULL),
(2, 'TU', 'Satu', 'tu', 'c4ca4238a0b923820dcc509a6f75849b', 'TU', '1', NULL, NULL),
(3, 'Kepala', 'Sekolah', 'sekolah', 'c4ca4238a0b923820dcc509a6f75849b', 'KEPALA_SEKOLAH', '1', NULL, NULL),
(4, 'STAFF', 'TU', 'stafftu', 'c4ca4238a0b923820dcc509a6f75849b', 'STAFF_TU', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `_not_used_kp_new_iuran`
--

CREATE TABLE `_not_used_kp_new_iuran` (
  `id_iuran` int(11) NOT NULL,
  `id_siswa_iuran` varchar(100) NOT NULL,
  `id_siswa_kelas_iuran` varchar(100) NOT NULL,
  `jatuh_tempo_iuran` date DEFAULT NULL,
  `jenis_iuran` varchar(50) DEFAULT NULL,
  `status_iuran` varchar(100) DEFAULT NULL,
  `besar_iuran` decimal(15,2) DEFAULT 0.00,
  `bulan_iuran` varchar(50) DEFAULT NULL,
  `cicilan_iuran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `_not_used_kp_new_iuran_detil`
--

CREATE TABLE `_not_used_kp_new_iuran_detil` (
  `id_iuran_detil` int(11) NOT NULL,
  `header_iuran_detil` varchar(100) DEFAULT NULL,
  `jenis_iuran` varchar(50) DEFAULT NULL,
  `no_iuran` varchar(50) DEFAULT NULL,
  `status_iuran` varchar(100) DEFAULT NULL,
  `tanggal_iuran` datetime DEFAULT NULL,
  `besar_iuran` decimal(15,2) DEFAULT 0.00,
  `cicilan_iuran` int(11) NOT NULL,
  `user_added` varchar(50) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `user_modified` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `_not_used_kp_new_siswa_detil`
--

CREATE TABLE `_not_used_kp_new_siswa_detil` (
  `id_siswa_detil` int(11) NOT NULL,
  `header_id_kelas_siswa_detil` varchar(100) DEFAULT NULL,
  `iuran_bulanan_siswa_detil` varchar(50) DEFAULT NULL,
  `iuran_dpmp_siswa_detil` varchar(50) DEFAULT NULL,
  `iuran_du_siswa_detil` varchar(50) DEFAULT NULL,
  `iuran_tabungan_siswa_detil` varchar(50) DEFAULT NULL,
  `tingkat_siswa_detil` varchar(50) DEFAULT NULL,
  `user_added_siswa_detil` varchar(50) DEFAULT NULL,
  `date_added_siswa_detil` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `_not_used_kp_siswa_detil_new`
--

CREATE TABLE `_not_used_kp_siswa_detil_new` (
  `id_siswa_detil` int(11) NOT NULL,
  `header_id_siswa_detil` varchar(100) DEFAULT NULL,
  `jenis_iuran_siswa_detil` varchar(50) DEFAULT NULL,
  `besar_iuran_siswa_detil` int(11) DEFAULT NULL,
  `potongan_iuran_siswa_detil` int(11) DEFAULT NULL,
  `tingkat_siswa_detil` varchar(50) DEFAULT NULL,
  `user_added_siswa_detil` varchar(50) DEFAULT NULL,
  `date_added_siswa_detil` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `kp_iuran`
--
ALTER TABLE `kp_iuran`
  ADD PRIMARY KEY (`id_iuran`);

--
-- Indeks untuk tabel `kp_jenis_iuran`
--
ALTER TABLE `kp_jenis_iuran`
  ADD PRIMARY KEY (`id_jenis_iuran`);

--
-- Indeks untuk tabel `kp_kelas`
--
ALTER TABLE `kp_kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indeks untuk tabel `kp_kwitansi_iuran`
--
ALTER TABLE `kp_kwitansi_iuran`
  ADD PRIMARY KEY (`id_kwitansi_iuran`);

--
-- Indeks untuk tabel `kp_siswa`
--
ALTER TABLE `kp_siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indeks untuk tabel `kp_siswa_detil`
--
ALTER TABLE `kp_siswa_detil`
  ADD PRIMARY KEY (`id_siswa_detil`);

--
-- Indeks untuk tabel `kp_siswa_kelas`
--
ALTER TABLE `kp_siswa_kelas`
  ADD PRIMARY KEY (`id_siswa_kelas`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indeks untuk tabel `_not_used_kp_new_iuran`
--
ALTER TABLE `_not_used_kp_new_iuran`
  ADD PRIMARY KEY (`id_iuran`);

--
-- Indeks untuk tabel `_not_used_kp_new_iuran_detil`
--
ALTER TABLE `_not_used_kp_new_iuran_detil`
  ADD PRIMARY KEY (`id_iuran_detil`);

--
-- Indeks untuk tabel `_not_used_kp_new_siswa_detil`
--
ALTER TABLE `_not_used_kp_new_siswa_detil`
  ADD PRIMARY KEY (`id_siswa_detil`);

--
-- Indeks untuk tabel `_not_used_kp_siswa_detil_new`
--
ALTER TABLE `_not_used_kp_siswa_detil_new`
  ADD PRIMARY KEY (`id_siswa_detil`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `kp_jenis_iuran`
--
ALTER TABLE `kp_jenis_iuran`
  MODIFY `id_jenis_iuran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `kp_kwitansi_iuran`
--
ALTER TABLE `kp_kwitansi_iuran`
  MODIFY `id_kwitansi_iuran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
