-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2019 at 04:23 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kp_smanbull`
--

-- --------------------------------------------------------

--
-- Table structure for table `kp_iuran_dpmp_siswa`
--

CREATE TABLE `kp_iuran_dpmp_siswa` (
  `id_iuaran_dpmp_siswa` int(11) NOT NULL,
  `id_siswa_iuaran_dpmp_siswa` int(11) NOT NULL,
  `nominal_iuaran_iuaran_dpmp_siswa` int(11) NOT NULL,
  `nominal_dpmp_iuaran_dpmp_siswa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kp_pembayaran`
--

CREATE TABLE `kp_pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `id_siswa_pembayaran` int(11) NOT NULL,
  `bulan_pembayaran` int(11) NOT NULL,
  `iuran_pembayaran` int(11) NOT NULL,
  `dpmp_pembayaran` int(11) NOT NULL,
  `du_pembayaran` int(11) NOT NULL,
  `keterangan_pembayaran` varchar(50) NOT NULL,
  `tanggal_pembayaran` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kp_rekap_pembayaran`
--

CREATE TABLE `kp_rekap_pembayaran` (
  `id_rekap_pembayaran` int(11) NOT NULL,
  `header_rekap_pembayaran` int(11) NOT NULL,
  `keterangan_kelas_rekap_pembayaran` varchar(50) NOT NULL,
  `jumlah_iuaran_rekap_pembayaran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kp_siswa`
--

CREATE TABLE `kp_siswa` (
  `id_siswa` int(11) NOT NULL,
  `nis_siswa` varchar(50) NOT NULL,
  `nama_lengkap_siswa` varchar(50) NOT NULL,
  `kelas_siswa` varchar(50) NOT NULL,
  `wali_kelas_siswa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kp_wali_kelas`
--

CREATE TABLE `kp_wali_kelas` (
  `id_wali_kelas` int(11) NOT NULL,
  `nama_walikelas` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kp_iuran_dpmp_siswa`
--
ALTER TABLE `kp_iuran_dpmp_siswa`
  ADD PRIMARY KEY (`id_iuaran_dpmp_siswa`),
  ADD KEY `kp_iuran_dpmp_siswa_fk0` (`id_siswa_iuaran_dpmp_siswa`);

--
-- Indexes for table `kp_pembayaran`
--
ALTER TABLE `kp_pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`),
  ADD KEY `kp_pembayaran_fk0` (`id_siswa_pembayaran`);

--
-- Indexes for table `kp_rekap_pembayaran`
--
ALTER TABLE `kp_rekap_pembayaran`
  ADD PRIMARY KEY (`id_rekap_pembayaran`);

--
-- Indexes for table `kp_siswa`
--
ALTER TABLE `kp_siswa`
  ADD PRIMARY KEY (`id_siswa`),
  ADD KEY `kp_siswa_fk0` (`wali_kelas_siswa`);

--
-- Indexes for table `kp_wali_kelas`
--
ALTER TABLE `kp_wali_kelas`
  ADD PRIMARY KEY (`id_wali_kelas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kp_iuran_dpmp_siswa`
--
ALTER TABLE `kp_iuran_dpmp_siswa`
  MODIFY `id_iuaran_dpmp_siswa` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kp_pembayaran`
--
ALTER TABLE `kp_pembayaran`
  MODIFY `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kp_rekap_pembayaran`
--
ALTER TABLE `kp_rekap_pembayaran`
  MODIFY `id_rekap_pembayaran` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kp_siswa`
--
ALTER TABLE `kp_siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kp_wali_kelas`
--
ALTER TABLE `kp_wali_kelas`
  MODIFY `id_wali_kelas` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `kp_iuran_dpmp_siswa`
--
ALTER TABLE `kp_iuran_dpmp_siswa`
  ADD CONSTRAINT `kp_iuran_dpmp_siswa_fk0` FOREIGN KEY (`id_siswa_iuaran_dpmp_siswa`) REFERENCES `kp_siswa` (`id_siswa`);

--
-- Constraints for table `kp_pembayaran`
--
ALTER TABLE `kp_pembayaran`
  ADD CONSTRAINT `kp_pembayaran_fk0` FOREIGN KEY (`id_siswa_pembayaran`) REFERENCES `kp_siswa` (`id_siswa`);

--
-- Constraints for table `kp_siswa`
--
ALTER TABLE `kp_siswa`
  ADD CONSTRAINT `kp_siswa_fk0` FOREIGN KEY (`wali_kelas_siswa`) REFERENCES `kp_wali_kelas` (`id_wali_kelas`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
