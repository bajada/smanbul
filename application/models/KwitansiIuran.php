<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KwitansiIuran extends CI_Model {
	var $table = 'kp_kwitansi_iuran';
	var $table_id = 'id_kwitansi_iuran';
	var $order = array('id_kwitansi_iuran' => 'desc');


	public function __construct() {
		parent::__construct();
	}

	public function newId() {
        $query = $this->db->query("SELECT max($this->table_id) newid from {$this->table}");
        $query = $query->row();
		$query = $query->newid + 1;
        return $query;
	}

 	public function save($data) {
		$this->db->trans_start();
			$this->db->insert($this->table, $data);
		$this->db->trans_complete();
		// return $this->db->insert_id();
	} 
	
 	public function update($data) {
		$this->db->where($this->table_id, $data[$this->table_id]);
		$this->db->update($this->table, $data);
		return $this->db->affected_rows();
	}

	public function delete($id) {
		$this->db->where($this->table_id, $id);
		$this->db->delete($this->table);
	}

	public function get_list_table_query($param) {
		$this->db->SELECT('*');    //select *
		$this->db->FROM($this->table); //from table
		// $this->db->where($param);
	}

	public function get_list_table($param) {
		$this->get_list_table_query($param);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered() {
		$this->get_list_table_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

	public function get_by_id($id) {
		$this->db->from($this->table);
		$this->db->where($this->table_id,$id);
		$query = $this->db->get();

		return $query->row();
	}

	//NEW 
	public function table_query2($param) {
		$order="order by kode_kwitansi_iuran";
		$query=$this->db->query("select a.*, c.*, b.kelas_siswa_kelas, b.tahun_ajaran_siswa_kelas, d.*, CONCAT(d.tingkat_kelas,'-', d.jurusan_kelas,'-', d.sub_tingkat_kelas) kelas from kp_kwitansi_iuran a
left join kp_siswa_kelas b on b.id_siswa_kelas = a.id_siswa_kelas_kwitansi_iuran
left join kp_siswa c on c.id_siswa = b.header_id_siswa_kelas
left join kp_kelas d on d.id_kelas = b.kelas_siswa_kelas where {$param} {$order}");
		return $query;
	}
	public function get_list_table2($param="1") {
		$query=$this->table_query2($param);
		return $query->result();
	}
}