<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model {
	var $table = 'tbl_user';
	var $table_id = 'id_user';
	var $order = array('user_id' => 'desc');



	public function __construct() {
		parent::__construct();
		$this->load->database();	
		//$this->load->library('session');
	}

	public function newId() {
        $query = $this->db->query("SELECT max($this->table_id) newid from {$this->table}");
        $query = $query->row();
		$query = $query->newid + 1;
        return $query;
	}

 	public function save($data) {
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	} 
	
 	public function update($data) {
 		// $where = $data['id_user'];
		$this->db->where($this->table_id, $data['id_user']);
		$this->db->update($this->table, $data);
		return $this->db->affected_rows();
	}

	public function delete($id) {
		$this->db->where($this->table_id, $id);
		$this->db->delete($this->table);
	}

	public function get_list_table_query($param) {
       	//$this->db->query("SELECT a.*, b.nama_karyawan FROM tbl_user a LEFT JOIN tbl_karyawan b ON karyawan = b.karyawan_id");
		$this->db->SELECT('*');    //select *
		$this->db->FROM('tbl_user'); //from table
		// $this->db->JOIN('tbl_karyawan b', 'a.karyawan = b.karyawan_id','inner'); //inner join table
		// $this->db->JOIN('tbl_jabatan c', 'jabatan = c.jabatan_id','inner'); //inner join table
		// print_r($param);
		// $i = 0;
		// foreach ($param as $col=>$val) {
		// 	print_r($key);
		// 	// if($_POST['search']['value']) {
		// 	   	if($i===0) {
		// 	   		$this->db->group_start();
		// 	   		$this->db->like($col, $val);
		// 	   	}else {
		// 	   		$this->db->or_like($col, $val);
		// 	   	}

		//    		if(count($this->column_search) - 1 == $i) $this->db->group_end();
		// 	// }
		// 	$i++;
		// }
		$this->db->or_like($param);
		// if(isset($_POST['order'])) // here order processing
		// {
		// 	$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		// } 
		// else if(isset($this->order))
		// {
		// 	$order = $this->order;
		// 	//$this->db->order_by(key($order), $order[key($order)]);
		// }
	}

	public function get_list_table($param) {
		$this->get_list_table_query($param);
		// if($_POST['length'] != -1) $this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered() {
		$this->get_list_table_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

	public function get_by_id($id) {
		$this->db->from($this->table);
		$this->db->where($this->table_id,$id);
		$query = $this->db->get();

		return $query->row();
	}

	
	//NEW 
	public function table_query2($param) {
		$order="";//order by tingkat_kelas
		$query=$this->db->query("select a.*, CONCAT(a.firstname_user, ' ', a.lastname_user) as fullname from {$this->table} a where {$param} {$order}");
		return $query;
	}
	public function get_list_table2($param="1") {
		$query=$this->table_query2($param);
		return $query->result();
	}
}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */ 