<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiswaKelas extends CI_Model {
	var $table = 'kp_siswa_kelas';
	var $table_id = 'id_siswa_kelas';
	var $order = array('id_siswa_kelas' => 'desc');


	public function __construct() {
		parent::__construct();
	}

	public function newId() {
        $query = $this->db->query("SELECT max($this->table_id) newid from {$this->table}");
        $query = $query->row();
		$query = $query->newid + 1;
        return $query;
	}

	public function longNumberId() {
        $range = (999 - 100) + 1;
        $return = (rand() * $range) + 100;
        return time() ."". $return;
	}

 	public function save($data) {
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	} 
 	public function save_kelas($data) {
		$this->db->insert_batch($this->table, $data);
	} 
	
 	public function update($data) {
		$this->db->where($this->table_id, $data[$this->table_id]);
		$this->db->update($this->table, $data);
		return $this->db->affected_rows();
	}
	
 	public function updateAll($data) {
 		$this->db->trans_start();
			$this->db->update_batch($this->table, $data, 'header_id_siswa_kelas');
		$this->db->trans_complete();
	}

	//update batch
	// $this->db->update_batch('mytable', $data, 'title');

	public function delete($id) {
		$this->db->where($this->table_id, $id);
		$this->db->delete($this->table);
	}

	public function get_list_table_query($param) {
		// $order="order by ref_siswa";
		$order="";
		// $query=$this->db->query("SELECT * FROM {$this->table} where {$param} {$order}");
		$query=$this->db->query("select a.*, 
			b.ref_siswa, b.nama_lengkap_siswa, a.tanggal_registrasi_siswa_kelas, b.tahun_angkatan_siswa,
			CONCAT(c.`tingkat_kelas`,' ',c.`jurusan_kelas`,' ',c.`sub_tingkat_kelas`) kelas, c.tingkat_kelas 
			from {$this->table} a
			left join kp_siswa b on b.id_siswa = a.header_id_siswa_kelas
			left join kp_kelas c on c.id_kelas = a.kelas_siswa_kelas where {$param} {$order}");
		return $query;
	}

	public function get_list_table($param) {
		$query=$this->get_list_table_query($param);
		return $query->result();
	}

	// public function get_list_table_query($param) {
	// 	$this->db->SELECT('*');    //select *
	// 	$this->db->FROM($this->table); //from table
	// 	$this->db->where($param);
	// }

	// public function get_list_table($param) {
	// 	$this->get_list_table_query($param);
	// 	$query = $this->db->get();
	// 	return $query->result();
	// }

	function count_filtered() {
		$this->get_list_table_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_list_table_query_2() {
		$query="select a.*, 
			b.ref_siswa, b.nama_lengkap_siswa, a.tanggal_registrasi_siswa_kelas, b.tahun_angkatan_siswa,
			CONCAT(c.`tingkat_kelas`,' ',c.`jurusan_kelas`,' ',c.`sub_tingkat_kelas`) kelas, c.tingkat_kelas 
			from {$this->table} a
			left join kp_siswa b on b.id_siswa = a.header_id_siswa_kelas
			left join kp_kelas c on c.id_kelas = a.kelas_siswa_kelas ";
		return $query;
	}

	public function get_by_id($id) {
		$param=$this->table_id."=".$id;
		$query=$this->get_list_table_query_2();
		$query.="where '{$param}'";
		$query=$this->db->query($query);
		return $query->row();
	}

	public function get_active() {
		$this->db->from($this->table);
		$this->db->where('status_siswa_kelas',1);
		$query = $this->db->get();

		return $query->row();
	}
}