<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function get_list_table_query($param) {
		$order="order by ref_siswa";
		$query=$this->db->query("SELECT a.*, CONCAT(b.`tingkat_kelas`,' ',b.`jurusan_kelas`,' ',b.`sub_tingkat_kelas`) kelas FROM {$this->table} a left join kp_kelas b on a.kelas_siswa=b.id_kelas	 where {$param} {$order}");
		return $query;
	}

	public function get_list_table($param) {
		$query=$this->table_query($param);
		return $query->result();
	}

	public function get_list_harian_uniq($param){
		$query=$this->db->query("select * from (
	select real_tanggal_iuran, count(jumlah_row) jumlah_record from (
		select SUBSTRING_INDEX(tanggal_iuran,' ',1) real_tanggal_iuran, count(*) jumlah_row from kp_iuran
		group by no_iuran
	) tabel_iuran
	group by tabel_iuran.real_tanggal_iuran
) new_tabel
			where {$param}
			");
		return $query->result();
	}

	public function get_list_harian_query($param) {
		// $order="order by ref_siswa";
		// $query=$this->db->query("SELECT a.*, CONCAT(b.`tingkat_kelas`,' ',b.`jurusan_kelas`,' ',b.`sub_tingkat_kelas`) kelas FROM {$this->table} a left join kp_kelas b on a.kelas_siswa=b.id_kelas	 where {$param} {$order}");

		$query=$this->db->query("select * from(
			select a.no_iuran, b.ref_siswa, b.nama_lengkap_siswa, x.kelas,
			c.iuran_bulanan_bulan bulan_iuran_bulanan, IFNULL(c.total_besar_iuran_bulanan,0) total_besar_iuran_bulanan, 
			d.bulan_iuran_tabungan, IFNULL(d.total_besar_iuran_tabungan,0) total_besar_iuran_tabungan, 
			IFNULL(e.total_besar_iuran_dpmp,0) total_besar_iuran_dpmp, IFNULL(f.total_besar_iuran_du,0) total_besar_iuran_du,
			IFNULL(c.total_besar_iuran_bulanan, 0)+IFNULL(d.total_besar_iuran_tabungan, 0)+IFNULL(e.total_besar_iuran_dpmp, 0)+IFNULL(f.total_besar_iuran_du, 0) as total_bulanan_tabungan_dpmp_du,
			SUBSTRING_INDEX(tanggal_iuran,' ',1) real_tanggal_iuran, a.keterangan_iuran
			from kp_iuran a
			left join kp_siswa b ON b.id_siswa = a.id_siswa_iuran
			left join (
				select id_siswa_kelas, CONCAT(b.`tingkat_kelas`,' ',b.`jurusan_kelas`,' ',b.`sub_tingkat_kelas`) kelas from kp_siswa_kelas a 
				left join kp_kelas b on b.id_kelas = a.kelas_siswa_kelas
			) x on x.id_siswa_kelas = a.id_siswa_kelas_iuran
			left join(
					select 
					a.id_iuran, a.no_iuran, a.nama_lengkap_siswa, 
					GROUP_CONCAT(a.bulan_iuran_number order by a.bulan_iuran_number asc) as iuran_bulanan_bulan, 
					SUM(a.besar_iuran) as total_besar_iuran_bulanan
					from (
						select a.id_iuran, a.no_iuran,
						a.bulan_iuran, a.keterangan_iuran,
						CASE
							WHEN bulan_iuran LIKE '%Jul%' THEN '7'
							WHEN bulan_iuran LIKE '%Aug%' THEN '8'
							WHEN bulan_iuran LIKE '%Sep%' THEN '9'
							WHEN bulan_iuran LIKE '%Oct%' THEN '10'
							WHEN bulan_iuran LIKE '%Nov%' THEN '11'
							WHEN bulan_iuran LIKE '%Dec%' THEN '12'
							WHEN bulan_iuran LIKE '%Jan%' THEN '1'
							WHEN bulan_iuran LIKE '%Feb%' THEN '2'
							WHEN bulan_iuran LIKE '%Mar%' THEN '3'
							WHEN bulan_iuran LIKE '%Apr%' THEN '4'
							WHEN bulan_iuran LIKE '%May%' THEN '5'
							WHEN bulan_iuran LIKE '%Jun%' THEN '6'
							ELSE ''
						END AS bulan_iuran_number,
						a.jenis_iuran, SUBSTRING_INDEX(a.tanggal_iuran,' ',1) real_tanggal_iuran, a.besar_iuran, b.nama_lengkap_siswa from kp_iuran a
						left join kp_siswa b on b.id_siswa = a.id_siswa_iuran	
					) a
					where {$param}
					and jenis_iuran='IURAN_PER_BULAN' or jenis_iuran='IURAN_PER_BULAN_MANUAL'
					group by a.no_iuran
					order by a.nama_lengkap_siswa
			) c on c.no_iuran = a.no_iuran
			left join(
					select 
					a.id_iuran, a.no_iuran, a.nama_lengkap_siswa, 
					GROUP_CONCAT(a.bulan_iuran_number order by a.bulan_iuran_number asc) as bulan_iuran_tabungan, 
					SUM(a.besar_iuran) as total_besar_iuran_tabungan
					from (
						select a.id_iuran, a.no_iuran,
						a.bulan_iuran, a.keterangan_iuran,
						CASE
							WHEN bulan_iuran LIKE '%Jul%' THEN '7'
							WHEN bulan_iuran LIKE '%Aug%' THEN '8'
							WHEN bulan_iuran LIKE '%Sep%' THEN '9'
							WHEN bulan_iuran LIKE '%Oct%' THEN '10'
							WHEN bulan_iuran LIKE '%Nov%' THEN '11'
							WHEN bulan_iuran LIKE '%Dec%' THEN '12'
							WHEN bulan_iuran LIKE '%Jan%' THEN '1'
							WHEN bulan_iuran LIKE '%Feb%' THEN '2'
							WHEN bulan_iuran LIKE '%Mar%' THEN '3'
							WHEN bulan_iuran LIKE '%Apr%' THEN '4'
							WHEN bulan_iuran LIKE '%May%' THEN '5'
							WHEN bulan_iuran LIKE '%Jun%' THEN '6'
							ELSE ''
						END AS bulan_iuran_number,
						a.jenis_iuran, SUBSTRING_INDEX(a.tanggal_iuran,' ',1) real_tanggal_iuran, a.besar_iuran, b.nama_lengkap_siswa from kp_iuran a
						left join kp_siswa b on b.id_siswa = a.id_siswa_iuran	
					) a
					where {$param}
					and jenis_iuran='TABUNGAN' or jenis_iuran='TABUNGAN_MANUAL'
					group by a.no_iuran
					order by a.nama_lengkap_siswa
			) d on d.no_iuran = a.no_iuran
			left join(
					select 
					a.id_iuran, a.no_iuran, a.nama_lengkap_siswa, 
					SUM(a.besar_iuran) as total_besar_iuran_dpmp
					from (
						select a.id_iuran, a.no_iuran,
						a.bulan_iuran, a.keterangan_iuran,
						a.jenis_iuran, SUBSTRING_INDEX(a.tanggal_iuran,' ',1) real_tanggal_iuran, a.besar_iuran, 
						b.nama_lengkap_siswa 
						from kp_iuran a
						left join kp_siswa b on b.id_siswa = a.id_siswa_iuran
					) a
					where {$param}
					and jenis_iuran='DPMP'
					group by a.no_iuran
					order by a.nama_lengkap_siswa
			) e on e.no_iuran = a.no_iuran
			left join(
					select 
					a.id_iuran, a.no_iuran, a.nama_lengkap_siswa, 
					SUM(a.besar_iuran) as total_besar_iuran_du
					from (
						select a.id_iuran, a.no_iuran,
						a.bulan_iuran, a.keterangan_iuran,
						a.jenis_iuran, SUBSTRING_INDEX(a.tanggal_iuran,' ',1) real_tanggal_iuran, a.besar_iuran, 
						b.nama_lengkap_siswa 
						from kp_iuran a
						left join kp_siswa b on b.id_siswa = a.id_siswa_iuran
					) a
					where {$param}
					and jenis_iuran='DU' or jenis_iuran='DU_MANUAL'
					group by a.no_iuran
					order by a.nama_lengkap_siswa
			) f on f.no_iuran = a.no_iuran
			) new_tabel
			where {$param} 
			group by no_iuran
			order by kelas");
			// and keterangan_iuran is null
		return $query;
	}

	public function get_list_harian($param) {
		$query=$this->get_list_harian_query($param);
		return $query->result();
	}

// 	public function table_query($param) {
// 		// print_r($param);
// 		// $order="order by ref_siswa";
// 		// $param="and tanggal_iuran='".$param['tanggal_iuran']."'";
// 		$query=$this->db->query("select id_siswa, ref_siswa, nama_lengkap_siswa, jenis_kelamin_siswa, wali_kelas, CONCAT(c.tingkat_kelas,' ',c.jurusan_kelas,' ',c.sub_tingkat_kelas) kelas, tahun_ajaran_siswa, tahun_ajaran, id_siswa_kelas, '[]' as list_bulan, '[]' as list_dpmp,
// 								count_bulan total_bulan, ifnull(SPP,0) spp, ifnull(DPMP,0) dpmp, ifnull(DU,0) du, f.besar_iuran_siswa_detil besar_spp, g.besar_iuran_siswa_detil besar_dpmp from kp_siswa_kelas a
// left join kp_siswa b on b.id_siswa = a.header_id_siswa_kelas
// left join kp_kelas c on c.id_kelas = a.kelas_siswa_kelas
// 								left join(
// 									select id_siswa_iuran, id_siswa_kelas_iuran, SUM(besar_iuran) SPP, count(bulan_iuran) count_bulan, tanggal_iuran from kp_iuran
// 									where {$param} AND jenis_iuran='IURAN_PER_BULAN' and status_iuran='LUNAS' 
// 									group by id_siswa_kelas_iuran
// 								) b on b.id_siswa_kelas_iuran = a.id_siswa_kelas
// 								left join(
// 									select id_siswa_iuran, id_siswa_kelas_iuran, SUM(besar_iuran) DPMP, tanggal_iuran from kp_iuran
// 									where {$param} AND jenis_iuran='DPMP' and status_iuran='LUNAS' 
// 									group by id_siswa_kelas_iuran
// 								) c on c.id_siswa_kelas_iuran = a.id_siswa_kelas
// 								left join(
// 									select id_siswa_iuran, id_siswa_kelas_iuran, SUM(besar_iuran) DU, tanggal_iuran from kp_iuran
// 									where {$param} AND jenis_iuran='DU' and status_iuran='LUNAS' 
// 									group by id_siswa_kelas_iuran
// 								) d on d.id_siswa_kelas_iuran = a.id_siswa_kelas
								
// 								left join (
// 									select * from kp_siswa_detil where jenis_iuran_siswa_detil='IURAN_PER_BULAN'
// 								) f on f.header_id_siswa_detil = a.header_id_siswa_kelas and c.tingkat_kelas = f.tingkat_siswa_detil
// 								left join (
// 									select * from kp_siswa_detil where jenis_iuran_siswa_detil='DPMP'
// 								) g on g.header_id_siswa_detil = a.header_id_siswa_kelas
								
// 								group by id_siswa_kelas");
// 		return $query;
// 	}

	//pake ini jika diatas pusing
	public function table_query($param) {
		$query=$this->db->query("select id_siswa, ref_siswa, nama_lengkap_siswa, jenis_kelamin_siswa, jenis_kelamin_siswa, wali_kelas, CONCAT(c.tingkat_kelas,' ',c.jurusan_kelas,' ',c.sub_tingkat_kelas) kelas, c.tingkat_kelas, keterangan_masuk_siswa, tanggal_registrasi_siswa_kelas, tahun_ajaran_siswa_kelas, id_siswa_kelas, '[]' as list_bulan, '[]' as list_tabungan, '[]' as new_list_bulan, '[]' as new_list_tabungan, '[]' as list_dpmp, '[]' as list_du, '[]' as besar_iuran,
								count_bulan total_bulan, ifnull(SPP,0) spp, ifnull(DPMP,0) dpmp, ifnull(DU,0) du, f.besar_iuran_siswa_detil besar_spp, g.besar_iuran_siswa_detil besar_dpmp, h.besar_iuran_siswa_detil besar_du, b.real_tanggal_iuran tanggal_bulanan, c.real_tanggal_iuran tanggal_dpmp, d.real_tanggal_iuran tanggal_du
								 from kp_siswa_kelas a
left join kp_siswa b on b.id_siswa = a.header_id_siswa_kelas
left join kp_kelas c on c.id_kelas = a.kelas_siswa_kelas
								left join(
									select id_siswa_iuran, id_siswa_kelas_iuran, SUM(besar_iuran) SPP, count(bulan_iuran) count_bulan, tanggal_iuran, real_tanggal_iuran from (
										select *, SUBSTRING_INDEX(tanggal_iuran,' ',1) real_tanggal_iuran from kp_iuran 
									) a
									left join kp_siswa_kelas b on b.id_siswa_kelas = a.id_siswa_kelas_iuran
									left join kp_kelas c on c.id_kelas = b.kelas_siswa_kelas
									where {$param} AND jenis_iuran='IURAN_PER_BULAN'
									group by id_siswa_kelas_iuran
								) b on b.id_siswa_kelas_iuran = a.id_siswa_kelas
								left join(
									select id_siswa_iuran, id_siswa_kelas_iuran, SUM(besar_iuran) DPMP, tanggal_iuran, real_tanggal_iuran from (
											select *, SUBSTRING_INDEX(tanggal_iuran,' ',1) real_tanggal_iuran from kp_iuran
										) a
										left join kp_siswa_kelas b on b.id_siswa_kelas = a.id_siswa_kelas_iuran
										left join kp_kelas c on c.id_kelas = b.kelas_siswa_kelas
									where {$param} AND jenis_iuran='DPMP' and status_iuran='LUNAS' 
									group by id_siswa_kelas_iuran
								) c on c.id_siswa_kelas_iuran = a.id_siswa_kelas
								left join(
									select id_siswa_iuran, id_siswa_kelas_iuran, SUM(besar_iuran) DU, tanggal_iuran, real_tanggal_iuran from (select *, SUBSTRING_INDEX(tanggal_iuran,' ',1) real_tanggal_iuran from kp_iuran) a
										left join kp_siswa_kelas b on b.id_siswa_kelas = a.id_siswa_kelas_iuran
										left join kp_kelas c on c.id_kelas = b.kelas_siswa_kelas
									where {$param} AND jenis_iuran='DU' and status_iuran='LUNAS' 
									group by id_siswa_kelas_iuran
								) d on d.id_siswa_kelas_iuran = a.id_siswa_kelas
								
								left join (
									select * from kp_siswa_detil where jenis_iuran_siswa_detil='IURAN_PER_BULAN'
								) f on f.header_id_siswa_detil = a.header_id_siswa_kelas and c.tingkat_kelas = f.tingkat_siswa_detil
								left join (
									select * from kp_siswa_detil where jenis_iuran_siswa_detil='DPMP'
								) g on g.header_id_siswa_detil = a.header_id_siswa_kelas and 'ALL' = g.tingkat_siswa_detil
								left join (
									select * from kp_siswa_detil where jenis_iuran_siswa_detil='DU'
								) h on h.header_id_siswa_detil = a.header_id_siswa_kelas and c.tingkat_kelas = h.tingkat_siswa_detil
								where {$param}
								group by id_siswa_kelas
								order by ref_siswa");
		return $query;
	}



	// back ini
	/*public function table_query($param) {
		$query=$this->db->query("select id_siswa, ref_siswa, nama_lengkap_siswa, jenis_kelamin_siswa, jenis_kelamin_siswa, wali_kelas, CONCAT(c.tingkat_kelas,' ',c.jurusan_kelas,' ',c.sub_tingkat_kelas) kelas, keterangan_masuk_siswa, tanggal_registrasi_siswa_kelas, tahun_ajaran_siswa_kelas, id_siswa_kelas, '[]' as list_bulan, '[]' as new_list_bulan, '[]' as list_dpmp, '[]' as list_du, '[]' as besar_iuran,
								count_bulan total_bulan, ifnull(SPP,0) spp, ifnull(DPMP,0) dpmp, ifnull(DU,0) du, f.besar_iuran_siswa_detil besar_spp, g.besar_iuran_siswa_detil besar_dpmp, h.besar_iuran_siswa_detil besar_du, b.real_tanggal_iuran tanggal_bulanan, c.real_tanggal_iuran tanggal_dpmp, d.real_tanggal_iuran tanggal_du
								 from kp_siswa_kelas a
left join kp_siswa b on b.id_siswa = a.header_id_siswa_kelas
left join kp_kelas c on c.id_kelas = a.kelas_siswa_kelas
								left join(
									select id_siswa_iuran, id_siswa_kelas_iuran, SUM(besar_iuran) SPP, count(bulan_iuran) count_bulan, tanggal_iuran, real_tanggal_iuran from (
										select *, SUBSTRING_INDEX(tanggal_iuran,' ',1) real_tanggal_iuran from kp_iuran 
									) a
									left join kp_siswa_kelas b on b.id_siswa_kelas = a.id_siswa_kelas_iuran
									left join kp_kelas c on c.id_kelas = b.kelas_siswa_kelas
									where {$param} AND jenis_iuran='IURAN_PER_BULAN' and status_iuran='LUNAS' 
									group by id_siswa_kelas_iuran
								) b on b.id_siswa_kelas_iuran = a.id_siswa_kelas
								left join(
									select id_siswa_iuran, id_siswa_kelas_iuran, SUM(besar_iuran) DPMP, tanggal_iuran, real_tanggal_iuran from (
											select *, SUBSTRING_INDEX(tanggal_iuran,' ',1) real_tanggal_iuran from kp_iuran
										) a
										left join kp_siswa_kelas b on b.id_siswa_kelas = a.id_siswa_kelas_iuran
										left join kp_kelas c on c.id_kelas = b.kelas_siswa_kelas
									where {$param} AND jenis_iuran='DPMP' and status_iuran='LUNAS' 
									group by id_siswa_kelas_iuran
								) c on c.id_siswa_kelas_iuran = a.id_siswa_kelas
								left join(
									select id_siswa_iuran, id_siswa_kelas_iuran, SUM(besar_iuran) DU, tanggal_iuran, real_tanggal_iuran from (select *, SUBSTRING_INDEX(tanggal_iuran,' ',1) real_tanggal_iuran from kp_iuran) a
										left join kp_siswa_kelas b on b.id_siswa_kelas = a.id_siswa_kelas_iuran
										left join kp_kelas c on c.id_kelas = b.kelas_siswa_kelas
									where {$param} AND jenis_iuran='DU' and status_iuran='LUNAS' 
									group by id_siswa_kelas_iuran
								) d on d.id_siswa_kelas_iuran = a.id_siswa_kelas
								
								left join (
									select * from kp_siswa_detil where jenis_iuran_siswa_detil='IURAN_PER_BULAN'
								) f on f.header_id_siswa_detil = a.header_id_siswa_kelas and c.tingkat_kelas = f.tingkat_siswa_detil
								left join (
									select * from kp_siswa_detil where jenis_iuran_siswa_detil='DPMP'
								) g on g.header_id_siswa_detil = a.header_id_siswa_kelas and c.tingkat_kelas = g.tingkat_siswa_detil
								left join (
									select * from kp_siswa_detil where jenis_iuran_siswa_detil='DU'
								) h on h.header_id_siswa_detil = a.header_id_siswa_kelas and c.tingkat_kelas = h.tingkat_siswa_detil
								
								group by id_siswa_kelas
								order by ref_siswa");
		return $query;
	}*/

	public function get_by_id($id) {
		$param="id_siswa='".$id."'";
		$query=$this->table_query($param);
		return $query->row();
	}

	function count_filtered() {
		$this->get_list_table_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}