<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Model {
	var $table = 'tbl_user';
	var $table_id = 'id_user';

	public function __construct() {
		parent::__construct();
	}

    //login
	public function get_user($login) {
       	$this->db->SELECT('*');    //select *
		$this->db->FROM('tbl_user'); //from table
		// $this->db->JOIN('tbl_karyawan b', 'a.karyawan = b.karyawan_id','inner'); //inner join table
		// $this->db->JOIN('tbl_jabatan c', 'b.jabatan = c.jabatan_id','inner'); //inner join table
		$this->db->where($login);
		$query = $this->db->get();
		return $query->result();
    }

	public function count_user($login) {
        $this->db->from($this->table);
		$this->db->where('username_user', $login['username_user']);
		$this->db->where('password_user', $login['password_user']);
        return $this->db->count_all_results();
    }

    public function code($code='code', $message='message', $data=[]){
    	$resp = array(
        				"code" => $code,
        				"message" => $message,
                        "data" => $data,
                );
    	return $resp;
    }
}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */ 