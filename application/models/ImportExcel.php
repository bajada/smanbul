<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class ImportExcel extends CI_Model {
 
    public function importData($data) {
 
        $res = $this->db->insert_batch('kp_siswa',$data);
        if($res){
            return TRUE;
        }else{
            return FALSE;
        }
 
    }
 	
 	// Fungsi untuk melakukan proses upload file
	public function upload_file($filename){
		$this->load->library('upload'); // Load librari upload

		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']  = '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;

		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if($this->upload->do_upload('uploadFile')){ // Lakukan upload dan Cek jika proses upload berhasil
		  // Jika berhasil :
		  $return = array('result' => 'success', 'uploadFile' => $this->upload->data(), 'error' => '');
		  return $return;
		}else{
		  // Jika gagal :
		  $return = array('result' => 'failed', 'uploadFile' => '', 'error' => $this->upload->display_errors());
		  return $return;
		}
	}

	// Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	public function insert_multiple($data){
		$this->db->insert_batch('kp_siswa', $data);
	}
}
 
?>