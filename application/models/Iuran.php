<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Iuran extends CI_Model {
	var $table = 'kp_iuran';
	var $table_kwitansi = 'kp_kwitansi_iuran';
	var $table_id = 'id_iuran';
	var $order = array('id_iuran' => 'desc');


	public function __construct() {
		parent::__construct();
	}

	public function newId() {
        $query = $this->db->query("SELECT max($this->table_id) newid from {$this->table}");
        $query = $query->row();
		$query = $query->newid + 1;
        return $query;
	}

 	public function save($data) {
		$this->db->trans_start();
		$this->db->insert($this->table, $data);
		$this->db->trans_complete();
		return $this->db->insert_id();
	} 

 	public function save_detil($data) {
 		// print_r($data);
		$this->db->trans_start();
		$this->db->insert_batch($this->table, $data);
		//CREATE kwitansi
		if(isset($_POST["created_kwitansi"])){
			$this->db->insert($this->table_kwitansi, array(
				// 'id_kwitansi_iuran' => 0,
				'tanggal_kwitansi_iuran' => date('Y-m-d H:i:s'),
				'id_siswa_kelas_kwitansi_iuran' => $data[0]['id_siswa_kelas_iuran'],
				'kode_kwitansi_iuran' => "INV-".date('YmdHis'),
			));
		}
		//end CREATE
		$this->db->trans_complete();
	} 

 	public function update_detil($data) {
		$this->db->trans_start();
		$this->db->update_batch($this->table, $data, 'id_iuran');
		$this->db->trans_complete();
	} 

 	public function update_detil_by($data, $column) {
		$this->db->trans_start();
		$this->db->update_batch($this->table, $data, $column);
		$this->db->trans_complete();
	} 
	
 	public function update($data) {
		$this->db->trans_start();
		$this->db->where($this->table_id, $data[$this->table_id]);
		$this->db->update($this->table, $data);
		$this->db->trans_complete();
		return $this->db->affected_rows();
	}

	public function delete($id) {
		$this->db->where($this->table_id, $id);
		$this->db->delete($this->table);
	}

	//
	// public function get_list_table_query($param) {
	// 	$this->db->SELECT('*');    //select *
	// 	$this->db->FROM($this->table); //from table
	// 	// $this->db->or_like($param);
	// 	$this->db->WHERE($param);
	// }

	// public function get_list_table($param) {
	// 	$this->get_list_table_query($param);
	// 	$query = $this->db->get();
	// 	return $query->result();
	// }
	//

	public function get_list_table_query($param) {
		//$param->clause
		// echo $param;
		// die();

		// print_r($param->clause);
		// die();
		$clause="";
		if($param->clause=="") $param->clause=1;
		if($param->clause!="1") $clause="WHERE ".$param->clause;
		$order="";
		if($param->order!="") $order="ORDER BY ".$param->order;
		// foreach ($param as $key => $value) {
		// 	print_r( $value."<br/>");
		// }

		// $param=explode(" ", $param);
		// print_r($order);
		// $clause=$param['clause'];
		// print_r($params);
		$query=$this->db->query("SELECT a.*, b.*, c.*, '[]' as real_status FROM (
			select *, SUBSTRING_INDEX(tanggal_iuran,' ',1) real_tanggal_iuran from {$this->table}) a
			left join kp_siswa_kelas b on b.id_siswa_kelas = a.id_siswa_kelas_iuran
			left join kp_kelas c on c.id_kelas = b.kelas_siswa_kelas
			${clause} ${order}");//${order}
		// $query=$this->db->query("select a.*, CONCAT(d.`tingkat_kelas`,' ',d.`jurusan_kelas`,' ',d.`sub_tingkat_kelas`) from kp_iuran a
		// left join kp_siswa b on b.id_siswa = a.id_siswa_iuran
		// left join kp_siswa_kelas c on c.header_id_siswa_kelas = b.id_siswa and c.status_siswa_kelas=1
		// left join kp_kelas d on d.id_kelas = c.kelas_siswa_kelas where {$param} {$order}");
		return $query;
	}

	public function get_list_table($param) {
		$query=$this->get_list_table_query($param);
		return $query->result();
	}

	//

	function count_filtered() {
		$this->get_list_table_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

	public function get_by_id($id) {
		$this->db->from($this->table);
		$this->db->where($this->table_id,$id);
		$query = $this->db->get();

		return $query->row();
	}
}