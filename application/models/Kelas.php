<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Model {
	var $table = 'kp_kelas';
	var $table_id = 'id_kelas';
	var $order = array('id_kelas' => 'desc');


	public function __construct() {
		parent::__construct();
	}

	public function newId() {
        $query = $this->db->query("SELECT max($this->table_id) newid from {$this->table}");
        $query = $query->row();
		$query = $query->newid + 1;
        return $query;
	}

 	public function save($data) {
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	} 
	
 	public function update($data) {
		$this->db->where($this->table_id, $data[$this->table_id]);
		$this->db->update($this->table, $data);
		return $this->db->affected_rows();
	}

	public function delete($id) {
		$this->db->where($this->table_id, $id);
		$this->db->delete($this->table);
	}

	public function get_list_table_query($param) {
		$this->db->SELECT('*');    //select *
		$this->db->FROM($this->table); //from table
		$this->db->or_like($param);
	}

	public function get_list_table($param) {
		$this->get_list_table_query($param);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered() {
		$this->get_list_table_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

	public function get_by_id($id) {
		$this->db->from($this->table);
		$this->db->where($this->table_id,$id);
		$query = $this->db->get();

		return $query->row();
	}
	
	//NEW 
	public function table_query2($param) {
		$order="order by tingkat_kelas";
		// $query=$this->db->query("select a.*, CONCAT(a.`tingkat_kelas`,' ',a.`jurusan_kelas`,' ',a.`sub_tingkat_kelas`) kelas from {$this->table} a where {$param->getClause()} {$order}");
		$query=$this->db->query("select tbl.* from(
	select a.*, CONCAT(a.`tingkat_kelas`,' ',a.`jurusan_kelas`,' ',a.`sub_tingkat_kelas`) as kelas from {$this->table} a
) tbl
where {$param->getClause()} {$order}");
		return $query;
	}
	public function get_list_table2($param) {
		$query=$this->table_query2($param);
		return $query->result();
	}

	public function getQryKelasByJurusan($param){
		//$order="order by tingkat_kelas";
		$query=$this->db->query("
			select tingkat_kelas, jurusan_kelas, count(*) jumlah_kelas from kp_kelas
			where {$param->getClause()}
			group by jurusan_kelas
		");
		return $query;
	}
	public function getListKelasByJurusan($param) {
		$query=$this->getQryKelasByJurusan($param);
		return $query->result();
	}

}