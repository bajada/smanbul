<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Model {
	var $table = 'kp_siswa';
	var $table_id = 'id_siswa';
	var $order = array('ref_siswa' => 'desc');


	public function __construct() {
		parent::__construct();
	}

	public function newId() {
        $query = $this->db->query("SELECT max($this->table_id) newid from {$this->table}");
        $query = $query->row();
		$query = $query->newid + 1;
        return $query;
	}

	public function newRef() {
        $query = $this->db->query("SELECT max(ref_siswa) newref from {$this->table}");
        $query = $query->row();
		$query = $query->newref + 1;
		// $query = $query->newref + 1;
        return $query;
	}

	public function longNumberId() {
        $range = (999 - 100) + 1;
        $return = (rand() * $range) + 100;
        return time() ."". $return;
	}

 	public function save($data) {
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	} 
 	public function save_detil($data) {
 		$this->db->trans_start();
		$this->db->insert_batch('kp_siswa_detil', $data);
 		$this->db->trans_complete();
	}
 	public function save_kelas($data) {
 		$this->db->trans_start();
		$this->db->insert_batch('kp_siswa_kelas', $data);
 		$this->db->trans_complete();
	}
 	public function save_master($data) {
 		$this->db->trans_start();
		$this->db->insert_batch('kp_siswa', $data);
 		$this->db->trans_complete();
	} 
	public function save_dm($data_siswa_detil,$data_siswa_kelas, $data_siswa){
		$this->db->trans_start();
			$this->db->insert_batch('kp_siswa_detil', $data_siswa_detil);
			$this->db->insert_batch('kp_siswa', $data_siswa);
			$this->db->insert_batch('kp_siswa_kelas', $data_siswa_kelas);
 		$this->db->trans_complete();
	}
	public function save_siswa_kelas_iuran($data){
		// print_r($data['detil']);
		// die();
			if($data['siswa']){
				$this->db->trans_start();
				$this->db->insert_batch('kp_siswa', $data['siswa']);
				$this->db->insert_batch('kp_siswa_kelas', $data['kelas']);
				$this->db->insert_batch('kp_siswa_detil', $data['detil']);
				if(count($data['iuran']) > 0) $this->db->insert_batch('kp_iuran', $data['iuran']);
 				$this->db->trans_complete();
			}else{
				$this->db->trans_start();
				$this->db->insert_batch('kp_siswa_kelas', $data['kelas']);
				$this->db->insert_batch('kp_siswa_detil', $data['detil']);
				if(count($data['iuran']) > 0) $this->db->insert_batch('kp_iuran', $data['iuran']);
				$this->db->trans_complete();
			}
	}
	public function delete_all($data, $permanent=false){
		// print_r($data);
		// die();
		$this->db->trans_start();
			if($permanent){
				//this just for development
				$this->db->query("DELETE FROM kp_iuran where id_siswa_iuran='{$data[$this->table_id]}'");
				$this->db->query("DELETE FROM kp_siswa where id_siswa='{$data[$this->table_id]}'");
				$this->db->query("DELETE FROM kp_siswa_kelas where header_id_siswa_kelas='{$data[$this->table_id]}'");
				$this->db->query("DELETE FROM kp_siswa_detil where header_id_siswa_detil='{$data[$this->table_id]}'");
			}else{
				//$this->db->query("UPDATE kp_siswa SET user_deleted_siswa='-1' where id_siswa={$id}");
				$this->db->where($this->table_id, $data[$this->table_id]);
				$this->db->update($this->table, $data);
			}
 		$this->db->trans_complete();
	}
	
 	public function update($data) {
 		$this->db->trans_start();
			$this->db->where($this->table_id, $data[$this->table_id]);
			$this->db->update($this->table, $data);
 		$this->db->trans_complete();
		return $this->db->affected_rows();
	}
	
 	public function update_all($data) {
 		// print_r($data);
 		$this->db->trans_start();
 			$this->db->update_batch('kp_siswa', $data, 'id_siswa');
 		$this->db->trans_complete();
	}

	public function delete($id) {
		$this->db->where($this->table_id, $id);
		$this->db->delete($this->table);
	}

	public function get_list_table_query($param) {
		// print_r($param->getOrder());
		$order="";
		if($param->order!=""){
			$order="order by ref_siswa";
		}
		// $param="1";
		$query=$this->db->query("SELECT * FROM {$this->table} where {$param->getClause()} {$order}");
		return $query;
	}

	public function get_list_table($param="1") {
		$query=$this->get_list_table_query($param);
		return $query->result();

	}public function get_list_table2($param="1") {
		$query=$this->table_query2($param);
		return $query->result();
	}

	function count_filtered() {
		$this->get_list_table_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
	public function table_query($param) {
		$order="order by ref_siswa";
		// $query=$this->db->query("SELECT a.*, CONCAT(b.`tingkat_kelas`,' ',b.`jurusan_kelas`,' ',b.`sub_tingkat_kelas`) kelas FROM {$this->table} a left join kp_kelas b on a.kelas_siswa=b.id_kelas	 where {$param} {$order}");
		$query=$this->db->query("SELECT * FROM {$this->table} where {$param} {$order}");
		return $query;
	}
	public function table_query2($param) {
		$order="order by ref_siswa";
		// $query=$this->db->query("SELECT * FROM {$this->table} a left join kp_siswa_kelas b on b.header_id_siswa_kelas=a.id_siswa left join kp_kelas c on c.id_kelas = b.kelas_siswa_kelas where {$param} {$order}");
		
		$query=$this->db->query("SELECT a.*, b.*, c.*, '[]' as list_iuran FROM {$this->table} a left join kp_siswa_kelas b on b.header_id_siswa_kelas=a.id_siswa left join kp_kelas c on c.id_kelas = b.kelas_siswa_kelas where {$param} {$order}");
		return $query;
	}
	public function get_by_id($id) {
		// $this->db->from($this->table);
		// $this->db->where($this->table_id,$id);
		$param="id_siswa='".$id."'";
		$query=$this->table_query($param);
		// $query=$query->db->get();

		return $query->row();
	}
}