<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utils {

        
	public function rand_float($st_num=0,$end_num=1,$mul=1000000){
		if ($st_num>$end_num) return false;
		return mt_rand($st_num*$mul,$end_num*$mul)/$mul;
	}

	public function getAlphaNumericString($length_string){
		$AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" . "0123456789";
		$sb = "";
		// $string .= "appended string";
		// $stringBuilder = new StringBuilder();
		// $stringBuilder->append($length_string);
		for ($i=0; $i < $length_string; $i++) { 
			// print_r(strlen($AlphaNumericString));
			$index = intval(strlen($AlphaNumericString) * $this->rand_float());
			// print_r($this->rand_float()."<br/>");
			$sb.=$AlphaNumericString[$index];
		}
		return $sb;
	}

	public function longNumberId() {
        $range = (999 - 100) + 1;
        $return = intval($this->rand_float() * $range) + 100;
        return time() ."". $return;
	}

	public function longNumberIdNew($id) {
        $range = (9999 - 1000) * $id;
        $return = intval($this->rand_float() * $range) + 100;
        return time() ."". $return.$id;
	}

	public function guidv4(){
		if(function_exists('com_create_guid') === true)
			return trim(com_create_guid(), '{}');

		$data = openssl_random_pseudo_bytes(16);
		$data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set versi to 0100
		$data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bit 6-7 to 10
		return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
	}
}