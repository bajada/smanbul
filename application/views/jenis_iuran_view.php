<?php $this->load->view('inc/header');?>

    <div class="container-fluid mt-3">

          <!-- <h1 class="card-title text-white mb-4 font-main">LOGIN</h1> -->
          <div class="pt-2">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb" style="background-color:transparent;">
                <li class="breadcrumb-item"><a href="dashboard_view" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Kembali</a></li>

                <li class="breadcrumb-item" aria-current="page">
                      <a href="" class="text-white-href" style="display: inline-flex;">
                      <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-money-check fa-stack-1x fa-inverse text-muted"></i>
                      </span>
                      <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Pengaturan Iuran</span>
                      </a>
                </li>
              </ol>
            </nav>

          </div>

    <div class="row justify-content-center">
      <div class="col-md-10">
        <div class="card bg-head pill">
          <!-- <h5 class="card-header bg-dark text-white">Data</h5> -->
          <div class="card-header bg-dark text-white" style="background-color: #487d95!important;font-family: segoeui;">
          <div class="row">
            <div class="col-md-8">
            <h5>Pengaturan Jenis Iuran</h5>
            </div>
            <div class="col-md-2 offset-2 text-right">
              <div class="btn-group" role="group">
                  <button id="btnGroupDrop1" type="button" class="btn btn-secondary pl-4 pr-4 pill-right pill-left" onclick="add();">
                    Tambah
                  </button>
                </div>
            </div>
          </div>
          </div>

          <div class="card-body">
            <!--<div class="card-title text-white" style="font-family: segoeuib">
              <div class="row">
                <div class="col-md-10"><h5>Pengaturan Jenis Iuran</h5></div>
                <div class="col-md-2 text-right pull-right float-right">
                  <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-secondary pl-4 pr-4 pill-right pill-left" onclick="add();">
                      Tambah
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <hr/>-->
           <!--  <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a> -->

            <form id="search-form">
            <div class="form-group row">
              <div class="col-sm-3 ">
                  <div class="input-group ">
                    <!-- <input name="filter_keyword" type="text" class="form-control " placeholder="Ketik Parameter Pencarian"> -->
                    <select name="filter_jenis_iuran" class="form-control ml-1" onchange="display()">
                        <option value="0">Semua Jenis Iuran</option>
                        <option value="DPMP">DPMP</option>
                        <option value="DU">DU</option>
                        <option value="IURAN_PER_BULAN">IURAN_PER_BULAN</option>
                        <option value="TABUNGAN">TABUNGAN</option>
                        <!-- <option value="ALUMNI">ALUMNI</option> -->
                    </select>
                  </div>
              </div>
              <div class="col-sm-2 offset-7">
                  <div class="input-group ">
                    <select name="filter_kelas" class="form-control ml-1" onchange="display()">
                        <option value="0">Semua Kelas</option>
                        <option value="X">X</option>
                        <option value="XI">XI</option>
                        <option value="XII">XII</option>
                        <!-- <option value="ALUMNI">ALUMNI</option> -->
                    </select>
                  </div>
              </div>
            </div>  
            </form>

            <div class="row">
              <div class="col-sm-12">
                <table id="tbl-data" class="table table-sm table-striped table-hover" >
                  <thead class="thead-lights text-white" style="background-color: #487d95!important;font-family: segoeui;">
                    <tr>
                      <!-- <th scope="col">#</th>
                      <th scope="col">Nama Alternatif</th> -->
                        <th width="10px">No</th>
                        <th width="10px">Kelas</th>
                        <th>Jenis Iuran</th>
                        <th>Besar Iuran</th>
                        <!-- <th>Cicilan Berapa Kali</th> -->
                        <th style="width:125px;"></th>
                    </tr>
                  </thead>
                  <tbody class="bg-light">

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog pill text-white" role="document">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="font-family: segoeui;">Form Jenis Iuran</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div id="modal-form-msg" class="alert alert-danger " role="alert">
                <strong>Holy guacamole!</strong> You should check in on some of those fields below.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="form">  

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>Jenis Iuran</label>
                    <input name="nama" type="text" class="form-control" id="" placeholder="Ketik nama jenis iuran" autocomplete="off">
                </div>
            </div> 

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Kelas</label>
                    <select name="tingkat" class="form-control select-style" >
                        <option value="0">Pilih Kelas</option>
                        <option value="ALL">ALL</option>
                        <option value="X">X</option>
                        <option value="XI">XI</option>
                        <option value="XII">XII</option>
                        <!-- <option value="ALUMNI">ALUMNI</option> -->
                    </select>
                </div>
                
              <div class="form-group col-md-6">
                <label for="">Besar IURAN</label>
                <input name="besar" type="number" class="form-control" id=""  placeholder="Ketik jumlah tanpa tanda titik atau comma">
              </div>
                <div class="form-group col-md-6 d-none">
                    <label>Cicilan</label>
                    <select name="cicilan" class="form-control select-style">
                        <option value="0">Pilih Cicilan</option>
                        <option value="1">1x</option>
                        <option value="2">2x</option>
                        <option value="3">3x</option>
                        <option value="4">4x</option>
                        <option value="5">5x</option>
                        <option value="6">6x</option>
                        <option value="7">7x</option>
                        <option value="8">8x</option>
                        <option value="9">9x</option>
                        <option value="10">10x</option>
                        <option value="11">11x</option>
                        <option value="12">12x</option>
                    </select>
                </div>
            </div> 

           <!--  <div class="form-row">
              <div class="form-group col-md-6">
                <label for="">Besar IURAN</label>
                <input name="besar" type="number" class="form-control" id=""  placeholder="Ketik jumlah tanpa tanda titik atau comma">
              </div>
            </div>    -->


            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light bg-head pill pl-4 pr-4" data-dismiss="modal">CLOSE</button>
            <button id="btn-submit" type="button" class="btn btn-light btn-secondary pill pl-4 pr-4 text-white" onclick="save()">SUBMIT</button>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script src="<?php echo base_url('assets/js/jenis-iuran-script.js?yess')?>"></script> 
</body>

</html>