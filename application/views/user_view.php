<?php $this->load->view('inc/header');?>

    <div class="container-fluid mt-3" id="main-cont">

          <!-- <h1 class="card-title text-white mb-4 font-main">LOGIN</h1> -->
          <div class="pt-2">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb" style="background-color:transparent;">
               <li class="breadcrumb-item"><a href="dashboard_view" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Kembali</a></li>

                <li class="breadcrumb-item" aria-current="page">
                      <a href="" class="text-white-href" style="display: inline-flex;">
                      <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-user fa-stack-1x fa-inverse text-muted"></i>
                      </span>
                      <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Pengguna</span>
                      </a>
                </li>
              </ol>
            </nav>

          </div>

    <div class="row justify-content-center">
      <div class="col-md-10">
        <div class="card bg-head pill" ><!--style="border-top-color: #0062cc;border-top-width: 2px;"-->
            <div class="card-header bg-dark text-white" style="background-color: #487d95!important;font-family: segoeui;">
            <div class="row">
              <div class="col-md-8">
              <h5>Data User/Pengguna</h5>
              </div>
              <div class="col-md-2 offset-2 text-right">
                <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-secondary pl-4 pr-4 pill-right pill-left" onclick="add();">
                      Tambah
                    </button>
                  </div>
              </div>
            </div>
            </div>

          <!-- </div> -->
          
          <div class="card-body">
            <!-- <h5 class="card-title" style="font-family: segoeuib">Data User</h5>
            <hr/> -->
           <!--  <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a> -->

            <form id="search-form">
            <div class="form-group row">
              <div class="col-sm-4">
                  <div class="input-group ">
                    <input name="filter_keyword" autocomplete="off" type="text" class="form-control" placeholder="Ketik Parameter Pencarian">
                    <!-- <div class="input-group-append">
                      <button class="btn btn-secondary pill-right pr-4" type="submit">Cari</button>
                    </div> -->
                  </div>
              </div>

              <div class="col-sm-2 offset-6">
                  <div class="input-group ">
                    <select name="filter_role" class="form-control ml-1" onchange="display()">
                        <option value="0">Semua Role</option>
                        <option value="ADMIN">ADMIN</option>
                        <option value="TU">TU</option>
                        <option value="STAFF_TU">STAFF_TU</option>
                        <option value="KEPALA_SEKOLAH">KEPALA_SEKOLAH</option>
                    </select>
                  </div>
              </div>

              <!-- <div class="col-sm-6 text-right">
                <div class="btn-group" role="group">
                  <button id="btnGroupDrop1" type="button" class="btn btn-secondary pl-5 pr-5 pill-right pill-left btn-lg" onclick="add();">
                    Tambah
                  </button>
                </div>
              </div> -->
            </div>  
            </form>

            <div class="row">
              <div class="col-sm-12">
                <table id="tbl-data" class="table table-sm table-striped table-hover" >
                  <thead class="thead-lights text-white" style="background-color: #487d95!important;font-family: segoeui;">
                    <tr>
                      <!-- <th scope="col">#</th>
                      <th scope="col">Nama Alternatif</th> -->
                        <th width="10px">No</th>
                        <th>Username</th>
                        <th>Nama Depan</th>
                        <th>Nama Belakang</th>
                        <th>Role</th>
                        <!-- <th>Status</th> -->
                        <th style="width:125px;"></th>
                    </tr>
                  </thead>
                  <tbody class="bg-light">
                    
                    <!-- <php 
                      foreach ($alternatif as $key => $value) {
                        echo "<tr>";
                          echo "<td>{$key}</td>";
                          echo "<td>{$value}</td>";
                        echo "</tr>";
                      }
                    ?> -->

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog pill" role="document">
        <div class="modal-content pill bg-gradient text-white">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form">
            <div id="modal-form-msg" class="alert alert-danger " role="alert">
                <strong>Holy guacamole!</strong> You should check in on some of those fields below.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="">Nama Depan</label>
                <input name="firstname_user" type="text" class="form-control" placeholder="Nama Depan">
              </div>
              <div class="form-group col-md-6">
                <label for="">Nama Belakang</label>
                <input name="lastname_user" type="text" class="form-control" placeholder="Nama Belakang">
              </div>
            </div>    
            <!-- <div class="subtitle border-bottom">Informasi Akun</div> -->
            <!-- <hr/> -->
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="">Username</label>
                <input name="username_user" type="text" class="form-control" placeholder="Username">
              </div>
                <div id="form-role" class="form-group col-md-6">
                    <label>Role</label>
                    <select name="role_user" class="form-control select-style">
                        <option value="">Pilih Role</option>
                        <option value="ADMIN">ADMIN</option>
                        <option value="TU">TU</option>
                        <option value="STAFF_TU">STAFF_TU</option>
                        <option value="KEPALA_SEKOLAH">KEPALA_SEKOLAH</option>
                    </select>
                </div>
            </div>
            <div id="form-pass" class="form-row d-none">
              <div class="form-group col-md-6">
                <label for="">Password Baru</label>
                <input name="password_user" type="password" class="form-control" placeholder="Password">
              </div>
                <div class="form-group col-md-6">
                    <label for="">Ketik Ulang Password Baru</label>
                    <input name="password_baru_user" type="password" class="form-control" placeholder="Ketik Ulang Password Baru">
                </div>
            </div>
              <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary pill pl-4 pr-4" data-dismiss="modal">CLOSE</button>
            <button type="button" class="btn bg-head pill pl-4 pr-4" onclick="save();">SUBMIT</button>
          </div>
        </div>
      </div>
    </div>


   <!--  <div class="modal fade" id="modal-form_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="">NIS</label>
                  <div class="input-group ">
                    <input name="nis_siswa" type="text" class="form-control" placeholder="NIS Siswa">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="submit">Cari</button>
                    </div>
                  </div>
              </div>
              <div class="form-group col-md-6">
                <label for="">Nama Lengkap</label>
                <input name="nama_lengkap" type="text" class="form-control" id="" placeholder="Nama Lengkap" readonly="">
              </div>
            </div>    
            <div class="subtitle text-info">Informasi Akun</div>
            <hr/>

            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="">Username</label>
                <input name="username_user" type="text" class="form-control" id="" placeholder="Username">
              </div>
                <div id="form-role" class="form-group col-md-6">
                    <label>Hak Akses</label>
                    <select name="hak_akses_user" class="form-control">
                        <option value="0">Pilih Hak Akses</option>
                    </select>
                </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="">Password Baru</label>
                <input name="password_user" type="password" class="form-control" id="" placeholder="Password">
              </div>
                <div class="form-group col-md-6">
                    <label for="">Ketik Ulang Password Baru</label>
                    <input name="password_baru_user" type="password" class="form-control" id="" placeholder="Ketik Ulang Password Baru">
                </div>
            </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" onclick="save();">Save changes</button>
          </div>
        </div>
      </div>
    </div> -->

    <?php $this->load->view('inc/footer');?>
    <script src="<?php echo base_url('assets/js/user-js.js?yes')?>"></script> 
</body>

</html>