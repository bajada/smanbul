<?php $this->load->view('inc/header');?>
<style type="text/css">
  .nav-item .nav-link {
    border-color: white;
    color: white;
  }
  .nav-item .nav-link .active {
    /*border-color: red;*/
  }
</style>

  <div class="pt-2">

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color:transparent;">
        <!-- <li class="breadcrumb-item"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Kembali</button></li> -->

        <li class="breadcrumb-item" aria-current="page">
              <a href="siswa_personal_view" class="text-white-href" style="display: inline-flex;">
                <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fas fa-users fa-stack-1x fa-inverse text-muted"></i>
                </span>
                <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Personal Siswa</span>
              </a>
              <a href="siswa_view" class="text-white-href" style="display: inline-flex;">
                <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fas fa-chalkboard-teacher fa-stack-1x fa-inverse text-muted"></i>
                </span>
                <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Kelas Siswa</span>
              </a>
        </li>
       <!--  <li class="breadcrumb-item" aria-current="page">
              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-suitcase fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span>
              </a>
        </li> -->
      </ol>
    </nav>

  </div>



    <div class="row justify-content-center">
      <div class="col-md-11">

  <div class="card bg-head pill ">
    <div class="card-header text-white" style="background-color: #487d95!important;font-family: segoeui;">
    <div class="row">
      <div class="col-md-7"> <h5 style="font-family: segoeui;">Data Siswa</h5></div>
      <div class="col-md-3 offset-2">
        
        <div class="btn-group float-right mr-2">
          <button onclick="$('#modal-form').modal('show');" type="button" class="btn btn-light pill-left pl-4">Tambah</button>
          <button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split pill-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="javascript:void(0)" onclick="$('#modal-form-import').modal('show');" >Import Data Siswa(.xlsx)</a>
          </div>
        </div>

        <button id="btn-naik-kelas" type="button" class="btn btn-light pill float-right mr-2" onclick="goToNaikKelas()" disabled="true">Naik Kelas</button>
      </div>
      <!-- <div class="col-md-6">
        <a href="<?php echo base_url('page/form_siswa_view') ?>" type="" class="btn btn-light btn-lg btn-light pill pl-5 pr-5 float-right">Tambah</a>
      </div> -->
    </div>
    </div>

    <div class="card-body">
    <!-- <h3 class="card-title">
    <div class="row">
      <div class="col-md-6"> <h5 style="font-family: segoeui;">Data Siswa</h5></div>
      <div class="col-md-6"><button onclick="$('#modal-form').modal('show');" type="button" class="btn btn-light btn-light pill pl-4 pr-4 float-right">Tambah</button> <button onclick="goToEdit()" type="button" class="btn btn-light btn-light pill pl-4 pr-4 float-right mr-1">Action</button></div>
     
    </div>

    </h3> -->
      
    <!-- <hr/> -->

    <form id="search-form">
      <div class="form-group row">
        <div class="col-md-4">
          <input onkeyup="display();" name="filter_keyword" type="text" class="form-control" autocomplete="off" placeholder="Ketik Nama/No.Ref/Peminatan Jurusan">
        </div>
        <div class="col-md-2 offset-4">
            <select name="filter_kelas" class="form-control d-none" onchange="display()">
                <option value="X">X</option>
                <option value="XI">XI</option>
                <option value="XII">XII</option>
                <!-- <option value="ALUMNI">ALUMNI</option> -->
            </select> 
        </div>
        <div class="col-md-2">
            <select name="filter_tahun_ajaran" onchange="filter();" id="tahun_ajaran" class="form-control">
              <option>Semua Tahun Ajaran</option>
            </select>
        </div>
      </div>
    </form>

    <div class="row">
      <!-- <div class="col-3">
        <div class="row">
          <div class="col-md-11 pill-top p-3" style="background-color: #487d95!important;font-family: segoeui;opacity: 0.8;">
            <span class="col-form-label">Kelas <select onchange="filter();" id="tahun_ajaran" class="float-right"><option>Pilih Tahun Ajaran</option></select></span>
          </div>
        </div>

        <div class="row">
          <div class="col-md-11 pill-top p-0" style="font-family: segoeui;opacity: 0.8;">
          <table id="tbl-data-kelas" class="table-responsive card-list-table table-pills" style="font-family: segoeui;opacity: 0.8;display: table;">
            <tbody class="text-dark">
              <tr data-tingkat="ALL" class="" onclick="filter('tingkat_kelas=ALL&');"><td>SEMUA KELAS</td></tr>
              <tr data-tingkat="X" class="" onclick="filter('tingkat_kelas=X&');"><td>X</td></tr>
              <tr data-tingkat="XI" class="" onclick="filter('tingkat_kelas=XI&');"><td>XI</td></tr>
              <tr data-tingkat="XII" class="" onclick="filter('tingkat_kelas=XII&');"><td>XII</td></tr>
              <tr data-tingkat="ALUMNI" class="" onclick="filter('tingkat_kelas=ALUMNI&');"><td>ALUMNI</td></tr>
            </tbody>
          </table>
        </div>
        </div>
      </div> -->
      
      <div class="col-12">
        <div class="tab-content" id="v-pills-tabContent">
          <div class="tab-pane fade show active" id="v-pills-kelas-none">
                <form id="form-data" method="POST">
                <input type="text" name="list_obj" class="d-none">
                <input type="text" name="tingkat_kelas" class="d-none">

                <div class="row">
                  <div class="col-md-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a onclick="$('[name=filter_kelas]').val('X').trigger('change');" class="nav-link active" id="x-tab" data-toggle="tab" href="#x" role="tab">X</a>
                      </li>
                      <li class="nav-item">
                        <a onclick="$('[name=filter_kelas]').val('XI').trigger('change');" class="nav-link" id="xi-tab" data-toggle="tab" href="#xi" role="tab">XI</a>
                      </li>
                      <li class="nav-item">
                        <a onclick="$('[name=filter_kelas]').val('XII').trigger('change');" class="nav-link" id="xii-tab" data-toggle="tab" href="#xii" role="tab">XII</a>
                      </li>
                    </ul>

                    <div class="tab-content pt-3" id="myTabContent">
                      <div class="tab-pane fade show active" id="bulanan">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 pill-top container-scrollbar" style="height: 500px;overflow-x: scroll;">


                  <table id="tbl-data" class="table table-striped table-responsive table-sm" style="font-family: segoeui;opacity: 0.8;display: table;">
                    <thead class="text-white" style="background-color: #487d95!important;">
                      <tr>
                        <th align="center" width="4%"><div class="text-center form-check"><input id="check_all" class="form-check-input" style="position:unset" type="checkbox"></div></th>
                        <!-- <th width="8%">No</th> -->
                        <th width="5%">No.REF</th>
                        <th width="20%">Nama Lengkap</th>
                        <th width="8%">Kelas</th>
                        <th width="5%">Jenis Kelamin</th>
                        <th width="8%">Tahun Ajaran</th>
                        <th width="5%">Angkatan</th>
                        <th width="10%">Registrasi</th>
                        <th width="10%"></th>
                      </tr>
                    </thead>
                    <tbody class="text-dark"></tbody>
                  </table>

                  </div>
                </div>
                <div class="row no-gutters px-1 py-3 align-items-center">
                    <div class="col pl-3">
                        <div class="dataTables_info" id="info-tabel">1 to 8 Items of 11 — <a href="#!" class="font-weight-semi-bold"> view all <span class="fa fa-angle-right"></span></a>
                        </div>
                    </div>
                    <div class="col-auto pr-3 d-none">
                        <div class="dataTables_paginate paging_simple" id="DataTables_Table_0_paginate">
                            <ul class="pagination pagination-sm">
                                <li class="paginate_button page-item previous disabled" id="DataTables_Table_0_previous"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>
                                <li class="paginate_button page-item next" id="DataTables_Table_0_next"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0" class="page-link">Next</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </form>
          </div>
        </div>
      </div>
      
    </div>

    </div>
  </div>
</div>
</div>
    </div>

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md pill text-white" role="document" styles="max-width: 700px">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="font-family: segoeuib">Tambah Siswa Baru</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form-siswa" method="POST">
              <div class="form-row justify-content-center">
                <div class="form-group col-md-6">
                  <label for="">Jumlah Siswa</label>
                  <input name="jumlah_siswa" type="text" class="form-control" autocomplete="off"  placeholder="Masukkan Jumlah Data">
                </div>
                
               <div class="form-group col-md-6">
                  <label>Kelas</label>
                  <!-- <select name="kelas" class="form-control select-styles">
                      <option value="0">Pilih kelas</option>
                  </select> -->
                  <select name="kelas" class="form-control">
                      <option value="X">X</option>
                      <option value="XI">XI</option>
                      <option value="XII">XII</option>
                      <!-- <option value="ALUMNI">ALUMNI</option> -->
                  </select> 
                </div> 
              </div>  

              <button id="btn-submit-2" type="submit" class="btn btn-secondary pill pl-4 pr-4 float-right d-none">SUBMIT</button>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light bg-head pill pl-4 pr-4" data-dismiss="modal">CLOSE</button>
            <button onclick="$('#btn-submit-2').click()" type="button" class="btn btn-secondary btn-secondary pill pl-4 pr-4">SUBMIT</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap modal import -->
    <div class="modal fade" id="modal-form-import" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog pill text-white" role="document">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="font-family: segoeuib">Import Data Siswa</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form-import" action="<?php echo base_url('siswaController/uploadData') ?>" method="post" enctype="multipart/form-data">
              
              <div class="form-group">
                <label for="" class="">Kelas</label>
                <select name="kelasFile" class="form-control" onchange="createForm(this);">
                    <option value="X">X</option>
                    <option value="XI">XI</option>
                    <option value="XII">XII</option>
                    <!-- <option value="ALUMNI">ALUMNI</option> -->
                </select> 
              </div>

              <div id="form-upload-file" class="form-group">
                <label for="">Upload file(.xlsx)</label>
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile" name="uploadFile" onchange="$('#custom-file-name').text(this.files[0].name)">
                    <label id="custom-file-name" class="custom-file-label" for="customFile">Choose file</label>
                  </div>
                <small class="form-text"><a id="unduh_temp" class="text-white" href="<?php echo base_url("excel/template-excel-10.xlsx"); ?>">Klik disini, untuk mendownload template.</a></small>
              </div>
              <input class="d-none" type="submit" name="preview" value="Preview" />
            </form>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light bg-head pill pl-4 pr-4" data-dismiss="modal">CLOSE</button>
            <button onclick="submit_form();" type="button" class="btn btn-secondary btn-secondary pill pl-4 pr-4">SUBMIT</button>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script src="<?php echo base_url('assets/js/siswa-script.js?yes')?>"></script> 

  </body>
</html>