<?php $this->load->view('inc/header');?>

  <div class="pt-2">

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color:transparent;">
        <li class="breadcrumb-item"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Kembalilah</button></li>

        <li class="breadcrumb-item" aria-current="page">

            <!-- <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-hand-holding-usd fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span></a> -->

              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-users fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Siswa</span>
              </a>
        </li>
       <!--  <li class="breadcrumb-item" aria-current="page">
              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-suitcase fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span>
              </a>
        </li> -->
      </ol>
    </nav>

  </div>



    <div class="row justify-content-center">
      <div class="col-md-11">

  <div class="card bg-head pill ">
    <div class="card-header text-white" style="background-color: #487d95!important;font-family: segoeui;">
    <div class="row">
      <div class="col-md-7"> <h5 style="font-family: segoeui;">Data Siswa</h5></div>
      <div class="col-md-3 offset-2">
        <button onclick="$('#modal-form').modal('show');" type="button" class="btn btn-light btn-light pill pl-4 pr-4 float-right">Tambah</button> 
        <!-- <button type="button" class="btn  btn-secondary pill pl-4 pr-4 float-right mr-1">Aksi</button> -->
        <!-- Example split danger button -->
        <div class="btn-group float-right mr-2">
          <button type="button" class="btn btn-light pill-left pl-4">Aksi</button>
          <button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split pill-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="#">Edit</a>
            <a class="dropdown-item" href="#">Hapus</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="javascript:void(0)" onclick="goToEdit()" >Naik Kelas</a>
          </div>
        </div>
      </div>
      <!-- <div class="col-md-6">
        <a href="<?php echo base_url('page/form_siswa_view') ?>" type="" class="btn btn-light btn-lg btn-light pill pl-5 pr-5 float-right">Tambah</a>
      </div> -->
    </div>
    </div>

    <div class="card-body">
    <div class="row">
      
      <div class="col-12">
        <div class="tab-content" id="v-pills-tabContent">
          <div class="tab-pane fade show active" id="v-pills-kelas-none">

                <div class="row">
                  <div class="col-md-12 pill-top ">

                  
                  <form id="form-siswa">
                    <div class="form-row justify-content-center">
                      <div class="form-group col-md-12">
                        <label for="">Judul</label>
                        <input name="judul" type="text" class="form-control" autocomplete="off">
                      </div>
                    </div>

                    <div class="form-row justify-content-center">
                      <div class="form-group col-md-12">
                        <label for="">Uraian</label>
                        <input name="uraian" type="text" class="form-control" autocomplete="off">
                      </div>
                    </div> 

                    <button id="btn-submit" type="submit" class="btn btn-secondary pill pl-4 pr-4 float-right">SUBMIT</button>

                  </form>

                  <hr/>
                

                  </div>
                </div>
          </div>
        </div>
      </div>
    </div>

    </div>
  </div>
</div>
</div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script type="text/javascript">
      function init() {
        tinymce.init({
          selector: '[name=uraian]',
          plugins: 'image code',
          toolbar: 'undo redo | link image | code',
          /* enable title field in the Image dialog*/
          image_title: true,
          /* enable automatic uploads of images represented by blob or data URIs*/
          automatic_uploads: true,
          /*
            URL of our upload handler (for more details check: https://www.tiny.cloud/docs/configure/file-image-upload/#images_upload_url)*/
            images_upload_url: ctx + 'UploadController/postAcceptor',
            /*here we add custom filepicker only to Image dialog
          */
          file_picker_types: 'image',
          /* and here's our custom image picker*/
          file_picker_callback: function (cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');

            /*
              Note: In modern browsers input[type="file"] is functional without
              even adding it to the DOM, but that might not be the case in some older
              or quirky browsers like IE, so you might want to add it to the DOM
              just in case, and visually hide it. And do not forget do remove it
              once you do not need it anymore.
            */

            input.onchange = function () {
              var file = this.files[0];

              var reader = new FileReader();
              reader.onload = function () {
                /*
                  Note: Now we need to register the blob in TinyMCEs image blob
                  registry. In the next release this part hopefully won't be
                  necessary, as we are looking to handle it internally.
                */
                var id = 'blobid' + (new Date()).getTime();
                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                var base64 = reader.result.split(',')[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);

                /* call the callback and populate the Title field with the file name */
                cb(blobInfo.blobUri(), { title: file.name });
              };
              reader.readAsDataURL(file);
            };

            input.click();
          }
        });
      }
    </script>

  </body>
</html>