<?php $this->load->view('inc/header');?>

    <div class="container-fluid mt-3">
<!-- 
      <div class="media bg-head pill pl-5 pt-3  pb-3">
        <img src="http://sman1-cibungbulang.sch.id/assets/img/smanbul.png" style="width: 100px;" class="ml-3 mr-3" alt="GAMBAR HERE">
        <div class="media-body mt-auto mb-auto">
          <h2 class="font-main">Sistem Informasi Pembayaran</h2>
          <h3 class="font-main">SMAN 1 CIBUNGBULANG</h3>
        </div>
        <div class="pr-5 pt-3  pb-3">
          <a href="" class="mr-4 text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-home fa-4x" aria-hidden="true"></i> Home</a>
          <a href="" class="text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-globe fa-4x" aria-hidden="true"></i> Website</a>
        </div>
      </div> -->

      <div class="row d-none">
        <div class="col-md-12">
          <div class="jumbotron">
            <h1 class="">Sistem Informasi Pembayaran</h1>
            <p class="lead">SMAN 1 CIBUNGBULANG</p>
            <hr class="my-4">
            <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
            <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
          </div>
        </div>
      </div>
      <div class="row justify-content-center d-flex align-items-center p-5">
        <div class="col-md-7">
          <h1 class="card-title text-center text-white mb-4 font-main">LOGIN</h1>
          <div class="card bg-login pill p-5">
            <!-- <h5 class="card-header bg-primary text-white">Login SPK SMART</h5> -->
            <div class="card-body">
             <!--  <h5 class="card-title">LOGIN</h5>
              <hr/>
 -->
              <form action="<?php echo base_url('PageController/user_login') ?>" method="POST">
                <?php if(isset($status)) { ?>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-12 col-form-label text-danger" style="font-family: segoeuib">Username atau Password yang anda masukan salah!</label>
                </div>
                <?php } ?>

                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-3 col-form-label">Username</label>
                  <div class="col-sm-8">
                    <input value="" type="text" class="form-control" name="username" placeholder="Masukkan Username" autocomplete="off">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword" class="col-sm-3 col-form-label">Password</label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" name="password" placeholder="Masukkan Password" autocomplete="off">
                  </div>
                </div>

                <button id="btn-submit" type="submit" class="btn btn-primary btn-block d-none">Login</button>
              </form>

            </div>
          </div>

          <div class="mt-4 text-right">
                <button type="button" class="btn btn-lg bg-head pill pl-5 pr-5">Batal</button>
                <button id="btn-login" onclick="$('#btn-submit').click()" type="button" class="btn btn-lg btn-secondary pill pl-5 pr-5">Login</button>
                  <!-- <button onclick="$('#btn-submit').click()" type="button" class="btn btn-lg btn-secondary rounded-pill "><span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Login</button> -->
          </div>  
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


    <script src="<?php echo base_url('assets/js/login-js.js?kfkwfwk')?>"></script> 

  </body>
</html>
