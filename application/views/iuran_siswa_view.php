<?php $this->load->view('inc/header');?>

    <div class="container-fluid mt-3">

      <div class="media bg-head pill pl-5 pt-3  pb-3">
        <img src="http://sman1-cibungbulang.sch.id/assets/img/smanbul.png" style="width: 100px;" class="ml-3 mr-3" alt="GAMBAR HERE">
        <div class="media-body mt-auto mb-auto">
          <h2 class="font-main">Sistem Informasi Pembayaran</h2>
          <h3 class="font-main">SMAN 1 CIBUNGBULANG</h3>
        </div>
        <div class="pr-5 pt-3  pb-3">
          <a href="<?php echo base_url('page/dashboard_view') ?>" class="mr-4 text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-home fa-4x" aria-hidden="true"></i> Home</a>
          <a href="<?php echo base_url('page/login_view') ?>" class="text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-globe fa-4x" aria-hidden="true"></i> Website</a>
        </div>
      </div>

          <!-- <h1 class="card-title text-white mb-4 font-main">LOGIN</h1> -->
          <div class="pt-2">

            <nav aria-label="breadcrumb">
              <ol class="breadcrumb" style="background-color:transparent;">
                <li class="breadcrumb-item"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">IURAN</button></li>

                <li class="breadcrumb-item" aria-current="page">

                    <!-- <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-hand-holding-usd fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span></a> -->

                      <a href="" class="text-white-href" style="display: inline-flex;">
                      <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-dollar fa-stack-1x fa-inverse text-muted"></i>
                      </span>
                      <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran SPP</span>
                      </a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                   <!--  <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-suitcase fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> DPMP Siswa</span></a>
                    -->
                      <a href="" class="text-white-href" style="display: inline-flex;">
                      <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-suitcase fa-stack-1x fa-inverse text-muted"></i>
                      </span>
                      <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran DPMP</span>
                      </a>
                </li>
              </ol>
            </nav>

          </div>

          <div class="card bg-login pill p-5 text-white ">
            <h3 class="card-header d-none">Entri Iuran Siswa</h3>
            <div class="card-body">
            <h3 class="card-title">
            <span>Data Siswa</span>
            <button onclick="$('#modal-form').modal('show');" type="button" class="btn btn-light btn-lg btn-secondary pill pl-5 pr-5 float-right">Tambah</button>
            </h3>
              
            <hr/>

            <div class="row">
              <div class="col-sm-12">
                <table id="tbl-data" class="table table-bordereds table-hover">
                  <thead class="thead-light">
                    <tr>
                        <th width="10px">No</th>
                        <th>Ref</th>
                        <th>Nama Siswa</th>
                        <th>Kelas</th>
                        <th>Jenis Kelamin</th>
                        <th style="width:125px;"></th>
                    </tr>
                  </thead>
                  <tbody>

                  <tr class="data-row" id="row-1"><td>1</td><td class="">151105150975</td><td class="">Abdul</td><td class="">X-SURTA</td><td class="">L</td><td class=""><a href="javascript:void(0)" class="btn btn-sm btn-square btn-outline-info" type="button" onclick="doAction('1','edit')"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="btn btn-sm btn-square btn-outline-danger" type="button" onclick="doAction('1','delete')"><i class="fa fa-trash-o"></i></a></td></tr><tr class="data-row" id="row-2"><td>2</td><td class="">151105150974</td><td class="">Hajat</td><td class="">XI-RPL</td><td class="">L</td><td class=""><a href="javascript:void(0)" class="btn btn-sm btn-square btn-outline-info" type="button" onclick="doAction('2','edit')"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="btn btn-sm btn-square btn-outline-danger" type="button" onclick="doAction('2','delete')"><i class="fa fa-trash-o"></i></a></td></tr><tr class="data-row" id="row-3"><td>3</td><td class="">151105150990</td><td class="">Muhammad</td><td class="">XII</td><td class="">L</td><td class=""><a href="javascript:void(0)" class="btn btn-sm btn-square btn-outline-info" type="button" onclick="doAction('3','edit')"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="btn btn-sm btn-square btn-outline-danger" type="button" onclick="doAction('3','delete')"><i class="fa fa-trash-o"></i></a></td></tr><tr class="data-row" id="row-4"><td>4</td><td class="">1711051510203</td><td class="">XI-RPL</td><td class="">Rehan</td><td class="">L</td><td class=""><a href="javascript:void(0)" class="btn btn-sm btn-square btn-outline-info" type="button" onclick="doAction('4','edit')"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" class="btn btn-sm btn-square btn-outline-danger" type="button" onclick="doAction('4','delete')"><i class="fa fa-trash-o"></i></a></td></tr></tbody>
                </table>
              </div>
            </div>

            </div>
          </div>
    </div>

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg pill text-white" role="document">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Siswa</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form">

            <div class="form-group row">                  
              <div class="col-sm-6">
              <label for="" class="col-form-label">Ref/Nama Siswa</label>
                <select name="siswa" class="form-control select-auto-complete" id="" onchange="$('#modal-form').modal('show');">
                  <option>Ketik Ref/Nama Siswa</option>
                  <option>1001-Ahmad Maulana</option>
                  <option>1102-Rehan Sehan</option>
                  <option>1123-Mohamamd</option>
                  <option>1343-Abudrahman</option>
                </select>
              </div>              
              <div class="col-sm-6"> 
              <label for="" class="col-form-label">SPP</label>
                <input name="spp" type="number" class="form-control" id="" placeholder="Tentukan Jumlah Iuran SPP">
              </div>
            </div> 

            <div class="form-group row">
              <div class="form-group col-md-6">
                <label for="">DPMP</label>
                <input name="dpmp" type="number" class="form-control" id="" placeholder="Tentukan Jumlah Iuran DPMP">
              </div>
              <div class="form-group col-md-6">
                <label for="">DU</label>
                <input name="du" type="number" class="form-control" id="" placeholder="Tentukan Jumlah Iuran DU">
              </div>
            </div>    

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5" data-dismiss="modal">CLOSE</button>
            <button onclick="$('#btn-submit').click()" type="button" class="btn btn-light btn-lg btn-secondary pill pl-5 pr-5">SUBMIT</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script src="<?php echo base_url('assets/selectric/jquery.selectric.js')?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script type="text/javascript">
      $(document).ready(function() {

        $('.select-auto-complete').select2({
          placeholder: "Tentukan Pilihan",
          width:"100%"
        });


      });

      $(function() {
        $('.select-style').selectric();
      });
    </script>
    <script src="<?php echo base_url('assets/js/login-js.js')?>"></script> 


  </body>
</html>