<?php $this->load->view('inc/header');?>
<style type="text/css">
  .table-condensed td {
    padding-left: 0!important;
    padding-right: 0!important;
  }
  .table-condensed th {
    padding-left: 0!important;
    padding-right: 0!important;
  }
</style>
  <div class="pt-2">

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color:transparent;">
        <li class="breadcrumb-item"><a href="dashboard_view" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Kembali</a></li>

        <li class="breadcrumb-item" aria-current="page">

            <!-- <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-hand-holding-usd fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span></a> -->

              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-edit fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Harian</span>
              </a>
        </li>
        <li class="breadcrumb-item" aria-current="page">
           <!--  <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-suitcase fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> DPMP Siswa</span></a>
            -->
              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-clipboard fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Mingguan</span>
              </a>
        </li>
      </ol>
    </nav>

  </div>

  <div class="row justify-content-center">
  <div class="col-md-6">
    <form id="search-form">
      <div class="form-group">
      <div class="input-group input-group-lg">
      <div class="input-group-prepend">
        <span class="input-group-text pill-left">Tanggal Laporan</span>
      </div>

      <input onchange="trychange()" class="form-control" type="text" id="reportrangestart" name="filter_start_date" placeholder="Dari tanggal" autocomplete="off">
      
      <div class="input-group-append">
        <span class="input-group-text">s/d</span>
      </div>

      <input class="form-control" type="text" id="reportrangeend" name="filter_end_date" placeholder="Sampai tanggal" readonly="" autocomplete="off">
      <div class="input-group-append">
        <button class="btn btn-secondary pill-right" type="submit" id="btn-cari">Cari/Filter</button>
      </div>
      </div>
      </div> 
    </form>
  </div>
  </div>

  <div class="row justify-content-center">
      <div class="col-sm-12">
        <div class="card bg-head pill">
          <div class="card-body">
            <div class="card-title">
              
              <div class="row">
                <div class="col-md-auto align-self-center">
                  <h5 style="font-family: segoeui;">Data Laporan Mingguan</h5>
                </div>
                <div class="col-md-auto">
                  <ul id="wrap-sheet" class="nav nav-pills ml-auto">

                  </ul>
                </div>
              </div>

            </h3>
            <hr/>
            <div class="tab-content" id="pills-tabContent">

            </div>

          </div>
        </div>
      </div>
    </div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script src="<?php echo base_url('assets/js/laporan-global-script.js?yess')?>"></script>

  </body>
</html>