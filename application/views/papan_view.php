<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Hello, world!</title>

    <style type="text/css">
      /*.grad {
        background-image: linear-gradient(to bottom right, rgba(78,123,105,1), rgba(133,154,52,1));
        background-repeat: no-repeat;
         background-size: cover;
         height: 100%;
      }*/


      html, body {
        background-image: linear-gradient(to bottom right, rgb(2, 124, 161), rgb(149, 169, 18));
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        height: 100%;
        font-family: segoeuili;
        background-attachment: fixed;
      }

      .bg-login {
        background-color: rgba(170, 192, 171, 1)
      }

      .bg-head {
        background-color: rgba(170, 192, 171, 0.5);
        color:white;
      }

      .bg-head-2 {
        background-color: rgba(170, 192, 171, 0.34);
        color:white;
      }

      .form-control {
          height: 46px;
          border: none;
          background: #f7f7f7;
          width: 100%;
          padding: 0 25px;
          border-radius: 23px;
          color: #333;
      }

      input, textarea, select, button {
          /*font-family: poppins-regular;*/
          font-size: 13px;
          border-radius: 23px;
      }
      label {
        font-size: 20px!important;
      }

      .pill {
        border-radius: 23px;
      }

      .font-main {
        font-weight: 300!important;
      }
      .text-white-href {
          color:white!important;
      }
      .text-white-href:hover {
          color: -webkit-link;
          cursor: pointer;
          text-decoration: underline;
          color: #f8f9fa/*rgba(0,123,255,.25)*/!important;
          text-decoration: none;
      }

      .brightness-filter {
        filter: brightness(0.25);
      }

      .btn-light {
        border:0px;
      }
      .btn-secondary{
        background: #49766d;
      }

      .text-muted{
        color:#768749!important;
      }

      @font-face {
        font-family: segoeui;
        src: url("<?php echo base_url('assets/my/font/segoeui.ttf')?>");
      }

      @font-face {
        font-family: segoeuili;
        src: url("<?php echo base_url('assets/my/font/segoeuil.ttf')?>");
      }

      .btn-light.disabled, .btn-light:disabled {
        background-color: rgba(170, 192, 171, 0.5);
        color: white;
        opacity: unset;
      }
    </style>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="<?php echo base_url('assets/my/font-awesome-custom/css/font-more-awesome.css')?>" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  </head>
  <body class="bg-light">
    <!-- <php include('inc/navbar.php'); ?> -->

    <div class="p-5 bg-head-2">
      <div class="media">
        <img src="http://sman1-cibungbulang.sch.id/assets/img/smanbul.png" style="width: 140px;" class="ml-3 mr-3" alt="GAMBAR HERE">
        <div class="media-body  text-center">
          <h2 class="font-main">Sistem Informasi Pembayaran</h2>
          <h3 class="font-main display-4">SMAN 1 CIBUNGBULANG</h3>
          <h5 class="font-main" style="font-size: 1.15rem;font-style: bold;font-weight: bold!important;">Jl. Kapten Dasuki Bakri No.18, Cibatok, Cibungbulang, Bogor, Jawa Barat 16630</h5>
        </div>
        <div class="pr-5 pt-3  pb-3">
          <a href="" class="mr-4 text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-home fa-4x" aria-hidden="true"></i> Home</a>
          <a href="<?php echo base_url('PageController/user_logout') ?>" class="text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-power-off fa-4x" aria-hidden="true"></i> Log Out</a>
        </div>
      </div>
    </div>

    <div class="container-fluid mt-4 mb-5">
      

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script type="text/javascript">
      // var matriks=[];
      // var matriks = {
      //     'matriks_1' : {
      //       'baris_1' : [4, 2, 3],
      //       'baris_2' : [5, 7, 6],
      //     },
      //     'matriks_2' : {
      //       'baris_1' : [1, 8, 9],
      //       'baris_2' : [3, 5, 4],
      //     }
      // };
      // console.log(matriks);
      //order 2x3
      var baris=2;
      var kolom=3;
      $( document ).ready(function(){
        //working
        console.log("penjumlahan");
        var matriks = [{
            'matriks_1' : {
              'baris_1' : [1, 5, 2],
              'baris_2' : [-1, 0, 1],
              'baris_3' : [3, 2, 4],
            },
            'matriks_2' : {
              'baris_1' : [6, 1, 3],
              'baris_2' : [-1, 1, 2],
              'baris_3' : [4, 1, 3],
            }
        }];
        // console.log(hitung_penjumlahan(matriks, 'baris_1'));
        // console.log(hitung_penjumlahan(matriks, 'baris_2'));
        // console.log(hitung_penjumlahan(matriks, 'baris_3'));

        // console.log("pengurangan");
        // var matriks = [{
        //     'matriks_1' : {
        //       'baris_1' : [4, 6, 5, 7],
        //       'baris_2' : [3, 1, 9, 4],
        //     },
        //     'matriks_2' : {
        //       'baris_1' : [2, 8, 3, -1],
        //       'baris_2' : [5, 2, -4, 6],
        //     }
        // }];
        // console.log(hitung_pengurangan(matriks, 'baris_1'));
        // console.log(hitung_pengurangan(matriks, 'baris_2'));
        //working

        console.log("Invers dari :");
        //hitung determinan
        // var matriks = [{
        //   'matriks' : {
        //     'baris_1' : [1, 2, 3],
        //     'baris_2' : [4, 1, 5],
        //     'baris_3' : [6, 0, 2],
        //   }
        // }];
        var matriks = [{
          'matriks' : {
            'baris_1' : [1, 5, 2],
            'baris_2' : [-1, 0, 1],
            'baris_3' : [3, 2, 4],
          }
        }];
        console.log(matriks[0].matriks.baris_1);
        console.log(matriks[0].matriks.baris_2);
        console.log(matriks[0].matriks.baris_3);
        console.log(hitung_determinan(matriks));
        console.log(hitung_matriks_kofaktor(matriks));

      });

      var matriks_kofaktor=[];
      function hitung_determinan(matriks){
        var str_nilai_1; var str_nilai_2; var str_nilai_3;
        var nilai_1=0; var nilai_2=0; var nilai_3=0;
        $.each(matriks, function(key){
            str_nilai_1=( this.matriks.baris_1[0]+"("+(this.matriks.baris_2[1]*this.matriks.baris_3[2])+"-"+(this.matriks.baris_3[1]*this.matriks.baris_2[2])+")" );
            
            nilai_1=this.matriks.baris_1[0]*((this.matriks.baris_2[1]*this.matriks.baris_3[2])-(this.matriks.baris_3[1]*this.matriks.baris_2[2]));
         
            str_nilai_2=( this.matriks.baris_1[1]+"("+(this.matriks.baris_2[0]*this.matriks.baris_3[2])+"-"+(this.matriks.baris_3[0]*this.matriks.baris_2[2])+")" );

            nilai_2=this.matriks.baris_1[1]*((this.matriks.baris_2[0]*this.matriks.baris_3[2])-(this.matriks.baris_3[0]*this.matriks.baris_2[2]));
         

            str_nilai_3=( this.matriks.baris_1[2]+"("+(this.matriks.baris_2[0]*this.matriks.baris_3[1])+"-"+(this.matriks.baris_3[0]*this.matriks.baris_2[1])+")" );

            nilai_3=this.matriks.baris_1[2]*((this.matriks.baris_2[0]*this.matriks.baris_3[1])-(this.matriks.baris_3[0]*this.matriks.baris_2[1]));
        });
        console.log("a. determinan");
        // matriks_kofaktor.push(nilai_1);
        // matriks_kofaktor.push(nilai_2);
        // matriks_kofaktor.push(nilai_3);
        return( str_nilai_1+"-"+str_nilai_2+"+"+str_nilai_3+"="+(nilai_1-nilai_2+nilai_3) );
      }

      function hitung_minor(matriks, element){
        var minor_1=[];
        var minor_2=[];
        var type;
        $.each(matriks, function(key){
          console.log(this.matriks.baris_1); 
          console.log("minor elemen "+this.matriks.baris_1[element] +" adalah");
          if(element==0){
            minor_1.push(this.matriks.baris_2[1]);
            minor_1.push(this.matriks.baris_2[2]);

            minor_2.push(this.matriks.baris_3[1]);
            minor_2.push(this.matriks.baris_3[2]);
            type="+";
          }else if(element==1){
            minor_1.push(this.matriks.baris_2[0]);
            minor_1.push(this.matriks.baris_2[2]);

            minor_2.push(this.matriks.baris_3[0]);
            minor_2.push(this.matriks.baris_3[2]);
            type="-";
          }else if(element==2){
            minor_1.push(this.matriks.baris_2[0]);
            minor_1.push(this.matriks.baris_2[1]);

            minor_2.push(this.matriks.baris_3[0]);
            minor_2.push(this.matriks.baris_3[1]);
            type="+";
          }

          
          // this.matriks.baris_1[0]=100;
          // console.log(this.matriks);
        });
        console.log(minor_1);
        console.log(minor_2);
        // console.log(minor_1[0]+"*"+minor_2[1]+"="+minor_1[0]*minor_2[1]);
        
        if(type=="-"){
          console.log( "="+type+"("+(minor_1[0]*minor_2[1])+"-"+(minor_2[0]*minor_1[1])+")="+-(minor_1[0]*minor_2[1]-minor_2[0]*minor_1[1]))
        }else{
          console.log( "="+type+"("+(minor_1[0]*minor_2[1])+"-"+(minor_2[0]*minor_1[1])+")="+(minor_1[0]*minor_2[1]-minor_2[0]*minor_1[1]))
        }
      }

      function hitung_matriks_kofaktor(matriks){
        console.log("b. matriks_kofaktor");
        console.log( hitung_minor(matriks, 0) );
        console.log( hitung_minor(matriks, 1) );
        console.log( hitung_minor(matriks, 2) );
      }

      // function hitung_penjumlahan(matriks, name_obj){
      //   var nilai_1=0;
      //   var nilai_2=0;
      //   var nilai_3=0;

      //   var nilai_arr=[];
      //   $.each(matriks, function(key){
      //       nilai_1+=this[name_obj][0];
      //       nilai_2+=this[name_obj][1];
      //       nilai_3+=this[name_obj][2];
      //   });
      //   nilai_arr.push(nilai_1);
      //   nilai_arr.push(nilai_2);
      //   nilai_arr.push(nilai_3);

      //   return nilai_arr;
      // }


      function hitung_penjumlahan(matriks, name_obj){
        var nilai_arr=[];

        $.each(matriks, function(key){
          // console.log(this);
          // console.log(this.matriks_1.baris_1[0]-this.matriks_2.baris_1[0]);
            // console.log(matriks_1_nilai_1);
            // console.log("jumlah"+ this.matriks_1[name_obj].length);
            var total_baris=this.matriks_1[name_obj].length;
            for(var i=0; i < total_baris; i++){
              nilai_arr.push(this.matriks_1[name_obj][i]+this.matriks_2[name_obj][i]);
            }

        });

        return nilai_arr;
      }

      function hitung_pengurangan(matriks, name_obj){
        var nilai_arr=[];

        $.each(matriks, function(key){
          // console.log(this);
          // console.log(this.matriks_1.baris_1[0]-this.matriks_2.baris_1[0]);
            // console.log(matriks_1_nilai_1);
            // console.log("jumlah"+ this.matriks_1[name_obj].length);
            var total_baris=this.matriks_1[name_obj].length;
            for(var i=0; i < total_baris; i++){
              nilai_arr.push(this.matriks_1[name_obj][i]-this.matriks_2[name_obj][i]);
            }

        });

        return nilai_arr;
      }

      function hitung_perkalian(matriks, ordo){
        var nilai_arr=[];

        $.each(matriks, function(key, value){
          console.log(ordo);
          $.each(ordo.baris, function(key_baris, baris){
            $.each(ordo.kolom, function(key_kolom, kolom){
              // console.log( value.matriks_1[kolom][key_baris] * value.matriks_2[baris] );
              nilai_arr.push(value.matriks_1[kolom][key_baris] * value.matriks_2[baris]);
            })
          })
          

           // console.log("baris_1");
           // console.log( this.matriks_1['baris_1'][0] * this.matriks_2['baris_1'] );
           // console.log( this.matriks_1['baris_1'][1] * this.matriks_2['baris_2'] );
           // console.log( this.matriks_1['baris_1'][2] * this.matriks_2['baris_3'] );

           // console.log("baris_2");
           // console.log( this.matriks_1['baris_2'][0] * this.matriks_2['baris_1'] );
           // console.log( this.matriks_1['baris_2'][1] * this.matriks_2['baris_2'] );
           // console.log( this.matriks_1['baris_2'][2] * this.matriks_2['baris_3'] );

            // var total_baris=this.matriks_1[name_obj].length;
            // for(var i=0; i < ordo.kolom; i++){
            //   //nilai_arr.push(this.matriks_1[name_obj][i]-this.matriks_2[name_obj][i]);
            //   for(var j=0; j < ordo.baris; j++){
            //     console.log( this.matriks_1['baris_1'][i] * this.matriks_2['baris_1'] );
            //   }
            // }

        });

        return nilai_arr;
      }

      // function hitung_pengurangan(matriks, name_obj){
      //   var nilai_1=0;
      //   var nilai_2=0;
      //   var nilai_3=0;

      //   var nilai_arr=[];
      //   var matriks_1_col_1=0;
      //   var matriks_2_col_1=0;

      //   $.each(matriks, function(key){
      //     console.log(this);

      //     // console.log(this.matriks_1.baris_1[0]-this.matriks_2.baris_1[0]);
      //       // console.log(matriks_1_nilai_1);
      //       nilai_1=this.matriks_1[name_obj][0]-this.matriks_2[name_obj][0];
      //       nilai_2=this.matriks_1[name_obj][1]-this.matriks_2[name_obj][1];
      //       nilai_3=this.matriks_1[name_obj][2]-this.matriks_2[name_obj][2];
      //   });
      //   nilai_arr.push(nilai_1);
      //   nilai_arr.push(nilai_2);
      //   nilai_arr.push(nilai_3);

      //   return nilai_arr;
      // }

    </script>
  </body>
</html>