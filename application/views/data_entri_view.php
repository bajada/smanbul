<?php $this->load->view('inc/header');?>

    <div class="container-fluid mb-3">
    <div class="row justify-content-center">
      <div class="col-md-9">
        <div class="card d-nones" style="border-top-color: #0062cc;border-top-width: 2px;">
          <!-- <h5 class="card-header bg-dark text-white">Data</h5> -->
          <div class="card-body">
            <h5 class="card-title">Form Entri Data</h5>
            <hr/>
           <!--  <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a> -->

            <form id="search-form">
            <div class="form-group row">
              <div class="col-sm-6">
                  <select class="custom-select" name="id_siswa" >
                    <option value="" selected="">Pilih/Cari Siswa</option>
                    <option value="1545904701505">Abdul</option>
                    <option value="1545904708360">Hajat</option>
                  </select>
              </div>
            </div>  
            </form>

            <div class="row">
              <div class="col-sm-12">

                <table id="tbl-data" class="table table-striped table-hover">
                <thead class="thead-light">
                  <!-- try rendered -->
                  <!-- <tr>
                    <th scope="col" width="10%">Nama Ekskul</th>
                    <th scope="col" width="15%">Minat(K1)</th>
                    <th scope="col" width="15%">Bakat/Kemampuan(K2)</th>
                    <th scope="col" width="15%">Jadwal Latihan(K3)</th>
                    <th scope="col" width="15%">Prestasi Ekskul(K4)</th>
                  </tr> -->
                </thead>
                <tbody>

                  <!-- <tr class="data-row" id="row-undefined">
                    <td class="">Basket</td>
                    <td width="15%"><select class="custom-select" name="nilai"><option
                          selected="" value="">*Pilih</option>
                        <option value="4">Sangat Berminat(4)</option>
                        <option value="3">Cukup Berminat(3)</option>
                        <option value="2">Kurang Berminat(2)</option>
                        <option value="1">Tidak Berminat(1)</option></select></td>
                    <td width="15%"><select class="custom-select" name="nilai"><option
                          selected="" value="">*Pilih</option>
                        <option value="4">Sangat Berbakat(4)</option>
                        <option value="3">Cukup Berbakat(3)</option>
                        <option value="2">Kurang Berbakat(2)</option>
                        <option value="1">Tidak Berbakat(1)</option></select></td>
                    <td width="15%"><select class="custom-select" name="nilai"><option
                          selected="" value="">*Pilih</option>
                        <option value="4">Sesuai(4)</option>
                        <option value="3">Cukup Sesuai(3)</option>
                        <option value="2">Kurang Sesuai(2)</option>
                        <option value="1">Tidak Sesuai(1)</option></select></td>
                    <td width="15%"><select class="custom-select" name="nilai"><option
                          selected="" value="">*Pilih</option>
                        <option value="4">Sangat Berprestasi(4)</option>
                        <option value="3">Cukup Berprestasi(3)</option>
                        <option value="2">Kurang Berprestasi(2)</option>
                        <option value="1">Tidak Berprestasi/Belum Pernah Ikut(1)</option></select>
                    </td>
                  </tr> -->

                </tbody>
              </table>
                
              </div>
            </div>

            <hr/>

            <div class="row">
            <div class="col-md-6 float-left">
              <button onclick="window.location.href='<?php echo base_url('DataController') ?>'" type="button" id="btn-identifikasi-kembali" class="btn btn-danger">
                <i class="fa fa-arrow-left mr_"></i> Kembali/Batal
              </button>
            </div>
            
            <div class="col-md-6 float-right text-right">
              <button type="button" id="btn-submit" class="btn btn-success btn-load"><span class="ladda-label">
                SUBMIT <i class="fa fa-check mr_"></i>
              </span></button>
            </div>
            
          </div>

          </div>
        </div>
      </div>
    </div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script src="<?php echo base_url('assets/js/data-entri-js.js')?>"></script> 
</body>

</html>