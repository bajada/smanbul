<?php $this->load->view('inc/header');?>

  <div class="pt-2">

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color:transparent;">
        <li class="breadcrumb-item"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Kembalilah</button></li>

        <li class="breadcrumb-item" aria-current="page">

            <!-- <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-hand-holding-usd fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span></a> -->

              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-users fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Siswa</span>
              </a>
        </li>
       <!--  <li class="breadcrumb-item" aria-current="page">
              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-suitcase fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span>
              </a>
        </li> -->
      </ol>
    </nav>

  </div>



    <div class="row justify-content-center">
      <div class="col-md-11">

  <div class="card bg-head pill ">
    <div class="card-header text-white" style="background-color: #487d95!important;font-family: segoeui;">
    <div class="row">
      <div class="col-md-7"> <h5 style="font-family: segoeui;">Data Siswa</h5></div>
      <div class="col-md-3 offset-2">
        <button onclick="$('#modal-form').modal('show');" type="button" class="btn btn-light btn-light pill pl-4 pr-4 float-right">Tambah</button> 
        <!-- <button type="button" class="btn  btn-secondary pill pl-4 pr-4 float-right mr-1">Aksi</button> -->
        <!-- Example split danger button -->
        <div class="btn-group float-right mr-2">
          <button type="button" class="btn btn-light pill-left pl-4">Aksi</button>
          <button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split pill-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="#">Edit</a>
            <a class="dropdown-item" href="#">Hapus</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="javascript:void(0)" onclick="goToEdit()" >Naik Kelas</a>
          </div>
        </div>
      </div>
      <!-- <div class="col-md-6">
        <a href="<?php echo base_url('page/form_siswa_view') ?>" type="" class="btn btn-light btn-lg btn-light pill pl-5 pr-5 float-right">Tambah</a>
      </div> -->
    </div>
    </div>

    <div class="card-body">
    <!-- <h3 class="card-title">
    <div class="row">
      <div class="col-md-6"> <h5 style="font-family: segoeui;">Data Siswa</h5></div>
      <div class="col-md-6"><button onclick="$('#modal-form').modal('show');" type="button" class="btn btn-light btn-light pill pl-4 pr-4 float-right">Tambah</button> <button onclick="goToEdit()" type="button" class="btn btn-light btn-light pill pl-4 pr-4 float-right mr-1">Action</button></div>
     
    </div>

    </h3> -->
      
    <!-- <hr/> -->

    <form id="search-form">
      <div class="form-group row">
        <div class="col-md-4">
          <input name="filter_keyword" type="text" class="form-control" autocomplete="off" placeholder="Ketik Nama/No.Ref/Peminatan Jurusan">
        </div>
        <div class="col-md-2 offset-4">
            <select name="filter_kelas" class="form-control" onchange="display()">
                <option value="0">Semua Kelas</option>
                <option value="X">X</option>
                <option value="XI">XI</option>
                <option value="XII">XII</option>
                <!-- <option value="ALUMNI">ALUMNI</option> -->
            </select> 
        </div>
        <div class="col-md-2">
            <select name="filter_tahun_ajaran" onchange="filter();" id="tahun_ajaran" class="form-control">
              <option>Pilih Tahun Ajaran</option>
            </select>
        </div>
      </div>
    </form>

    <div class="row">
      
      <div class="col-12">
        <div class="tab-content" id="v-pills-tabContent">
          <div class="tab-pane fade show active" id="v-pills-kelas-none">

                <div class="row">
                  <div class="col-md-12 pill-top ">

                  <table id="tbl-data" class="table table-striped table-responsive" style="font-family: segoeui;opacity: 0.8;display: table;">
                    <thead class="text-white" style="background-color: #487d95!important;">
                      <tr>
                        <th width="4%"></th>
                        <!-- <th width="8%">No</th> -->
                        <th width="10%">No.REF</th>
                        <th>Nama Lengkap</th>
                        <th>Kelas</th>
                        <th>Jenis Kelamin</th>
                        <th>Tahun Ajaran</th>
                        <th>Angkatan</th>
                        <th style="width:125px;"></th>
                      </tr>
                    </thead>
                    <tbody class="text-dark">


                    </tbody>
                  </table>

                  <hr/>
                   <?php
  if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form 
    if(isset($upload_error)){ // Jika proses upload gagal
      echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
      die; // stop skrip
    }
    
    // Buat sebuah tag form untuk proses import data ke database
    echo "<form method='post' action='".base_url("index.php/Siswa/import")."'>";
    
    // Buat sebuah div untuk alert validasi kosong
    echo "<div style='color: red;' id='kosong'>
    Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
    </div>";
    
    echo "<table border='1' cellpadding='8'>
    <tr>
      <th colspan='6'>Preview Data</th>
    </tr>
    <tr>
      <th class='text-center' rowspan='2'>Nama</th>
      <th class='text-center' rowspan='2'>Jenis Kelamin</th>
      <th class='text-center' colspan='3'>IURAN</th>
      <th class='text-center' rowspan='2'>Kelas</th>
    </tr>
    <tr>
      <th>BULANAN</th>
      <th>DPMP</th>
      <th>DU</th>
    </tr>";
    
    $numrow = 1;
    $kosong = 0;
    
    // Lakukan perulangan dari data yang ada di excel
    // $sheet adalah variabel yang dikirim dari controller
    foreach($sheet as $row){ 
      // Ambil data pada excel sesuai Kolom
      $col_1 = $row['A']; // Ambil data Kolom A
      $col_2 = $row['B'];
      $col_3 = $row['C']; 
      $col_4 = $row['D'];
      $col_5 = $row['E'];
      $col_6 = $row['F'];
      
      // Cek jika semua data tidak diisi
      if(empty($col_1) && empty($col_2))
        continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)
      
      // Cek $numrow apakah lebih dari 2
      // Artinya karena baris pertama adalah nama-nama kolom
      // Jadi dilewat saja, tidak usah diimport
      if($numrow > 2){
        // Validasi apakah semua data telah diisi
        $nama_td = ( ! empty($col_1))? "" : " style='background: #E07171;'"; // Jika Nama kosong, beri warna merah
        $jk_td = ( ! empty($col_2))? "" : " style='background: #E07171;'"; // Jika JK kosong, beri warna merah
        
        // Jika salah satu data ada yang kosong
        if(empty($col_1) or empty($col_2)){
          $kosong++; // Tambah 1 variabel $kosong
        }
        
        echo "<tr>";
          echo "<td".$nama_td.">".$col_1."</td>";
          echo "<td".$jk_td.">".$col_2."</td>";
          echo "<td>".$col_3."</td>";
          echo "<td>".$col_4."</td>";
          echo "<td>".$col_5."</td>";
          echo "<td>".$col_6."</td>";
        echo "</tr>";
      }
      
      $numrow++; // Tambah 1 setiap kali looping
    }
    
    echo "</table>";
    
    // Cek apakah variabel kosong lebih dari 0
    // Jika lebih dari 0, berarti ada data yang masih kosong
    if($kosong > 0){
    ?>  
      <script>
      $(document).ready(function(){
        // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
        $("#jumlah_kosong").html('<?php echo $kosong; ?>');
        
        $("#kosong").show(); // Munculkan alert validasi kosong
      });
      </script>
    <?php
    }else{ // Jika semua data sudah diisi
      echo "<hr>";
      
      // Buat sebuah tombol untuk mengimport data ke database
      echo "<button type='submit' name='import'>Import</button>";
      echo "<a href='".base_url("index.php/Siswa")."'>Cancel</a>";
    }
    
    echo "</form>";
  }
  ?>

                  </div>
                </div>
          </div>
        </div>
      </div>
    </div>

    </div>
  </div>
</div>
</div>
    </div>

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md pill text-white" role="document" styles="max-width: 700px">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="font-family: segoeuib">Tambah Siswa Baru</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form-siswa">
              <div class="form-row justify-content-center">
                <div class="form-group col-md-6">
                  <label for="">Jumlah Siswa</label>
                  <input name="jumlah_siswa" type="text" class="form-control" autocomplete="off"  placeholder="Masukkan Jumlah Data">
                </div>
                
               <div class="form-group col-md-6">
                  <label>Kelas</label>
                  <select name="kelas" class="form-control select-styles">
                      <option value="0">Pilih kelas</option>
                  </select>
                </div> 
              </div>    

             <!--  <div class="form-row d-none">
                  <div class="form-group col-3">
                      <label>Kelas</label>
                      <select name="kelas" class="form-control select-style">
                          <option value="">Pilih Kelas</option>
                          <option value="X">X</option>
                          <option value="XI">XI</option>
                          <option value="XII">XII</option>
                      </select>
                  </div>
                  <div class="form-group col-6">
                      <label>Jurusan</label>
                      <select name="jurusan" class="form-control select-style">
                          <option value="">Pilih Jurusan</option>
                          <option value="IPS">IPS</option>
                          <option value="MIPA">MIPA</option>
                          <option value="BAHASA">BAHASA</option>
                      </select>
                  </div>
                  <div class="form-group col-3">
                      <label>Sub Kelas</label>
                      <select name="sub_kelas" class="form-control select-style">
                          <option value="0">Pilih</option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                      </select>
                  </div>
              </div>    -->

              <!-- <div class="form-row">
                  <div class="form-group col-md-12">
                      <label>Wali Kelas</label>
                       <input name="" type="text" class="form-control" id=""  placeholder="Cecep Supriatna" readonly>
                  </div>
              </div>  -->

              <button id="btn-submit-2" type="submit" class="btn btn-secondary pill pl-4 pr-4 float-right d-none">SUBMIT</button>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light bg-head pill pl-4 pr-4" data-dismiss="modal">CLOSE</button>
            <button onclick="$('#btn-submit-2').click()" type="button" class="btn btn-secondary btn-secondary pill pl-4 pr-4">SUBMIT</button>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <!-- <script src="<?php echo base_url('assets/js/siswa-script.js?lol')?>"></script>  -->

  </body>
</html>