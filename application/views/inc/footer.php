
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->

<script src="<?php echo base_url('assets/jquery/jquery.min.js')?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script src="<?php echo base_url('assets/jquery/jquery.number.min.js')?>"></script>
<script src="<?php echo base_url('assets/selectric/jquery.selectric.js')?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
 --><script src="<?php echo base_url('assets/sweetalert/sweetalert2.min.js')?>"></script>
<!-- -->

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


<!-- Tiny Mce -->
<!-- <script src="https://cdn.tiny.cloud/1/ozex2sshqfvvngksdwhvlsp31e3xcnes8jhx9o773n4g4mi0/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
 -->

<script src="<?php echo base_url('assets/js/common-script.js')?>"></script>