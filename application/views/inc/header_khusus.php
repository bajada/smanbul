<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>KP SISTEM ADMINISTRASI SEKOLAH</title>
	
	<script>var ctx = "<?php echo base_url() ?>"</script>

    <style type="text/css">

      html, body {
        background-image: linear-gradient(to bottom right, rgb(2, 124, 161), rgb(149, 169, 18));
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        height: 100%;
        font-family: segoeuili;
        background-attachment: fixed;
      }

	  table, thead, th, td {
	    /*padding: 1rem!important;
	    padding-left: 1rem!important;
    	padding-right: 1rem!important;*/
	  }
	  .table-pill{
    	border-radius: 20px!important;
    	overflow: hidden;
	  }

      .bg-gradient{
        background-image: linear-gradient(to bottom right, rgb(2, 124, 161), rgb(149, 169, 18));
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        height: 100%;
      }
      .bg-login {
        background-color: rgba(170, 192, 171, 1);
        opacity: 0.9;	
      }

      .bg-head {
        background-color: rgba(170, 192, 171, 0.5);
        color:white;
      }

      .form-control {
          height: 46px;
          border: none;
          background: #f7f7f7;
          width: 100%;
          padding: 0 25px;
          border-radius: 23px;
          color: #333;
      }

      /*.select-auto-complete {
          height: 46px;
          border: none;
          background: #f7f7f7;
          width: 100%;
          padding: 0 25px;
          border-radius: 23px;
          color: #333;
      }*/
      .select2-container--default .select2-selection--multiple {
        border-radius: 23px!important;
        /* height: 41px; */
        background: #f7f7f7!important;
        height: 46px!important;
        color: #333;
        padding: 8px 17px;
      }
      .select2-container--default.select2-container--focus .select2-selection--multiple {
          border: solid black 0px!important;
          outline: 0!important;
          padding: 4px 16px!important;
      }
      .select2-container--default .select2-selection--single {
        border-radius: 23px!important;
        /* height: 41px; */
        background: #f7f7f7!important;
        height: 46px!important;
        color: #333;
        padding: 8px 17px;
      }
      .select2-container--default .select2-selection--single .select2-selection__arrow {
          padding: 20px;
      }

      .selectric .label {
          /*height: 46px;
          border: none;
          background: #f7f7f7;
          width: 100%;
          border-radius: 23px;*/
          padding: 0 15px!important;
          font-size: 1rem!important;
          color: #495057!important;
      }

      input, textarea, select, button .select-auto-complete {
          /*font-family: poppins-regular;*/
          font-size: 13px;
          border-radius: 23px;
      }
      label {
        font-size: 20px!important;
      }

      .pill {
        border-radius: 23px!important;
      }

      .pill-top {
        border-top-left-radius: 23px!important;
        border-top-right-radius: 23px!important;
      }

      .pill-bottom {
        border-bottom-left-radius: 23px!important;
        border-bottom-right-radius: 23px!important;
      }
      .pill-right {
        border-top-right-radius: 23px!important;
        border-bottom-right-radius: 23px!important;
      }
      .pill-left {
        border-top-left-radius: 23px!important;
        border-bottom-left-radius: 23px!important;
      }

      .font-main {
        font-weight: 300!important;
      }
      .text-white-href {
          color:white!important;
      }
      .text-white-href:hover {
          color: -webkit-link;
          cursor: pointer;
          text-decoration: none;
          color:#f8f9fa!important;
      }
      .btn-light {
        border:0px;
      }
      .btn-secondary{
        background: #49766d;
      }

      @font-face {
        font-family: segoeui;
        src: url("<?php echo base_url('assets/my/font/segoeui.ttf')?>");
      }

      @font-face {
        font-family: segoeuili;
        src: url("<?php echo base_url('assets/my/font/segoeuil.ttf')?>");
      }

      @font-face {
        font-family: segoeuib;
        src: url("<?php echo base_url('assets/my/font/segoeuib.ttf')?>");
      }
    </style>

    
<style class="text/css">
::-webkit-scrollbar {
  width: 0.15em;
  height: 0.15em; }

::-webkit-scrollbar-thumb {
  background: slategray; }

::-webkit-scrollbar-track {
  background: #b8c0c8; }

body {
  scrollbar-face-color: slategray;
  scrollbar-track-color: #b8c0c8; }

body,
html {
  height: 100%;
  width: 100%; }

/*body {
  font-family: "Roboto"; }*/

.wrap {
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  height: 100%;
  width: 100%; }

body {
/*  background: #f8f8f8; */
}

button.btn {
  /*box-shadow: 0 2px 3px rgba(0, 0, 0, 0.2);*/
  border: 0;
  border-radius: 0px; 
}
  button.btn i {
    margin-right: 3px; }

body.large-screen .table-wrapper {
  max-width: 800px; }
body.large-screen .card-list-table {
  background: white; }
  body.large-screen .card-list-table tbody tr {
    background: transparent;
    box-shadow: none;
    margin: 0; }
    body.large-screen .card-list-table tbody tr:nth-of-type(even) {
      background: #dfdfdf; }
  body.large-screen .card-list-table thead {
    display: table-header-group; }
    body.large-screen .card-list-table thead th:last-child {
      box-shadow: none; }
    body.large-screen .card-list-table thead th {
      border-bottom: 1px solid #dfdfdf;
      padding: 12px 24px; }
  body.large-screen .card-list-table tbody tr {
    display: table-row;
    padding-bottom: 0; }
    body.large-screen .card-list-table tbody tr:nth-of-type(even) {
      background: #fff; }
  body.large-screen .card-list-table tbody td {
    border-bottom: 1px solid #dfdfdf;
    cursor: pointer;
    display: table-cell;
    /*padding: 20px 24px;*/padding: 0.75rem 24px;
    transition: background .2s ease-out;
    vertical-align: middle; }
    body.large-screen .card-list-table tbody td:after {
      display: none; }
    body.large-screen .card-list-table tbody td:before {
      content: ''; }
  body.large-screen .card-list-table tbody tr:hover td {
    background: #fcf3d0; }

.buttons {
  margin: 10px 0 50px; }

.table-wrapper {
  max-width: 300px;
  width: 80%;
  margin: 0 auto 0;
  max-height: 500px;
  overflow-y: scroll;
  position: relative;
  transition: all .2s ease-out; }
  @media (min-width: 768px) {
    .table-wrapper {
      background: white;
      box-shadow: 0 2px 3px rgba(0, 0, 0, 0.2); } }

.card-list-table {
  table-layout: fixed;
  background: transparent;
  margin-bottom: 0;
  width: 100%; }
  .card-list-table thead {
    display: none; }
  .card-list-table tbody tr {
    box-shadow: 0 2px 3px rgba(0, 0, 0, 0.2);
    background: #fff;
    border-bottom: 1px solid #dfdfdf;
    cursor: pointer;
    display: block;
    padding: 15px 10px;
    margin: 0 0 10px 0; }
  .card-list-table tbody td {
    border: 0;
    display: block;
    padding: 10px 10px 20px 40%;
    position: relative; }
    .card-list-table tbody td:first-of-type::after {
      visibility: hidden; }
    .card-list-table tbody td:after {
      content: '';
      width: calc(100% - 30px);
      display: block;
      margin: 0 auto;
      height: 1px;
      background: #dfdfdf;
      position: absolute;
      left: 0;
      right: 0;
      top: -6px; }
    .card-list-table tbody td:before {
      color: rgba(0, 0, 0, 0.35);
      text-transform: uppercase;
      font-size: .85em;
      content: attr(data-title);
      display: table-cell;
      font-weight: 500;
      height: 100%;
      left: 15px;
      margin: auto;
      position: absolute;
      vertical-align: middle;
      white-space: nowrap;
      width: 40%; }
  .card-list-table thead th {
    text-transform: uppercase;
    font-size: .85em;
    /*color: rgba(0, 0, 0, 0.35);*/
    letter-spacing: .5pt; }

/*# sourceMappingURL=style-jadi.css.map */

  
</style>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="<?php echo base_url('assets/my/font-awesome-custom/css/font-more-awesome.css')?>" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="<?php echo base_url('assets/selectric/selectric.css')?>" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url('assets/sweetalert/sweetalert2.min.css')?>" rel="stylesheet">
  </head>
  <body class="large-screen"> 

    <div class="container-fluid mt-3">

      <div class="media bg-head pill pl-5 pt-3  pb-3">
        <img src="http://sman1-cibungbulang.sch.id/assets/img/smanbul.png" style="width: 100px;" class="ml-3 mr-3" alt="GAMBAR HERE">
        <div class="media-body mt-auto mb-auto">
          <h2 class="font-main">Sistem Informasi Pembayaran</h2>
          <h3 class="font-main">SMAN 1 CIBUNGBULANG</h3>
        </div>
        <div class="pr-5 pt-3  pb-3">
          <a href="<?php echo base_url('page/dashboard_view') ?>" class="mr-4 text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-home fa-4x" aria-hidden="true"></i> Home</a>
          <a href="<?php echo base_url('page/login_view') ?>" class="text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-globe fa-4x" aria-hidden="true"></i> Website</a>
        </div>
      </div>