<div class="modal fade" data-backdrop="static" id="modalconfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">
        
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div id="msg" class="alert alert-success" role="alert" hidden="hidden"></div>
                    </div>
                </div>
            </div>
            
            <div class="modal-body">        
                <div  id="progressBarConfirm"  class="row">
                  <div class="col-xs-12">
                    <div class="progress">
                      <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                        <span class="sr-only">45% Complete</span>
                      </div>
                    </div>
                  </div>
                </div>      
                <div id="txtConfirmContent" class="form-group">
                    <p>Apakah anda yakin akan menghapus data pengguna ini?</p>
                </div>
                
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">BATAL</button>
                    <button id="btnRemove" onclick="doRemove($(this).data('id'));" data-id="0" type="button" class="btn btn-primary">
                    <span class="glyphicon glyphicon-trash"></span>  
                    HAPUS</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="msg-confirm" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md app flex-row align-items-center">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title font-weight-bold">Konfirmasi</div>
      </div>
      <div class="modal-body">
      <p id="msg-confirm-alert" style="color: red">asdasd</p>
        <p class="card-title" id="msg-confirm-body"></p>
        <div class="row">
          <div class="col">
            <button onclick="$('#msg-confirm').modal('hide');" type="button" class="btn btn-primary btn-block" data-dismiss="modal"><i class="fa fa-close"></i> TIDAK</button>
          </div>
          <div class="col">
            <button id="btn-yes-msg-confirm" type="button" class="btn btn-danger btn-block"><i class="fa fa-check"></i> YA</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--  <div class="row justify-content-center d-flex align-items-center" style="height: 600px!important"> -->
<!-- <footer class="footer bg-dark">
	<div class="container justify-content-center d-flex align-items-end" style="height: 150px!important">
		<p class="text-muted text-center">Copyright © 2019 - Kelompok </p>
	</div>
</footer> -->

<!-- <script src="<php echo base_url('assets/jquery/jquery-3.3.1.slim.min.js')?>"></script> -->
<script src="<?php echo base_url('assets/jquery/jquery.min.js')?>"></script>
<script src="<?php echo base_url('assets/ajax/libs/popper.min.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap/4.2.1/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script src="<?php echo base_url('assets/jquery/jquery.number.min.js')?>"></script>

<script src="<?php echo base_url('assets/js/common-script.js')?>"></script>
