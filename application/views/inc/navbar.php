<!-- <header class="image-bg-fluid-height"> -->
<!-- </header> -->

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="<?php echo base_url('page/dashboard_view');?>">SMART ESKUL</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url('page/dashboard_view');?>">Home <span class="sr-only">(current)</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="data_entri_view">Entri Data</a>
      </li>

      <!-- <li class="nav-item">
        <a class="nav-link" href="SmartController/getNormalisasi">Normalisasi</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="DataController">Data</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="SmartController/getUtility">Utility</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="SmartController/getNilaiAkhir">Nilai Akhir</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="SmartController/getRank">Ranking</a>
      </li> -->

      </li>
      <li class="nav-item dropdown d-none">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Perhitungan
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Normalisasi</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Data</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Utility</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Nilai Akhir</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Ranking</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Pengaturan
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo base_url('page/user_view');?>">Pengguna</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url('page/siswa_view');?>">Siswa</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url('page/alternatif_view');?>">Alternatif</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url('page/kriteria_view');?>">Bobot Kriteria</a>
        </div>
      </li>
    </ul>
   <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">

    
      <span class="nav-item nav-link mr-md-2">
        Welcome User
      </span>

  </ul>
      <a href="LoginController" class="btn btn-outline-danger my-2 my-sm-0" >Logout</a>
  </div>
</nav>

<div class="jumbotron jumbotron-fluid" style="background: #0062cc;color:white">
<div class="container">
    <h1 class="display-4">SPK SMART ESKUL</h1>
    <p class="lead">Aplikasi ini adalah sistem yang dapat membantu pengambilan keputusan dengan menggunakan Metode SMART.</p>
  </div>
</div>  