<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>KP SISTEM ADMINISTRASI SEKOLAH</title>
	
	<script>var ctx = "<?php echo base_url() ?>"</script>

    <style type="text/css">

      html, body {
        background-image: linear-gradient(to bottom right, rgb(2, 124, 161), rgb(149, 169, 18));
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        height: 100%;
        font-family: segoeuili;
        background-attachment: fixed;
      }

	  /*table, thead, th, td {
	    padding-left: 1rem!important;
    	padding-right: 1rem!important;
	  }*/
    /*.daterangepicker, table, thead, th, td {
      padding-left: unset!important;
      padding-right: unset!important;
    }*/

	  .table-pill{
    	border-radius: 20px!important;
    	overflow: hidden;
	  }

      .bg-gradient{
        background-image: linear-gradient(to bottom right, rgb(2, 124, 161), rgb(149, 169, 18));
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        height: 100%;
      }

      .bg-gradient-2{
        background-image: linear-gradient(to bottom right, #28a745, #5897fb);
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        height: 100%;
      }

      .bg-gradient-3{
          opacity: .9;
          background-image: linear-gradient(90deg,#f68a1e,#ef403d),linear-gradient(180deg,#f88a00,#f88a00);
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
          height: 100%;
      }
      .bg-login {
        background-color: rgba(170, 192, 171, 1);
        opacity: 0.9;	
      }

      .bg-head {
        background-color: rgba(170, 192, 171, 0.5);
        color:white;
      }

      .bg-head-2 {
        background-color: rgba(170, 192, 171, 0.34);
        color:white;
      }

      .bg-purple {
        background-color: var(--purple)!important;
      }

      .form-control {
          height: 46px;
          border: none;
          background: #f7f7f7;
          width: 100%;
          padding: 0 25px;
          border-radius: 23px;
          color: #333;
      }

      /*.select-auto-complete {
          height: 46px;
          border: none;
          background: #f7f7f7;
          width: 100%;
          padding: 0 25px;
          border-radius: 23px;
          color: #333;
      }*/
      .select2-container--default .select2-selection--multiple {
        border-radius: 23px!important;
        /* height: 41px; */
        background: #f7f7f7!important;
        height: 46px!important;
        color: #333;
        padding: 8px 17px;
      }
      .select2-container--default.select2-container--focus .select2-selection--multiple {
          border: solid black 0px!important;
          outline: 0!important;
          padding: 4px 16px!important;
      }
      .select2-container--default .select2-selection--single {
        border-radius: 23px!important;
        /* height: 41px; */
        background: #f7f7f7!important;
        height: 46px!important;
        color: #333;
        padding: 8px 17px;
      }
      .select2-container--default .select2-selection--single .select2-selection__arrow {
          padding: 20px;
      }

      .selectric .label {
          /*height: 46px;
          border: none;
          background: #f7f7f7;
          width: 100%;
          border-radius: 23px;*/
          padding: 0 15px!important;
          font-size: 1rem!important;
          color: #495057!important;
      }

      input, textarea, select, button .select-auto-complete {
          /*font-family: poppins-regular;*/
          font-size: 13px;
          border-radius: 23px;
      }
      label {
        font-size: 20px!important;
      }

      .pill {
        border-radius: 23px!important;
      }
      .pill-right {
        border-top-right-radius: 23px!important;
        border-bottom-right-radius: 23px!important;
      }
      .pill-left {
        border-top-left-radius: 23px!important;
        border-bottom-left-radius: 23px!important;
      }

      .font-main {
        font-weight: 300!important;
      }
      .text-white-href {
          color:white!important;
      }
      .text-white-href:hover {
          color: -webkit-link;
          cursor: pointer;
          text-decoration: none;
          color:#f8f9fa!important;
      }
      .btn-light {
        border:0px;
      }
      .btn-secondary{
        background: #49766d;
      }

      @font-face {
        font-family: segoeui;
        src: url("<?php echo base_url('assets/my/font/segoeui.ttf')?>");
      }

      @font-face {
        font-family: segoeuili;
        src: url("<?php echo base_url('assets/my/font/segoeuil.ttf')?>");
      }

      @font-face {
        font-family: segoeuib;
        src: url("<?php echo base_url('assets/my/font/segoeuib.ttf')?>");
      }
      .border-radius-0{
        border-radius:0!important;
      }

    </style>

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="<?php echo base_url('assets/my/font-awesome-custom/css/font-more-awesome.css')?>" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="<?php echo base_url('assets/selectric/selectric.css')?>" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url('assets/sweetalert/sweetalert2.min.css')?>" rel="stylesheet">

    <!--perfect scroll bar -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.css" rel="stylesheet" />
    
  </head>
  <body class="large-screen"> 

  <div id="bg-head" class="p-3 bg-head">
    <div class="media ">
      
      <img src="<?php echo base_url('assets/img/smanbul.png')?>" style="width: 100px;" class="ml-3 mr-3" alt="GAMBAR HERE">
        <div class="media-body mt-auto mb-auto">
          <h2 class="font-main">Sistem Informasi Pembayaran</h2>
          <h3 class="font-main">SMAN 1 CIBUNGBULANG</h3>
        </div>
        <div class="pr-5 pt-3  pb-3">
          <a href="<?php echo base_url('page/dashboard_view') ?>" class="mr-4 text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-home fa-4x" aria-hidden="true"></i> Home</a>
          <a href="<?php echo base_url('PageController/user_logout') ?>" class="text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-power-off fa-4x" aria-hidden="true"></i> Log Out</a>
        </div>
      </div>
    </div>
    
    <div class="container-fluid mt-3">

<!--       <div class="media bg-head pill pl-5 pt-3  pb-3">
        <img src="http://sman1-cibungbulang.sch.id/assets/img/smanbul.png" style="width: 100px;" class="ml-3 mr-3" alt="GAMBAR HERE">
        <div class="media-body mt-auto mb-auto">
          <h2 class="font-main">Sistem Informasi Pembayaran</h2>
          <h3 class="font-main">SMAN 1 CIBUNGBULANG</h3>
        </div>
        <div class="pr-5 pt-3  pb-3">
          <a href="<?php echo base_url('page/dashboard_view') ?>" class="mr-4 text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-home fa-4x" aria-hidden="true"></i> Home</a>
          <a href="<?php echo base_url('page/login_view') ?>" class="text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-globe fa-4x" aria-hidden="true"></i> Website</a>
        </div>
      </div> -->