<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Hello, world!</title>

    <style type="text/css">
      /*.grad {
        background-image: linear-gradient(to bottom right, rgba(78,123,105,1), rgba(133,154,52,1));
        background-repeat: no-repeat;
         background-size: cover;
         height: 100%;
      }*/


      html, body {
        background-image: linear-gradient(to bottom right, rgb(2, 124, 161), rgb(149, 169, 18));
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        height: 100%;
        font-family: segoeuili;
      }

      .bg-login {
        background-color: rgba(170, 192, 171, 1)
      }

      .bg-head {
        background-color: rgba(170, 192, 171, 0.5);
        color:white;
      }

      .form-control {
          height: 46px;
          border: none;
          background: #f7f7f7;
          width: 100%;
          padding: 0 25px;
          border-radius: 23px;
          color: #333;
      }

      input, textarea, select, button {
          /*font-family: poppins-regular;*/
          font-size: 13px;
          border-radius: 23px;
      }
      label {
        font-size: 20px!important;
      }

      .pill {
        border-radius: 23px;
      }

      .font-main {
        font-weight: 300!important;
      }
      .text-white-href {
          color:white!important;
      }
      .text-white-href:hover {
          color: -webkit-link;
          cursor: pointer;
          text-decoration: underline;
          color: #f8f9fa/*rgba(0,123,255,.25)*/!important;
          text-decoration: none;
      }

      .brightness-filter {
        filter: brightness(0.25);
      }

      .btn-light {
        border:0px;
      }
      .btn-secondary{
        background: #49766d;
      }

      .text-muted{
        color:#768749!important;
      }

      @font-face {
        font-family: segoeui;
        src: url("<?php echo base_url('assets/my/font/segoeui.ttf')?>");
      }

      @font-face {
        font-family: segoeuili;
        src: url("<?php echo base_url('assets/my/font/segoeuil.ttf')?>");
      }
    </style>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="<?php echo base_url('assets/my/font-awesome-custom/css/font-more-awesome.css')?>" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  </head>
  <body class="bg-light">
    <!-- <php include('inc/navbar.php'); ?> -->
    <div class="container-fluid mt-4">
      <div class="media bg-head pill pl-5 pt-3  pb-3">
        <img src="http://sman1-cibungbulang.sch.id/assets/img/smanbul.png" style="width: 140px;" class="ml-3 mr-3" alt="GAMBAR HERE">
        <div class="media-body  text-center">
          <h2 class="font-main">Sistem Informasi Pembayaran</h2>
          <h3 class="font-main display-4">SMAN 1 CIBUNGBULANG</h3>
          <h5 class="font-main" style="font-size: 1.15rem;font-style: bold;font-weight: bold!important;">Jl. Kapten Dasuki Bakri No.18, Cibatok, Cibungbulang, Bogor, Jawa Barat 16630</h5>
        </div>
        <div class="pr-5 pt-3  pb-3">
          <a href="" class="mr-4 text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-home fa-4x" aria-hidden="true"></i> Home</a>
          <a href="<?php echo base_url('PageController/user_logout') ?>" class="text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-globe fa-4x" aria-hidden="true"></i> Website</a>
        </div>
      </div>

      <div class="row d-none">
        <div class="col-md-12">
          <div class="jumbotron">
            <h1 class="">Sistem Informasi Pembayaran</h1>
            <p class="lead">SMAN 1 CIBUNGBULANG</p>
            <hr class="my-4">
            <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
            <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
          </div>
        </div>
      </div>


      <div class="container pl-5 pt-5 pb-3 pr-5">
      <div class="row">
        <div class="col-md-4">
            <div class="text-center">
              <a href="<?php echo base_url('page/iuran_view') ?>" class="text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><img src="<?php echo base_url('assets/my/icon/P1-Lighticons-full-155 copy.png')?>" style="width: 110px;" class="ml-5 mr-5 img-fluid mb-4" alt="GAMBAR HERE"> <button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">IURAN</button></a>
            </div>
        </div>

        <?php if($this->session->userdata('role_user')==1) {?>
        <div class="col-md-4">
          <div class="text-center">
            <a href="<?php echo base_url('page/laporan_view') ?>" class="text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><img src="<?php echo base_url('assets/my/icon/document6 copy.png')?>" style="width: 110px;" class="ml-5 mr-5 img-fluid mb-4" alt="GAMBAR HERE"> <button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">LAPORAN</button></a>
          </div>  
        </div>
        <?php } ?>

        <div class="col-md-4">
          <div class="text-center">
                <a href="<?php echo base_url('page/pengaturan_view') ?>" class="text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><img src="<?php echo base_url('assets/my/icon/P1-Lighticons-full-320 copy.png')?>" style="width: 110px;" class="ml-5 mr-5 img-fluid mb-4" alt="GAMBAR HERE"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">MANAJEMEN</button></a>
          </div>  
        </div>
      </div>
      </div>


      <div class="container mr-5 pl-0">
      <div class="row">
        <div class="col-md-4">
            <div style="font-size: 2.5rem;" class="text-white">
              <div>
                <a href="" class="text-white-href">
                <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fas fa-dollar fa-stack-1x fa-inverse text-muted"></i>
                </span>
                <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran BULANAN</span>
                </a>
              </div>

              <!-- 
              <div><a href="" class="text-white-href"><i class="fas fa-hand-holding-usd fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span></a></div>

              <div><a href="" class="text-white-href"><i class="fas fa-suitcase fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran DPMP</span></a></div>
              -->
              <div>
                <a href="<?php echo base_url('page/iuran_dpmp_view') ?>" class="text-white-href">
                <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fas fa-building fa-stack-1x fa-inverse text-muted"></i>
                </span>
                <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran DPMP</span>
                </a>
              </div>

              <div>
                <a href="" class="text-white-href">
                <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fas fa-suitcase fa-stack-1x fa-inverse text-muted"></i>
                </span>
                <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran DU</span>
                </a>
              </div>

            </div>
        </div>
        
        <div class="col-md-4">
            <div style="font-size: 2.5rem;" class="text-white">
              <!-- <div style="margin-left: 5px;"><a href="" class="text-white-href"><i class="fas fa-edit fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center">Harian</span></a></div>

              <div><a href="" class="text-white-href"><i class="far fa-clipboard fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Bulanan</span></a></div>

              <div><a href="" class="text-white-href"><i class="far fa-file-alt fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Tahunan</span></a></div>
              -->
              <div>
                <a href="<?php echo base_url('page/laporan_global_view') ?>" class="text-white-href">
                <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fas fa-edit fa-stack-1x fa-inverse text-muted"></i>
                </span>
                <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Harian</span>
                </a>
              </div>


              <div>
                <a href="" class="text-white-href">
                <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fas fa-clipboard fa-stack-1x fa-inverse text-muted"></i>
                </span>
                <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Bulanan</span>
                </a>
              </div>


              <div>
                <a href="" class="text-white-href">
                <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fas fa-file-alt fa-stack-1x fa-inverse text-muted"></i>
                </span>
                <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Tahunan</span>
                </a>
              </div>

            </div>
        </div>
        <div class="col-md-4">
            <div style="font-size: 2.5rem;" class="text-white">
              <!-- <div><a href="" class="text-white-href"><i class="fas fa-users fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Siswa</span></a></div>

              <div><a href="" class="text-white-href"><i class="fas fa-chalkboard-teacher fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Wali Kelas</span></a></div>

              <div><a href="" class="text-white-href"><i class="fa fa-user-circle fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Pengguna</span></a></div> -->

              <div>
                <a href="<?php echo base_url('page/siswa_view') ?>" class="text-white-href">
                <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fas fa-users fa-stack-1x fa-inverse text-muted"></i>
                </span>
                <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Siswa</span>
                </a>
              </div>

              <div>
                <a href="<?php echo base_url('page/kelas_view') ?>" class="text-white-href">
                <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fas fa-chalkboard-teacher fa-stack-1x fa-inverse text-muted"></i>
                </span>
                <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Kelas</span>
                </a>
              </div>

              <div>
                <a href="<?php echo base_url('page/jenis_iuran_view') ?>" class="text-white-href">
                <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fa fa-money-check fa-stack-1x fa-inverse text-muted"></i>
                </span>
                <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Pengaturan Iuran</span>
                </a>
              </div>

              <div>
                <a href="<?php echo base_url('page/user_view') ?>" class="text-white-href">
                <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fas fa-user fa-stack-1x fa-inverse text-muted"></i>
                </span>
                <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Pengguna</span>
                </a>
              </div>

            </div>
        </div>
      </div>
      </div>

      <div class="container d-none">
      <div class="row justify-content-center d-flex align-items-center p-5">
        <div class="col-md-4">
          <div class="card" style="width: 18rem;">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
            <ul class="list-group list-group-flush">
              <li class="list-group-item">Cras justo odio</li>
              <li class="list-group-item">Dapibus ac facilisis in</li>
              <li class="list-group-item">Vestibulum at eros</li>
            </ul>
            <div class="card-body">
              <a href="#" class="card-link">Card link</a>
              <a href="#" class="card-link">Another link</a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card" style="width: 18rem;">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
            <ul class="list-group list-group-flush">
              <li class="list-group-item">Cras justo odio</li>
              <li class="list-group-item">Dapibus ac facilisis in</li>
              <li class="list-group-item">Vestibulum at eros</li>
            </ul>
            <div class="card-body">
              <a href="#" class="card-link">Card link</a>
              <a href="#" class="card-link">Another link</a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card" style="width: 18rem;">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
            <ul class="list-group list-group-flush">
              <li class="list-group-item">Cras justo odio</li>
              <li class="list-group-item">Dapibus ac facilisis in</li>
              <li class="list-group-item">Vestibulum at eros</li>
            </ul>
            <div class="card-body">
              <a href="#" class="card-link">Card link</a>
              <a href="#" class="card-link">Another link</a>
            </div>
          </div>
        </div>
      </div>
    </div>


      <div class="row justify-content-center  d-none align-items-center p-5"> <!--d-flex-->
        <div class="col-md-4">
          <div class="container mr-auto ml-auto">
            <div class="row text-left">
              <div class="col mb-3"><a href="#" class="mt-auto mb-auto text-white"> <img src="<?php echo base_url('assets/my/icon/P1-Lighticons-full-109 copy.png')?>" style="width: 45px;" class="align-self-center mr-1" alt="..."> Iuran Pembayaran Siswa</a></div>
              <div class="w-100"></div>
              <div class="col"><a href="#" class="mt-auto mb-auto text-white"> <img src="<?php echo base_url('assets/my/icon/P1-Lighticons-full-109 copy.png')?>" style="width: 45px;" class="align-self-center mr-1" alt="..."> Iuran DPMP Siswa</a></div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="container mr-auto ml-auto">
            <div class="row text-left">
              <div class="col mb-3"><a href="#" class="mt-auto mb-auto text-white"> <img src="<?php echo base_url('assets/my/icon/P1-Lighticons-full-109 copy.png')?>" style="width: 45px;" class="align-self-center mr-1" alt="..."> Iuran Pembayaran Siswa</a></div>
              <div class="w-100"></div>
              <div class="col"><a href="#" class="mt-auto mb-auto text-white"> <img src="<?php echo base_url('assets/my/icon/P1-Lighticons-full-109 copy.png')?>" style="width: 45px;" class="align-self-center mr-1" alt="..."> Iuran DPMP Siswa</a></div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="container mr-auto ml-auto">
            <div class="row text-left">
              <div class="col mb-3"><a href="#" class="mt-auto mb-auto text-white"> <img src="<?php echo base_url('assets/my/icon/P1-Lighticons-full-109 copy.png')?>" style="width: 45px;" class="align-self-center mr-1" alt="..."> Iuran Pembayaran Siswa</a></div>
              <div class="w-100"></div>
              <div class="col"><a href="#" class="mt-auto mb-auto text-white"> <img src="<?php echo base_url('assets/my/icon/P1-Lighticons-full-109 copy.png')?>" style="width: 45px;" class="align-self-center mr-1" alt="..."> Iuran DPMP Siswa</a></div>
            </div>
          </div>
        </div>
      </div>

      <div class="row justify-content-center  d-none align-items-center p-5"> <!--d-flex-->
        <div class="col-md-4">
          <!-- <h1 class="card-title text-center text-white mb-4 font-main">IURAN</h1>
           -->
          <div class="mt-4">
            <div class="row">
              <a href="" class="col-md-12 mr-4 text-white-href" style="text-align: center;text-decoration: none;"><i class="fa fa-dollar fa-2x" aria-hidden="true"></i> Iuran Pembayaran Siswa</a>
            </div>
            <div class="row">
            <a href="" class="col-md-12 mr-4 text-white-href" style="text-align: center;text-decoration: none;"><i class="fa fa-shopping-bag fa-2x" aria-hidden="true"></i> Iuran DPMP Siswa</a>
            </div>
                
          </div>  
        </div>

        <div class="col-md-4">
          <div class="mt-4 text-center">
                <a href="" class="mr-4 text-white-href" style="text-align: center;text-decoration: none;"><i class="fa fa-home fa-4x" aria-hidden="true"></i>LAPORAN</a>
          </div>  
        </div>

        <div class="col-md-4">
          <div class="mt-4 text-center">
                <a href="" class="mr-4 text-white-href" style="text-align: center;text-decoration: none;"><i class="fa fa-home fa-4x" aria-hidden="true"></i> PENGATURAN</a>
          </div>  
        </div>
        <!-- <div class="col-md-4">
          <h1 class="card-title text-center text-white mb-4 font-main">PENGATURAN</h1>
          <div class="mt-4 text-center">
                <button type="button" class="btn btn-lg bg-head pill pl-5 pr-5">PENGATURAN</button>
          </div>  
        </div> -->
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

  </body>
</html>