<?php $this->load->view('inc/header_khusus');?>

  <div class="pt-2">

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color:transparent;">
        <li class="breadcrumb-item"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Kembali</button></li>

        <li class="breadcrumb-item" aria-current="page">

            <!-- <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-hand-holding-usd fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span></a> -->

              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-users fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Siswa</span>
              </a>
        </li>
       <!--  <li class="breadcrumb-item" aria-current="page">
              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-suitcase fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span>
              </a>
        </li> -->
      </ol>
    </nav>

  </div>



  <div class="row justify-content-center">
  <div class="col-md-6">
    <form id="search-form">
      <div class="form-group">
      <div class="input-group input-group-lg">
      <div class="input-group-prepend">
        <span class="input-group-text mr-1">Tahun/Kelas</span>
      </div>
      <select name="filter_tahun_ajaran" class="form-control select-styles">
          <option value="">Pilih Tahun Ajaran</option>
      </select>

      <select name="filter_kelas" class="form-control select-styles ml-1">
          <option value="">Pilih Kelas</option>
      </select>
      <div class="input-group-append ml-1">
        <button class="btn btn-secondary" type="submit" id="btn-cari">Cari/Filter</button>
      </div>
      </div>
      </div> 
    </form>
  </div>
  </div>

            <div class="row justify-content-center">
              <div class="col-sm-12">

          <div class="card bg-head pill">            
            <div class="card-body">
            <h3 class="card-title">
            <div class="row">
              <div class="col-md-6"> <span style="font-family: segoeui;">Data Siswa</span></div>
              <div class="col-md-6"><button onclick="readForm();" type="button" class="btn btn-light btn-lg btn-light pill pl-5 pr-5 float-right">Pindah Kelas</button></div>
              <!-- <div class="col-md-6">
                <a href="<?php echo base_url('page/form_siswa_view') ?>" type="" class="btn btn-light btn-lg btn-light pill pl-5 pr-5 float-right">Tambah</a>
              </div> -->
            </div>

            </h3>
              
            <hr/>

            <div class="row">
              <div class="col-sm-3">
                <!-- <div class="wrap"> -->
                  <label>Tahun Ajaran</label>
                <select name="filter_tahun_ajaran" class="form-control select-styles mb-3">
                    <option value="">Pilih Tahun Ajaran</option>
                </select>

                <div class="">
                  <table id="tbl-data-kelas" class="table-responsive card-list-table table-pill" style="font-family: segoeui;opacity: 0.8;display: table;">
                    <thead class="text-white" style="background-color: #487d95!important;">
                      <tr>
                        <th>Daftar KELAS</th>
                      </tr>
                    </thead>
                    <tbody class="text-dark">
                    </tbody>
                  </table>
                </div>
                <!-- </div> -->
              </div>

              <div class="col-sm-9">

              <div class="">
              <table id="tbl-data" class="table-responsive card-list-table table-pill" style="font-family: segoeui;opacity: 0.8;display: table;">
              <thead class="text-white" style="background-color: #487d95!important;">
              <tr>
                <th width="7%">#</th>
                <th width="10%">No.REF</th>
                <th>Nama Lengkap</th>
                <th>Kelas/Tahun Ajaran<span class="bg-success badge-pill">(NEW)</span></th>
                <th>Kelas/Tahun Ajaran<span class="bg-danger badge-pill">(OLD)</span></th>
                <th>Jenis Kelamin</th>
                <th style="width:125px;"></th>
              </tr>
              </thead>
              <tbody class="text-dark">
              </tbody>
              </table>
              </div>
              </div>
            </div>

            </div>
          </div>
        </div>
      </div>

          <!-- <div class="card bg-login pill p-5">
            <h3 class="card-header text-white d-none">Entri Iuran Siswa</h3>
            <div class="card-body">
            <h3 class="card-title text-white">Entri Iuran Siswa</h3>
              
            <hr/>

            <form action="<?php echo base_url('PageController') ?>" >
                <div class="form-group row">                  
                  <div class="col-sm-6">
                    <select class="form-control select-auto-complete" id="">
                      <option>Ketik Nama Siswa</option>
                    </select>
                  </div>              
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="" placeholder="Tanggal Pembayaran">
                  </div>
                </div>
                <div class="form-group row">  
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="" placeholder="Bulan Iuran">
                  </div>  
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="" placeholder="Jumlah Iuran">
                  </div>
                </div>
                <div class="form-group row">                  
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="" placeholder="Jumlah Iuran DPMP">
                  </div>       
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="" placeholder="Jumlah Iuran Daftar Ulang">
                  </div>
                </div>
                <div class="form-group row">         
                  <div class="col-sm-12">
                    <textarea class="form-control" id="" rows="5" placeholder="Keterangan"></textarea>
                  </div>
                </div>

                <button id="btn-submit" type="submit" class="btn btn-lg btn-secondary pill pl-5 pr-5 float-right">SUBMIT</button>
              </form>

            </div>
          </div> -->
    </div>

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md pill text-white" role="document">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="font-family: segoeuib">Form Siswa</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form-siswa" action="<?php echo base_url('page/form_siswa_view') ?>">
              <div class="form-row justify-content-center">
                <div class="form-group col-md-8">
                  <label for="">Jumlah Siswa yang akan di entri</label>
                  <input name="jumlah_siswa" type="text" class="form-control" id=""  placeholder="Masukkan Jumlah Data">
                </div>
                
               <!--  <div class="form-group col-md-6">
                  <label>Angakatan</label>
                  <select name="angaktan" class="form-control select-style">
                      <option value="0">Pilih Angkatan</option>
                      <option value="2019">2019</option>
                      <option value="2020">2020</option>
                      <option value="2021">2021</option>
                  </select>
                </div> -->
              </div>    

              <!-- <div class="form-row">
                  <div class="form-group col-3">
                      <label>Kelas</label>
                      <select name="kelas" class="form-control select-style">
                          <option value="">Pilih Kelas</option>
                          <option value="X">X</option>
                          <option value="XI">XI</option>
                          <option value="XII">XII</option>
                      </select>
                  </div>
                  <div class="form-group col-6">
                      <label>Jurusan</label>
                      <select name="jurusan" class="form-control select-style">
                          <option value="">Pilih Jurusan</option>
                          <option value="IPS">IPS</option>
                          <option value="MIPA">MIPA</option>
                          <option value="BAHASA">BAHASA</option>
                      </select>
                  </div>
                  <div class="form-group col-3">
                      <label>Sub Kelas</label>
                      <select name="sub_kelas" class="form-control select-style">
                          <option value="0">Pilih</option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                      </select>
                  </div>
              </div>   

              <div class="form-row">
                  <div class="form-group col-md-12">
                      <label>Wali Kelas</label>
                       <input name="" type="text" class="form-control" id=""  placeholder="Cecep Supriatna" readonly>
                  </div>
              </div>  -->

              <button id="btn-submit-2" type="submit" class="btn btn-lg btn-secondary pill pl-5 pr-5 float-right d-none">SUBMIT</button>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5" data-dismiss="modal">CLOSE</button>
            <button onclick="$('#btn-submit-2').click()" type="button" class="btn btn-secondary btn-lg btn-secondary pill pl-5 pr-5">SUBMIT</button>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script src="<?php echo base_url('assets/js/siswa-kelas-script.js')?>"></script>

  </body>
</html>