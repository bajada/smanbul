<?php $this->load->view('inc/header');?>

  <div class="pt-2">

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color:transparent;">
        <li class="breadcrumb-item"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Kembali</button></li>

        <li class="breadcrumb-item" aria-current="page">

            <!-- <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-hand-holding-usd fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span></a> -->

              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-users fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Siswa</span>
              </a>
        </li>
      </ol>
    </nav>

  </div>



    <div class="row justify-content-center">
      <div class="col-md-12">

  <div class="card bg-head pill ">
    <div class="card-header text-white" style="background-color: #487d95!important;font-family: segoeui;">
    <div class="row">
      <div class="col-md-7"> <h5 style="font-family: segoeui;">Konfirmasi Siswa Naik Kelas</h5></div>
      <div class="col-md-3 offset-2">
        <button onclick="save();" type="button" class="btn btn-secondary pill pl-4 pr-4 float-right mr-2">SUBMIT</button>
      </div>
    </div>
    </div>

    <div class="card-body" style="height: 540px; overflow-x: scroll; overflow-y: scroll;">

    <!-- <form id="search-form">
      <div class="form-group row">
        <div class="col-md-4">
          <input onkeyup="display();" name="filter_keyword" type="text" class="form-control" autocomplete="off" placeholder="Ketik Nama/No.Ref/Peminatan Jurusan">
        </div>
        <div class="col-md-2 offset-4">
            <select name="filter_kelas" class="form-control" onchange="display()">
                <option value="0">Semua Kelas</option>
                <option value="X">X</option>
                <option value="XI">XI</option>
                <option value="XII">XII</option>
            </select> 
        </div>
        <div class="col-md-2">
            <select name="filter_tahun_ajaran" onchange="filter();" id="tahun_ajaran" class="form-control">
              <option>Semua Tahun Ajaran</option>
            </select>
        </div>
      </div>
    </form> -->

    <div class="row">
      
      <div class="col-12">
        <div class="tab-content" id="v-pills-tabContent">
          <div class="tab-pane fade show active" id="v-pills-kelas-none">
                <form id="form-data">
                <div class="row">
                  <div class="col-md-12 pill-top table-responsive">
                  <table id="tbl-data" class="table table-striped table-bordered table-sm" style="font-family: segoeui;opacity: 0.8;display: table; width: 140%" >
                    <thead class="text-white" style="background-color: #487d95!important;">
                      <tr id="sub-du-1">
                        <th rowspan="2"></th>
                        <!-- <th width="8%">No</th> -->
                        <th rowspan="2" class="align-middle">No.REF</th>
                        <th rowspan="2" class="align-middle">Nama Lengkap</th>
                        <th rowspan="2" class="align-middle">JK</th>
                        <th rowspan="2" class="align-middle">Tahun Ajaran</th>
                        <th rowspan="2" class="align-middle">Angkatan</th>
                        <!-- <th rowspan="2" class="align-middle">Registrasi</th> -->
                        <th rowspan="2" class="text-danger align-middle">Kelas Lama</th>
                        <th rowspan="2" class="text-success align-middle">Kelas Baru</th>
                        <!-- <td colspan="3" class="text-center">DAFTAR ULANG</td>
                        <td colspan="" class="text-center">Rincian Iuran</td> -->

                      </tr>
                      <tr id="sub-du">
                        <!-- <th width="1%">SPP JULI</th>
                        <th width="1%">TABUNGAN JULI</th>
                        <th width="1%">OSIS</th> -->
                      </tr>
                    </thead>
                    <tbody class="text-dark">
                    </tbody>
                  </table>
                  </div>
                </div>

                <!---here before-->
                </form>
          </div>
        </div>
      </div>
      
    </div>

    </div>
    <div class="card-footer">
      <div class="row no-gutters px-1 py-3 align-items-center">
          <div class="col pl-3">
              <div class="dataTables_info" id="info-tabel">1 to 8 Items of 11 — <a href="#!" class="font-weight-semi-bold"> view all <span class="fa fa-angle-right"></span></a>
              </div>
          </div>
          <div class="col-auto pr-3 d-none">
              <div class="dataTables_paginate paging_simple" id="DataTables_Table_0_paginate">
                  <ul class="pagination pagination-sm">
                      <li class="paginate_button page-item previous disabled" id="DataTables_Table_0_previous"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>
                      <li class="paginate_button page-item next" id="DataTables_Table_0_next"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0" class="page-link">Next</a></li>
                  </ul>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
</div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script type="text/javascript">
      var arr_siswa_kelas = [<?php echo '"'.implode('","', $_POST["id"]).'"' ?>];
      var list_obj = "<?php echo $_POST["list_obj"]; ?>";
      var kelas_baru = list_obj.split(",")[0];
      var tingkat_kelas = "<?php echo $_POST["tingkat_kelas"]; ?>";
      var tahun_ajaran_baru = list_obj.split(",")[1];
      console.log(arr_siswa_kelas);
      console.log(kelas_baru);
    </script>
    <script src="<?php echo base_url('assets/js/form-siswa-naik-kelas-script.js?yesss')?>"></script> 
  </body>
</html>