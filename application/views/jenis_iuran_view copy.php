<?php $this->load->view('inc/header_khusus');?>

    <div class="container-fluid mt-3">

          <!-- <h1 class="card-title text-white mb-4 font-main">LOGIN</h1> -->
          <div class="pt-2">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb" style="background-color:transparent;">
                <li class="breadcrumb-item"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">IURAN</button></li>

                <li class="breadcrumb-item" aria-current="page">
                      <a href="" class="text-white-href" style="display: inline-flex;">
                      <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-dollar fa-stack-1x fa-inverse text-muted"></i>
                      </span>
                      <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran SPP</span>
                      </a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                      <a href="" class="text-white-href" style="display: inline-flex;">
                      <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-suitcase fa-stack-1x fa-inverse text-muted"></i>
                      </span>
                      <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran DPMP</span>
                      </a>
                </li>
              </ol>
            </nav>

          </div>

    <div class="row justify-content-center">
      <div class="col-md-10">
        <div class="card bg-head pill">
          <!-- <h5 class="card-header bg-dark text-white">Data</h5> -->
          <div class="card-body">
            <h5 class="card-title text-white" style="font-family: segoeuib">Jenis Iuran</h5>
            <hr/>
           <!--  <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a> -->

          <!--   <form id="search-form">
            <div class="form-group row">
              <div class="col-sm-4">
                  <div class="input-group ">
                    <input name="filter_keyword" type="text" class="form-control" placeholder="Ketik Parameter Pencarian">
                    <div class="input-group-append">
                      <button class="btn btn-secondary pill-right" type="submit">Cari</button>
                    </div>
                  </div>
              </div>
                    <div class="col-sm-8 text-right">
                      <div class="btn-group" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-secondary pl-5 pr-5 pill-right pill-left btn-lg" onclick="add();">
                          Tambah
                        </button>
                      </div>
                    </div>
            </div>  
            </form> -->

            <div class="row">
              <div class="col-md-3">
                <table id="tbl-data-kelas" class="table-responsive card-list-table table-pill" style="font-family: segoeui;opacity: 0.8;display: table;">
                    <thead class="text-white" style="background-color: #487d95!important;">
                      <tr>
                        <th>KELAS</th>
                        <th><button onclick="add();" class="btn btn-default btn-sm float-right pill">+</button></th>
                      </tr>
                    </thead>
                    <tbody class="text-dark">
                      <tr data-id="X" onclick="display()"><td>X</td><td><span class="badge badge-pill badge-secondary pull-right">-</span></td></tr>
                      <tr data-id="XI" onclick="display()"><td>XI</td><td> <span class="badge badge-pill badge-secondary pull-right">-</span></td></tr>
                      <tr data-id="XI" onclick="display()"><td>XII</td><td><span class="badge badge-pill badge-secondary pull-right">-</span></td></tr>
                    </tbody>
                  </table>
            </div>
              <div class="col-sm-9">
                <table id="tbl-data" class="table table-bordereds table-striped table-pill table-hover" >
                  <thead class="thead-lights text-white" style="background-color: #487d95!important;font-family: segoeui;">
                    <tr>
                        <th width="10px">No</th>
                        <th>Jenis Iuran</th>
                        <th>Besar Iuran</th>
                        <th>Keterangan</th>
                        <th style="width:125px;"></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog pill text-white" role="document">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="font-family: segoeui;">Form Jenis IURAN</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form">  

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>Jenis Iuran</label>
                    <input name="nama" type="text" class="form-control" id="" placeholder="Jenis IURAN">
                </div>
            </div> 

            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="">Besar IURAN</label>
                <input name="besar" type="number" class="form-control" id=""  placeholder="Jumlah IURAN">
              </div>
            </div>   

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>Keterangan</label>
                    <textarea name="Keterangan" class="form-control" rows="3"></textarea> 
                </div>
            </div> 


            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5" data-dismiss="modal">CLOSE</button>
            <button id="btn-submit" type="button" class="btn btn-light btn-lg btn-secondary pill pl-5 pr-5 text-white" onclick="save()">SUBMIT</button>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script src="<?php echo base_url('assets/js/jenis-iuran-script.js')?>"></script> 
</body>

</html>