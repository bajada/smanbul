<?php $this->load->view('inc/header_khusus');?>

  <div class="pt-2">

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color:transparent;">
        <li class="breadcrumb-item"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Kembali</button></li>

        <li class="breadcrumb-item" aria-current="page">

            <!-- <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-hand-holding-usd fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span></a> -->

              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-users fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Siswa</span>
              </a>
        </li>
       <!--  <li class="breadcrumb-item" aria-current="page">
              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-suitcase fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span>
              </a>
        </li> -->
      </ol>
    </nav>

  </div>



    <div class="row justify-content-center">
      <div class="col-sm-12">

  <div class="card bg-head pill p-1 ">            
    <div class="card-body">
    <h3 class="card-title">
    <div class="row">
      <div class="col-md-6"> <span style="font-family: segoeui;">Data Siswa</span></div>
      <div class="col-md-6"><button onclick="$('#modal-form').modal('show');" type="button" class="btn btn-light btn-lg btn-light pill pl-5 pr-5 float-right">Tambah</button> <button onclick="goToEdit()" type="button" class="btn btn-light btn-lg btn-light pill pl-5 pr-5 float-right mr-1">Action</button></div>
      <!-- <div class="col-md-6">
        <a href="<?php echo base_url('page/form_siswa_view') ?>" type="" class="btn btn-light btn-lg btn-light pill pl-5 pr-5 float-right">Tambah</a>
      </div> -->
    </div>

    </h3>
      
    <hr/>

    <div class="row">
      <div class="col-3">
        <div class="row">
          <div class="col-md-11 pill-top p-3" style="background-color: #487d95!important;font-family: segoeui;opacity: 0.8;">
            <span class="col-form-label">Kelas <select onchange="filter();" id="tahun_ajaran" class="float-right"><option>Pilih Tahun Ajaran</option></select></span>
          </div>
        </div>

        <div class="row">
          <div class="col-md-11 pill-top p-0" style="font-family: segoeui;opacity: 0.8;">
          <table id="tbl-data-kelas" class="table-responsive card-list-table table-pills" style="font-family: segoeui;opacity: 0.8;display: table;">
            <tbody class="text-dark">
              <tr data-tingkat="ALL" class="" onclick="filter('tingkat_kelas=ALL&');"><td>SEMUA KELAS</td></tr>
              <tr data-tingkat="X" class="" onclick="filter('tingkat_kelas=X&');"><td>X</td></tr>
              <tr data-tingkat="XI" class="" onclick="filter('tingkat_kelas=XI&');"><td>XI</td></tr>
              <tr data-tingkat="XII" class="" onclick="filter('tingkat_kelas=XII&');"><td>XII</td></tr>
              <tr data-tingkat="ALUMNI" class="" onclick="filter('tingkat_kelas=ALUMNI&');"><td>ALUMNI</td></tr>
            </tbody>
          </table>
        </div>
        </div>
      </div>
      <div class="col-9">
        <div class="tab-content" id="v-pills-tabContent">
          <div class="tab-pane fade show active" id="v-pills-kelas-none">
          <div class="row">
              <div class="col-sm-12">

              <div class="wrap">
              <div class="">
                <div class="row">
                  <div class="col-md-12 pill-top p-3" style="background-color: #487d95!important;font-family: segoeui;opacity: 0.8;">
                    <form>
                      <div class="row">
                        <div class="col-md-1">
                          <label class="col-form-label">Filter :</label>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control" placeholder="Ketik nama/ref siswa">
                        </div>
                        <div class="col-md-3">
                          <select class="form-control" name="kelas">
                            <option>Pilih Jurusan/Peminatan</option>
                          </select>
                        </div>
                      </div>
                    </form>

                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 pill-top p-0">
                  <table id="tbl-data" class="table-responsive card-list-table" style="font-family: segoeui;opacity: 0.8;display: table;">
                    <thead class="text-white" style="background-color: #487d95!important;">
                      <tr>
                        <th width="4%"></th>
                        <!-- <th width="8%">No</th> -->
                        <th width="10%">No.REF</th>
                        <th>Nama Lengkap</th>
                        <th>Kelas</th>
                        <th>Jenis Kelamin</th>
                        <th>Tahun Ajaran</th>
                        <th>Angkatan</th>
                        <th style="width:125px;"></th>
                      </tr>
                    </thead>
                    <tbody class="text-dark">
                      <tr>
                        <th colspan="8" class="text-center p-3">-- Pilih kelas terlebih dahulu --</th>
                      </tr>
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    </div>
  </div>
</div>
</div>
    </div>

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md pill text-white" role="document" styles="max-width: 700px">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="font-family: segoeuib">Tambah Siswa Baru</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form-siswa" action="<?php echo base_url('page/form_siswa_view') ?>">
              <div class="form-row justify-content-center">
                <div class="form-group col-md-6">
                  <label for="">Jumlah Siswa</label>
                  <input name="jumlah_siswa" type="text" class="form-control" id=""  placeholder="Masukkan Jumlah Data">
                </div>
                
               <div class="form-group col-md-6">
                  <label>Kelas</label>
                  <select name="kelas" class="form-control select-styles">
                      <option value="0">Pilih kelas</option>
                  </select>
                </div> 
              </div>    

             <!--  <div class="form-row d-none">
                  <div class="form-group col-3">
                      <label>Kelas</label>
                      <select name="kelas" class="form-control select-style">
                          <option value="">Pilih Kelas</option>
                          <option value="X">X</option>
                          <option value="XI">XI</option>
                          <option value="XII">XII</option>
                      </select>
                  </div>
                  <div class="form-group col-6">
                      <label>Jurusan</label>
                      <select name="jurusan" class="form-control select-style">
                          <option value="">Pilih Jurusan</option>
                          <option value="IPS">IPS</option>
                          <option value="MIPA">MIPA</option>
                          <option value="BAHASA">BAHASA</option>
                      </select>
                  </div>
                  <div class="form-group col-3">
                      <label>Sub Kelas</label>
                      <select name="sub_kelas" class="form-control select-style">
                          <option value="0">Pilih</option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                      </select>
                  </div>
              </div>    -->

              <!-- <div class="form-row">
                  <div class="form-group col-md-12">
                      <label>Wali Kelas</label>
                       <input name="" type="text" class="form-control" id=""  placeholder="Cecep Supriatna" readonly>
                  </div>
              </div>  -->

              <button id="btn-submit-2" type="submit" class="btn btn-lg btn-secondary pill pl-5 pr-5 float-right d-none">SUBMIT</button>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5" data-dismiss="modal">CLOSE</button>
            <button onclick="$('#btn-submit-2').click()" type="button" class="btn btn-secondary btn-lg btn-secondary pill pl-5 pr-5">SUBMIT</button>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script src="<?php echo base_url('assets/js/siswa-script.js')?>"></script> 

  </body>
</html>