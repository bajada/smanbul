<?php $this->load->view('inc/header');?>
  
    <div class="container-fluid mt-3">

      <!-- <div class="media bg-head pill pl-5 pt-3  pb-3">
        <img src="http://sman1-cibungbulang.sch.id/assets/img/smanbul.png" style="width: 100px;" class="ml-3 mr-3" alt="GAMBAR HERE">
        <div class="media-body mt-auto mb-auto">
          <h2 class="font-main">Sistem Informasi Pembayaran</h2>
          <h3 class="font-main">SMAN 1 CIBUNGBULANG</h3>
        </div>
        <div class="pr-5 pt-3  pb-3">
          <a href="<?php echo base_url('page/dashboard_view') ?>" class="mr-4 text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-home fa-4x" aria-hidden="true"></i> Home</a>
          <a href="<?php echo base_url('page/login_view') ?>" class="text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-globe fa-4x" aria-hidden="true"></i> Website</a>
        </div>
      </div> -->

          <!-- <h1 class="card-title text-white mb-4 font-main">LOGIN</h1> -->
          <div class="pt-2">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb" style="background-color:transparent;">
                <li class="breadcrumb-item"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Kembali</button></li>

                <li class="breadcrumb-item" aria-current="page">
                      <a href="" class="text-white-href" style="display: inline-flex;">
                      <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-dollar fa-stack-1x fa-inverse text-muted"></i>
                      </span>
                      <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Bulanan/DPMP/DU</span>
                      </a>
                </li>
              </ol>
            </nav>

          </div>

          <div class="row justify-content-center">
          <div class="col-md-6">
            <form id="search-form">
            <div class="form-group">
            <div class="input-group input-group-lg">
            <div class="input-group-prepend">
              <span class="input-group-text pill-left pr-4 pl-4">NO REF</span>
            </div>
            <input name="filter_keyword" id="txtsearch" size="50" type="text" class="form-control input-sm" autocomplete="off" placeholder="Ketik NO.REF Untuk melakukan pencarian siswa">
            <div class="input-group-append">
              <button class="btn btn-secondary pill-right pl-4 pr-4" type="submit" id="btn-cari">Cari</button>
            </div>
            </div>
            </div> 
            </form>
          </div>
          </div>

          <div class="row justify-content-center">
          <div class="col-md-4">
          <div class="card bg-head pill p-1">
            <div class="card-body">
            <h4 class="card-title" style="font-family: segoeui;">Informasi Siswa</h4>
            <hr/>
            <form action="<?php echo base_url('PageController') ?>" >
              

              <div class="form-group row">
                <div class="col-md-12 text-center">
                  <div>
                    <a href="" class="text-white-href">
                    <span class="fa-stack fa-5x">
                      <i class="fas fa-circle fa-stack-2x"></i>
                      <i class="fas fa-user fa-stack-1x fa-inverse text-muted"></i>
                    </span>
                    </a>
                  </div>
                </div>
              </div>
              <div class="form-group row border-bottom">
                <label for="" class="col-md-4">No.REF</label>
                <div class="col-md-8">
                  <label id="lbl_no_ref" for="" style="font-weight: 100;">: </label>
                </div>
              </div>
              <div class="form-group row border-bottom">
                <label for="" class="col-md-4">Nama Siswa</label>
                <div class="col-md-8">
                  <label id="lbl_nama_lengkap" for="" style="font-weight: 100;">: </label>
                </div>
              </div>

              <div class="form-group row border-bottom">
                <label for="" class="col-md-4">Kelas</label>
                <div class="col-md-8">
                  <label id="lbl_kelas" for="" style="font-weight: 100;">: </label>
                </div>
              </div>

              <div class="form-group row border-bottom">
                <label for="" class="col-md-4">Tahun Ajaran</label>
                <div class="col-md-8">
                  <label id="lbl_tahun_ajaran" for="" style="font-weight: 100;">: </label>
                </div>
              </div>

              <div class="form-group row border-bottom d-none">
                <label for="" class="col-md-4">Angkatan</label>
                <div class="col-md-8">
                  <label id="lbl_tahun_angkatan" for="" style="font-weight: 100;">: </label>
                </div>
              </div>

              <div class="form-group row border-bottom">
                <label for="" class="col-md-4">Tgl.Registrasi</label>
                <div class="col-md-8">
                  <label id="lbl_tanggal_registrasi" for="" style="font-weight: 100;">: </label>
                </div>
              </div>
             <!--  <a href="" class="small" style="font-family: segoeuib;color: yellow">*siswa mempunyai tunggakan iuran,pada tahun ajaran sebelumnya</a><br/> -->
              <!-- <small class="" style="color:yellow" onclick=""></small> -->

              <a id="lbl_tunggakan" href="javascript:void(0)" class="d-none"><small class="" style="color:yellow"  onclick="readDaftarTunggakan();">*siswa memiliki tunggakan iuran,klik untuk melihat detail</small></a>
            </form>

            </div>
          </div>

          <div class="card bg-head pill p-1 mt-3">
            <div class="card-body pb-0">
            <h4 class="card-title" style="font-family: segoeui;">Rincian Biaya</h4>
            <hr/>
            <form action="<?php echo base_url('PageController') ?>" >
              <div class="form-group row">
                <label for="" class="col-md-4">Biaya Iuran</label>
                <div class="col-md-8">
                  <label class="" style="font-weight: 100;" id="lbl_biaya_iuran_per_bulan">: 0</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-4">Biaya DPMP</label>
                <div class="col-md-8">
                  <label class="" style="font-weight: 100;" id="lbl_biaya_dpmp">: 0</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-4">Biaya DU</label>
                <div class="col-md-8">
                  <label class="" style="font-weight: 100;" id="lbl_biaya_du">: 0</label>
                </div>
              </div>
              <hr/>

              <div class="form-group row">
                <label for="" style="font-family: segoeuib" class="col-md-4">TOTAL</label>
                <div class="col-md-8">
                  <label for="" style="font-family: segoeuib" id="lbl_total_biaya">: 0</label>
                </div>
              </div>

            </form>

            </div>
          </div>
          <?php if(isset($_GET["id"]) != '') { ?>
          <div class="form-group text-center mt-2 row">
            <div class="col">
              <button id="btn-simpan" class="btn btn-light btn-lg btn-block bg-head pill pl-5 pr-5"><i class="fa fa-save"></i> SIMPAN</button>
            </div>
            <div class="col">
              <!--<a href="<php echo base_url('IuranSiswaController/CETAK') ?>" class="btn btn-light btn-lg btn-block bg-head pill pl-5 pr-5"><i class="fa fa-print"></i> CETAK</a>-->
              <button onclick="$('#modal-form-cetak').modal('show')" class="btn btn-light btn-lg btn-block bg-head pill pl-5 pr-5"><i class="fa fa-print"></i> CETAK</button>
            </div>
            <small class="text-white text-hide">*Klik Tombol 'SUBMIT' apabila telah selesai.</small>
          </div>
          <?php } ?>

          </div>

          <div class="col-md-8">
          <div class="card bg-head pill p-1">
            <div class="card-body">
            <h4 class="card-title" style="font-family: segoeui;">Biaya Iuran Siswa(<span id="lbl_iuran_status"></span>)</h4>
            <hr/>

            <table id="tbl-data" class="table table-sm table-hovered table-pill text-dark" style="opacity: 0.9;">
              <thead class="text-white" style="background-color: #408382!important;font-family: segoeui;">
                <tr>
                  <!-- <th scope="col" class="p-3">No.</th> -->
                  <th scope="col" class="text-center p-3">#</th>
                  <th scope="col" class="p-3">Bulan</th>
                  <th scope="col" class="p-3 d-none">Jatuh Tempo</th>
                  <th scope="col" class="p-3">Besar Iuran</th>
                  <th scope="col" class="p-3">Jumlah Iuran</th>
                  <th scope="col" class="p-3" class="text-left" >Tgl.Iuran</th>
                  <th scope="col" class="p-3">Keterangan</th>
                  <th scope="col" class="p-3">Petugas</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
            <small class="pull-right">*Klik Tombol 'Centang' untuk memilih.</small>

            </div>
          </div>

          <div class="card bg-head pill p-1 mt-2">
            <div class="card-body pb-0">
            <h4 class="card-title" style="font-family: segoeui;">Biaya DPMP Siswa(<span id="lbl_dpmp_status"></span>)
            <a href="javascript:void(0)"><small class="float-right " style="color:yellow"  onclick="readHistoryPayment('DPMP');">Riwayat Pembayaran</small></a>
            </h4>
            <hr/>
            <form>
              
              <div class="form-group row">
                <div class="col-md-6">
                    <label>Nominal DPMP</label>
                    <input name="nominal_dpmp" type="text" data-type='currency' pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" class="form-control" placeholder="Masukkan Nominal Rupiah yang diterima" >
                </div>
                <div class="col-md-6">
                  <div class="form-group row">
                    <label for="" class="col-md-5">Besar DPMP</label>
                    <div class="col-md-7">
                      <label class="dollar" style="font-weight: 100;" id="lbl_besar_dpmp">: 0</label>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-md-5">Sisa DPMP</label>
                    <div class="col-md-7">
                      <label class="dollar" for="" style="font-weight: 100;" id="lbl_sisa_dpmp">: 0</label>
                    </div>
                  </div>
                </div>
              </div>
              
            </form>
            </div>
          </div>

          <div class="card bg-head pill p-1 mt-2 mb-2">
            <div class="card-body pb-0">
            <h4 class="card-title" style="font-family: segoeui;">Biaya DU Siswa(<span id="lbl_du_status"></span>)
            <a href="javascript:void(0)"><small class="float-right " style="color:yellow"  onclick="readHistoryPayment('DU');">Riwayat Pembayaran</small></a>
            </h4>
            <hr/>
            <form>
              
              <div class="form-group row">
                <div class="col-md-6">
                    <label>Nominal DU</label>

                    <!-- <div class="input-group input-group-lg">
                    <div class="input-group-prepend">
                      <span class="input-group-text pill-left">NO REF</span>
                    </div>
                    <input type="number" class="form-control" placeholder="Nominal Rupiah yang diterima">
                    </div> -->
                    <input name="nominal_du" type="text" data-type='currency' class="form-control" placeholder="Masukkan Nominal Rupiah yang diterima">
                </div>
                <div class="col-md-6">
                  <div class="form-group row">
                    <label for="" class="col-md-5">Besar DU</label>
                    <div class="col-md-7">
                      <label class="dollar" for="" style="font-weight: 100;" id="lbl_besar_du">: 0</label>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-md-5">Sisa DU</label>
                    <div class="col-md-7">
                      <label class="dollar" for="" style="font-weight: 100;" id="lbl_sisa_du">: 0</label>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            </div>
          </div>
          </div>
          </div>
    </div>

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg pill text-white" role="document">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel" style="margin-left:auto;">INFORMASI IURAN SISWA</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form">
              <div class="form-group row">
                <label for="" class="col-md-3">Ref / Nama Siswa</label>
                <div class="col-md-9">
                  <label for="" style="font-weight: 100;">: 100101 - Adbuhraman</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Kelas</label>
                <div class="col-md-9">
                  <label for="" style="font-weight: 100;">: X-IPA-1</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Besar Iuran/Bulan</label>
                <div class="col-md-9">
                  <label for="" style="font-weight: 100;">: 150,000</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Iuran Terakhir</label>
                <div class="col-md-9">
                  <label id="view_detil_last_iuran" for="" style="font-weight: 100;" data-toggle="popover" title="Detail" data-content="Iuran terakhir adalah bulan ke 11">: 05-11-2018</label>
                </div>
              </div>
              <div class="form-group row">
              <label for="" class="col-md-3">Histori Iuran</label>
              <div class="col-md-9">
              <table class="table table-bordered table-sm">
              <thead class="bg-danger">
              <tr>
              <th scope="col">JUL</th>
              <th scope="col">AGU</th>
              <th scope="col">SEP</th>
              <th scope="col">OKT</th>
              <th scope="col">NOV</th>
              <th scope="col">DES</th>
              <th scope="col">JAN</th>
              <th scope="col">FEB</th>
              <th scope="col">MAR</th>
              <th scope="col">APR</th>
              <th scope="col">MEI</th>
              <th scope="col">JUN</th>
              </tr>
              </thead>
              <tbody class="bg-info">
              <tr>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>-</td>
              <td>-</td>
              </tr>
              </tbody>
              </table>
              </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Sisa Iuran</label>
                <div class="col-md-9">
                  <label for="" style="font-weight: 100;">: 2x</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Sisa DPMP</label>
                <div class="col-md-9">
                  <label for="" style="font-weight: 100;">: 1,000,000</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Sisa DU</label>
                <div class="col-md-9">
                  <label for="" style="font-weight: 100;">: 300,0000</label>
                </div>
              </div>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5" data-dismiss="modal">BATAL</button>
            <button onclick="" type="button" class="btn btn-light btn-lg btn-secondary pill pl-5 pr-5 text-white" data-dismiss="modal">LANJUT</button>
          </div>
        </div>
      </div>
    </div>


    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-form-cetak" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md pill text-white" role="document">
        <div class="modal-content bg-gradient-2 pill">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel" style="margin-left:auto;">Cetak INVOICE/KWITANSI</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form-cetak" action="<?php echo base_url('IuranSiswaController/cetak') ?>">
              <div class="row">
                <div class="col-6 form-group">
                    <label for="">Kode/No.Urut</label>
                    <input type="text" class="form-control" name="kode_transaksi" placeholder="Automatic" readonly="true">
                </div>
                <div class="col-6 form-group">
                    <label for="">Tanggal Cetak</label>
                    <input type="date" class="form-control" name="tanggal_transaksi">
                </div>
              </div>

              <div class="form-group">
                  <label for="">Mengetahui</label>
                  <input type="text" class="form-control" name="mengetahui" placeholder="Name Here">
              </div>

              <div class="form-group">
                  <label for="">Tahun Ajaran</label>
                  <?php 
                    if(isset($_GET["tahun_ajaran"]) && $_GET["tahun_ajaran"] != 'undefined') {
                      echo '<input type="text" class="form-control" name="tahun_ajaran" value='.$_GET["tahun_ajaran"].'>';
                    }else{
                      echo '<input type="text" class="form-control" name="tahun_ajaran">';
                    }
                  ?>
              </div>

              <!-- <div class="row">
                <div class="col-6 form-group">
                    <label for="">Mengetahui</label>
                    <input type="text" class="form-control" name="mengetahui" placeholder="Name Here">
                </div>
                <div class="col-6 form-group">
                    <label for="">Tanggal Cetak</label>
                    <input type="date" class="form-control" name="tanggal_cetak">
                </div>
              </div> -->
              <input type="hidden" name="id_transaksi" value="<?php echo $_GET['id']; ?>">
              <button type="submit" id="btn-cetak" hidden="true"></button>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5" data-dismiss="modal">BATAL</button>
            <button onclick="$('#btn-cetak').click();" type="button" class="btn btn-light btn-lg btn-secondary pill pl-5 pr-5 text-white" data-dismiss="modal">LANJUT</button>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script src="<?php echo base_url('assets/js/iuranv2-script.js?yess')?>"></script> 

  </body>
</html>