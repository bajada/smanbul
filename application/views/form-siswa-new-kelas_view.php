<?php $this->load->view('inc/header');?>

  <div class="pt-2">

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color:transparent;">
        <li class="breadcrumb-item"><a href="siswa_view" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Kembali</a></li>


        <li class="breadcrumb-item" aria-current="page">

            <!-- <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-hand-holding-usd fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span></a> -->

              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-users fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Siswa</span>
              </a>
        </li>
      </ol>
    </nav>

  </div>



    <div class="row justify-content-center">
      <div class="col-md-12">

  <div class="card bg-head pill ">
    <div class="card-header text-white" style="background-color: #487d95!important;font-family: segoeui;">
    <div class="row">
      <div class="col-md-7"> <h5 style="font-family: segoeui;">Form Tambah Siswa(Baru)</h5></div>
      <div class="col-md-3 offset-2">
        <button onclick="save();" type="button" class="btn btn-secondary pill pl-4 pr-4 float-right mr-2">SUBMIT</button>
      </div>
    </div>
    </div>

    <div class="card-body">

    <!-- <form id="search-form">
      <div class="form-group row">
        <div class="col-md-4">
          <input onkeyup="display();" name="filter_keyword" type="text" class="form-control" autocomplete="off" placeholder="Ketik Nama/No.Ref/Peminatan Jurusan">
        </div>
        <div class="col-md-2 offset-4">
            <select name="filter_kelas" class="form-control" onchange="display()">
                <option value="0">Semua Kelas</option>
                <option value="X">X</option>
                <option value="XI">XI</option>
                <option value="XII">XII</option>
            </select> 
        </div>
        <div class="col-md-2">
            <select name="filter_tahun_ajaran" onchange="filter();" id="tahun_ajaran" class="form-control">
              <option>Semua Tahun Ajaran</option>
            </select>
        </div>
      </div>
    </form> -->

    <div class="row">
      
      <div class="col-12">
        <div class="tab-content" id="v-pills-tabContent">
          <div class="tab-pane fade show active" id="v-pills-kelas-none">
                <form id="form-data">
                <div class="row">
                  <div class="col-md-12 pill-top table-responsive" style="height: 600px">
                  <table id="tbl-data" class="table table-striped table-bordered table-sm" style="font-family: segoeui;opacity: 0.8;display: table; width: 200%" >
                    <thead class="text-white" style="background-color: #487d95!important;">
                      <tr id="sub-du-1">
                        <th rowspan="2" width="1%" class="align-middle">No.</th>
                        <!-- <th width="8%">No</th> -->
                        <th rowspan="2" width="2%" class="align-middle">No.REF</th>
                        <th rowspan="2" width="5%" class="align-middle">Nama Lengkap</th>
                        <th rowspan="2" width="2%" class="align-middle">JK</th>
                        <th rowspan="2" width="2%" class="align-middle">Kelas</th>
                        <!-- <th rowspan="2" class="align-middle">Tahun Ajaran</th>
                        <th rowspan="2" class="align-middle">Angkatan</th>
                        <th rowspan="2" class="align-middle">Registrasi</th> -->
                        <!-- <td colspan="3" class="text-center">DAFTAR ULANG</td>
                        <td colspan="" class="text-center">Rincian Iuran</td> -->

                      </tr>
                      <tr id="sub-du">
                        <!-- <th width="1%">SPP JULI</th>
                        <th width="1%">TABUNGAN JULI</th>
                        <th width="1%">OSIS</th> -->
                      </tr>
                    </thead>
                    <tbody class="text-dark">
                    </tbody>
                  </table>
                  </div>
                </div>
                    <span id="jumlah_siswa">*Mohon periksa kembali data siswa yang akan naik kelas.</span>
                </form>
          </div>
        </div>
      </div>
      
    </div>

    </div>
  </div>
</div>
</div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script type="text/javascript">
      //var arr_siswa_kelas = [<php echo '"'.implode('","', $_POST["id"]).'"' ?>];
      var jumlah_siswa = <?php echo $_POST["jumlah_siswa"]; ?>;
      var kelas_siswa = <?php echo '"'.$_POST["kelas"].'"'; ?>;
      $('#jumlah_siswa').text("Total Row: " +jumlah_siswa);
      console.log(jumlah_siswa);
      console.log(kelas_siswa);
    </script>
    <script src="<?php echo base_url('assets/js/form-siswa-new-kelas-script.js?yes')?>"></script>
  </body>
</html>