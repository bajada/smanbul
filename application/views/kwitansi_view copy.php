<?php $this->load->view('inc/header');?>

    <div class="container-fluid mt-3">

          <!-- <h1 class="card-title text-white mb-4 font-main">LOGIN</h1> -->
          <div class="pt-2">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb" style="background-color:transparent;">
                <li class="breadcrumb-item"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Kembali</button></li>

                <li class="breadcrumb-item" aria-current="page">
                      <a href="" class="text-white-href" style="display: inline-flex;">
                      <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-money-check fa-stack-1x fa-inverse text-muted"></i>
                      </span>
                      <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Kwitansi</span>
                      </a>
                </li>
              </ol>
            </nav>

          </div>

    

          <div class="row justify-content-center">
          <div class="col-md-6">
            <form id="search-form">
            <div class="form-group">
            <div class="input-group input-group-lg">
            <div class="input-group-prepend">
              <span class="input-group-text pill-left pr-4 pl-4">NO REF</span>
            </div>
            <input name="filter_keyword" id="txtsearch" size="50" type="text" class="form-control input-sm" aria-label="..." placeholder="Ketik NO.REF Untuk melakukan pencarian siswa">
            <div class="input-group-append">
              <button class="btn btn-secondary pill-right pl-5 pr-5" type="submit" id="btn-cari">Cari</button>
            </div>
            </div>
            </div> 
            </form>
          </div>
          </div>

    <div class="row justify-content-center">
      <div class="col-md-4">

          <div class="card bg-head pill p-1">
            <div class="card-body">
            <h4 class="card-title" style="font-family: segoeui;">Informasi Siswa</h4>
            <hr/>
            <form action="<?php echo base_url('PageController') ?>" >
              

              <div class="form-group row">
                <div class="col-md-12 text-center">
                  <div>
                    <a href="" class="text-white-href">
                    <span class="fa-stack fa-5x">
                      <i class="fas fa-circle fa-stack-2x"></i>
                      <i class="fas fa-user fa-stack-1x fa-inverse text-muted"></i>
                    </span>
                    </a>
                  </div>
                </div>
              </div>
              <div class="form-group row border-bottom">
                <label for="" class="col-md-4">No.REF</label>
                <div class="col-md-8">
                  <label id="lbl_no_ref" for="" style="font-weight: 100;">: </label>
                </div>
              </div>
              <div class="form-group row border-bottom">
                <label for="" class="col-md-4">Nama Siswa</label>
                <div class="col-md-8">
                  <label id="lbl_nama_lengkap" for="" style="font-weight: 100;">: </label>
                </div>
              </div>

              <div class="form-group row border-bottom">
                <label for="" class="col-md-4">Kelas</label>
                <div class="col-md-8">
                  <label id="lbl_kelas" for="" style="font-weight: 100;">: </label>
                </div>
              </div>

              <div class="form-group row border-bottom">
                <label for="" class="col-md-4">Tahun Ajaran</label>
                <div class="col-md-8">
                  <label id="lbl_tahun_ajaran" for="" style="font-weight: 100;">: </label>
                </div>
              </div>

              <div class="form-group row border-bottom">
                <label for="" class="col-md-4">Angkatan</label>
                <div class="col-md-8">
                  <label id="lbl_tahun_angkatan" for="" style="font-weight: 100;">: </label>
                </div>
              </div>
             <!--  <a href="" class="small" style="font-family: segoeuib;color: yellow">*siswa mempunyai tunggakan iuran,pada tahun ajaran sebelumnya</a><br/> -->
              <!-- <small class="" style="color:yellow" onclick=""></small> -->

              <a id="lbl_tunggakan" href="javascript:void(0)" class="d-none"><small class="" style="color:yellow"  onclick="readDaftarTunggakan();">*siswa memiliki tunggakan iuran,klik untuk melihat detail</small></a>
            </form>

            </div>
          </div>

      </div>
      <div class="col-md-8">
        <div class="card bg-head pill">
          <!-- <h5 class="card-header bg-dark text-white">Data</h5> -->
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h5 class="card-title text-white" style="font-family: segoeuib">Riwayat Transaksi</h5>
              </div>
              <div class="col-auto">
                <button class="btn btn-default">Cetak</button>
              </div>
            </div>
            <hr/>

            <div class="row">
              <div class="col-sm-12">
                <table id="tbl-data" class="table table-bordereds table-striped table-pill table-hover" >
                  <thead class="thead-lights text-white" style="background-color: #487d95!important;font-family: segoeui;">
                    <tr>
                      <!-- <th scope="col">#</th>
                      <th scope="col">Nama Alternatif</th> -->
                        <th width="10px">No</th>
                        <th>Tanggal Transaksi</th>
                        <th>Iuran Bulanan</th>
                        <th>DPMP</th>
                        <th>DU</th>
                        <th>Total</th>
                        <!-- <th style="width:125px;"></th> -->
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script src="<?php echo base_url('assets/js/kwitansi-script.js?wffw')?>"></script> 
</body>

</html>