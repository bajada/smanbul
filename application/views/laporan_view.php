<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>SMART Eskul</title>

    <style type="text/css">
      /*.grad {
        background-image: linear-gradient(to bottom right, rgba(78,123,105,1), rgba(133,154,52,1));
        background-repeat: no-repeat;
         background-size: cover;
         height: 100%;
      }*/


      html, body {
        background-image: linear-gradient(to bottom right, rgba(78,123,105,1), rgba(144,159,45,1));
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        height: 100%;
      }

      .bg-login {
        background-color: rgba(170, 192, 171, 1)
      }

      .bg-head {
        background-color: rgba(170, 192, 171, 0.5);
        color:white;
      }

      .form-control {
          height: 46px;
          border: none;
          background: #f7f7f7;
          width: 100%;
          padding: 0 25px;
          border-radius: 23px;
          color: #333;
      }

      input, textarea, select, button {
          /*font-family: poppins-regular;*/
          font-size: 13px;
          border-radius: 23px;
      }
      label {
        font-size: 20px!important;
      }

      .pill {
        border-radius: 23px;
      }

      .font-main {
        font-weight: 300!important;
      }
      .text-white-href {
          color:white!important;
      }
      .text-white-href:hover {
          color: -webkit-link;
          cursor: pointer;
          text-decoration: underline;
          color:rgba(0,123,255,.25)!important;
      }

    </style>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="<?php echo base_url('assets/my/font-awesome-custom/css/font-more-awesome.css')?>" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  </head>
  <body class=""> 
    <div class="container-fluid mt-3">

      <div class="media bg-head pill pl-5 pt-3  pb-3">
        <img src="http://sman1-cibungbulang.sch.id/assets/img/smanbul.png" style="width: 100px;" class="ml-3 mr-3" alt="GAMBAR HERE">
        <div class="media-body mt-auto mb-auto">
          <h2 class="font-main">Sistem Informasi Pembayaran</h2>
          <h3 class="font-main">SMAN 1 CIBUNGBULANG</h3>
        </div>
        <div class="pr-5 pt-3  pb-3">
          <a href="<?php echo base_url('page/dashboard_view') ?>" class="mr-4 text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-home fa-4x" aria-hidden="true"></i> Home</a>
          <a href="<?php echo base_url('page/login_view') ?>" class="text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-globe fa-4x" aria-hidden="true"></i> Website</a>
        </div>
      </div>

          <!-- <h1 class="card-title text-white mb-4 font-main">LOGIN</h1> -->
          <div class="pt-3">
            <!-- <div class="row">
              <div class="col-md-2"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">IURAN</button></div>
              <div class="col-md-2">
                
                <div style="font-size: 2.5rem;" class="text-white">
                  <div><a href="" class="text-white-href"><i class="fas fa-hand-holding-usd fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span></a></div>
                </div>

              </div>
              <div class="col-md-2">
                
                <div style="font-size: 2.5rem;" class="text-white">
                  <div><a href="" class="text-white-href"><i class="fas fa-suitcase fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> DPMP Siswa</span></a></div>
                </div>

              </div>
            </div> -->

            <nav aria-label="breadcrumb">
              <ol class="breadcrumb" style="background-color:transparent;">
                <li class="breadcrumb-item"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">LAPORAN</button></li>
                <li class="breadcrumb-item" aria-current="page">
                    <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-edit fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Harian</span></a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                    <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-clipboard fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Bulanan</span></a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                    <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-file-alt fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Tahunan</span></a>
                </li>
              </ol>
            </nav>

          </div>

          <div class="card bg-login pill p-5 h-100" >
            <!-- <h5 class="card-header bg-primary text-white">Login SPK SMART</h5> -->
            <div class="card-body">
             <!--  <h5 class="card-title">LOGIN</h5>
              <hr/>
 -->
              <!-- <form action="<?php echo base_url('PageController') ?>" >
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-3 col-form-label">Username</label>
                  <div class="col-sm-8">
                    <input value=" " type="text" class="form-control" id="" placeholder="Masukkan Username">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword" class="col-sm-3 col-form-label">Password</label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" id="" placeholder="Masukkan Password">
                  </div>
                </div>

                <button id="btn-submit" type="submit" class="btn btn-primary btn-block d-none">Login</button>
              </form> -->

            </div>
          </div>

          <!-- <div class="mt-4 text-right">
                <button type="button" class="btn btn-lg bg-head pill pl-5 pr-5">Batal</button>
                <button onclick="$('#btn-submit').click()" type="button" class="btn btn-lg btn-secondary pill pl-5 pr-5">Login</button>
          </div>  -->
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


    <script src="<?php echo base_url('assets/js/login-js.js')?>"></script> 

  </body>
</html>