<?php $this->load->view('inc/header');?>
<?php
  function cek_kelas($data, $val){
    $status=true;
    foreach ($data as $key => $value) {
      $new_val1=preg_replace('/\s+/', ' ', trim($val));
      $new_val2=preg_replace('/\s+/', ' ', trim($value->kelas));
       
      if($new_val1==$new_val2){
        $status=false;
      }
     } 
     return $status;
  }

  function has_dupes($array, $val2) {
    $count=0;
    $dup=[];
    $obj=[];
    foreach ($array as $val) {
      if(empty($val['A'])) continue;
      if($val['A'] != "REF"){
        if($val['A']==$val2){
          $count++;
        }
      }
    }
    return $count;
  }

  function array_has_dupes($array) {
     // streamline per @Felix
     return count($array) !== count(array_unique($array));
  }

 ?>
  <div class="pt-2">

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color:transparent;">
        <li class="breadcrumb-item"><button onclick="window.history.back();" type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Kembali</button></li>

        <li class="breadcrumb-item" aria-current="page">

            <!-- <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-hand-holding-usd fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span></a> -->

              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-users fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Siswa</span>
              </a>
        </li>
       <!--  <li class="breadcrumb-item" aria-current="page">
              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-suitcase fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span>
              </a>
        </li> -->
      </ol>
    </nav>

  </div>



    <div class="row justify-content-center">
      <div class="col-md-11">

  <div class="card bg-head pill ">
    <div class="card-header text-white" style="background-color: #487d95!important;font-family: segoeui;">
    <div class="row">
      <div class="col-md-7"> <h5 style="font-family: segoeui;">Preview Data Siswa</h5></div>
      <div id="btn-action" class="col-md-5 text-right">
        
      </div>
      <!-- <div class="col-md-6">
        <a href="<?php echo base_url('page/form_siswa_view') ?>" type="" class="btn btn-light btn-lg btn-light pill pl-5 pr-5 float-right">Tambah</a>
      </div> -->
    </div>
    </div>

    <div class="card-body">
    <!-- <h3 class="card-title">

    <div class="row">
      <div class="col-md-6"> <h5 style="font-family: segoeui;">Data Siswa</h5></div>
      <div class="col-md-6"><button onclick="$('#modal-form').modal('show');" type="button" class="btn btn-light btn-light pill pl-4 pr-4 float-right">Tambah</button> <button onclick="goToEdit()" type="button" class="btn btn-light btn-light pill pl-4 pr-4 float-right mr-1">Action</button></div>
    </div>

    </h3> -->

    <form id="search-form">
      <div class="form-group row">
        <div class="col-md-4">
          <input name="filter_keyword" type="text" class="form-control" autocomplete="off" placeholder="Ketik Nama Siswa /No.Ref" onkeyup="renderDisplay(this.value)">
        </div>
        <div class="col-md-2 offset-6">
            <select name="filter_kelas" class="form-control" onchange="renderDisplay(this.value)">
                <option value="0">Semua Kelas</option>
            </select> 
        </div>
      </div>
    </form>

    <div class="" id="jumlah_kosong" style="color: yellow;display: none">
      <ul id="msg-row"></ul>
    </div>

    <script src="<?php echo base_url('assets/jquery/jquery.min.js')?>"></script>
    <script type="text/javascript">var container=$("#jumlah_kosong");</script>
    
    <div class="row">
      <div class="col-12">
        <div class="tab-content" id="v-pills-tabContent">
          <div class="tab-pane fade show active" id="v-pills-kelas-none">
                <div class="row">
                  <div class="col-md-12 pill-top table-responsive" style="height: 600px!important;">
                  
                   <?php
                   $true=true;
                    if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form 
                      if(isset($upload_error)){ // Jika proses upload gagal
                        echo "<div style='color: yellow;'>".$upload_error."</div>"; // Muncul pesan error upload
                        die; // stop skrip
                      }

                      // Buat sebuah tag form untuk proses import data ke database
                      echo "<form method='post' action='".base_url("SiswaController/import")."'>";
                      
                      // Buat sebuah div untuk alert validasi kosong
                      // echo "<div style='color: yellow;' id='kosong'>
                      // Ada <span id='jumlah_kosong'></span>.
                      // </div>";

                      if(isset($_POST['kelasFile']) && $_POST['kelasFile']=="X") $colspan_h1=4;
                      else $colspan_h1=3;
                      $temp_tabel = "<table id='tbl-data' class='table table-sm table-hover table-responsive table-striped table-bordered bg-light text-dark' style='font-family: segoeui;opacity: 0.8;display: table;width:180%'>
                      
                      <thead class='text-white' style='background-color: #487d95!important;''>
                      <tr>
                        <th class='text-center align-middle' rowspan='2'>REF</th>
                        <th class='text-left align-middle' rowspan='2'>NAMA LENGKAP</th>
                        <th class='text-center align-middle' rowspan='2'>JK</th>
                        <th class='text-center align-middle' rowspan='2'>KELAS</th>
                        <th class='text-center' colspan='${colspan_h1}'>IURAN DAFTAR ULANG(DU)</th>
                        <th class='text-center align-middle' rowspan='2'>TAHUN AJARAN</th>
                        <th class='text-center align-middle' rowspan='2'>ANGKATAN</th>
                        <th class='text-center align-middle' rowspan='2'>REGISTRASI</th>
                        <th class='text-center' colspan='${colspan_h1}'>RINCIAN PEMBAYARAN(DU)</th>
                        <th class='text-center align-middle' rowspan='2'>TGL BAYAR</th>
                        <th class='text-center align-middle' rowspan='2'>KETERANGAN MASUK</th>
                      </tr>
                      <tr>
                        <th class='text-center'>BULANAN</th>
                        <th class='text-center'>DPMP</th>
                        <th class='text-center'>OSIS</th>";
                      if(isset($_POST['kelasFile']) && $_POST['kelasFile']=="X")
                      $temp_tabel .= "<th class='text-center'>TABUNGAN</th>";
                      $temp_tabel .= "<th class='text-center align-middle' >OSIS</th>";
                      if(isset($_POST['kelasFile']) && $_POST['kelasFile']=="X")
                      $temp_tabel .= "<th class='text-center'>TABUNGAN</th>";
                      $temp_tabel .= "<th class='text-center align-middle' >BULANAN</th>
                        <th class='text-center align-middle' >DPMP</th>
                      </tr>
                      </thead>
                      ";
                      echo $temp_tabel;
                      // print_r($data_siswa);
                      $numrow = 2;
                      $kosong = 0;
                      $kosong_iuran = 0;
                      $have_duplicate = 0;
                      $have_duplicate_db = 0;
                      $have_wrong_db = 0;
                      $have_wrong_class = 0;
                      $have_quit_class = 0;
                      
                      // Lakukan perulangan dari data yang ada di excel
                      // $sheet adalah variabel yang dikirim dari controller
                      $ref=$ref_sheet;
                      sort($sheet);

                      $html=[];
                      foreach ($sheet as $key => $value) {
                        if(empty($value['A'])) continue;
                          if($value['A'] != 'REF'){
                            $value_init = $value['A'];
                            if(array_keys($html) != null) $html[]=$value_init;
                            else $html[]="";
                          }
                      }

                      $arrayName = array();

                      $ne_key=0;
                      $new_sheet=[];
                      $i=array('One','Two','Two','Three','Four','Five','Five',null);
                      $ar = array_replace($i,array_fill_keys(array_keys($i, null),''));
                      $arrayValueCounts = array_count_values($ar);
                     
                      foreach($sheet as $row){
                        // Ambil data pada excel sesuai Kolom
                        $col_1 = $row['A'];
                        $col_2 = $row['B'];
                        $col_3 = $row['C']; 
                        $col_4 = $row['D'];
                        $col_5 = $row['E'];
                        $col_6 = $row['F'];
                        $col_7 = $row['G'];
                        $col_8 = $row['H'];
                        $col_9 = $row['I'];
                        $col_10 = $row['J'];
                        $col_11 = $row['K'];
                        $col_12 = $row['L'];
                        $col_13 = $row['M'];
                        $col_14 = $row['N'];
                        $col_15 = $row['O'];
                        $col_16 = $row['P'];
                        $col_17 = $row['Q'];

                        $status_r="";

                          if(empty($col_1) && empty($col_2))
                          continue;

                          if($col_1 != 'REF'){

                            if($col_1 == "" || $col_2 == "" || $col_4 == "" || $col_9 == "" || $col_10 == "" || $col_11 == ""){
                              $kosong++;
                              $status_r=" bg-danger text-white";
                            }else if($col_12 == "" && $col_13 == "" && $col_14 == "" && $col_15 == ""){
                              $status_r=" bg-danger text-white";
                              $kosong_iuran++;
                                
                            }else if(has_dupes($sheet, $col_1)>1){
                              // echo "<tr class='data-row "."bg-warning text-white"."'>";
                              $status_r=" bg-warning text-white";
                              $have_duplicate++;
                            }else{
                              if($row['D']!="KELUAR"){
                                // if(cek_kelas($data_kelas, $row['D'])){
                                //   $status_r=" bg-danger text-white";
                                //   $have_wrong_class++;
                                // }

                                $have_idk_class=false;
                                foreach ($data_kelas as $key => $value) {
                                  if($row['D']==$value->kelas){
                                    $have_idk_class=true;
                                  }
                                }

                                if($have_idk_class==false){
                                  $status_r=" bg-danger text-white";
                                  $have_wrong_class++;
                                }
                              }else{
                                  $status_r=" bg-info text-white";
                                  $have_quit_class++;
                              }

                            }

                            if(!empty($data_siswa)){
                              foreach ($data_siswa as $key => $value) {
                                if($col_1 == $value->ref_siswa){
                                  // $status_r=" bg-secondary text-white";
                                  $have_duplicate_db++;
                                }
                              }
                              // foreach ($data_kelas as $key => $value) {
                              //   if($col_4 != $value->kelas){
                              //     $status_r=" bg-danger text-white";
                              //     $have_wrong_db++;
                              //   }
                              // }
                            }

                            echo "<tr class='data-row ".$status_r."'>";


                            $arrayName[] = array(
                              'ref' => $row['A'], 
                              'nama' => str_replace("'", ' ', $row['B']), 
                              'jenis_kelamin' => $row['C'], 
                              'kelas' => $row['D'], 
                              'bulanan' => $row['E'], 
                              'dpmp' => $row['F'], 
                              'du' => $row['G'], 
                              'tabungan' => $row['H'], 
                              'tahun_ajaran' => $row['I'], 
                              'tahun_angkatan' => $row['J'], 
                              'registrasi' => $row['K'], 
                              'iuran_du' => $row['L'], 
                              'iuran_tabungan' => $row['M'], 
                              'iuran_bulanan' => $row['N'], 
                              'iuran_dpmp' => $row['O'], 
                              'tanggal_iuran' => $row['P'], 
                              'keterangan_masuk' => $row['Q'], 
                              'status' => $status_r
                            );
                              echo "<td>".$col_1."</td>";
                              echo "<td class='text-left'>".$col_2."</td>";
                              echo "<td>".$col_3."</td>";
                              echo "<td>".$col_4."</td>";
                              echo "<td>".number_format($col_5)."</td>";
                              echo "<td>".number_format($col_6)."</td>";
                              echo "<td>".number_format($col_7)."</td>";
                              if(isset($_POST['kelasFile']) && $_POST['kelasFile']=="X"){
                                echo "<td>".number_format($col_8)."</td>";
                              }
                              else {
                                echo "<td>".$col_8."</td>";
                              }
                              echo "<td>".$col_9."</td>";
                              echo "<td>".$col_10."</td>";
                              echo "<td>".$col_11."</td>";
                              echo "<td>".number_format($col_12)."</td>";
                              echo "<td>".number_format($col_13)."</td>";
                              if(isset($_POST['kelasFile']) && $_POST['kelasFile']=="X"){
                                echo "<td>".number_format($col_14)."</td>";
                                echo "<td>".number_format($col_15)."</td>";
                              }
                              else {
                                echo "<td>".$col_14."</td>";
                                echo "<td>".$col_15."</td>";
                              }
                              if(isset($_POST['kelasFile']) && $_POST['kelasFile']=="X"){
                                echo "<td>".$col_16."</td>";
                                echo "<td>".$col_17."</td>";
                              }
                            echo "</tr>";

                            $ref++;
                            $ne_key++;
                          }
                      }

                      $x = array_map(function($v) {return $v['ref'];}, $arrayName);
                      // print_r($x);
                      $ar = array_replace($x,array_fill_keys(array_keys($x, null),''));
                      // $countes = array_count_values($ar);
                      // print_r($countes);
                      $numrow++; // Tambah 1 setiap kali looping
                      //}
                      
                      echo "</table>";
                      
                      // Cek apakah variabel kosong lebih dari 0
                      // Jika lebih dari 0, berarti ada data yang masih kosong
                      if($kosong > 0){
                      ?>  
                        <script>
                          $(document).ready(function(){
                            $("#msg-row").append('<li class="text-white"><span class="bg-danger"><?php echo ($kosong); ?> record belum lengkap, pastikan telah diisi(REF, NAMA, JK, KELAS, TAHUN AJARAN, ANGKATAN, dan REGISTRASI).</span></li>');
                            container.show();
                          });
                        </script>
                      <?php
                        $true=false;
                      }
                      if($kosong_iuran > 0){
                      ?>  
                        <script>
                          $(document).ready(function(){
                            $("#msg-row").append('<li class="text-white"><span class="bg-danger"><?php echo ($kosong_iuran); ?> record belum lengkap, pastikan telah diisi(OSIS, TABUNGAN, BULANAN, ANGKATAN, dan DPMP).</span></li>');
                            container.show();
                          });
                        </script>
                      <?php
                        $true=false;
                      }
                      if($have_duplicate > 0 || $have_duplicate_db > 0){
                      ?>  
                        <script>
                          $(document).ready(function(){
                            $("#msg-row").append('<li class="text-white"><span class="bg-warning"><?php echo ($have_duplicate); ?> record duplikat/kesamaan record(<?php echo ($have_duplicate_db); ?> record in db).</span></li>');
                            container.show();
                          });
                        </script>
                      <?php
                        $true=false;
                      }

                      if($have_quit_class > 0){
                      ?>
                        <script>
                        $(document).ready(function(){
                          $("#msg-row").append('<li class="text-white"><span class="bg-info"><?php echo ($have_quit_class); ?> record data, yang akan KELUAR.</span></li>');
                          container.show();
                        });
                        </script>
                      <?php
                        $true=false;
                      }
                      if($have_wrong_class>0){
                        ?>
                        <script>
                        $(document).ready(function(){
                          $("#msg-row").append('<li class="text-white"><span class="bg-danger"><?php echo ($have_wrong_class); ?> record data, kolom <strong>KELAS</strong> tidak sesuai.</span></li>');
                          container.show();
                        });
                        </script>
                      <?php
                        $true=false;
                      }
                      echo "</form>";
                    }

                    if($true){
                      ?>
                        <script>
                        $(document).ready(function(){
                          container.show();
                          var ctx="<?php echo base_url("page/siswa-view"); ?>";
                          var temp="<button class='btn btn-light mr-2' type='button' id='btn-import'>Import</button>"+
                          "<a class='btn btn-danger' href="+ctx+">Cancel</a>";
                          $('#btn-action').html(temp);
                        });
                        </script>
                      <?php 
                    }
                    ?>
                  </div>
                </div>
          </div>
        </div>
      </div>
    </div>

    </div>

    <div class="card-footer">
      <div class="row no-gutters px-1 py-3 align-items-center">
          <div class="col pl-3">
              <div class="dataTables_info" id="info-tabel">
                <?php
                    if(isset($_POST['preview'])){
                      echo "Total Data ".count($arrayName)." Record";
                    }
                ?>  
                <!-- 1 to 8 Items of 11 — <a href="#!" class="font-weight-semi-bold"> view all <span class="fa fa-angle-right"></span></a> -->

              </div>
          </div>
          <div class="col-auto pr-3 d-none">
              <div class="dataTables_paginate paging_simple" id="DataTables_Table_0_paginate">
                  <ul class="pagination pagination-sm">
                      <li class="paginate_button page-item previous disabled" id="DataTables_Table_0_previous"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>
                      <li class="paginate_button page-item next" id="DataTables_Table_0_next"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0" class="page-link">Next</a></li>
                  </ul>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
</div>
    </div>

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md pill text-white" role="document" styles="max-width: 700px">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="font-family: segoeuib">Tambah Siswa Baru</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form-siswa">
              <div class="form-row justify-content-center">
                <div class="form-group col-md-6">
                  <label for="">Jumlah Siswa</label>
                  <input name="jumlah_siswa" type="text" class="form-control" autocomplete="off"  placeholder="Masukkan Jumlah Data">
                </div>
                
               <div class="form-group col-md-6">
                  <label>Kelas</label>
                  <select name="kelas" class="form-control select-styles">
                      <option value="0">Pilih kelas</option>
                  </select>
                </div> 
              </div> 

              <button id="btn-submit-2" type="submit" class="btn btn-secondary pill pl-4 pr-4 float-right d-none">SUBMIT</button>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light bg-head pill pl-4 pr-4" data-dismiss="modal">CLOSE</button>
            <button onclick="$('#btn-submit-2').click()" type="button" class="btn btn-secondary btn-secondary pill pl-4 pr-4">SUBMIT</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap modal import -->
    <div class="modal fade" id="modal-form-import" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md pill text-white" role="document" style="max-width: 920px">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="font-family: segoeuib">Import Data Siswa</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="<?php echo base_url('siswaController/uploadData') ?>" method="post" enctype="multipart/form-data">
                Upload excel file : 
                <input type="file" name="uploadFile" value="" /><br><br>
                <input type="submit" name="submit" value="Upload" />
                <input type="button" name="download" value="Download Template" onclick="window.location.href='<?php echo base_url("excel/template_excel.xlsx"); ?>'" />
                <input type="submit" name="preview" value="Preview" />
            </form>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light bg-head pill pl-4 pr-4" data-dismiss="modal">CLOSE</button>
            <button onclick="$('#btn-submit-2').click()" type="button" class="btn btn-secondary btn-secondary pill pl-4 pr-4">SUBMIT</button>
          </div>
        </div>
      </div>
    </div>

  <?php $this->load->view('inc/footer');?>
   <!--  <script src="<?php echo base_url('assets/js/siswa-script.js?yes')?>"></script>  -->

  <script type="text/javascript">

  var path = ctx + 'SiswaController';
  //var count = jumlah_siswa;
  const escapePattern = s => s.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&');

  // converts ILIKE pattern to a RegExp object
  const ilikeToRegExp = pattern =>
    new RegExp(
      `^${escapePattern(pattern)}$`
        // convert ILIKE wildcards, don't match escaped
        .replace(/(?<![\\])%/g, '.*')
        .replace(/(?<![\\])_/g, '.')
        // replace ILIKE escapes
        .replace(/\\%/g, '%')
        .replace(/\\_/g, '_'),
      'i'
    );
  function init() {
    getSelectTahun();

    $('#btn-import').click(function(e){
        var obj = {};
        ajaxPOST(path + '/import',obj,'onActionSuccess','onSaveError');
        e.preventDefault();
    });

    $('#bg-head').hide();
  };


  function onActionSuccess(resp){
    console.log(resp);
    if(resp.code==200){
      Swal.fire('Berhasil!', resp.message, 'success');
      window.location.href="./";
    }
  }

  function onSaveError(response){
    console.log(response);
    Swal.fire("Data Sudah Ada", response.responseJSON.message, 'warning');
    var resp_msg={"title" : "Message", "body" : response.responseJSON.message, "icon"  : "warning"};
    showAlertMessage(resp_msg, 1800);
  }

  function getSelectTahun() {
     $('[name="filter_tahun_ajaran"]').empty();
      $('[name="filter_tahun_ajaran"]').append(new Option('Semua Tahun Ajaran', ''));
      var d = new Date();
      var n = d.getFullYear();
      var tahun = n - 5;
      for (var i = n; i>= tahun; i--) {
          $('[name="filter_tahun_ajaran"]').append(new Option(i+"/"+(i+1)));
      }
  }

  function renderDisplay(param=""){
    var obj = '<?php echo json_encode($arrayName); ?>';
    console.log(JSON.parse(obj));
    response_data = JSON.parse(obj);
    $('#loading-row').remove();
    $('#no-data-row').remove();
    
    var tbody = $("#tbl-data").find('tbody');
    tbody.text('');
    var row = "";
    var num = $('.data-row').length+1;

    var html=[];
    $.each(response_data, function(key){
      var value = this.ref;
      // console.log(value);
      console.log(html.indexOf(value));
      if(html.indexOf(value) == -1) html.push(value);
      else html.push("");
    });

    $.each(response_data,function(key,value){
      if(param != ""){
          if(ilikeToRegExp('%'+param+'%').test(value.ref) || ilikeToRegExp('%'+param+'%').test(value.nama) || ilikeToRegExp('%'+param+'%').test(value.kelas) || ilikeToRegExp('%'+param+'%').test(value.tahun_ajaran)){
            // row += render_row(value, num);
            if(html[key]!=value.ref){
              row += '<tr contenteditable="false" class="data-row'+""+'" id="row-'+value.ref+'">';
            }else{
              row += '<tr contenteditable="false" class="data-row'+value.status+"  text-whie"+'" id="row-'+value.ref+'">';
            }
              row += '<td class="">'+value.ref+'</td>';
              row += '<td class="">'+value.nama+'</td>';
              row += '<td class="">'+value.jenis_kelamin+'</td>';
              row += '<td class="">'+value.kelas+'</td>';
              row += '<td class="">'+value.bulanan+'</td>';
              row += '<td class="">'+value.dpmp+'</td>';
              row += '<td class="">'+value.du+'</td>';
              row += '<td class="">'+value.tabungan+'</td>';
              row += '<td class="">'+value.tahun_ajaran+'</td>';
              row += '<td class="">'+value.tahun_angkatan+'</td>';
              row += '<td class="">'+value.registrasi+'</td>';
              row += '<td class="">'+value.iuran_du+'</td>';
              row += '<td class="">'+value.iuran_tabungan+'</td>';
              row += '<td class="">'+value.iuran_bulanan+'</td>';
              row += '<td class="">'+value.iuran_dpmp+'</td>';
              row += '<td class="">'+value.tanggal_iuran+'</td>';
              row += '<td class="">'+value.keterangan_masuk+'</td>';
            row += '</tr>';

            num++;
          }
      }else{
        row += render_row(value, num);
        num++;
      }
    });
    row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="10"><div align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
  }

  function render_row(value, num){
  //  console.log(value);
      var row = "";
      row += '<tr contenteditable="false" class="data-row'+value.status+'" id="row-'+value.ref+'">';
      // row+='<td align="center">';
      // row+='<div class="form-check">';
      //     row+='<input class="form-check-input" style="position:unset" type="checkbox" value='+value.id_siswa+' name="id[]" data-tahun_ajaran='+value.tahun_ajaran+'>';
      // row+='</div>';
      // row+='</td>';
      // row += '<td>'+(num)+'</td>';
      row += '<td class="">'+value.ref+'</td>';
      // row += '<td class="">'+value.nis_siswa+'</td>';
      row += '<td class="">'+value.nama+'</td>';
      row += '<td class="">'+value.jenis_kelamin+'</td>';
      row += '<td class="">'+value.kelas+'</td>';
      row += '<td class="">'+value.bulanan+'</td>';
      row += '<td class="">'+value.dpmp+'</td>';
      row += '<td class="">'+value.du+'</td>';
      row += '<td class="">'+value.tabungan+'</td>';
      row += '<td class="">'+value.tahun_ajaran+'</td>';
      row += '<td class="">'+value.tahun_angkatan+'</td>';
      row += '<td class="">'+value.registrasi+'</td>';
      row += '<td class="">'+value.iuran_du+'</td>';
      row += '<td class="">'+value.iuran_tabungan+'</td>';
      row += '<td class="">'+value.iuran_bulanan+'</td>';
      row += '<td class="">'+value.iuran_dpmp+'</td>';
      row += '<td class="">'+value.tanggal_iuran+'</td>';
      row += '<td class="">'+value.keterangan_masuk+'</td>';
      // row += '<td class="">'+"-"+'</td>';
      // row += '<td class="">';
      // // row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-info pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'edit\')"><i class="fas fa-pencil-alt"></i></a> ';
      // row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'delete\')"><i class="fas fa-trash-alt"></i></a>';
      // row += '</td>';
      row += '</tr>';
      return row;
  }

  </script>
  </body>
</html>