<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>SMART Eskul</title>

    <style type="text/css">
      /*.grad {
        background-image: linear-gradient(to bottom right, rgba(78,123,105,1), rgba(133,154,52,1));
        background-repeat: no-repeat;
         background-size: cover;
         height: 100%;
      }*/


      html, body {
        background-image: linear-gradient(to bottom right, rgba(78,123,105,1), rgba(144,159,45,1));
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        height: 100%;
      }

      .bg-gradient{
        background-image: linear-gradient(to bottom right, rgba(78,123,105,1), rgba(144,159,45,1));
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        height: 100%;
      }

      .bg-login {
        background-color: rgba(170, 192, 171, 1)
      }

      .bg-head {
        background-color: rgba(170, 192, 171, 0.5);
        color:white;
      }

      .form-control {
          height: 46px;
          border: none;
          background: #f7f7f7;
          width: 100%;
          padding: 0 25px;
          border-radius: 23px;
          color: #333;
      }

      /*.select-auto-complete {
          height: 46px;
          border: none;
          background: #f7f7f7;
          width: 100%;
          padding: 0 25px;
          border-radius: 23px;
          color: #333;
      }*/
      .select2-container--default .select2-selection--multiple {
        border-radius: 23px!important;
        /* height: 41px; */
        background: #f7f7f7!important;
        height: 46px!important;
        color: #333;
        padding: 8px 17px;
      }
      .select2-container--default.select2-container--focus .select2-selection--multiple {
          border: solid black 0px!important;
          outline: 0!important;
          padding: 4px 16px!important;
      }
      .select2-container--default .select2-selection--single {
        border-radius: 23px!important;
        /* height: 41px; */
        background: #f7f7f7!important;
        height: 46px!important;
        color: #333;
        padding: 8px 17px;
      }
      .select2-container--default .select2-selection--single .select2-selection__arrow {
          padding: 20px;
      }

      .selectric .label {
          /*height: 46px;
          border: none;
          background: #f7f7f7;
          width: 100%;
          border-radius: 23px;*/
          padding: 0 15px!important;
          font-size: 1rem!important;
          color: #495057!important;
      }

      input, textarea, select, button .select-auto-complete {
          /*font-family: poppins-regular;*/
          font-size: 13px;
          border-radius: 23px;
      }
      label {
        font-size: 20px!important;
      }

      .pill {
        border-radius: 23px;
      }

      .font-main {
        font-weight: 300!important;
      }
      .text-white-href {
          color:white!important;
      }
      .text-white-href:hover {
          color: -webkit-link;
          cursor: pointer;
          text-decoration: none;
          color:#f8f9fa!important;
      }
      .btn-light {
        border:0px;
      }
      .btn-secondary{
        background: #49766d;
      }


    </style>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="<?php echo base_url('assets/my/font-awesome-custom/css/font-more-awesome.css')?>" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="<?php echo base_url('assets/selectric/selectric.css')?>" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  </head>
  <body class=""> 
    <div class="container-fluid mt-3">

      <div class="media bg-head pill pl-5 pt-3  pb-3">
        <img src="http://sman1-cibungbulang.sch.id/assets/img/smanbul.png" style="width: 100px;" class="ml-3 mr-3" alt="GAMBAR HERE">
        <div class="media-body mt-auto mb-auto">
          <h2 class="font-main">Sistem Informasi Pembayaran</h2>
          <h3 class="font-main">SMAN 1 CIBUNGBULANG</h3>
        </div>
        <div class="pr-5 pt-3  pb-3">
          <a href="<?php echo base_url('page/dashboard_view') ?>" class="mr-4 text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-home fa-4x" aria-hidden="true"></i> Home</a>
          <a href="<?php echo base_url('page/login_view') ?>" class="text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-globe fa-4x" aria-hidden="true"></i> Website</a>
        </div>
      </div>

          <!-- <h1 class="card-title text-white mb-4 font-main">LOGIN</h1> -->
          <div class="pt-2">
            <!-- <div class="row">
              <div class="col-md-2"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">IURAN</button></div>
              <div class="col-md-2">
                
                <div style="font-size: 2.5rem;" class="text-white">
                  <div><a href="" class="text-white-href"><i class="fas fa-hand-holding-usd fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span></a></div>
                </div>

              </div>
              <div class="col-md-2">
                
                <div style="font-size: 2.5rem;" class="text-white">
                  <div><a href="" class="text-white-href"><i class="fas fa-suitcase fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> DPMP Siswa</span></a></div>
                </div>

              </div>
            </div> -->

            <nav aria-label="breadcrumb">
              <ol class="breadcrumb" style="background-color:transparent;">
                <li class="breadcrumb-item"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">IURAN</button></li>

                <li class="breadcrumb-item" aria-current="page">

                    <!-- <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-hand-holding-usd fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span></a> -->

                      <a href="" class="text-white-href" style="display: inline-flex;">
                      <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-dollar fa-stack-1x fa-inverse text-muted"></i>
                      </span>
                      <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran SPP</span>
                      </a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                   <!--  <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-suitcase fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> DPMP Siswa</span></a>
                    -->
                      <a href="" class="text-white-href" style="display: inline-flex;">
                      <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-suitcase fa-stack-1x fa-inverse text-muted"></i>
                      </span>
                      <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran DPMP</span>
                      </a>
                </li>
              </ol>
            </nav>

          </div>

          <div class="card bg-login pill p-5 text-white ">
            <h3 class="card-header d-none">Entri Iuran Siswa</h3>
            <div class="card-body">
            <h3 class="card-title">Entri Iuran Siswa</h3>
              
            <hr/>

            <form action="<?php echo base_url('PageController') ?>" >
                <div class="form-group row">                  
                  <div class="col-sm-6">
                  <label for="" class="col-form-label">Ref/Nama Siswa</label>
                    <select class="form-control select-auto-complete" id="" onchange="$('#modal-form').modal('show');">
                      <option>Ketik Ref/Nama Siswa</option>
                      <option>1001-Ahmad Maulana</option>
                      <option>1102-Rehan Sehan</option>
                      <option>1123-Mohamamd</option>
                      <option>1343-Abudrahman</option>
                    </select>
                  </div>              
                  <div class="col-sm-6"> 
                  <label for="" class="col-form-label">Tanggal Iuran</label>
                    <input type="date" class="form-control" id="" placeholder="Tanggal Pembayaran">
                  </div>
                </div>

                <div class="form-group row">  
                  <!-- <div class="col-sm-6">
                    <label for="" class="col-form-label">Bulan Iuran</label>
                    <select class="form-control select-auto-complete" id="" multiple="" >
                      <option>Pilih Untuk Berapa Bulan Iuran</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                    </select>
                  </div>   -->

                  <div class="col-sm-6"> 
                  <label for="" class="col-form-label">Bulan Iuran</label>
                    <input readonly="" type="text" class="form-control" name="bulan_iuran" placeholder="Klik untuk menentukan jumlah bulan iuran" onclick="$('#modal-bulan-iuran').modal('show');">
                  </div>

                  <div class="col">
                  <label for="" class="col-form-label">SPP</label>
                    <input type="number" class="form-control" id="" placeholder="Nominal SPP">
                  </div>                 
                  <div class="col">
                  <label for="" class="col-form-label">DPMP</label>
                    <input type="number" class="form-control" id="" placeholder="Nominal DPMP">
                  </div>       
                  <div class="col">
                  <label for="" class="col-form-label">DU</label>
                    <input type="number" class="form-control" id="" placeholder="Nominal DU">
                  </div>
                </div>
                <div class="form-group row">       
                  <div class="col-sm-6">
                  <label for="" class="col-form-label">Keterangan</label>
                    <textarea class="form-control" id="" rows="5" placeholder="Keterangan"></textarea>
                  </div>             
                  <div class="col-sm-6">
                  <label for="" class="col-form-label">Total Iuran</label>
                    <input type="number" class="form-control" id="" placeholder="Total">
                  </div>  
                </div>

                <button id="btn-submit" type="submit" class="btn btn-lg btn-secondary pill pl-5 pr-5 float-right">SUBMIT</button>
              </form>

            </div>
          </div>

          <div class="card bg-login pill p-5 d-none">
            <h3 class="card-header text-white d-none">Entri Iuran Siswa</h3>
            <div class="card-body">
            <h3 class="card-title text-white">Entri Iuran Siswa</h3>
              
            <hr/>

            <form action="<?php echo base_url('PageController') ?>" >
                <div class="form-group row">                  
                  <div class="col-sm-6">
                    <select class="form-control select-auto-complete" id="">
                      <option>Ketik Nama Siswa</option>
                    </select>
                  </div>              
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="" placeholder="Tanggal Pembayaran">
                  </div>
                </div>
                <div class="form-group row">  
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="" placeholder="Bulan Iuran">
                  </div>  
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="" placeholder="Jumlah Iuran">
                  </div>
                </div>
                <div class="form-group row">                  
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="" placeholder="Jumlah Iuran DPMP">
                  </div>       
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="" placeholder="Jumlah Iuran Daftar Ulang">
                  </div>
                </div>
                <div class="form-group row">         
                  <div class="col-sm-12">
                    <textarea class="form-control" id="" rows="5" placeholder="Keterangan"></textarea>
                  </div>
                </div>

                <button id="btn-submit" type="submit" class="btn btn-lg btn-secondary pill pl-5 pr-5 float-right">SUBMIT</button>
              </form>

            </div>
          </div>
    </div>

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg pill text-white" role="document">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel" style="margin-left:auto;">INFORMASI IURAN SISWA</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form">
              <div class="form-group row">
                <label for="" class="col-md-3">Ref / Nama Siswa</label>
                <div class="col-md-9">
                  <label for="" style="font-weight: 100;">: 100101 - Adbuhraman</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Kelas</label>
                <div class="col-md-9">
                  <label for="" style="font-weight: 100;">: X-IPA-1</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Besar Iuran/Bulan</label>
                <div class="col-md-9">
                  <label for="" style="font-weight: 100;">: 150,000</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Iuran Terakhir</label>
                <div class="col-md-9">
                  <label id="view_detil_last_iuran" for="" style="font-weight: 100;" data-toggle="popover" title="Detail" data-content="Iuran terakhir adalah bulan ke 11">: 05-11-2018</label>
                </div>
              </div>
              <div class="form-group row">
              <label for="" class="col-md-3">Histori Iuran</label>
              <div class="col-md-9">
              <table class="table table-bordered table-sm">
              <thead class="bg-danger">
              <tr>
              <th scope="col">JUL</th>
              <th scope="col">AGU</th>
              <th scope="col">SEP</th>
              <th scope="col">OKT</th>
              <th scope="col">NOV</th>
              <th scope="col">DES</th>
              <th scope="col">JAN</th>
              <th scope="col">FEB</th>
              <th scope="col">MAR</th>
              <th scope="col">APR</th>
              <th scope="col">MEI</th>
              <th scope="col">JUN</th>
              </tr>
              </thead>
              <tbody class="bg-info">
              <tr>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>-</td>
              <td>-</td>
              </tr>
              </tbody>
              </table>
              </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Sisa Iuran</label>
                <div class="col-md-9">
                  <label for="" style="font-weight: 100;">: 2x</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Sisa DPMP</label>
                <div class="col-md-9">
                  <label for="" style="font-weight: 100;">: 1,000,000</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Sisa DU</label>
                <div class="col-md-9">
                  <label for="" style="font-weight: 100;">: 300,0000</label>
                </div>
              </div>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5" data-dismiss="modal">BATAL</button>
            <button onclick="" type="button" class="btn btn-light btn-lg btn-secondary pill pl-5 pr-5 text-white" data-dismiss="modal">LANJUT</button>
          </div>
        </div>
      </div>
    </div>


    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-bulan-iuran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg pill text-white" role="document">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel" style="margin-left:auto;">FORM BULAN IURAN</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form-bulan">
              <div class="form-group row">
                <label for="" class="col-md-3">Jumlah Bulan Iuran</label>
                <div class="col-md-9">
                    <select class="form-control select-style" id="" onchange="renderBulanKe(this.value);">
                      <option>Pilih Untuk Berapa Bulan Iuran</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                    </select>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Bulan Ke</label>
                <div id="bulan-ke" class="col-md-9">
                </div>
              </div>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5" data-dismiss="modal">BATAL</button>
            <button onclick="setBulanIuran()" type="button" class="btn btn-light btn-lg btn-secondary pill pl-5 pr-5 text-white" data-dismiss="modal">LANJUT</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script src="<?php echo base_url('assets/selectric/jquery.selectric.js')?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script type="text/javascript">
      $(document).ready(function() {

        $('.select-auto-complete').select2({
          placeholder: "Tentukan Pilihan",
        });


      });

      $(function() {
        $('.select-style').selectric();
      });
      $(function () {
        $('[data-toggle="popover"]').popover()
      })

      function renderBulanKe(val){
        var html="";
        for(var i = 0; i < val;i++){
          html+='<input type="text" class="form-control mt-1" id="" placeholder="Bulan Ke">';
        };
        $('#bulan-ke').html(html);
      }
      function setBulanIuran(){
        var val='';
        $.each($('#bulan-ke input'), function(key){
          console.log(this.value);
          val+=";"+this.value;
        })
        $('[name=bulan_iuran]').val(val.substring(1));
      }
    </script>
    <script src="<?php echo base_url('assets/js/login-js.js')?>"></script> 


  </body>
</html>