<?php $this->load->view('inc/header');?>

    <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-md-9">
        <div class="card d-nones" style="border-top-color: #0062cc;border-top-width: 2px;">
          <!-- <h5 class="card-header bg-dark text-white">Data</h5> -->
          <div class="card-body">
            <h5 class="card-title">Data</h5>
            <hr/>
           <!--  <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a> -->

            <form id="search-form">
            <div class="form-group row">
              <div class="col-sm-6">
                  <div class="input-group ">
                    <input name="filter_keyword" type="text" class="form-control" placeholder="Ketik Parameter Pencarian">
                    <div class="input-group-append">
                      <button class="btn btn-outline-secondary" type="submit">Cari</button>
                    </div>
                  </div>
              </div>
                    <div class="col-sm-6 text-right">
                      <div class="btn-group" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-primary" onclick="add();">
                          Tambah
                        </button>
                      </div>
                    </div>
            </div>  
            </form>

            <div class="row">
              <div class="col-sm-12">

                <table class="table table-bordereds table-hover table-stripeds">
                  <thead class="thead-light">
                    <tr>
                      <th colspan="2" scope="col">Alternatif</th>
                      <th scope="col" colspan="5">Kriteria</th>
                    </tr>
                    <tr>
                      <th scope="col">Kode</th>
                      <th scope="col">Nama</th>
                      <th>Minat</th><th>Kemampuan/Bakat</th><th>Jadwal Latihan</th><th>Prestasi Eskul</th>                    </tr>
                  </thead>
                  <tbody>
                    <tr><td>A<sub>1</sub></td><td>Basket</td><td>4</td><td>3</td><td>1</td><td>1</td></tr><tr><td>A<sub>2</sub></td><td>Volly</td><td>3</td><td>2</td><td>4</td><td>2</td></tr><tr><td>A<sub>3</sub></td><td>Aikido</td><td>2</td><td>1</td><td>4</td><td>2</td></tr><tr><td>A<sub>4</sub></td><td>Futsal</td><td>3</td><td>1</td><td>4</td><td>3</td></tr>
                  </tbody>
                </table>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form">
            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="">Nama Kriteria</label>
                <input name="nama_kriteria" type="text" class="form-control" id=""  placeholder="Kriteria">
              </div>
            </div>    
            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="">Bobot Kriteria</label>
                <input name="bobot_kriteria" type="text" class="form-control" id="" placeholder="Contoh: 1 - 100">
              </div>
            </div>   
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" onclick="save();">Save changes</button>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script src="<?php echo base_url('assets/js/kriteria-js.js')?>"></script> 
</body>

</html>