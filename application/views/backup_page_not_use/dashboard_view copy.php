<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body class="bg-light">
    <?php include('inc/navbar.php'); ?>

    <div class="container-fluid mt-3">
      <div class="row justify-content-center">
        <!-- <div class="col-md-3">
          <ul class="list-group">
            <li class="list-group-item active">Kategori</li>
            <li class="list-group-item">Home</li>
            <li class="list-group-item">Daftar Ekskul</li>
            <li class="list-group-item">Grafik Ekskul</li>
            <li class="list-group-item">Perhitungan Smart</li>
            <li class="list-group-item">Kelola Data</li>
          </ul>
          <ul class="list-group mt-2">
            <li class="list-group-item active">Tentang Kami</li>
            <li class="list-group-item">Jl.Pinang Perak IV, Sektor VI, No.10
            <br/>Telp.082119260548 (Tata Usaha)
            <br/>Email:kayumiman@gmail.com</li>
          </ul>
        </div> -->
        <div class="col-md-9">
        <div class="card" style="border-top-color: #0062cc;border-top-width: 2px;">
            <!-- <h5 class="card-header bg-dark text-white">Daftar Eskul</h5> -->
            <div class="card-body">
              <h5 class="card-title text-success">Selamat Datang</h5>
              <p class="card-text">Anda telah masuk di Sistem Pendukung Keputusan(SPK) Pemilihan Ekstrakulikuler Siswa di SMAN 1 Cibungbulang.</p>
              <!-- <a href="#" class="btn btn-primary">Mulai</a> -->
              <p class="card-text">Cara Penggunaan :</p>
              <div class="card-text">1. Tentukan data Kriteria dan Bobot,caranya pilih menu <strong>Pengaturan->Bobot Kriteria</strong><br/>
                2. Dan juga entri data Alternatif,caranya pilih menu <strong>Pengaturan->Alternatif</strong><br/>
                3. Kemudian mulailah entri data-data yang akan dilakukan penentuan/keputusan,caranya pilih menu <strong>Data</strong>
              </p>
            </div>
          </div>
        </div>
      </div>
      <!---------------------------------------------------------------------------------------------------------------->
      <div class="row justify-content-center d-none">
        <!-- <div class="col-md-3">
          <ul class="list-group">
            <li class="list-group-item active">Kategori</li>
            <li class="list-group-item">Home</li>
            <li class="list-group-item">Daftar Ekskul</li>
            <li class="list-group-item">Grafik Ekskul</li>
            <li class="list-group-item">Perhitungan Smart</li>
            <li class="list-group-item">Kelola Data</li>
          </ul>
          <ul class="list-group mt-2">
            <li class="list-group-item active">Tentang Kami</li>
            <li class="list-group-item">Jl.Pinang Perak IV, Sektor VI, No.10
            <br/>Telp.082119260548 (Tata Usaha)
            <br/>Email:kayumiman@gmail.com</li>
          </ul>
        </div> -->
        <div class="col-md-9">
          <div class="card">
            <h5 class="card-header bg-dark text-white">Daftar Eskul</h5>
            <div class="card-body">
              <!-- <h5 class="card-title">Special title treatment</h5> -->
             <!--  <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
              <a href="#" class="btn btn-primary">Go somewhere</a> -->

              <div class="row">
                <div class="col-sm-4">
                  <div class="card">
                    <img class="card-img-top" height="200px" src="https://flowchainsensei.files.wordpress.com/2013/04/randori1.jpg?w=545" alt="Card image cap">
                    <div class="card-body">
                      <h5 class="card-title">AIKIDO</h5>
                      <p class="card-text"><strong>Deskripsi :</strong> This is a wider card with supporting text below as a natural lead-in to additional content.</p>
                      <p><strong>Jumlah Peserta :</strong>-</p>
                      <p><strong>Jadwal Latihan :</strong>Senin, Selasa, Rabu</p>
                      <p><strong>Prestasi Ekskul :</strong>-</p>
                    </div>
                    <div class="card-footer">
                      <small class="text-muted">Last updated 3 mins ago</small>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="card">
                    <img class="card-img-top" height="200px"  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRi-PQ8BNqKOius3jEd-2RznhT48PRzAzEtXkRDnX7yLN7aoXOGEA" alt="Card image cap" >
                    <div class="card-body">
                      <h5 class="card-title">KELAS ANIMASI</h5>
                      <p class="card-text"><strong>Deskripsi :</strong> This is a wider card with supporting text below as a natural lead-in to additional content.</p>
                      <p><strong>Jumlah Peserta :</strong>-</p>
                      <p><strong>Jadwal Latihan :</strong>Senin, Selasa, Rabu</p>
                      <p><strong>Prestasi Ekskul :</strong>-</p>
                    </div>
                    <div class="card-footer">
                      <small class="text-muted">Last updated 3 mins ago</small>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="card">
                    <img class="card-img-top" height="200px" src="https://flowchainsensei.files.wordpress.com/2013/04/randori1.jpg?w=545" alt="Card image cap">
                    <div class="card-body">
                      <h5 class="card-title">Card title</h5>
                      <p class="card-text"><strong>Deskripsi :</strong> This is a wider card with supporting text below as a natural lead-in to additional content.</p>
                      <p><strong>Jumlah Peserta :</strong>-</p>
                      <p><strong>Jadwal Latihan :</strong>Senin, Selasa, Rabu</p>
                      <p><strong>Prestasi Ekskul :</strong>-</p>
                    </div>
                    <div class="card-footer">
                      <small class="text-muted">Last updated 3 mins ago</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- <div class="container-fluid bg-primary text-white">
      <div class="row">
        <div class="col-12 col-md-6 align-self-center order-2 order-md-1 mb-5 mb-md-0">
          <h1 class="display-1 text-bold text-center text-md-left mb-2">Get 40% Off</h1>
          <h4 class="text-gray-soft text-regular text-center text-md-left mb-3 mb-md-4">Our biggest sale ever. Enjoy <img draggable="false" class="emoji" alt="❤" src="https://s.w.org/images/core/emoji/2.3/svg/2764.svg">
          </h4>
          
          <p class="text-gray-soft text-center text-md-left">Code: <span class="badge badge-dark" style="font-size: 14px; position: relative; top: -1px; padding: 6px 10px;">BLACKANDCYBER2018</span> <em class="text-gray-soft"> Valid until 12/1</em>
          </p>
        </div>
        <div class="col-12 col-md-6 order-1 order-md-2">
          <img class="black-friday-graphic" src="https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/images/elements/gifts@2x.png">
        </div>
      </div>
    </div> -->


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


  </body>
</html>