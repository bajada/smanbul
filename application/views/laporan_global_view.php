<?php $this->load->view('inc/header');?>

  <div class="pt-2">

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color:transparent;">
        <li class="breadcrumb-item"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Kembali</button></li>

        <li class="breadcrumb-item" aria-current="page">

            <!-- <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-hand-holding-usd fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span></a> -->

              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-edit fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Harian</span>
              </a>
        </li>
        <li class="breadcrumb-item" aria-current="page">
           <!--  <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-suitcase fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> DPMP Siswa</span></a>
            -->
              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-clipboard fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Mingguan</span>
              </a>
        </li>
      </ol>
    </nav>

  </div>

  <div class="row justify-content-center">
  <div class="col-md-6">
    <form id="search-form">
    <div class="form-group">
    <div class="input-group input-group-lg">
    <div class="input-group-prepend">
      <span class="input-group-text pill-left">TANGGAL IURAN</span>
    </div>
    <input name="filter_keyword" type="date" class="form-control input-sm" aria-label="..." placeholder="Ketik Nomor REF">
    <div class="input-group-append">
      <button class="btn btn-secondary pill-right  pr-5" type="submit" id="btn-cari">Cari</button>
    </div>
    </div>
    </div> 
    </form>
  </div>
  </div>

  <div class="row justify-content-center">
      <div class="col-sm-12">
        <div class="card bg-head pill">
          <div class="card-body">
            <h3 class="card-title">
              <div class="row">
                <div class="col-md-6">
                  <span style="font-family: segoeui;">Data Laporan Harian</span>
                </div>
                <div class="col-md-6">
                  <button id="btn-export" type="button" class="btn btn-secondary btn-lg pill pl-4 pr-4 float-right"><i class="fa fa-arrow-down"></i> DOWNLOAD(.xlsx)</button>
                </div>
              </div>
            </h3>
            <hr/>
            <div class="row">
              <div class="col-sm-3">
                  <div class="">
                    <table id="tbl-data-rekap" class="table table-striped table-pill" style="font-family: segoeui;opacity: 0.8;">
                      <thead class="text-white" style="background-color: #487d95!important;">
                        <tr>
                          <th colspan="2" class="text-center">REKAP</th>
                        </tr>
                      </thead>
                      <tbody class="text-dark btn-light"></tbody>
                      <tfoot class="text-white" style="background-color: #487d95!important;">
                        <tr>
                          <th>Jumlah</th>
                          <th id="total-rekap">0</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
              </div>

              <div class="col-sm-9">
                  <div class="">
                    <table id="tbl-data" class="table btn-light table-striped table-pill"  style="font-family: segoeui;opacity: 0.8;display: table;">
                      <thead class="text-white" style="background-color: #487d95!important;">
                        <tr>
                          <th width="5%">No</th>
                          <th width="6%">REF</th>
                          <th width="15%">SISWA</th>
                          <th width="10%">BULAN KE</th>
                          <!-- <th width="15%" colspan="3">IURAN</th> -->
                          <th width="10%">IURAN BULAN</th>
                          <th width="10%">DPMP</th>
                          <th width="10%">DU</th>
                          <th width="10%">TOTAL</th>
                          <th width="10%">KET</th>
                        </tr>
                      </thead>
                      <tbody class="text-dark">
                      </tbody>
                      <tfoot class="text-white" style="background-color: #487d95!important;">
                        <tr>
                          <th class="text-center" colspan="4">Jumlah</th>
<!--                           <th width="6%">REF</th>
                          <th width="15%">SISWA</th>
                          <th width="10%">BULAN</th> -->
                          <th id="jumlah-iuran">0</th>
                          <th id="jumlah-dpmp">0</th>
                          <th id="jumlah-du">0</th>
                          <th id="jumlah-total">0</th>
                          <th width="10%"></th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script src="<?php echo base_url('assets/js/laporan-global-script.js?yess')?>"></script> 

  </body>
</html>