<?php $this->load->view('inc/header');?>

    <div class="container-fluid mt-3">

          <!-- <h1 class="card-title text-white mb-4 font-main">LOGIN</h1> -->
          <div class="pt-2">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb" style="background-color:transparent;">
                <li class="breadcrumb-item"><a href="dashboard_view" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Kembali</a></li>

                <li class="breadcrumb-item" aria-current="page">
                      <a href="" class="text-white-href" style="display: inline-flex;">
                      <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-chalkboard-teacher fa-stack-1x fa-inverse text-muted"></i>
                      </span>
                      <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Kelas</span>
                      </a>
                </li>
              </ol>
            </nav>

          </div>

    <div class="row justify-content-center">
      <div class="col-md-3">
      <div class="card bg-head ">

      <div class="card-body">
        <select class="form-control" name="filter_jurusan_kelas" onchange="displaySide()">
          <option value="0">Semua Kelas</option>
          <option value="X">X</option>
          <option value="XI">XI</option>
          <option value="XII">XII</option>
        </select>
      </div>
      <ul id="list-group-data" class="list-group list-group-flush ">
        <!-- <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center bg-head">IPA <span class="badge badge-primary badge-pill">14</span></li>
        <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center bg-head">IPS <span class="badge badge-primary badge-pill">14</span></li>
        <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center bg-head">BAHASA <span class="badge badge-primary badge-pill">14</span></li> -->
      </ul>

      </div>
      </div>
      <div class="col-md-8">
        <div class="card bg-head pill">
          <div class="card-header text-white" style="background-color: #487d95!important;font-family: segoeui;">
            <div class="row">

              <div class="col-md-10">
                  <h5>Data Kelas</h5>
              </div>
              <div class="col-md-2 text-right">
                <div class="btn-group" role="group">
                  <button id="btnGroupDrop1" type="button" class="btn btn-secondary pl-4 pr-4 pill-right pill-left" onclick="add();">
                    Tambah
                  </button>
                </div>
              </div>

            </div>
          </div>
          <div class="card-body">
            <!-- <h5 class="card-title text-white" style="font-family: segoeuib">Data Kelas</h5> -->
            <!-- <hr/> -->
           <!--  <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a> -->

            

            <form id="search-form">
            <div class="form-group row">
              <div class="col-md-6">
                  <div class="input-group ">
                    <input name="filter_keyword" type="text" class="form-control form-control-sm" autocomplete="off" placeholder="Ketik Parameter Pencarian">
                  </div>
              </div>
              <div class="col-md-3 offset-3"> 
              </div>  
            </div>  
            </form>

            <div class="row">
              <div class="col-sm-12">
                <table id="tbl-data" class="table table-striped table-hover table-sm" >
                  <thead class="thead-lights text-white" style="background-color: #487d95!important;font-family: segoeui;">
                    <tr>
                      <!-- <th scope="col">#</th>
                      <th scope="col">Nama Alternatif</th> -->
                        <th width="10px">No</th>
                        <th width="10px">Kelas</th>
                        <th width="10px">Jurusan</th>
                        <th width="10px">Sub Kelas</th>
                        <th width="10px">Wali Kelas</th>
                        <th width="10px">Action</th>
                    </tr>
                  </thead>
                  <tbody class="bg-light">
                    
                    <!-- <php 
                      foreach ($alternatif as $key => $value) {
                        echo "<tr>";
                          echo "<td>{$key}</td>";
                          echo "<td>{$value}</td>";
                        echo "</tr>";
                      }
                    ?> -->

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog pill text-white" role="document">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="font-family: segoeui;">Tambah Data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form">  

            <div id="modal-form-msg" class="alert alert-danger " role="alert">
              <strong>Holy guacamole!</strong> You should check in on some of those fields below.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Tingkat</label>
                    <select name="tingkat" class="form-control select-style">
                        <option value="">Pilih</option>
                        <option value="X">X</option>
                        <option value="XI">XI</option>
                        <option value="XII">XII</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                  <label>Sub Tingkat</label>
                  <input name="sub_tingkat" type="number" class="form-control" id="" placeholder="Tentukan Sub Tingkat" autocomplete="off">
                </div>
            </div> 

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>Jurusan/Peminatan</label>
                    <input name="jurusan" type="text" class="form-control" id="" placeholder="Tentukan Jurusan/Peminatan">
                </div>
            </div> 

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>Wali Kelas</label>
                    <input name="wali_kelas" type="text" class="form-control" id="" placeholder="Tentukan Wali Kelas">
                </div>
            </div> 


            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light  bg-head pill pl-4 pr-4" data-dismiss="modal">CLOSE</button>
            <button id="btn-submit" type="button" class="btn btn-light btn-secondary pill pl-4 pr-4 text-white" onclick="save()">SUBMIT</button>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script src="<?php echo base_url('assets/js/kelas-script.js?yess')?>"></script> 
</body>

</html>