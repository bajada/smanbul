<?php $this->load->view('inc/header');?>

    <div class="container-fluid mt-3">

      <div class="media bg-head pill pl-5 pt-3  pb-3">
        <img src="http://sman1-cibungbulang.sch.id/assets/img/smanbul.png" style="width: 100px;" class="ml-3 mr-3" alt="GAMBAR HERE">
        <div class="media-body mt-auto mb-auto">
          <h2 class="font-main">Sistem Informasi Pembayaran</h2>
          <h3 class="font-main">SMAN 1 CIBUNGBULANG</h3>
        </div>
        <div class="pr-5 pt-3  pb-3">
          <a href="<?php echo base_url('page/dashboard_view') ?>" class="mr-4 text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-home fa-4x" aria-hidden="true"></i> Home</a>
          <a href="<?php echo base_url('page/login_view') ?>" class="text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-globe fa-4x" aria-hidden="true"></i> Website</a>
        </div>
      </div>

          <div class="pt-2">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb" style="background-color:transparent;">
                <li class="breadcrumb-item"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">PENGATURAN</button></li>

                <li class="breadcrumb-item" aria-current="page">
                      <a href="" class="text-white-href" style="display: inline-flex;">
                      <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-users fa-stack-1x fa-inverse text-muted"></i>
                      </span>
                      <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Siswa</span>
                      </a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                      <a href="" class="text-white-href" style="display: inline-flex;">
                      <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-suitcase fa-stack-1x fa-inverse text-muted"></i>
                      </span>
                      <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span>
                      </a>
                </li>
              </ol>
            </nav>

          </div>

            <div class="row justify-content-center">
              <div class="col-md-8">

          <div class="card bg-login pill p-3 mt-1">
            <div class="card-body">
           
                <h3 class="card-title" style="font-family: segoeui;">Data Siswa Ke-1</h3>
            <hr/>
            <form id="form" class="form-horizontal">

            <div class="form-group row"> 
              <label class="col-md-3 text-left">NO.REF / Kelas</label>                      
              <div class="col-sm-4">
                <input name="ref" type="text" class="form-control" id="" placeholder="NO.REF">
              </div>                         
              <div class="col-sm-4">
                    <select name="hak_akses_user" class="form-control select-style">
                        <option value="0">Pilih Kelas</option>
                    </select>
              </div>   
            </div>
            <div class="form-group row">
              <label class="col-md-3 text-left">Nama Lengkap</label>                       
              <div class="col-sm-8">
                <input name="nama_lengkap" type="text" class="form-control" id="" placeholder="Nama Siswa">
              </div>
            </div>


            <div class="form-group row">
              <label class="col-md-3 text-left">IURAN </label>                       
              <div class="col-sm-8">
                    <div>
                      <span class="badge badge-secondary pill" onclick="$('#modal-form').modal('show');"><i class="fas fa-plus"></i></span>
                    </div>
                    <div>

                   <table id="tbl-data-iuran" class="table table-sm table-hovered table-hover" style="font-family: segoeui;border-radius: 20px!important;overflow: hidden;padding: 10px!important;">
                      <thead class="text-white" style="background-color: #487d95!important;">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Jenis IURAN</th>
                          <th scope="col">Besar IURAN</th>
                          <th scope="col">Cicilan</th>
                        </tr>
                      </thead>
                      <tbody><tr class="data-row" id="row-0"><td>1</td><td>IURAN_PER_BULAN</td><td>250000</td><td>0</td></tr><tr class="data-row" id="row-1"><td>2</td><td>DPMP</td><td>3000000</td><td>0</td></tr><tr class="data-row" id="row-2"><td>3</td><td>DU</td><td>300000</td><td>0</td></tr></tbody>
                    </table>
                  </div>
                </div>
            </div>  

              <hr/>

            </form>
                
              </div>
            </div>

          </div>

          <div class="col-md-4">

          <div class="card bg-login pill p-3 mt-1">
            <div class="card-body">
           
                <h3 class="card-title" style="font-family: segoeui;">Data Umum Siswa</h3>
            <hr/>
            <form id="form" class="form-horizontal">


            <div class="form-group row">
              <label class="col-md-4 text-left">Kelas</label>                       
                <div class="col-sm-8">
                    <input type="text" id="kelas" class="form-control" placeholder="X IPS 1" disabled>
                </div>              
            </div> 

            <div class="form-group row">
              <label class="col-md-4 text-left">Wali Kelas</label>                       
              <div class="col-sm-8">
                    <input type="text" id="wali_kelas" class="form-control" placeholder="Cecep Supriatna" disabled>
                </div>
            </div>  

            <div class="form-group row">
              <label class="col-md-4 text-left">Tahun Ajaran</label>                       
              <div class="col-sm-8">
                   <input type="text" id="wali_kelas" class="form-control" placeholder="2018/2019" disabled>
                </div>
            </div>  

              <hr/>

              </form>
                
              </div>
            </div>

          <div class="card bg-login pill p-3 mt-1">
            <div class="card-body">

            <h4 class="d-flex justify-content-between card-title" style="font-family: segoeui;">
            <span>IURAN SISWA</span>
            <span class="badge badge-secondary pill" onclick="$('#modal-form').modal('show');"><i class="fas fa-plus"></i></span>
            </h4>
              
            <hr/>

                
                <table id="tbl-data-iuran" class="table table-sm table-hovered table-hover" style="font-family: segoeui;border-radius: 20px!important;overflow: hidden;padding: 10px!important;">
              <thead class="text-white" style="background-color: #487d95!important;">
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Jenis IURAN</th>
                      <th scope="col">Besar IURAN</th>
                      <th scope="col">Cicilan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">1</th>
                      <td>IURAN/BULAN</td>
                      <td>250.000</td>
                      <td>-</td>
                    </tr>
                    <tr>
                      <th scope="row">2</th>
                      <td>DPMP</td>
                      <td>2.000.000</td>
                      <td>-</td>
                    </tr>
                    <tr>
                      <th scope="row">3</th>
                      <td>DU</td>
                      <td>300.000</td>
                      <td>-</td>
                    </tr>
                  </tbody>
                </table>
                <small class="text-justify">* Data tagihan, hanya dapat ditambahkan apabila data siswa telah di SUBMIT.</small>
              </div>

            </div>

            </div>
          </div>
    </div>

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog pill text-white" role="document">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="font-family: segoeui;">Form Tagihan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form">  

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Jenis Tagihan</label>
                    <select name="kelas" class="form-control select-style">
                        <option value="0">Pilih Jenis Tagihan</option>
                        <option value="1">IURAN</option>
                        <option value="2">DPMP</option>
                        <option value="2">DU</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label>Cicilan</label>
                    <select name="jurusan" class="form-control select-style">
                        <option value="0">Pilih Cicilan</option>
                        <option value="1">1x</option>
                        <option value="2">2x</option>
                        <option value="3">3x</option>
                        <option value="4">4x</option>
                        <option value="5">5x</option>
                        <option value="6">6x</option>
                        <option value="7">7x</option>
                        <option value="8">8x</option>
                        <option value="9">9x</option>
                        <option value="10">10x</option>
                        <option value="11">11x</option>
                        <option value="12">12x</option>
                    </select>
                </div>
            </div> 

            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="">Besar Tagihan</label>
                <input name="" type="text" class="form-control" id=""  placeholder="Jumlah Tagihan">
              </div>
            </div>   


            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5" data-dismiss="modal">CLOSE</button>
            <button id="btn-submit" type="button" class="btn btn-light btn-lg btn-secondary pill pl-5 pr-5 text-white">SUBMIT</button>
          </div>
        </div>
      </div>
    </div>

  <?php $this->load->view('inc/footer');?>
  <script src="<?php echo base_url('assets/js/form-siswa-script.js')?>"></script> 

  </body>
</html>