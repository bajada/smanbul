<?php $this->load->view('inc/header');?>
<!-- <php print_r($data) ?> -->
<!-- <php print_r($kriteria) ?> -->
    <div class="container-fluid mb-3">

    <div class="row justify-content-center mb-3">
      <div class="col-md-9">
        <div class="card d-nones" style="border-top-color: #0062cc;border-top-width: 2px;">
          <!-- <h5 class="card-header bg-dark text-white">Data</h5> -->
          <div class="card-body">
            <h5 class="card-title">Data</h5>
            <hr/>

            <div class="row">
              <div class="col-sm-12">

                
                <table class="table table-bordereds table-hover table-stripeds">
                  <thead class="thead-light">
                    <tr>
                      <th colspan="2" scope="col">Alternatif</th>
                      <th scope="col" colspan="5">Kriteria</th>
                    </tr>
                    <tr>
                      <th scope="col">Kode</th>
                      <th scope="col">Nama</th>
                      <?php 
                        foreach ($kriteria as $key => $value) {
                            echo "<th>{$value['nama_kriteria']}</th>";
                        }
                      ?>
                    </tr>
                  </thead>
                  <tbody>
                    <tr><td>A<sub>1</sub></td><td>Basket</td><td>4</td><td>3</td><td>1</td><td>1</td></tr><tr><td>A<sub>2</sub></td><td>Volly</td><td>3</td><td>2</td><td>4</td><td>2</td></tr><tr><td>A<sub>3</sub></td><td>Aikido</td><td>2</td><td>1</td><td>4</td><td>2</td></tr><tr><td>A<sub>4</sub></td><td>Futsal</td><td>3</td><td>1</td><td>4</td><td>3</td></tr>
                  </tbody>
                </table>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row justify-content-center mb-3">
      <div class="col-md-9">
        <div class="card d-nones" style="border-top-color: #0062cc;border-top-width: 2px;">
          <!-- <h5 class="card-header bg-dark text-white">Data</h5> -->
          <div class="card-body">
            <h5 class="card-title">Utility</h5>
            <hr/>

            <div class="row">
              <div class="col-sm-12">

                
                <table class="table table-bordereds table-hover table-stripeds">
                  <thead class="thead-light">
                    <tr>
                      <th colspan="2" scope="col">Alternatif</th>
                      <th scope="col" colspan="5">Kriteria</th>
                    </tr>
                    <tr>
                      <th scope="col">Kode</th>
                      <th scope="col">Nama</th>
                      <?php 
                        foreach ($kriteria as $key => $value) {
                            echo "<th>{$value['nama_kriteria']}</th>";
                        }
                      ?>
                    </tr>
                  </thead>
                  <tbody>

                    <tr><td>A<sub>1</sub></td><td>Basket</td><td>1</td><td>1</td><td>0</td><td>0</td></tr><tr><td>A<sub>2</sub></td><td>Volly</td><td>0.5</td><td>0.5</td><td>1</td><td>0.5</td></tr><tr><td>A<sub>3</sub></td><td>Aikido</td><td>0</td><td>0</td><td>1</td><td>0.5</td></tr><tr><td>A<sub>4</sub></td><td>Futsal</td><td>0.5</td><td>0</td><td>1</td><td>1</td></tr>
                  </tbody>
                </table>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row justify-content-center mb-3">
      <div class="col-md-9">
        <div class="card d-nones" style="border-top-color: #0062cc;border-top-width: 2px;">
          <!-- <h5 class="card-header bg-dark text-white">Data</h5> -->
          <div class="card-body">
            <h5 class="card-title">Nilai Akhir</h5>
            <hr/>

            <div class="row">
              <div class="col-sm-12">

                
                <table class="table table-bordereds table-hover table-stripeds">
                  <thead class="thead-light">
                    <tr>
                      <th colspan="2" scope="col">Alternatif</th>
                      <th scope="col" colspan="5">Kriteria</th>
                    </tr>
                    <tr>
                      <th scope="col">Kode</th>
                      <th scope="col">Nama</th>
                      <?php 
                        foreach ($kriteria as $key => $value) {
                            echo "<th>{$value['nama_kriteria']}</th>";
                        }
                      ?>
                      <th scope="col">Nilai Akhir</th>
                    </tr>
                  </thead>
                  <tbody>

                    <tr><td>A<sub>1</sub></td><td>Basket</td><td>0.4</td><td>0.15</td><td>0</td><td>0</td><td>0.55</td></tr><tr><td>A<sub>2</sub></td><td>Volly</td><td>0.2</td><td>0.075</td><td>0.35</td><td>0.05</td><td>0.675</td></tr><tr><td>A<sub>3</sub></td><td>Aikido</td><td>0</td><td>0</td><td>0.35</td><td>0.05</td><td>0.4</td></tr><tr><td>A<sub>4</sub></td><td>Futsal</td><td>0.2</td><td>0</td><td>0.35</td><td>0.1</td><td>0.65</td></tr>
                  </tbody>
                </table>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row justify-content-center mb-3">
      <div class="col-md-9">
        <div class="card d-nones" style="border-top-color: #0062cc;border-top-width: 2px;">
          <!-- <h5 class="card-header bg-dark text-white">Data</h5> -->
          <div class="card-body">
            <h5 class="card-title">Ranking</h5>
            <hr/>

            <div class="row">
              <div class="col-sm-12">

                
                <table class="table table-bordereds table-hover table-stripeds">
                  <thead class="thead-light">
                    <tr>
                      <th colspan="2" scope="col">Alternatif</th>
                      <th scope="col" colspan="5">Kriteria</th>
                    </tr>
                    <tr>
                      <th scope="col">Kode</th>
                      <th scope="col">Nama</th>
                      <th scope="col">Nilai Akhir</th>
                      <th scope="col">Rank</th>
                    </tr>
                  </thead>
                  <!-- <tbody> -->
                    <!-- <php 
                      // print_r($alternatif);
                      // print_r($sample);
                      // print_r($kriteria);
                      //just find this : echo "<td>{$sample[alternatif][kriteria]}</td>";

                      foreach ($alternatif as $key => $value) {
                          echo "<tr>";
                          echo "<td>A<sub>{$key}</sub></td>";
                          echo "<td>{$value['nama_alternatif']}</td>"; 
                          foreach ($kriteria as $key2 => $value3) {
                            if(isset($data[$key][$key2]))
                            echo "<td>{$data[$key][$key2]}</td>";
                            else
                            echo "<td>0</td>";
                          }
                          echo "</tr>";
                      }
                    ?> -->

                  <!-- </tbody> -->
                  <tbody>

                                        <tr><td>A<sub>2</sub></td><td>Volly</td><td>0.675</td><td>1</td></tr><tr><td>A<sub>4</sub></td><td>Futsal</td><td>0.65</td><td>2</td></tr><tr><td>A<sub>1</sub></td><td>Basket</td><td>0.55</td><td>3</td></tr><tr><td>A<sub>3</sub></td><td>Aikido</td><td>0.4</td><td>4</td></tr>                  
                  </tbody>
                </table>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    </div>


    <?php $this->load->view('inc/footer');?>
    <!-- <script src="<php echo base_url('assets/js/kriteria-js.js')?>"></script>  -->
</body>

</html>