<?php $this->load->view('inc/header');?>

    <div class="container-fluid mb-3">
    <div class="row justify-content-center">
      <div class="col-md-9">
        <div class="card d-nones" style="border-top-color: #0062cc;border-top-width: 2px;">
          <!-- <h5 class="card-header bg-dark text-white">Data</h5> -->
          <div class="card-body">
            <h5 class="card-title">Data</h5>
            <hr/>
           <!--  <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a> -->

            <form id="search-form">
            <div class="form-group row">
              <div class="col-sm-6">
                  <div class="input-group ">
                    <input name="filter_keyword" type="text" class="form-control" placeholder="Ketik Parameter Pencarian">
                    <div class="input-group-append">
                      <button class="btn btn-outline-secondary" type="submit">Cari</button>
                    </div>
                  </div>
              </div>
                    <div class="col-sm-6 text-right">
                      <div class="btn-group" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-primary" onclick="window.location.href='DataController/entri'">
                          Tambah
                        </button>
                      </div>
                    </div>
            </div>  
            </form>

            <div class="row">
              <div class="col-sm-12">

                <table id="tbl-data" class="table table-bordereds table-hover">
                  <thead class="thead-light">
                    <tr>
                        <th width="10px">No</th>
                        <th>Siswa</th>
                        <th>Rekomendasi Eskul</th>
                        <th>Data</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script src="<?php echo base_url('assets/js/data-siswa-js.js')?>"></script> 
</body>

</html>