<?php $this->load->view('inc/header');?>
<style type="text/css">
 /*
*
* ==========================================
* CUSTOM UTIL CLASSES
* ==========================================
*
*/
/*.table-fixed tbody {
    height: 300px;
    overflow-y: auto;
    width: 100%;
}

.table-fixed thead,
.table-fixed tbody,
.table-fixed tr,
.table-fixed td,
.table-fixed th {
    display: block;
}

.table-fixed tbody td, .table-fixed tbody th, .table-fixed thead > tr > th {
   float: left;
   position: relative;
}
 .table-fixed tbody td::after, .table-fixed tbody th::after, .table-fixed thead > tr > th::after {
   content: '';
   clear: both;
   display: block;
}
 */

</style>
  <div class="pt-2">

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color:transparent;">
        <li class="breadcrumb-item"><button onclick="window.history.back()" type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Kembali</button></li>

        <li class="breadcrumb-item" aria-current="page">

            <!-- <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-hand-holding-usd fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span></a> -->

              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-file-alt fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Tahunan</span>
              </a>
        </li>
      </ol>
    </nav>

  </div>

  <div class="row justify-content-center">
  <div class="col-md-6">
    <form id="search-form">
      <div class="form-group">
      <div class="input-group input-group-lg">
      <div class="input-group-prepend">
        <span class="input-group-text mr-1">Tahun/Kelas</span>
      </div>
      <select name="filter_tahun" class="form-control select-styles">
          <option value="">Pilih Tahun Ajaran</option>
      </select>

      <select name="filter_kelas" class="form-control select-styles ml-1">
          <option value="">Pilih Kelas</option>
          <option value="X">X</option>
          <option value="XI">XI</option>
          <option value="XII">XII</option>
      </select>
      <div class="input-group-append ml-1">
        <button class="btn btn-secondary" type="submit" id="btn-cari">Cari/Filter</button>
      </div>
      </div>
      </div> 
    </form>
  </div>
  </div>

  <div class="row justify-content-center">
      <div class="col-sm-12">
        <div class="card bg-head pill">
          <div class="card-body">
            <h5 class="card-title">
              <div class="row">
                <div class="col-md-6">
                  <span style="font-family: segoeui;">Data Laporan Tahunan</span>
                </div>
                <div class="col-md-6">
                  <button id="btn-export" type="button" class="btn btn-secondary pill pl-4 pr-4 float-right"><i class="fa fa-arrow-down"></i> DOWNLOAD(.xlsx)</button>
                </div>
              </div>
            </h5>
            <hr/>
            <div class="row">
              <div class="col-sm-12">
                  <div class="table-responsive" style="overflow-y: scroll;max-height: 600px;">
                    <table id="tbl-data" class="table table-bordered table-sm table-fixed" style="width: 700%">
                      <thead class="text-white" style="background-color: #487d95!important;font-family: segoeui;">
                        <tr id="thead-sub-tunggakan">
                          <th class="text-center align-middle" rowspan="3" width="50">No</th>
                          <th class="text-center align-middle" rowspan="3" width="50">REF</th>
                          <th class="text-center align-middle" rowspan="3" width="250">NAMA SISWA</th>
                          <th class="text-center align-middle" rowspan="3" width="50">JK</th>
                          <th class="text-center align-middle" rowspan="3" width="50">KELAS</th>
                          <th class="text-center align-middle" rowspan="3" width="100">WALI KELAS</th>
                          <th class="text-center align-middle" rowspan="3">BESAR SPP</th>
                          <th class="text-center" colspan="12">BULAN KE</th>
                          <th class="text-center align-middle" rowspan="3">JUMLAH <br>BAYAR <br>SPP</th>
                          <th class="text-center align-middle" rowspan="3">SISA SPP</th>
                          <th class="text-center bg-danger" colspan="9">DATA TUNGGAKAN KEUANGAN TAHUN LALU</th>
                          <th class="text-center align-middle bg-danger" rowspan="3">JUMLAH <br>TUNGGAKAN <br>DAFTAR ULANG</th>
                          <th class="text-center align-middle" rowspan="3">TOTAL</th>
                          <th class="text-center align-middle" rowspan="3">KETERANGAN</th>
                          <th class="text-center align-middle" rowspan="3">DSP</th>

                          <th class="text-center" colspan="24">BULAN</th>
                          <th class="text-center align-middle" rowspan="3">JUMLAH BAYAR DSP</th>
                          <th class="text-center align-middle" rowspan="3">SISA DSP</th>
                          <th class="text-center align-middle" rowspan="3">KET</th>
                          <th class="text-center align-middle" rowspan="3">JUMLAH BYR SPP 1 TH</th>
                          <th class="text-center align-middle" rowspan="3">TUNGGAKAN SPP KELAS XI</th>
                          <th class="text-center align-middle" rowspan="3">KELAS X</th>
                        </tr>
                        <tr id="thead-sub-tunggakan-1">
                          <th class="text-center">1</th>
                          <th class="text-center">2</th>
                          <th class="text-center">3</th>
                          <th class="text-center">4</th>
                          <th class="text-center">5</th>
                          <th class="text-center">6</th>
                          <th class="text-center">7</th>
                          <th class="text-center">8</th>
                          <th class="text-center">9</th>
                          <th class="text-center">10</th>
                          <th class="text-center">11</th>
                          <th class="text-center">12</th>

                         
                          <th id="thead-tunggakan-x" class="text-center" colspan="3">DU TAHUN (X)</th>
                          <th id="thead-tunggakan-xi" class="text-center " colspan="3">DU TAHUN (XI)</th>
                          <th id="thead-tunggakan-xii" class="text-center" colspan="3">DU TAHUN (XII)</th>

                          <th class="text-center" colspan="2">JUL 2018</th>
                          <th class="text-center" colspan="2">AUG 2018</th>
                          <th class="text-center" colspan="2">SEP 2018</th>
                          <th class="text-center" colspan="2">OKT 2018</th>
                          <th class="text-center" colspan="2">NOV 2018</th>
                          <th class="text-center" colspan="2">DEC 2018</th>
                          <th class="text-center" colspan="2">JAN 2019</th>
                          <th class="text-center" colspan="2">FEB 2019</th>
                          <th class="text-center" colspan="2">MAR 2019</th>
                          <th class="text-center" colspan="2">APR 2019</th>
                          <th class="text-center" colspan="2">MEI 2019</th>
                          <th class="text-center" colspan="2">JUN 2019</th>
                          

                        </tr>
                        <tr id="thead-sub-tunggakan-2">
                          <th class="text-center">JU</th>
                          <th class="text-center">AG</th>
                          <th class="text-center">SEP</th>
                          <th class="text-center">OK</th>
                          <th class="text-center">NO</th>
                          <th class="text-center">DE</th>
                          <th class="text-center">JAN</th>
                          <th class="text-center">FEB</th>
                          <th class="text-center">MAR</th>
                          <th class="text-center">APR</th>
                          <th class="text-center">MEI</th>
                          <th class="text-center">JUN</th>

                          <th class="text-center">OSIS</th>
                          <th class="text-center">Komputer</th>
                          <th class="text-center">SPP</th>

                          <th class="text-center">OSIS</th>
                          <th class="text-center">Komputer</th>
                          <th class="text-center">SPP</th>

                          <th class="text-center">OSIS</th>
                          <th class="text-center">Komputer</th>
                          <th class="text-center">SPP</th>

                          <th class="text-center">TGL</th>
                          <th class="text-center">BAYAR</th>
                          <th class="text-center">TGL</th>
                          <th class="text-center">BAYAR</th>
                          <th class="text-center">TGL</th>
                          <th class="text-center">BAYAR</th>
                          <th class="text-center">TGL</th>
                          <th class="text-center">BAYAR</th>
                          <th class="text-center">TGL</th>
                          <th class="text-center">BAYAR</th>
                          <th class="text-center">TGL</th>
                          <th class="text-center">BAYAR</th>
                          <th class="text-center">TGL</th>
                          <th class="text-center">BAYAR</th>
                          <th class="text-center">TGL</th>
                          <th class="text-center">BAYAR</th>
                          <th class="text-center">TGL</th>
                          <th class="text-center">BAYAR</th>
                          <th class="text-center">TGL</th>
                          <th class="text-center">BAYAR</th>
                          <th class="text-center">TGL</th>
                          <th class="text-center">BAYAR</th>
                          <th class="text-center">TGL</th>
                          <th class="text-center">BAYAR</th>
                        </tr>
                      </thead>
                      <tbody class="text-dark">

                      </tbody>
                      <tfoot class="text-white" style="background-color: #487d95!important;">
                        <tr>
                          <th class="text-center" colspan="7">Jumlah</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                          <th>0</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script src="<?php echo base_url('assets/js/laporan-tahunan-script.js?yess')?>"></script> 

  </body>
</html>