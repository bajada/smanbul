
<link rel='stylesheet' type='text/css' href='<?php echo base_url('assets/application.css')?>'>
	<table width="650" border="0" cellpadding="3" cellspacing="3" bgcolor="white">
		<tr>
			<td width="100">
				 <img src="http://sman1-cibungbulang.sch.id/assets/img/smanbul.png" style="width: 100px;" class="ml-3 mr-3" alt="GAMBAR HERE">
			</td>
			<td align="center">
				<font face="Times New Roman, Times, serif">
					<b>
						<font size="4">SMA 1 NEGERI CIBUNGBULANG</font>
						<br />
						<font size="5">Biro Administrasi Sumberdaya dan Kerjasama</font>
						<br />
					</b>
					<font size="3">Jl.Kapten Dasuki Bakri No.18, Cibatok 1, Cibungbulang, Bogor, Jawa Barat 16630. Telepon/Fax: (0251) 8645033</font>
				</font>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<hr />
			</td>
		</tr>
	</table>
	<table width="650" border="0" cellpadding="3" cellspacing="3" bgcolor="white">
		<tr>
			<td align="center">
				<span class="Title">Rincian Pembayaran </span>
			</td>
		</tr>
		<tr height="5">
			<td></td>
		</tr>
		<tr>
			<td align="center">
				<table>
					<tr>
						<td width="190">Nama Siswa</td>
						<td width="15">:</td>
						<td width="190">
							<b>KAYUM MUNAJIR</b>
						</td>
						<td width="30">&nbsp;</td>
						<td width="65">Tahun Ak</td>
						<td width="15">:</td>
						<td>2018/2019</td>
					</tr>
					<tr>
						<td>Program Studi</td>
						<td>:</td>
						<td>FT_TI &mdash; Teknik Informatika</td>
						<td>&nbsp;</td>
						<td>Semester</td>
						<td>:</td>
						<td>GENAP (8)</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center">
				<table width='600' class='List'>
					<tr class='Header'>
						<td align='center'>#</td>
						<td valign='top'>Item Pembayaran&nbsp;</td>
						<td valign='top'>Nominal (Rp)&nbsp;</td>
					</tr>
					<tr class='RowLight' onmouseover='javascript:OnMouseOver(this);' onmouseout='javascript:OnMouseOut(this);'>
						<td align='center' valign='top'>&nbsp;1&nbsp;</td>
						<td valign='top' align='left'>1200 - S K S&nbsp;</td>
						<td valign='top' align='right'>900,000&nbsp;</td>
					</tr>
					<tr class='RowDark' onmouseover='javascript:OnMouseOver(this);' onmouseout='javascript:OnMouseOut(this);'>
						<td align='center' valign='top'>&nbsp;2&nbsp;</td>
						<td valign='top' align='left'>1210 - Ujian Semester&nbsp;</td>
						<td valign='top' align='right'>450,000&nbsp;</td>
					</tr>
					<tr class='RowLight' onmouseover='javascript:OnMouseOver(this);' onmouseout='javascript:OnMouseOut(this);'>
						<td align='center' valign='top'>&nbsp;3&nbsp;</td>
						<td valign='top' align='left'>6032 - Kerja Praktik&nbsp;</td>
						<td valign='top' align='right'>300,000&nbsp;</td>
					</tr>
					<tr class='RowDark' onmouseover='javascript:OnMouseOver(this);' onmouseout='javascript:OnMouseOut(this);'>
						<td align='center' valign='top'>&nbsp;4&nbsp;</td>
						<td valign='top' align='left'>6033 - Seminar Proposal&nbsp;</td>
						<td valign='top' align='right'>300,000&nbsp;</td>
					</tr>
				</table>
				<table width='600' class='List'>
					<tr height='10' class='Navigation'></tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center">
				<table width="600">
					<tr height="25" bgcolor="#EEEEEE">
						<td align="right" width="449">
							<b>J U M L A H :</b>
						</td>
						<td align="right">
							<b>1,950,000</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center">
				<table width="600">
					<tr>
						<td width="350" valign="top">
							<font face="Arial" size="1">
								<u>Catatan</u>:
								<br />
								<br/>Pembayaran dapat dilakukan melalui transfer Via ATM/Mobile Banking: 
								<br/>1). No. Rek VA Bank Muamalat Indonesia  
								<b>8169991505150974</b>
								<br/>2). No. Rek VA Bank Syariah Mandiri 
								<b>9009045151105150974</b>
								<br/>3). Kendala Teknis VA bisa menghubungi 
								<b>08561942487</b> (WA Only) 
							</font>
							<br />
							<p align="center"> 20190315150049 
								<img src="https://siak.uika-bogor.ac.id/lib/barcode.inc.php?code=151105150974" /> 89BEFF8EF3CCB623144FBC4D1F70C6C9 
							</p>
						</td>
						<td width="250" valign="top"> Bogor, 15-03-2019
							<br /> a.n. Kepala BASK
							<br /> Kasubbag. Keuangan
							<br />
							<img src="tpl/printer/ttd_bauk_keuangan.png" width="100" />
							<p>(Gatot Darmadi, S.Kom)</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<!-- <script language='javascript'>window.print()</script> -->