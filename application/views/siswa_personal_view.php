<?php $this->load->view('inc/header');?>

  <div class="pt-2">

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color:transparent;">
        <!-- <li class="breadcrumb-item"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Kembali</button></li> -->

        <li class="breadcrumb-item" aria-current="page">
              <a href="siswa_personal_view" class="text-white-href" style="display: inline-flex;">
                <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fas fa-users fa-stack-1x fa-inverse text-muted"></i>
                </span>
                <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Personal Siswa</span>
              </a>
              <a href="siswa_view" class="text-white-href" style="display: inline-flex;">
                <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fas fa-chalkboard-teacher fa-stack-1x fa-inverse text-muted"></i>
                </span>
                <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Kelas Siswa</span>
              </a>
        </li>
       <!--  <li class="breadcrumb-item" aria-current="page">
              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-suitcase fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span>
              </a>
        </li> -->
      </ol>
    </nav>

  </div>



    <div class="row justify-content-center">
      <div class="col-md-11">

  <div class="card bg-head pill ">
    <div class="card-header text-white" style="background-color: #487d95!important;font-family: segoeui;">
    <div class="row">
      <div class="col-md-7"> <h5 style="font-family: segoeui;">Data Siswa</h5></div>
      <div class="col-md-3 offset-2">
        <div class="float-right">
          <form class="form-inline">
            <select class="custom-select my-1 mr-sm-2" id="multi-action" disabled="true">
              <!-- <option value="" selected="">Choose Action...</option>
              <option value="1">Edit Siswa</option>
              <option value="2">Hapus Siswa</option> -->
            </select>
            <div>
            <button type="button" class="btn btn-danger my-1" id="btn-batal-action" style="display: none">Batal</button>
            <button type="button" class="btn btn-light my-1" id="btn-apply-action" style="display: none">Update</button>
            </div>
          </form>
        </div>
      </div>
      <!-- <div class="col-md-6">
        <a href="<?php echo base_url('page/form_siswa_view') ?>" type="" class="btn btn-light btn-lg btn-light pill pl-5 pr-5 float-right">Tambah</a>
      </div> -->
    </div>
    </div>

    <div class="card-body">

    <form id="search-form">
      <div class="form-group row">
        <div class="col-md-4">
          <input onkeyup="display();" name="filter_keyword" type="text" class="form-control" autocomplete="off" placeholder="Ketik Nama/No.Ref">
        </div>
        <div class="col-md-2 offset-4">
            <select name="filter_status_aktif" class="form-control" onchange="filter()">
                <option value="1">AKTIF</option>
                <option value="0">NON AKTIF</option>
            </select> 
        </div>
        <div class="col-md-2">
            <select name="filter_tahun_ajaran" onchange="filter();" id="tahun_ajaran" class="form-control">
              <option>Semua Angkatan</option>
            </select>
        </div>
      </div>
    </form>

    <div class="row">
      
      <div class="col-12">
        <div class="tab-content" id="v-pills-tabContent">
          <div class="tab-pane fade show active" id="v-pills-kelas-none">
                <form id="form-data" method="POST">
                <div class="row">
                  <div class="col-md-12 pill-top " style="height: 500px;overflow-x: scroll;">
                  <table id="tbl-data" class="table table-striped table-responsive" style="font-family: segoeui;opacity: 0.8;display: table;">
                    <thead class="text-white" style="background-color: #487d95!important;">
                      <tr>
                        <th width="4%"><div class="form-check"><input id="check_all" class="form-check-input" style="position:unset" type="checkbox"></div></th>
                        <!-- <th width="8%">No</th> -->
                        <th width="10%">No.REF</th>
                        <th>Nama Lengkap</th>
                        <th>Jenis Kelamin</th>
                        <th>Angkatan</th>
                        <th>Keterangan</th>
                      </tr>
                    </thead>
                    <tbody class="text-dark bg-light"></tbody>
                  </table>
                  </div>
                </div>
                <div class="row no-gutters px-1 py-3 align-items-center">
                    <div class="col pl-3">
                        <div class="dataTables_info" id="info-tabel">1 to 8 Items of 11 — <a href="#!" class="font-weight-semi-bold"> view all <span class="fa fa-angle-right"></span></a>
                        </div>
                    </div>
                    <div class="col-auto pr-3 d-none">
                        <div class="dataTables_paginate paging_simple" id="DataTables_Table_0_paginate">
                            <ul class="pagination pagination-sm">
                                <li class="paginate_button page-item previous disabled" id="DataTables_Table_0_previous"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>
                                <li class="paginate_button page-item next" id="DataTables_Table_0_next"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0" class="page-link">Next</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </form>
          </div>
        </div>
      </div>
      
    </div>

    </div>
  </div>
</div>
</div>
    </div>

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md pill text-white" role="document" styles="max-width: 700px">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="font-family: segoeuib">Tambah Siswa Baru</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form-siswa">
              <div class="form-row justify-content-center">
                <div class="form-group col-md-6">
                  <label for="">Jumlah Siswa</label>
                  <input name="jumlah_siswa" type="text" class="form-control" autocomplete="off"  placeholder="Masukkan Jumlah Data">
                </div>
                
               <div class="form-group col-md-6">
                  <label>Kelas</label>
                  <select name="kelas" class="form-control select-styles">
                      <option value="0">Pilih kelas</option>
                  </select>
                </div> 
              </div>  

              <button id="btn-submit-2" type="submit" class="btn btn-secondary pill pl-4 pr-4 float-right d-none">SUBMIT</button>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light bg-head pill pl-4 pr-4" data-dismiss="modal">CLOSE</button>
            <button onclick="$('#btn-submit-2').click()" type="button" class="btn btn-secondary btn-secondary pill pl-4 pr-4">SUBMIT</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap modal import -->
    <div class="modal fade" id="modal-form-import" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog pill text-white" role="document">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="font-family: segoeuib">Import Data Siswa</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="<?php echo base_url('siswaController/uploadData') ?>" method="post" enctype="multipart/form-data">
              <div class="form-group text-white">
                <label for="">Upload file(.xlsx)</label>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile" name="uploadFile" onchange="$('#custom-file-name').text(this.files[0].name)">
                    <label id="custom-file-name" class="custom-file-label" for="customFile">Choose file</label>
                  </div>
                <small id="emailHelp" class="form-text"><a class="text-white" href="<?php echo base_url("excel/template_excel_new.xlsx"); ?>">Klik disini, untuk mendownload template.</a></small>
              </div>
             <!--  <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Upload file(.xlsx)</label>
                <div class="col-sm-10">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile" name="uploadFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                  </div>
                </div>
              </div> -->
                <!-- <input type="file" name="uploadFile" value="" /><br><br> -->
                <input class="d-none" type="submit" name="preview" value="Preview" />
            </form>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light bg-head pill pl-4 pr-4" data-dismiss="modal">CLOSE</button>
            <button onclick="$('[name=preview]').click()" type="button" class="btn btn-secondary btn-secondary pill pl-4 pr-4">SUBMIT</button>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script src="<?php echo base_url('assets/js/siswa-personal-script.js?yesss')?>"></script>

  </body>
</html>