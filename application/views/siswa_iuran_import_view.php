<?php $this->load->view('inc/header');?>
<?php
  function cek_kelas($data, $val){
    $status=true;
    foreach ($data as $key => $value) {
      $new_val1=preg_replace('/\s+/', ' ', trim($val));
      $new_val2=preg_replace('/\s+/', ' ', trim($value->kelas));
       if($new_val1==$new_val2){
        $status=false;
       }
     } 
     return $status;
  }
  function cek_siswa($data, $val){
    $status="";
    foreach ($data as $key => $value) {
      $new_val1=preg_replace('/\s+/', ' ', trim($val));
      $new_val2=preg_replace('/\s+/', ' ', trim($value->ref_siswa));
       if($new_val1==$new_val2){
        $status=$value->id_siswa;
       }
     } 
     return $status;
  }
  function cek_siswa_kelas($data, $val, $val2){
    $status="";
    foreach ($data as $key => $value) {
      $new_val1=preg_replace('/\s+/', ' ', trim($val));
      $new_val2=preg_replace('/\s+/', ' ', trim($val2));
      $new_val3=preg_replace('/\s+/', ' ', trim($value->header_id_siswa_kelas));
      $new_val4=preg_replace('/\s+/', ' ', trim($value->kelas));
       if($new_val1==$new_val3 && $new_val2==$new_val4){
        $status=$value->id_siswa_kelas;
       }
     } 
     return $status;
  }


  function has_dupes($array, $val2) {
    $count=0;
    $dup=[];
    $obj=[];
    foreach ($array as $val) {
      if(empty($val['A'])) continue;
      if($val['A'] != "REF"){
        if($val['A']==$val2){
          $count++;
        }
      }
    }
    return $count;
  }

  function array_has_dupes($array) {
     // streamline per @Felix
     return count($array) !== count(array_unique($array));
  }

 ?>

  <div class="pt-2">

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color:transparent;">
        <li class="breadcrumb-item"><a href="dashboard_view" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Kembali</a></li>

        <li class="breadcrumb-item" aria-current="page">

            <!-- <a style="font-size: 2.5rem;" href="" class="text-white-href"><i class="fas fa-hand-holding-usd fa-fw mr-3"></i><span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span></a> -->

              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-users fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Siswa</span>
              </a>
        </li>
       <!--  <li class="breadcrumb-item" aria-current="page">
              <a href="" class="text-white-href" style="display: inline-flex;">
              <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-suitcase fa-stack-1x fa-inverse text-muted"></i>
              </span>
              <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran Siswa</span>
              </a>
        </li> -->
      </ol>
    </nav>

  </div>



    <div class="row justify-content-center">
      <div class="col-md-11">

  <div class="card bg-head pill ">
    <div class="card-header text-white" style="background-color: #487d95!important;font-family: segoeui;">
    <div class="row">
      <div class="col-md-7"> <h5 style="font-family: segoeui;">Preview Data Siswa</h5></div>
      <div class="col-md-3 offset-2">
        <button onclick="$('#modal-form-import').modal('show');" type="button" class="btn btn-light btn-light pill pl-4 pr-4 float-right">Import</button> 
      </div>
      <!-- <div class="col-md-6">
        <a href="<?php echo base_url('page/form_siswa_view') ?>" type="" class="btn btn-light btn-lg btn-light pill pl-5 pr-5 float-right">Tambah</a>
      </div> -->
    </div>
    </div>

    <div class="card-body">
    <!-- <h3 class="card-title">
    <div class="row">
      <div class="col-md-6"> <h5 style="font-family: segoeui;">Data Siswa</h5></div>
      <div class="col-md-6"><button onclick="$('#modal-form').modal('show');" type="button" class="btn btn-light btn-light pill pl-4 pr-4 float-right">Tambah</button> <button onclick="goToEdit()" type="button" class="btn btn-light btn-light pill pl-4 pr-4 float-right mr-1">Action</button></div>
     
    </div>

    </h3> -->
      
    <!-- <hr/> -->

    <form id="search-form">
      <div class="form-group row">
        <div class="col-md-4">
          <input name="filter_keyword" type="text" class="form-control" autocomplete="off" placeholder="Ketik Nama/No.Ref/Peminatan Jurusan">
        </div>
        <div class="col-md-2 offset-4">
            <select name="filter_kelas" class="form-control" onchange="display()">
                <option value="0">Semua Kelas</option>
                <option value="X">X</option>
                <option value="XI">XI</option>
                <option value="XII">XII</option>
                <!-- <option value="ALUMNI">ALUMNI</option> -->
            </select> 
        </div>
        <div class="col-md-2">
            <select name="filter_tahun_ajaran" onchange="filter();" id="tahun_ajaran" class="form-control">
              <option>Pilih Tahun Ajaran</option>
            </select>
        </div>
      </div>
    </form>

    <div class="" id="jumlah_kosong" style="color: yellow;display: none">
      <ul id="msg-row"></ul>
    </div>

    <script src="<?php echo base_url('assets/jquery/jquery.min.js')?>"></script>
    <script type="text/javascript">var container=$("#jumlah_kosong");</script>

    <div class="row">
      <!-- <div class="col-3">
        <div class="row">
          <div class="col-md-11 pill-top p-3" style="background-color: #487d95!important;font-family: segoeui;opacity: 0.8;">
            <span class="col-form-label">Kelas <select onchange="filter();" id="tahun_ajaran" class="float-right"><option>Pilih Tahun Ajaran</option></select></span>
          </div>
        </div>

        <div class="row">
          <div class="col-md-11 pill-top p-0" style="font-family: segoeui;opacity: 0.8;">
          <table id="tbl-data-kelas" class="table-responsive card-list-table table-pills" style="font-family: segoeui;opacity: 0.8;display: table;">
            <tbody class="text-dark">
              <tr data-tingkat="ALL" class="" onclick="filter('tingkat_kelas=ALL&');"><td>SEMUA KELAS</td></tr>
              <tr data-tingkat="X" class="" onclick="filter('tingkat_kelas=X&');"><td>X</td></tr>
              <tr data-tingkat="XI" class="" onclick="filter('tingkat_kelas=XI&');"><td>XI</td></tr>
              <tr data-tingkat="XII" class="" onclick="filter('tingkat_kelas=XII&');"><td>XII</td></tr>
              <tr data-tingkat="ALUMNI" class="" onclick="filter('tingkat_kelas=ALUMNI&');"><td>ALUMNI</td></tr>
            </tbody>
          </table>
        </div>
        </div>
      </div> -->

      <div class="col-12">
        <div class="tab-content" id="v-pills-tabContent">
          <div class="tab-pane fade show active" id="v-pills-kelas-none">

                <div class="row">
                  <div class="col-md-12 pill-top table-responsive" style="height: :600px;">

                  <!-- <table id="tbl-data" class="table table-hover table-responsive table-striped table-bordered" style="font-family: segoeui;opacity: 0.8;display: table;">
                    <thead class="text-white" style="background-color: #487d95!important;">
                        <tr>
                          <th class="text-center" rowspan="2">NAMA LENGKAP</th>
                          <th class="text-center" rowspan="2">JK</th>
                          <th class="text-center" colspan="3">IURAN</th>
                          <th class="text-center" rowspan="2">Kelas</th>
                        </tr>
                        <tr>
                          <th>BULANAN</th>
                          <th>DPMP</th>
                          <th>DU</th>
                        </tr>
                    </thead>
                    <tbody class="text-dark"></tbody>
                  </table> -->
                  
                   <?php
                    if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form 
                      if(isset($upload_error)){ // Jika proses upload gagal
                        echo "<div style='color: yellow;'>".$upload_error."</div>"; // Muncul pesan error upload
                        die; // stop skrip
                      }

                      // Buat sebuah tag form untuk proses import data ke database
                      echo "<form method='post' action='".base_url("SiswaController/import_iuran_new")."'>";
                      
                      // Buat sebuah div untuk alert validasi kosong
                      // echo "<div style='color: red;' id='kosong'>
                      // Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
                      // </div>";
                      
                      echo "<table class='table table-sm table-hover table-responsive table-striped table-bordered bg-light text-dark' style='font-family: segoeui;opacity: 0.8;display: table; width:140%'>
                      
                      <thead class='text-white' style='background-color: #487d95!important;''>
                      <tr>
                        <th class='text-center align-middle' rowspan='2'>NO</th>
                        <th class='text-center align-middle' rowspan='2'>REF</th>
                        <th class='text-center align-middle d-none' rowspan='2'>ID_SISWA</th>
                        <th class='text-center align-middle d-none' rowspan='2'>ID_SISWA_KELAS</th>
                        <th class='text-center align-middle' rowspan='2'>NAMA</th>
                        <th class='text-center align-middle' rowspan='2'>KELAS</th>
                        <th class='text-center align-middle' colspan='2'>IURAN BULANAN</th>
                        <th class='text-center align-middle' colspan='2'>TABUNGAN BULANAN</th>
                        <th class='text-center align-middle' rowspan='2'>DPMP</th>
                        <th class='text-center align-middle' rowspan='2'>OSIS</th>
                        <th class='text-center align-middle' rowspan='2'>TOTAL</th>
                        <th class='text-center align-middle' rowspan='2'>SISA DPMP</th>
                        <th class='text-center align-middle' rowspan='2'>KET</th>
                        <th class='text-center align-middle' rowspan='2'>TANGGAL</th>
                        <th class='text-center align-middle' rowspan='2'>TAHUN AJARAN</th>
                      </tr>
                      <tr>
                        <th class='text-center'>BULAN</th>
                        <th class='text-center'>JUMLAH</th>
                        <th class='text-center'>BULAN</th>
                        <th class='text-center'>JUMLAH</th>
                      </tr>
                      </thead>
                      ";
                      
                      $numrow = 4;
                      $kosong = 0;
                      $have_duplicate = 0;
                      $have_wrong_data_db = 0;
                      $have_wrong_class = 0;
                      $have_quit_class = 0;
                      $have_lunas = 0;
                      $no=1;
                      
                      // Lakukan perulangan dari data yang ada di excel
                      // $sheet adalah variabel yang dikirim dari controller
                      // $ref=$ref_sheet;
                      sort($sheet);
                      foreach($sheet as $row){ 
                        // Ambil data pada excel sesuai Kolom
                        $col_1 = $row['A']; // Ambil data Kolom A
                        $col_2 = $row['B'];
                        $col_3 = $row['C']; 
                        $col_4 = $row['D'];
                        $col_5 = $row['E'];
                        $col_6 = $row['F'];
                        $col_7 = $row['G'];
                        $col_8 = $row['H'];
                        $col_9 = $row['I'];
                        $col_10 = $row['J'];
                        $col_11 = $row['K'];
                        $col_12 = $row['L'];
                        $col_13 = $row['M'];
                        $col_14 = $row['N'];

                        $status_r="";
                        // Cek jika semua data tidak diisi
                        if(empty($col_1) && empty($col_2))
                          continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)
                        
                        // Cek $numrow apakah lebih dari 2
                        // Artinya karena baris pertama adalah nama-nama kolom
                        // Jadi dilewat saja, tidak usah diimport
                        //if($numrow > 2){
                          // Validasi apakah semua data telah diisi
                          $nama_td = ( ! empty($col_1))? "" : " style='background: #E07171;'"; // Jika Nama kosong, beri warna merah
                          $jk_td = ( ! empty($col_2))? "" : " style='background: #E07171;'"; // Jika JK kosong, beri warna merah
                          
                          // Jika salah satu data ada yang kosong
                          /*if(empty($col_1) or empty($col_2)){
                            $kosong++; // Tambah 1 variabel $kosong
                          }*/
                          if($col_1 != 'REF'){

                            if($col_1 == "" || $col_2 == "" || $col_3 == "" || $col_13 == ""){
                              $kosong++;
                              $status_r=" bg-danger text-white";
                            }
                            else if(has_dupes($sheet, $col_1)>1){
                              // echo "<tr class='data-row "."bg-warning text-white"."'>";
                              //$status_r=" bg-warning text-white";
                              //$have_duplicate++;
                            }else{
                              if(cek_kelas($data_kelas, $col_3)){
                                // echo "<tr class='data-row "."bg-info text-white"."'>";
                                $status_r=" bg-info text-white";
                                $have_wrong_class++;
                              }
                            }

                            if($row['D_status'] == "LUNAS") {
                              $status_r=" bg-success text-white";
                              $have_lunas++;
                            }

                            
                            echo "<tr class='data-row ".$status_r."'>";
                            echo "<td class='text-center'>".$no."</td>";
                            echo "<td".$nama_td.">".$col_1."</td>";
                            // echo "<td class='text-center'>".cek_siswa($data_siswa, $col_1)."</td>";
                            // echo "<td class='text-center'>".cek_siswa_kelas($data_siswa_kelas, cek_siswa($data_siswa, $col_1), $col_3)."</td>";
                            echo "<td>".$col_2."</td>";
                            // echo "<td class='text-left'>".$col_3."</td>";
                            echo "<td>".$col_3."</td>";
                            echo "<td>".$col_4."</td>";
                            echo "<td>".$col_5."</td>";
                            echo "<td>".$col_6."</td>";
                            echo "<td>".$col_7."</td>";
                            echo "<td>".$col_8."</td>";
                            echo "<td>".$col_9."</td>";
                            echo "<td>".$col_10."</td>";
                            echo "<td>".$col_11."</td>";
                            echo "<td>".$col_12."</td>";
                            echo "<td>".$col_13."</td>";
                            echo "<td>".$col_14."</td>";
                          echo "</tr>";

                          $no++;

                          }
                        }
                        
                        $numrow++; // Tambah 1 setiap kali looping
                      //}
                      
                      echo "</table>";
                      
                      // Cek apakah variabel kosong lebih dari 0
                      // Jika lebih dari 0, berarti ada data yang masih kosong
                      if($kosong > 0){
                      ?>  
                        <script>
                          $(document).ready(function(){
                            $("#msg-row").append('<li class="text-white"><span class="bg-danger"><?php echo ($kosong); ?> record belum lengkap, pastikan telah diisi(REF, NAMA, KELAS, dan TANGGAL).</span></li>');
                            container.show();
                          });
                        </script>
                      <?php
                      }
                      if($have_duplicate > 0){
                      ?>  
                        <script>
                          $(document).ready(function(){
                            //$("#msg-row").append('<li class="text-white"><span class="bg-warning"><?php echo ($have_duplicate); ?> record duplikat/kesamaan record(<?php echo ($have_duplicate); ?> record in db).</span></li>');
                            //container.show();
                          });
                        </script>
                      <?php
                      }
                      /*if($have_wrong_data_db > 0){
                      ?>  
                        <script>
                          $(document).ready(function(){
                            $("#msg-row").append('<li class="text-white"><span class="bg-secondary"><?php echo ($have_wrong_data_db); ?> record data, belum ada di db.</span></li>');
                            container.show();
                          });
                        </script>
                      <?php
                      }*/
                      // if($have_quit_class > 0){
                      // >
                      //   <script>
                      //   $(document).ready(function(){
                      //     $("#msg-row").append('<li class="text-white"><span class="bg-info"><?php echo ($have_quit_class); > record data, yang akan KELUAR.</span></li>');
                      //     container.show();
                      //   });
                      //   </script>
                      // <php
                      // }
                      if($have_wrong_class>0){
                        ?>
                        <script>
                        $(document).ready(function(){
                          $("#msg-row").append('<li class="text-white"><span class="bg-info"><?php echo ($have_wrong_class); ?> record data, yang kelasnya tidak sesuai.</span></li>');
                          container.show();
                        });
                        </script>
                      <?php
                      }

                      if($have_lunas>0){
                        ?>
                        <script>
                        $(document).ready(function(){
                          $("#msg-row").append('<li class="text-white"><span class="bg-success"><?php echo ($have_lunas); ?> record data, telah melunasi pembayaran.</span></li>');
                          container.show();
                        });
                        </script>
                      <?php
                      }

                      if(empty($kosong) && empty($have_duplicate) && empty($have_wrong_class) && empty($have_lunas)){
                        // if($have_quit_class>0){
                        //   >
                        //   <script>
                        //   $(document).ready(function(){
                        //     $("#msg-row").append('<li class="text-white"><span class="bg-info"><?php echo ($have_quit_class); > record data, yang akan KELUAR.</span>');
                        //     container.show();
                        //   });
                        //   </script>
                        // <php
                        // }

                        echo "<hr>";
                        
                        // Buat sebuah tombol untuk mengimport data ke database
                        echo "<div class='row'>";
                        echo "<div class='col-md-12 text-right bg-head p-3 fixed-bottom'>";
                          echo "<button class='btn mr-2' type='button' id='btn-import'>Import</button>";
                          echo "<a class='btn btn-danger' href='".base_url("page/siswa_iuran_import_view")."'>Cancel</a>";
                        echo "</div>";
                        echo "</div>";

                        ?>
                        <script>
                        $(document).ready(function(){
                          //container.show();
                          var ctx="<?php echo base_url("page/siswa-view"); ?>";
                          var temp="<button class='btn mr-2' type='button' id='btn-import'>Import</button>"+
                          "<a class='btn btn-danger' href="+ctx+">Cancel</a>";
                          //$('#btn-action').html(temp);
                        });
                        </script>
                        <?php
                        // echo "<hr>";
                        // echo "<div class='row'>";
                        // echo "<div class='col-md-12 text-right bg-head p-3 fixed-bottom'>";
                        //   echo "<button class='btn mr-2' type='button' id='btn-import'>Import</button>";
                        //   echo "<a class='btn btn-danger' href='".base_url("page/siswa-view")."'>Cancel</a>";
                        // echo "</div>";
                        // echo "</div>";
                      }
                      
                      echo "</form>";
                    }
                    ?>
                  </div>
                </div>
          </div>
        </div>
      </div>
    </div>

    </div>
  </div>
</div>
</div>
    </div>

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md pill text-white" role="document" styles="max-width: 700px">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="font-family: segoeuib">Tambah Siswa Baru</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form-siswa">
              <div class="form-row justify-content-center">
                <div class="form-group col-md-6">
                  <label for="">Jumlah Siswa</label>
                  <input name="jumlah_siswa" type="text" class="form-control" autocomplete="off"  placeholder="Masukkan Jumlah Data">
                </div>
                
               <div class="form-group col-md-6">
                  <label>Kelas</label>
                  <select name="kelas" class="form-control select-styles">
                      <option value="0">Pilih kelas</option>
                  </select>
                </div> 
              </div> 

              <button id="btn-submit-2" type="submit" class="btn btn-secondary pill pl-4 pr-4 float-right d-none">SUBMIT</button>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light bg-head pill pl-4 pr-4" data-dismiss="modal">CLOSE</button>
            <button onclick="$('#btn-submit-2').click()" type="button" class="btn btn-secondary btn-secondary pill pl-4 pr-4">SUBMIT</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap modal import -->
    <div class="modal fade" id="modal-form-import" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog pill text-white" role="document">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="font-family: segoeuib">Import Data Iuran Siswa</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <form action="<?php echo base_url('siswaController/uploadDataIuran') ?>" method="post" enctype="multipart/form-data">
              <div id="form-upload-file" class="form-group">
                <label for="">Upload file(.xlsx)</label>
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile" name="uploadFile" onchange="$('#custom-file-name').text(this.files[0].name)">
                    <label id="custom-file-name" class="custom-file-label" for="customFile">Choose file</label>
                  </div>
                <small class="form-text"><a id="unduh_temp" class="text-white" href="<?php echo base_url("excel/new_model_iuran.xlsx"); ?>">Klik disini, untuk mendownload template.</a></small>
              </div>
                <input id="btn-submit-prev" type="submit" name="preview" value="Preview" class="d-none" />
            </form>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light bg-head pill pl-4 pr-4" data-dismiss="modal">CLOSE</button>
            <button onclick="$('#btn-submit-prev').click()" type="button" class="btn btn-secondary btn-secondary pill pl-4 pr-4">SUBMIT</button>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view('inc/footer');?>
   <!--  <script src="<?php echo base_url('assets/js/siswa-script.js?yes')?>"></script>  -->

  <script type="text/javascript">
  var path = ctx + 'SiswaController';
  //var count = jumlah_siswa;
   function init() {
    setTahunAjaran(); 
    //setRef();


    $('#btn-import').click(function(e){
        var obj = {};
        ajaxPOST(path + '/import_iuran_new',obj,'onActionSuccess','onSaveError');
    });

  };

  // function onActionSuccess(response){
  //     Swal.fire({
  //       type: 'success',
  //       title: 'Message',
  //       text: response.message,
  //       //footer: '<a href>Why do I have this issue?</a>'
  //       showCancelButton: false,
  //       confirmButtonColor: '#3085d6',
  //       cancelButtonColor: '#d33',
  //       confirmButtonText: 'Oke!'
  //     })
  //     .then((value) => {
  //         if(value.value){
  //             location.reload();
  //         }
  //     });
  // } 
  
  function formatMD5(){
    var hash = CryptoJS.MD5("Message");
    return hash;
  }
  function cekidot() {
      var path = ctx + 'SiswaController';
      var obj = new FormData(document.querySelector('#form'));
      if(selected_id != ""){
          if(selected_id.indexOf(",")!=-1) {
            selected_id=selected_id.split(",");
            $.each(selected_id, function(key){
              obj.append('id_siswa[]',this);
            });
          }else{
            obj.append('id_siswa[]',selected_id);
          } 
      }
      console.log(obj.getAll('id_siswa'));

      obj.append('list_iuran', JSON.stringify(list_iuran[0]));
      ajaxPOST(path + '/save_all',obj,'onActionSuccess','onSaveError');
  }

    function onActionSuccess(resp){
      console.log(resp);
      if(resp.code==200){
        Swal.fire('Berhasil!', resp.message, 'success');
        window.location.href="./";
      }
    }
    function onSaveError(response){
      console.log(response);
      Swal.fire("Data Sudah Ada", response.responseJSON.message, 'warning');
      var resp_msg={"title" : "Message", "body" : response.responseJSON.message, "icon"  : "warning"};
      showAlertMessage(resp_msg, 1800);
    }
    function setVal(val){
      $('#table'+val).find('tbody [name="header_siswa[]"]').val(val);
    }
    function setTahunAjaran(){
      $('[name="tahun_ajaran[]"]').val(checkTahunAjaran());
    }
    function setRef(){
      var path = ctx + 'SiswaController'; 
      ajaxGET(path + '/get_ref_now','onSetRef','onError');
      // console.log(path + '/get_ref_now');
    }
    function onSetRef(resp){
      console.log("here", resp);
      temp=resp.data;
      // count = (parseInt(temp)+parseInt(jumlah_siswa));
      // var key=0;
      // for(var i = temp; i < count; i++) {
      //   // alert(i);
      //   $('[name="ref[]"]')[key].value=i;
      //   key++;
      // }
      return temp;
    }

    function onError(){
      alert("ERR");
    }
  </script>
  </body>
</html>