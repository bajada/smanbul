<?php $this->load->view('inc/header');?>
    <div class="container-fluid mt-3">

      <div class="media bg-head pill pl-5 pt-3  pb-3">
        <img src="http://sman1-cibungbulang.sch.id/assets/img/smanbul.png" style="width: 100px;" class="ml-3 mr-3" alt="GAMBAR HERE">
        <div class="media-body mt-auto mb-auto">
          <h2 class="font-main">Sistem Informasi Pembayaran</h2>
          <h3 class="font-main">SMAN 1 CIBUNGBULANG</h3>
        </div>
        <div class="pr-5 pt-3  pb-3">
          <a href="<?php echo base_url('page/dashboard_view') ?>" class="mr-4 text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-home fa-4x" aria-hidden="true"></i> Home</a>
          <a href="<?php echo base_url('page/login_view') ?>" class="text-white-href" style="display: inline-grid;text-align: center;text-decoration: none;"><i class="fa fa-globe fa-4x" aria-hidden="true"></i> Website</a>
        </div>
      </div>

          <!-- <h1 class="card-title text-white mb-4 font-main">LOGIN</h1> -->
          <div class="pt-2">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb" style="background-color:transparent;">
                <li class="breadcrumb-item"><button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">IURAN</button></li>

                <li class="breadcrumb-item" aria-current="page">
                      <a href="" class="text-white-href" style="display: inline-flex;">
                      <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-dollar fa-stack-1x fa-inverse text-muted"></i>
                      </span>
                      <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran SPP</span>
                      </a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                      <a href="" class="text-white-href" style="display: inline-flex;">
                      <span class="fa-stack fa-1x" style="font-size: 1.5rem">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fas fa-suitcase fa-stack-1x fa-inverse text-muted"></i>
                      </span>
                      <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Iuran DPMP</span>
                      </a>
                </li>
              </ol>
            </nav>

          </div>

          <div class="row justify-content-center">
          <div class="col-md-6">
            <form id="search-form">
            <div class="form-group">
            <div class="input-group input-group-lg">
            <div class="input-group-prepend">
              <span class="input-group-text pill-left">NO REF</span>
            </div>
            <input name="filter_keyword" id="txtsearch" size="50" type="text" class="form-control input-sm" aria-label="..." placeholder="Ketik Nomor REF">
            <div class="input-group-append">
              <button class="btn btn-secondary pill-right" type="submit" id="btn-cari">Cari</button>
            </div>
            </div>
            </div> 
            </form>
          </div>
          </div>

          <div class="row justify-content-center">
          <div class="col-md-4">
          <div class="card bg-login pill p-1">
            <div class="card-body">
            <h3 class="card-title" style="font-family: segoeui;">Info Siswa</h3>
            <hr/>
            <form action="<?php echo base_url('PageController') ?>" >
              

              <div class="form-group row">
                <div class="col-md-12 text-center">
                  <div>
                    <a href="" class="text-white-href">
                    <span class="fa-stack fa-5x">
                      <i class="fas fa-circle fa-stack-2x"></i>
                      <i class="fas fa-user fa-stack-1x fa-inverse text-muted"></i>
                    </span>
                    </a>
                  </div>
                </div>
              </div>
              <div class="form-group row border-bottom">
                <label for="" class="col-md-4">No.REF</label>
                <div class="col-md-8">
                  <label id="lbl_no_ref" for="" style="font-weight: 100;">: </label>
                </div>
              </div>
              <div class="form-group row border-bottom">
                <label for="" class="col-md-4">Nama Siswa</label>
                <div class="col-md-8">
                  <label id="lbl_nama_lengkap" for="" style="font-weight: 100;">: </label>
                </div>
              </div>

              <div class="form-group row border-bottom">
                <label for="" class="col-md-4">Kelas</label>
                <div class="col-md-8">
                  <label id="lbl_kelas" for="" style="font-weight: 100;">: </label>
                </div>
              </div>

              <div class="form-group row border-bottom">
                <label for="" class="col-md-4">Tahun Ajaran</label>
                <div class="col-md-8">
                  <label id="lbl_tahun_ajaran" for="" style="font-weight: 100;">: </label>
                </div>
              </div>
            </form>

            </div>
          </div>

          <div class="card bg-login pill p-1 mt-3">
            <div class="card-body">
            <h3 class="card-title" style="font-family: segoeui;">Rincian Biaya</h3>
            <hr/>
            <form action="<?php echo base_url('PageController') ?>" >
              <div class="form-group row">
                <label for="" class="col-md-4">Biaya Iuran</label>
                <div class="col-md-8">
                  <label class="" style="font-weight: 100;" id="lbl_biaya_iuran_per_bulan">: 0</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-4">Biaya DPMP</label>
                <div class="col-md-8">
                  <label class="" style="font-weight: 100;" id="lbl_biaya_dpmp">: 0</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-4">Biaya DU</label>
                <div class="col-md-8">
                  <label class="" style="font-weight: 100;" id="lbl_biaya_du">: 0</label>
                </div>
              </div>
              <hr/>

              <div class="form-group row">
                <label for="" style="font-family: segoeuib" class="col-md-4">TOTAL</label>
                <div class="col-md-8">
                  <label for="" style="font-family: segoeuib" id="lbl_total_biaya">: 0</label>
                </div>
              </div>

            </form>

            </div>
          </div>
          <div class="form-group text-center mt-2">
            <button class="btn btn-light btn-lg btn-block bg-head pill pl-5 pr-5">SUBMIT</button>
            <small class="text-white">*Klik Tombol 'SUBMIT' apabila telah selesai.</small>
          </div>

          </div>

          <div class="col-md-8">


          <div class="card bg-login pill p-1 mt-2">
            <div class="card-body">
            <h3 class="card-title" style="font-family: segoeui;">Biaya DPMP Siswa(<span class="text-danger" >STATUS</span>)</h3>
            <hr/>
            <form>
              
              <div class="form-group row">
                <div class="col-md-6">
                    <label>Nominal DPMP</label>
                    <input name="nominal_dpmp" type="number" class="form-control" placeholder="Nominal Rupiah yang diterima" >
                </div>
                <div class="col-md-6">
                  <div class="form-group row">
                    <label for="" class="col-md-5">Besar DPMP</label>
                    <div class="col-md-7">
                      <label class="dollar" style="font-weight: 100;" id="lbl_besar_dpmp">: 0</label>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-md-5">Sisa DPMP</label>
                    <div class="col-md-7">
                      <label class="dollar" for="" style="font-weight: 100;" id="lbl_sisa_dpmp">: 0</label>
                    </div>
                  </div>
                </div>
              </div>
              
            </form>
            </div>
          </div>

          <div class="card bg-login pill p-1 mt-2">
            <div class="card-body">
            <h3 class="card-title" style="font-family: segoeui;">Biaya DU Siswa(<span class="text-success">STATUS</span>)</h3>
            <hr/>
            <form>
              
              <div class="form-group row">
                <div class="col-md-6">
                    <label>Nominal DU</label>

                    <!-- <div class="input-group input-group-lg">
                    <div class="input-group-prepend">
                      <span class="input-group-text pill-left">NO REF</span>
                    </div>
                    <input type="number" class="form-control" placeholder="Nominal Rupiah yang diterima">
                    </div> -->
                    <input name="nominal_du" type="number" class="form-control" placeholder="Nominal Rupiah yang diterima">
                </div>
                <div class="col-md-6">
                  <div class="form-group row">
                    <label for="" class="col-md-5">Besar DU</label>
                    <div class="col-md-7">
                      <label class="dollar" for="" style="font-weight: 100;" id="lbl_besar_du">: 0</label>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-md-5">Sisa DU</label>
                    <div class="col-md-7">
                      <label class="dollar" for="" style="font-weight: 100;" id="lbl_sisa_du">: 0</label>
                    </div>
                  </div>
                </div>
              </div>
              
            </form>
            </div>
          </div>
          
          <div class="card bg-login pill p-1">
            <div class="card-body">
            <h3 class="card-title" style="font-family: segoeui;">Biaya Iuran Siswa(<span class="text-danger" >STATUS</span>)</h3>
            <hr/>

              

            <table id="tbl-data" class="table table-sm table-hovered table-pill" style="opacity: 0.9;">
              <thead class="text-white" style="background-color: #487d95!important;font-family: segoeui;">
                <tr>
                  <th scope="col" class="p-3">No.</th>
                  <th scope="col" class="p-3">Bulan</th>
                  <th scope="col" class="p-3">Jatuh Tempo</th>
                  <th scope="col" class="p-3">No.Iuran</th>
                  <th scope="col" class="p-3">Tgl.Iuran</th>
                  <th scope="col" class="p-3">Besar Iuran</th>
                  <th scope="col" class="p-3">Keterangan</th>
                  <th scope="col" class="text-center p-3">#</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
            <small class="pull-right">*Klik Tombol 'Centang' untuk memilih.</small>

            </div>
          </div>

          </div>
          </div>


    </div>

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg pill text-white" role="document">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel" style="margin-left:auto;">INFORMASI IURAN SISWA</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form">
              <div class="form-group row">
                <label for="" class="col-md-3">Ref / Nama Siswa</label>
                <div class="col-md-9">
                  <label for="" style="font-weight: 100;">: 100101 - Adbuhraman</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Kelas</label>
                <div class="col-md-9">
                  <label for="" style="font-weight: 100;">: X-IPA-1</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Besar Iuran/Bulan</label>
                <div class="col-md-9">
                  <label for="" style="font-weight: 100;">: 150,000</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Iuran Terakhir</label>
                <div class="col-md-9">
                  <label id="view_detil_last_iuran" for="" style="font-weight: 100;" data-toggle="popover" title="Detail" data-content="Iuran terakhir adalah bulan ke 11">: 05-11-2018</label>
                </div>
              </div>
              <div class="form-group row">
              <label for="" class="col-md-3">Histori Iuran</label>
              <div class="col-md-9">
              <table class="table table-bordered table-sm">
              <thead class="bg-danger">
              <tr>
              <th scope="col">JUL</th>
              <th scope="col">AGU</th>
              <th scope="col">SEP</th>
              <th scope="col">OKT</th>
              <th scope="col">NOV</th>
              <th scope="col">DES</th>
              <th scope="col">JAN</th>
              <th scope="col">FEB</th>
              <th scope="col">MAR</th>
              <th scope="col">APR</th>
              <th scope="col">MEI</th>
              <th scope="col">JUN</th>
              </tr>
              </thead>
              <tbody class="bg-info">
              <tr>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>OK</td>
              <td>-</td>
              <td>-</td>
              </tr>
              </tbody>
              </table>
              </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Sisa Iuran</label>
                <div class="col-md-9">
                  <label for="" style="font-weight: 100;">: 2x</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Sisa DPMP</label>
                <div class="col-md-9">
                  <label for="" style="font-weight: 100;">: 1,000,000</label>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Sisa DU</label>
                <div class="col-md-9">
                  <label for="" style="font-weight: 100;">: 300,0000</label>
                </div>
              </div>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5" data-dismiss="modal">BATAL</button>
            <button onclick="" type="button" class="btn btn-light btn-lg btn-secondary pill pl-5 pr-5 text-white" data-dismiss="modal">LANJUT</button>
          </div>
        </div>
      </div>
    </div>


    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal-bulan-iuran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg pill text-white" role="document">
        <div class="modal-content bg-gradient pill">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel" style="margin-left:auto;">FORM BULAN IURAN</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form-bulan">
              <div class="form-group row">
                <label for="" class="col-md-3">Jumlah Bulan Iuran</label>
                <div class="col-md-9">
                    <select class="form-control select-style" id="" onchange="renderBulanKe(this.value);">
                      <option>Pilih Untuk Berapa Bulan Iuran</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                    </select>
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-md-3">Bulan Ke</label>
                <div id="bulan-ke" class="col-md-9">
                </div>
              </div>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5" data-dismiss="modal">BATAL</button>
            <button onclick="setBulanIuran()" type="button" class="btn btn-light btn-lg btn-secondary pill pl-5 pr-5 text-white" data-dismiss="modal">LANJUT</button>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view('inc/footer');?>
    <script src="<?php echo base_url('assets/js/iuran-script.js')?>"></script> 

  </body>
</html>