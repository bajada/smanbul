<?php $this->load->view('inc/header');?>

  <div class="row">
    <div class="col-md-6">
      <a href="" class="text-white-href" style="display: inline-flex;">
          <span class="fa-stack fa-1x" style="font-size: 1.5rem">
            <i class="fas fa-circle fa-stack-2x"></i>
            <i class="fas fa-users fa-stack-1x fa-inverse text-muted"></i>
          </span>
          <span style="font-size: 25px;vertical-align: text-bottom;font-weight: 200;" class="align-self-center"> Siswa</span>
          </a>
    </div>

    <div class="col-md-6 text-right">
      <button onclick="cekidot()" type="button" class="btn btn-light btn-lg btn-info pill pl-5 pr-5">SUBMIT/SIMPAN</button>
    </div>
  </div>

          
    <form id="form" class="form-horizontal" style="overflow-x: scroll; height: 600px">
      <div class="container card-deck" id="form-container" >
      </div>
    </form>

    </div>

  <?php $this->load->view('inc/footer');?>
  <script src="<?php echo base_url('assets/js/form-siswa-script.js?yes')?>"></script> 
  <script type="text/javascript">
  var count = jumlah_siswa;
  $(document).ready(function(){
    setTahunAjaran(); 
    setRef();
  });

  function cekidot() {
      var path = ctx + 'SiswaController';
      var obj = new FormData(document.querySelector('#form'));
      if(selected_id != ""){
          if(selected_id.indexOf(",")!=-1) {
            selected_id=selected_id.split(",");
            $.each(selected_id, function(key){
              obj.append('id_siswa[]',this);
            });
          }else{
            obj.append('id_siswa[]',selected_id);
          } 
      }
      console.log(obj.getAll('id_siswa'));

      obj.append('list_iuran', JSON.stringify(list_iuran[0]));
      ajaxPOST(path + '/save_all',obj,'onActionSuccess','onSaveError');
  }

    function onActionSuccess(resp){
      console.log(resp);
      if(resp.code==200){
        Swal.fire('Berhasil!', resp.message, 'success');
        window.location.href="siswa_view";
      }
    }
    function onSaveError(response){
      console.log(response);
      Swal.fire("Data Sudah Ada", response.responseJSON.message, 'warning');
      var resp_msg={"title" : "Message", "body" : response.responseJSON.message, "icon"  : "warning"};
      showAlertMessage(resp_msg, 1800);
    }
    function setVal(val){
      $('#table'+val).find('tbody [name="header_siswa[]"]').val(val);
    }
    function setTahunAjaran(){
      $('[name="tahun_ajaran[]"]').val(checkTahunAjaran());
    }
    function setRef(){
      var path = ctx + 'SiswaController'; 
      ajaxGET(path + '/get_ref_now','onSetRef','onError');
      // console.log(path + '/get_ref_now');
    }
    function onSetRef(resp){
      console.log("here", resp);
      temp=resp.data;
      count = (parseInt(temp)+parseInt(jumlah_siswa));
      var key=0;
      for(var i = temp; i < count; i++) {
        // alert(i);
        $('[name="ref[]"]')[key].value=i;
        key++;
      }
    }

    function onError(){
      alert("ERR");
    }

  </script>
  </body>
</html>