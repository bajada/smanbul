<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class SiswaController extends CI_Controller
{
	private $filename = "import_data"; // Kita tentukan nama filenya
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('Siswa');
        $this->load->model('Iuran');
        $this->load->model('Kelas');
        $this->load->model('SiswaKelas');
        $this->load->model('SiswaDetil');
        $this->load->model('ImportExcel', 'import');
    	$utils = new Utils();
	}

	public function index()
	{	
		$this->load->view('siswa_view');
	}

	public function get_by_id($id) {
		$data = $this->Siswa->get_by_id($id);
		$action=$this->input->get('action');
        $resp = array(
        				"code" => "200",
        				"message" => "success",
                        "data" => $data,
                );

		if(isset($action)) {
			if($action=='delete') {
				$resp['message'] = "Apakah anda yakin akan menghapus data '".$data->id_siswa."'?";
                
			}
		}
		echo json_encode($resp);
	}
	public function get_ref_now() {
		$data = $this->Siswa->newRef();
        $resp = array(
        				"code" => "200",
        				"message" => "success",
                        "data" => $data,
                );
		echo json_encode($resp);
	}
	public function do_action() {//$id, $action
  //       $resp = array(
  //       				"code" => "code",
  //       				"message" => "message",
  //                       "data" => [],
  //               );

		// if(isset($action)) {
		// 	if($action=='delete') {
		// 		$data = $this->Siswa->get_by_id($id); //getEntity
		// 		$resp['data'] = $data;
		// 		$this->Siswa->delete($id);
				
		// 		$resp['message'] = "Data '".$data->id_siswa."' berhasil dihapus.";
                
		// 	}
		// }
		// echo json_encode($resp);

	 	$resp = array(
				"code" => http_response_code(200),
				"message" => "success",
	            "data" => [],
        );
		$data=[];
		foreach ($this->input->post('id_siswa') as $key => $id) {
			$data[] =  array(
				'id_siswa' => $id,
				'status_aktif_siswa' => $this->input->post('status_aktif_siswa')[$key],
			);
			// $this->Siswa->delete_all($data, false);
		}
		$this->Siswa->update_all($data);
		
		
		$resp['message'] = "Data berhasil diupdate.";
		$resp['data'] = $data;	
		return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($resp));
	}

	public function list_table() {
		$param = "1";
        $resp = array(
        				"code" => http_response_code(200),
        				"message" => "success",
                        "data" => [],
        );
        // $kelas=$this->input->get('kelas_siswa');//
        $tahun_ajaran=$this->input->get('filter_tahun_ajaran');
        $filter_keyword=$this->input->get('filter_keyword');
        $filter_status_aktif=$this->input->get('filter_status_aktif');
        $param = new QueryParameter();
        if($tahun_ajaran!="")
        $param->setClause($param->getClause() . " AND tahun_angkatan_siswa" . " ='".$tahun_ajaran."'");

        if($filter_keyword!=""){
        	$param->setClause($param->getClause() . " AND ref_siswa" . " LIKE'%".$filter_keyword."%'");
        	$param->setClause($param->getClause() . " OR nama_lengkap_siswa" . " LIKE'%".$filter_keyword."%'");
        }

        $id_siswa=$this->input->get('id_siswa');
        if(isset($id_siswa)){
			foreach ($id_siswa as $key => $id) {
	        	if($key==0)
	        	$param->setClause($param->getClause() . " AND id_siswa" . " ='".$id."'");
	        	else
	        	$param->setClause($param->getClause() . " OR id_siswa" . " ='".$id."'");
	        }
        }
        // if($filter_keyword!=""){
       if(isset($filter_status_aktif)) $param->setClause($param->getClause() . " AND status_aktif_siswa" . " ='".$filter_status_aktif."'");
        // }
        $param->setClause($param->getClause() . " AND user_deleted_siswa is null");

		$data = $this->Siswa->get_list_table($param);

		$resp['data'] = $data;	
		echo json_encode($resp);
	}

	public function check_id() {
		$param = "1";
        $resp = array(
        				"code" => http_response_code(200),
        				"message" => "success",
                        "data" => [],
        );

        $param = new QueryParameter();
        $id_siswa=$_POST["id_siswa"];//$this->input->post('id_siswa');

  //       $data_arr = json_decode(json_encode($_POST["id_siswa"],FALSE));
		// $data_arr=json_decode($data_arr); 
        // print_r($id_siswa);
        // print_r("OR");
        // die();
        if(isset($id_siswa)){
			foreach ($id_siswa as $key => $id) {
	        	if($key==0)
	        	$param->setClause($param->getClause() . " AND id_siswa" . " ='".$id."'");
	        	else
	        	$param->setClause($param->getClause() . " OR id_siswa" . " ='".$id."'");
	        }
        }
        // if($filter_keyword!=""){
       /*if(isset($filter_status_aktif)) $param->setClause($param->getClause() . " AND status_aktif_siswa" . " ='".$filter_status_aktif."'");*/
        // }
        $param->setClause($param->getClause() . " AND user_deleted_siswa is null");

		$data = $this->Siswa->get_list_table($param);

		$resp['data'] = $data;	
		echo json_encode($resp);
	}

	public function list_kelas_table() {
		$param = "1";
        $resp = array(
			"code" => http_response_code(200),
			"message" => "success",
	        "data" => [],
        );
		//$param = $param." AND "."header_id_siswa_kelas"." = "."'".$data_siswa->id_siswa."'";
		$tahun_ajaran=$this->input->get('tahun_ajaran_siswa');
		if(isset($tahun_ajaran) && $tahun_ajaran != ''){
			$param.=" AND "."tahun_ajaran_siswa"." = "."'".$tahun_ajaran."'";
		}else{
			$param.=" AND "."status_siswa_kelas"." = "."'1'";
		}
		// $data_siswa;
		$data = $this->SiswaKelas->get_list_table($param);
		foreach ($data  as $key => $value) {
			$param = "1";
			$data_siswa = $this->Siswa->get_list_table($param);
			// array_push($data, array( 'bar' => '1234' ));
			$data_siswa = $this->Siswa->get_by_id($value->header_id_siswa_kelas);
			$value->header_id_siswa_kelas=$data_siswa;
		}
		// $data = (object) array_merge( (array)$data, array( 'bar' => '1234' ) );
		$arrayName = array(
			'data_siswa' => $this->Siswa->get_list_table("1"),
			'data_siswa_kelas' => $data
		);
		// print_r($arrayName);
		$resp['data'] = $arrayName;	
		echo json_encode($resp);
	}

	public function list_siswa_kelas_table_new() {
        // $kelas=$this->input->get('kelas_siswa');
        // $tahun_ajaran_siswa=$this->input->get('tahun_ajaran_siswa');
        // $tahun_ajaran=$this->input->get('tahun_ajaran');
        // $tingkat_kelas=$this->input->get('tingkat_kelas');

		// if(isset($tahun_ajaran_siswa)) $param.=" AND "."tahun_ajaran"." = "."'".$tahun_ajaran_siswa."'";
		// else if(isset($tahun_ajaran)) $param.=" AND "."tahun_ajaran"." = "."'".$tahun_ajaran."'";
		// if($tingkat_kelas!='ALL'){
			
		// 	if(isset($tingkat_kelas)) $param.=" AND "."tingkat_kelas"." = "."'".$tingkat_kelas."'";
		// }

		$resp = array(
				"code" => http_response_code(200),
				"message" => "http_response_code(200)",
                "data" => [],
        );
    	$param = new QueryParameter();
		$filterKeyword = $this->input->get('filter_keyword');
		$filterKelas = $this->input->get('filter_kelas');
		$filterTahunAjaran = $this->input->get('filter_tahun_ajaran');

		if(isset($filterKelas) && $filterKelas != '0') {
			$param->setClause($param->getClause() . " AND tingkat_kelas" . "='".$filterKelas."'");
		}
		
		if(isset($filterTahunAjaran) && $filterTahunAjaran != '') {
			$param->setClause($param->getClause() . " AND tahun_ajaran_siswa_kelas" . "='".$filterTahunAjaran."'");
		}

		if(isset($filterKeyword) && $filterKeyword != '') {
			$param->setClause($param->getClause() . " AND ref_siswa" . " LIKE'%".$filterKeyword."%'");
			$param->setClause($param->getClause() . " OR nama_lengkap_siswa" . " LIKE'%".$filterKeyword."%'");
			$param->setClause($param->getClause() . " OR jurusan_kelas" . " LIKE'%".$filterKeyword."%'");
		}
		
		$param->setClause($param->getClause() . " AND user_deleted_siswa is null");
		$param->setClause($param->getClause() . " AND status_aktif_siswa = '1'");
		$param = $param->getClause();
		// print_r($param);
		// die();
		$data = $this->Siswa->get_list_table2($param);
		foreach ($data as $key => $value) {
			$param="1";
	    	$param = $param." AND "."header_id_siswa_detil"." = "."'".$value->header_id_siswa_kelas."'";
	    	$param = $param." AND "."tingkat_siswa_detil"." in('".$value->tingkat_kelas."',"."'ALL'".")";

	    	// $param = $param." AND "."tahun_ajaran"." = "."'".$value->tahun_ajaran."'";
			$data_siswa_detil=$this->SiswaDetil->get_list_table($param);

			$value->list_iuran=$data_siswa_detil;
		}
		$resp['data'] = $data;	
		return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($resp));
	}

	public function delete_all(){
		 $resp = array(
				"code" => http_response_code(200),
				"message" => "success",
	            "data" => [],
        );
		$data=[];
		foreach ($this->input->post('id_siswa') as $key => $id) {
			$data =  array(
				'id_siswa' => $id,
				'user_deleted_siswa' => $this->session->userdata('fullname'),
				'date_deleted_siswa' => date("Y-m-d H:i:s"),
			);
			$this->Siswa->delete_all($data, true);
		}
		
		$resp['message'] = "Data berhasil dihapus.";
		$resp['data'] = $data;	
		return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($resp));
	}

	public function save_all() {
	    $resp = array(
				"code" => http_response_code(200),
				"message" => "success",
	            "data" => [],
	    );
	    $utils = new Utils();
	    $data_siswa=array();
	    $data_detil=array();
	    $data_kelas=array();
	    $data_multi_kelas=array();

		//
		$year=date("Y");
		$s_year=date("Y");
		$e_year=date("Y")+1;
		$month=date("m");
		if($month>6){
			$s_year=$year;
			$e_year=($year+1);
		}else{
			$s_year=($year-1);
			$e_year=$year;
		}
	  	$full_year=$s_year."/".$e_year;
		$begin = new DateTime( $s_year.'-07-10' );
		$end = new DateTime( $e_year.'-06-10' );
		$end = $end->modify( '+1 month' ); 

		$interval = new DateInterval('P1M');
		$daterange = new DatePeriod($begin, $interval ,$end);
		$split=explode("/", $full_year);
		//

	    $pass=true;
		for ($i=0; $i < count($this->input->post('ref')); $i++) {
			$id_siswa = $this->input->post('id_siswa')[$i];
			$ref_siswa = $this->input->post('ref')[$i];
			$nama_lengkap_siswa = $this->input->post('nama_lengkap')[$i];
			$jenis_kelamin_siswa = $this->input->post('jenis_kelamin')[$i];
			$tahun_ajaran_siswa = $this->input->post('tahun_ajaran')[$i];
			$tanggal_registrasi_siswa = date("Ymd");
			$kelas = $this->input->post('kelas')[$i];

			$data_siswa[$i] =  array(
				'id_siswa' => $utils->longNumberId(),
				'ref_siswa' => null,
				'nama_lengkap_siswa' => null,
				// 'kelas_siswa' => $this->input->post('kelas')[$i],
				'jenis_kelamin_siswa' => null,
				'tahun_angkatan_siswa' => null
			);

			// print_r($id_siswa . "<");	
			if(isset($id_siswa)){
				$data_siswa[$i]['id_siswa'] =  $id_siswa;
				//harusnya ada check apakah id exist or not???
			}

			if(isset($ref_siswa)) $data_siswa[$i]['ref_siswa'] = $ref_siswa;
			if(isset($nama_lengkap_siswa)) $data_siswa[$i]['nama_lengkap_siswa'] = $nama_lengkap_siswa;
			if(isset($jenis_kelamin_siswa)) $data_siswa[$i]['jenis_kelamin_siswa'] = $jenis_kelamin_siswa;

			// if(isset($tanggal_registrasi_siswa)) $data_siswa[$i]['tanggal_registrasi_siswa'] = $tanggal_registrasi_siswa;
			// if(isset($ref_siswa)) $data_siswa[$i]['ref_siswa'] = $ref_siswa;
			//tahun ajaran & angkatan
			// if(isset($tahun_ajaran_siswa)) {
			// 	$data_siswa[$i]['tahun_ajaran_siswa'] = ($tahun_ajaran_siswa==$full_year?$full_year:$tahun_ajaran_siswa);
			// 	$data_siswa[$i]['tahun_angkatan_siswa'] = explode("/", $tahun_ajaran_siswa)[0];
			// }

			// $data_siswa[$i] =  array(
			// 	'id_siswa' => $id_siswa,
			// 	'ref_siswa' => $this->input->post('ref')[$i],
			// 	'nama_lengkap_siswa' => $this->input->post('nama_lengkap')[$i],
			// 	// 'kelas_siswa' => $this->input->post('kelas')[$i],
			// 	'jenis_kelamin_siswa' => $this->input->post('jenis_kelamin')[$i],
			// 	'tahun_ajaran_siswa' => ($this->input->post('tahun_ajaran')[$i]==$full_year?$full_year:$this->input->post('tahun_ajaran')[$i]),
			// 	'tahun_angkatan_siswa' => $new_tahun[0]
			// );

			$data_multi_kelas[$i] =  array(
				'kelas_siswa' => $this->input->post('kelas')[$i],
			);
			//init to check duplicate data
			// $check_vals.="AND nama_lengkap_siswa"."='".$this->input->post('nama_lengkap')[$i]."'";
			// $check_vals.=" OR ref_siswa"."='".$this->input->post('ref')[$i]."'";

			$param = new QueryParameter();
			$param->setClause($param->getClause() . " AND nama_lengkap_siswa" . "='".$this->input->post('nama_lengkap')[$i]."'");
			$param->setClause($param->getClause() . " OR ref_siswa" . "='".$this->input->post('ref')[$i]."'");

			//substr($check_vals, 4);
		    $lst=$this->Siswa->get_list_table($param);

		    if(count($lst)>0 && !isset($id_siswa)){
		    	$check_duplicate="";
		    	for ($i=0; $i < count($lst); $i++) { 
		    		$check_duplicate.=",".$lst[$i]->nama_lengkap_siswa;
				}

				$resp['code']=http_response_code(409);
		    	$resp['message'] = "Terjadi DUPLIKASI pada data: ".substr($check_duplicate, 1);

		    	return $this->output
		        ->set_content_type('application/json')
		        ->set_status_header(409)
		        ->set_output(json_encode($resp));
		    }
			if($ref_siswa == "" || $nama_lengkap_siswa == "" || $tahun_ajaran_siswa == "") {
				$pass=false;
			}else if($kelas == 0) {//$jenis_kelamin_siswa == "" || 
				$pass=false;
			}
			//print_r($jenis_kelamin_siswa);
			//
			$data_kelas[$i] =  array(
				'header_id_siswa_kelas' => $data_siswa[$i]['id_siswa'],
				'kelas_siswa_kelas' => $this->input->post('kelas')[$i],
				'tahun_ajaran_siswa_kelas' => $this->input->post('tahun_ajaran')[$i],
				'status_siswa_kelas' => 1
			);
			$param = new QueryParameter();
			$param->setClause($param->getClause() . " AND header_id_siswa_kelas" . "='".$data_siswa[$i]['id_siswa']."'");
			$param->setClause($param->getClause() . " AND tahun_ajaran_siswa_kelas" . "='".$this->input->post('tahun_ajaran')[$i]."'");
			$param=$param->getClause();
			$lst_kelas=$this->SiswaKelas->get_list_table($param);
			if(count($lst_kelas)>0){// && !isset($id_siswa)
		    	$check_duplicate="";
		    	for ($i=0; $i < count($lst_kelas); $i++) { 
		    		$check_duplicate.=",".$lst_kelas[$i]->header_id_siswa_kelas;
				}

				$resp['code']=http_response_code(409);
		    	$resp['message'] = "Terjadi DUPLIKASI pada data: ".substr($check_duplicate, 1);

		    	return $this->output
		        ->set_content_type('application/json')
		        ->set_status_header(409)
		        ->set_output(json_encode($resp));
		    }
			//
		}
		if(!$pass) {
			$resp['code']=http_response_code(400);
    		$resp['message'] = "Input belum lengkap, harap dilengkapi terlebih dahulu.";
    		return $this->output
		        ->set_content_type('application/json')
		        ->set_status_header(400)
		        ->set_output(json_encode($resp));
		}

		//looping data untuk iuran/detil
		for ($i=0; $i < count($this->input->post('jenis_iuran')); $i++) {
			$id=$this->input->post('header_siswa')[$i];
			$data_detil[$i] =  array(
				'header_id_siswa_detil' => $data_siswa[$id]['id_siswa'],
				'tingkat_siswa_detil' => $this->input->post('kelas_siswa')[$i],
				'jenis_iuran_siswa_detil' => $this->input->post('jenis_iuran')[$i],
				'besar_iuran_siswa_detil' => $this->input->post('besar_iuran')[$i],
			);
		}
        $data = array(
        	'id' => isset($id_siswa),
        	'siswa' => $data_siswa,
        	'kelas' => $data_kelas,
        	'detil' => $data_detil//iuran
        );
  //       if(isset($id_siswa)){
        	//jika naik kelas maka status kelas sebelumnya update jadi 0
        $param = new QueryParameter();
		$param->setClause($param->getClause() . " AND header_id_siswa_kelas" . "='".$id_siswa."'");

		$param = $param->getClause();
    	$data_siswa_kelas = $this->SiswaKelas->get_list_table($param);

    	foreach ($data_siswa_kelas as $key => $value) {
    		$value->status_siswa_kelas=0;
    		// print_r($value);
    		$data_kelas[$key] =  array(
				'header_id_siswa_kelas' => $value->header_id_siswa_kelas,
				'status_siswa_kelas' => $value->status_siswa_kelas
			);
    	}
    	$this->SiswaKelas->updateAll($data_kelas);

		$this->Siswa->save_siswa_kelas_iuran($data);

		// }else{
		// 	$this->Siswa->save_siswa_kelas_iuran($data);
		// }
		// $this->Siswa->save_siswa_kelas_iuran($data);
        // $this->Siswa->save_master($arr);
    	$resp['message'] = "Data telah berhasil disimpan.";
        $resp['data'] = $data;    

		return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(200)
	        ->set_output(json_encode($resp));
	}

	public function update_all() {
	    $resp = array(
				"code" => http_response_code(200),
				"message" => "success",
	            "data" => [],
	    );

	    $utils = new Utils();
		$param = new QueryParameter();
		$param_check_data = new QueryParameter();
		$param_data = new QueryParameter();
		$pass=false;

		$data_siswa=[];
		foreach ($this->input->post('id_siswa') as $key_siswa => $value_siswa) {
			$data_siswa[] =  array(
				'id_siswa' => $value_siswa,
				'ref_siswa' => $this->input->post('ref_siswa')[$key_siswa],
				'nama_lengkap_siswa' => $this->input->post('nama_lengkap_siswa')[$key_siswa],
				'jenis_kelamin_siswa' => $this->input->post('jenis_kelamin_siswa')[$key_siswa],
				// 'tahun_angkatan_siswa' => $this->input->post('tahun_angkatan_siswa')[$key_siswa],
				'keterangan_masuk_siswa' => $this->input->post('keterangan_masuk_siswa')[$key_siswa],
			);
		}

	    foreach ($data_siswa as $key_siswa => $value_siswa) {
			if($value_siswa['ref_siswa'] == "" || $value_siswa['nama_lengkap_siswa'] == "" || $value_siswa['keterangan_masuk_siswa'] == "") {
				$pass=true;
				$param_check_data->setClause($param_check_data->getClause() . " AND id_siswa" . "='".$value_siswa."'");
			}
	    }

		// print_r($pass);
		$check_siswa=$this->Siswa->get_list_table($param_check_data);
		if($pass){
			$resp['data']=$check_siswa;
			$resp['code']=http_response_code(400);
	    	$resp['message'] = "Input belum lengkap, harap dilengkapi terlebih dahulu.";

	    	return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(400)
	        ->set_output(json_encode($resp));
		}
		
		$duplicate=false;
		$param_du = new QueryParameter();
		$other_data=[];
		foreach ($data_siswa as $key => $value) {
			$other_data[]=array('id' => $value['id_siswa'], 'ref' => $value['ref_siswa'], 'nama' => $value['nama_lengkap_siswa']);
			//$value['ref_siswa'];
			if($key==0)
			$param_du->setClause($param_du->getClause() . " AND ref_siswa" . "='".$value['ref_siswa']."'");
			else
			$param_du->setClause($param_du->getClause() . " OR ref_siswa" . "='".$value['ref_siswa']."'");	
		}
		// print_r($param_du);
		foreach ($this->Siswa->get_list_table(new QueryParameter()) as $key_data => $value_data) {
			$other_data[]=array('id' => $value_data->id_siswa, 'ref' => $value_data->ref_siswa, 'nama' => $value_data->nama_lengkap_siswa);
			//$value_data->ref_siswa;
		}
		// print_r($other_data);
		// die();
		// print_r(array_count_values($other_data));
		$data_duplicate=[];
		$new_other_data=[];
		foreach ($other_data as $key_o_data => $value_o_data) {

			$new_other_data[]=$value_o_data['ref'];

		}
		// print_r($other_data);
		// die();
		$i=0;
		$ea=0;$param_du = new QueryParameter();
		foreach (array_count_values($new_other_data) as $key_count => $value_count) {
			if(empty($this->Siswa->get_list_table($param_du))){
				// print_r($value_count);
				// if($value_count >= 2){
				// 	$duplicate=true;
				// 	// $data_duplicate[]=$key_count;
				// 	$data_duplicate[]= array('id' => $other_data[$i]['id'], 'status' => 1, 'nama' => $other_data[$i]['nama'], "HOLY" => "SHIT");
				// }
			}else{
				// if($value_count >= 2){
				// 	$duplicate=true;
				// 	$data_duplicate[]=$key_count;
				// 	// print_r($value_count);
				// }
				if($duplicate==false){
					foreach ($this->Siswa->get_list_table($param_du) as $key_data => $value_data) {
						foreach ($data_siswa as $key => $value) {
							if($value_data->id_siswa==$value['id_siswa']){

									// print_r($value_data->ref_siswa."==".$value['ref_siswa']);
								if($value_data->ref_siswa!=$value['ref_siswa']){
									// $duplicate=true;
									// $data_duplicate[]= array('id' => $value['id_siswa'], 'status' => 1, 'nama' => $value_data->nama_lengkap_siswa, "HOLY" => "SHIT2");
									
									if($key==0)
									$param_du->setClause($param_du->getClause() . " AND ref_siswa" . "='".$value['ref_siswa']."'");
									else
									$param_du->setClause($param_du->getClause() . " OR ref_siswa" . "='".$value['ref_siswa']."'");
									
									if(!empty($this->Siswa->get_list_table($param_du))){
									
										$duplicate=true;
										$data_duplicate[]= array('id' => $value['id_siswa'], 'status' => 1, 'nama' => $value['nama_lengkap_siswa'], "DAMN" => true);

										$ea++;
									}
								}
							}
						}	
					}
				}

				if($duplicate==false){
					foreach ($this->Siswa->get_list_table(new QueryParameter()) as $key_data => $value_data) {
						foreach ($data_siswa as $key => $value) {
							if($value_data->id_siswa==$value['id_siswa']){
								if($value_data->ref_siswa!=$value['ref_siswa']){
									$param_du = new QueryParameter();
									$param_du->setClause($param_du->getClause() . " AND ref_siswa" . "='".$value['ref_siswa']."'");
									
									if(count($this->Siswa->get_list_table($param_du)) >= 2){
									
										$duplicate=true;
										$data_duplicate[]= array('id' => $value['id_siswa'], 'status' => 1, 'nama' => $value_data->nama_lengkap_siswa, "DAMN" => true);

										$ea++;
									}

								}
								// else{
								// 	$duplicate=true;
								// 	$data_duplicate[]= array('id' => $value['id_siswa'], 'status' => 1, 'nama' => $value_data->nama_lengkap_siswa, "DAMN" => true);

								// 	$ea++;
								// }
							}
						}
					}
				}
				// print_r($ea);
				// if($value_count > 3){
				// 	$duplicate=true;
				// 	$data_duplicate[]= array('id' => $other_data[$i]['id'], 'status' => 1, 'nama' => $other_data[$i]['nama'], "HOLY" => "SHIT2");
				// }
				// if($value_count >= 2){
				// 	if($value_data->ref_siswa==$value['ref_siswa']) $duplicate=false;
				// }
				// if($value_count >= 2 && $value_data->id_siswa==$value['id_siswa']){
				// 	$duplicate=false;
				// }
			}
			$i++;
		}
		// print_r($data_duplicate);
		
		if($duplicate){
			$resp['data']=$data_duplicate;
			$resp['code']=http_response_code(409);
	    	$resp['message'] = "Terjadi DUPLIKASI data";

	    	return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(409)
	        ->set_output(json_encode($resp));
		}

		// print_r($data_siswa);
		$data=$this->Siswa->update_all($data_siswa);

    	$resp['message'] = "Data telah berhasil diupdate.";
        $resp['data'] = $data;  

		return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(200)
	        ->set_output(json_encode($resp));
	}

	public function update_siswa_detil() {
	    $resp = array(
				"code" => http_response_code(200),
				"message" => "success",
	            "data" => [],
	    );

	    $utils = new Utils();
		$param = new QueryParameter();
		$param_check_data = new QueryParameter();
		$param_data = new QueryParameter();
		$pass=false;

		$data=[];
		foreach ($this->input->post('id') as $key_siswa_detil => $value_siswa_detil) {
			$data[] =  array(
				'id_siswa_detil' => $value_siswa_detil,
				'besar_iuran_siswa_detil' => $this->input->post('besar_iuran')[$key_siswa_detil],
				// 'nama_lengkap_siswa' => $this->input->post('nama_lengkap_siswa')[$key_siswa],
			);
		}

		// print_r($data);
		// die();
	 //    foreach ($data_siswa as $key_siswa => $value_siswa) {
		// 	if($value_siswa['ref_siswa'] == "" || $value_siswa['nama_lengkap_siswa'] == "" || $value_siswa['keterangan_masuk_siswa'] == "") {
		// 		$pass=true;
		// 		$param_check_data->setClause($param_check_data->getClause() . " AND id_siswa" . "='".$value_siswa."'");
		// 	}
	 //    }

		// $check_siswa=$this->Siswa->get_list_table($param_check_data);
		// if($pass){
		// 	$resp['data']=$check_siswa;
		// 	$resp['code']=http_response_code(400);
	 //    	$resp['message'] = "Input belum lengkap, harap dilengkapi terlebih dahulu.";

	 //    	return $this->output
	 //        ->set_content_type('application/json')
	 //        ->set_status_header(400)
	 //        ->set_output(json_encode($resp));
		// }
		
		// print_r($data_siswa);
		$data=$this->SiswaDetil->update($data);

    	$resp['message'] = "Data telah berhasil diupdate.";
        $resp['data'] = $data;  

		return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(200)
	        ->set_output(json_encode($resp));
	}


	public function save_naik_kelas() {
	    $resp = array(
				"code" => http_response_code(200),
				"message" => "success",
	            "data" => [],
	    );
	    $utils = new Utils();

	    // print_r($_POST["list_obj"]);

		$data_arr = json_decode(json_encode($_POST["list_obj"],FALSE));
		$data_arr=json_decode($data_arr); 
		// print_r($data_arr);
	 //    die(111);

	    $data_siswa=array();
	    $data_kelas=array();
	    $data_old_kelas=array();
	    $data_detil=array();
	    $data_iuran=array();
	    

	    // print_r($data_arr);
	    // die();
	    // die();
	    // die();
	    // die();
	    
	    foreach ($data_arr as $key_data => $value_data) {
	    	$same_no_iuran=$utils->getAlphaNumericString(10);
	    	// $value_data->siswa_kelas
	    	//print_r($value_data->id_siswa);
	    	if(isset($value_data->id_siswa) && $value_data->id_siswa!=0){
	    		$data_siswa=[];//=$this->Siswa->get_by_id($value_data->id_siswa);
	    	}else{
	    		$same_id=$utils->longNumberId();
	    		$data_siswa[]=array(
					'id_siswa' => $same_id,
	    			"ref_siswa" => $value_data->ref_siswa,
	    			"nama_lengkap_siswa" => $value_data->nama_lengkap_siswa,
	    			"jenis_kelamin_siswa" => $value_data->jenis_kelamin_siswa,
	    			"tahun_angkatan_siswa" => $value_data->tahun_angkatan_siswa,
	    			"keterangan_masuk_siswa" => $value_data->keterangan_masuk_siswa,
	    			"status_aktif_siswa" => $value_data->status_aktif_siswa,
					'user_added_siswa' => $this->session->userdata('fullname'),
					'date_added_siswa' => date("Ymd"),
	    		);
	    		$value_data->id_siswa=$same_id;
	    	}

	    	foreach ($value_data->siswa_kelas as $key_siswa_kelas => $value_siswa_kelas) {
		    	$data_kelas[]=array(
					'id_siswa_kelas' => $utils->longNumberId(),
					'header_id_siswa_kelas' => $value_data->id_siswa,
					'kelas_siswa_kelas' => $value_siswa_kelas->kelas_siswa_kelas,
					'tahun_ajaran_siswa_kelas' => $value_siswa_kelas->tahun_ajaran_siswa_kelas,
					'status_siswa_kelas' => 1,
					'tanggal_registrasi_siswa_kelas' => $value_siswa_kelas->tanggal_registrasi_siswa_kelas,
					'user_added_siswa_kelas' => $this->session->userdata('fullname'),
					'date_added_siswa_kelas' => date("Ymd"),
				);
		    }
			//
			$param = new QueryParameter();
	    	$data_siswa_kelas = $this->SiswaKelas->get_list_table($param->getClause());

	    	foreach ($data_siswa_kelas as $key => $value) {
	    		if($value_data->id_siswa==$value->header_id_siswa_kelas){
		    		$value->status_siswa_kelas=0;
		    		$data_old_kelas[$key] =  array(
						'header_id_siswa_kelas' => $value->header_id_siswa_kelas,
						'tahun_ajaran_siswa_kelas' => $value->tahun_ajaran_siswa_kelas,
						'status_siswa_kelas' => $value->status_siswa_kelas
					);
				}
	    	}
			//
			foreach ($value_data->siswa_detil as $key_siswa_detil => $value_siswa_detil) {

				// $value_data->siswa_detil[$key_siswa_detil]->id_siswa_detil=$utils->longNumberId();
				// $value_data->siswa_detil[$key_siswa_detil]->header_id_siswa_detil=$value_data->id_siswa;
	    		$data_detil[] =  array(
					'id_siswa_detil' => $utils->longNumberId(),
					'header_id_siswa_detil' => $value_data->id_siswa,
					'user_added_siswa_detil' => $this->session->userdata('fullname'),
					'date_added_siswa_detil' => date("Ymd"),
					'tingkat_siswa_detil' => $value_siswa_detil->tingkat_siswa_detil,
		            'besar_iuran_siswa_detil' => $value_siswa_detil->besar_iuran_siswa_detil,
		            'jenis_iuran_siswa_detil' =>  $value_siswa_detil->jenis_iuran_siswa_detil
				);
	    		// $data_detil=$value_data->siswa_detil;
			}
			

				// print_r($data_detil);
			// $data_detil[$key_data][]=$value_siswa_detil;
			
			foreach ($value_data->siswa_iuran as $key_siswa_iuran => $value_siswa_iuran) {
				$value_data->siswa_iuran[$key_siswa_iuran]->id_iuran=$utils->longNumberId();
				$value_data->siswa_iuran[$key_siswa_iuran]->id_siswa_iuran=$value_data->id_siswa;
				$bulan_iuran = new DateTime($value_siswa_iuran->bulan_iuran);
				$bulan_iuran = $bulan_iuran->format("M")." ".$bulan_iuran->format("Y");
				$value_data->siswa_iuran[$key_siswa_iuran]->bulan_iuran=($value_siswa_iuran->jenis_iuran=="IURAN_PER_BULAN"?$bulan_iuran:"");
				$value_data->siswa_iuran[$key_siswa_iuran]->id_siswa_kelas_iuran=$data_kelas[$key_data]['id_siswa_kelas'];
				$value_data->siswa_iuran[$key_siswa_iuran]->no_iuran=$same_no_iuran;
				$value_data->siswa_iuran[$key_siswa_iuran]->user_added_iuran=$this->session->userdata('fullname');
				$value_data->siswa_iuran[$key_siswa_iuran]->date_added_iuran=date("Ymd");

	    		$data_iuran[]=$value_siswa_iuran;
			}
	    }

	     $data = array(
        	'siswa' => $data_siswa,
        	'kelas' => $data_kelas,
        	'detil' => $data_detil,//iuran
        	'iuran' => $data_iuran,
        );

	    // die("WORK");
	    // print_r($data);
	    // die();
	    //
	    $duplicate=false;
		$param_du = new QueryParameter();
		$other_data=[];
		foreach ($data_siswa as $key => $value) {
			$other_data[]=$value['ref_siswa'];
			if($key==0)
			$param_du->setClause($param_du->getClause() . " AND ref_siswa" . "='".$value['ref_siswa']."'");
			else
			$param_du->setClause($param_du->getClause() . " OR ref_siswa" . "='".$value['ref_siswa']."'");	
		}
		foreach ($this->Siswa->get_list_table(new QueryParameter()) as $key_data => $value_data) {
			$other_data[]=$value_data->ref_siswa;
		}
		// print_r($other_data);
		// print_r(array_count_values($other_data));
		$data_duplicate=[];
		foreach (array_count_values($other_data) as $key_count => $value_count) {

			if(empty($this->Siswa->get_list_table($param_du))){
				if($value_count > 1){
					$duplicate=true;
					$data_duplicate[]=$key_count;
				}
			}else{
				if($value_count >= 2){
					$duplicate=true;
					$data_duplicate[]=$key_count;
				}
			}
		}
		if($duplicate){
			$resp['data']=$data_duplicate;
			$resp['code']=http_response_code(409);
	    	$resp['message'] = "Terjadi DUPLIKASI data";

	    	return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(409)
	        ->set_output(json_encode($resp));
		}
	    //
	    // print_r($data);
	    // die();
	    if(count($data_old_kelas) > 0)
	    $this->SiswaKelas->updateAll($data_old_kelas);
	    //
		$this->Siswa->save_siswa_kelas_iuran($data);
	    //

    	$resp['message'] = "Data telah berhasil disimpan.";
        $resp['data'] = $data;    

		return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(200)
	        ->set_output(json_encode($resp));
	}

	public function save() {
		
		$data = array(
			'id_siswa' => "",
            'nis_siswa'=> "",
            'ref_siswa'=> "",
            'nama_lengkap_siswa'=> "",
            'kelas_siswa'=>  "",
            'tahun_ajaran_siswa'=>  "",
            'jk_siswa'=> "",
            'alamat_lengkap_siswa'=> "",
            'file_siswa'=> ""
        );
        $resp = array(
				"code" => "code",
				"message" => "message",
                "data" => [],
        );
		$id = $this->input->post('id');

		if(isset($id)){
			$data['id_siswa'] = $id;
		}else{
			$data['id_siswa'] = $this->Siswa->newId();
		}
		$data['nis_siswa'] = $this->input->post('nis');
		$data['ref_siswa'] = $this->input->post('ref');
		$data['nama_lengkap_siswa'] = $this->input->post('nama_lengkap');
		$data['kelas_siswa'] = $this->input->post('kelas_siswa');
		$data['tahun_ajaran_siswa'] = $this->input->post('tahun_ajaran');
		$data['jk_siswa'] = $this->input->post('jk');
		$data['alamat_lengkap_siswa'] = $this->input->post('alamat_lengkap');
		#$data['file_siswa'] = $this->input->post('file');

		$resp['data'] = $data;	
        if(isset($id)){
			$this->Siswa->update($data);
			$resp['message'] = "Data berhasil diupdate.";
		}else{
			$this->Siswa->save($data);
			$data_arr = json_decode(json_encode($_POST["list_iuran"],FALSE));
			$data_arr=json_decode($data_arr); 

			//print_r($data_arr);
			$cart = array();
			foreach ($data_arr as $key => $value) {
				$cart[$key] =  array(
					'header_id_siswa_detil' => $data['id_siswa'],
					'besar_iuran_siswa_detil' => $value->besar_jenis_iuran,
					'jenis_iuran_siswa_detil' => $value->nama_jenis_iuran
				);
			}
			$this->Siswa->save_detil($cart);
			$resp['message']="Data berhasil disimpan.";
		}
        echo json_encode($resp);
	}

	public function save_siswa_kelas(){
	    $resp = array(
				"code" => http_response_code(200),
				"message" => "success",
	            "data" => [],
	    );
		$data_arr = json_decode(json_encode($_POST["list_siswa_kelas"],FALSE));
		$data_arr=json_decode($data_arr); 

		// print_r($data_arr);
		// $arr = array();
		// foreach ($data_arr as $key => $value) {
		// 	$arr[$key] =  array(
		// 		'header_id_siswa_detil' => $data['id_siswa'],
		// 		'besar_iuran_siswa_detil' => $value->besar_jenis_iuran,
		// 		'jenis_iuran_siswa_detil' => $value->nama_jenis_iuran
		// 	);
		// }
		$this->SiswaKelas->save_kelas($data_arr);
		$resp['message']="Data berhasil disimpan.";
		$resp['data'] = $data_arr;
        return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(200)
	        ->set_output(json_encode($resp));
	}

	public function uploadDataIuran(){
		$data = array(); // Buat variabel $data sebagai array
	    if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
	      // lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
	      $upload = $this->import->upload_file($this->filename);
	      
	      if($upload['result'] == "success"){ // Jika proses upload sukses
	        // Load plugin PHPExcel nya
	        //include APPPATH.'third_party/PHPExcel/PHPExcel.php';
	        require_once APPPATH . "/third_party/PHPExcel.php";
	        $excelreader = new PHPExcel_Reader_Excel2007();
	        $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
	        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
	        
	        // Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
	        // Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
	        
	        $param = new QueryParameter();
	        $data_kelas = $this->Kelas->get_list_table2($param);
	        $data['data_kelas'] = $data_kelas;

			// $param->setClause(" ref_siswa" . " IN(".implode(",",$val).")");
	        $data_siswa = $this->Siswa->get_list_table($param);
	        $data['data_siswa'] = $data_siswa;

	        $param = new QueryParameter();
	        $val=[];
	        $new_sheet=[];
	        foreach ($sheet as $key_sheet => $value_sheet) {
	        	$value_sheet['D_status']="";
	        	if($value_sheet['A'] == 'REF' || $value_sheet['A'] == '')  continue;
	        	array_push($val, $value_sheet['A']);
	        	//
	        	
	        	$new_iuran_bulan = [];

				$bulan = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
				$bulan_2 = ["Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "Jan", "Feb", "Mar", "Apr", "May", "Jun"];

		    	$new_bulan=[];
		    	$get_year=$this->get_tahun_ajaran($value_sheet['N']);


		    	$row_D=$value_sheet['D'];
		    	//if(strpos($row_D, "S.D") || strpos($row_D, "S/D")) str_replace($row_D, "-", $row_D);
		    	//if(strpos($row_D, "S.D")) $row_D=str_replace($row_D, "-", $row_D);
		    	$row_D=str_replace("S.D", "-", $row_D);
		    	$row_D=str_replace("S/D", "-", $row_D);
		    	$row_D=str_replace("s.d", "-", $row_D);
		    	$row_D=str_replace("s/d", "-", $row_D);
		    	if($value_sheet['D'] != ""){
		    		if(strpos($row_D, "-")){
		    			$row_D=array_map('intval', explode('-', $row_D));
		    			$start_i = $row_D[0]-1;
		    			$end_i = $row_D[1]-1;
		    			$max=[];
		    			for($i=0; $i < 12; $i++){
		    				if($start_i > 11){
		    					$start_i = 0;
		    				}
		    				$max[]=$start_i;
		    				if($end_i==$start_i) {
		    					break;
		    				}
	    					$start_i++;
		    			}

		    			foreach ($max as $key_max => $value_max) {
	    					$new_value = $bulan[$value_max];
				    		$new_keys = array_search($new_value, $bulan_2);
				    		$new_iuran_bulan["bulanan"][]=$get_year[$new_keys];
		    			}
		    		}else{
		    			foreach (explode(",", $row_D) as $key => $value) {
				    		$new_value = $bulan[$value-1];
				    		$new_keys = array_search($new_value, $bulan_2);
				    		$new_iuran_bulan["bulanan"][]=$get_year[$new_keys];
				    	}
		    		}
			    }
			    // $value_sheet['D']=json_encode($new_iuran_bulan);
			    if($new_iuran_bulan != null)
			    foreach ($new_iuran_bulan["bulanan"] as $key_iuran_bulan => $value_iuran_bulan) {
			        $param = new QueryParameter();
			        $param->setClause($param->getClause() . " AND id_siswa_iuran" . "='".$this->cek_siswa($data_siswa, $value_sheet['A'])."'");
		    		$param->setClause($param->getClause() . " AND bulan_iuran" . "='".$value_iuran_bulan."'");
		    		//$param = $param->getClause();
			        $data_iuran = $this->Iuran->get_list_table($param);
			        foreach ($data_iuran as $key_iuran => $value_iuran) {
			        	$value_sheet['D_status']=$value_iuran->status_iuran;//json_encode($value_iuran);//$value_iuran;
			        }
			    	// $value_sheet['D']="-".json_encode($data_iuran);
			    }
	        	//
	        	$new_sheet[]=$value_sheet;
			}
			// print_r($new_sheet);
	        $data['sheet'] = $new_sheet;
	        $data['data_siswa_kelas'] = $this->SiswaKelas->get_list_table("1");

	        //print_r($param->getClause());
	        // $data['ref_sheet'] = $this->Siswa->newRef();
	      }else{ // Jika proses upload gagal
	        $data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
	      }
	    }
	    // print_r($_POST['uploadFile']);
	    // print_r($upload);
	    // print_r('excel/'.$this->filename.'.xlsx');
	    $this->load->view('siswa_iuran_import_view', $data);
	}

	public function cek_siswa($data, $val){
	    $status="";
	    foreach ($data as $key => $value) {
	      $new_val1=preg_replace('/\s+/', ' ', trim($val));
	      $new_val2=preg_replace('/\s+/', ' ', trim($value->ref_siswa));
	       if($new_val1==$new_val2){
	        $status=$value->id_siswa;
	       }
	     } 
	     return $status;
	  }

	public function uploadData(){

		// if($this->input->post('submit')) {
		// 	$path = 'uploads/';
  //           require_once APPPATH . "/third_party/PHPExcel.php";
  //           $config['upload_path'] = $path;
  //           $config['allowed_types'] = 'xlsx|xls';
  //           $config['remove_spaces'] = TRUE;
  //           $this->load->library('upload', $config);
  //           $this->upload->initialize($config);

  //           if (!$this->upload->do_upload('uploadFile')) {
  //               $error = array('error' => $this->upload->display_errors());
  //           } else {
  //               $data = array('upload_data' => $this->upload->data());
  //           }
  //           if(empty($error)){
  //             if (!empty($data['upload_data']['file_name'])) {
  //               $import_xls_file = $data['upload_data']['file_name'];
  //             } else {
  //               $import_xls_file = 0;
  //             }

  //           $inputFileName = $path . $import_xls_file;
			
  //           try {
  //               $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
  //               $objReader = PHPExcel_IOFactory::createReader($inputFileType);
  //               $objPHPExcel = $objReader->load($inputFileName);
  //               $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
  //               $flag = true;
  //               $i=0;
  //               foreach ($allDataInSheet as $value) {
  //                 if($flag){
  //                   $flag =false;
  //                   continue;
  //                 }
  //                 $inserdata[$i]['id_siswa'] = $this->Siswa->longNumberId();
  //                 $inserdata[$i]['nama_lengkap_siswa'] = $value['C'];
  //                 $inserdata[$i]['jenis_kelamin_siswa'] = $value['D'];

  //                 $i++;
  //               }               
  //               $result = $this->import->importdata($inserdata);   
  //               if($result){
  //                 echo "Imported successfully";
  //               }else{
  //                 echo "ERROR !";
  //               }             
 
	 //          } catch (Exception $e) {
	 //               die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
	 //                        . '": ' .$e->getMessage());
	 //          }
  //         }else{
  //             echo $error['error'];
  //         }
            
		// }

		$data = array(); // Buat variabel $data sebagai array
	    if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
	      // lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
	      $upload = $this->import->upload_file($this->filename);
	      
	      if($upload['result'] == "success"){ // Jika proses upload sukses
	        // Load plugin PHPExcel nya
	        //include APPPATH.'third_party/PHPExcel/PHPExcel.php';
	        require_once APPPATH . "/third_party/PHPExcel.php";
	        $excelreader = new PHPExcel_Reader_Excel2007();
	        $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel

	        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
	        $data_siswa = $this->Siswa->get_list_table(new QueryParameter());
	        $param = new QueryParameter();
	        // $param->setClause($param->getClause() . " AND tingkat_kelas" . "='".$_POST['kelasFile']."'");
	        $param->setClause($param->getClause() . " AND tingkat_kelas" . "='X'");
	        $param->setClause($param->getClause() . " OR tingkat_kelas" . "='XI'");
	        $param->setClause($param->getClause() . " OR tingkat_kelas" . "='XII'");
			
	        $data_kelas = $this->Kelas->get_list_table2($param);
	        // Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
	        // Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
	        $data['sheet'] = $sheet;
	        $data['data_siswa'] = $data_siswa;
	        $data['data_kelas'] = $data_kelas;
	        $data['ref_sheet'] = $this->Siswa->newRef();
	      }else{ // Jika proses upload gagal
	        $data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
	      }
	    }
	    // print_r($_POST['uploadFile']);
	    // print_r($upload);
	    // print_r('excel/'.$this->filename.'.xlsx');
	    $this->load->view('siswa_import_view', $data);
	}

	public function import(){
		$resp = array(
				"code" => http_response_code(200),
				"message" => "success",
	            "data" => [],
	    );
	    $utils = new Utils();
		require_once APPPATH . "/third_party/PHPExcel.php";
	    $excelreader = new PHPExcel_Reader_Excel2007();
	    $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
	    $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
	    $master_jenis_iuran = array();
	    $data_siswa = array(); // Buat variabel $data sebagai array untuk di insert ke db
	    $data_kelas = array(); // Buat variabel $data sebagai array untuk di insert ke db
	    $data_detil = array(); // Buat variabel $data sebagai array untuk di insert ke db
	    $data_iuran = array(); // Buat variabel $data sebagai array untuk di insert ke db
	    $numrow = 1;
	    $key=0;
	    $arr_iuran = array();
	    
	    $errList = array();

	    foreach ($sheet as $row) {
	    	// if(empty($row['A'])) continue;
	    	if($numrow > 2){
	    		if(empty($row['A'])) continue;
	    		// if(explode(" ", $row['D'])[0]=="X"){
		    		array_push($data_siswa, array(
		    			'siswa' => array(
		    				'id_siswa' =>  $utils->guidv4(),
			    			'ref_siswa' => $row['A'],
			    			'nama_lengkap_siswa' => $row['B'],
			    			'jenis_kelamin_siswa' => $row['C'],
							'tahun_angkatan_siswa' => $row['J'],
							'keterangan_masuk_siswa' => $row['Q'],
							'status_aktif_siswa' => 1,
							'user_added_siswa' => $this->session->userdata('fullname'),
							'date_added_siswa' => date("Ymd")
		    			),
						'kelas_siswa' => $row['D'],
						'iuran' => array(
			    			'bulanan' => $row['E'],
			    			'dpmp' => $row['F'],
			    			'du' => $row['G'],
			    			'osis' => $row['G'],
			    			'tabungan' => $row['H'],
			    		), 
						'iuran_bayaran' => array(
			    			'osis' => $row['L'],
			    			'bulanan' => $row['N'],
			    			'dpmp' => $row['O'],
			    			'tabungan' => $row['M'],
			    		),
						'tanggal_iuran_bayaran' => $row['P'],
						'tanggal_registrasi' => $row['K'],
						'tahun_ajaran' => $row['I']
		    		));// $key++;

		    		
				/*rose
				if(sizeof($errList) > 0) {
	    			//resp.setMessage("Satker Belum terdaftar pada master database :"+Arrays.asList(errList).get(0));
					$resp['message']="Satker Belum terdaftar pada master database :";
					//$resp['data'] = $data;
			        return $this->output
				        ->set_content_type('application/json')
				        ->set_status_header(400)
				        ->set_output(json_encode($resp));
				}*/
	    
    			// }else{
		    // 		array_push($data_siswa, array(
		    // 			'siswa' => array(
		    // 				'id_siswa' =>  $utils->guidv4(),
			   //  			'ref_siswa' => $row['A'],
			   //  			'nama_lengkap_siswa' => $row['B'],
			   //  			'jenis_kelamin_siswa' => $row['C'],
						// 	'tahun_angkatan_siswa' => $row['I'],
						// 	'keterangan_masuk_siswa' => $row['O'],
						// 	'status_aktif_siswa' => 1,
						// 	'user_added_siswa' => $this->session->userdata('fullname'),
						// 	'date_added_siswa' => date("Ymd")
		    // 			),
						// 'kelas_siswa' => $row['D'],
						// 'iuran' => array(
			   //  			'bulanan' => $row['E'],
			   //  			'dpmp' => $row['F'],
			   //  			'du' => $row['G'],
			   //  			'osis' => $row['G'],
			   //  		),
						// 'iuran_bayaran' => array(
			   //  			'osis' => $row['K'],
			   //  			'bulanan' => $row['L'],
			   //  			'dpmp' => $row['M'],
			   //  		),
						// 'tanggal_iuran_bayaran' => $row['N'],
						// 'tanggal_registrasi' => $row['J'],
						// 'tahun_ajaran' => $row['H']
		    // 		));
    			// }
    		}
	    	$numrow++;
    	}
	    foreach ($data_siswa as $key => $value) {
	    	$param_for_kelas = new QueryParameter();
			$param_for_kelas->setClause($param_for_kelas->getClause() . " AND kelas" . "='".$value['kelas_siswa']."'");
			$data_for_kelas = $this->Kelas->get_list_table2($param_for_kelas)[0];


    		array_push($data_kelas, array(
    			'id_siswa_kelas' =>  $utils->guidv4(),
				'header_id_siswa_kelas' => $value['siswa']['id_siswa'],
				'kelas_siswa_kelas' => $data_for_kelas->id_kelas,
				'tahun_ajaran_siswa_kelas' => $value['tahun_ajaran'],
				'tanggal_registrasi_siswa_kelas' => $value['tanggal_registrasi'],
				'status_siswa_kelas' => $value['siswa']['status_aktif_siswa'],
				'user_added_siswa_kelas' => $this->session->userdata('fullname'),
				'date_added_siswa_kelas' => date("Ymd")
			));

		 	$master_jenis_iuran[] = array(
                    array(
		    			'id_siswa_detil' =>  $utils->guidv4(),
						'header_id_siswa_detil' => $value['siswa']['id_siswa'],
                        "tingkat_siswa_detil" => explode(" ", $value['kelas_siswa'])[0],
                        "besar_iuran_siswa_detil" => $value['iuran']['du'],
                        "jenis_iuran_siswa_detil" => "DU",
						'user_added_siswa_detil' => $this->session->userdata('fullname'),
						'date_added_siswa_detil' => date("Ymd")
                    ),
                    array(
		    			'id_siswa_detil' =>  $utils->guidv4(),
						'header_id_siswa_detil' => $value['siswa']['id_siswa'],
                        "tingkat_siswa_detil" => explode(" ", $value['kelas_siswa'])[0],
                        "besar_iuran_siswa_detil" => $value['iuran']['bulanan'],
                        "jenis_iuran_siswa_detil" => "IURAN_PER_BULAN",
						'user_added_siswa_detil' => $this->session->userdata('fullname'),
						'date_added_siswa_detil' => date("Ymd")
                    ),
                    array(
		    			'id_siswa_detil' =>  $utils->guidv4(),
						'header_id_siswa_detil' => $value['siswa']['id_siswa'],
                        "tingkat_siswa_detil" => "ALL",
                        "besar_iuran_siswa_detil" => $value['iuran']['dpmp'],
                        "jenis_iuran_siswa_detil" => "DPMP",
						'user_added_siswa_detil' => $this->session->userdata('fullname'),
						'date_added_siswa_detil' => date("Ymd")
                    ),
            );

		 	// if(explode(" ", $value['kelas_siswa'])[0]=="X"){
	            array_push($master_jenis_iuran[$key], array(
		    			'id_siswa_detil' =>  $utils->guidv4(),
						'header_id_siswa_detil' => $value['siswa']['id_siswa'],
	                    "tingkat_siswa_detil" => explode(" ", $value['kelas_siswa'])[0],
	                    "besar_iuran_siswa_detil" => $value['iuran']['tabungan'],
	                    "jenis_iuran_siswa_detil" => "TABUNGAN",
						'user_added_siswa_detil' => $this->session->userdata('fullname'),
						'date_added_siswa_detil' => date("Ymd")
	            ));
	        // }

			$date_from = new DateTime($value['tanggal_iuran_bayaran']);
			$bulan_iuran = $date_from->format("M")." ".$date_from->format("Y");
			$bulan_iuran_osis = $date_from->format("M")." ".$date_from->format("Y");

			// print_r($bulan_iuran_osis);
			// die();
			// $selisih = $angkatan->format("Y")+$date->format("Y")-$angkatan->format("Y");
			
    		$key_array_jenis_iuran = ["DU", "IURAN_PER_BULAN", "DPMP", "TABUNGAN"];
    		$getSameRandom=$utils->getAlphaNumericString(10);
    		// if($value['iuran_bayaran'][$key_array_jenis_iuran[$key_jenis_iuran_new]]!="")
        	
			$index_iuran_bayaran=0;
			// $new_m=[];
        	foreach ($value['iuran_bayaran'] as $key_iuran_bayaran => $value_iuran_bayaran) {

        		if($value_iuran_bayaran!="")
        		array_push($data_iuran, array(
					'id_iuran' =>  $utils->guidv4(),
	    			'id_siswa_iuran' => $value['siswa']['id_siswa'],
	    			'id_siswa_kelas_iuran' => $data_kelas[$key]['id_siswa_kelas'],
	                "bulan_iuran" => ($key_array_jenis_iuran[$index_iuran_bayaran]=="IURAN_PER_BULAN"||$key_array_jenis_iuran[$index_iuran_bayaran]=="TABUNGAN"?$bulan_iuran:""),
	                "besar_iuran" => $value_iuran_bayaran,
	                "jenis_iuran" => $key_array_jenis_iuran[$index_iuran_bayaran],
	                "tanggal_iuran" => $value['tanggal_iuran_bayaran'],
	                "status_iuran" => ($value['iuran'][$key_iuran_bayaran]==$value_iuran_bayaran?"LUNAS":"BELUM_LUNAS"),
	                "keterangan_iuran" => "REGISTRASI_ULANG",
					'no_iuran' => $getSameRandom,
					'user_added_iuran' => $this->session->userdata('fullname'),
					'date_added_iuran' => date("Ymd")
	            ));
            	$index_iuran_bayaran++;
        	}
        	
	     //    foreach ($master_jenis_iuran as $key_jenis_iuran => $value_jenis_iuran) {

	     //    	foreach ($value_jenis_iuran as $key_jenis_iuran_new => $value_jenis_iuran_new) {
		    // 		if($value['iuran_bayaran'][$key_array_jenis_iuran[$key_jenis_iuran_new]]!="")
		    //     	array_push($data_iuran, array(
	    	// 			'id_iuran' =>  $utils->longNumberId(),
		    // 			'id_siswa_iuran' => $value['siswa']['id_siswa'],
		    // 			'id_siswa_kelas_iuran' => $data_kelas[$key]['id_siswa_kelas'],
	     //                "bulan_iuran" => ($value_jenis_iuran_new['jenis_iuran_siswa_detil']=="IURAN_PER_BULAN"?$bulan_iuran:""),
	     //                "besar_iuran" => $value['iuran_bayaran'][$key_array_jenis_iuran[$key_jenis_iuran_new]],
	     //                "jenis_iuran" => $value_jenis_iuran_new['jenis_iuran_siswa_detil'],
	     //                "tanggal_iuran" => $value['tanggal_iuran_bayaran'],
	     //                "status_iuran" => ($value_jenis_iuran_new['besar_iuran_siswa_detil']==$value['iuran_bayaran'][$key_array_jenis_iuran[$key_jenis_iuran_new]]?"LUNAS":"BELUM_LUNAS"),
	     //                "keterangan_iuran" => "REGISTRASI_ULANG",
						// 'no_iuran' => $getSameRandom,
						// 'user_added_iuran' => $this->session->userdata('fullname'),
						// 'date_added_iuran' => date("Ymd")
	     //            ));
	     //    	}
	     //    }

    		/*
            var elem=this;
            $.each(master_jenis_iuran, function(key){
                if($(elem)[0].cells[6+master_jenis_iuran.length+key].children[0].value!=""){
                    var tanggal_iuran;
                    if(master_jenis_iuran.length==3){
                        tanggal_iuran=$(elem)[0].cells[12].children[0].value;
                        //
                        keterangan_masuk_siswa=$(elem)[0].cells[13].children[0].value;
                        tahun_angkatan_siswa=check_tahun_ajaran_by($(elem)[0].cells[8].children[0].value);
                        tanggal_registrasi_siswa=$(elem)[0].cells[8].children[0].value
                    }else if(master_jenis_iuran.length==4){
                        tanggal_iuran=$(elem)[0].cells[14].children[0].value;
                        //
                        keterangan_masuk_siswa=$(elem)[0].cells[15].children[0].value;
                        tahun_angkatan_siswa=check_tahun_ajaran_by($(elem)[0].cells[9].children[0].value);
                        tanggal_registrasi_siswa=$(elem)[0].cells[9].children[0].value
                    }
                    master_iuran.push(
                        {
                            "bulan_iuran": "Jul "+tahun_angkatan_siswa.split("/")[0],
                            "besar_iuran" : $(elem)[0].cells[6+master_jenis_iuran.length+key].children[0].value,
                            "jenis_iuran" : this.jenis_iuran_siswa_detil,
                            "tanggal_iuran" : tanggal_iuran,
                            "status_iuran" : ($(elem)[0].cells[5+key].children[0].value==$(elem)[0].cells[6+master_jenis_iuran.length+key].children[0].value?"LUNAS":"BELUM_LUNAS"),//"LUNAS",
                            "keterangan_iuran": "REGISTRASI_ULANG",
                        }
                    );
                }
            });
    		*/
    		//
			// foreach ($value['iuran'] as $key_iuran => $value_iuran) {
				// array_push($data_detil, array(
	   //  			'id_siswa_detil' =>  $utils->longNumberId(),
				// 	'header_id_siswa_detil' => $value['siswa']['id_siswa'],
				// 	'tingkat_siswa_detil' => explode(" ", $value['kelas_siswa'])[0],
				// 	'jenis_iuran_siswa_detil' => 'IURAN_PER_BULAN',
				// 	'besar_iuran_siswa_detil' => $value['iuran']['bulanan'],
				// ));
	   //  		array_push($data_detil, array(
	   //  			'id_siswa_detil' =>  $utils->longNumberId(),
				// 	'header_id_siswa_detil' => $value['siswa']['id_siswa'],
				// 	'tingkat_siswa_detil' => "ALL",//explode(" ", $value['kelas_siswa'])[0],
				// 	'jenis_iuran_siswa_detil' => 'DPMP',
				// 	'besar_iuran_siswa_detil' => $value['iuran']['dpmp'],
				// ));
	   //  		array_push($data_detil, array(
	   //  			'id_siswa_detil' =>  $utils->longNumberId(),
				// 	'header_id_siswa_detil' => $value['siswa']['id_siswa'],
				// 	'tingkat_siswa_detil' => explode(" ", $value['kelas_siswa'])[0],
				// 	'jenis_iuran_siswa_detil' => 'DU',
				// 	'besar_iuran_siswa_detil' => $value['iuran']['du'],
				// ));
	   //  		if(explode(" ", $value['kelas_siswa'])[0] == "X")
	   //  		array_push($data_detil, array(
	   //  			'id_siswa_detil' =>  $utils->longNumberId(),
				// 	'header_id_siswa_detil' => $value['siswa']['id_siswa'],
				// 	'tingkat_siswa_detil' => explode(" ", $value['kelas_siswa'])[0],
				// 	'jenis_iuran_siswa_detil' => 'TABUNGAN',
				// 	'besar_iuran_siswa_detil' => $value['iuran']['tabungan'],
				// ));
    		// }

	  //   	$tanggal_iuran_bayaran=$value['tanggal_iuran_bayaran'];
   //  		if($tanggal_iuran_bayaran != ""){
   //  			//BULANAN & OSIS & TABUNGAN
			//     $new_iuran_bulan=['7']; //pasti mulai dari bulan juli
			//     // $new_iuran_bulan=explode(",", $row['D']);
   //  			$bulan = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
		 //    	$new_bulan=[];

		 //    	$get_year=$this->get_tahun_ajaran($data_kelas[$key]['tahun_ajaran_siswa_kelas']);

		 //    	foreach ($new_iuran_bulan as $key_new_iuran => $value_new_bulan) {
		 //    		array_push($new_bulan, $get_year[$key_new_iuran]);
		 //    	}

	  //   		foreach ($new_bulan as $key_bulanan => $value_bulanan) {
	  //   			$getSameRandom=$utils->getAlphaNumericString(10);
	  //   			if($value['iuran_bayaran']['bulanan']!="")
	  //   			array_push($data_iuran, array(
		 //    			// 'id_iuran' =>  $this->Siswa->longNumberId(),
	  //   				'id_iuran' =>  $utils->longNumberId(),
		 //    			'id_siswa_iuran' => $value['siswa']['id_siswa'],
		 //    			'id_siswa_kelas_iuran' => $data_kelas[$key]['id_siswa_kelas'],//$data_for_kelas->id_kelas,
		 //    			'bulan_iuran' => $value_bulanan,
		 //    			'besar_iuran' => $value['iuran_bayaran']['bulanan'],//substr(str_replace(",","",$new_iuran_dpmp), 4) ,
			// 			'jenis_iuran' => "IURAN_PER_BULAN",
			// 			'status_iuran' => ($value['iuran']['bulanan']==$value['iuran_bayaran']['bulanan']?"LUNAS":"BELUM_LUNAS"),
			// 			'keterangan_iuran' => "REGISTRASI_ULANG",
			// 			'jatuh_tempo_iuran' => "9999-00-00",
			// 			'no_iuran' => $getSameRandom,
			// 			'tanggal_iuran' => $tanggal_iuran_bayaran,
			// 			'user_added_iuran' => $this->session->userdata('fullname'),
			// 			'date_added_iuran' => date("Ymd")
		 //    		));
		 //    		//
	  //   			if($value['iuran_bayaran']['osis']!="")
	  //   			array_push($data_iuran, array(
		 //    			// 'id_iuran' =>  $this->Siswa->longNumberId(),
	  //   				'id_iuran' =>  $utils->longNumberId(),
		 //    			'id_siswa_iuran' => $value['siswa']['id_siswa'],
		 //    			'id_siswa_kelas_iuran' => $data_kelas[$key]['id_siswa_kelas'],//$data_for_kelas->id_kelas,
		 //    			'bulan_iuran' => $value_bulanan,
		 //    			'besar_iuran' => $value['iuran_bayaran']['osis'],//substr(str_replace(",","",$new_iuran_dpmp), 4) ,
			// 			'jenis_iuran' => "DU",
			// 			'status_iuran' => ($value['iuran']['du']==$value['iuran_bayaran']['osis']?"LUNAS":"BELUM_LUNAS"),
			// 			'keterangan_iuran' => "REGISTRASI_ULANG",
			// 			'jatuh_tempo_iuran' => "9999-00-00",
			// 			'no_iuran' => $getSameRandom,
			// 			'tanggal_iuran' => $tanggal_iuran_bayaran,
			// 			'user_added_iuran' => $this->session->userdata('fullname'),
			// 			'date_added_iuran' => date("Ymd")
		 //    		));

	  //   			if($value['iuran_bayaran']['tabungan']!="")
	  //   			array_push($data_iuran, array(
		 //    			// 'id_iuran' =>  $this->Siswa->longNumberId(),
	  //   				'id_iuran' =>  $utils->longNumberId(),
		 //    			'id_siswa_iuran' => $value['siswa']['id_siswa'],
		 //    			'id_siswa_kelas_iuran' => $data_kelas[$key]['id_siswa_kelas'],//$data_for_kelas->id_kelas,
		 //    			'bulan_iuran' => $value_bulanan,//kosong karena dpmp tidak perlu
		 //    			'besar_iuran' => $value['iuran_bayaran']['tabungan'],//substr(str_replace(",","",$new_iuran_dpmp), 4) ,
			// 			'jenis_iuran' => "TABUNGAN",
			// 			'status_iuran' => ($value['iuran']['tabungan']==$value['iuran_bayaran']['tabungan']?"LUNAS":"BELUM_LUNAS"),
			// 			'keterangan_iuran' => "REGISTRASI_ULANG",
			// 			'jatuh_tempo_iuran' => "9999-00-00",
			// 			'no_iuran' => $getSameRandom,
			// 			'tanggal_iuran' => $tanggal_iuran_bayaran,
			// 			'user_added_iuran' => $this->session->userdata('fullname'),
			// 			'date_added_iuran' => date("Ymd")
		 //    		));

   //  			}

   //  			//DPMP
   //  			if($value['iuran_bayaran']['dpmp']!="")
   //  			array_push($data_iuran, array(
	  //   			// 'id_iuran' =>  $this->Siswa->longNumberId(),
	  //   			'id_iuran' =>  $utils->longNumberId(),
	  //   			'id_siswa_iuran' => $value['siswa']['id_siswa'],
	  //   			'id_siswa_kelas_iuran' => $data_kelas[$key]['id_siswa_kelas'],//$data_for_kelas->id_kelas,
	  //   			'bulan_iuran' => "",//kosong karena dpmp tidak perlu
	  //   			'besar_iuran' => $value['iuran_bayaran']['dpmp'],//substr(str_replace(",","",$new_iuran_dpmp), 4) ,
			// 		'jenis_iuran' => "DPMP",
			// 		'status_iuran' => ($value['iuran']['dpmp']==$value['iuran_bayaran']['dpmp']?"LUNAS":"BELUM_LUNAS"),
			// 		'keterangan_iuran' => "REGISTRASI_ULANG",
			// 		'jatuh_tempo_iuran' => "9999-00-00",
			// 		'no_iuran' => $getSameRandom,
			// 		'tanggal_iuran' => $tanggal_iuran_bayaran,
			// 		'user_added_iuran' => $this->session->userdata('fullname'),
			// 		'date_added_iuran' => date("Ymd")
	  //   		));

			// }
	    }
	    // print_r($data_siswa);
	    $data_new_siswa=[];
	    foreach ($data_siswa as $key => $value) {
	    	$data_new_siswa[]=$value['siswa'];
	    }

        foreach ($master_jenis_iuran as $key_jenis_iuran => $value_jenis_iuran) {
        	foreach ($value_jenis_iuran as $key => $value) {
        		$data_detil[]=$value;
        	}
        }

	    $id_siswa=$this->input->post('id_siswa');

	    //insert here
	    $data = array(
        	'id' => isset($id_siswa),
        	'siswa' => $data_new_siswa,
        	'kelas' => $data_kelas,
        	'detil' => $data_detil,//$siswa_detil,//iuran
        	'iuran' => $data_iuran,
        );

		$this->Siswa->save_siswa_kelas_iuran($data);

		$resp['message']="Data berhasil diimport.";
		$resp['data'] = $data;
        return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(200)
	        ->set_output(json_encode($resp));
	}


	public function import_iuran(){
		$resp = array(
				"code" => http_response_code(200),
				"message" => "success",
	            "data" => [],
	    );
		require_once APPPATH . "/third_party/PHPExcel.php";
	    $excelreader = new PHPExcel_Reader_Excel2007();
	    $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
	    $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
	    
	    $data_siswa = array(); // Buat variabel $data sebagai array untuk di insert ke db
	    $data_kelas = array(); // Buat variabel $data sebagai array untuk di insert ke db
	    $data_detil = array(); // Buat variabel $data sebagai array untuk di insert ke db
	    $numrow = 1;
	    $key=0;
	    $arr_iuran = array();

	    $arr_real_bulanan_iuran = array();
	    $arr_bulanan_name = array();

	    foreach ($sheet as $row) {
	    	if($numrow == 4){
	    		$arr_bulanan_name = array($row['D'], $row['E'], $row['F'], $row['G'], $row['H'], $row['I'], $row['J'], $row['K'],$row['L'], $row['M'], $row['N'], $row['O']);
	    	}
	    	if($numrow > 4){

	    		$param_for_siswa = new QueryParameter();
				$param_for_siswa->setClause($param_for_siswa->getClause() . " AND ref_siswa" . "='".$row['A']."'");
				
				$id_siswa = $this->Siswa->get_list_table($param_for_siswa)[0]->id_siswa;
				$param_for_siswa_kelas = new QueryParameter();
				$param_for_siswa_kelas->setClause($param_for_siswa_kelas->getClause() . " AND header_id_siswa_kelas" . "='".$id_siswa."'");

	    		$arr_bulanan_iuran = array($row['D'], $row['E'], $row['F'], $row['G'], $row['H'], $row['I'], $row['J'], $row['K'],$row['L'], $row['M'], $row['N'], $row['O']);
			    
			    foreach ($arr_bulanan_iuran as $key => $value) {
			    	if($value!="") array_push($arr_real_bulanan_iuran, str_replace(" ","",$value."000"));
			    }
			    //print_r($arr_real_bulanan_iuran);

			    foreach ($arr_real_bulanan_iuran as $key => $value) {
		    		array_push($data_siswa, array(
		    			// 'id_iuran' =>  $this->Siswa->longNumberId(),
		    			'id_siswa_iuran' => $id_siswa,
		    			'id_siswa_kelas_iuran' => $this->SiswaKelas->get_list_table($param_for_siswa_kelas->getClause())[0]->id_siswa_kelas,//$this->Kelas->get_list_table2($param_for_siswa_kelas)[0]->id_kelas,
		    			'bulan_iuran' => $arr_bulanan_name[$key],
		    			'besar_iuran' => $arr_real_bulanan_iuran[$key],
						'jenis_iuran' => "IURAN_PER_BULAN",
						'status_iuran' => "LUNAS",
						'jatuh_tempo_iuran' => "9999-00-00",
						'no_iuran' => "99990000",
						'tanggal_iuran' => "0000-00-00 00:00:00",
		    		));

	    		}
		    	// print_r($data_siswa);

	    	$key++;
	    	}
	    	$numrow++;
	    }

	    $this->Iuran->save_detil($data_siswa);

	    // $id_siswa=$this->input->post('id_siswa');
	    //insert here
	    // $data = array(
     //    	'id' => isset($id_siswa),
     //    	'siswa' => $data_siswa,
     //    	'kelas' => $data_kelas,
     //    	'detil' => $data_detil//iuran
     //    );

		// $param = new QueryParameter();
		// $param->setClause($param->getClause() . " AND nama_lengkap_siswa" . "='".$row['B']."'");
		// $param->setClause($param->getClause() . " OR ref_siswa" . "='".$row['A']."'");

	 //    $lst=$this->Siswa->get_list_table($param);

	 //    if(count($lst)>0 && !isset($id_siswa)){
	 //    	$check_duplicate="";
	 //    	for ($i=0; $i < count($lst); $i++) { 
	 //    		$check_duplicate.=",".$lst[$i]->nama_lengkap_siswa;
		// 	}

			// $resp['code']=http_response_code(409);
	  //   	$resp['message'] = "Terjadi DUPLIKASI pada data: ".substr($check_duplicate, 1);

	    	// return $this->output
	     //    ->set_content_type('application/json')
	     //    ->set_status_header(409)
	     //    ->set_output(json_encode($resp));
	 //    }
		// $this->Siswa->save_siswa_kelas_iuran($data);

	    //redirect("page/siswa_view");

		$resp['message']="Data berhasil diimport.";
		$resp['data'] = $data;
        return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(200)
	        ->set_output(json_encode($resp));
	}


	public function get_tahun_ajaran($string_date){
        $tahun_ajaran = explode("/", $string_date);
        $new_month=[];
        $start = $month = strtotime($tahun_ajaran[0].'-07-10');
		$end = strtotime($tahun_ajaran[1].'-06-10');
		while($month <= $end)
		{
		     // echo date('F Y', $month), PHP_EOL;
		     $new_month[]=date('M Y', $month);
		     $month = strtotime("+1 month", $month);
		}
        return $new_month;
    }

    public function replaceAll($arr, $val){
    	$new_val="";
    	foreach ($arr as $key => $value) {
    		$new_val=str_replace($value, "", $val);
    	}
    	return intval($new_val);
    }
	public function import_iuran_new(){
		$resp = array(
				"code" => http_response_code(200),
				"message" => "success",
	            "data" => [],
	    );
	    $utils = new Utils();
		require_once APPPATH . "/third_party/PHPExcel.php";
	    $excelreader = new PHPExcel_Reader_Excel2007();
	    $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
	    $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
	    
	    $data_siswa = array(); // Buat variabel $data sebagai array untuk di insert ke db
	    $data_kelas = array(); // Buat variabel $data sebagai array untuk di insert ke db
	    $data_detil = array(); // Buat variabel $data sebagai array untuk di insert ke db
	    $numrow = 1;
	    $key=0;
	    $arr_iuran = array();

	    $arr_real_bulanan_iuran = array();
	    $arr_bulanan_name = array();

	    foreach ($sheet as $row) {
	    	if($numrow > 2){
				//find kelas
	    		$param_for_kelas = new QueryParameter();
	    		// print_r($row['C']);
	    		// die()
				$param_for_kelas->setClause($param_for_kelas->getClause() . " AND kelas" . "='".$row['C']."'");
				$data_kelas = $this->Kelas->get_list_table2($param_for_kelas)[0];
				//end here

	    		$param_for_siswa = new QueryParameter();
				$param_for_siswa->setClause($param_for_siswa->getClause() . " AND ref_siswa" . "='".$row['A']."'");

				$id_siswa = $this->Siswa->get_list_table($param_for_siswa)[0]->id_siswa;

				$param_for_siswa_kelas = new QueryParameter();
				$param_for_siswa_kelas->setClause($param_for_siswa_kelas->getClause() . " AND header_id_siswa_kelas" . "='".$id_siswa."'");
				$param_for_siswa_kelas->setClause($param_for_siswa_kelas->getClause() . " AND kelas_siswa_kelas" . "='".$data_kelas->id_kelas."'");
				$data_siswa_kelas = $this->SiswaKelas->get_list_table($param_for_siswa_kelas->getClause())[0];
				

				$param_for_siswa_detil = new QueryParameter();
				$param_for_siswa_detil->setClause($param_for_siswa_detil->getClause() . " AND header_id_siswa_detil" . "='".$id_siswa."'");
				//$param_for_siswa_detil->setClause($param_for_siswa_detil->getClause() . " AND tingkat_siswa_detil" . "='".$data_kelas->tingkat_kelas."'");
				// $param_for_siswa_detil->setClause($param_for_siswa_detil->getClause() . " AND jenis_iuran_siswa_detil" . "='"."IURAN_PER_BULAN"."'");

				$data_siswa_detil = $this->SiswaDetil->get_list_table($param_for_siswa_detil->getClause());
				$arr_data_siswa_detil=[];
				$besar_iuran_bulanan=0;
				$besar_tabungan=0;
				$besar_dpmp=0;
				$besar_du=0;
				foreach ($data_siswa_detil as $key_data_siswa_detil => $value_data_siswa_detil) {
					
					if($value_data_siswa_detil->tingkat_siswa_detil=="ALL"){
						if($value_data_siswa_detil->jenis_iuran_siswa_detil=="DPMP"){
							$besar_dpmp=$value_data_siswa_detil->besar_iuran_siswa_detil;
						}
					}else{
						if($value_data_siswa_detil->jenis_iuran_siswa_detil=="IURAN_PER_BULAN"){
							$besar_iuran_bulanan=$value_data_siswa_detil->besar_iuran_siswa_detil;
						}
						if($value_data_siswa_detil->jenis_iuran_siswa_detil=="TABUNGAN"){
							$besar_tabungan=$value_data_siswa_detil->besar_iuran_siswa_detil;
						}
						if($value_data_siswa_detil->jenis_iuran_siswa_detil=="DU"){
							$besar_du=$value_data_siswa_detil->besar_iuran_siswa_detil;
						}
					}
				}

			    $new_iuran_bulan = [];

			    // print_r($new_iuran_bulan);
				$bulan = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
				$bulan_2 = ["Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "Jan", "Feb", "Mar", "Apr", "May", "Jun"];

		    	$new_bulan=[];
		    	$get_year=$this->get_tahun_ajaran($row['N']);

		    	$row_D=$row['D'];
		    	//if(strpos($row_D, "S.D") || strpos($row_D, "S/D")) str_replace($row_D, "-", $row_D);
		    	//if(strpos($row_D, "S.D")) $row_D=str_replace($row_D, "-", $row_D);
		    	$row_D=str_replace("S.D", "-", $row_D);
		    	$row_D=str_replace("S/D", "-", $row_D);
		    	$row_D=str_replace("s.d", "-", $row_D);
		    	$row_D=str_replace("s/d", "-", $row_D);
		    	// print_r($row_D."<br/>");
		    	if($row['D'] != ""){

		    		if(strpos($row_D, "-")){
		    			$row_D=array_map('intval', explode('-', $row_D));
		    			$start_i = $row_D[0]-1;
		    			$end_i = $row_D[1]-1;
		    			$max=[];
		    			for($i=0; $i < 12; $i++){
		    				if($start_i > 11){
		    					$start_i = 0;
		    				}

		    				$max[]=$start_i;

		    				if($end_i==$start_i) {
		    					break;
		    				}
	    					$start_i++;

		    			}
		    			// print_r($max);
		    			foreach ($max as $key_max => $value_max) {
	    					$new_value = $bulan[$value_max];
				    		$new_keys = array_search($new_value, $bulan_2);
				    		$new_iuran_bulan["bulanan"][]=$get_year[$new_keys];
		    			}
		    		}else{
		    			foreach (explode(",", $row_D) as $key => $value) {
				    		$new_value = $bulan[$value-1];
				    		$new_keys = array_search($new_value, $bulan_2);
				    		$new_iuran_bulan["bulanan"][]=$get_year[$new_keys];
				    	}
		    		}
		    		//print_r(explode(",", $row['D']));
			    }
			    if($row['F'] != ""){
			    	foreach (explode(",", $row['F']) as $key => $value) {
			    		$new_value = $bulan[$value-1];
			    		$new_keys = array_search($new_value, $bulan_2);
			    		$new_iuran_bulan["tabungan"][]=$get_year[$new_keys];
			    	}
			    }
			    //
		    	$getSameRandom=$utils->getAlphaNumericString(10);
		    	if($row['D'] != ""){
		    		foreach ($new_iuran_bulan['bulanan'] as $key => $value) {
		    			//
				    	$param = new QueryParameter();
						$param->setClause($param->getClause() . " AND id_siswa_iuran" . "='".$id_siswa."'");
						$param->setClause($param->getClause() . " AND id_siswa_kelas_iuran" . "='".$this->SiswaKelas->get_list_table($param_for_siswa_kelas->getClause())[0]->id_siswa_kelas."'");
						$param->setClause($param->getClause() . " AND bulan_iuran" . "='".$value."'");
						$param->setClause($param->getClause() . " AND jenis_iuran" . "='"."IURAN_PER_BULAN"."'");
						//$param->setClause($param->getClause() . " AND status_iuran" . "='"."BELUM_LUNAS"."'");
					    // print_r($param->getClause());
					    $lst_iuran_sebelum=$this->Iuran->get_list_table($param);

				    	$besar_iuran_telah_bayar=0;
					    foreach ($lst_iuran_sebelum as $key_iuran_sebelum => $value_iuran_sebelum) {
					    	if($value_iuran_sebelum->status_iuran=="BELUM_LUNAS"){
					    		$besar_iuran_telah_bayar=$value_iuran_sebelum->besar_iuran;
					    	}
					    }

				    	array_push($data_siswa, array(
			    			'id_iuran' =>  $utils->guidv4(),
			    			'id_siswa_iuran' => $id_siswa,
			    			'id_siswa_kelas_iuran' => $this->SiswaKelas->get_list_table($param_for_siswa_kelas->getClause())[0]->id_siswa_kelas,
			    			'bulan_iuran' => $value,
			    			'besar_iuran' => (substr(str_replace(",", "", $row['E']), 4)/count($new_iuran_bulan['bulanan'])),
							'jenis_iuran' => "IURAN_PER_BULAN".$row['O'],
							'status_iuran' => ($besar_iuran_bulanan-($besar_iuran_telah_bayar+intval((substr(str_replace(",", "", $row['E']), 4)/count($new_iuran_bulan['bulanan']))))==0?"LUNAS":"BELUM_LUNAS"),
							'keterangan_iuran' => "SPP ".$row['N'],
							'jatuh_tempo_iuran' => "9999-00-00",
							'no_iuran' => $getSameRandom,
							'tanggal_iuran' => $row['M']." 00:00:00",
							'user_added_iuran' => $this->session->userdata('fullname'),
							'date_added_iuran' => date('Y-m-d H:i:s'),
			    		));
		    			
		    		}
		    	}

		    	if($row['F'] != ""){
		    		foreach ($new_iuran_bulan['tabungan'] as $key => $value) {
		    			//
				    	$param = new QueryParameter();
						$param->setClause($param->getClause() . " AND id_siswa_iuran" . "='".$id_siswa."'");
						$param->setClause($param->getClause() . " AND id_siswa_kelas_iuran" . "='".$this->SiswaKelas->get_list_table($param_for_siswa_kelas->getClause())[0]->id_siswa_kelas."'");
						$param->setClause($param->getClause() . " AND bulan_iuran" . "='".$value."'");
						$param->setClause($param->getClause() . " AND jenis_iuran" . "='"."TABUNGAN"."'");
						//$param->setClause($param->getClause() . " AND status_iuran" . "='"."BELUM_LUNAS"."'");
					    // print_r($param->getClause());
					    $lst_tabungan_sebelum=$this->Iuran->get_list_table($param);
					    $besar_tabungan_telah_bayar=0;
					    foreach ($lst_tabungan_sebelum as $key_tabungan_sebelum => $value_tabungan_sebelum) {
					    	
					    	if($value_tabungan_sebelum->status_iuran=="BELUM_LUNAS"){
					    		$besar_tabungan_telah_bayar=$value_tabungan_sebelum->besar_iuran;
					    	}
			    		}

		    			array_push($data_siswa, array(
			    			'id_iuran' =>  $utils->guidv4(),
			    			'id_siswa_iuran' => $id_siswa,
			    			'id_siswa_kelas_iuran' => $this->SiswaKelas->get_list_table($param_for_siswa_kelas->getClause())[0]->id_siswa_kelas,
			    			'bulan_iuran' => $value,
			    			'besar_iuran' => (substr(str_replace(",", "", $row['G']), 4)/count($new_iuran_bulan['tabungan'])),// ((substr(str_replace(",", "", $row['H']), 4))/count($new_iuran_bulan['tabungan'])),//
							'jenis_iuran' => "TABUNGAN".$row['O'],
							'status_iuran' => ($besar_tabungan-($besar_tabungan_telah_bayar+intval((substr(str_replace(",", "", $row['G']), 4)/count($new_iuran_bulan['tabungan']))))==0?"LUNAS":"BELUM_LUNAS"),
							'keterangan_iuran' => "TABUNGAN ".$row['N'],
							'jatuh_tempo_iuran' => "9999-00-00",
							'no_iuran' => $getSameRandom,
							'tanggal_iuran' => $row['M']." 00:00:00",
							'user_added_iuran' => $this->session->userdata('fullname'),
							'date_added_iuran' => date('Y-m-d H:i:s'),
			    		));
		    		}
		    	}

				//iuran DPMP start here
			    $new_iuran_dpmp=$row['H'];
	    		if($new_iuran_dpmp != ""){
	    			$param = new QueryParameter();
					$param->setClause($param->getClause() . " AND id_siswa_iuran" . "='".$id_siswa."'");
					$param->setClause($param->getClause() . " AND id_siswa_kelas_iuran" . "='".$this->SiswaKelas->get_list_table($param_for_siswa_kelas->getClause())[0]->id_siswa_kelas."'");
					$param->setClause($param->getClause() . " AND bulan_iuran" . "='".""."'");
					$param->setClause($param->getClause() . " AND jenis_iuran" . "='"."DPMP"."'");
					//$param->setClause($param->getClause() . " AND status_iuran" . "='"."BELUM_LUNAS"."'");
				    // print_r($param->getClause());
				    $lst_dpmp_sebelum=$this->Iuran->get_list_table($param);

			    	$besar_dpmp_telah_bayar=0;
				    foreach ($lst_dpmp_sebelum as $key_dpmp_sebelum => $value_dpmp_sebelum) {
				    	if($value_dpmp_sebelum->status_iuran=="BELUM_LUNAS"){
				    		$besar_dpmp_telah_bayar=$value_dpmp_sebelum->besar_iuran;
				    	}
				    }

	    			array_push($data_siswa, array(
			    		'id_iuran' =>  $utils->guidv4(),
		    			'id_siswa_iuran' => $id_siswa,
		    			'id_siswa_kelas_iuran' => $this->SiswaKelas->get_list_table($param_for_siswa_kelas->getClause())[0]->id_siswa_kelas,
		    			'bulan_iuran' => "",//kosong karena dpmp tidak perlu
		    			'besar_iuran' => (substr(str_replace(",", "", $new_iuran_dpmp), 4)),//substr(
						'jenis_iuran' => "DPMP".$row['O'],
						'status_iuran' => ($besar_dpmp-($besar_dpmp_telah_bayar+intval((substr(str_replace(",", "", $row['H']), 4))))==0?"LUNAS":"BELUM_LUNAS"),
						'keterangan_iuran' => "DPMP ".$row['N'],
						'jatuh_tempo_iuran' => "9999-00-00",
						'no_iuran' => $getSameRandom,
						'tanggal_iuran' => $row['M']." 00:00:00",
						'user_added_iuran' => $this->session->userdata('fullname'),
						'date_added_iuran' => date('Y-m-d H:i:s'),
		    		));
	    		}

				//iuran DU start here
			    $new_iuran_du=$row['I'];
	    		if($new_iuran_du != ""){
	    			$month=explode("-", $row['M']);
			    	//print_r(intval($month[1]));
		    		$new_value = $bulan[$month[1]-1];
		    		$new_keys = array_search($new_value, $bulan_2);

		    		///*
		    			$param = new QueryParameter();
						$param->setClause($param->getClause() . " AND id_siswa_iuran" . "='".$id_siswa."'");
						$param->setClause($param->getClause() . " AND id_siswa_kelas_iuran" . "='".$this->SiswaKelas->get_list_table($param_for_siswa_kelas->getClause())[0]->id_siswa_kelas."'");
						$param->setClause($param->getClause() . " AND bulan_iuran" . "='".""."'");
						$param->setClause($param->getClause() . " AND jenis_iuran" . "='"."DU"."'");
						//$param->setClause($param->getClause() . " AND status_iuran" . "='"."BELUM_LUNAS"."'");
					    // print_r($param->getClause());
					    $lst_du_sebelum=$this->Iuran->get_list_table($param);

				    	$besar_du_telah_bayar=0;
					    foreach ($lst_du_sebelum as $key_du_sebelum => $value_du_sebelum) {
					    	if($value_du_sebelum->status_iuran=="BELUM_LUNAS"){
					    		$besar_du_telah_bayar+=$value_du_sebelum->besar_iuran;
					    	}
					    }
				    //*/

	    			array_push($data_siswa, array(
			    		'id_iuran' =>  $utils->guidv4(),
		    			'id_siswa_iuran' => $id_siswa,
		    			'id_siswa_kelas_iuran' => $this->SiswaKelas->get_list_table($param_for_siswa_kelas->getClause())[0]->id_siswa_kelas,
		    			'bulan_iuran' => "",//$get_year[$new_keys] //kosong karena du tidak perlu
		    			'besar_iuran' => (substr(str_replace(",", "", $new_iuran_du), 4)),//substr(str_replace(",","",$new_iuran_dpmp), 4) ,
						'jenis_iuran' => "DU".$row['O'],
						'status_iuran' => ($besar_du-($besar_du_telah_bayar+intval((substr(str_replace(",", "", $row['I']), 4))))==0?"LUNAS":"BELUM_LUNAS"),
						'keterangan_iuran' => "OSIS ".$row['N'],
						'jatuh_tempo_iuran' => "9999-00-00",
						'no_iuran' => $getSameRandom,
						'tanggal_iuran' => $row['M']." 00:00:00",
						'user_added_iuran' => $this->session->userdata('fullname'),
						'date_added_iuran' => date('Y-m-d H:i:s'),
		    		));

	    		}
	    		$key++;
	    	}
	    	$numrow++;
	    }

	    // print_r($data_siswa);

    	// die();
	    $data=[];

		$other_arr=[];
	    foreach ($data_siswa as $key => $value) {
	    	// if($value['bulan_iuran']!="") {
	    	$param = new QueryParameter();
			$param->setClause($param->getClause() . " AND id_siswa_iuran" . "='".$value['id_siswa_iuran']."'");
			$param->setClause($param->getClause() . " AND id_siswa_kelas_iuran" . "='".$value['id_siswa_kelas_iuran']."'");
			$param->setClause($param->getClause() . " AND bulan_iuran" . "='".$value['bulan_iuran']."'");
			$param->setClause($param->getClause() . " AND besar_iuran" . "='".$value['besar_iuran']."'");
			$param->setClause($param->getClause() . " AND jenis_iuran" . "='".$value['jenis_iuran']."'");
			$param->setClause($param->getClause() . " AND status_iuran" . "='"."LUNAS"."'");
			// }

	    	$param_check_status = new QueryParameter();
			//
			if($value['status_iuran']=="LUNAS"){
				$param_check_status->setClause($param_check_status->getClause() . " AND id_siswa_iuran" . "='".$value['id_siswa_iuran']."'");
				if($value['jenis_iuran'] == "IURAN_PER_BULAN")
				$param_check_status->setClause($param_check_status->getClause() . " AND bulan_iuran" . "='".$value['bulan_iuran']."'");
				else 
				$param_check_status->setClause($param_check_status->getClause() . " AND jenis_iuran" . "='".$value['jenis_iuran']."'");
			}
			//
			$param_check_status=$param_check_status;
			if($param_check_status!="1"){
				foreach ($this->Iuran->get_list_table($param_check_status) as $key_iuran => $value_iuran) {
					$other_arr[] =  array(
						'id_iuran' => $value_iuran->id_iuran,
						'id_siswa_iuran' => $value_iuran->id_siswa_iuran,
						'bulan_iuran' => $value_iuran->bulan_iuran,
						'status_iuran' => "LUNAS",
					);
				}
			}
			// print_r($param_check_status);
	    }
	    // print_r($param->getClause());
	    $lst=$this->Iuran->get_list_table($param);

	    // print_r($lst);
	    if(count($lst)>0){
			$resp['code']=http_response_code(409);
	    	$resp['message'] = "Terjadi DUPLIKASI, mohon periksa kembali data yang anda input.";

	    	return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(409)
	        ->set_output(json_encode($resp));
	    }

		// print_r($other_arr);
		if(sizeof($other_arr) > 0) $this->Iuran->update_detil($other_arr);
		

	    $data=$this->Iuran->save_detil($data_siswa);

		$resp['message']="Data berhasil diimport.";
		$resp['data'] = $data;
        return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(200)
	        ->set_output(json_encode($resp));
	}
}

// Class for encapsulating school data 

class QueryParameter {
	var $locale = "en_US";
	var $clause = "1";
	var $innerClause = "1";
	var $values = "";
	var $order = "";
	var $group = "";
	var $limit = 100;
	var $offset = 0;

	function getClause(){
		return $this->clause;
	}

	function setClause($val){
		$this->clause = $val;
	}

	function getOrder(){
		return $this->order;
	}

	function setOrder($val){
		$this->order = $val;
	}

	function getLimit(){
		if($this->limit > 200) return 200;
		return $limit;
	}

	function setLimit($val){
		$this->limit = $val;
	}
	//start offset

	//end offset

	function getValues(){
		return $values;
	}

	function setValues($val){
		$this->values = $val;
	}
	//start group

	//end group

	//start innerClause

	//end innerClause

	//start localse

	//end localse
}