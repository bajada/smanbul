<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class PembayaranController extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('Pembayaran');
	}

	public function index()
	{	
		$this->load->view('pembayaran_view');
	}

	public function get_by_id($id) {
		$data = $this->Pembayaran->get_by_id($id);
		$action=$this->input->get('action');
        $resp = array(
        				"code" => "code",
        				"message" => "message",
                        "data" => $data,
                );

		if(isset($action)) {
			if($action=='delete') {
				$resp['message'] = "Apakah anda yakin akan menghapus data '".$data->id_siswa_pembayaran."'?";
                
			}
		}
		echo json_encode($resp);
	}

	public function do_action($id, $action) {
        $resp = array(
        				"code" => "code",
        				"message" => "message",
                        "data" => [],
                );

		if(isset($action)) {
			if($action=='delete') {
				$data = $this->Pembayaran->get_by_id($id); //getEntity
				$resp['data'] = $data;
				$this->Pembayaran->delete($id);
				
				$resp['message'] = "Data '".$data->id_siswa_pembayaran."' berhasil dihapus.";
                
			}
		}
		echo json_encode($resp);
	}

	public function list_table() {
		$param = [];
        $resp = array(
        				"code" => "code",
        				"message" => "message",
                        "data" => [],
        );

		$filterKeyword = $this->input->get('filter_keyword');

		if(isset($filterKeyword)) {
			if($filterKeyword != ''){
				$param= $this->input->get("filter_keyword");
				$param= array(
					'id_siswa_pembayaran' => $param
				);
    		}
    	}
		$data = $this->Pembayaran->get_list_table($param);

		$resp['data'] = $data;	
		echo json_encode($resp);
	}

	public function save() {
		
		$data = array(
			'id_pembayaran' => "",
            'id_siswa_pembayaran' => "",
            'bulan_pembayaran' => "",
            'iuran_pembayaran' => "",
            'dpmp_pembayaran' => "",
            'du_pembayaran' => "",
            'keterangan_pembayaran' => "",
            'tanggal_pembayaran' => ""
        );

        $resp = array(
				"code" => "code",
				"message" => "message",
                "data" => [],
        );

		$id_pembayaran = $this->input->post('id_pembayaran');
		$id_siswa_pembayaran = $this->input->post('id_siswa_pembayaran');
		$bulan_pembayaran = $this->input->post('bulan_pembayaran');
		$iuran_pembayaran = $this->input->post('iuran_pembayaran');
		$dpmp_pembayaran = $this->input->post('dpmp_pembayaran');
		$du_pembayaran = $this->input->post('du_pembayaran');
		$keterangan_pembayaran = $this->input->post('keterangan_pembayaran');
		$tanggal_pembayaran = $this->input->post('tanggal_pembayaran');

		if(isset($id_pembayaran)){
			$data['id_pembayaran'] = $this->input->post('id_pembayaran');
		}

		$data['id_siswa_pembayaran'] = $this->input->post('id_siswa_pembayaran');
		$data['bulan_pembayaran'] = $this->input->post('bulan_pembayaran');
		$data['iuran_pembayaran'] = $this->input->post('iuran_pembayaran');
		$data['dpmp_pembayaran'] = $this->input->post('dpmp_pembayaran');
		$data['du_pembayaran'] = $this->input->post('du_pembayaran');
		$data['keterangan_pembayaran'] = $this->input->post('keterangan_pembayaran');
		$data['tanggal_pembayaran'] = $this->input->post('tanggal_pembayaran');

		$resp['data'] = $data;	
        if(isset($id_pembayaran)){
			$this->Pembayaran->update($data);
			$resp['message'] = "Data berhasil diupdate.";
		}else{
			$this->Pembayaran->save($data);
			$resp['message'] = "Data berhasil disimpan.";
		}
        echo json_encode($resp);
	}
}