<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class PageController extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}

    public function costume_url(){

        // echo $this->uri->segment(2);
        if($this->session->userdata('id_user')){
            if($this->uri->segment(2) == 'user_view'){
                if($this->session->userdata('role_user')!="ADMIN"){
                    redirect(site_url('page/dashboard_view'));
                    // echo "Anda tidak memiliki akses untuk halaman ini!";
                }
            }
            $this->load->view($this->uri->segment(2));
        }else{
            redirect(site_url('PageController'));
        }
        //$this->login();
    }

	public function index()
	{	
        if($this->session->userdata('id_user')){
            redirect(site_url('page/dashboard_view'));
            // if($this->session->userdata('role_user')!="ADMIN"){
            //     redirect(site_url('page/dashboard_view'));
            // }
        }else{
          $this->load->view('login_view');
        // $this->user_login();
        } 

	}

	public function login()
    {
        if($this->session->userdata('id_user')):     
            redirect(site_url('page/dashboard_view'));
        else :          
            //$this->load->view('login_view');
            //$this->load->view($this->uri->segment(2));

            redirect(site_url('page/login_view'));
        endif;          
    }
    
    public function user_login()
    {
        $this->load->model('User','users');
        $msg['status']=true;   
        // $login = array(
        //     'username_user'=> $this->input->post('username'),
        //     'password_user' => md5($this->input->post('password'))
        // );
        //
        $param = new QueryParameter();
        $param->setClause($param->getClause() . " AND username_user" . " ='".$this->input->post('username')."'");
        $param->setClause($param->getClause() . " AND password_user" . " ='".md5($this->input->post('password'))."'");
        $param = $param->getClause();
        // print_r($param);
        // die();
        $data = $this->users->get_list_table2($param);
        //
        // $data = $this->users->get_user($login);
        // $data = $this->users->get_user($login);
        if($data){
            $value = $data[0];
            if(sizeof($data)>0){
                // $data = $this->users->get_user($login);
                $this->session->set_userdata('username', $value->username_user);
                $this->session->set_userdata('first_name', $value->firstname_user);
                $this->session->set_userdata('fullname', $value->fullname);
                $this->session->set_userdata('last_name', $value->lastname_user);
                $this->session->set_userdata('role_user', $value->role_user);
                $this->session->set_userdata('id_user', $value->id_user);
                //echo "succ";
                redirect(site_url('page/dashboard_view'));
                //$this->load->view('login_view',$msg);
            }else{
                echo "ggl";
                $msg="GAGAL";
                $msg['status']=false;   
                $this->load->view('login_view',$msg);
                // redirect(site_url('PageController'));
            }
        }
        $this->load->view('login_view',$msg);
        //redirect(site_url('PageController'));
    }

    public function user_logout()
    {
        $this->session->unset_userdata('id_user');
        $this->login();
    }
}


class QueryParameter {
    var $locale = "en_US";
    var $clause = "1";
    var $innerClause = "1";
    var $values = "";
    var $order = "";
    var $group = "";
    var $limit = 100;
    var $offset = 0;

    function getClause(){
        return $this->clause;
    }

    function setClause($val){
        $this->clause = $val;
    }

    function getOrder(){
        return $clause;
    }

    function setOrder($val){
        $this->clause = $val;
    }

    function getLimit(){
        if($this->limit > 200) return 200;
        return $limit;
    }

    function setLimit($val){
        $this->limit = $val;
    }
    //start offset

    //end offset

    function getValues(){
        return $values;
    }

    function setValues($val){
        $this->values = $val;
    }
}