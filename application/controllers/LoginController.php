<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('User');
	}

	public function index()
	{	
		$this->load->view('login_view');
	}

	public function login()
    {
        if($user=$this->session->userdata('id_user')): 

            
            redirect(site_url('user/dashboard'));

        else :          
            $this->load->view('login_view');
        endif;          
    }
    
    public function user_login()
    {
        $login = array(
            'username'=> $this->input->post('username'),
            'password' => md5($this->input->post('password'))
        );

        $data = $this->users->get_user($login);
        foreach ($data as $value) {
            if($this->users->count_user($login)) :
                $data = $this->users->get_user($login);
                $this->session->set_userdata('username', $login['username']);
                $this->session->set_userdata('id_user', $value->nama_jabatan);
                echo 1;
            else :
               	echo 0;
            endif;
        }
    }

    public function user_logout()
    {
        $this->session->unset_userdata('user_id');
        redirect(site_url('user/login'));
    }
}