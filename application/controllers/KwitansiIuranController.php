<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class KwitansiIuranController extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('KwitansiIuran');
        $this->load->model('Siswa');
        $this->load->model('SiswaKelas');
	}

	public function index()
	{	
		$this->load->view('kwitansi_iuran_view');
	}

	public function get_by_id($id) {
		$data = $this->Siswa->get_by_id($id);
		$action=$this->input->get('action');
        $resp = array(
        				"code" => "200",
        				"message" => "success",
                        "data" => $data,
                );

		if(isset($action)) {
			if($action=='delete') {
				$resp['message'] = "Apakah anda yakin akan menghapus data '".$data->id_siswa."'?";
                
			}
		}
		echo json_encode($resp);
	}
	public function get_ref_now() {
		$data = $this->Siswa->newRef();
        $resp = array(
        				"code" => "200",
        				"message" => "success",
                        "data" => $data,
                );
		echo json_encode($resp);
	}
	public function do_action($id, $action) {
        $resp = array(
        				"code" => "code",
        				"message" => "message",
                        "data" => [],
                );

		if(isset($action)) {
			if($action=='delete') {
				$data = $this->Siswa->get_by_id($id); //getEntity
				$resp['data'] = $data;
				$this->Siswa->delete($id);
				
				$resp['message'] = "Data '".$data->id_siswa."' berhasil dihapus.";
                
			}
		}
		echo json_encode($resp);
	}

	public function list_table() {
		$param = "1";
        $resp = array(
        				"code" => http_response_code(200),
        				"message" => "success",
                        "data" => [],
        );
  //       $kelas=$this->input->get('kelas_siswa');//
  //       $tahun_ajaran=$this->input->get('tahun_ajaran_siswa');
		// $param.=" AND "."tahun_ajaran_siswa"." = "."'".$tahun_ajaran."'";
        $param = new QueryParameter();

		$filterKeyword = $this->input->get('filter_keyword');
		if(isset($filterKeyword) && $filterKeyword != '') {
			$param->setClause($param->getClause() . " AND kode_kwitansi_iuran" . "='".$filterKeyword."'");
			$param->setClause($param->getClause() . " OR ref_siswa" . "='".$filterKeyword."'");
		}
		$param = $param->getClause();
		$data = $this->KwitansiIuran->get_list_table2($param);

		$resp['data'] = $data;	
		echo json_encode($resp);
	}

	public function list_kelas_table() {
		$param = "1";
        $resp = array(
			"code" => http_response_code(200),
			"message" => "success",
	        "data" => [],
        );
		//$param = $param." AND "."header_id_siswa_kelas"." = "."'".$data_siswa->id_siswa."'";
		$tahun_ajaran=$this->input->get('tahun_ajaran_siswa');
		if(isset($tahun_ajaran) && $tahun_ajaran != ''){
			$param.=" AND "."tahun_ajaran_siswa"." = "."'".$tahun_ajaran."'";
		}else{
			$param.=" AND "."status_siswa_kelas"." = "."'1'";
		}
		// $data_siswa;
		$data = $this->SiswaKelas->get_list_table($param);
		foreach ($data  as $key => $value) {
			$param = "1";
			$data_siswa = $this->Siswa->get_list_table($param);
			// array_push($data, array( 'bar' => '1234' ));
			$data_siswa = $this->Siswa->get_by_id($value->header_id_siswa_kelas);
			$value->header_id_siswa_kelas=$data_siswa;
		}
		// $data = (object) array_merge( (array)$data, array( 'bar' => '1234' ) );
		$arrayName = array(
			'data_siswa' => $this->Siswa->get_list_table("1"),
			'data_siswa_kelas' => $data
		);
		// print_r($arrayName);
		$resp['data'] = $arrayName;	
		echo json_encode($resp);
	}


	public function save_all() {
	    $resp = array(
				"code" => http_response_code(200),
				"message" => "success",
	            "data" => [],
	    );

	    $data_siswa=array();
	    $data_detil=array();
	    $data_kelas=array();
	    $data_multi_kelas=array();

		//
		$year=date("Y");
		$s_year=date("Y");
		$e_year=date("Y")+1;
		$month=date("m");
		if($month>6){
			$s_year=$year;
			$e_year=($year+1);
		}else{
			$s_year=($year-1);
			$e_year=$year;
		}
	  	$full_year=$s_year."/".$e_year;
		$begin = new DateTime( $s_year.'-07-10' );
		$end = new DateTime( $e_year.'-06-10' );
		$end = $end->modify( '+1 month' ); 

		$interval = new DateInterval('P1M');
		$daterange = new DatePeriod($begin, $interval ,$end);
		$split=explode("/", $full_year);
		//

	    $pass=true;
		for ($i=0; $i < count($this->input->post('ref')); $i++) {
			$id_siswa = $this->input->post('id_siswa')[$i];
			$ref_siswa = $this->input->post('ref')[$i];
			$nama_lengkap_siswa = $this->input->post('nama_lengkap')[$i];
			$jenis_kelamin_siswa = $this->input->post('jenis_kelamin')[$i];
			$tahun_ajaran_siswa = $this->input->post('tahun_ajaran')[$i];
			// $tahun_angkatan_siswa = explode("/", $tahun_ajaran_siswa);

			$data_siswa[$i] =  array(
				'id_siswa' => $this->Siswa->longNumberId(),
				'ref_siswa' => null,
				'nama_lengkap_siswa' => null,
				// 'kelas_siswa' => $this->input->post('kelas')[$i],
				'jenis_kelamin_siswa' => null,
				'tahun_ajaran_siswa' => null,
				'tahun_angkatan_siswa' => null
			);

			// print_r($id_siswa . "<");	
			if(isset($id_siswa)){
				$data_siswa[$i]['id_siswa'] =  $id_siswa;
				//harusnya ada check apakah id exist or not???
			}

			if(isset($ref_siswa)) $data_siswa[$i]['ref_siswa'] = $ref_siswa;
			if(isset($nama_lengkap_siswa)) $data_siswa[$i]['nama_lengkap_siswa'] = $nama_lengkap_siswa;
			if(isset($jenis_kelamin_siswa)) $data_siswa[$i]['jenis_kelamin_siswa'] = $jenis_kelamin_siswa;
			if(isset($ref_siswa)) $data_siswa[$i]['ref_siswa'] = $ref_siswa;
			//tahun ajaran & angkatan
			if(isset($tahun_ajaran_siswa)) {
				$data_siswa[$i]['tahun_ajaran_siswa'] = ($tahun_ajaran_siswa==$full_year?$full_year:$tahun_ajaran_siswa);
				$data_siswa[$i]['tahun_angkatan_siswa'] = explode("/", $tahun_ajaran_siswa)[0];
			}

			

			$data_multi_kelas[$i] =  array(
				'kelas_siswa' => $this->input->post('kelas')[$i],
			);
			//init to check duplicate data
			// $check_vals.="AND nama_lengkap_siswa"."='".$this->input->post('nama_lengkap')[$i]."'";
			// $check_vals.=" OR ref_siswa"."='".$this->input->post('ref')[$i]."'";

			$param = new QueryParameter();
			$param->setClause($param->getClause() . " AND nama_lengkap_siswa" . "='".$this->input->post('nama_lengkap')[$i]."'");
			$param->setClause($param->getClause() . " OR ref_siswa" . "='".$this->input->post('ref')[$i]."'");

			//substr($check_vals, 4);
		    $lst=$this->Siswa->get_list_table($param);

		    if(count($lst)>0 && !isset($id_siswa)){
		    	$check_duplicate="";
		    	for ($i=0; $i < count($lst); $i++) { 
		    		$check_duplicate.=",".$lst[$i]->nama_lengkap_siswa;
				}

				$resp['code']=http_response_code(409);
		    	$resp['message'] = "Terjadi DUPLIKASI pada data: ".substr($check_duplicate, 1);

		    	return $this->output
		        ->set_content_type('application/json')
		        ->set_status_header(409)
		        ->set_output(json_encode($resp));
		    }

			if($this->input->post('nama_lengkap')[$i] == "") {
				$pass=false;
			}
			//
			$data_kelas[$i] =  array(
				'header_id_siswa_kelas' => $data_siswa[$i]['id_siswa'],
				'kelas_siswa_kelas' => $this->input->post('kelas')[$i],
				'tahun_ajaran' => $this->input->post('tahun_ajaran')[$i],
				'status_siswa_kelas' => 1
			);
			//
		}
		if(!$pass) {
			$resp['code']=http_response_code(400);
    		$resp['message'] = "Input belum lengkap, harap dilengkapi terlebih dahulu.";
    		return $this->output
		        ->set_content_type('application/json')
		        ->set_status_header(400)
		        ->set_output(json_encode($resp));
		}

		//looping data untuk iuran/detil
		for ($i=0; $i < count($this->input->post('jenis_iuran')); $i++) {
			$id=$this->input->post('header_siswa')[$i];
			$data_detil[$i] =  array(
				'header_id_siswa_detil' => $data_siswa[$id]['id_siswa'],
				'tingkat_siswa_detil' => $this->input->post('kelas_siswa')[$i],
				'jenis_iuran_siswa_detil' => $this->input->post('jenis_iuran')[$i],
				'besar_iuran_siswa_detil' => $this->input->post('besar_iuran')[$i],
			);
		}
        $data = array(
        	'id' => isset($id_siswa),
        	'siswa' => $data_siswa,
        	'kelas' => $data_kelas,
        	'detil' => $data_detil//iuran
        );
  //       if(isset($id_siswa)){
        	//jika naik kelas maka status kelas sebelumnya update jadi 0
        $param = new QueryParameter();
		$param->setClause($param->getClause() . " AND header_id_siswa_kelas" . "='".$id_siswa."'");
		$param = $param->getClause();
    	$data_siswa_kelas = $this->SiswaKelas->get_list_table($param);

    	foreach ($data_siswa_kelas as $key => $value) {
    		$value->status_siswa_kelas=0;
    		// print_r($value);
    		$data_kelas[$key] =  array(
				'header_id_siswa_kelas' => $value->header_id_siswa_kelas,
				'status_siswa_kelas' => $value->status_siswa_kelas
			);
    	}
    	$this->SiswaKelas->updateAll($data_kelas);

		$this->Siswa->save_siswa_kelas_iuran($data);

		// }else{
		// 	$this->Siswa->save_siswa_kelas_iuran($data);
		// }
		// $this->Siswa->save_siswa_kelas_iuran($data);
        // $this->Siswa->save_master($arr);
    	$resp['message'] = "Data telah berhasil disimpan.";
        $resp['data'] = $data;    

		return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(200)
	        ->set_output(json_encode($resp));
	}

	public function save() {
		
		$data = array(
			'id_siswa' => "",
            'nis_siswa'=> "",
            'ref_siswa'=> "",
            'nama_lengkap_siswa'=> "",
            'kelas_siswa'=>  "",
            'tahun_ajaran_siswa'=>  "",
            'jk_siswa'=> "",
            'alamat_lengkap_siswa'=> "",
            'file_siswa'=> ""
        );
        $resp = array(
				"code" => "code",
				"message" => "message",
                "data" => [],
        );
		$id = $this->input->post('id');

		if(isset($id)){
			$data['id_siswa'] = $id;
		}else{
			$data['id_siswa'] = $this->Siswa->newId();
		}
		$data['nis_siswa'] = $this->input->post('nis');
		$data['ref_siswa'] = $this->input->post('ref');
		$data['nama_lengkap_siswa'] = $this->input->post('nama_lengkap');
		$data['kelas_siswa'] = $this->input->post('kelas_siswa');
		$data['tahun_ajaran_siswa'] = $this->input->post('tahun_ajaran');
		$data['jk_siswa'] = $this->input->post('jk');
		$data['alamat_lengkap_siswa'] = $this->input->post('alamat_lengkap');
		#$data['file_siswa'] = $this->input->post('file');

		$resp['data'] = $data;	
        if(isset($id)){
			$this->Siswa->update($data);
			$resp['message'] = "Data berhasil diupdate.";
		}else{
			$this->Siswa->save($data);
			$data_arr = json_decode(json_encode($_POST["list_iuran"],FALSE));
			$data_arr=json_decode($data_arr); 

			//print_r($data_arr);
			$cart = array();
			foreach ($data_arr as $key => $value) {
				$cart[$key] =  array(
					'header_id_siswa_detil' => $data['id_siswa'],
					'besar_iuran_siswa_detil' => $value->besar_jenis_iuran,
					'jenis_iuran_siswa_detil' => $value->nama_jenis_iuran
				);
			}
			$this->Siswa->save_detil($cart);
			$resp['message']="Data berhasil disimpan.";
		}
        echo json_encode($resp);
	}

	public function save_siswa_kelas(){
	    $resp = array(
				"code" => http_response_code(200),
				"message" => "success",
	            "data" => [],
	    );
		$data_arr = json_decode(json_encode($_POST["list_siswa_kelas"],FALSE));
		$data_arr=json_decode($data_arr); 

		// print_r($data_arr);
		// $arr = array();
		// foreach ($data_arr as $key => $value) {
		// 	$arr[$key] =  array(
		// 		'header_id_siswa_detil' => $data['id_siswa'],
		// 		'besar_iuran_siswa_detil' => $value->besar_jenis_iuran,
		// 		'jenis_iuran_siswa_detil' => $value->nama_jenis_iuran
		// 	);
		// }
		$this->SiswaKelas->save_kelas($data_arr);
		$resp['message']="Data berhasil disimpan.";
		$resp['data'] = $data_arr;
        return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(200)
	        ->set_output(json_encode($resp));
	}
}

class QueryParameter {
	var $locale = "en_US";
	var $clause = "1";
	var $innerClause = "1";
	var $values = "";
	var $order = "";
	var $group = "";
	var $limit = 100;
	var $offset = 0;

	function getClause(){
		return $this->clause;
	}

	function setClause($val){
		$this->clause = $val;
	}

	function getOrder(){
		return $clause;
	}

	function setOrder($val){
		$this->clause = $val;
	}

	function getLimit(){
		if($this->limit > 200) return 200;
		return $limit;
	}

	function setLimit($val){
		$this->limit = $val;
	}
	//start offset

	//end offset

	function getValues(){
		return $values;
	}

	function setValues($val){
		$this->values = $val;
	}
	//start group

	//end group

	//start innerClause

	//end innerClause

	//start localse

	//end localse
}