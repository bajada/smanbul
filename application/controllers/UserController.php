<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class UserController extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('User');
	}

	public function index()
	{	
		// $this->load->view('user_view');
	}

	public function get_by_id($id) {
		$data = $this->User->get_by_id($id);
		$action=$this->input->get('action');
        $resp = array(
        				"code" => "code",
        				"message" => "message",
                        "data" => $data,
                );

		if(isset($action)) {
			if($action=='delete') {
				$resp['message'] = "Apakah anda yakin akan menghapus data '".$data->username_user."'?";
                
			}
		}
		echo json_encode($resp);
	}
	public function do_action($id, $action) {
        $resp = array(
        				"code" => "code",
        				"message" => "message",
                        "data" => [],
                );

		if(isset($action)) {
			if($action=='delete') {
				$data = $this->User->get_by_id($id); //getEntity
				$resp['data'] = $data;
				$this->User->delete($id);
				
				$resp['message'] = "Data '".$data->username_user."' berhasil dihapus.";
                
			}
		}
		echo json_encode($resp);
	}
	//

	public function list_table() {
		$resp = array(
				"code" => http_response_code(200),
				"message" => "http_response_code(200)",
                "data" => [],
        );
    	$param = new QueryParameter();
		$filterKeyword = $this->input->get('filter_keyword');
		$filterRole = $this->input->get('filter_role');
		if(isset($filterKeyword) && $filterKeyword != '') {
			$param->setClause($param->getClause() . " AND username_user" . " LIKE'%".$filterKeyword."%'");
			$param->setClause($param->getClause() . " OR firstname_user" . " LIKE'%".$filterKeyword."%'");
			$param->setClause($param->getClause() . " OR lastname_user" . " LIKE'%".$filterKeyword."%'");
		}
		if(isset($filterRole) && $filterRole != '0') {
			$param->setClause($param->getClause() . " AND role_user" . "='".$filterRole."'");
		}
		$param = $param->getClause();
		// print_r($param);
		// die();
		$data = $this->User->get_list_table2($param);

		$resp['data'] = $data;	
		return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($resp));
	}

	public function save() {
		$resp = array(
				"code" => http_response_code(200),
				"message" => "http_response_code(200)",
                "data" => [],
        );
		
		$data = array(
			'id_user' => "",
            'username_user'=> "",
            'password_user'=> "",
            'role_user'=>  "",
            'firstname_user'=>  "",
            'lastname_user'=>  "",
            'status_user'=> 1
        );
		$id = $this->input->post('id');

		if(isset($id)){
			$data['id_user'] = $id;
			$data = $this->User->get_by_id($id);
			$data = json_decode(json_encode($data), TRUE);
		}

		$data['username_user'] = $this->input->post('username_user');
		//set password hanya dilakukan apabila bukan dalam mode edit
		if(!isset($id)) $data['password_user'] = md5($this->input->post('password_user'));
		$data['role_user'] = $this->input->post('role_user');
		$data['firstname_user'] = $this->input->post('firstname_user');
		$data['lastname_user'] = $this->input->post('lastname_user');


		// print_r($data);
		// die();

		$resp['data'] = $data;

		$pass = true;
		if($data['username_user'] == null || $data['firstname_user'] == null || $data['lastname_user'] == null || $data['role_user'] == null){
			$pass = false;
		}else{
			if($data['username_user'] == "" || $data['firstname_user'] == "" || $data['lastname_user'] == "" || $data['role_user'] == ""){
				$pass = false;
			}
		}

		if(!$pass){
			$resp['code'] = http_response_code(500);
			$resp['message'] = "Input belum lengkap, harus dilengkapi terlebih dahulu.";
			return $this->output
            ->set_content_type('application/json')
            ->set_status_header(500)
            ->set_output(json_encode($resp));
		}

        if(isset($id)){
			$this->User->update($data);
			$resp['message'] = "Data berhasil diupdate.";
		}else{
			$this->User->save($data);
			$resp['message'] = "Data berhasil disimpan.";
		}
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($resp));
	}
}





class QueryParameter {
	var $locale = "en_US";
	var $clause = "1";
	var $innerClause = "1";
	var $values = "";
	var $order = "";
	var $group = "";
	var $limit = 100;
	var $offset = 0;

	function getClause(){
		return $this->clause;
	}

	function setClause($val){
		$this->clause = $val;
	}

	function getOrder(){
		return $clause;
	}

	function setOrder($val){
		$this->clause = $val;
	}

	function getLimit(){
		if($this->limit > 200) return 200;
		return $limit;
	}

	function setLimit($val){
		$this->limit = $val;
	}
	//start offset

	//end offset

	function getValues(){
		return $values;
	}

	function setValues($val){
		$this->values = $val;
	}
	//start group

	//end group

	//start innerClause

	//end innerClause

	//start localse

	//end localse
}