<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class IuranSiswaController extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('Siswa');
        $this->load->model('SiswaDetil');
        $this->load->model('SiswaKelas');
        $this->load->model('Iuran');
        $this->load->model('KwitansiIuran');
	}

	public function index()
	{	
		$this->load->view('iuran_view');
	}

	public function check_tunggakan($id){

		// $data = $this->Siswa->get_by_id($id);
		// $action=$this->input->get('action');
		
		// if(isset($action)) {
		// 	if($action=='delete') {
		// 		$resp['message'] = "Apakah anda yakin akan menghapus data '".$data->id_iuran_dpmp_siswa."'?";
                
		// 	}
		// }
		$check_tunggakan=$this->input->get('check_tunggakan');
    	
    	$list_data = [];
    	$data_siswa = [];
		$resp = array(
			"code" => "code",
			"message" => "success",
            "data" => [],
        );
		//untuk check data siswa saja
		$data_siswa = $this->Siswa->get_by_id($id);

    	if($data_siswa!=null){
	        $no_ref=$data_siswa->ref_siswa;
			$tahun_ajaran=$this->input->get('tahun_ajaran');

			$param="1";
			$param = $param." AND "."header_id_siswa_kelas"." = "."'".$data_siswa->id_siswa."'";

			// if(!isset($check_tunggakan) && !$check_tunggakan){
				// if(isset($tahun_ajaran) && $tahun_ajaran != ''){
				// 	$param.=" AND "."tahun_ajaran"." = "."'".$tahun_ajaran."'";
				// }else{

				// 	$param.=" AND "."status_siswa_kelas"." = "."'0'";
				// }
				// if(isset($tahun_ajaran) && $tahun_ajaran != ''){
				// 	$param.=" AND "."tahun_ajaran"." = "."'".$tahun_ajaran."'";
				// }else{
				// 	if(isset($check_tunggakan) && $check_tunggakan==true){
				// 		$param.=" AND "."status_siswa_kelas"." = "."'0'";
				// 	}else{
				// 		$param.=" AND "."status_siswa_kelas"." = "."'1'";
				// 	}
				// }
			// }

    		$data_siswa_kelas = $this->SiswaKelas->get_list_table($param);
    		// print_r($param);
    		// print_r($data_siswa_kelas);
    		// die();

			// $param="1";
			// $param = $param." AND "."id_siswa_iuran"." = "."'".$data_siswa->id_siswa."'";
			$param = new QueryParameter();
    		$param->setClause($param->getClause() . " AND id_siswa_iuran" . "='".$data_siswa->id_siswa."'");
    		$data_iuran = $this->Iuran->get_list_table($param);

    		$param = [];
    		// $klas = $data_siswa_kelas[0]->kelas;
    		$param= array(
				'header_id_siswa_detil' => $data_siswa->id_siswa,
				// 'tingkat_siswa_detil' => explode(" ", $klas)[0],
			);
    		$data_siswa_detil = $this->SiswaDetil->get_list_table($param);

			$date = new DateTime();
			$angkatan = new DateTime();
			$angkatan->setDate($data_siswa->tahun_angkatan_siswa, 7, 10);
			

			$selisih = $angkatan->format("Y")+$date->format("Y")-$angkatan->format("Y");
			$month = $date->format("m");
            if($month>=6) $date=$date->format("Y")."/".($date->format("Y")+1);
            else $date=($date->format("Y")-1)."/".$date->format("Y");
			//
			// print_r($angkatan);
			$get_year=[];
			$get_years=[];
			for($tahun_ajaran_i=$angkatan->format("Y"); $tahun_ajaran_i < $selisih; $tahun_ajaran_i++){
                // if(count($data_siswa_kelas)>1){
                // if($tahun_ajaran_i."/".($tahun_ajaran_i+1)!=$date){
                    $get_year[]=$this->get_tahun_ajaran($tahun_ajaran_i."/".($tahun_ajaran_i+1));
                    $get_years[]=$tahun_ajaran_i."/".($tahun_ajaran_i+1);
                // }
                // }else{
                // 	$get_year[0]=$this->get_tahun_ajaran($tahun_ajaran_i."/".($tahun_ajaran_i+1));
                //     $get_years[0]=$tahun_ajaran_i."/".($tahun_ajaran_i+1);
                // }

            }
    //         if(isset($tahun_ajaran) && $tahun_ajaran != ''){
    //         	$tahun=$tahun_ajaran;
    //         	$get_year=[];
    //         	$get_year[]=$this->get_tahun_ajaran($tahun_ajaran);
    //         }else{
    //         	if(isset($check_tunggakan) && $check_tunggakan==true){
				// 	//$param.=" AND "."status_siswa_kelas"." = "."'0'";
				// 	$tahun=$data_siswa_kelas[0]->tahun_ajaran;
				// }else{
				// 	$tahun=$data_siswa_kelas[0]->tahun_ajaran;
				// 	$get_year=[];
    //         		$get_year[]=$this->get_tahun_ajaran($tahun);
				// }
    //         }

            // print_r($date==$tahun);
            // print_r($data_siswa_kelas);
            //
            $total_spp=0;
            $total_spp1=[];
            $total_du=0;
            $total_du1=[];
            $total_dpmp=0;
            $tingkat_kelas=[];
            // print_r($data_siswa_detil);
           foreach ($data_siswa_detil as $key_siswa_detil => $value_siswa_detil) {

           		foreach ($data_siswa_kelas as $key_siswa_kelas => $value_siswa_kelas) {
            	 	// print_r($value_siswa_detil);
	        		if($value_siswa_detil->tingkat_siswa_detil == $value_siswa_kelas->tingkat_kelas){
		            	if($value_siswa_detil->jenis_iuran_siswa_detil=="IURAN_PER_BULAN") $total_spp1[$key_siswa_kelas]=($value_siswa_detil->besar_iuran_siswa_detil);
		                if($value_siswa_detil->jenis_iuran_siswa_detil=="DU") $total_du1[$key_siswa_kelas]=$value_siswa_detil->besar_iuran_siswa_detil;
		                if($value_siswa_detil->jenis_iuran_siswa_detil=="DPMP") $total_dpmp=$value_siswa_detil->besar_iuran_siswa_detil;
		                $tingkat_kelas[]=$value_siswa_detil->tingkat_siswa_detil;
	                }
	            }
            }
            //
            $sum_spp=0;
            $sum_du=0;
            $sum_dpmp=0;
            $tunggakan=[];
            // $index=0;
            $master=[];
            foreach ($get_year as $key => $all_date) {
            	$sum_spp1=[];
            	$sum_spp=0;
            	$sum_du=0;
            	$sum_du1=[];
            	$sum_dpmp1=[];
            	$tunggakan[$key] = array(
	            	'tahun_ajaran' => '', 
	            	'spp' => 0, 
	            	// 'dpmp' => 0, 
	            	'du' => 0, 
	            );
	            $tunggakan[$key]['tahun_ajaran']= $get_years[$key];
            	foreach ($all_date as $key_2 => $month) {

	                foreach ($data_iuran as $keys => $value) {
	                	if($month==$value->bulan_iuran){
	                        if($value->jenis_iuran=='IURAN_PER_BULAN'){
	                            $sum_spp+=$value->besar_iuran;
	                            $sum_spp1[]=$value->besar_iuran;
	                        }
	                        // if($value->jenis_iuran=='DU'){
	                        //     $sum_du+=$value->besar_iuran;
	                        //     $sum_du1[]=$value->besar_iuran;
	                        // }
	                        // if($value->jenis_iuran=='DPMP'){
	                        //     $sum_dpmp += intval($value->besar_iuran);
	                        //     $sum_dpmp1[]= intval($value->besar_iuran);
	                        // }
	                    }
	                }
            	}
            	// print_r($data_siswa_kelas);
            	foreach ($data_iuran as $keys => $value) {

           			foreach ($data_siswa_kelas as $key_siswa_kelas => $value_siswa_kelas) {
	             //        if($value->status_iuran=="LUNAS" && $value_siswa_detil->jenis_iuran_siswa_detil=="DU")
	                    if($value_siswa_kelas->status_siswa_kelas==0)
	                    if($value->jenis_iuran=='DU' && $value->id_siswa_kelas==$value_siswa_kelas->id_siswa_kelas){
	                        $sum_du+=$value->besar_iuran;
	                        $sum_du1[]=$value->besar_iuran;
	                    }
	                }

                    if($value->jenis_iuran=='DPMP'){
                        $sum_dpmp += intval($value->besar_iuran);
                        $sum_dpmp1[]= intval($value->besar_iuran);
                    }
                }
                $tunggakan[$key]['spp']= $total_spp-$sum_spp;
                $tunggakan[$key]['du'] = $total_du-$sum_du;
                // $tunggakan[$key]['dpmp'] = $total_dpmp-$sum_dpmp;
                // print_r($sum_spp);
                if(intval($total_spp)-intval($sum_spp) == 0 && intval($total_du)-intval($sum_du) == 0){
                	$tunggakan=[];
                }

                if(count($data_siswa_kelas) > 1){
	                $master[] = array(
	                		'besar_iuran' => array(
	                			'spp' => $total_spp1[$key]*12,
	                			'du' => $total_du1[$key],
	                			'dpmp' => $total_dpmp,
	                		),
	                		'siswa_detil' => $data_siswa_kelas[$key],
	            			// 'tahun' => $get_years[$key],
	                		'siswa_detil_iuran' => array (
			                	'iuran_bulanan' => $sum_spp1, 
			                	'iuran_bulanan_count' => $sum_spp, 
			                	'du' =>  $sum_du1, 
			                	'du_count' =>  $sum_du, 
			                	'dpmp' =>  $sum_dpmp1
		                	),
	                );
	            }else{
	            	$master=[];
	            }
            }
            $tunggakan_new=[];
            foreach ($master as $key_key => $value) {
            	$tunggakan_new[]=array(
	            	'tahun_ajaran' => $value['siswa_detil']->tahun_ajaran_siswa_kelas,
	            	'sisa_spp' => $value['besar_iuran']['spp'] - $value['siswa_detil_iuran']['iuran_bulanan_count'],
	            	'sisa_du' => $value['besar_iuran']['du'] - $value['siswa_detil_iuran']['du_count'],
	            	'spp' => $value['besar_iuran']['spp'],
	            	'du' => $value['besar_iuran']['du'],
	            	'du_has_pay' => $value['siswa_detil_iuran']['du_count']
	            );
            }
            $new_method = array(
        		'besar' => array (
                	'iuran_bulanan' => $total_spp1, 
                	'du' =>  $total_du1, 
                	'dpmp' =>  $total_dpmp
            	),
        		'master' => $master,
        		'tunggakan' => $tunggakan_new
            );

            foreach ($get_years as $key => $value_tahun) {
            	if($value_tahun==$date){
            		$list_data["data_tunggakan"] = 'LUNAS';
            	}
            }

            foreach ($tunggakan_new as $key_tunggak => $value_tunggakan_new) {
            	// print_r($date!=$value_tunggakan_new['tahun_ajaran']);
            		
            		if($date!=$value_tunggakan_new['tahun_ajaran']){
            			if ($value_tunggakan_new['sisa_du']==0 && $value_tunggakan_new['sisa_spp']==0) {
            				$list_data["data_tunggakan"] = 'LUNAS';
            			}else{
            				$list_data["data_tunggakan"] = 'BELUM LUNAS';
            			}

            			// if($date==$tahun){
            			// 	$list_data["data_tunggakan"] = 'DALAM PROSES';
            			// }else
            		// }else{
            		// 	$list_data["data_tunggakan"] = 'DALAM PROSES';
            		}
            		// else{
            			// $list_data["data_tunggakan"] = 'DALAM PROSES';
            			// if ($value_tunggakan_new['sisa_du']==0 && $value_tunggakan_new['sisa_spp']==0) {
            			// 	$list_data["data_tunggakan"] = 'LUNAS';
            			// }else{
            			// 	$list_data["data_tunggakan"] = 'BELUM LUNAS';
            			// }
            		// }
            		// $list_data["data_tunggakan"] = 'BELUM LUNAS';
            		// else 
            		// else $list_data["data_tunggakan"] = 'DALAM PROSES';

            	
            	// if($date==$tahun && $value_tunggakan_new['sisa_du']!=0 && $value_tunggakan_new['sisa_spp']==0){
            	// 	$list_data["data_tunggakan"] = 'DALAM PROSES';
            	// }
            }
            // print_r($new_method);
			// $my_function=[];
			// $now=2019;
			// for ($i=$now; $i <= $now+1; $i++) { 
			// 	$my_function[]=$i;
			// }
			// $angkatan = array(
			// 	'angkatan' => $data_siswa->tahun_angkatan_siswa, 
			// 	'mulai_tahun ajaran' => $data_siswa->tahun_angkatan_siswa."/".($data_siswa->tahun_angkatan_siswa+1), 
			// 	'sekarang_tahun ajaran' => (date("Y")-1)."/".date("Y"),
			// 	'selesai_tahun ajaran' => ($data_siswa->tahun_angkatan_siswa+3)."/".($data_siswa->tahun_angkatan_siswa+4),
			// 	'my_function' => $get_year, 
			// 	'selisih' => $tunggakan, 
			// );

    		//
			// $param = new QueryParameter();
			// if(isset($no_ref)) $param->setClause($param->getClause() . " AND ref_siswa" . "='".$no_ref."'");
			// if(isset($tahun_ajaran) && $tahun_ajaran != '') {
			// 	$param->setClause($param->getClause() . " AND tahun_ajaran" . "='".$tahun_ajaran."'");
			// }else{
			// 	$param->setClause($param->getClause() . " AND status_siswa_kelas" . "='1'");
			// }
			// $param = $param->getClause();
   //  		$data_siswa = $this->Siswa->get_list_table2($param);
    		//
			$list_data["data_siswa"] = $data_siswa;
			$list_data["data_siswa_detil"] = $data_siswa_detil;
			$list_data["data_siswa_kelas"] = $data_siswa_kelas[0];
			$list_data["data_iuran"] = $data_iuran;
			//
			// if(sizeof($tunggakan)!=0)
			// $list_data["data_tunggakan"] = $tunggakan;
			//
			$list_data['new_method'] = $new_method;
    	}
		$resp['data'] = $list_data;	

		return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(200)
	        ->set_output(json_encode($resp));
	}

	public function get_by_id($id) {
    	$list_data = [];
    	$data_siswa = [];
		$resp = array(
			"code" => http_response_code(200),
			"message" => "success",
            "data" => [],
        );
		//untuk check data siswa saja
		$data_siswa = $this->SiswaKelas->get_by_id($id);
		$data_siswa = $this->Siswa->get_by_id($id);

    	if($data_siswa!=null){

    		$tahun_ajaran=$this->input->get('tahun_ajaran');
			$param = new QueryParameter();
			// if(isset($no_ref)) $param->setClause($param->getClause() . " AND ref_siswa" . "='".$no_ref."'");
			$param->setClause($param->getClause() . " AND header_id_siswa_kelas" . "='".$data_siswa->id_siswa."'");

			$status_siswa_kelas=0;
			if(isset($tahun_ajaran) && $tahun_ajaran != ''){
				$param->setClause($param->getClause() . " AND tahun_ajaran_siswa_kelas" . "='".$tahun_ajaran."'");
				// $param.=" AND "."tahun_ajaran"." = "."'".$tahun_ajaran."'";
			}else{
				$param->setClause($param->getClause() . " AND status_siswa_kelas" . "='1'");
				// $param->setClause($param->getClause() . " AND status_siswa_kelas" . "='1'");
				$status_siswa_kelas=1;
			}

    		$param = $param->getClause();
    		$data_siswa_kelas = $this->SiswaKelas->get_list_table($param);

    		$param = new QueryParameter();
    		$param->setClause($param->getClause() . " AND id_siswa_iuran" . "='".$data_siswa->id_siswa."'");
			
			if(isset($_GET['filter_jenis_iuran'])){
				$param->setClause($param->getClause() . " AND jenis_iuran" . "='".$_GET['filter_jenis_iuran']."'");
			}
			if(isset($_GET['filter_jenis_iuran'])){
				$param->setClause($param->getClause() . " AND jenis_iuran" . "='".$_GET['filter_jenis_iuran']."'");
			}
			if(isset($_GET['filter_bulan_iuran'])){
				$param->setClause($param->getClause() . " AND bulan_iuran" . "='".$_GET['filter_bulan_iuran']."'");
			}

			$param->setOrder("tanggal_iuran");
			//if(isset($_GET['filter_bulan_iuran'])) print_r($param);
			// $param_new = array(
			// 	'clause' => $param->getClause(), 
			// 	'order' => "ORDER BY ".$param->getOrder()
			// );
    		$data_iuran = $this->Iuran->get_list_table($param);

    		$param = [];
    		// $klas = $data_siswa_kelas[0]->kelas;
    		$param= array(
				'header_id_siswa_detil' => $data_siswa->id_siswa,
				// 'tingkat_siswa_detil' => explode(" ", $klas)[0],
			);
    		$data_siswa_detil = $this->SiswaDetil->get_list_table($param);

			$date = new DateTime();

			if(count($data_siswa_kelas)==1){
				$tahun_p=$data_siswa_kelas[0]->tahun_ajaran_siswa_kelas;
				$tahun_p=explode("/", $tahun_p);
				$date->setDate($tahun_p[1], 7, 10);
			}
			
			$angkatan = new DateTime();
			if(count($data_siswa_kelas)==1){
				$tahun_p=$data_siswa_kelas[0]->tahun_ajaran_siswa_kelas;
				$tahun_p=explode("/", $tahun_p);
				$angkatan->setDate($tahun_p[0], 7, 10);
			}else{
				$angkatan->setDate($data_siswa->tahun_angkatan_siswa, 7, 10);
			}

			// print_r($date);
			$selisih = $angkatan->format("Y")+$date->format("Y")-$angkatan->format("Y");
			$month = $date->format("m");
            if($month>=6) $date=$date->format("Y")."/".($date->format("Y")+1);
            else $date=($date->format("Y")-1)."/".$date->format("Y");
			//
			$get_year=[];
			$get_years=[];
			for($tahun_ajaran_i=$angkatan->format("Y"); $tahun_ajaran_i < $selisih; $tahun_ajaran_i++){
                // if($tahun_ajaran_i."/".($tahun_ajaran_i+1)!=$date){
                    $get_year[]=$this->get_tahun_ajaran($tahun_ajaran_i."/".($tahun_ajaran_i+1));
                    $get_years[]=$tahun_ajaran_i."/".($tahun_ajaran_i+1);
                // }
                if($tahun_ajaran_i."/".($tahun_ajaran_i+1)!=$date){
                    $get_year_now[]=$this->get_tahun_ajaran($tahun_ajaran_i."/".($tahun_ajaran_i+1));
                    $get_years_now[]=$tahun_ajaran_i."/".($tahun_ajaran_i+1);
                }
            }

            $total_spp=0;
            $total_spp1=[];
            $total_du=0;
            $total_du1=[];
            $total_dpmp=0;
            $tingkat_kelas=[];
            	// print_r($value_siswa_kelas);
            foreach ($data_siswa_detil as $key_siswa_detil => $value_siswa_detil) {
           		foreach ($data_siswa_kelas as $key_siswa_kelas => $value_siswa_kelas) {
           			// $data_siswa_kelas[$key_siswa_kelas]->kelas_sekarang=$this->SiswaKelas->get_active();
            	 	// print_r($value_siswa_detil);
	        		if($value_siswa_detil->tingkat_siswa_detil == $value_siswa_kelas->tingkat_kelas){
		            	if($value_siswa_detil->jenis_iuran_siswa_detil=="IURAN_PER_BULAN") $total_spp1[$key_siswa_kelas]=($value_siswa_detil->besar_iuran_siswa_detil);
		                if($value_siswa_detil->jenis_iuran_siswa_detil=="DU") $total_du1[$key_siswa_kelas]=$value_siswa_detil->besar_iuran_siswa_detil;
		                if($value_siswa_detil->jenis_iuran_siswa_detil=="DPMP") $total_dpmp=$value_siswa_detil->besar_iuran_siswa_detil;
		                $tingkat_kelas[]=$value_siswa_detil->tingkat_siswa_detil;
	                }
	            }
            }
           
            // print_r($get_years_now);
            $sum_spp=0;
            $sum_du=0;
            $sum_dpmp=0;
            $tunggakan=[];
            
            foreach ($get_year_now as $key => $all_date) {
            	$sum_spp1=[];
            	$sum_spp=0;
            	$sum_du=0;
            	$sum_du1=[];
            	$sum_dpmp1=[];
            	$tunggakan[$key] = array(
	            	'tahun_ajaran' => '', 
	            	'spp' => 0, 
	            	// 'dpmp' => 0, 
	            	'du' => 0, 
	            );
	            $tunggakan[$key]['tahun_ajaran']= $get_years[$key];
            	foreach ($all_date as $key_2 => $month) {

	                foreach ($data_iuran as $keys => $value) {
	                	if($month==$value->bulan_iuran){
	                        if($value->jenis_iuran=='IURAN_PER_BULAN'){
	                            $sum_spp+=$value->besar_iuran;
	                            $sum_spp1[]=$value->besar_iuran;
	                        }
	                        if($value->jenis_iuran=='DU'){
	                            $sum_du+=$value->besar_iuran;
	                            $sum_du1[]=$value->besar_iuran;
	                        }
	                        if($value->jenis_iuran=='DPMP'){
	                            $sum_dpmp += intval($value->besar_iuran);
	                            $sum_dpmp1[]= intval($value->besar_iuran);
	                        }
	                    }
	                }
            	}
                $tunggakan[$key]['spp']= $total_spp-$sum_spp;
                $tunggakan[$key]['du'] = $total_du-$sum_du;
                // $tunggakan[$key]['dpmp'] = $total_dpmp-$sum_dpmp;
                // print_r($sum_spp);
                if(intval($total_spp)-intval($sum_spp) == 0 && intval($total_du)-intval($sum_du) == 0){
                	$tunggakan=[];
                }

                // print_r(sizeof($total_spp1));
                if(sizeof($total_spp1) == 0){
	    			$list_data["data_tunggakan"] = 'DATA SISWA BARU BELUM ADA';
	    			$master=[];
	    		}else{
	    		
	                $master[$key] = array(
	                		'besar_iuran' => array(
	                			'spp' => $total_spp1[$key],
	                			'du' => $total_du1[$key],
	                			'dpmp' => $total_dpmp,
	                		),
	                		'siswa_detil' => $data_siswa_kelas[$key],
	                		'siswa_detil_iuran' => array (
			                	'iuran_bulanan' => $sum_spp1, 
			                	'iuran_bulanan_count' => $sum_spp, 
			                	'du' =>  $sum_du1, 
			                	'du_count' =>  $sum_du, 
			                	'dpmp' =>  $sum_dpmp1
		                	),
	                );
                }
            }
            $tunggakan_new=[];
            foreach ($master as $key => $value) {
            	// print_r($value);
            	$spp = $value['besar_iuran']['spp']*12;
            	$tunggakan_new[$key]=array(
	            	'tahun_ajaran' => $value['siswa_detil']->tahun_ajaran_siswa_kelas,
	            	'sisa_spp' => $spp - $value['siswa_detil_iuran']['iuran_bulanan_count'],
	            	'sisa_du' => $value['besar_iuran']['du'] - $value['siswa_detil_iuran']['du_count'],
	            	'spp' => $spp,
	            	'du' => $value['besar_iuran']['du'],
	            	'du_has_pay' => $value['siswa_detil_iuran']['du_count']
	            );
            }
            $new_method = array(
        		'besar' => array (
                	'iuran_bulanan' => $total_spp1, 
                	'du' =>  $total_du1, 
                	'dpmp' =>  $total_dpmp
            	),
        		'master' => $master,
        		'tunggakan' => $tunggakan_new
            );
            foreach ($get_years_now as $key => $value_year) {
            	foreach ($tunggakan_new as $key => $value_tunggakan_new) {
	            		$data_tunggakan = $this->check_tunggakan($id)->final_output;
			    		$data_tunggakan = json_decode($data_tunggakan);
			    		if(isset($data_tunggakan->data->data_tunggakan)){
			    			if($data_tunggakan->data->data_tunggakan=="BELUM LUNAS"){
				    			$list_data["data_tunggakan"] = 'BELUM LUNAS';
				    		}else{
				    			$list_data["data_tunggakan"] = 'LUNAS';
				    		}
			    		}
	        		
	            }
            }
			$list_data["data_siswa"] = $data_siswa;
			$list_data["data_siswa_detil"] = $data_siswa_detil;
			$list_data["data_siswa_kelas"] = $data_siswa_kelas;
			$list_data["data_iuran"] = $data_iuran;

			$list_data['new_method'] = $new_method;
    	}
		$resp['data'] = $list_data;	

		return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(200)
	        ->set_output(json_encode($resp));
	}

	public function get_tahun_ajaran($string_date){
        $tahun_ajaran = explode("/", $string_date);
        $new_month=[];
        $start = $month = strtotime($tahun_ajaran[0].'-07-10');
		$end = strtotime($tahun_ajaran[1].'-06-10');
		while($month <= $end)
		{
		     // echo date('F Y', $month), PHP_EOL;
		     $new_month[]=date('M Y', $month);
		     $month = strtotime("+1 month", $month);
		}
        return $new_month;
    }

	public function list_table() {
		$param = new QueryParameter();
		$resp = array(
			"code" => http_response_code(200),
			"message" => "success",
	        "data" => [],
        );

		$filterKeyword = $this->input->get('filter_keyword');
		$data_siswa=null;
		if(isset($filterKeyword) && $filterKeyword != '') {
			$param->setClause($param->getClause() . " AND ref_siswa" . "='".$this->input->get('filter_keyword')."'");
			$param = $param->getClause();
			$data_siswa=$this->Siswa->get_list_table2($param);
    	}

    	$list_data = [];
    	if($data_siswa!=null){
    		
    		$param = new QueryParameter();
   //  		$param= array(
			// 	'id_siswa_iuran' => $data_siswa[0]->id_siswa
			// );
	    		//
	   //  		$kelas=$this->input->get('kelas_siswa');//
		  //       $tahun_ajaran=$this->input->get('tahun_ajaran_siswa');
		  //       $tingkat_kelas=$this->input->get('tingkat_kelas');

				// if(isset($tahun_ajaran)) $param.=" AND "."tahun_ajaran"." = "."'".$tahun_ajaran."'";
				// if($tingkat_kelas!='ALL'){
					
				// 	if(isset($tingkat_kelas)) $param.=" AND "."tingkat_kelas"." = "."'".$tingkat_kelas."'";
				// }
				// $data = $this->Siswa->get_list_table2($param);
	    		//
			// $param="1";
			// $param = $param." AND "."id_siswa_iuran"." = "."'".$data_siswa[0]->id_siswa."'";
			// $param = $param." AND "."tanggal_iuran"." LIKE "."'%".$this->input->get("tanggal_transaksi")."%'";
    		$param->setClause($param->getClause() . " AND id_siswa_iuran" . "='".$data_siswa[0]->id_siswa."'");
    		// $param->setClause($param->getClause() . " AND id_siswa_iuran" . "='".$data_siswa[0]->id_siswa."'");
    		// $param = $param->getClause();
    		$data_iuran = $this->Iuran->get_list_table($param);
			$param = [];
    		$param=array(
				'header_id_siswa_detil' => $data_siswa[0]->id_siswa,
				'tingkat_siswa_detil' => $data_siswa[0]->tingkat_kelas,
			);
    		$data_siswa_detil = $this->SiswaDetil->get_list_table($param);

			$list_data["data_siswa"] = $data_siswa;
			$list_data["data_siswa_detil"] = $data_siswa_detil;
			$list_data["data_iuran"] = $data_iuran;
    	}

		$resp['data'] = $list_data;	
		return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(200)
	        ->set_output(json_encode($resp));
	}


	public function update_iuran() {
		http_response_code(200);
		$resp = array(
				"code" => http_response_code(),
				"message" => "success",
                "data" => [],
        );

	    $utils = new Utils();
		$data_arr = json_decode(json_encode($_POST["list_data_iuran_bulan"],FALSE));
		$data_arr=json_decode($data_arr); 

		$param_check_status = new QueryParameter();

		//
		$data_siswa_kelas = $this->SiswaKelas->get_by_id($data_arr->id_siswa_kelas);
		// print_r($data_siswa_kelas);
		// die();
		// print_r($data_arr);
		$param = array(
			'header_id_siswa_detil' => $data_arr->id_siswa,
			'tingkat_siswa_detil' =>  $data_siswa_kelas->tingkat_kelas,
			'jenis_iuran_siswa_detil' =>  $data_arr->iuran[0]->jenis_iuran,
		);
		// print_r($param);
		// die();
		$data_siswa_detil = $this->SiswaDetil->get_list_table($param);
		// echo $data_siswa_detil;
		// print_r(count($data_siswa_detil));
		// die();
		$besar_iuran = $data_siswa_detil[0]->besar_iuran_siswa_detil;
		// print_r($data_siswa_detil);
		//
		//
		$sum_besar_iuran=0;
		foreach ($data_arr->iuran as $key => $value) {

		// unset($data_arr[$key]->id_siswa);
		// unset($data_arr[$key]->tingkat_kelas);
		// unset($data_arr[$key]->mode_iuran);

		// array_remove_object($data_arr, $key, 'id_siswa');
			
			//if($value->status_iuran=="LUNAS"){
				// $param_check_status->setClause($param_check_status->getClause() . " AND id_siswa_iuran" . "='".$value->id_siswa_iuran."'");
				// $param_check_status->setClause($param_check_status->getClause() . " AND bulan_iuran" . "='".$value->bulan_iuran."'");
			/*
			$param_check_status->setClause($param_check_status->getClause() . " AND id_iuran" . "='".$value->id_iuran."'");
			//}
		
			$param_check_status=$param_check_status->getClause();

			if($param_check_status!="1"){
				foreach ($this->Iuran->get_list_table($param_check_status) as $key_iuran => $value_iuran) {
					$other_arr[] =  array(
						'id_iuran' => $value_iuran->id_iuran,
						// 'id_siswa_iuran' => $value_iuran->id_siswa_iuran,
						// 'bulan_iuran' => $value_iuran->bulan_iuran,
						// 'status_iuran' => "LUNAS",
					);
				}
			}
			*/
			if(intval($data_arr->total_iuran)-$besar_iuran==0){
				$value->status_iuran="LUNAS";
			}else{
				$value->status_iuran="BELUM_LUNAS";
			}
			// $sum_besar_iuran+=intval($value->besar_iuran);
		}
		// print_r($data_arr);
		if(intval($data_arr->total_iuran) > $besar_iuran){
			http_response_code(400);
			// echo "BAYAR LEBIH ".($sum_besar_iuran-$besar_iuran);
			$resp['code']=http_response_code();
	    	$resp['message'] = "Tidak dapat membayar, Karena Kelebihan ".number_format(intval($data_arr->total_iuran)-$besar_iuran);
		}else{
			// if($sum_besar_iuran-$besar_iuran<0)
			// echo "BAYAR KURANG ".($sum_besar_iuran-$besar_iuran);
			// else
			// echo "BAYAR CUKUP ".($sum_besar_iuran-$besar_iuran);
			//
			// if($param_check_status!="1"){
			// 	foreach ($this->Iuran->get_list_table($param_check_status) as $key_iuran => $value_iuran) {
			// 		$other_arr[] =  array(
			// 			'id_iuran' => $value_iuran->id_iuran,
			// 			'id_siswa_iuran' => $value_iuran->id_siswa_iuran,
			// 			'bulan_iuran' => $value_iuran->bulan_iuran,
			// 			'status_iuran' => "LUNAS",
			// 		);
			// 	}
			// }
			//
			$this->Iuran->update_detil($data_arr->iuran);
			http_response_code(200);
			$resp['code']=http_response_code();
			$resp['data']=$data_arr;
			$resp['message']="Data berhasil diubah.";
		}
		// print_r($sum_besar_iuran > $besar_iuran);
		// if(sizeof($other_arr) > 0) $this->Iuran->update_detil($other_arr);

		// // print_r($data_arr);
		// $this->Iuran->update_detil($data_arr);
		// $resp['data']=$data_arr;
		// $resp['message']="Data berhasil diubah.";

        return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header($resp['code'])
	        ->set_output(json_encode($resp));
	}

	public function save() {
		$resp = array(
				"code" => http_response_code(200),
				"message" => "success",
                "data" => [],
        );

	    $utils = new Utils();
		$data_arr = json_decode(json_encode($_POST["list_data_iuran_bulan"],FALSE));
		$data_arr=json_decode($data_arr); 

		$data_siswa_kelas=null;
		$param = new QueryParameter();
		if(isset($_POST["id_siswa"])){
			$param->setClause($param->getClause() . " AND header_id_siswa_kelas" . "='".$_POST["id_siswa"]."'");
			$param->setClause($param->getClause() . " AND status_siswa_kelas" . "='"."1"."'");

			$data_siswa_kelas = $this->SiswaKelas->get_list_table($param->getClause());
		}

		$same_no_iuran=$utils->getAlphaNumericString(10);
		$arr = array();
		$other_arr = array();
		date_default_timezone_set('Asia/Bangkok');

		$param_check_status = new QueryParameter();
		foreach ($data_arr as $key => $value) {
			$id_siswa_kelas=0;
			if($data_siswa_kelas == null) $id_siswa_kelas=$value->id_siswa_kelas_iuran;
			else $id_siswa_kelas=$data_siswa_kelas[0]->id_siswa_kelas;
			
			$arr[$key] =  array(
				'id_iuran' => $utils->guidv4(),
				'id_siswa_iuran' => $value->id_siswa_iuran,
				'id_siswa_kelas_iuran' => $id_siswa_kelas,
				// 'jumlah_iuran' => $value->jumlah_iuran,
				'bulan_iuran' => $value->bulan_iuran,
				// 'jatuh_tempo_iuran' => $value->jatuh_tempo_iuran,
				// 'no_iuran' => date("Ymd"),
				'no_iuran' => $same_no_iuran,
				'tanggal_iuran' => ($value->tanggal_iuran==null?date("Y-m-d H:i:s"):$value->tanggal_iuran),
				'besar_iuran' => $value->besar_iuran,
				'jenis_iuran' => $value->jenis_iuran,
				'cicilan_iuran' => 0,
				'keterangan_iuran' => $value->keterangan_iuran,
				'status_iuran' => $value->status_iuran,
				'user_added_iuran' => $this->session->userdata('fullname'),
				'date_added_iuran' => date('Y-m-d H:i:s'),
			);

			if($value->status_iuran=="LUNAS"){
				$param_check_status->setClause($param_check_status->getClause() . " AND id_siswa_iuran" . "='".$value->id_siswa_iuran."'");
				$param_check_status->setClause($param_check_status->getClause() . " AND bulan_iuran" . "='".$value->bulan_iuran."'");
				$param_check_status->setClause($param_check_status->getClause() . " AND jenis_iuran" . "='".$value->jenis_iuran."'");
			}
		}

		if($param_check_status->getClause() !="1"){
			foreach ($this->Iuran->get_list_table($param_check_status) as $key_iuran => $value_iuran) {
				$other_arr[] =  array(
					'id_iuran' => $value_iuran->id_iuran,
					'id_siswa_iuran' => $value_iuran->id_siswa_iuran,
					'bulan_iuran' => $value_iuran->bulan_iuran,
					'status_iuran' => "LUNAS",
				);
			}
		}

		// print_r($other_arr);
		// die();
		// $data_check_status=$this->Iuran->get_list_table($param_check_status);

		// print_r($data_check_status);
		// die();
		if(sizeof($other_arr) > 0) $this->Iuran->update_detil($other_arr);
		// print_r($other_arr);
		// die();
		// echo "FROM SERVER";
		// print_r($arr);
		// die();
		if(sizeof($arr) > 0) $this->Iuran->save_detil($arr);
		else{
			$resp['code']=http_response_code(400);
	    	$resp['message'] = "Data masih kosong";

	    	return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(400)
	        ->set_output(json_encode($resp));
		}

		$resp['message']="Data berhasil disimpan.";

        return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(200)
	        ->set_output(json_encode($resp));
	}

	public function cetak() {
		$param_filter= array(
			'id_siswa_iuran' => $this->input->get("id_transaksi"), 
			'tanggal_iuran' => $this->input->get("tanggal_transaksi"), 
			'tahun_ajaran' => $this->input->get("tahun_ajaran")
		);
		$param="1";
		$param = $param." AND "."id_siswa_iuran"." = "."'".$this->input->get("id_transaksi")."'";
		$param = $param." AND "."tanggal_iuran"." LIKE "."'%".$this->input->get("tanggal_transaksi")."%'";
		// print_r($param);
		// echo "<br/>";
		$data= $this->Iuran->get_list_table($param);

		// $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);
		// $a = new \NumberFormatter("it-IT", \NumberFormatter::CURRENCY); 

		$spp=0;
		$spp_ke="";
		$spp_key=[];
		$dpmp=0;
		$dll=0;
		$total=0;
		print_r($data);
		$bulan = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
		foreach ($data as $key => $value) {
			if($value->jenis_iuran=='IURAN_PER_BULAN'){
				$spp+=$value->besar_iuran;
				$piece=explode(" ",$value->bulan_iuran);
				$spp_ke.=",".array_keys($bulan, $piece[0])[0];
			}
			if($value->jenis_iuran=='DPMP'){
				$dpmp+=$value->besar_iuran;
			}
			if($value->jenis_iuran=='DU'){
				$dll+=$value->besar_iuran;
			}
			$total+=$value->besar_iuran;
		}
		// print_r($data);
		$data_header= $this->Siswa->get_by_id($this->input->get("id_transaksi"));
		//
		$param = new QueryParameter();
		$param->setClause($param->getClause() . " AND header_id_siswa_kelas" . "='".$data_header->id_siswa."'");

		if(isset($param_filter['tahun_ajaran']) && $param_filter['tahun_ajaran'] != '') $param->setClause($param->getClause() . " AND tahun_ajaran" . "='".$param_filter['tahun_ajaran']."'");
		else $param->setClause($param->getClause() . " AND status_siswa_kelas" . "='1'");
		// print_r($param->getClause());
		$param = $param->getClause();
		$data_siswa_kelas = $this->SiswaKelas->get_list_table($param)[0];
		//
		// print_r($data_siswa_kelas);
		$data['output'] = array(
							'ref' => $data_header->ref_siswa,
							'nama' => $data_header->nama_lengkap_siswa, 
							'kelas' => $data_siswa_kelas->kelas,
							'total_iuran_text' => terbilang($total), 
							'total_iuran_number' => number_format($total), 
							'spp' => array(
									'bulan' => substr($spp_ke, 1), 
									'jumlah' => number_format((int)$spp),
							), 
							'dsp' => number_format($dpmp),
							'dll' => number_format($dll),
						);

		$this->load->view('cetak_kwitansi_view',$data);
	}

	public function print_invoice($kode) {
		// $param_filter= array(
		// 	'id_siswa_iuran' => $this->input->get("id_transaksi"), 
		// 	'tanggal_iuran' => $this->input->get("tanggal_transaksi"), 
		// 	'tahun_ajaran' => $this->input->get("tahun_ajaran")
		// );
		//KwitansiIuran
		$param = new QueryParameter();
		$param->setClause($param->getClause() . " AND kode_kwitansi_iuran" . "='".$kode."'");
		$param = $param->getClause();
		$new_param=$this->KwitansiIuran->get_list_table2($param);

		print_r($new_param[0]);
		// $id_transaksi=$new_param[0]->id_siswa;
		$id_transaksi=$new_param[0]->id_siswa_kelas_kwitansi_iuran;
		$tanggal_transaksi=$new_param[0]->tanggal_kwitansi_iuran;
		$kode=$new_param[0]->kode_kwitansi_iuran;
		$ref_siswa=$new_param[0]->ref_siswa;
		$nama_lengkap_siswa=$new_param[0]->nama_lengkap_siswa;
		$kelas=$new_param[0]->kelas;
		// echo $tanggal_transaksi;
		// die();
		//$param="1";
		// $param = $param." AND "."id_siswa_iuran"." = "."'".$id_transaksi."'";
		//$param = $param." AND "."id_siswa_kelas_iuran"." = "."'".$id_transaksi."'";
		//$param = $param." AND "."tanggal_iuran"." LIKE "."'%".$tanggal_transaksi."%'";
		//
		$param = new QueryParameter();
		$param->setClause($param->getClause() . " AND id_siswa_kelas_iuran" . "='".$id_transaksi."'");
		//$param->setClause($param->getClause() . " AND tanggal_iuran" . " LIKE '%".$tanggal_transaksi."%'");
		//

		$data= $this->Iuran->get_list_table($param);

		$spp=0;
		$spp_ke="";
		$spp_key=[];
		$dpmp=0;
		$dll=0;
		$total=0;
		// print_r($data);
		$bulan = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
		foreach ($data as $key => $value) {
			if($value->jenis_iuran=='IURAN_PER_BULAN'){
				$spp+=$value->besar_iuran;
				$piece=explode(" ",$value->bulan_iuran);
				$spp_ke.=",".array_keys($bulan, $piece[0])[0];
			}
			if($value->jenis_iuran=='DPMP'){
				$dpmp+=$value->besar_iuran;
			}
			if($value->jenis_iuran=='DU'){
				$dll+=$value->besar_iuran;
			}
			$total+=$value->besar_iuran;
		}
		// print_r($data);
		// $data_header= $this->Siswa->get_by_id($id_transaksi);
		// //
		// $param = new QueryParameter();
		// $param->setClause($param->getClause() . " AND header_id_siswa_kelas" . "='".$data_header->id_siswa."'");

		// if(isset($param_filter['tahun_ajaran']) && $param_filter['tahun_ajaran'] != '') $param->setClause($param->getClause() . " AND tahun_ajaran" . "='".$param_filter['tahun_ajaran']."'");
		// else $param->setClause($param->getClause() . " AND status_siswa_kelas" . "='1'");
		// // print_r($param->getClause());
		// $param = $param->getClause();
		// $data_siswa_kelas = $this->SiswaKelas->get_list_table($param)[0];
		//
		// print_r($data_siswa_kelas);
		$data['output'] = array(
							'kode' => $kode,
							'ref' => $ref_siswa,
							'nama' => $nama_lengkap_siswa, 
							'kelas' => $kelas,
							'total_iuran_text' => terbilang($total), 
							'total_iuran_number' => number_format($total), 
							'spp' => array(
									'bulan' => substr($spp_ke, 1), 
									'jumlah' => number_format((int)$spp),
							), 
							'dsp' => number_format($dpmp),
							'dll' => number_format($dll),
						);

		$this->load->view('cetak_kwitansi_view',$data);
	}

	public function check_pembayaran_manual($id) {
		$list_data = [];

		$data_siswa = [];
		$resp = array(
			"code" => http_response_code(200),
			"message" => "success",
	        "data" => [],
	    );

		//untuk check data siswa saja
		$data_siswa = $this->Siswa->get_by_id($id);
		// $param="1";
		// $param = $param." AND "."header_id_siswa_kelas"." = "."'".$data_siswa->id_siswa."'";
		// $param = $param." AND "."status_siswa_kelas"." = "."'"."1"."'";
		// $data_siswa_kelas = $this->SiswaKelas->get_list_table($param);
		// print_r($data_siswa_kelas);
		
		if($data_siswa==null){
			$resp['code']=http_response_code(400);
	    	$resp['message'] = "Data dengan id '".$id."' tidak ditemukan!";

	    	return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(400)
	        ->set_output(json_encode($resp));
		}else{
			// print_r($data_siswa);
			// $angkatan_siswa = new DateTime();

			// $tanggal=explode("-", $data_siswa_kelas->tanggal_registrasi_siswa)[2];
			// $bulan=explode("-", $data_siswa_kelas->tanggal_registrasi_siswa)[1];
			// $tahun=explode("-", $data_siswa_kelas->tanggal_registrasi_siswa)[0];
			// // $tahun=2018;
			// // print_r($tanggal." ".$bulan." ".$tahun);
			// // die();
			// $angkatan_siswa->setDate($tahun, $bulan, $tanggal);
			
			// print_r($selisih);
			// die();
			$angkatan_sekarang = new DateTime();
			$month = $angkatan_sekarang->format("m");
		    if($month>=6) $angkatan_sekarang=$angkatan_sekarang->format("Y")."/".($angkatan_sekarang->format("Y")+1);
		    else $angkatan_sekarang=($angkatan_sekarang->format("Y")-1)."/".$angkatan_sekarang->format("Y");
			
		    // print_r($angkatan_sekarang);
		    // echo "<p>angkatan siswa</p>";

			// $month = $angkatan_siswa->format("m");
		    // if($month>=6) $angkatan_siswa=$angkatan_siswa->format("Y")."/".($angkatan_siswa->format("Y")+1);
		    // else $angkatan_siswa=($angkatan_siswa->format("Y")-1)."/".$angkatan_siswa->format("Y");
		    $angkatan_siswa = $data_siswa->tahun_angkatan_siswa."/".($data_siswa->tahun_angkatan_siswa+1);
			
		    // print_r($angkatan_siswa);
		    if($angkatan_sekarang==$angkatan_siswa){
				$granted = array(
					"message" => "Pembayaran manual, hanya dapat dilakukan untuk angkatan dibawah tahun ".$angkatan_sekarang,
			        "status" => FALSE,
			    );
				$list_data["data_granted"] = $granted;
		    }else $list_data["data_granted"] = TRUE;
			    //start
				$param = new QueryParameter();
				$param->setClause($param->getClause() . " AND header_id_siswa_kelas" . "='".$id."'");
				$param->setClause($param->getClause() . " AND status_siswa_kelas" . "='1'");
				$data_siswa_kelas = $this->SiswaKelas->get_list_table($param->getClause());

				$param = new QueryParameter();
				$param->setClause($param->getClause() . " AND id_siswa_iuran" . "='".$data_siswa->id_siswa."'");
				$param->setClause($param->getClause() . " AND jenis_iuran" . " LIKE '%".'_MANUAL'."%'");
				$data_iuran = $this->Iuran->get_list_table($param);
			    foreach ($data_iuran as $key_iuran_new => $value_iuran_new) {
			    	$object = $data_iuran[$key_iuran_new];
			    	if($value_iuran_new->jenis_iuran=="IURAN_PER_BULAN_MANUAL")
					$object->bulan_custom_iuran = "SPP:"." ".$value_iuran_new->bulan_iuran;

					$du_iuran = explode(" ",$value_iuran_new->keterangan_iuran);
			    	if($value_iuran_new->jenis_iuran=="DU_MANUAL")
					$object->bulan_custom_iuran = $du_iuran[0].": ".$du_iuran[1];
			    }
			    

				$list_data["data_siswa_kelas"] = $data_siswa_kelas[0];
				$list_data["data_iuran"] = $data_iuran;
		}

		$resp['data'] = $list_data;	

		return $this->output
	        ->set_content_type('application/json')
	        ->set_status_header(200)
	        ->set_output(json_encode($resp));
	}
}



function terbilang( $num ,$dec=4){
    $stext = array(
        "Nol",
        "Satu",
        "Dua",
        "Tiga",
        "Empat",
        "Lima",
        "Enam",
        "Tujuh",
        "Delapan",
        "Sembilan",
        "Sepuluh",
        "Sebelas"
    );
    $say  = array(
        "Ribu",
        "Juta",
        "Milyar",
        "Triliun",
        "Biliun", // remember limitation of float
        "--apaan---" ///setelah biliun namanya apa?
    );
    $w = "";

    if ($num <0 ) {
        $w  = "Minus ";
        //make positive
        $num *= -1;
    }

    $snum = number_format($num,$dec,",",".");
    // die($snum);
    $strnum =  explode(".",substr($snum,0,strrpos($snum,",")));
    //parse decimalnya
    $koma = substr($snum,strrpos($snum,",")+1);

    $isone = substr($num,0,1)  ==1;
    if (count($strnum)==1) {
        $num = $strnum[0];
        switch (strlen($num)) {
            case 1:
            case 2:
                if (!isset($stext[$strnum[0]])){
                    if($num<19){
                        $w .=$stext[substr($num,1)]." Belas";
                    }else{
                        $w .= $stext[substr($num,0,1)]." Puluh ".
                            (intval(substr($num,1))==0 ? "" : $stext[substr($num,1)]);
                    }
                }else{
                    $w .= $stext[$strnum[0]];
                }
                break;
            case 3:
                $w .=  ($isone ? "Seratus" : terbilang(substr($num,0,1)) .
                    " Ratus").
                    " ".(intval(substr($num,1))==0 ? "" : terbilang(substr($num,1)));
                break;
            case 4:
                $w .=  ($isone ? "Seribu" : terbilang(substr($num,0,1)) .
                    " Ribu").
                    " ".(intval(substr($num,1))==0 ? "" : terbilang(substr($num,1)));
                break;
            default:
                break;
        }
    }else{
        $text = $say[count($strnum)-2];
        $w = ($isone && strlen($strnum[0])==1 && count($strnum) <=3? "Satu ".strtolower($text) : terbilang($strnum[0]).' '.$text);
        array_shift($strnum);
        $i =count($strnum)-2;
        foreach ($strnum as $k=>$v) {
            if (intval($v)) {
                $w.= ' '.terbilang($v).' '.($i >=0 ? $say[$i] : "");
            }
            $i--;
        }
    }
    $w = trim($w);
    if ($dec = intval($koma)) {
        $w .= " Koma ". terbilang($koma);
    }
    return trim($w);
}

class QueryParameter {
	var $locale = "en_US";
	var $clause = "1";
	var $innerClause = "1";
	var $values = "";
	var $order = "";
	var $group = "";
	var $limit = 100;
	var $offset = 0;

	function getClause(){
		return $this->clause;
	}

	function setClause($val){
		$this->clause = $val;
	}

	function getOrder(){
		return $this->order;
	}

	function setOrder($val){
		$this->order = $val;
	}

	function getLimit(){
		if($this->limit > 200) return 200;
		return $limit;
	}

	function setLimit($val){
		$this->limit = $val;
	}
	//start offset

	//end offset

	function getValues(){
		return $values;
	}

	function setValues($val){
		$this->values = $val;
	}
	//start group

	//end group

	//start innerClause

	//end innerClause

	//start localse

	//end localse

}