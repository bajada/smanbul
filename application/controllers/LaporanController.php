<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class LaporanController extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('Laporan');
        $this->load->model('Iuran');
	}

	public function index()
	{	
		die("NOT DEFINE ANYTING");
		// $this->load->view('iuran_view');
	}

	public function list_table() {
		$param = [];
    	$list_data = [];
    	$data_laporan = [];
		$resp = array(
    				"code" => "code",
    				"message" => "message",
                    "data" => [],
            );
		$spesifik_tanggal = $this->input->get('spesifik_tanggal');

		$filterKeyword = $this->input->get('filter_keyword');

		if(isset($filterKeyword)) {
			if($filterKeyword != ''){
    			$param="1";
				$param = $param." AND "."real_tanggal_iuran"." LIKE "."'%".$filterKeyword."%'";
				$data_laporan=$this->Laporan->get_list_harian($param);
				$data_laporan_uniq=$this->Laporan->get_list_harian_uniq($param);
    		}
    	}else{
			$param="1";
			$new_value1=$this->input->get("filter_start_date");
			$new_value1=explode('/', $new_value1);
			$new_value1=date('Y-m-d', strtotime($new_value1[2]."-".$new_value1[0]."-".$new_value1[1])); 
			$new_value2=$this->input->get("filter_end_date");
			$new_value2=explode('/', $new_value2);
			$new_value2=date('Y-m-d', strtotime($new_value2[2]."-".$new_value2[0]."-".$new_value2[1]));
			$param = $param." AND "."real_tanggal_iuran"." BETWEEN "."'".$new_value1."'"." AND "."'".$new_value2."'";
			$data_laporan=$this->Laporan->get_list_harian($param);
			$data_laporan_uniq=$this->Laporan->get_list_harian_uniq($param);
    	}

    	// print_r($data_laporan);
    	// die();

   //  	$daaa=[];
   //  	foreach ($data_laporan as $key => $value) {
   //  		$param="1";
	  //   	// $param = $param." AND "."id_siswa_iuran"." = "."'".$value->id_siswa."'";
	  //   	$param = $param." AND "."id_siswa_kelas_iuran"." = "."'".$value->id_siswa_kelas."'";
	  //   	$param = $param." AND "."jenis_iuran"." = "."'IURAN_PER_BULAN'";
			// if(isset($filterKeyword)){
			// 	$param = $param." AND "."tanggal_iuran"." LIKE "."'%".$this->input->get("filter_keyword")."%'";	
			// }else{
			// 	$param = $param." AND "."real_tanggal_iuran"." BETWEEN "."'".$new_value1."'"." AND "."'".$new_value2."'";
			// }
   //  		// print_r($param);
			// $data_iuran=$this->Iuran->get_list_table($param);
			// $value->total_bulan=$data_iuran;
   //  	}
    	//
		$param="1";
		$param = new QueryParameter();
		if(isset($filterKeyword)){
			// $param = $param." AND "."tanggal_iuran"." LIKE "."'%".$this->input->get("filter_keyword")."%'";	
			$param->setClause($param->getClause() . " AND tanggal_iuran" . "LIKE'%".$this->input->get("filter_keyword")."%'");
		}else{
			// $param = $param." AND "."real_tanggal_iuran"." BETWEEN "."'".$new_value1."'"." AND "."'".$new_value2."'";
			$param->setClause($param->getClause() . " AND real_tanggal_iuran" . " BETWEEN '".$new_value1."'"." AND "."'".$new_value2."'");
		}
		$data_iuran =$this->Iuran->get_list_table($param);
    
		$arrayName = array(
			'laporan' => $data_laporan, 
			'laporan_uniq' => $data_laporan_uniq,
			'iuran' => $data_iuran
		);
		$resp['data'] = $arrayName;	
		echo json_encode($resp);
	}
}


class QueryParameter {
	var $locale = "en_US";
	var $clause = "1";
	var $innerClause = "1";
	var $values = "";
	var $order = "";
	var $group = "";
	var $limit = 100;
	var $offset = 0;

	function getClause(){
		return $this->clause;
	}

	function setClause($val){
		$this->clause = $val;
	}

	function getOrder(){
		return $this->order;
	}

	function setOrder($val){
		$this->order = $val;
	}

	function getLimit(){
		if($this->limit > 200) return 200;
		return $limit;
	}

	function setLimit($val){
		$this->limit = $val;
	}
	//start offset

	//end offset

	function getValues(){
		return $values;
	}

	function setValues($val){
		$this->values = $val;
	}
	//start group

	//end group

	//start innerClause

	//end innerClause

	//start localse

	//end localse

}