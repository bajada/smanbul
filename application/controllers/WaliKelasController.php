<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class WaliKelasController extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('WaliKelas');
	}

	public function index()
	{	
		$this->load->view('kriteria_view');
	}

	public function get_by_id($id) {
		$data = $this->WaliKelas->get_by_id($id);
		$action=$this->input->get('action');
        $resp = array(
        				"code" => "code",
        				"message" => "message",
                        "data" => $data,
                );

		if(isset($action)) {
			if($action=='delete') {
				$resp['message'] = "Apakah anda yakin akan menghapus data '".$data->id_wali_kelas."'?";
                
			}
		}
		echo json_encode($resp);
	}

	public function do_action($id, $action) {
        $resp = array(
        				"code" => "code",
        				"message" => "message",
                        "data" => [],
                );

		if(isset($action)) {
			if($action=='delete') {
				$data = $this->WaliKelas->get_by_id($id); //getEntity
				$resp['data'] = $data;
				$this->WaliKelas->delete($id);
				
				$resp['message'] = "Data '".$data->id_wali_kelas."' berhasil dihapus.";
                
			}
		}
		echo json_encode($resp);
	}

	public function list_table() {
		$param = [];
        $resp = array(
        				"code" => "code",
        				"message" => "message",
                        "data" => [],
        );

		$filterKeyword = $this->input->get('filter_keyword');

		if(isset($filterKeyword)) {
			if($filterKeyword != ''){
				$param= $this->input->get("filter_keyword");
				$param= array(
					'nama_wali_kelas' => $param
				);
    		}
    	}
		$data = $this->WaliKelas->get_list_table($param);

		$resp['data'] = $data;	
		echo json_encode($resp);
	}

	public function save() {
		
		$data = array(
			'id_wali_kelas' => "",
            'nama_wali_kelas' => ""
        );
        $resp = array(
				"code" => "code",
				"message" => "message",
                "data" => [],
        );
		$id_wali_kelas = $this->input->post('id_wali_kelas');

		if(isset($id_wali_kelas)){
			$data['id_wali_kelas'] = $this->input->post('id_wali_kelas');
		}

		$data['nama_wali_kelas'] = $this->input->post('nama_wali_kelas');

		$resp['data'] = $data;	
        if(isset($id_wali_kelas)){
			$this->WaliKelas->update($data);
			$resp['message'] = "Data berhasil diupdate.";
		}else{
			$this->WaliKelas->save($data);
			$resp['message'] = "Data berhasil disimpan.";
		}
        echo json_encode($resp);
	}
}