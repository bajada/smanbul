<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class JenisIuranController extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
        $this->load->model('JenisIuran');
	}

	public function index()
	{	
		$this->load->view('jenis_iuran_view');
	}

	public function get_by_id($id) {
		$resp = array(
				"code" => "code",
				"message" => "message",
                "data" => [],
        );
		$data = $this->JenisIuran->get_by_id($id);
		$resp['data'] = $data;
		$action=$this->input->get('action');

		if(isset($action)) {
			if($action=='delete') {
				$resp['message'] = "Apakah anda yakin akan menghapus data '".$data->nama_jenis_iuran."'?";    
			}
		}
		echo json_encode($resp);
	}
	public function do_action($id, $action) {
		$resp = array(
				"code" => "code",
				"message" => "message",
                "data" => [],
        );
		if(isset($action)) {
			if($action=='delete') {
				$data = $this->JenisIuran->get_by_id($id); //getEntity
				$resp['data'] = $data;
				$this->JenisIuran->delete($id);				
				$resp['message'] = "Data '".$data->nama_jenis_iuran."' berhasil dihapus.";     
			}
		}
		echo json_encode($resp);
	}

	public function list_table() {
		$resp = array(
				"code" => http_response_code(200),
				"message" => "success",
                "data" => [],
        );
		$nama_jenis = $this->input->get('filter_jenis_iuran');
		$tingkat_jenis = $this->input->get('filter_kelas');
		$tingkat_jenis_iuran = $this->input->get('tingkat_jenis_iuran');

		// $param.=" AND "."tingkat_jenis_iuran"." = '".$filterKeyword."'";
		// if(isset($filterKeyword)) {
		// 	if($filterKeyword != ''){
		// 		$param= array(
		// 			// 'nama_jenis_iuran' => $this->input->get("filter_keyword"),
		// 			'tingkat_jenis_iuran' => $this->input->get("tingkat_jenis_iuran"),
		// 		);
  //   		}
  //   	}
		$param = new QueryParameter();
		if(isset($tingkat_jenis) && $tingkat_jenis!='0') {
			$param->setClause($param->getClause() . " AND tingkat_jenis_iuran" . "='".$tingkat_jenis."'");
			$tingkat_jenis="ALL";
			$param->setClause($param->getClause() . " OR tingkat_jenis_iuran" . "='".$tingkat_jenis."'");
		}
		if(isset($tingkat_jenis_iuran) && $tingkat_jenis_iuran!='0') {
			$param->setClause($param->getClause() . " AND tingkat_jenis_iuran" . "='".$tingkat_jenis_iuran."'");
			$tingkat_jenis="ALL";
			$param->setClause($param->getClause() . " OR tingkat_jenis_iuran" . "='".$tingkat_jenis."'");
		}

		// print_r($param);
		// die();
		if(isset($nama_jenis) && $nama_jenis!='0') $param->setClause($param->getClause() . " AND nama_jenis_iuran" . " LIKE'%".$nama_jenis."%'");
		
		
		$param=$param->getClause();
		// $param->setClause($param->getClause() . " OR ref_siswa" . "='".$this->input->post('ref')[$i]."'");
		$data = $this->JenisIuran->get_list_table2($param);
		$resp['data'] = $data;	
		echo json_encode($resp);
	}

	public function save() {
		$resp = array(
				"code" => "code",
				"message" => "message",
                "data" => [],
        );
		
		$data = array(
			'id_jenis_iuran' => "",
            'nama_jenis_iuran'=> "",
            'besar_jenis_iuran'=> "",
            'cicilan_jenis_iuran'=> "",
        );
		$id = $this->input->post('id');

		if(isset($id)){
			$data['id_jenis_iuran'] = $id;
		}

		$data['nama_jenis_iuran'] = $this->input->post('nama');
		$data['besar_jenis_iuran'] = $this->input->post('besar');
		$data['cicilan_jenis_iuran'] = $this->input->post('cicilan');
		$data['tingkat_jenis_iuran'] = $this->input->post('tingkat');

		$resp['data'] = $data;	

		$pass = true;
		if($data['nama_jenis_iuran'] == null || $data['tingkat_jenis_iuran'] == null || $data['besar_jenis_iuran'] == null){
			$pass = false;
		}else{
			if($data['nama_jenis_iuran'] == "" || $data['tingkat_jenis_iuran'] == "" || $data['besar_jenis_iuran'] == ""){
				$pass = false;
			}
		}

		if(!$pass){
			$resp['code'] = http_response_code(500);
			$resp['message'] = "Input belum lengkap, harus dilengkapi terlebih dahulu.";
			return $this->output
            ->set_content_type('application/json')
            ->set_status_header(500)
            ->set_output(json_encode($resp));
		}

        if(isset($id)){
			$this->JenisIuran->update($data);
			$resp['message'] = "Data berhasil diupdate.";
		}else{
			$this->JenisIuran->save($data);
			$resp['message'] = "Data berhasil disimpan.";
		}
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($resp));
	}
}


class QueryParameter {
	var $locale = "en_US";
	var $clause = "1";
	var $innerClause = "1";
	var $values = "";
	var $order = "";
	var $group = "";
	var $limit = 100;
	var $offset = 0;

	function getClause(){
		return $this->clause;
	}

	function setClause($val){
		$this->clause = $val;
	}

	function getOrder(){
		return $clause;
	}

	function setOrder($val){
		$this->clause = $val;
	}

	function getLimit(){
		if($this->limit > 200) return 200;
		return $limit;
	}

	function setLimit($val){
		$this->limit = $val;
	}
	//start offset

	//end offset

	function getValues(){
		return $values;
	}

	function setValues($val){
		$this->values = $val;
	}
	//start group

	//end group

	//start innerClause

	//end innerClause

	//start localse

	//end localse
}