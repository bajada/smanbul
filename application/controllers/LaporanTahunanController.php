<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class LaporanTahunanController extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('Laporan');
        $this->load->model('Iuran');
        $this->load->model('SiswaDetil');
	}

	public function index()
	{	
		die("NOT DEFINE ANYTING");
	}

	public function list_table() {
    	$list_data = [];
    	$data_laporan = [];
		$resp = array(
    				"code" => http_response_code(200),
    				"message" => "success",
                    "data" => [],
            );

		$filterTahun = $this->input->get('filter_tahun');
		$filterKelas = $this->input->get('filter_kelas');

		$param="1";	
		if(isset($filterTahun) && $filterTahun != '') {
			$param = $param." AND "."tahun_ajaran_siswa_kelas"." = "."'".$filterTahun."'";
    	}	
		if(isset($filterKelas) && $filterKelas != '') {
			$param = $param." AND "."tingkat_kelas"." = "."'".$filterKelas."'";
    	}
		$data_laporan=$this->Laporan->get_list_table($param);
		// print_r($param);
		// print_r($data_laporan);
		// die();
		$list_data=[];
		if(isset($filterTahun) && $filterTahun != '' && isset($filterKelas) && $filterKelas != '') {
			// $thn=$this->check_year_now($filterTahun, $filterKelas);

			// foreach ($thn as $key => $value) {
			// 	$arrayName['tahun']=$value;
			// 	$arrayName['kelas']=$value;
			// }
			foreach ($this->check_year_now($filterTahun, $filterKelas) as $key_head => $value_head) {
				// print_r($filterTahun==$value_head['tahun']);
				if($filterTahun==$value_head['tahun']){
					$list_data['header'][]=$value_head;
				}
			}
			// $list_data['header'] = $this->check_year_now($filterTahun, $filterKelas);
    	}
		// die();
		
		$bulan_temp = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
    	
    	foreach ($data_laporan as $key => $value) {
    		// $param="1";
	    	// $param = $param." AND "."id_siswa_iuran"." = "."'".$value->id_siswa."'";
	    	// $param = $param." AND "."id_siswa_kelas_iuran"." = "."'".$value->id_siswa_kelas."'";
	    	// $param = $param." AND "."jenis_iuran"." = "."'IURAN_PER_BULAN'";
			$param = new QueryParameter();
			$param->setClause($param->getClause() . " AND id_siswa_iuran" . "='".$value->id_siswa."'");
			$param->setClause($param->getClause() . " AND id_siswa_kelas_iuran" . "='".$value->id_siswa_kelas."'");
			// $param->setClause($param->getClause() . " AND jenis_iuran" . "='"."IURAN_PER_BULAN"."'");
			$data_iuran_tabungan = $this->Iuran->get_list_table($param);

			//
			$new_data_iuran = [];
			$new_data_tabungan = [];
			foreach ($data_iuran_tabungan as $key_iuran => $value_iuran_tabungan) {//
				foreach ($list_data['header'] as $key_1 => $value_1) {
					foreach ($value_1['month_year'] as $key_2 => $value_2) {

						if($value_2==$value_iuran_tabungan->bulan_iuran){
							if($value_iuran_tabungan->jenis_iuran=="IURAN_PER_BULAN")
							$new_data_iuran[]=$value_iuran_tabungan;
							if($value_iuran_tabungan->jenis_iuran=="TABUNGAN")
							$new_data_tabungan[]=$value_iuran_tabungan;
						}

					}
				}
			}
			//
			$param = new QueryParameter();
			$param->setClause($param->getClause() . " AND id_siswa_iuran" . "='".$value->id_siswa."'");
			$param->setClause($param->getClause() . " AND jenis_iuran" . "='"."DPMP"."'");
    		// $param="1";
	    	// $param = $param." AND "."id_siswa_iuran"." = "."'".$value->id_siswa."'";
	    	// $param = $param." AND "."jenis_iuran"." = "."'DPMP'";
			$data_iuran_dpmp =$this->Iuran->get_list_table($param);
			//
			$param = new QueryParameter();
			$param->setClause($param->getClause() . " AND id_siswa_iuran" . "='".$value->id_siswa."'");
			$param->setClause($param->getClause() . " AND jenis_iuran" . "='"."DU"."'");
    		// $param="1";
	    	// $param = $param." AND "."id_siswa_iuran"." = "."'".$value->id_siswa."'";
	    	// $param = $param." AND "."jenis_iuran"." = "."'DU'";
	    	// $param = $param." AND "."tahun_ajaran"." = "."'".$value->tahun_ajaran."'";
			$data_iuran_du =$this->Iuran->get_list_table($param);

			$arrayMonthTemp = array('Jul' => 0, 'Aug' => 0, 'Sep' => 0, 'Oct' => 0, 'Nov' => 0, 'Dec' => 0, 'Jan' => 0, 'Feb' => 0, 'Mar' => 0, 'Apr' => 0, 'May' => 0, 'Jun' => 0);
			//set data

			foreach ($new_data_iuran as $key_data_iuran => $value_data_iuran) {
				$arrayMonthTemp[explode(" ", $value_data_iuran->bulan_iuran)[0]] +=$value_data_iuran->besar_iuran;
				$value->new_list_bulan=$arrayMonthTemp;
			}
			
			$arrayMonthTemp = array('Jul' => 0, 'Aug' => 0, 'Sep' => 0, 'Oct' => 0, 'Nov' => 0, 'Dec' => 0, 'Jan' => 0, 'Feb' => 0, 'Mar' => 0, 'Apr' => 0, 'May' => 0, 'Jun' => 0);
			foreach ($new_data_tabungan as $key_data_tabungan => $value_data_tabungan) {
				$arrayMonthTemp[explode(" ", $value_data_tabungan->bulan_iuran)[0]] +=$value_data_tabungan->besar_iuran;
				$value->new_list_tabungan=$arrayMonthTemp;
			}
			
			$value->list_bulan=$new_data_iuran; //$data_iuran;
			$value->list_tabungan=$new_data_tabungan; //$data_iuran;
			$value->list_dpmp=$data_iuran_dpmp;
			$value->list_du=$data_iuran_du;
			//
			// $param = new QueryParameter();
			// $param->setClause($param->getClause() . " AND header_id_siswa_detil" . "='".$value->id_siswa."'");
			$param="1";
	    	$param = $param." AND "."header_id_siswa_detil"." = "."'".$value->id_siswa."'";
	    	// $param = $param." AND "."tingkat_siswa_detil"." = "."'".explode(" ", $value->kelas)[0]."'";
			$data_jenis_iuran =$this->SiswaDetil->get_list_table($param);

			$value->besar_iuran=$data_jenis_iuran;
    	}
    	// $list_data['besar_iuran'] = $data_jenis_iuran;
    	// print_r($data_laporan);
    	// die();
    	
		// $param="1";
		// $param = $param." AND "."tanggal_iuran"." LIKE "."'%".$this->input->get("filter_keyword")."%'";	
		// $data_iuran =$this->Iuran->get_list_table($param);
    	
    	// print_r($data_laporan);
		// $arrayName = array('laporan' => $data_laporan, 'iuran' => $data_iuran);
    	// print_r($new_data_iuran);
    	// die();
		$list_data['main'] = $data_laporan;
		// $sub_data = array(
		// 	'header' => $list_data['header'], 
		// 	'main' => $list_data, 
		// );
		$resp['data'] = $list_data;	
		// print_r($resp);
		// die();
		echo json_encode($resp);
	}

	function check_year_now($tahun_ajaran, $kelas){
		// $tahun_ajaran=$this->input->get('tahun_ajaran');
		// $kelas=$this->input->get('kelas');
		$date = new DateTime();

		$tahun_p=$tahun_ajaran;
		$tahun_p=explode("/", $tahun_p);
		$date->setDate($tahun_p[1], 7, 10);


		$obj=[];
		$angkatan = new DateTime();
		if($kelas=="XII"){
			$angkatan->setDate($tahun_p[0]-2, 7, 10);
			
			$obj['kelas'][] = "X";
			$obj['kelas'][] = "XI";
			$obj['kelas'][] = $kelas;
		}else if($kelas=="XI"){
			$angkatan->setDate($tahun_p[0]-1, 7, 10);
			$obj['kelas'][] = "X";
			$obj['kelas'][] = $kelas;
		}else if($kelas=="X"){
			$angkatan->setDate($tahun_p[0], 7, 10);
			$obj['kelas'][] = $kelas;
		}else{
			echo 400;
			die();
		}
		
		$selisih = $angkatan->format("Y")+$date->format("Y")-$angkatan->format("Y");

		$month = $date->format("m");
        if($month>=6) $date=$date->format("Y")."/".($date->format("Y")+1);
        else $date=($date->format("Y")-1)."/".$date->format("Y");
		//
		$get_year=[];
		$get_years=[];
		$index=0;
		for($tahun_ajaran_i=$angkatan->format("Y"); $tahun_ajaran_i < $selisih; $tahun_ajaran_i++){
            // if($tahun_ajaran_i."/".($tahun_ajaran_i+1)!=$date){
                $get_year[]=$this->get_tahun_ajaran($tahun_ajaran_i."/".($tahun_ajaran_i+1));
                $get_years[]=$tahun_ajaran_i."/".($tahun_ajaran_i+1);
            // }
            if($tahun_ajaran_i."/".($tahun_ajaran_i+1)!=$date){
                $get_year_now[]=$this->get_tahun_ajaran($tahun_ajaran_i."/".($tahun_ajaran_i+1));
                $get_years_now[]=$tahun_ajaran_i."/".($tahun_ajaran_i+1);
            }
			$arrayName[] = array(
				'kelas' => $obj['kelas'][$index],
				'tahun' => $tahun_ajaran_i."/".($tahun_ajaran_i+1),
				'month_year' => $this->get_tahun_ajaran($tahun_ajaran_i."/".($tahun_ajaran_i+1))
			);
			$index++;
        }
		// print_r($get_year);
		// print_r($angkatan);
		// echo "<br/>";
		// print_r($date);
		// echo "<br/>";
		// print_r($selisih);
		return $arrayName;
	}

	public function get_tahun_ajaran($string_date){
        $tahun_ajaran = explode("/", $string_date);
        $new_month=[];
        $start = $month = strtotime($tahun_ajaran[0].'-07-10');
		$end = strtotime($tahun_ajaran[1].'-06-10');
		while($month <= $end)
		{
		     // echo date('F Y', $month), PHP_EOL;
		     $new_month[]=date('M Y', $month);
		     $month = strtotime("+1 month", $month);
		}
        return $new_month;
    }
}