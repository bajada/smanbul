<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class SiswaController extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('Siswa');
	}

	public function index()
	{	
		$this->load->view('siswa_view');
	}

	public function get_by_id($id) {
		$data = $this->Siswa->get_by_id($id);
		$action=$this->input->get('action');
        $resp = array(
        				"code" => "200",
        				"message" => "success",
                        "data" => $data,
                );

		if(isset($action)) {
			if($action=='delete') {
				$resp['message'] = "Apakah anda yakin akan menghapus data '".$data->id_siswa."'?";
                
			}
		}
		echo json_encode($resp);
	}
	public function get_ref_now() {
		$data = $this->Siswa->newRef();
        $resp = array(
        				"code" => "200",
        				"message" => "success",
                        "data" => $data,
                );
		echo json_encode($resp);
	}
	public function do_action($id, $action) {
        $resp = array(
        				"code" => "code",
        				"message" => "message",
                        "data" => [],
                );

		if(isset($action)) {
			if($action=='delete') {
				$data = $this->Siswa->get_by_id($id); //getEntity
				$resp['data'] = $data;
				$this->Siswa->delete($id);
				
				$resp['message'] = "Data '".$data->id_siswa."' berhasil dihapus.";
                
			}
		}
		echo json_encode($resp);
	}

	public function list_table() {
		$param = "1";
        $resp = array(
        				"code" => http_response_code(200),
        				"message" => "http_response_code(200)",
                        "data" => [],
        );

		// $filterKeyword = $this->input->get('filter_keyword');

		// if(isset($filterKeyword)) {
		// 	if($filterKeyword != ''){
		// 		$param= $this->input->get("filter_keyword");
		// 		$param= array(
		// 			'ref_siswa' => $param,
		// 			'nama_lengkap_siswa' => $param
		// 		);
  //   		}
  //   	}
		$data = $this->Siswa->get_list_table($param);

		$resp['data'] = $data;	
		echo json_encode($resp);
	}


	public function save_all() {
        $resp = array(
				"code" => http_response_code(200),
				"message" => "http_response_code(200)",
                "data" => [],
        );
		$data = array(
			'id_siswa' => "",
            'ref_siswa'=> "",
            'nama_lengkap_siswa'=> "",
            'kelas_siswa'=>  "",
            'jenis_kelamin_siswa'=>  "",
            'tahun_ajaran_siswa'=>  ""
        );

        $arr=array();
        $arr_2=array();
        $arr_3=array();
        $check_val=[];
        $check_vals="1";

        //
      $year=date("Y");
      $s_year=date("Y");
      $e_year=date("Y")+1;
      $month=date("m");
      if($month>6){
        $s_year=$year;
        $e_year=($year+1);
      }else{
        $s_year=($year-1);
        $e_year=$year;
      }
      $full_year=$s_year."/".$e_year;
		$begin = new DateTime( $s_year.'-07-10' );
		$end = new DateTime( $e_year.'-06-10' );
		$end = $end->modify( '+1 month' ); 


		$interval = new DateInterval('P1M');
		$daterange = new DatePeriod($begin, $interval ,$end);
        //

		for ($i=0; $i < count($this->input->post('ref')); $i++) { 
			$arr[$i] =  array(
				'id_siswa' => $this->Siswa->longNumberId(),
				'ref_siswa' => $this->input->post('ref')[$i],
				'nama_lengkap_siswa' => $this->input->post('nama_lengkap')[$i],
				'kelas_siswa' => $this->input->post('kelas')[$i],
				'jenis_kelamin_siswa' => $this->input->post('jenis_kelamin')[$i],
				'tahun_ajaran_siswa' => ($this->input->post('tahun_ajaran')[$i]==$full_year?$full_year:$this->input->post('tahun_ajaran')[$i])
			);

			if($this->input->post('nama_lengkap')[$i] == "") {
				$resp['code']=http_response_code(400);
        		$resp['message'] = "Input belum lengkap, harap dilengkapi terlebih dahulu.";
        		return http_response_code(400);
			}
			$check_val[$i]=$this->input->post('nama_lengkap')[$i];
			$check_vals.=" OR nama_lengkap_siswa"."='".$this->input->post('nama_lengkap')[$i]."'";
			$check_vals.=" OR ref_siswa"."='".$this->input->post('ref_siswa')[$i]."'";
		}


		for ($i=0; $i < count($this->input->post('jenis_iuran')); $i++) {
			$x=$this->input->post('header_siswa')[$i];
			$arr_2[$i] =  array(
				'header_id_siswa_detil' => $arr[$x]['id_siswa'],
				'jenis_iuran_siswa_detil' => $this->input->post('jenis_iuran')[$i],
				'besar_iuran_siswa_detil' => $this->input->post('besar_iuran')[$i],
			);
		}

		foreach($daterange as $date){
			foreach ($arr as $key => $value) {
				$arr_3[] =  array(
					'id_siswa_iuran' => $value['id_siswa'],
					'bulan_iuran' => $date->format('M Y'),
					'jatuh_tempo_iuran' => $date->format('Y-m-d'),
					'no_iuran' => '',
					'tanggal_iuran' => '',
				);
			}
		}

		// print_r($arr);	
		// echo "string";
		// print_r($arr_2);
		// echo "string";
		// print_r($arr_3);

        $check=$this->Siswa->get_list_table(substr($check_vals, 4));
		
		// echo "string";
		// print_r($check);
		// echo "cek:".count($check);
        if(count($check)>0){
			$resp['code']=http_response_code(409);
        	$ca="";
        	for ($i=0; $i < count($check); $i++) { 
        		$ca.=",".$check[$i]->nama_lengkap_siswa;
			}
        	$resp['message'] = "Terjadi DUPLIKASI pada data: ".substr($ca, 1);
        	return $this->output
            ->set_content_type('application/json')
            ->set_status_header(409)
            ->set_output(json_encode($resp));
        }else{
	        //save master
	        // $this->Siswa->save_master($arr);
	        // //save detil
	        // $this->Siswa->save_detil($arr_2);
	        // //save iuran
	        // $this->Siswa->save_iuran($arr_3);

	        //save all
	        $this->Siswa->save_dmi($arr_2, $arr, $arr_3);

        	$resp['message'] = "Data telah berhasil disimpan.";
	        $resp['data'] = array('data_master' => $arr,'data_detil' => $arr_2,'data_iuran' => $arr_3);
        }
		return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($resp));
	}

	public function save() {
		
		$data = array(
			'id_siswa' => "",
            'nis_siswa'=> "",
            'ref_siswa'=> "",
            'nama_lengkap_siswa'=> "",
            'kelas_siswa'=>  "",
            'tahun_ajaran_siswa'=>  "",
            'jk_siswa'=> "",
            'alamat_lengkap_siswa'=> "",
            'file_siswa'=> ""
        );
        $resp = array(
				"code" => "code",
				"message" => "message",
                "data" => [],
        );
		$id = $this->input->post('id');

		if(isset($id)){
			$data['id_siswa'] = $id;
		}else{
			$data['id_siswa'] = $this->Siswa->newId();
		}
		$data['nis_siswa'] = $this->input->post('nis');
		$data['ref_siswa'] = $this->input->post('ref');
		$data['nama_lengkap_siswa'] = $this->input->post('nama_lengkap');
		$data['kelas_siswa'] = $this->input->post('kelas_siswa');
		$data['tahun_ajaran_siswa'] = $this->input->post('tahun_ajaran');
		$data['jk_siswa'] = $this->input->post('jk');
		$data['alamat_lengkap_siswa'] = $this->input->post('alamat_lengkap');
		#$data['file_siswa'] = $this->input->post('file');

		$resp['data'] = $data;	
        if(isset($id)){
			$this->Siswa->update($data);
			$resp['message'] = "Data berhasil diupdate.";
		}else{
			$this->Siswa->save($data);
			$data_arr = json_decode(json_encode($_POST["list_iuran"],FALSE));
			$data_arr=json_decode($data_arr); 

			//print_r($data_arr);
			$cart = array();
			foreach ($data_arr as $key => $value) {
				$cart[$key] =  array(
					'header_id_siswa_detil' => $data['id_siswa'],
					'besar_iuran_siswa_detil' => $value->besar_jenis_iuran,
					'jenis_iuran_siswa_detil' => $value->nama_jenis_iuran
				);
			}
			$this->Siswa->save_detil($cart);
			$resp['message']="Data berhasil disimpan.";
		}
        echo json_encode($resp);
	}
}