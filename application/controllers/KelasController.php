<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class KelasController extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('Kelas');
	}

	public function index()
	{	
		$this->load->view('kelas_view');
	}

	public function get_by_id($id) {
		$resp = array(
				"code" => http_response_code(200),
				"message" => "http_response_code(200)",
                "data" => [],
        );
		$data = $this->Kelas->get_by_id($id);
		$resp['data'] = $data;
		$action=$this->input->get('action');

		if(isset($action)) {
			if($action=='delete') {
				$resp['message'] = "Apakah anda yakin akan menghapus data '".$data->id_kelas."'?";    
			}
		}
		echo json_encode($resp);
	}

	public function do_action($id, $action) {
		$resp = array(
				"code" => http_response_code(200),
				"message" => "http_response_code(200)",
                "data" => [],
        );
		if(isset($action)) {
			if($action=='delete') {
				$data = $this->Kelas->get_by_id($id); //getEntity
				$resp['data'] = $data;
				$this->Kelas->delete($id);				
				$resp['message'] = "Data '".$data->id_kelas."' berhasil dihapus.";     
			}
		}
		return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($resp));
	}

	public function list_table_side() {
		$resp = array(
				"code" => http_response_code(200),
				"message" => "http_response_code(200)",
                "data" => [],
        );

    	$param = new QueryParameter();
		$filterKelas = $this->input->get('filter_jurusan_kelas');
		if(isset($filterKelas) && $filterKelas != '0') {
			$param->setClause($param->getClause() . " AND tingkat_kelas" . "='".$filterKelas."'");
		}
		$data = $this->Kelas->getListKelasByJurusan($param);

		$resp['data'] = $data;	
		return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($resp));
	}

	public function list_table() {
		$resp = array(
				"code" => http_response_code(200),
				"message" => "http_response_code(200)",
                "data" => [],
        );
    	$param = new QueryParameter();
		$filterKeyword = $this->input->get('filter_keyword');
		$filterKelas = $this->input->get('filter_kelas');
		$filterJurusanKelas = $this->input->get('filter_jurusan_kelas');
		if(isset($filterKeyword) && $filterKeyword != '') {
			$param->setClause($param->getClause() . " AND jurusan_kelas" . "='".$filterKeyword."'");
			$param->setClause($param->getClause() . " OR wali_kelas" . " LIKE'%".$filterKeyword."%'");
		}
		if(isset($filterKelas) && $filterKelas != '0') {
			$param->setClause($param->getClause() . " AND tingkat_kelas" . "='".$filterKelas."'");
		}
		if(isset($filterJurusanKelas) && $filterJurusanKelas != '0') {
			$param->setClause($param->getClause() . " AND jurusan_kelas" . "='".$filterJurusanKelas."'");
		}
		// $param = $param->getClause();
		// print_r($param);
		// die();
		$data = $this->Kelas->get_list_table2($param);

		$resp['data'] = $data;	
		return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($resp));
	}

	public function save() {
		$resp = array(
				"code" => http_response_code(200),
				"message" => "http_response_code(200)",
                "data" => [],
        );
		$utils = new Utils();
		$data = array(
			'id_kelas' => "",
            'tingkat_kelas'=> "",
            'sub_tingkat_kelas	'=> "",
            'jurusan_kelas'=> "",
            'wali_kelas'=> "",
        );
		$id = $this->input->post('id');

		if(isset($id)){
			$data['id_kelas'] = $id;
		}else{
			$data['id_kelas'] = $utils->longNumberId();
		}

		
		$data['tingkat_kelas'] = $this->input->post('tingkat');
		$data['sub_tingkat_kelas'] = $this->input->post('sub_tingkat');
		$data['jurusan_kelas'] = $this->input->post('jurusan');
		$data['wali_kelas'] = $this->input->post('wali_kelas');
		$resp['data'] = $data;

		$pass = true;
		if($data['tingkat_kelas'] == null || $data['jurusan_kelas'] == null){
			$pass = false;
		}else{
			if($data['tingkat_kelas'] == "" || $data['jurusan_kelas'] == ""){
				$pass = false;
			}
		}

		if(!$pass){
			$resp['code'] = http_response_code(500);
			$resp['message'] = "Input belum lengkap, harus dilengkapi terlebih dahulu.";
			return $this->output
            ->set_content_type('application/json')
            ->set_status_header(500)
            ->set_output(json_encode($resp));
		}

        if(isset($id)){
			$this->Kelas->update($data);
			$resp['message'] = "Data berhasil diupdate.";
		}else{
			$this->Kelas->save($data);
			$resp['message'] = "Data berhasil disimpan.";
		}
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($resp));
	}
}



class QueryParameter {
	var $locale = "en_US";
	var $clause = "1";
	var $innerClause = "1";
	var $values = "";
	var $order = "";
	var $group = "";
	var $limit = 100;
	var $offset = 0;

	function getClause(){
		return $this->clause;
	}

	function setClause($val){
		$this->clause = $val;
	}

	function getOrder(){
		return $clause;
	}

	function setOrder($val){
		$this->clause = $val;
	}

	function getLimit(){
		if($this->limit > 200) return 200;
		return $limit;
	}

	function setLimit($val){
		$this->limit = $val;
	}
	//start offset

	//end offset

	function getValues(){
		return $values;
	}

	function setValues($val){
		$this->values = $val;
	}
	//start group

	//end group

	//start innerClause

	//end innerClause

	//start localse

	//end localse
}