<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

// Load library phpspreadsheet
require('./excel/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Style;
// End load library phpspreadsheet

class LaporanExcel extends CI_Controller
 {
 	
 	public function __construct()
 	{
 		parent::__construct();
 		$this->load->model("Laporan");
 		$this->load->model("Iuran");
 		$this->load->model("SiswaDetil");
 	}

 	public function tahunan(){
 		$filterTahun = $this->input->get('filter_tahun');
		$filterKelas = $this->input->get('filter_kelas');

 		$spreadsheet = new Spreadsheet();
 		$spreadsheet->getDefaultStyle()->getFont()->setName('Times New Roman');
		$spreadsheet->getDefaultStyle()->getFont()->setSize(9);


		$sheet = $spreadsheet->getActiveSheet();
		$sheet->getSheetView()->setZoomScale(100);
		$sheet->setShowGridlines(false);
		$sheet->setTitle('LAPORAN TAHUNAN');
		//
		// $arr_col=["KEUANGAN IURAN KELAS", "SMA NEGERI 1 CIBUNGBULANG", "TAHUN PELAJARAN {$filterTahun}"];
		
		// $arr=["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ"];
		// foreach ($arr_col as $key => $value) {
		// 	$sheet->setCellValue('A1', 'KEUANGAN IURAN KELAS '.$filterKelas);
		// }

		$sheet->setCellValue('A1', 'KEUANGAN IURAN KELAS '.$filterKelas);
		$sheet->setCellValue('A2', 'SMA NEGERI 1 CIBUNGBULANG');
		$sheet->setCellValue('A3', "TAHUN PELAJARAN {$filterTahun}");
		
		$font_A1=$sheet->getStyle("A1");
		$font_A1->getFont()->setBold(true);
		$font_A1->getFont()->setSize(12);
		$font_A1->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
		->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		
		$font_A2=$sheet->getStyle("A2");
		$font_A2->getFont()->setBold(true);
		$font_A2->getFont()->setSize(12);
		$font_A2->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
		->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$font_A3=$sheet->getStyle("A3");
		$font_A3->getFont()->setBold(true);
		$font_A3->getFont()->setSize(9);
		$font_A3->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
		->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$sheet->mergeCells('A1:AJ1');
		$sheet->mergeCells('A2:AJ2');
		$sheet->mergeCells('A3:AJ3');
		// sheet peratama
		$sheet->setCellValue('A6', 'NO');
		$sheet->setCellValue('B6', 'REF');
		$sheet->setCellValue('C6', 'NAMA SISWA');
		$sheet->setCellValue('D6', 'JK');
		$sheet->setCellValue('E6', 'KELAS');
		$sheet->setCellValue('F6', 'WALI KELAS');
		$sheet->setCellValue('G6', 'BESAR IURAN');
		$sheet->setCellValue('H6', 'BULAN KE');

        $monthNameList_random = ["Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "Jan", "Feb", "Mar", "Apr", "May", "Jun"];
        $arr_col=array("H","I","J","K","L","M","N","O","P","Q","R","S");
        foreach($arr_col as $key => $value){
            $sheet->setCellValue($value.'7', $key+1);
            $sheet->setCellValue($value.'8', strtoupper($monthNameList_random[$key]));
        }

		// $sheet->setCellValue('T6', 'JUMLAH BAYAR IURAN BULANAN');
		// $sheet->setCellValue('U6', 'SISA IURAN BULANAN');
		// $sheet->setCellValue('V6', 'DATA TUNGGAKAN KEUANGAN TAHUN LALU');

		//
		$sheet->getCell('T6')->setValue("JUMLAH BAYAR\nIURAN BULANAN");
		$sheet->getStyle('T6')->getAlignment()->setWrapText(true);

		$sheet->getCell('U6')->setValue("SISA IURAN\nBULANAN");
		$sheet->getStyle('U6')->getAlignment()->setWrapText(true);

		$sheet->getCell('V6')->setValue("DATA TUNGGAKAN\nKEUANGAN TAHUN LALU");
		$sheet->getStyle('V6')->getAlignment()->setWrapText(true);
		//
		

		$sheet->mergeCells('A6:A8');
		$sheet->mergeCells('B6:B8');
		$sheet->mergeCells('C6:C8');
		$sheet->mergeCells('D6:D8');
		$sheet->mergeCells('E6:E8');
		$sheet->mergeCells('F6:F8');
		$sheet->mergeCells('G6:G8');
		$sheet->mergeCells('H6:S6');
		$sheet->mergeCells('T6:T8');
		$sheet->mergeCells('U6:U8');

		//V, Y, AB
		$header = $this->check_year_now($filterTahun, $filterKelas);
		$arr=array("V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ","BA","BB","BC","BD","BE","BF","BG","BH","BI","BJ","BK","BL","BM");
		$sheet->mergeCells('V6:'.$arr[sizeof($header)*3-1].'6');

		$arr_data_header=["OSIS","Komputer","SPP"];
		$arr_data_header_2=["TGL","BAYAR"];
		$keytu=0;
		$keytu2=0;
		$key_ot=0;
		$idx=0;
		// print_r((sizeof($header)*3+4).'<br/>');
		$sheet->setCellValue($arr[(sizeof($header)*3+4)].'6', 'BULAN');
		$sheet->mergeCells($arr[(sizeof($header)*3+4)].'6:'.$arr[(sizeof($header)*3+4+23)].'6');
		$this->set_border_and_background2($arr[(sizeof($header)*3+4)].'6:'.$arr[sizeof($header)*3+4+23].'6', $sheet, $spreadsheet, false);
		foreach ($header as $key => $value) {
			$sheet->setCellValue($arr[$key*3].'7', 'DU '.$value['tahun'].' ('.$value['kelas'].')');
			// print_r(($key*3).'</br>');
			$sheet->mergeCells($arr[$key*3].'7:'.$arr[$key*3+2].'7');
			
			foreach ($arr_data_header as $key2 => $value2) {
				$sheet->setCellValue($arr[$keytu].'8', $value2);
				$sheet->getColumnDimension($arr[$keytu])->setWidth(12);
				$keytu++;
			}

			$key_ot=0;
			foreach ($value['month_year'] as $key_month => $month) {
				if(isset($filterKelas) && $filterKelas != ''){
					if($value['kelas']==$filterKelas){
	                    $sheet->setCellValue($arr[(sizeof($header)*3+4+$key_month+$key_ot)].'7', $month."-".$key_month);
	                    $sheet->mergeCells($arr[(sizeof($header)*3+4+$key_month+$key_ot)].'7:'.$arr[(sizeof($header)*3+4+$key_month+$key_ot+1)].'7');
	                    $this->set_border_and_background2($arr[(sizeof($header)*3+4+$key_month+$key_ot)].'7:'.$arr[sizeof($header)*3+4+$key_month+$key_ot+1].'7', $sheet, $spreadsheet, false);
                		$key_ot++;

						foreach ($arr_data_header_2 as $key2 => $value2) {
							$sheet->setCellValue($arr[(sizeof($header)*3+4+$idx)].'8', $value2);
							$this->set_border_and_background2($arr[(sizeof($header)*3+4+$idx)].'8:'.$arr[sizeof($header)*3+4+$idx].'8', $sheet, $spreadsheet,false);

							$sheet->getColumnDimension("{$arr[(sizeof($header)*3+4+$idx)]}")->setWidth(12);

							$idx++;
						}
					}
                }
			}
		}

		// die();
		$row_data=[];
		$row_data=["JUMLAH\nTUNGGAKAN\nDAFTAR ULANG", "TOTAL", "KETERANGAN", "DPMP"];

		if(isset($filterKelas)) {
			if($filterKelas == 'X') $row_data[3]="DPMP";
			else $row_data[3]="DPMP SEBELUMNYA";
    	}
	
		foreach ($row_data as $key => $value) {
			if($key==0) {
				$sheet->getStyle("{$arr[$key+sizeof($header)*3]}6")->getAlignment()->setWrapText(true);
				$sheet->getColumnDimension("{$arr[$key+sizeof($header)*3]}")->setWidth(20);
			}
			if($key > 0) $sheet->getColumnDimension("{$arr[$key+sizeof($header)*3]}")->setWidth(20);
			
			$sheet->setCellValue($arr[$key+sizeof($header)*3].'6', $value);
			$this->set_border_and_background2($arr[$key+sizeof($header)*3].'6:'.$arr[$key+sizeof($header)*3].'8', $sheet, $spreadsheet,false);
			$sheet->mergeCells($arr[$key+sizeof($header)*3].'6:'.$arr[$key+sizeof($header)*3].'8');
		}

		$num_temp=24;
		$row_data=[];
		$row_data=["JUMLAH\nBAYAR DPMP", "SISA DPMP", "KETERANGAN", "JUMLAH\nBIAYA\nIURAN 1 TH"];

		if(isset($filterKelas)) {
			if($filterKelas == 'XII') {
				array_push($row_data,"TUNGGAKAN\nIURAN KELAS XI","KELAS X");
				$num_temp=22;
			}
			if($filterKelas == 'XI') {
				array_push($row_data,"TUNGGAKAN\nIURAN KELAS X");
				$num_temp=23;
			}
    	}

		// print_r(sizeof($row_data));
		foreach ($row_data as $key => $value) {

			if($key==0 || $key==3 || $key==4 || $key==5) $sheet->getStyle("{$arr[$key+sizeof($header)*3+sizeof($row_data)+$num_temp]}6")->getAlignment()->setWrapText(true);

			$sheet->getColumnDimension("{$arr[$key+sizeof($header)*3+sizeof($row_data)+$num_temp]}")->setWidth(20);
			// print_r($key);
			$sheet->setCellValue($arr[$key+sizeof($header)*3+sizeof($row_data)+$num_temp].'6', $value);
			// print_r($arr[$key+sizeof($header)*3+sizeof($row_data)+22]."<br/>");
			$this->set_border_and_background2($arr[$key+sizeof($header)*3+sizeof($row_data)+$num_temp].'6:'.$arr[$key+sizeof($header)*3+sizeof($row_data)+$num_temp].'8', $sheet, $spreadsheet,false);
			$sheet->mergeCells($arr[$key+sizeof($header)*3+sizeof($row_data)+$num_temp].'6:'.$arr[$key+sizeof($header)*3+sizeof($row_data)+$num_temp].'8');
		}

	    //set width size
	    $arr_col=array("F","G");
	    foreach ($arr_col as $key => $value) {
	    	$spreadsheet->getActiveSheet()->getColumnDimension($value)->setWidth(15);
	    }
	    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
	    $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(7);
	    $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(30);
	    $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	    $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	    $spreadsheet->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
	    $spreadsheet->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);

		$this->set_border_and_background2('A6:S6', $sheet, $spreadsheet,false);
	 	$this->set_border_and_background2('H7:S7', $sheet, $spreadsheet,false);
	 	$this->set_border_and_background2('H8:S8', $sheet, $spreadsheet,false);
	 	$this->set_border_and_background('A7:G8', $sheet, $spreadsheet,false);
	 	$this->set_border_and_background2('T6:T8', $sheet, $spreadsheet,false);
	 	$this->set_border_and_background2('U6:U8', $sheet, $spreadsheet,false);
	 	$this->set_border_and_background2('V6:'.$arr[sizeof($header)*3-1].'6', $sheet, $spreadsheet,false);
	 	$this->set_border_and_background2('V7:'.$arr[sizeof($header)*3-1].'7', $sheet, $spreadsheet,false);
	 	$this->set_border_and_background2('V8:'.$arr[sizeof($header)*3-1].'8', $sheet, $spreadsheet,false);

	 	
    	$param="1";	
		if(isset($filterTahun) && $filterTahun != '') {
			$param = $param." AND "."tahun_ajaran_siswa_kelas"." = "."'".$filterTahun."'";
    	}	
		if(isset($filterKelas) && $filterKelas != '') {
			$param = $param." AND "."tingkat_kelas"." = "."'".$filterKelas."'";
    	}
		// print_r($param);
		$data_laporan=$this->Laporan->get_list_table($param);

	 	foreach ($data_laporan as $key => $value) {
	 		$param = new QueryParameter();
			$param->setClause($param->getClause() . " AND id_siswa_iuran" . "='".$value->id_siswa."'");
			// $param->setClause($param->getClause() . " AND id_siswa_kelas_iuran" . "='".$value->id_siswa_kelas."'");
			$param->setClause($param->getClause() . " AND jenis_iuran" . "='"."IURAN_PER_BULAN"."'");

    		// $param="1";
	    	// $param = $param." AND "."id_siswa_iuran"." = "."'".$value->id_siswa."'";
	    	// $param = $param." AND "."jenis_iuran"." = "."'IURAN_PER_BULAN'";
			$data_iuran =$this->Iuran->get_list_table($param);
			$param = new QueryParameter();
			$param->setClause($param->getClause() . " AND id_siswa_iuran" . "='".$value->id_siswa."'");
			$param->setClause($param->getClause() . " AND jenis_iuran" . "='"."DPMP"."'");
			
    		// $param="1";
	    	// $param = $param." AND "."id_siswa_iuran"." = "."'".$value->id_siswa."'";
	    	// $param = $param." AND "."jenis_iuran"." = "."'DPMP'";
			$data_iuran_dpmp =$this->Iuran->get_list_table($param);
			
			$param = new QueryParameter();
			$param->setClause($param->getClause() . " AND id_siswa_iuran" . "='".$value->id_siswa."'");
			$param->setClause($param->getClause() . " AND jenis_iuran" . "='"."DU"."'");
    		// $param="1";
	    	// $param = $param." AND "."id_siswa_iuran"." = "."'".$value->id_siswa."'";
	    	// $param = $param." AND "."jenis_iuran"." = "."'DU'";
	    	// $param = $param." AND "."tahun_ajaran"." = "."'".$value->tahun_ajaran."'";
			$data_iuran_du =$this->Iuran->get_list_table($param);
			//
			$arrayMonthTemp = array('Jul' => 0, 'Aug' => 0, 'Sep' => 0, 'Oct' => 0, 'Nov' => 0, 'Dec' => 0, 'Jan' => 0, 'Feb' => 0, 'Mar' => 0, 'Apr' => 0, 'May' => 0, 'Jun' => 0);
			foreach ($data_iuran as $key_data_iuran => $value_data_iuran) {
				$arrayMonthTemp[explode(" ", $value_data_iuran->bulan_iuran)[0]] += $value_data_iuran->besar_iuran;
				$value->new_list_bulan=$arrayMonthTemp;
			}
			//
			//set data
			$value->list_bulan=$data_iuran;
			$value->list_dpmp=$data_iuran_dpmp;
			$value->list_du=$data_iuran_du;

			$param="1";
	    	$param = $param." AND "."header_id_siswa_detil"." = "."'".$value->id_siswa."'";
	    	// $param = $param." AND "."tingkat_siswa_detil"." = "."'".explode(" ", $value->kelas)[0]."'";
			$data_jenis_iuran =$this->SiswaDetil->get_list_table($param);

			$value->besar_iuran=$data_jenis_iuran;
    	}
 		//
 		$no=1;
		$row=9; foreach($data_laporan as $key => $value) {
	        $tingkat_kelas = explode(" ", $value->kelas);
            $tingkat_kelas=$tingkat_kelas[0];
                
               if($value->tahun_ajaran_siswa_kelas==$filterTahun && $tingkat_kelas==$filterKelas ){
	  //       if($value->total_bulan != FALSE){
				$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A'.$row, ($no))
				->setCellValue('B'.$row, $value->ref_siswa)
				->setCellValue('C'.$row, $value->nama_lengkap_siswa)
				->setCellValue('D'.$row, substr($value->jenis_kelamin_siswa, 0, 1))
				->setCellValue('E'.$row, $value->kelas)
				->setCellValue('F'.$row, $value->wali_kelas)
				->setCellValue('G'.$row, $value->besar_spp)
				->setCellValue('T'.$row, $value->spp)
				->setCellValue('U'.$row, ($value->besar_spp*12)-$value->spp)
				;

				$arr = ["H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S"];
				$monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                $monthNameList_random = ["Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "Jan", "Feb", "Mar", "Apr", "May", "Jun"];
                
                $new_monthNameList_random = array(
                	'Jul' => 'Jul', 
                	'Aug' => 'Aug', 
                	'Sep' => 'Sep', 
                	'Oct' => 'Oct', 
                	'Nov' => 'Nov', 
                	'Dec' => 'Dec', 
                	'Jan' => 'Jan', 
                	'Feb' => 'Feb', 
                	'Mar' => 'Mar', 
                	'Apr' => 'Apr', 
                	'May' => 'May', 
                	'Jun' => 'Jun'
                );
                
                $dump=[];///dipake ga ?
                $bulan_iuran_bulanan=[];
                $besar_iuran_bulanan=[];
                $bulan_iuran_du=[];
                $besar_iuran_du=[];
                
                $temp_bulan=[];
                foreach ($monthNameList_random as $key_2 => $month) {

                	// foreach ($value->new_list_bulan as $key => $value_2) {
                	// 	$str = $value_2->bulan_iuran;
                	// 	$str = explode(" ", $str);
                	// 	if($value->id_siswa_kelas==$value_2->id_siswa_kelas){
                	// 		if($month==$str[0]){

	                // 			$sheet->setCellValue($arr[$key_2].$row, substr($value_2->besar_iuran, 0, 3));

                 //            	$besar_iuran_bulanan[]=($value_2->besar_iuran);
	                // 		}
                	// 	}
                	// }
                	//

                	$bulan_iuran_bulanan[]=$month;
                	// if(isset($value->new_list_bulan[$month]))
                	// print_r($value->new_list_bulan[$month]);
                    // $besar_iuran_bulanan[]=$value->new_list_bulan['"'.$month.'"'];
                    // console.log(value.new_list_bulan[month]);
                    //
	                    // var new_val=value.new_list_bulan[month];
	                    // console.log(new_val==undefined);
	                    // if(new_val==0 || new_val==undefined){
	                    //     row += '<td class="text-center">'+"-"+'</td>';
	                    //     temp_bulan.push(parseInt(0));
	                    // }
	                    // if(new_val>0){
	                    //     row += '<td class="bg-success text-white">'+((''+new_val+'').substring(0,3))+'</td>';
	                    //     temp_bulan.push(parseInt((''+new_val+'').substring(0,3)));
	                    // }

						if(isset($value->new_list_bulan[$month])) {

							if($value->new_list_bulan[$month]==0)
							$sheet->setCellValue($arr[$key_2].$row, "-");
							else 
							$sheet->setCellValue($arr[$key_2].$row, intval(substr(number_format($value->new_list_bulan[$month]), 0, 3)));
						
						}else{
							$sheet->setCellValue($arr[$key_2].$row, "-");
						}
						// row += '<td class="bg-success text-white">'+((''+new_val+'').substring(0,3))+'</td>';
      //                   temp_bulan.push(parseInt((''+new_val+'').substring(0,3)));
                	//
                }
                $sum_total_per_bulan[]=$temp_bulan;
                //
                $temp_besar_iuran=[];
                $temp_test=[];
                foreach ($value->besar_iuran as $key => $iuran) {
                	if($iuran->jenis_iuran_siswa_detil=="DU") $temp_besar_iuran[]=$iuran->besar_iuran_siswa_detil;
                }
                $arr=array("V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ","BA","BB","BC","BD","BE","BF","BG","BH","BI","BJ","BK","BL","BM");
				$idx=0;
				$keytu=0;
				$sum_du=0;
                $sum_all_du=0;
                $temp_du=[];
                $keys=0;
                foreach ($header as $key1 => $value1) {
            		$sum_du=0;
            		foreach ($value->list_du as $key_mu => $value_2) {
            			if($value1['tahun']==$value_2->tahun_ajaran_siswa_kelas){
            				$sum_du+=$value_2->besar_iuran;
            			}
            		}
                    $sum_all_du+=$temp_besar_iuran[$key1]-$sum_du;
                    $temp_test[]=$temp_besar_iuran[$key1];
            			foreach ($arr_data_header as $key2 => $value2) {
            				if($key2==$key1){
            					$sheet->setCellValue($arr[$keytu+$key2+$key1+0].$row, $temp_besar_iuran[$key2]-$sum_du);
            					$sheet->setCellValue($arr[$keytu+$key2+$key1+1].$row, 0);
            					$sheet->setCellValue($arr[$keytu+$key2+$key1+2].$row, 0);
            				}
						}
					$keytu++;
					$keytu2++;
				}
				$sheet->setCellValue($arr[sizeof($header)*3].$row, $sum_all_du);
				//
				// print_r($value);
				$row_data=[$sum_all_du, ($value->besar_spp*12)-$value->spp+$sum_all_du, $value->keterangan_masuk_siswa];//, "DPMP"];

				$count_sisa_dpmp_x=0;
                $count_sisa_dpmp_xi=0;
                foreach ($value->list_dpmp as $key => $value_dpmp) {
                	if($filterKelas=="XI"){
                        if($value_dpmp->tingkat_kelas=="X"){
                            $count_sisa_dpmp_x+=$value_dpmp->besar_iuran;
                        }
                    }
                    if($filterKelas=="XII"){
                        if($value_dpmp->tingkat_kelas=="X"){
                            $count_sisa_dpmp_x+=$value_dpmp->besar_iuran;
                        }
                        if($value_dpmp->tingkat_kelas=="XI"){
                            $count_sisa_dpmp_xi+=$value_dpmp->besar_iuran;
                        }
                    }
                }
                if($tingkat_kelas=="X"){
                    $row_data[]=$value->besar_dpmp;
                }else{
                    $row_data[]=$value->besar_dpmp-($count_sisa_dpmp_x+$count_sisa_dpmp_xi);
                }

				foreach ($row_data as $key_data => $value_data) {
					$sheet->setCellValue($arr[sizeof($header)*3+$key_data].$row, $value_data);
				}
                
				$new_value=$value;
				$dump_dpmp=[];$damps_month="";$damps_besar_iuran=0;$damps_tanggal_iuran="";
				$dump_dpmp_month=[];

				$re=0;
				$po=1;
                foreach ($monthNameList_random as $key_ran => $month) {
                	$re=0;
                	// $dump_dpmp_month=[];
                	foreach ($new_value->list_dpmp as $keyss => $value_dpmp) {
                		// $date = new Date(value_dpmp.tanggal_iuran);
                		$newdate=$value_dpmp->tanggal_iuran;
                		$newdate=explode(" ", $newdate);
                		$newdate=$newdate[0];
                		$newdate=explode("-", $newdate);
                		$date = new DateTime();
                		// print_r($newdate);
                		$date->setDate($newdate[0], $newdate[1], $newdate[2]);

                        if($month==$monthNameList[(int)$date->format("m")-1] && $value_dpmp->id_siswa_kelas==$new_value->id_siswa_kelas){
                            $damps_month=$monthNameList[(int)$date->format("m")-1];
                            $temp_date=$value_dpmp->tanggal_iuran;
                            $temp_date= explode(" ", $temp_date);
                            $damps_tanggal_iuran=$temp_date[0];
                            $damps_besar_iuran+=$value_dpmp->besar_iuran;

                            $dump_dpmp_month[]=$temp_date[0];
                            $dump_dpmp_month[]=$value_dpmp->besar_iuran;
                        }
                        // 
                	}

                	if($damps_month==$month){
	                	$sheet->setCellValue($arr[sizeof($header)*3+4+$key_ran+$key_ran].$row, $damps_tanggal_iuran);
	                	$sheet->setCellValue($arr[sizeof($header)*3+4+$key_ran+$po].$row, $damps_besar_iuran);
                    }else{
                    	$sheet->setCellValue($arr[sizeof($header)*3+4+$key_ran+$key_ran].$row, "-");
	                	$sheet->setCellValue($arr[sizeof($header)*3+4+$key_ran+$po].$row, "-");
                    }
                    $po++;
                }

                $sisa_dpmp=($value->besar_dpmp)-((int)$count_sisa_dpmp_x+(int)$count_sisa_dpmp_xi)-$value->dpmp;
                $arr_data_header_3=[$value->dpmp, $sisa_dpmp, ($sisa_dpmp==0?"DPMP LUNAS":"DPMP MENUNGGAK"), "-"];
                foreach ($arr_data_header_3 as $key_head_3 => $value_head_3) {
                	$sheet->setCellValue($arr[sizeof($header)*3+4+(sizeof($monthNameList_random)*2)+$key_head_3].$row, $value_head_3);
                }

                $besar_spp_x=0;
                $besar_spp_xi=0;
                foreach ($value->besar_iuran as $value_besar_iuran) {
                	if($value_besar_iuran->jenis_iuran_siswa_detil=="IURAN_PER_BULAN") {
                        if($filterKelas=="XI"){
                            if($value_besar_iuran->tingkat_siswa_detil=="X"){
                                $besar_spp_x=$value_besar_iuran->besar_iuran_siswa_detil;
                            }
                        }
                        if($filterKelas=="XII"){
                            if($value_besar_iuran->tingkat_siswa_detil=="X"){
                                $besar_spp_x=$value_besar_iuran->besar_iuran_siswa_detil;
                            }
                            if($value_besar_iuran->tingkat_siswa_detil=="XI"){
                                $besar_spp_xi=$value_besar_iuran->besar_iuran_siswa_detil;
                            }
                        }
                    }
                }

                $count_sisa_spp_x=0;
                $count_sisa_spp_xi=0;
                foreach ($value->list_bulan as $key_bulan => $value_bulan) {
                	if($filterKelas=="XI"){
                        if($value_bulan->tingkat_kelas=="X"){
                            $count_sisa_spp_x+=(int)$value_bulan->besar_iuran;
                        }
                    }
                	if($filterKelas=="XII"){
                        if($value_bulan->tingkat_kelas=="X"){
                            $count_sisa_spp_x+=(int)$value_bulan->besar_iuran;
                        }
                        if($value_bulan->tingkat_kelas=="XI"){
                            $count_sisa_spp_xi+=(int)$value_bulan->besar_iuran;
                        }
                    }
                }
                
                $temp_besar_tunggakan=[];
                if($tingkat_kelas=="XI"){
                	$sheet->setCellValue($arr[sizeof($header)*3+4+(sizeof($monthNameList_random)*2)+sizeof($arr_data_header_3)+0].$row, (int)$besar_spp_x*12-$count_sisa_spp_x);
                }
                if($tingkat_kelas=="XII"){
                	$sheet->setCellValue($arr[sizeof($header)*3+4+(sizeof($monthNameList_random)*2)+sizeof($arr_data_header_3)+0].$row, (int)$besar_spp_xi*12-$count_sisa_spp_xi);
                	$sheet->setCellValue($arr[sizeof($header)*3+4+(sizeof($monthNameList_random)*2)+sizeof($arr_data_header_3)+1].$row, (int)$besar_spp_x*12-$count_sisa_spp_x);
                }

                // $spreadsheet->setActiveSheetIndex(0)->setCellValue('T'.$row, $value->spp);
			// 	->setCellValue('E'.$row, substr($dump, 1))
			// 	->setCellValue('F'.$row, $value->spp)
			// 	->setCellValue('G'.$row, $value->dpmp)
			// 	->setCellValue('H'.$row, $value->du)
			// 	// ->setCellValue('I'.$row, $total)
			// 	->setCellValue('I'.$row, '=SUM(F'.$row.':H'.$row.')')
			// 	->setCellValue('J'.$row, $value->tahun_ajaran)
			// 	;
			// 	$arr = array('F','G','H','I');
			// 	foreach ($arr as $key => $value) {
			// 		$sheet->getStyle($value.$row)->getNumberFormat()
   //  				->setFormatCode('#,##0.00');
			// 	}


			// 	//
			// 	$sheet->getStyle('A'.$row.':'.'J'.$row)->applyFromArray(
			// 	   array(
			// 	      'fill' => array(
			// 	          'type' => Fill::FILL_SOLID,
			// 	          'color' => array('rgb' => '963634' )
			// 	      ),
			// 	      'borders' => array(
			// 			'allBorders' => [
			// 			'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
			// 			// 'color' => ['argb' => 'FFFF0000'],
			// 			],
			// 	      ),
			// 	      'font' => array(
			// 	          // 'bold'  =>  true,
			// 	          // 'size' => 24
			// 	      ),
			// 	      'alignment' => array(
			// 	      		'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			// 	      		'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
			// 	      )
			// 	   )
			//  	);

			//     $sheet->getStyle('C'.$row)
		 //    	->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);


			//     $sheet->getStyle('F'.$row)
		 //    	->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
			//     $sheet->getStyle('G'.$row)
		 //    	->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
			//     $sheet->getStyle('H'.$row)
		 //    	->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
			//     $sheet->getStyle('I'.$row)
		 //    	->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
				
			// 	//
				$row++;
				$no++;
			}
		}
		// print_r($row);
		// die();
		//set footer
		$sheet->setCellValue('A'.$row, 'JUMLAH');
		$sheet->mergeCells('A'.$row.':G'.$row);
		$this->set_border_and_background2('A'.$row.':G'.$row, $sheet, $spreadsheet,false);

		$first_row=9;
		$last_row=$row-1;
		//

		$arr_head = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U"];
		for($i=0; $i < sizeof($arr_head); $i++){
			// print_r($arr[$i]."<br/>");
			// $sheet->setCellValue($arr[$i].$row, '=SUM('.$arr[$i].$first_row.':'.$arr[$i].$last_row.')');
			//set footer
			if($i==1 || $i==2 || $i==3 || $i==4 || $i==5)
			$this->set_border_and_background_color($arr_head[$i].$first_row.':'.$arr_head[$i].$last_row, $sheet, $spreadsheet,"", false);
			else $this->set_border_and_background_color($arr_head[$i].$first_row.':'.$arr_head[$i].$last_row, $sheet, $spreadsheet,"", true);
			//set body
			// $this->set_border_and_background2($arr[$i].$row, $sheet, $spreadsheet);
		}

		//
		$arr_head = ["H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U"];
		foreach ($arr_head as $key_random_head => $value_random_head) {
			$sheet->setCellValue($value_random_head.$row, '=SUM('.$value_random_head.$first_row.':'.$value_random_head.$last_row.')');
			$this->set_border_and_background2($value_random_head.$row, $sheet, $spreadsheet,true);

			//set width
			$spreadsheet->getActiveSheet()->getColumnDimension($value_random_head)->setWidth(5);

			for($i=0; $i < sizeof($header)*3; $i++){
				if($key_random_head==$i){	
					//print_r($arr[$i].$first_row."-".$arr[$i].$last_row."<br/>");
					$sheet->setCellValue($arr[$i].$row, '=SUM('.$arr[$i].$first_row.':'.$arr[$i].$last_row.')');
					//set footer
					$this->set_border_and_background_color($arr[$i].$first_row.':'.$arr[$i].$last_row, $sheet, $spreadsheet,"",true);
					//set body
					$this->set_border_and_background2($arr[$i].$row, $sheet, $spreadsheet,true);
				}
			}
		}
		$continue_col=sizeof($header)*3;
		for($i=$continue_col; $i <= sizeof($header)*3+3; $i++){
			// print_r($arr[$i]."<br/>");
			$sheet->setCellValue($arr[$i].$row, '=SUM('.$arr[$i].$first_row.':'.$arr[$i].$last_row.')');
			//set footer
			$this->set_border_and_background_color($arr[$i].$first_row.':'.$arr[$i].$last_row, $sheet, $spreadsheet,"",true);
			//set body
			$this->set_border_and_background2($arr[$i].$row, $sheet, $spreadsheet,true);
		}

		for($i=0; $i < sizeof($monthNameList_random)*2; $i++){
			$sheet->setCellValue($arr[(sizeof($header)*3)+$i+4].$row, '=SUM('.$arr[(sizeof($header)*3)+$i+4].$first_row.':'.$arr[(sizeof($header)*3)+$i+4].$last_row.')');
			//set footer
			$this->set_border_and_background_color($arr[(sizeof($header)*3)+$i+4].$first_row.':'.$arr[(sizeof($header)*3)+$i+4].$last_row, $sheet, $spreadsheet,"",true);
			//set body
			$this->set_border_and_background2($arr[(sizeof($header)*3)+$i+4].$row, $sheet, $spreadsheet,true);
		}

		$num_temp=24;
		$row_data=["JUMLAH BAYAR DPMP", "SISA DPMP", "KETERANGAN", "JUMLAH BIAYA IURAN 1 TH"];
		if(isset($filterKelas)) {
			if($filterKelas == 'XII') {
				array_push($row_data,"TUNGGAKAN IURAN KELAS XI","KELAS X");
				// $num_temp=22;
			}
			if($filterKelas == 'XI') {
				array_push($row_data,"TUNGGAKAN IURAN KELAS X");
				// $num_temp=23;
			}
    	}

		for($i=0; $i < sizeof($row_data); $i++){
			// print_r($arr[(sizeof($header)*3)+$i+4+24]."<br>");
			$sheet->setCellValue($arr[(sizeof($header)*3)+$i+4+24].$row, '=SUM('.$arr[(sizeof($header)*3)+$i+4+24].$first_row.':'.$arr[(sizeof($header)*3)+$i+4+24].$last_row.')');
			//set footer
			$this->set_border_and_background_color($arr[(sizeof($header)*3)+$i+4+24].$first_row.':'.$arr[(sizeof($header)*3)+$i+4+24].$last_row, $sheet, $spreadsheet,"",true);
			//set body
			$this->set_border_and_background2($arr[(sizeof($header)*3)+$i+4+24].$row, $sheet, $spreadsheet,true);
		}
		//set height footer
		$sheet->getRowDimension($row)->setRowHeight(23);
		//set height header
		$sheet->getRowDimension(8)->setRowHeight(25);

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="laporan-tahunan.xls"');
		header('Cache-Control: max-age=0');

		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xls');

		$writer->save('php://output');

		exit;
 	}

	//laporan harian and mingguan
 	public function per_tanggal(){
 		$param="";
		$filterKeyword = $this->input->get('filter_keyword');;

		if(isset($filterKeyword)) {
			if($filterKeyword != ''){
				$param="1";
				$param = $param." AND "."real_tanggal_iuran"." LIKE "."'%".$filterKeyword."%'";
				$data_laporan=$this->Laporan->get_list_harian($param);
				$data_laporan_uniq=$this->Laporan->get_list_harian_uniq($param);
    		}
    	}else{
			$param="1";
			$new_value1=$this->input->get("filter_start_date");
			$new_value1=explode('/', $new_value1);
			$new_value1=date('Y-m-d', strtotime($new_value1[2]."-".$new_value1[0]."-".$new_value1[1])); 
			$new_value2=$this->input->get("filter_end_date");
			$new_value2=explode('/', $new_value2);
			$new_value2=date('Y-m-d', strtotime($new_value2[2]."-".$new_value2[0]."-".$new_value2[1]));
			$param = $param." AND "."real_tanggal_iuran"." BETWEEN "."'".$new_value1."'"." AND "."'".$new_value2."'";
			$data_laporan=$this->Laporan->get_list_harian($param);
			$data_laporan_uniq=$this->Laporan->get_list_harian_uniq($param);
    	}

		$html=[];
		$index=1;
		$no=1;
		$row=8;

		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('./template_harian.xlsx');
 		$spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
		// $spreadsheet->getDefaultStyle()->getFont()->setSize(20);

		$sheetCount = $spreadsheet->getSheetCount();

		// print_r($data_laporan_uniq);
		// die();
		// $tes=[];
		foreach ($data_laporan_uniq as $key_html => $value_laporan) {
		//foreach ($html as $key_html => $value_html) {
			//foreach ($data_laporan as $key => $value_laporan) {
			$name=$value_laporan->real_tanggal_iuran;
			$jumlah_name=$value_laporan->jumlah_record+8;
			// $tes[]=$value_laporan->jumlah_record+8;
			$date = new DateTime();
			//
			$tahun_ajaran=explode('-', $name);
			$tahun=$tahun_ajaran[0];
			$bulan=intval($tahun_ajaran[1]);
			$tanggal=intval($tahun_ajaran[2]);
			//
			$date->setDate($tahun, $bulan, $tanggal);
			$month = $date->format("m");
			if($month>6) $tahun_ajaran=$date->format("Y")."/".($date->format("Y")+1);
	        else $tahun_ajaran=($date->format("Y")-1)."/".$date->format("Y");
	        // $tahun_ajaran=$date;
				//if($value_html == $name){
			$sheet = clone $spreadsheet->getSheetByName('example');
			$sheet->setTitle(strval($name));
			$spreadsheet->addSheet($sheet);
			// $spreadsheet->createSheet();
			$spreadsheet->setActiveSheetIndex($index);

			$sheet = $spreadsheet->getActiveSheet();

			$sheet->getSheetView()->setZoomScale(140);
			$sheet->setShowGridlines(false);
			// $sheet->setTitle(strval($name));
			$sheet->setCellValue('A2', 'DPMP, IURAN DAN TABUNGAN BULANAN SISWA TAHUN PELAJARAN '.$tahun_ajaran);
			// $date = new DateTime();
			// $month = $date->format("m");

			// $origDate=$this->input->get('filter_keyword');
			$sheet->setCellValue('A4', 'HARI');
			$sheet->setCellValue('D4', ': '.$this->hari_indo($name));
			$sheet->setCellValue('A5', 'TANGGAL');
			$sheet->setCellValue('D5', ': '.$this->tanggal_indo($name));
			
			// membaca data body
	 		$no=1;
			$row=8;
			$row_tingkat_kelas_x=[];
			$row_tingkat_kelas_xi=[];
			$row_tingkat_kelas_xii=[];

			$arrayOsis=[];
			$tahun_sekarang=$date->format("Y");
			$selisih = $tahun_sekarang - 2;
			for($tahun_ajaran_i=$selisih; $tahun_ajaran_i <= $tahun_sekarang; $tahun_ajaran_i++){
				$arrayOsis[] = array(
					'tahun' => $tahun_ajaran_i,
					'tahun_ajaran' => $tahun_ajaran_i."/".($tahun_ajaran_i+1),
					'total_du' => 0
				);
	        }
	        // print_r($arrayName);
	        //
			foreach($data_laporan as $key => $value) {
				// print_r($name==$value->real_tanggal_iuran);
				// echo "<br/>";
				// print_r($key_html.':'.$name."==".$value->real_tanggal_iuran."<br/>");

				if($name==$value->real_tanggal_iuran){
					// if($row<$tes[$key_html]){
					// print_r($row);
					// echo "<br/>";
					if(explode(" ",$value->kelas)[0]=="X") {
						$row_tingkat_kelas_x[]=$row;
					}
					if(explode(" ",$value->kelas)[0]=="XI") {
						$row_tingkat_kelas_xi[]=$row;
					}
					if(explode(" ",$value->kelas)[0]=="XII") {
						$row_tingkat_kelas_xii[]=$row;
					}

					// print_r($value->keterangan_iuran);
					// die();
					if($value->keterangan_iuran!=""){
						$ta="";
						if($value->keterangan_iuran!="REGISTRASI_ULANG") $ta= explode(" ", $value->keterangan_iuran)[1];
						else $ta= $tahun_ajaran;
						foreach ($arrayOsis as $key_osis => $value_osis) {
							if($value_osis['tahun_ajaran']==$ta) {
								$arrayOsis[$key_osis]['total_du'] += $value->total_besar_iuran_du;
							}
						}
					}

					$spreadsheet->setActiveSheetIndex($index)
					->setCellValue('A'.$row, ($no))
					->setCellValue('B'.$row, $value->ref_siswa)
					->setCellValue('C'.$row, $value->nama_lengkap_siswa)
					->setCellValue('D'.$row, $value->kelas)
					->setCellValue('E'.$row, $value->bulan_iuran_bulanan)
					->setCellValue('F'.$row, $value->total_besar_iuran_bulanan)
					->setCellValue('G'.$row, $value->bulan_iuran_tabungan)
					->setCellValue('H'.$row, $value->total_besar_iuran_tabungan)
					->setCellValue('I'.$row, $value->total_besar_iuran_dpmp)
					->setCellValue('J'.$row, $value->total_besar_iuran_du)
					->setCellValue('K'.$row, '=SUM(F'.$row.')+SUM(H'.$row.':J'.$row.')')
					->setCellValue('L'.$row, '')
					// ->setCellValue('M'.$row, $value->keterangan_iuran."---".$value->real_tanggal_iuran)
					->setCellValue('M'.$row, $value->keterangan_iuran)
					;
					//set format currency money
					$arr = array('F','H','I','J','K','L','M');
					foreach ($arr as $key => $value) {
						$sheet->getStyle($value.$row)->getNumberFormat()
						->setFormatCode('#,##0');
					}

					//set border
					$sheet->getStyle('A'.$row.':'.'M'.$row)->applyFromArray(
					   array(
					      'fill' => array(
					          'type' => Fill::FILL_SOLID,
					          'color' => array('rgb' => '963634' )
					      ),
					      'borders' => array(
							'allBorders' => [
							'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
							],
					      ),
					      'font' => array(
					      ),
					      'alignment' => array(
					      		'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
					      		'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					      )
					   )
				 	);

					$col_to_right = ["F","H","I","J","K","L","M"];
					foreach ($col_to_right as $key_col_right => $value_col_right) {
						// print_r($value_col_right);
						$sheet->getStyle($value_col_right.$row)
			    	->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
					}

				    $sheet->getStyle('C'.$row)
			    	->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
					// print_r($key_html."=".$no.":".$row."<br/>");
					$row++;
					$no++;
					// }
				}
			}

			$style_head_foot = array(
			      'fill' => array(
			          'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
			          'color' => array('rgb' => '963634'),
			          'startColor' => array('rgb' => '963634' ),//warna background
			      ),
			      'borders' => array(
					'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					// 'color' => ['argb' => 'FFFF0000'],
					],
			      ),
			      'font' => array(
			          'bold'  =>  true,
			          'color' => array('argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE)
			          // 'size' => 24
			      ),
			      'alignment' => array(
			      		'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			      		'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
			      )
		   );
		   $style_body = array(
			      'borders' => array(
					'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					// 'color' => ['argb' => 'FFFF0000'],
					],
			      ),
			      'font' => array(
			          // 'bold'  =>  true,
			          // 'color' => array('argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE)
			          // 'size' => 24
			      ),
			      'alignment' => array(
			      		// 'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			      		// 'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
			      )
		   );

			//
			$sheet->setCellValue('A'.$row, 'JUMLAH');
			$sheet->setCellValue('F'.$row, '=SUM(F8:F'.($row-1).')');
			$sheet->setCellValue('G'.$row, '');
			$sheet->setCellValue('H'.$row, '=SUM(H8:H'.($row-1).')');
			$sheet->setCellValue('I'.$row, '=SUM(I8:I'.($row-1).')');
			$sheet->setCellValue('J'.$row, '=SUM(J8:J'.($row-1).')');
			$sheet->setCellValue('K'.$row, '=SUM(K8:K'.($row-1).')');
			$sheet->setCellValue('K5', '=K'.($row));
			$sheet->setCellValue('L'.$row, '');
			$sheet->setCellValue('M'.$row, '');

			$sheet->mergeCells('A'.$row.':E'.$row);

			$sheet->getStyle('A'.$row.':'.'M'.$row)->applyFromArray($style_head_foot);


		    $arr=array("F","G","H","I","J","K");
		    foreach ($arr as $key => $value) {
				$sheet->getStyle($value.$row)->getNumberFormat()
				->setFormatCode('#,##0');
			    $sheet->getStyle($value.$row)
		    	->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
		    }
		    //
		    $ttd1 = array(
	        	array ('name' => 'Mengetahui'), 
	        	array ('name' => 'Bendahara'),  
	        );

	        $old_row = $row+2;
	        foreach ($ttd1 as $key_ttd1 => $value_ttd1) {
	        	$sheet->setCellValue('B'.$old_row, $value_ttd1['name']);
	        	$old_row++;
	        }

		    $ttd2 = array(
	        	array ('name' => 'Cucum Kulsum, S.Si'), 
	        	array ('name' => 'NIP. 198005032007012008'),  
	        );

	        $old_row = $row+8;
	        foreach ($ttd2 as $key_ttd1 => $value_ttd1) {
	        	$sheet->setCellValue('B'.$old_row, $value_ttd1['name']);
	        	$old_row++;
	        }
		    //
		    $old_row=$row+2;
			// $sheet->mergeCells('J6:J7');
			$sheet->setCellValue('F'.$old_row, 'REKAP IURAN DAN DPMP');
			$sheet->mergeCells('F'.$old_row.':H'.$old_row);
			$sheet->getStyle('F'.$old_row.':'.'H'.$old_row)->applyFromArray($style_head_foot);

			
			$sheet->setCellValue('J'.$old_row, 'REKAP TABUNGAN BULANAN');
			$sheet->mergeCells('J'.$old_row.':K'.$old_row);
			$sheet->getStyle('J'.$old_row.':'.'K'.$old_row)->applyFromArray($style_head_foot);

	        $data_jenis_iuran = array(
	        	array ('name' => 'Iuran Bulanan'), 
	        	array ('name' => 'DPMP'),  
	        );
	        $data_kelas = array(
	        	array ('kelas' => 'XII', 'total' => 0), 
	        	array ('kelas' => 'XI', 'total' => 0), 
	        	array ('kelas' => 'X', 'total' => 0), 
	        );

	        $old_row=$row+3;
	        foreach ($data_jenis_iuran as $key_1 => $value_1) {
		        foreach ($data_kelas as $key_2 => $value_2) {
		        	$dump=0;
					$sheet->setCellValue('F'.$old_row, $value_1['name']." Kelas ".$value_2['kelas']);
					$sheet->mergeCells('F'.$old_row.':G'.$old_row);

					if($value_1['name']=="Iuran Bulanan"){
						if($value_2['kelas']=="X"  && !empty($row_tingkat_kelas_x))
						$sheet->setCellValue('H'.$old_row, "=SUM(F".$row_tingkat_kelas_x[0].":"."F".$row_tingkat_kelas_x[count($row_tingkat_kelas_x)-1].")");
						else if($value_2['kelas']=="XI"  && !empty($row_tingkat_kelas_xi))
						$sheet->setCellValue('H'.$old_row, "=SUM(F".$row_tingkat_kelas_xi[0].":"."F".$row_tingkat_kelas_xi[count($row_tingkat_kelas_xi)-1].")");
						else if($value_2['kelas']=="XII"  && !empty($row_tingkat_kelas_xii))
						$sheet->setCellValue('H'.$old_row, "=SUM(F".$row_tingkat_kelas_xii[0].":"."F".$row_tingkat_kelas_xii[count($row_tingkat_kelas_xii)-1].")");
						else $sheet->setCellValue('H'.$old_row, "0");
					}

					if($value_1['name']=="DPMP"){
						if($value_2['kelas']=="X"  && !empty($row_tingkat_kelas_x))
						$sheet->setCellValue('H'.$old_row, "=SUM(I".$row_tingkat_kelas_x[0].":"."I".$row_tingkat_kelas_x[count($row_tingkat_kelas_x)-1].")");
						else if($value_2['kelas']=="XI"  && !empty($row_tingkat_kelas_xi))
						$sheet->setCellValue('H'.$old_row, "=SUM(I".$row_tingkat_kelas_xi[0].":"."I".$row_tingkat_kelas_xi[count($row_tingkat_kelas_xi)-1].")");
						else if($value_2['kelas']=="XII"  && !empty($row_tingkat_kelas_xii))
						$sheet->setCellValue('H'.$old_row, "=SUM(I".$row_tingkat_kelas_xii[0].":"."I".$row_tingkat_kelas_xii[count($row_tingkat_kelas_xii)-1].")");
						else $sheet->setCellValue('H'.$old_row, "0");
					}

					$sheet->getStyle('H'.$old_row)->getNumberFormat()
					->setFormatCode('#,##0');

					$sheet->getStyle('F'.$old_row.':'.'H'.$old_row)->applyFromArray($style_body);

					$old_row++;
		        }
	        }
	        //
	        $sheet->setCellValue('F'.$old_row, 'JUMLAH');
			$sheet->mergeCells('F'.$old_row.':G'.$old_row);

			$sheet->setCellValue('H'.$old_row, '=SUM(H'.($row+3).':H'.($old_row-1).')');
			$sheet->getStyle('H'.$old_row)->getNumberFormat()
			->setFormatCode('#,##0');
			$sheet->getStyle('F'.$old_row.':'.'H'.$old_row)->applyFromArray($style_head_foot);
	        //
	        $old_row=$row+3;
			$sheet->setCellValue('J'.$old_row, "Kelas X");
			if(!empty($row_tingkat_kelas_x))
			$sheet->setCellValue('K'.$old_row, "=SUM(H".$row_tingkat_kelas_x[0].":"."H".$row_tingkat_kelas_x[count($row_tingkat_kelas_x)-1].")");
			else $sheet->setCellValue('K'.$old_row, 0);
			$sheet->getStyle('K'.$old_row)->getNumberFormat()
			->setFormatCode('#,##0');

			//
			$sheet->getStyle('J'.$old_row.':'.'K'.$old_row)->applyFromArray($style_body);

	        $old_row=$row+5;
	        $sheet->setCellValue('J'.$old_row, 'OSIS');
			$sheet->mergeCells('J'.$old_row.':K'.$old_row);
			$sheet->getStyle('J'.$old_row.':'.'K'.$old_row)->applyFromArray($style_head_foot);

			$old_row=$row+6;
	        foreach ($arrayOsis as $key_osis => $value_osis) {
	        	$sheet->setCellValue('J'.$old_row, $value_osis['tahun_ajaran']);
				$sheet->setCellValue('K'.$old_row, $value_osis['total_du']);
	      
		        $sheet->getStyle('K'.$old_row)->getNumberFormat()
				->setFormatCode('#,##0');
				$sheet->getStyle('J'.$old_row.':'.'K'.$old_row)->applyFromArray($style_body);
				$old_row++;
			}
	        $sheet->setCellValue('J'.$old_row, 'JUMLAH');

			$sheet->setCellValue('K'.$old_row, '=SUM(K'.($row+6).':'.'K'.($old_row-1).')');
			$sheet->getStyle('K'.$old_row)->getNumberFormat()
			->setFormatCode('#,##0');
			$sheet->getStyle('J'.$old_row.':'.'K'.$old_row)->applyFromArray($style_head_foot);
			$index++;
		}
		// die();
		if(isset($filterKeyword)) {
			// $sheet = $spreadsheet->getActiveSheet();
			// $sheet->setTitle(' 1 ');
			// $spreadsheet->removeSheetByIndex($sheetCount);
			header('Content-Disposition: attachment;filename="laporan_harian"'.date("Y-m-d H:i:s").'".xls"');	
		}else{
			header('Content-Disposition: attachment;filename="laporan_mingguan"'.date("Y-m-d H:i:s").'".xls"');	
			// $spreadsheet->removeSheetByIndex($sheetCount - 1);
		}
		
		header('Content-Type: application/vnd.ms-excel');
		header('Cache-Control: max-age=0');

		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xls');

		ob_end_clean();
		$writer->save('php://output');

		exit;
 	}

 	public function export(){
 		$param="";
		$filterKeyword = $this->input->get('filter_keyword');

		if(isset($filterKeyword)) {
			if($filterKeyword != ''){
				$param= $this->input->get("filter_keyword");
				$param= array(
					'tanggal_iuran' => $param
				);
				$param="1";
				$param = $param." AND "."tanggal_iuran"." LIKE "."'%".$this->input->get("filter_keyword")."%'";
    			// print_r($param);
    			$data_laporan =$this->Laporan->get_list_table($param);
    		}
    	}
    	//
		foreach ($data_laporan as $key => $value) {
    		$param="1";
	    	$param = $param." AND "."id_siswa_iuran"." = "."'".$value->id_siswa."'";
	    	$param = $param." AND "."jenis_iuran"." = "."'IURAN_PER_BULAN'";
			$param = $param." AND "."tanggal_iuran"." LIKE "."'%".$this->input->get("filter_keyword")."%'";	
    		// print_r($param);
			$data_iuran =$this->Iuran->get_list_table($param);
			$value->total_bulan=$data_iuran;
    	}
    	//
		$param="1";
		$param = $param." AND "."tanggal_iuran"." LIKE "."'%".$this->input->get("filter_keyword")."%'";	
		$data_iuran =$this->Iuran->get_list_table($param);

    	//
 		$data = $this->Laporan->get_list_table($param);
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();
		$activeSheet2 = $spreadsheet->getActiveSheet();
		// Set document properties
		$spreadsheet->getProperties()->setCreator('Andoyo - Java Web Media')
		->setLastModifiedBy('Andoyo - Java Web Medi')
		->setTitle('Office 2007 XLSX Test Document')
		->setSubject('Office 2007 XLSX Test Document')
		->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
		->setKeywords('office 2007 openxml php')
		->setCategory('Test result file');

		// Add some data
		// for ($i=1; $i < 11; $i++) { 
			$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A6', 'NO')
			->setCellValue('B6', 'REF')
			->setCellValue('C6', 'NAMA')
			->setCellValue('D6', 'KELAS')
			->setCellValue('E6', 'BULAN')
			->setCellValue('F6', 'SPP')
			->setCellValue('G6', 'DPMP')
			->setCellValue('H6', 'DU')
			->setCellValue('I6', 'TOTAL')
			// ->setCellValue('J6', 'SISA DSP')
			->setCellValue('J6', 'KET');

			$activeSheet2->mergeCells('A6:A7');
			$activeSheet2->mergeCells('B6:B7');
			$activeSheet2->mergeCells('C6:C7');
			$activeSheet2->mergeCells('D6:D7');
			$activeSheet2->mergeCells('E6:E7');
			$activeSheet2->mergeCells('F6:F7');
			$activeSheet2->mergeCells('G6:G7');
			$activeSheet2->mergeCells('H6:H7');
			$activeSheet2->mergeCells('I6:I7');
			$activeSheet2->mergeCells('J6:J7');


		// }
		// Miscellaneous glyphs, UTF-8
		$no=1;
		$i=8; foreach($data_laporan as $key => $value) {
			$total=($value->spp+$value->dpmp+$value->du);
			$monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	        $a=implode(", ",$monthNameList);
	        
	        $dump="";
        	foreach($value->total_bulan as $keys => $values) {	
	            $str = $values->bulan_iuran;
	            $str=explode(" ",$str);
	            // stripos($string, $substring);
	            $str = strpos($a, $str[0]);
	            $dump.=",".$str;
	            // $dump[]=($monthNameList.indexOf($str[0])+1);
	        }
	        if($value->total_bulan != FALSE){
				$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A'.$i, ($no))
				->setCellValue('B'.$i, $value->ref_siswa)
				->setCellValue('C'.$i, $value->nama_lengkap_siswa)
				->setCellValue('D'.$i, $value->kelas)
				->setCellValue('E'.$i, substr($dump, 1))
				->setCellValue('F'.$i, $value->spp)
				->setCellValue('G'.$i, $value->dpmp)
				->setCellValue('H'.$i, $value->du)
				->setCellValue('I'.$i, $total)
				->setCellValue('J'.$i, $value->tahun_ajaran)
				;
				$i++;
				$no++;

				$activeSheet2->getStyle('A6:J6')->applyFromArray(
				   array(
				      'fill' => array(
				          'type' => Fill::FILL_SOLID,
				          'color' => array('rgb' => '963634' )
				      ),
				      'font'  => array(
				          'bold'  =>  true,
				      ),
				      'alignment' => array(
				      		'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				      		'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				      ),
					    'borders' => array(
					        'allborders' => array(
					            'style' => Border::BORDER_THICK,
					            'color' => array('argb' => '000000'),
					        ),
					    ),
				   )
				 );

				// $sheet=$spreadsheet->getActiveSheet();
				// $sheet->getStyle('A6:J6')->getFill()
			 //    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			 //    ->getStartColor()->setRGB('963634')
			 //    ;
			    // $sheet->getStyle('A6:J6')
    			// ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);


			    // $sheet->getStyle('B6:J6')
    			// ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_GREEN);

				// $sheet->getStyle('A3')
				//     ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

			 //    $sheet->getStyle('B6:J6')
				// 	->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
				// $sheet->getStyle('B6:J6')
				// 	->getFill()->getStartColor()->setARGB('FFFF0000');
				// $sheet->getStyle('A6')
    // 			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
				// ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
			}
		}
		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));
		$activeSheet = $spreadsheet->getActiveSheet();

		// $activeSheet->getDefaultStyle();
		$activeSheet->setTitle('PEMBAYARAN');
		// $activeSheet->getDefaultStyle()->getFont()->setName('Times New Roman');
		$activeSheet->setCellValue('A1', 'PEMBAYARAN KEUANGAN');
		$activeSheet->getStyle("A1")->getFont()->setBold(true);
		// $activeSheet->getStyle("A1")->getFont()->setSize(16);
		$activeSheet->mergeCells('A1:I1');
		$activeSheet->getStyle("A1")->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
		->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


		$activeSheet->setCellValue('A2', 'SPP,DSP SISWA TAHUN PELAJARAN 2017-2018');
		$activeSheet->getStyle("A2")->getFont()->setBold(true);
		$activeSheet->mergeCells('A2:I2');
		$activeSheet->getStyle("A2")->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
		->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$colors = array(
		  '2'=>'00FF00',
		  '3'=>'FFA500',
		  '5'=>'8B0000'
		);

		$webstats = array(
			array(
				'Domain' => 'robgravelle.com',
				"Status" => "200 OK","Speed" => 0.57,
				"Last Backup"=>"2017-10-27",
				"SSL Certificate?"=>"No"
			),
		);

		//output headers
		$activeSheet->fromArray(array_keys($webstats[0]), NULL, 'A3');

		$activeSheet->getStyle('A3:E3')->applyFromArray(
		   array(
		      'fill' => array(
		          'type' => Fill::FILL_SOLID,
		          'color' => array('rgb' => 'E5E4E2' )
		      ),
		      'font'  => array(
		          'bold'  =>  true
		      ),
			    'borders' => array(
			        'outline' => array(
			            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
			            'color' => ['argb' => 'FFFF0000'],
			        ),
			    ),
			    // 'fill' => [
			    //     'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
			    //     'rotation' => 90,
			    //     'startColor' => [
			    //         'argb' => 'FFA0A0A0',
			    //     ],
			    //     'endColor' => [
			    //         'argb' => 'FFFFFFFF',
			    //     ],
			    // ],
		   )
		 );

		
		 
		foreach (range('B','E') as $col) {
		  $activeSheet->getColumnDimension($col)->setAutoSize(true);  
		}
		   
		$styleArray = array(
		    'borders' => array(
		        'allborders' => array(
		            'style' => Border::BORDER_THICK,
		            'color' => array('argb' => 'E5E4E2'),
		        ),
		    ),
		);
		$activeSheet->getStyle('A3:E3')->applyFromArray($styleArray);
		//
		// $styleArray = [
		//     'borders' => [
		//         'outline' => [
		//             'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
		//             'color' => ['argb' => 'FFFF0000'],
		//         ],
		//     ],
		// ];

		// $spreadsheet->getStyle('A2:B2')->applyFromArray($styleArray);
		//

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);


		$activeSheet2->getSheetView()->setZoomScale(400);
		

		// Redirect output to a client’s web browser (Xlsx)
		// header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="01simple.xls"');
		header('Cache-Control: max-age=0');

		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xls');

		$writer->save('php://output');

 		// die("string");
		exit;
 	}

 	//other
 	public function setStyle($col1, $col2, $sheet){
 		$font=$sheet->getStyle($col1);
		$font->getFont()->setBold(true);
		$font->getFont()->setSize(11);
		$font->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)
		->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		
		$sheet->getStyle($col1.':'.$col2)->applyFromArray(
		   array(
		      'fill' => array(
		          'type' => Fill::FILL_SOLID,
		          'color' => array('argb' => '963634' )
		      ),
		      'borders' => array(
				'bottom' => [
				'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
				],
		      ),
		   )
	 	);
 	}

 	
 	public function set_border_and_background_color($col_idx, $sheet, $spreadsheet, $color, $is_numeric){
 		if(!isset($col_idx)  || empty($col_idx)) $col_idx=0;
	    $arr_style=array(
		      'borders' => array(
				'allBorders' => [
				'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				// 'color' => ['argb' => 'FFFF0000'],
				],
				'alignment' => array()
		      ),
		   );
	    if($color!=''){
	    	$arr=array(
			      'fill' => array(
			          'type' => Fill::FILL_SOLID,
			          'color' => array('argb' => $color )
			      )
			  );
	    	array_push($arr_style, $arr);
	    }
	    $arr_align=array();
 		if($is_numeric){
 			$arr_align=array(
		      		'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
		      		'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
		  	);
 			$sheet->getStyle($col_idx)
		 	->getNumberFormat()
	    	->setFormatCode('#,##0');
 		}else{
 			$arr_align=array(
		      		'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
		      		'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
		     );
 		}
 		$arr_style['alignment']=$arr_align;

 		$sheet->getStyle($col_idx)->applyFromArray($arr_style);
 	}

 	public function set_border_and_background($col_idx, $sheet, $spreadsheet){

 		if(!isset($col_idx)  || empty($col_idx)) $col_idx=0;
 		//set backgorund
 		$sheet->getStyle($col_idx)->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setRGB('963634');

	    $spreadsheet->getActiveSheet()->getStyle($col_idx)
	    ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

 		$sheet->getStyle($col_idx)->applyFromArray(
		   array(
		      'fill' => array(
		          'type' => Fill::FILL_SOLID,
		          'color' => array('argb' => '963634' )
		      ),
		      'borders' => array(
				'allBorders' => [
				'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				// 'color' => ['argb' => 'FFFF0000'],
				],
		      ),
		      'font' => array(
		          'bold'  =>  true,
		          // 'size' => 24
		      ),
		      'alignment' => array(
		      		'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
		      		'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
		      )
		   )
	 	);
 	}

 	public function set_border_and_background2($col_idx, $sheet, $spreadsheet, $is_numeric){

 		if(!isset($col_idx) || empty($col_idx)) $col_idx=0;

 		$sheet->getStyle($col_idx)->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()->setRGB('963634');

	    $spreadsheet->getActiveSheet()->getStyle($col_idx)
	    ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

	    $arr_style = array(
		      'borders' => array(
				'allBorders' => [
				'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				// 'color' => ['argb' => 'FFFF0000'],
				],
		      ),
		      'font' => array(
		          'bold'  =>  true,
		          // 'size' => 24
		      ),
		      'alignment' => array()
		   );

	    // print_r($is_numeric);
	    $arr_align=[];
 		if($is_numeric){
	    	$arr_align=array(
		      		'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
		      		'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
		  	);

		 	$sheet->getStyle($col_idx)
		 	->getNumberFormat()
	    	->setFormatCode('#,##0');
    	}else{
	    	$arr_align=array(
		      		'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
		      		'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
		  	);
    	}
    	$arr_style['alignment']=$arr_align;

    	// print_r($arr_style);
 		$sheet->getStyle($col_idx)->applyFromArray($arr_style);
    	// die();
 	}

	function check_year_now($tahun_ajaran, $kelas){
		// $tahun_ajaran=$this->input->get('tahun_ajaran');
		// $kelas=$this->input->get('kelas');
		$date = new DateTime();

		$tahun_p=$tahun_ajaran;
		$tahun_p=explode("/", $tahun_p);
		$date->setDate($tahun_p[1], 7, 10);


		$obj=[];
		$angkatan = new DateTime();
		if($kelas=="XII"){
			$angkatan->setDate($tahun_p[0]-2, 7, 10);
			
			$obj['kelas'][] = "X";
			$obj['kelas'][] = "XI";
			$obj['kelas'][] = $kelas;
		}else if($kelas=="XI"){
			$angkatan->setDate($tahun_p[0]-1, 7, 10);
			$obj['kelas'][] = "X";
			$obj['kelas'][] = $kelas;
		}else if($kelas=="X"){
			$angkatan->setDate($tahun_p[0], 7, 10);
			$obj['kelas'][] = $kelas;
		}else{
			echo 400;
			die();
		}
		
		$selisih = $angkatan->format("Y")+$date->format("Y")-$angkatan->format("Y");

		$month = $date->format("m");
        if($month>=6) $date=$date->format("Y")."/".($date->format("Y")+1);
        else $date=($date->format("Y")-1)."/".$date->format("Y");
		//
		$get_year=[];
		$get_years=[];
		$index=0;
		for($tahun_ajaran_i=$angkatan->format("Y"); $tahun_ajaran_i < $selisih; $tahun_ajaran_i++){
            // if($tahun_ajaran_i."/".($tahun_ajaran_i+1)!=$date){
                $get_year[]=$this->get_tahun_ajaran($tahun_ajaran_i."/".($tahun_ajaran_i+1));
                $get_years[]=$tahun_ajaran_i."/".($tahun_ajaran_i+1);
            // }
            if($tahun_ajaran_i."/".($tahun_ajaran_i+1)!=$date){
                $get_year_now[]=$this->get_tahun_ajaran($tahun_ajaran_i."/".($tahun_ajaran_i+1));
                $get_years_now[]=$tahun_ajaran_i."/".($tahun_ajaran_i+1);
            }
			$arrayName[] = array(
				'kelas' => $obj['kelas'][$index],
				'tahun' => $tahun_ajaran_i."/".($tahun_ajaran_i+1),
				'month_year' => $this->get_tahun_ajaran($tahun_ajaran_i."/".($tahun_ajaran_i+1))
			);
			$index++;
        }
		// print_r($get_year);
		// print_r($angkatan);
		// echo "<br/>";
		// print_r($date);
		// echo "<br/>";
		// print_r($selisih);
		return $arrayName;
	}

	public function get_tahun_ajaran($string_date){
        $tahun_ajaran = explode("/", $string_date);
        $new_month=[];
        $start = $month = strtotime($tahun_ajaran[0].'-07-10');
		$end = strtotime($tahun_ajaran[1].'-06-10');
		while($month <= $end)
		{
		     // echo date('F Y', $month), PHP_EOL;
		     $new_month[]=date('M Y', $month);
		     $month = strtotime("+1 month", $month);
		}
        return $new_month;
    }

    function tanggal_indo($tanggal)
	{
		$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
		$split = explode('-', $tanggal);
		return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	}

	function hari_indo($tanggal){
		// $hari = date ("D");
	 	$date= new DateTime();
		$split = explode('-', $tanggal);
		// return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
		$date->setDate($split[0], (int)$split[1], $split[2]);
		$hari=$date->format("D");

		switch($hari){
			case 'Sun':
				$hari_ini = "Minggu";
			break;
	 
			case 'Mon':			
				$hari_ini = "Senin";
			break;
	 
			case 'Tue':
				$hari_ini = "Selasa";
			break;
	 
			case 'Wed':
				$hari_ini = "Rabu";
			break;
	 
			case 'Thu':
				$hari_ini = "Kamis";
			break;
	 
			case 'Fri':
				$hari_ini = "Jumat";
			break;
	 
			case 'Sat':
				$hari_ini = "Sabtu";
			break;
			
			default:
				$hari_ini = "Tidak di ketahui";		
			break;
		}
	 
		return $hari_ini;//"<b>" . $hari_ini . "</b>";
	 
	}
 }

class QueryParameter {
	var $locale = "en_US";
	var $clause = "1";
	var $innerClause = "1";
	var $values = "";
	var $order = "";
	var $group = "";
	var $limit = 100;
	var $offset = 0;

	function getClause(){
		return $this->clause;
	}

	function setClause($val){
		$this->clause = $val;
	}

	function getOrder(){
		return $clause;
	}

	function setOrder($val){
		$this->clause = $val;
	}

	function getLimit(){
		if($this->limit > 200) return 200;
		return $limit;
	}

	function setLimit($val){
		$this->limit = $val;
	}
	//start offset

	//end offset

	function getValues(){
		return $values;
	}

	function setValues($val){
		$this->values = $val;
	}
} 
