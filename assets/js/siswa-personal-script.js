	var path = ctx + 'SiswaController';
    var path_jenis_iuran = ctx + 'JenisIuranController'; 
    var path_kelas = ctx + 'KelasController'; 
    var params='';
    var selected_action='';
    var selected_id='';
    var list_iuran = [];
    var jumlah_siswa=0;
    var kelas=0;
    var selected_all=true;
    var selected_key='';
    var list_obj=[];
    var list_duplicate=[];

    function init() {
        
        getSelectTahun();
        display(1);
        getSelectAction();

        $('#search-form').submit(function(e){
            display();
            e.preventDefault();
        });

        $('#check_all').click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
            if($(this).is(':checked')){
                $('#multi-action').prop("disabled", false);
            }else{
                $('#multi-action').prop("disabled", true);
            }
        });
        $(document).on('click', '#tbl-data-kelas tr', function() {
           $("#tbl-data-kelas tr").removeClass("bg-info");
           $(this).toggleClass("bg-info");
        });

        $('#multi-action').on('change',function(){
            if (this.value==2 || this.value==3 || this.value==4) {
                doAction();
            }else{
                $('#btn-apply-action').show();
                $('#btn-batal-action').show();
                $(this).hide();
                if(this.value==1) goToEdit(true);
                else {
                    $('#btn-apply-action').hide();
                    $('#btn-batal-action').hide();
                    $(this).show(); 
                }
            }
        });

        $('#btn-batal-action').click(function(){
            $('#btn-apply-action').hide();
            $('#btn-batal-action').hide();
            //
            // goToEdit(false);
            display();
            //
            $('#multi-action').val("");
            $('#multi-action').show();
        });

        $('#btn-apply-action').click(function(){
            save();
        });
    };
    function getSelectAction(){
        var status_action = ["Edit", "Hapus"];
        if($('[name=filter_status_aktif]').val()==0){
            status_action.push("Aktifkan");
        }
        if($('[name=filter_status_aktif]').val()==1){
            status_action.push("Non Aktifkan");
        }
        $('#multi-action').html(new Option("Choose Action...",0));
        $.each(status_action, function(key){
            if(this=="Non Aktifkan") $('#multi-action').append(new Option(this,(key+2)));
            else
            $('#multi-action').append(new Option(this,(key+1)));
        });
    }
    function setChecked(elem){
        if($(elem).is(':checked')){
            $('#multi-action').prop("disabled", false);
        }else{
            $('#multi-action').prop("disabled", true);
        }
    }
    function check_tahun_ajaran(){
        var date = new Date();
            // date = new Date("July 10, 2019");
        var month = date.getMonth();

        if(month>=6){
            date=date.getFullYear()+"/"+(date.getFullYear()+1);
        }else{
            date=(date.getFullYear()-1)+"/"+date.getFullYear();
        }
        return date;
    }
    function getSelectTahun() {
       $("#tahun_ajaran").empty();
        $("#tahun_ajaran").append(new Option('Semua Tahun', ''));
        var d = new Date();
        var n = d.getFullYear();
        var tahun = n - 5;
        for (var i = n; i>= tahun; i--) {
            $("#tahun_ajaran").append(new Option(i,i));
        }
    }

    function display(page = 1, params=''){
        $('#more-button-row').remove();
        var tbody = $("#tbl-data").find('tbody');
        // if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
        if($( "#tahun_ajaran" ).val() != ''){
            tbody.text('');
        }
        
        tbody.append('<tr id="loading-row"><td colspan="9"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        
        params = $( "#search-form" ).serialize();
        //jgn difilter kalao mau edit
        // params += "tahun_ajaran_siswa="+$( "#tahun_ajaran" ).val();//+"&tingkat_kelas="+$("#tbl-data-kelas").find('tbody tr.bg-info').data('tingkat');
        ajaxGET(path + '/list_table?page='+page+'&'+params,'onGetListSuccess','onGetListError');
    }//list_siswa_kelas_table_new

    function onGetListSuccess(response){
        console.log(response);
        $('#loading-row').remove();
        $('#no-data-row').remove();
        
        var tbody = $("#tbl-data").find('tbody');
        
        var row = "";
        var num = $('.data-row').length+1;
        $.each(response.data,function(key,value){
            row += render_row(value, num);
            num++;
        });
        row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="9"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
        $('#info-tabel').html("Total data : <span>"+response.data.length+"</span>");
    }

    function render_row(value, num){
    //  console.log(value);
        var row = "";
            row += '<tr contenteditable="false" class="data-row" id="row-'+value.id_siswa+'">';
            row+='<td align="centesr">';
            row+='<div class="form-check">';//onclick="cancelToEdit(this);"
                row+='<input onclick="setChecked(this)" class="form-check-input" style="position:unset" type="checkbox" value='+value.id_siswa+' name="id_siswa[]" data-tahun-angkatan='+value.tahun_angkatan_siswa+'>';
            row+='</div>';
            row+='</td>';
            // row += '<td>'+(num)+'</td>';
            row += '<td class="" data-original-html=\''+value.ref_siswa+'\'>'+value.ref_siswa+'</td>';
            // row += '<td class="">'+value.nis_siswa+'</td>';
            row += '<td class="" data-original-html=\''+value.nama_lengkap_siswa+'\'>'+value.nama_lengkap_siswa+'</td>';
            row += '<td class="" data-original-html=\''+value.jenis_kelamin_siswa+'\'>'+value.jenis_kelamin_siswa+'</td>';
            row += '<td class="">'+value.tahun_angkatan_siswa+'</td>';
            row += '<td class="" data-original-html=\''+value.keterangan_masuk_siswa+'\'>'+value.keterangan_masuk_siswa+'</td>';
            // row += '<td class="">'+"-"+'</td>';
            // row += '<td class="">';
            // // row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-info pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'edit\')"><i class="fas fa-pencil-alt"></i></a> ';
            // row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-primary pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'delete\')"><i class="fas fa-search"></i></a>';
            // row += '</td>';
            row += '</tr>';
        return row;
    }

    

    function onGetListError(response) {
        alert("ERROR");
        $('#ui-view').html($('#ui-view-msg-error').html());
    }

    function save(){
        // var obj = new FormData(document.querySelector('#form'));
        // if(selected_id != '') obj.append('id', selected_id);
        // obj.append('kelas', $('#tingkat').val()+' '+$('#jurusan').val()+' '+$('#sub_tingkat').val());
        // obj.append('list_iuran', JSON.stringify(list_iuran[0]));
        
        // ajaxPOST(path + '/save',obj,'onActionSuccess','onSaveError');
        var obj = new FormData(document.querySelector("#form-data"));
        
        // var arr_for_obj=[];
        //   var arrayData, objectData;
        //   arrayData = $('#form-data').serializeArray();
        //   objectData = {};

        //   $.each(arrayData, function() {
        //     var value;

        //     if (this.value != null) {
        //       value = this.value;
        //     } else {
        //       value = '';
        //     }

        //     if (objectData[this.name] != null) {
        //       if (!objectData[this.name].push) {
        //         objectData[this.name] = [objectData[this.name]];
        //       }

        //       objectData[this.name].push(value);
        //     } else {
        //       objectData[this.name] = value;
        //     }
        //   });
        // console.log(objectData);
        // var k=0;
        // $.each(objectData, function(key){
        //     // console.log(objectData['id_siswa[]'][k]);
        //     obj = {
        //         "id_siswa" : objectData['id_siswa[]'][k]
        //     }
        //     arr_for_obj.push(obj);
        //     k++;
        // })
        // console.log(arr_for_obj);
        ajaxPOST(path + '/update_all',obj,'onActionSuccess','onSaveFailed');
    }

    function onSaveFailed(response){
        console.log(response);
        // $('#msg-info-body').html('Data telah berhasil di update.');
        // $('#msg-info').modal('show');
        // setTimeout(function(){ window.location.href="pengguna?vmenu=daftar"; }, 1000);
        // Swal.fire({
        //   type: 'error',
        //   title: 'Message',
        //   text: response.responseJSON.message,
        //   //footer: '<a href>Why do I have this issue?</a>'
        //   showCancelButton: false,
        //   confirmButtonColor: '#3085d6',
        //   cancelButtonColor: '#d33',
        //   confirmButtonText: 'Oke!'
        // })
        // .then((value) => {
        //     if(value.value){
        //         location.reload();
        //     }
        // });
        list_duplicate=response.responseJSON.data;
        // var active="";
        // $.each(list_duplicate, function(key){
        //     if(value.ref_siswa==this.ref_siswa) active="bg-danger";
        // });
        // $.each($('#tbl-data').find('tbody tr'), function(key_1,value_form){
        //     var id_row=$(this).find('td input').eq(0).val();
        //     $.each(list_duplicate, function(key_2,value_data){
        //         // if(key_1==key_2) 
        //             console.log($(value_form).find('td input').eq(0).val()+"=="+value_data.id);
        //         // if(key_1==key_2){
        //             if($(value_form).find('td input').eq(0).val()==value_data.id) $(value_form).addClass("bg-danger");
        //             // if($(value_form).find('td input').eq(0).val()!=value_data.id) 
        //             if($(value_form).find('td input').eq(0).val()!=value_data.id) $(value_form).removeClass("bg-danger");
        //         // }
        //     });
        // });

            $('.data-row').removeClass("bg-danger");
        $.each(list_duplicate, function(key_2,value_data){
            $('#row-'+value_data.id).addClass("bg-danger");
            // if($(value_form).find('td input').eq(0).val()==value_data.id) $(value_form).addClass("bg-danger");
            // if($(value_form).find('td input').eq(0).val()!=value_data.id) $(value_form).removeClass("bg-danger");
        });

    }


    function onSaveSuccess(response){
        console.log(response);
        $('#msg-info-body').html('Data telah berhasil di update.');
        $('#msg-info').modal('show');
        // setTimeout(function(){ window.location.href="pengguna?vmenu=daftar"; }, 1000);
    }
    //

    //
    function onActionSuccess(response){
        console.log(response);
        Swal.fire({
          type: 'success',
          title: 'Message',
          text: response.message,
          //footer: '<a href>Why do I have this issue?</a>'
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Oke!'
        })
        .then((value) => {
            if(value.value){
                location.reload();
            }
        });
    }

    function doAction(confirm = 0){
        var obj = new FormData(document.querySelector('#form-data'));
        if(confirm == 0) {
            var page=1;
            // obj=$('#form-data').serialize();

            // obj.append("id_siswa", JSON.stringify(obj_arr));
            $.each($('#form-data').serializeArray(), function(key){
                console.log(this);
                obj.append(this.name, this.value);
            });
            // ajaxGET(path + '/list_table?page='+page+'&'+obj,'onPrepareActionSuccess','onError');
            ajaxPOST(path + '/check_id',obj,'onPrepareActionSuccess','onError');
        }
        else {
            if($('#multi-action').val()==3){
                obj.append("status_aktif_siswa[]", 1);
            }else if($('#multi-action').val()==4){
                obj.append("status_aktif_siswa[]", 0);
            }
            if($('#multi-action').val()==2){
                ajaxPOST(path + '/delete_all/',obj,'onActionSuccess','onError');
            }else{
                ajaxPOST(path + '/do_action/',obj,'onActionSuccess','onError');
            }
        }
    }

    function onPrepareActionSuccess(response) {
        console.log(response);
        var value = response.data;
        var my_html="";
        my_html+="<p class='text-left font-weight-bold'>Anda yakin dengan data berikut:</p>";
        my_html+="<table class='table table-stripped table-sm text-left'>";
        my_html+="<tr class='bg-light'><td>NO</td><td>REF</td><td>NAMA</td></tr>"
        $.each(response.data, function(key){
            my_html+="<tr><td>"+(key+1)+"</td><td>"+this.ref_siswa+"</td><td>"+this.nama_lengkap_siswa+"</td></tr>"
        });
        my_html+="</table>";
        Swal.fire({
          // title: 'Mohon Periksa kembali..',
          html: my_html,
          // type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, submit it!'
        }).then((result) => {
          if (result.value) {
            doAction(1);
            // Swal.fire(
            //   'Deleted!',
            //   'Your file has been deleted.',
            //   'success'
            // )
            // display(1);
          }
        })
    }
    

    function fillFormValue(value){
        $('#form')[0].reset();
        
        $('[name=nis_siswa]').val(value.nis_siswa);
        $('[name=nama_siswa]').val(value.nama_siswa);
        $('[name=kelas_siswa]').val(value.kelas_siswa);
    }

	function add() {
        selected_id = ''; // wajib
        selected_action = 'add'; // wajib
	    $('#form')[0].reset(); // reset form on modals
	    $('.form-group').removeClass('has-error'); // clear error class
	    $('.help-block').empty(); // clear error string
	    $('#modal-form').modal('show'); // show bootstrap modal
	    $('.modal-title').text('Tambah Siswa'); // Set Title to Bootstrap modal title
        $('#form-pass').removeClass("d-none");

	}

    function getTahunAjaranAll(){
        var strdate=[];
        var angkatan= new Date("July 10, 2017");
        // var date = new Date("December 10, 2019");//+tahun_ajaran[0]
        var date = new Date();
        var selisih=(date.getFullYear()-angkatan.getFullYear());
        for(var start_ajaran=angkatan.getFullYear(); start_ajaran < angkatan.getFullYear()+selisih; start_ajaran++){
            if(start_ajaran+"/"+(start_ajaran+1)!=date){
                strdate.push(start_ajaran+"/"+(start_ajaran+1).toString());
                $('#tahun_ajaran').append(new Option(start_ajaran+"/"+(start_ajaran+1).toString(), start_ajaran+"/"+(start_ajaran+1).toString()));
            }
        }
        return strdate;
    }

    function filter(params){
        display(1, params);

        getSelectAction();
    }



    function goToEdit(mode){

        var row_is_check = $('#tbl-data tbody').find('input:checkbox');//$('#tbl-data').find('input:checked');
        // console.log(id);
        // var arr=[];
        $.each(row_is_check, function(key){
            
            console.log($(this));
            var parentElement = $(this)[0].parentElement.parentElement.parentElement;
            // console.log($(this)[0].parentElement.parentElement.parentElement);
            // parentElement.cells[1].innerHTML="<input style='border-radius:0px;width:100%' type='text' value=\""+parentElement.cells[1].textContent+"\">";
            if(mode==true){
                $(this).hide();
                parentElement.cells[1].innerHTML="<input name='ref_siswa[]' style='border-radius:0px;width:100%' type='text' value=\""+parentElement.cells[1].textContent+"\">";
                parentElement.cells[2].innerHTML="<input name='nama_lengkap_siswa[]' style='border-radius:0px;width:100%' type='text' value=\""+parentElement.cells[2].textContent+"\">";
                parentElement.cells[3].innerHTML="<select value="+parentElement.cells[3].textContent+" name='jenis_kelamin_siswa[]' style='border-radius:0px;width:100%'><option "+(parentElement.cells[3].textContent=="LAKI_LAKI"?"selected":"")+" value='LAKI_LAKI'>LAKI-LAKI</option><option "+(parentElement.cells[3].textContent=="PEREMPUAN"?"selected":"")+" value='PEREMPUAN'>PEREMPUAN</option></select>";
                parentElement.cells[5].innerHTML="<input name='keterangan_masuk_siswa[]' style='border-radius:0px;width:100%' type='text' value=\""+parentElement.cells[5].textContent+"\">";
            }else if(mode==false){
                $(this).show();
                parentElement.cells[1].innerHTML=parentElement.cells[1].dataset.originalHtml;
                parentElement.cells[2].innerHTML=parentElement.cells[2].dataset.originalHtml;
                parentElement.cells[3].innerHTML=parentElement.cells[3].dataset.originalHtml;
                parentElement.cells[5].innerHTML=parentElement.cells[5].dataset.originalHtml;
            }else{
                alert("Something Wrong..");
            }

            if($(this).is(':checked')==false){
                $(this)[0].parentElement.parentElement.parentElement.hidden=true;
                var id = $(this)[0].parentElement.parentElement.parentElement.id;
                $("#"+id).remove();
            }else{
                $(this)[0].parentElement.parentElement.parentElement.hidden=false;
            }
        });

        // if(arr=='') {
        //     var resp_msg={"title" : "Tidak dapat menambah data", "body" : "Karena anda belum memilih data siswa!", "icon"  : "warning"};
        //     showAlertMessage(resp_msg, 1500);
        // }else{
        //     var tahun_ajaran=id.data('tahun_ajaran');
        //     if(mode=='edit'){
        //         mode="&mode=edit";
        //     }
        //     window.location.href=ctx+'page/form_siswa_view?id='+arr.toString()+"&tahun_ajaran="+tahun_ajaran+mode;
        // }
        // var obj = new FormData(document.querySelector('#form-data'));
        // obj.append("list_obj", obj);
        // $('#form-data').attr('action', ctx+'page/form-siswa-naik-kelas_view');
        // $('#form-data').attr('list_obj', JSON.stringify(list_obj));
        // $('#form-data').trigger('submit');
    }

    function cancelToHide(){
        $.each($('[name="id[]"]'), function(key){
           if(thi.value!=this.value){
                $(this)[0].parentElement.parentElement.parentElement.hidden=true;
           }
        });
    }

    function cancelToEdit(element){
        var id_row=$(element)[0].parentElement.parentElement.parentElement;
        // console.log($('#'+id_row).find('td'));
        if($(element).is(':checked')==false){
            $('#'+id_row.id).find('td')[1].innerHTML=id_row.cells[1].children[0].value;
            $('#'+id_row.id).find('td')[2].innerHTML=id_row.cells[2].children[0].value;
        }
    }

    function goToNaikKelas(){
        Swal.fire({
          title: 'Pilih Kelas & Tanggal Registrasi',
          html: 
            '<select style="width:100%" id="kelas_baru" class="swal2-select"><option value disabled selected>Pilih Kelas Baru</select>' +
            '<input type="text" placeholder="Pilih Tanggal Registrasi" id="tanggal_registrasi" class="swal2-input form-control" autocomplete="off">',
          focusConfirm: false,
  showCancelButton: true,
  confirmButtonText: 'Submit',
  // showLoaderOnConfirm: true,
          onOpen: function() {
              $('#tanggal_registrasi').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 2017,
                maxYear: parseInt(moment().format('YYYY'),10),
                locale: {
                    format: 'YYYY-MM-DD'
                }
              }, function(start, end, label) {
                var years = moment().diff(start, 'years');
                // $("date").data('daterangepicker').setStartDate(data.startDate);
                // $("date").data('daterangepicker').setEndDate(data.startDate);
                //alert("You are " + years + " years old!");
                // console.log(start);
                // console.log(label);
                // console.log(this);

                // $(this).val(start.format('DD-MMM-YYYY'));
                // $(this).val(end.format('DD-MMM-YYYY'));
                // this.autoUpdateInput = true;
                // $(this).change();
              });

              $.each(list_obj, function(key, o){
                console.log(this);
                if($('[name=filter_kelas').val()=="X"){
                    if(o.tingkat=="XI") $('#kelas_baru').append(new Option(o.name, o.id));
                }
                if($('[name=filter_kelas').val()=="XI"){
                    if(o.tingkat=="XII") $('#kelas_baru').append(new Option(o.name, o.id));
                }
                if($('[name=filter_kelas').val()=="XII"){
                    // if(o.tingkat=="XII") options[o.id] = o.name;
                }
                

              });
        },
          preConfirm: () => {
            var kelas_baru = $('#kelas_baru').val();
            var tanggal_registrasi = $('#tanggal_registrasi').val();
            console.log(kelas_baru);
            // if (kelas_baru === null || tanggal_registrasi === null) {
            //     resolve('Kelas/Tanggal belum dipilih')
            // }else{
            //     $('[name=list_obj]').val(kelas_baru+","+tanggal_registrasi);
            //     // goToEdit();
            // }
            //return new Promise(function(resolve,reject) {
              if (kelas_baru==null) {
                $("#swal2-validation-message").show();

                $("#swal2-validation-message").text("Kelas belum dipilih");
                // reject('Kelas/Tanggal belum dipilih');
              }else{
                $('[name=list_obj]').val(kelas_baru+","+tanggal_registrasi);
                goToEdit();
              }
            //});
            // return;
          }
          // inputValidator: (value) => {
          //   return new Promise((resolve) => {
          //     if (value === '') {
          //       resolve('Kelas belum dipilih')
          //     }else{
          //       $('[name=list_obj]').val(value);
          //       goToEdit();
          //     }
          //   })
          // }
        })


        // if (fruit) {
        //   Swal.fire('You selected: ' + fruit)
        // }
        
    }

    function goToDelete(){
        var obj = new FormData(document.querySelector('#form-data'));
        ajaxPOST(path + '/delete_all/',obj,'onActionSuccess','onError');
    }
