	var path = ctx + 'IuranSiswaController';
    var params='';
    var selected_action='';
    var selected_id='';
    var selected_id_kelas=0;
    var selected_kelas_n_tahun_ajaran='';
    var tahun_ajaran='';
    var jenis_iuran='';
    var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    var obj_arr = [];       
    
    var lbl_total_biaya = 0;
    var lbl_nominal_dpmp = 0;
    var lbl_nominal_du = 0;
    var lbl_nominal_iuran = 0;

    function init() {
        selected_id = ($.urlParam('id') == null ? '' : $.urlParam('id'));
        tahun_ajaran = ($.urlParam('tahun_ajaran') == null ? '' : $.urlParam('tahun_ajaran'));

        //
        $('#modal-form').on('hidden.bs.modal', function (e) {
            $('#main-cont').attr('style', '');
        });

        $('#modal-form').on('shown.bs.modal', function (e) {
            $('#main-cont').attr('style', 'filter: blur(8px);-webkit-filter: blur(8px);');
        });
        
        $('#modal-form-msg').hide();
        
        //
        if(selected_id != '') {
            ajaxGET(path + '/check_pembayaran_manual/'+selected_id+'?tahun_ajaran='+tahun_ajaran,'onGetSuccess','onGetError');
        }

        $('#search-form').submit(function(e){
            displaySiswa();
            e.preventDefault();
        });
        $('[name=nominal_dpmp]').change(function(){
            lbl_nominal_dpmp=parseInt($(this).val().replace(/,/g, ''));
            $('#lbl_biaya_dpmp').html(": "+$.number(this.value,2));
            $('#lbl_total_biaya').text(": "+($.number(lbl_nominal_iuran+lbl_nominal_dpmp+lbl_nominal_du,2)));
        });
        $('[name=nominal_du]').change(function(){
            lbl_nominal_du=parseInt($(this).val().replace(/,/g, ''));
            $('#lbl_biaya_du').html(": "+$.number(this.value,2));
            $('#lbl_total_biaya').text(": "+($.number(lbl_nominal_iuran+lbl_nominal_dpmp+lbl_nominal_du,2)));

        });

        $('#btn-simpan').click(function(){
            obj_arr = []; 
            var obj ={}

            var data = $('#tbl-data').find('tbody tr');
            $.each(data,function(key){
                if($(this).find('td input:checked').length){
                    console.log($(this).data('id'));
                    console.log($(this).find('td'));
                    console.log($(this));
                    console.log($(this)[0].cells);
                    console.log("duit", $(this)[0].cells[0].children[0].children[0].value);
                    obj={
                        "id_siswa_iuran": selected_id,
                        "id_siswa_kelas_iuran": selected_id_kelas,
                        "bulan_iuran": $(this).data('bulan_iuran'),
                        "jumlah_iuran": "",
                        "besar_iuran": $(this)[0].cells[0].children[0].children[0].value,
                        "jenis_iuran": $(this).data('jenis_iuran'),
                        "keterangan_iuran": $(this).data('keterangan_iuran'),
                        "status_iuran": ""
                    }

                    obj_arr.push(obj);
                }
            });
            console.log(obj_arr);

            if(obj_arr.length>0) {
                save();
            }else{
                var resp_msg={"title" : "Tidak dapat menyimpan data", "body" : "Karena anda belum melakukan entri data apapun!", "icon"  : "warning"};
                showAlertMessage(resp_msg, 1200);
            } 
        });

        $(document).on('click', '.list-group li.list-group-item', function() {
          $(".list-group .collapse li.list-group-item").removeClass("active");
          // alert($(this).hasClass("active"));
          if($(this).hasClass("active")){
             $(this).html($(this).data('text'));
          }else{
            $(this).html('<i class="fa fa-check"></i> '+$(this).data('text'));
          }
          $(this).toggleClass("active");
          
        });
        $('[name=jenis_iuran]').change(function(){
            console.log(this.value);
            if(this.value!="DU_MANUAL") renderDisplayIuranBulanan();
            else $("#form-bulan-iuran").html('');
        });
    }

    function getSelectTahun() {
       $("[name=tahun_ajaran]").empty();
       $("[name=tahun_ajaran]").append(new Option('Pilih Tahun Ajaran Kelas', ''));
        var d = new Date();
        var n = d.getFullYear();
        var kelas = [];
        if(selected_kelas_n_tahun_ajaran.split(",")[0]=="XI"){
            kelas = ["X"];
        }else if(selected_kelas_n_tahun_ajaran.split(",")[0]=="XII"){
            kelas = ["XI","X"];
        }else{
            kelas = ["X"];
        }
        console.log(kelas);
        var key=0;
        for (var i = n; i>= selected_kelas_n_tahun_ajaran.split(",")[1]; i--) {
            if(i < n){
                $("[name=tahun_ajaran_kelas]").append(new Option(i+"/"+(i+1) +" - "+(kelas[key]), i+"/"+(i+1)+" - "+(kelas[key])));
                key++;
            }
        }
         $("[name=tahun_ajaran_kelas]").selectric();
        // $("#tahun_ajaran").val(check_tahun_ajaran()).trigger('change');
    }

    // function getSelectKelas() {
    //     $("[name=kelas]").empty();
    //     $("[name=kelas]").append(new Option('Pilih Kelas', ''));
    //     var kelas = [];
    //     if(selected_kelas_n_tahun_ajaran.split(",")[0]=="XI"){
    //         kelas = ["X"];
    //     }else if(selected_kelas_n_tahun_ajaran.split(",")[0]=="XII"){
    //         kelas = ["XI","X"];
    //     }
    //     $.each(kelas, function(key){
    //         // kelas.indexOf(selected_kelas_n_tahun_ajaran.split(",")[0])
    //         // if(kelas.indexOf(selected_kelas_n_tahun_ajaran.split(",")[0])){
    //             $("[name=kelas]").append(new Option(this, this));
    //         // }
    //     });
    //     $("[name=kelas]").selectric();
    // }
    function getTahunAjaranAll(){
        var strdate=[];
        var angkatan= new Date("July 10, 2017");
        // var date = new Date("December 10, 2019");//+tahun_ajaran[0]
        var date = new Date();
        var selisih=(date.getFullYear()-angkatan.getFullYear());
        for(var start_ajaran=angkatan.getFullYear(); start_ajaran < angkatan.getFullYear()+selisih; start_ajaran++){
            if(start_ajaran+"/"+(start_ajaran+1)!=date){
                strdate.push(start_ajaran+"/"+(start_ajaran+1).toString());
            }
        }
        return strdate;
    }
    function getTahunAjaranAllAndSet(tahun_angkatan){
        var strdate=[];
        var angkatan= new Date("July 10, "+tahun_angkatan);
        // var date = new Date("December 10, 2019");//+tahun_ajaran[0]
        var date = new Date();
        var selisih=(date.getFullYear()-angkatan.getFullYear());
        for(var start_ajaran=angkatan.getFullYear(); start_ajaran < angkatan.getFullYear()+selisih; start_ajaran++){
            if(start_ajaran+"/"+(start_ajaran+1)!=date){
                strdate.push(start_ajaran+"/"+(start_ajaran+1).toString());
            }
        }
        return strdate;
    }

    function onGetSuccess(resp){
        console.log(resp);
        var count_besar_dpmp=0;
        var count_sisa_dpmp=0;
        var count_besar_du=0;
        var count_sisa_du=0;
        var count_besar_iuran=0;

        var value_siswa = resp.data.data_siswa_kelas;
        var value_iuran = resp.data.data_iuran;

        selected_kelas_n_tahun_ajaran=value_siswa.tingkat_kelas+","+value_siswa.tahun_angkatan_siswa
        getSelectTahun();
        // getSelectKelas();
        $('[name=filter_keyword]').val(value_siswa.ref_siswa);
        $('#lbl_no_ref').html(": "+value_siswa.ref_siswa);
        $('#lbl_nama_lengkap').html(": "+value_siswa.nama_lengkap_siswa);
        $('#lbl_tahun_angkatan').html(": "+value_siswa.tahun_angkatan_siswa);
        $('#lbl_tanggal_registrasi').html(": "+value_siswa.tanggal_registrasi_siswa_kelas);
        
        $('#lbl_kelas').html(": "+value_siswa.kelas);
        $('#lbl_tahun_ajaran').html(": "+value_siswa.tahun_ajaran_siswa_kelas);

       
        if(resp.data.data_granted==true){
            renderDisplay(resp.data);
        }else{
            var tbody = $("#tbl-data").find('tbody');
            var value_data_granted=resp.data.data_granted;                
            tbody.html('<tr class="text-danger"><td colspan="10"><div class="list-group-item text-center"><span class="fas fa-warning"></span> <strong>'+value_data_granted.message+'</strong></div></td></tr>');
            $('#btn-add').remove();
        }
        $('#collapse-form-siswa').collapse('show');
        $('#collapse-form-biaya').collapse('show');
            
    }

    // function onSaveError(response){
    //   console.log(response);
    //   Swal.fire("Data Sudah Ada", response.responseJSON.message, 'warning');
    //   var resp_msg={"title" : "Message", "body" : response.responseJSON.message, "icon"  : "warning"};
    //   showAlertMessage(resp_msg, 1800);
    // }

    function onGetError(response, resp2, resp3){
        console.log(response);
        Swal.fire("Data Sudah Ada", response.responseJSON.message, 'warning');
        var resp_msg={"title" : "Message", "body" : response.responseJSON.message, "icon"  : "warning"};
        showAlertMessage(resp_msg, 5000);
    }

    function displaySiswa(){
        //$('#more-button-row').remove();
        //var tbody = $("#tbl-data").find('tbody');
        // if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
        //     tbody.text('');
        // }
        
        //tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        
        params = $( "#search-form" ).serialize();
        ajaxGET(path + '/list_table?page='+1+'&'+params,'onDisplaySiswaSuccess','onGetListError');
    }
    function onDisplaySiswaSuccess(resp){
        console.log(resp);
        console.log(resp.data.data_siswa);

            
        var count_besar_dpmp=0;
        var count_sisa_dpmp=0;
        var count_besar_du=0;
        var count_sisa_du=0;
        var count_besar_iuran=0;

        if(resp.data.data_siswa){
            var value_siswa = resp.data.data_siswa[0];
            window.location.href="iuran_manual_view?id="+value_siswa.id_siswa;
            // window.location.href="iuran_view?id="+value_siswa.id_siswa_kelas;
        }else{
            $('#lbl_no_ref').html(":");
            $('#lbl_nama_lengkap').html(":");
            $('#lbl_kelas').html(":");
            $('#lbl_tahun_ajaran').html(":");
            $('#lbl_tahun_angkatan').html(":");
            var resp_msg={"title" : "Data tidak ditemukan", "body" : "Mohon periksa kembali NO.REF anda", "icon"  : "warning"};
            showAlertMessage(resp_msg, 1200);
            // swal("Data tidak ditemukan","Mohon periksa kembali NO.REF anda","warning");
            // renderDisplayIuranBulanan();
            // //dpmp
            $('#lbl_besar_dpmp').html(": "+$.number(count_besar_dpmp,2));
            $('#lbl_sisa_dpmp').html(": "+$.number((count_besar_dpmp-count_sisa_dpmp),2));       
            // //du
            $('#lbl_besar_du').html(": "+$.number(count_besar_du,2));
            $('#lbl_sisa_du').html(": "+$.number((count_besar_du-count_sisa_du),2));  

            $('#collapse-form-siswa').collapse('hide');
            $('#collapse-form-biaya').collapse('hide');
        }
    }

    function get_tahun_ajaran_sekarang(){
        var tahun_ajaran = $('#lbl_tahun_ajaran').text().substring(2).split("/");
        var date = new Date("July 10, "+tahun_ajaran[0]);
        var endDate = new Date("June 10, "+tahun_ajaran[1]);
        var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        var new_month=[];
        for (var i=date; i <= endDate; date.setMonth(date.getMonth() + 1)){
            var stringDate = monthNameList[date.getMonth()] + " " + date.getFullYear();
            var jatuh_tempo = date.getFullYear() + "-" + ((date.getMonth()+1).toString().length==1?"0"+(date.getMonth()+1):date.getMonth()+1) + "-" + date.getDate();  
            // console.log(new Date(monthNameList[date.getMonth()]+" 10, "+date.getFullYear()));
            new_month.push(new Date(monthNameList[date.getMonth()]+" 10, "+date.getFullYear()));
        }
        return new_month;
    }
    function get_tahun_ajaran(string_date){
        console.log(string_date);
        var tahun_ajaran = string_date.split("/");
        var date = new Date("July 10, "+tahun_ajaran[0]);
        var endDate = new Date("June 10, "+tahun_ajaran[1]);
        var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        var new_month=[];
        for (var i=date; i <= endDate; date.setMonth(date.getMonth() + 1)){
            var stringDate = monthNameList[date.getMonth()] + " " + date.getFullYear();
            var jatuh_tempo = date.getFullYear() + "-" + ((date.getMonth()+1).toString().length==1?"0"+(date.getMonth()+1):date.getMonth()+1) + "-" + date.getDate();  
            // console.log(new Date(monthNameList[date.getMonth()]+" 10, "+date.getFullYear()));
            new_month.push(new Date(monthNameList[date.getMonth()]+" 10, "+date.getFullYear()));
        }
        return new_month;
    }

    function renderDisplay(resp){
        console.log(resp);
        var tbody = $("#tbl-data").find('tbody');
        var row="";
        var new_arr=[];
        
        var tgl_regis= $('#lbl_tanggal_registrasi').text().split("-")[1]-1;
        var resp_data;
        if(resp.temp==true) resp_data=resp.data;
        else resp_data = resp.data_iuran;
        
        // var keys=$("#tbl-data").find('tbody tr').length;
        // console.log(keys);
        // if(keys > 0) keys+=1;
        $.each(resp_data,function(key, value){
            var keys=value.keys;
            if(resp.temp==true){
                row+='<tr id=row-'+keys+' class="" style="opacity: 0.9;" data-bulan_iuran="'+value.bulan_iuran+'" data-jenis_iuran="'+value.jenis_iuran+'" data-keterangan_iuran="'+value.keterangan_iuran+'" >';
            }else{
                row+='<tr id=row-'+keys+' class="bg-light" style="opacity: 0.9;">';
            }
            // if(new_obj.status_iuran=="LUNAS"){
            //     row+='<td align="center"><i class="fas text-success fa-check-circle"></i></td>';
            // }else 
            if(resp.temp==true){
                row+='<td align="center">';
                row+='<div class="form-check">';
                    // row+='<input id="besar_iuran_'+keys+'" class="form-check-input" type="checkbox" value='+count_besar_iuran+' name="tekan_saja" onclick="setIuranPerBulan(this,\''+key+'\')" required>';
                    row+='<input data-jenis="'+value.jenis_iuran+'" id="besar_iuran_'+keys+'" class="form-check-input" type="checkbox" value='+(parseInt(0))+' name="tekan_saja"  onclick="setIuranPerBulan(this,\''+keys+'\')" required>';
                row+='</div>';
                row+='</td>';
            }else{
                row+='<td align="center"><i class="fas text-success fa-check-circle"></i></td>';
            }
            //     row+='<td align="center">';
            //     if(new_arr.indexOf(tgl_regis) == -1){
            //        row+='<span class="small">-</span>';
            //     }else{
            //         row+='<div class="form-check">';
            //             row+='<input id="besar_iuran_'+key+'" class="form-check-input" type="checkbox" value='+count_besar_iuran+' name="tekan_saja" onclick="setIuranPerBulan(this,\''+key+'\')" required>';
            //         row+='</div>';
            //     }
            //     row+='</td>';
            // }

            row+='<td>'+value.bulan_custom_iuran+'</td>';

            // var text_color="text-danger";
            // if(new_obj.status_iuran=="LUNAS") text_color="text-success";
            // //row+='<td class="'+text_color+'">'+jatuh_tempo+'</td>';
            // row+='<td>'+$.number(count_besar_iuran)+'</td>';

            // var edit_is=true;
            // if(new_obj.status_iuran=="LUNAS") {
            //     edit_is=false;
            //     row+='<td contenteditable="'+false+'" onkeyup="javascript:setIuranPerBulans(this,\''+key+'\');">'+$.number(new_obj.besar_iuran)+'</td>';
            // }else if(new_obj.status_iuran=="BELUM LUNAS") {
            //     edit_is=false;
            //     row+='<td id="val_besar_iuran_'+key+'" contenteditable="'+false+'" onkeyup="javascript:setIuranPerBulans(this,\''+key+'\');"><input style="border-radius:0px!important;" type="text" value='+(parseInt(count_besar_iuran)-parseInt(new_obj.besar_iuran))+'></td>'
            // }else{
            //     if(new_arr.indexOf(tgl_regis) == -1){
            //         row+='<td><span class="small">0</span></td>';
            //     }else{
            //         row+='<td id="val_besar_iuran_'+key+'" contenteditable="'+false+'" onkeyup="javascript:setIuranPerBulans(this,\''+key+'\');"><input style="border-radius:0px!important;" type="text" value='+new_obj.besar_iuran+'></td>';
            //     }
            // }
            if(resp.temp==true){
                row+='<td id="val_besar_iuran_'+keys+'" contenteditable="'+false+'" onkeyup="javascript:setIuranPerBulans(this,\''+keys+'\');"><input style="border-radius:0px!important;" placeholder="Tentukan Jumlah" type="number" value='+value.besar_iuran+'></td>';
            }else{
                row+='<td contenteditable="'+false+'">'+$.number(value.besar_iuran)+'</td>';
            }
            // if(new_arr.indexOf(tgl_regis) == -1){
            //     row+='<td><span class="small">-</span></td>';
            //     row+='<td><span class="small">Tidak dipungut biaya</span></td>';
            // }else{ 
                row+='<td><span class="small">'+value.tanggal_iuran+'</span></td>';
                row+='<td><span class="small">'+value.keterangan_iuran+'</span></td>';
            // }
                row+='<td><span class="small">'+(value.user_added==null?"-":value.user_added)+'</span></td>'; 
                if(resp.temp==true) row+='<td><span data-remove="true" onclick="setIuranPerBulan(this,\''+keys+'\')" class="fa fa-times-circle text-danger"></span></td>'; 
                else row+='<td><span class="small">-</span></td>';
            row+='</tr>';
            // keys++;
        });
        console.log(row);
        row==""?tbody.html("<tr id='no-record' class='text-center'><td colspan='7'>Tidak ada data</td></tr>"):tbody.append(row);
        if(resp.temp==true) $('#no-record').remove();

        $("#modal-form").modal('hide');
    }

    function renderDisplayIuranBulanan(){
        // console.log(res_data);
        var tahun_ajaran = $('[name=tahun_ajaran_kelas]').val().split("-")[0].split("/");
        // alert($('[name=tahun_ajaran]').val());
        var date = new Date("July 10, "+tahun_ajaran[0]);
        var endDate = new Date("June 10, "+tahun_ajaran[1]);
        var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var monthNameList_random = ["Jul", "Aug", "Sep", "Oct", "Nov", "Dec","Jan", "Feb", "Mar", "Apr", "May", "Jun"];
        
        var tbody = $("#form-bulan-iuran");
        var row="";
        var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        
        var new_tahun_ajaran = tahun_ajaran[0]+"/"+tahun_ajaran[1];
        var new_month=get_tahun_ajaran(new_tahun_ajaran);
    
        var new_arr=[];    
        $.each(new_month, function(key, date){
            new_arr.push(date.getMonth());
            // var date = new Date("July 10, "+tahun_ajaran[0]);
            var new_obj={
                "id_iuran" : 0,
                "no_iuran" : "",
                "tanggal_iuran" : "",
                "keterangan_iuran" : "",
                "besar_iuran" : 0,
                "status_iuran" : "",
            }
            stringDate = monthNameList[date.getMonth()] + " " + date.getFullYear();
            console.log(stringDate);
            // new_obj['id_iuran']=0; 
            row+='<li  data-bulan="'+date.getMonth()+'" data-text="'+stringDate+'" class="list-group-item list-group-item-action pt-2 pb-2">'+stringDate+'</li>';
            // var text_color="text-danger";
            // if(new_obj.status_iuran=="LUNAS") text_color="text-success";
            // row+='<td class="'+text_color+'">'+jatuh_tempo+'</td>';
            // row+='<td>'+$.number(count_besar_iuran)+'</td>';

            // var edit_is=true;
            // if(new_obj.status_iuran=="LUNAS") {
            //     edit_is=false;
            //     row+='<td contenteditable="'+false+'" onkeyup="javascript:setIuranPerBulans(this,\''+key+'\');">'+$.number(new_obj.besar_iuran)+'</td>';
            // }else if(new_obj.status_iuran=="BELUM LUNAS") {
            //     edit_is=false;
            //     row+='<td id="val_besar_iuran_'+key+'" contenteditable="'+false+'" onkeyup="javascript:setIuranPerBulans(this,\''+key+'\');"><input style="border-radius:0px!important;" type="text" value='+(parseInt(count_besar_iuran)-parseInt(new_obj.besar_iuran))+'></td>'
            // }else{
            //     if(new_arr.indexOf(tgl_regis) == -1){
            //         row+='<td><span class="small">0</span></td>';
            //     }else{
            //         row+='<td id="val_besar_iuran_'+key+'" contenteditable="'+false+'" onkeyup="javascript:setIuranPerBulans(this,\''+key+'\');"><input style="border-radius:0px!important;" type="text" value='+new_obj.besar_iuran+'></td>';
            //     }
            // }

            // if(new_arr.indexOf(tgl_regis) == -1){
            //     row+='<td><span class="small">-</span></td>';
            //     row+='<td><span class="small">Tidak dipungut biaya</span></td>';
            // }else{
            //     row+='<td><span class="small">'+new_obj.tanggal_iuran+'</span></td>';
            //     row+='<td><span class="small">'+new_obj.keterangan_iuran+'</span></td>';
            // }

            // row+='<td><span class="small">'+(new_obj.user_added==null?"-":new_obj.user_added)+'</span></td>';  

            // key++;
        });
            //
        tbody.html(row);
    }
    function setIuranPerBulans(elem, id_key){
        $('#besar_iuran_'+id_key).val($(elem).find('input').val());
        console.log(elem);
        console.log($(elem).find('input').val());
    }
    function setIuranPerBulan(elem, id_key){
        console.log(elem);
        console.log(id_key);

        if($(elem).data('remove')==true) $('#row-'+id_key).remove();
        var data = $('[name=tekan_saja]:checked');
        lbl_nominal_iuran=0;
        lbl_nominal_du=0;
        lbl_nominal_dpmp=0;
        lbl_nominal_tabungan=0;
        $.each(data,function(key){
            console.log(this);
            if($(this).data('jenis')=="IURAN_PER_BULAN_MANUAL"){
                lbl_nominal_iuran+=parseInt(this.value);
                $('#lbl_biaya_iuran_per_bulan').html(': '+$.number(lbl_nominal_iuran,2));
            }
            if($(this).data('jenis')=="DU_MANUAL"){
                lbl_nominal_du+=parseInt(this.value);
                $('#lbl_biaya_du').html(': '+$.number(lbl_nominal_du,2));
            }
            if($(this).data('jenis')=="TABUNGAN_MANUAL"){
                lbl_nominal_tabungan+=parseInt(this.value);
                $('#lbl_biaya_tabungan').html(': '+$.number(lbl_nominal_tabungan,2));
            }

            

            console.log(this.id+"=="+('besar_iuran_'+id_key).toString());
            // alert('#'+this.id);
            // if($('#'+this.id).is('cheked')){
            // // if(this.id==('besar_iuran_'+id_key).toString()){
            //     $('#val_besar_iuran_'+id_key).html($.number($('#besar_iuran_'+id_key).val()));
            // }else{
            //     $('#val_besar_iuran_'+id_key).html("<input style='border-radius:0px!important;' type='text' value="+$('#besar_iuran_'+id_key).val()+">");
            // }

        });
        // alert($('#'+elem.id).is(':checked'));
        if($('#'+elem.id).is(':checked')){
        // if(this.id==('besar_iuran_'+id_key).toString()){
            $('#val_besar_iuran_'+id_key).html($.number($('#besar_iuran_'+id_key).val()));
        }else{
            $('#val_besar_iuran_'+id_key).html("<input placeholder='Tentukan Jumlah' style='border-radius:0px!important;' type='text' value="+$('#besar_iuran_'+id_key).val()+">");
        }
        if(data.length==0)  {
            $('#val_besar_iuran_'+id_key).html("<input placeholder='Tentukan Jumlah' style='border-radius:0px!important;' type='text' value="+$('#besar_iuran_'+id_key).val()+">");
             $('#lbl_biaya_iuran_per_bulan').html(': 0');
            $('#lbl_biaya_du').html(': 0');
            $('#lbl_biaya_dpmp').html(': 0');
            $('#lbl_biaya_tabungan').html(': 0');
        }
        console.log(data.length);
        // var check = $('[name=tekan_saja]').is(':checked');
        // if(check) {
        //     // alert($(elem).data('jenis'));
        //     if($(elem).data('jenis')=="IURAN_PER_BULAN_MANUAL"){
        //         $('#lbl_biaya_iuran_per_bulan').html(': '+$.number(lbl_nominal_iuran,2));
        //     }
        //     if($(elem).data('jenis')=="DU_MANUAL"){
        //         $('#lbl_biaya_du').html(': '+$.number(lbl_nominal_du,2));
        //     }

        // }
        // if(check==false) {
            // $('#lbl_biaya_iuran_per_bulan').html(': 0');
            // $('#lbl_biaya_du').html(': 0');
            // $('#lbl_biaya_dpmp').html(': 0');

            // if($(elem).data('jenis')=="IURAN_PER_BULAN_MANUAL"){
            //     // lbl_nominal_iuran+=parseInt(this.value);
            //     // $('#lbl_biaya_iuran_per_bulan').html(': '+$.number(lbl_nominal_iuran,2));
            //     $('#lbl_biaya_iuran_per_bulan').html(': 0');
            // }
            // if($(elem).data('jenis')=="DU_MANUAL"){
            //     // lbl_nominal_du+=parseInt(this.value);
            //     // $('#lbl_biaya_du').html(': '+$.number(lbl_nominal_du,2));

            //     $('#lbl_biaya_du').html(': 0');
            // }
        // }

        $('#lbl_total_biaya').text(": "+($.number(lbl_nominal_iuran+lbl_nominal_dpmp+lbl_nominal_du+lbl_nominal_tabungan,2)));

        // alert($(elem).data('remove'));
        //set manual iuran
        console.log($('#besar_iuran_'+id_key).text().replace(",",""));

        //$('#besar_iuran_'+id_key).val($(elem).text());
        
    }

    function display(page = 1){
        $('#more-button-row').remove();
        var tbody = $("#tbl-data").find('tbody');
        if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
            tbody.text('');
        }
        
        tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        
        params = $( "#search-form" ).serialize();
        ajaxGET(path + '/list_table?page='+page+'&'+params,'onGetListSuccess','onGetListError');
    }

    function onGetListSuccess(response){
        console.log(response);
        $('#loading-row').remove();
        $('#no-data-row').remove();
        
        var tbody = $("#tbl-data").find('tbody');
        
        var row = "";
        var num = $('.data-row').length+1;
        $.each(response.data,function(key,value){
            row += render_row(value, num);
            num++;
        });
        row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
        
    }
    function render_row(value, num){
    //  console.log(value);
        var row = "";
        row += '<tr class="data-row" id="row-'+value.id_siswa+'">';
        row += '<td>'+(num)+'</td>';
        row += '<td class="">'+value.nis_siswa+'</td>';
        row += '<td class="">'+value.nama_lengkap_siswa+'</td>';
        row += '<td class="">'+value.kelas_siswa+'</td>';
        row += '<td class="">'+value.jk_siswa+'</td>';
        row += '<td class="">';
        row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-info pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'edit\')"><i class="fas fa-pencil-alt"></i></a> ';
        row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'delete\')"><i class="fas fa-trash-alt"></i></a>';
        row += '</td>';
        row += '</tr>';
        return row;
    }
    function onGetListError(response) {
        console.log(response);
        let timerInterval
        Swal.fire({
          title: 'Auto close alert!',
          html: 'I will close in <strong></strong> seconds.',
          timer: 1000,
          onBeforeOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
              Swal.getContent().querySelector('strong')
                .textContent = Swal.getTimerLeft()
            }, 100)
          },
          onClose: () => {
            clearInterval(timerInterval)
          }
        }).then((result) => {
          if (
            // Read more about handling dismissals
            result.dismiss === Swal.DismissReason.timer
          ) {
            console.log('I was closed by the timer')
          }
        })
        $('#ui-view').html($('#ui-view-msg-error').html());
    }

    function save_temp(){
        var obj = new FormData(document.querySelector('#form-data'));
        obj.append("list_data_iuran_bulan", JSON.stringify($('#form-bulan-iuran .active')));
        var sa=[];
        var new_obj=[];
        var resp={
            "data" : [],
            "temp" : true,
        }
        var random_id = new Date();
        random_id = random_id.getTime();
        // alert(random_id);
        if($('[name=jenis_iuran]').val()=="TABUNGAN_MANUAL") jenis_iuran="TABUNGAN";
        if($('[name=jenis_iuran]').val()=="IURAN_PER_BULAN_MANUAL") jenis_iuran="SPP";
        $('#form-bulan-iuran .active').each(function(key){
            sa.push($(this).data('bulan'));
            console.log($(this).data('bulan'));
            new_obj.push({
                "keys" : random_id+key,
                "bulan_iuran" : $(this).data('text'),
                "bulan_custom_iuran" : jenis_iuran+": "+$(this).data('text'),
                "besar_iuran" : "",
                "jenis_iuran" : $('[name=jenis_iuran]').val(),
                // "keterangan_iuran" : $('[name=tahun_ajaran_kelas]').val(),
                "tanggal_iuran" : "",
                "keterangan_iuran" : jenis_iuran+" KELAS"+$('[name=tahun_ajaran_kelas]').val().split("-")[1],
                "user_added" : ""
            });
        });
        if($('[name=jenis_iuran]').val()=="DU_MANUAL"){
            var date_now = new Date();
            new_obj.push({
                "keys" : random_id,
                "bulan_iuran" : monthNameList[date_now.getMonth()]+" "+date_now.getFullYear(),
                "bulan_custom_iuran" : "OSIS: "+$('[name=tahun_ajaran_kelas]').val(),
                "besar_iuran" : "",
                "jenis_iuran" : $('[name=jenis_iuran]').val(),
                // "keterangan_iuran" : $('[name=tahun_ajaran_kelas]').val(),
                "tanggal_iuran" : "-",
                "keterangan_iuran" : "OSIS"+$('[name=tahun_ajaran_kelas]').val().split("-")[0],
                "user_added" : "-"
            });
        }
        resp['data']=new_obj;
        renderDisplay(resp);
    }

    function save(){
        var obj = new FormData();
        if(selected_id != '') obj.append('id', selected_id);
        obj.append("id_siswa", selected_id);  
        obj.append("list_data_iuran_bulan", JSON.stringify(obj_arr));   
        ajaxPOST(path + '/save',obj,'onActionSuccess','onGetError');
    }

    function onSaveSuccess(response){
        console.log(response);
        $('#msg-info-body').html('Data telah berhasil di update.');
        $('#msg-info').modal('show');
        // setTimeout(function(){ window.location.href="pengguna?vmenu=daftar"; }, 1000);
    }
    function onActionSuccess(response){
        // display();
        // $('#modal-form').modal('hide');
        // $('#msg-confirm').modal('hide');
        // selected_action = '';
        console.log(response);
        Swal.fire({
          type: 'success',
          title: 'Message',
          text: response.message,
          //footer: '<a href>Why do I have this issue?</a>'
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Oke!'
        })
        .then((value) => {
            if(value.value){
                location.reload();
            }
        });
    }

    function doAction(id, action, confirm = 0){
        selected_action = action;
        $('#modal-form-msg').hide();
        $('#msg-confirm-alert').hide();
        if(confirm == 0) ajaxGET(path + '/get_by_id/'+id+'?action='+selected_action,'onPrepareActionSuccess','onError');
        else ajaxPOST(path + '/do_action/'+id+'/'+selected_action,{},'onActionSuccess','onError');
    }

    function onPrepareActionSuccess(response) {
        console.log(response);
        var value = response.data;
        selected_id = value.id_siswa;
        if(selected_action == 'edit'){
            fillFormValue(value);
            $('#modal-form').modal('show');
        }else{
            swal({
              title: "Are you sure?",
              text: response.message,
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
                console.log(willDelete);
                if(willDelete)
                doAction(selected_id,'delete', 1);
            });
        }
    }

    function fillFormValue(value){
        $('#form')[0].reset();
        
        $('[name=nis_siswa]').val(value.nis_siswa);
        $('[name=nama_siswa]').val(value.nama_siswa);
        $('[name=kelas_siswa]').val(value.kelas_siswa);
    }

	function add() {
        // selected_id = ''; // wajib
        selected_action = 'add'; // wajib
	    $('#form-data')[0].reset(); // reset form on modals
	    $('.form-group').removeClass('has-error'); // clear error class
	    $('.help-block').empty(); // clear error string
	    $('#modal-form').modal('show'); // show bootstrap modal
	    $('.modal-title').text('Form Iuran Manual'); // Set Title to Bootstrap modal title
        $('select').selectric('refresh');
        $("#form-bulan-iuran").html('');
	}
