	var path = ctx + 'SiswaController';
    var path_jenis_iuran = ctx + 'JenisIuranController'; 
    var path_kelas = ctx + 'KelasController'; 
    var params='';
    var selected_action='';
    var selected_id='';
    var list_iuran = [];
    var jumlah_siswa=0;
    var kelas=0;
    var selected_all=true;
    var selected_key='';
    var list_obj=[];

    function init() {
        
        getSelectKelas();
        getTahunAjaranAll();
        getSelectTahun();

        $('#form-data').submit(function(e){
          // e.preventDefault();
          // console.log(path);
          // save();
          console.log(e);
        });

        $('#check_all').click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
            if($(this).is(':checked')){
                $('#btn-naik-kelas').prop("disabled", false);
            }else{
                $('#btn-naik-kelas').prop("disabled", true);
            }
        });

        $('#search-form').submit(function(e){
            display();
            e.preventDefault();
        });

        $('#form-siswa').submit(function(e){
            // console.log(this);
            var jumlah_siswa=$('[name=jumlah_siswa]').val();
            if(jumlah_siswa=='') {
                alert("Jumlah siswa wajib diisi.");
                return false;
            }

            $('#form-siswa').attr('action', ctx+'page/form-siswa-new-kelas_view');
            $('#form-siswa').trigger('submit');
            e.preventDefault();
        });

        $(document).on('click', '#tbl-data-kelas tr', function() {
           $("#tbl-data-kelas tr").removeClass("bg-info");
           $(this).toggleClass("bg-info");
        });

    };

    function setChecked(elem){
        if($(elem).is(':checked')){
            $('#btn-naik-kelas').prop("disabled", false);
        }else{
            $('#btn-naik-kelas').prop("disabled", true);
        }
    }

    function check_tahun_ajaran(){
        var date = new Date();
            // date = new Date("July 10, 2019");
        var month = date.getMonth();

        if(month>=6){
            date=date.getFullYear()+"/"+(date.getFullYear()+1);
        }else{
            date=(date.getFullYear()-1)+"/"+date.getFullYear();
        }
        return date;
    }
    function getSelectTahun() {
       $("#tahun_ajaran").empty();
        $("#tahun_ajaran").append(new Option('Semua Tahun Ajaran', ''));
        var d = new Date();
        var n = d.getFullYear();
        var tahun = n - 5;
        for (var i = n; i>= tahun; i--) {
            $("#tahun_ajaran").append(new Option(i+"/"+(i+1)));
        }
        $("#tahun_ajaran").val(check_tahun_ajaran()).trigger('change');
    }
    function getSelectKelas(){
        $('[name="kelas[]"]').empty();//, [name=kelas]
        $('[name="kelas[]"]').append(new Option('Pilih Kelas', ''));
        // ajaxGET(path_kelas + '/list_table?page='+1+'&'+"",'onGetSelectKelas','onGetListError');
        ajaxGET(path_kelas + '/list_table?page='+1,'onGetSelectKelas','onGetListError');
    }
    function onGetSelectKelas(response){
        $.each(response.data, function(key_2, value) {
            // $('[name=kelas]').append(new Option(value.tingkat_kelas+"-"+value.jurusan_kelas+" "+value.sub_tingkat_kelas, value.id_kelas));
            list_obj.push({"id" : value.id_kelas, "tingkat" : value.tingkat_kelas, "name" : (value.tingkat_kelas+"-"+value.jurusan_kelas+" "+value.sub_tingkat_kelas)});
        });
        // $('[name=kelas]').selectric('refresh');
    }
    function display(page = 1, params=''){
        $('#more-button-row').remove();
        var tbody = $("#tbl-data").find('tbody');
        // if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
        if($( "#tahun_ajaran" ).val() != ''){
            tbody.text('');
        }
        
        tbody.append('<tr id="loading-row"><td colspan="9"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        
        params = $( "#search-form" ).serialize();
        //jgn difilter kalao mau edit
        // params += "tahun_ajaran_siswa="+$( "#tahun_ajaran" ).val();//+"&tingkat_kelas="+$("#tbl-data-kelas").find('tbody tr.bg-info').data('tingkat');
        ajaxGET(path + '/list_siswa_kelas_table_new?page='+page+'&'+params,'onGetListSuccess','onGetListError');
    }

    function onGetListSuccess(response){
        console.log(response);
        $('#loading-row').remove();
        $('#no-data-row').remove();
        
        var tbody = $("#tbl-data").find('tbody');
        
        var row = "";
        var num = $('.data-row').length+1;
        $.each(response.data,function(key,value){
            row += render_row(value, num);
            num++;
        });
        row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="9"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
        $('#info-tabel').html("Total data : <span>"+response.data.length+"</span>");
    }

    function render_row(value, num){
    //  console.log(value);
        var row = "";
        row += '<tr contenteditable="false" class="data-row bg-light" id="row-'+value.id_siswa_kelas+'">';
        row+='<td align="center">';
        if(value.status_siswa_kelas==1){
        row+='<div class="form-check">';
            row+='<input onclick="setChecked(this)" class="form-check-input" style="position:unset" type="checkbox" value='+value.id_siswa_kelas+' name="id[]" data-tahun_ajaran='+value.tahun_ajaran+'>';
        row+='</div>';
        }

        row+='</td>';
        // row += '<td>'+(num)+'</td>';
        row += '<td class=""><a href="#" class="text-danger">'+value.ref_siswa+'</a></td>';
        // row += '<td class="">'+value.nis_siswa+'</td>';
        row += '<td class="">'+value.nama_lengkap_siswa+'</td>';
        row += '<td class="">'+value.tingkat_kelas+" "+value.jurusan_kelas+" "+value.sub_tingkat_kelas+'</td>';
        row += '<td class="">'+value.jenis_kelamin_siswa+'</td>';
        row += '<td class="">'+value.tahun_ajaran_siswa_kelas+'</td>';
        row += '<td class="">'+value.tahun_angkatan_siswa+'</td>';
        row += '<td class="">'+value.tanggal_registrasi_siswa_kelas+'</td>';
        // row += '<td class="">'+"-"+'</td>';
        row += '<td class="">';
        var asss=JSON.stringify(value.list_iuran);
        // console.log(asss);
        row += '<a data-obj=\''+asss+'\' href="javascript:void(0)" class="" type="" onclick="editIuran(this);"><i class="fas fa-search d-none"></i> Detil Iuran</a> ';
        // row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-primary pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'delete\')"><i class="fas fa-search"></i></a>';
        row += '</td>';
        row += '</tr>';
        return row;
    }

    

    function onGetListError(response) {
        alert("ERROR");
        $('#ui-view').html($('#ui-view-msg-error').html());
    }

    function save(){
        var obj = new FormData(document.querySelector('#form'));
        if(selected_id != '') obj.append('id', selected_id);
        obj.append('kelas', $('#tingkat').val()+' '+$('#jurusan').val()+' '+$('#sub_tingkat').val());
        obj.append('list_iuran', JSON.stringify(list_iuran[0]));
        
        ajaxPOST(path + '/save',obj,'onActionSuccess','onSaveError');
    }

    function update(){
        var obj = new FormData(document.querySelector('#form-iuran'));
        
        ajaxPOST(path + '/update_siswa_detil',obj,'onActionSuccess','onSaveError');
    }

    function onSaveSuccess(response){
        console.log(response);
        $('#msg-info-body').html('Data telah berhasil di update.');
        $('#msg-info').modal('show');
        // setTimeout(function(){ window.location.href="pengguna?vmenu=daftar"; }, 1000);
    }
    //
    function onActionSuccess(response){
        Swal.fire({
          type: 'success',
          title: 'Message',
          text: response.message,
          //footer: '<a href>Why do I have this issue?</a>'
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Oke!'
        })
        .then((value) => {
            if(value.value){
                location.reload();
            }
        });
    }

    function doAction(id, action, confirm = 0){
        selected_action = action;
        $('#modal-form-msg').hide();
        $('#msg-confirm-alert').hide();
        if(confirm == 0) ajaxGET(path + '/get_by_id/'+id+'?action='+selected_action,'onPrepareActionSuccess','onError');
        // else ajaxPOST(path + '/do_action/'+id+'/'+selected_action,{},'onActionSuccess','onError');
        else ajaxPOST(path + '/delete_all/'+id,{},'onActionSuccess','onError');
    }

    function onPrepareActionSuccess(response) {
        console.log(response);
        var value = response.data;
        selected_id = value.id_siswa;
        if(selected_action == 'edit'){
            fillFormValue(value);
            $('#modal-form').modal('show');
        }else{
            Swal.fire({
              title: 'Are you sure?',
              text: response.message,
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                doAction(selected_id,'delete', 1);
                Swal.fire(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                display(1);
              }
            })
        }
    }
    

    function fillFormValue(value){
        $('#form')[0].reset();
        
        $('[name=nis_siswa]').val(value.nis_siswa);
        $('[name=nama_siswa]').val(value.nama_siswa);
        $('[name=kelas_siswa]').val(value.kelas_siswa);
    }

	function add() {
        selected_id = ''; // wajib
        selected_action = 'add'; // wajib
	    $('#form')[0].reset(); // reset form on modals
	    $('.form-group').removeClass('has-error'); // clear error class
	    $('.help-block').empty(); // clear error string
	    $('#modal-form').modal('show'); // show bootstrap modal
	    $('.modal-title').text('Tambah Siswa'); // Set Title to Bootstrap modal title
        $('#form-pass').removeClass("d-none");

	}

    function getTahunAjaranAll(){
        var strdate=[];
        var angkatan= new Date("July 10, 2017");
        // var date = new Date("December 10, 2019");//+tahun_ajaran[0]
        var date = new Date();
        var selisih=(date.getFullYear()-angkatan.getFullYear());
        for(var start_ajaran=angkatan.getFullYear(); start_ajaran < angkatan.getFullYear()+selisih; start_ajaran++){
            if(start_ajaran+"/"+(start_ajaran+1)!=date){
                strdate.push(start_ajaran+"/"+(start_ajaran+1).toString());
                $('#tahun_ajaran').append(new Option(start_ajaran+"/"+(start_ajaran+1).toString(), start_ajaran+"/"+(start_ajaran+1).toString()));
            }
        }
        return strdate;
    }

    function filter(params){
        display(1, params);
    }



    function goToEdit(mode=""){
        // var id = $('#tbl-data').find('input:checked');
        // var arr=[];
        // $.each(id, function(key){
        //     arr.push(this.value);
        // });
        // if(arr=='') {
        //     var resp_msg={"title" : "Tidak dapat menambah data", "body" : "Karena anda belum memilih data siswa!", "icon"  : "warning"};
        //     showAlertMessage(resp_msg, 1500);
        // }else{
        //     var tahun_ajaran=id.data('tahun_ajaran');
        //     if(mode=='edit'){
        //         mode="&mode=edit";
        //     }
        //     window.location.href=ctx+'page/form_siswa_view?id='+arr.toString()+"&tahun_ajaran="+tahun_ajaran+mode;
        // }
        // var obj = new FormData(document.querySelector('#form-data'));
        // obj.append("list_obj", obj);
        $('#form-data').attr('action', ctx+'page/form-siswa-naik-kelas_view');
        // $('#form-data').attr('list_obj', JSON.stringify(list_obj));
        $('#form-data').trigger('submit');
    }

    function goToNaikKelas(){
        Swal.fire({
            title: 'Pilih Kelas & Tanggal Registrasi',
            html: 
                '<select style="width:100%" id="kelas_baru" class="swal2-select"><option value disabled selected>Pilih Kelas Baru</select>' +
                '<input type="text" placeholder="Pilih Tanggal Registrasi" id="tanggal_registrasi" class="swal2-input form-control" autocomplete="off">',
            focusConfirm: false,
            showCancelButton: true,
            confirmButtonText: 'Submit',
            // showLoaderOnConfirm: true,
            onOpen: function() {
              $('#tanggal_registrasi').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 2017,
                maxYear: parseInt(moment().format('YYYY'),10),
                locale: {
                    format: 'YYYY-MM-DD'
                }
              }, function(start, end, label) {
                var years = moment().diff(start, 'years');
              });

              var tingkat_kelas="";
              $.each(list_obj, function(key, o){
                console.log(this);
                if($('[name=filter_kelas').val()=="X"){
                    if(o.tingkat=="XI") $('#kelas_baru').append(new Option(o.name, o.id));
                    tingkat_kelas="XI";
                }
                if($('[name=filter_kelas').val()=="XI"){
                    if(o.tingkat=="XII") $('#kelas_baru').append(new Option(o.name, o.id));
                    tingkat_kelas="XII";
                }
                if($('[name=filter_kelas').val()=="XII"){
                    // if(o.tingkat=="XII") options[o.id] = o.name;
                }
              });
                $('[name=tingkat_kelas]').val(tingkat_kelas);
            },
            preConfirm: () => {
                var kelas_baru = $('#kelas_baru').val();
                var tanggal_registrasi = $('#tanggal_registrasi').val();
                console.log(kelas_baru);
              // if(kelas_baru==null) {
              //   $("#swal2-validation-message").show();

              //   $("#swal2-validation-message").text("Kelas belum dipilih");
              //   // reject('Kelas/Tanggal belum dipilih');
              // }else{
              //   $('[name=list_obj]').val(kelas_baru+","+tanggal_registrasi);
              //   goToEdit();
              // }
                $('[name=list_obj]').val(kelas_baru+","+tanggal_registrasi);
                goToEdit();
            }
        })
    }

    function editIuran(elem){
        console.log($(elem).data('obj'));
        var list_iuran = $(elem).data('obj');
        var tabel_iuran = '<form id="form-iuran"><div class="table-responsive text-left">'+
        '    <table id="table0" class="table table-sm table-hovered table-hover tbl-data-iuran" style="font-family: segoeui;overflow: hidden;padding: 10px!important;">'+
        '        <thead class="text-white" style="background-color: #487d95!important;">'+
        '            <tr>'+
        '                <th scope="col" >Jenis Iuran</th>'+
        '                <th scope="col" >Besar Iuran</th>'+
        '            </tr>'+
        '        </thead>'+
        '        <tbody>';
        $.each(list_iuran, function(key){
            tabel_iuran +='<tr class="data-row" id="row-'+key+'">'+
            '                <td class="d-none">'+
            '                    <input class="" name="id[]" type="text" value="'+this.id_siswa_detil+'">'+
            '                </td>'+
            // '                <td class="">'+
            // '                    <input name="kelas_siswa[]" type="text" value="'+this.tingkat_siswa_detil+'">'+
            // '                </td>'+
            '                <td>'+
            '                    <input class="border-radius-0 bg-light" name="jenis_iuran[]" type="text" readonly="TRUE" placeholder="'+this.jenis_iuran_siswa_detil+'">'+
            '                </td>'+
            '                <td>'+
            '                    <input class="border-radius-0" name="besar_iuran[]" type="number" value="'+this.besar_iuran_siswa_detil+'">'+
            '                </td>'+
            '            </tr>';
        });
        tabel_iuran +='</tbody>'+
        '    </table>'+
        '</div></form>';

        Swal.fire({
            title: 'Edit Iuran Siswa',
            html: tabel_iuran,
            focusConfirm: false,
            showCancelButton: true,
            confirmButtonText: 'Updated',
            // showLoaderOnConfirm: true,
            onOpen: function() {

              // $.each(list_obj, function(key, o){
              //   console.log(this);
              //   if($('[name=filter_kelas').val()=="X"){
              //       if(o.tingkat=="XI") $('#kelas_baru').append(new Option(o.name, o.id));
              //   }
              //   if($('[name=filter_kelas').val()=="XI"){
              //       if(o.tingkat=="XII") $('#kelas_baru').append(new Option(o.name, o.id));
              //   }
              //   if($('[name=filter_kelas').val()=="XII"){
              //       // if(o.tingkat=="XII") options[o.id] = o.name;
              //   }
              // });
            },
            preConfirm: () => {
              //   var kelas_baru = $('#kelas_baru').val();
              //   var tanggal_registrasi = $('#tanggal_registrasi').val();
              //   console.log(kelas_baru);
              // if (kelas_baru==null) {
              //   $("#swal2-validation-message").show();

              //   $("#swal2-validation-message").text("Kelas belum dipilih");
              //   // reject('Kelas/Tanggal belum dipilih');
              // }else{
              //   $('[name=list_obj]').val(kelas_baru+","+tanggal_registrasi);
              //   goToEdit();
              // }
              update();
            }
        })
    }

    function goToDelete(){
        var obj = new FormData(document.querySelector('#form-data'));
        ajaxPOST(path + '/delete_all/',obj,'onActionSuccess','onError');
    }

    function createForm(elem){
        var link_excel;
        if(elem.value=="X"){
            link_excel = ctx+"excel/template-excel-10.xlsx";
        }else if(elem.value=="XI" || elem.value=="XII"){
            link_excel = ctx+"excel/template-excel-11_12.xlsx";
        }

        var temp = '<label for="">Upload file(.xlsx)</label>'+
        '<div class="custom-file">'+
        '    <input type="file" class="custom-file-input" id="customFile" name="uploadFile" onchange="$(\'#custom-file-name\').text(this.files[0].name)">'+
        '    <label id="custom-file-name" class="custom-file-label" for="customFile">Choose file</label>'+
        '</div>'+
        '<small class="form-text"><a id="unduh_temp" class="text-white" href=\''+link_excel+'\'>Klik disini, untuk mendownload template.</a></small>';
        if(link_excel!=undefined) $('#form-upload-file').html(temp);
        else $('#form-upload-file').html("SOMETHING WRONG");
    }

    function submit_form(){
        if($('[name=uploadFile]').val()!="")
        $('[name=preview]').click();
        else alert("File belum dipilih.");
    }
