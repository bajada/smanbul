	var path = ctx + 'LaporanController';
    var path_jenis_iuran = ctx + 'JenisIuranController'; 
    var path_kelas = ctx + 'KelasController'; 
    var params='';
    var selected_action='';
    var selected_id='';
    var list_iuran = [];
    var jumlah_siswa=0;

    function init() {
        //tambahkan validasi, jika form halaman  bukan daftar maka jangan runing ini
        // display(1);
        $('#search-form').submit(function(e){
            if($('[name=filter_keyword]').val() != ""){
                display();
            }else{
                var resp_msg={"title" : "Message", "body" : "Tanggal pencarian wajib diisi!", "icon"  : "warning"};
                showAlertMessage(resp_msg, 1200);
            }
            e.preventDefault();
        });

        $('#btn-export').click(function(){
            var params="";
            params=$('#search-form').serialize();

            window.location.href=ctx+"LaporanExcel/tes?"+params;
        });
        

        //format tgl mm/dd/yyyy
        console.log(date_diff_indays('07/22/2019', '07/28/2019'));
        console.log(date_diff_inweeks('07/22/2019', '07/29/2019'));

     //    $(function(){
     //     $(".datepicker").datepicker({
     //        format: 'yyyy-mm-dd',
     //        autoclose: true,
     //        todayHighlight: true,
     //    });
     //    $("#tgl_mulai").on('changeDate', function(selected) {
     //        var startDate = new Date(selected.date.valueOf());
     //        $("#tgl_akhir").datepicker('setStartDate', startDate);
     //        if($("#tgl_mulai").val() > $("#tgl_akhir").val()){
     //          $("#tgl_akhir").val($("#tgl_mulai").val());
     //        }
     //    });
     // });

        $(function() {

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                // $('#reportrangestart')
                // $('#reportrangestart').val(start.format('MM/D/YYYY'));
               
                $('#reportrangestart').val(start.format('MM/DD/YYYY'));
                $('#reportrangeend').val(end.format('MM/DD/YYYY'));
                // $('#reportrangestart').val(start.format('YYYY-MM-DD'));
                // $('#reportrangeend').val(end.format('YYYY/MM-DD'));
            }

            $('#reportrangestart').daterangepicker({
                //singleDatePicker: true,
                // "locale": {
                // "format": "MM/DD/YYYY",
                // "separator": " = ",
                // },
                startDate: start,
                endDate: end,
                ranges: {
                   'Today': [moment(), moment()],
                   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                   'This Month': [moment().startOf('month'), moment().endOf('month')],
                   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);
        });

    };

    var date_diff_indays = function(date1, date2) {
    dt1 = new Date(date1);
    dt2 = new Date(date2);
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) / (1000 * 60 * 60 * 24));
    }

    var date_diff_inweeks = function(date1, date2) {
    dt1 = new Date(date1);
    dt2 = new Date(date2);
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) / (1000 * 60 * 60 * 24) / 7);
    }

    function trychange(){
        var fav=$('#reportrangestart').val();
        $('#reportrangestart').val(fav.split(" ")[0]);
    }

    function display(page = 1, param=''){
        $('#more-button-row').remove();
        var tbody = $("#tbl-data").find('tbody');
        var body=$('#tbl-data-rekap').find('tbody');
        // if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
        tbody.text('');
        body.text('');
        // }/
        
        tbody.append('<tr id="loading-row"><td colspan="10"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        body.append('<tr id="loading-row-2"><td colspan="10"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        if(param=='') {
            params = $( "#search-form" ).serialize();
            ajaxGET(path + '/list_table?page='+page+'&'+params,'onGetListTabSuccess','onGetListError');
        }else{
            params="filter_keyword="+param;
            ajaxGET(path + '/list_table?page='+page+'&'+params,'onGetListSuccess','onGetListError');
        } 
    }
    function onGetListTabSuccess(response){
        console.log(response);
        var row = "";
        var myvar = "";
        var wraper_sheet=$('#wrap-sheet');
        var wraper_content=$('#pills-tabContent');
        var new_key=0;
        var html = [];
        $.each(response.data.laporan, function(key) {

            var name = this.tanggal_bulanan;
            if(html.indexOf(name) == -1) html.push(name);
            else html.push("");

            if(html[key]){
                console.log(this);
                if(new_key==0) active="active show";
                else active="";

                row +=  '<li class="nav-item">'+// onclick="display(\'1\', \''+this.tanggal_bulanan+'\')" 
                            '<a id="btn-date-'+new_key+'" class="nav-link '+active+'" data-toggle="pill" href="#pills-date-'+new_key+'">'+this.tanggal_bulanan+'</a>'+
                        '</li>';

                //create body and table x date
                myvar += '<div class="tab-pane fade '+active+'" id="pills-date-'+new_key+'">'+
                '            <div class="row">'+
                '              <div class="col-sm-auto">'+
                '                  <div class="">'+
                '                    <table id="tbl-data-rekap-'+new_key+'" class="table table-sm table-striped table-pill" style="font-family: segoeui;opacity: 0.8;">'+
                '                      <thead class="text-white" style="background-color: #487d95!important;">'+
                '                        <tr>'+
                '                          <th colspan="2" class="text-center">REKAP</th>'+
                '                        </tr>'+
                '                      </thead>'+
                '                      <tbody class="text-dark">';
                //
                var data_jenis_iuran = [{"name" : "IURAN_PER_BULAN"},{"name" : "DPMP"},{"name" : "DU"}];
                var data=[{"kelas": "X", "total": 0},{"kelas": "XI", "total": 0},{"kelas": "XII", "total": 0}];

                var tanggal_bulanan = this.tanggal_bulanan;
                var total_rekap=0;
                $.each(data_jenis_iuran, function(key,val_1){
                    $.each(data, function(key, val_2){
                        var dump=0;
                        myvar +='<tr contenteditable="false" class="data-row-rekap">';
                          myvar +='<td>'+(val_1.name=='IURAN_PER_BULAN'?"BULANAN":val_1.name)+" Kelas "+val_2.kelas+'</td>';
                            var sum_besar_iuran=0;
                            $.each(response.data.iuran, function(key, val){
                                if(val.real_tanggal_iuran==tanggal_bulanan){
                                    if(val_1.name==val.jenis_iuran && val_2.kelas==val.tingkat_kelas){
                                      sum_besar_iuran+=parseInt(val.besar_iuran);
                                    }
                                }

                            });
                            myvar +='<td class="">'+$.number(sum_besar_iuran)+'</td>';
                            total_rekap+=sum_besar_iuran;
                        myvar +='</tr>';
                    });
                });

                // $('#total-rekap').text($.number(total_rekap));
                //
                myvar +=               '</tbody>'+
                '                      <tfoot class="text-white" style="background-color: #487d95!important;">'+
                '                        <tr>'+
                '                          <th>Jumlah</th>'+
                '                          <th id="total-rekap-'+new_key+'">0</th>'+
                '                        </tr>'+
                '                      </tfoot>'+
                '                    </table>'+
                '                  </div>'+
                '              </div>'+
                ''+
                '              <div class="col-sm-auto">'+
                '                  <div class="">'+
                '                    <table id="tbl-data-'+new_key+'" class="table table-striped table-pill"  style="font-family: segoeui;opacity: 0.8;display: table;">'+
                '                      <thead class="text-white" style="background-color: #487d95!important;">'+
                '                        <tr>'+
                '                          <th width="5%">No</th>'+
                '                          <th width="6%">REF</th>'+
                '                          <th width="25%">SISWA</th>'+
                '                          <th width="10%">BULAN KE</th>'+
                '                          <th width="10%">IURAN BULANAN</th>'+
                '                          <th width="10%">DPMP</th>'+
                '                          <th width="10%">DU</th>'+
                '                          <th width="10%">TOTAL</th>'+
                '                          <th width="10%">KETERANGAN</th>'+
                '                        </tr>'+
                '                      </thead>'+
                '                      <tbody class="text-dark">';
                //
                var no=1;
                $.each(response.data.laporan,function(key, value){
                    if(value.du != "0.00" || value.dpmp != "0.00" || value.spp != "0.00"){    
                        // row += render_row(value, num);
                        if(this.tanggal_bulanan==tanggal_bulanan){
                            myvar += '<tr contenteditable="false" class="data-row" id="row-'+value.id_siswa+'">';
                                myvar += '<td>'+(no)+'</td>';
                                myvar += '<td class="">'+value.ref_siswa+'</td>';
                                myvar += '<td class="">'; 
                                    myvar += '<b style="font-family: segoeuib;">Nama</b>: '+value.nama_lengkap_siswa+'<br/>';
                                    myvar += '<b style="font-family: segoeuib;">Kelas</b>: '+value.kelas;
                                    //

                                var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                                var dump=[];
                                $.each(value.total_bulan,function(key){
                                    var str = this.bulan_iuran;
                                    str=str.split(" ");
                                    dump.push(monthNameList.indexOf(str[0])+1);
                                });
                                myvar += '<td class="">'+dump+'</td>';
                                myvar += '<td>'+$.number(value.spp)+'</td>';
                                myvar += '<td>'+$.number(value.dpmp)+'</td>';
                                myvar += '<td>'+$.number(value.du)+'</td>';
                                myvar += '<td class="">'+$.number(parseInt(value.spp)+parseInt(value.dpmp)+parseInt(value.du))+'</td>';
                                myvar += '<td class="">'+value.tahun_ajaran+'</td>';
                                myvar += '<td class="d-none">';
                                    myvar +='<p><strong>bulanan:</strong> '+value.tanggal_bulanan+'</p>';
                                    myvar +='<p><strong>dpmp:</strong> '+value.tanggal_dpmp+'</p>';
                                    myvar +='<p><strong>du:</strong> '+value.tanggal_du+'</p>';
                                myvar += '</td>';
                                    //
                                myvar += '</td>';
                            myvar += '</tr>';
                            no++;
                        }
                    }
                });
                //
                myvar += '             </tbody>'+
                '                      <tfoot class="text-white" style="background-color: #487d95!important;">'+
                '                        <tr>'+
                '                          <th class="text-center" colspan="4">Jumlah</th>'+
                '                          <th id="jumlah-iuran-'+new_key+'">0</th>'+
                '                          <th id="jumlah-dpmp-'+new_key+'">0</th>'+
                '                          <th id="jumlah-du-'+new_key+'">0</th>'+
                '                          <th id="jumlah-total-'+new_key+'">0</th>'+
                '                          <th width="10%"></th>'+
                '                        </tr>'+
                '                      </tfoot>'+
                '                    </table>'+
                '                  </div>'+
                '              </div>'+
                '            </div>'+
                ''+
                '            </div>';
                    

                //
                new_key++;
            }
            
        });
        wraper_content.html(myvar);
        wraper_sheet.html(row);
    }

    function renderRowTable(response){

    }

    function onGetListSuccess(response){
        console.log(response);
        $('#loading-row').remove();
        $('#loading-row-2').remove();
        $('#no-data-row').remove();
        
        var tbody = $("#tbl-data").find('tbody');
        
        var row = "";
        var num = $('.data-row').length+1;
        var temp_data=[];
        var jumlah_iuran=0;
        var jumlah_dpmp=0;
        var jumlah_du=0;

        var jumlah_total=0;
        
        $.each(response.data.laporan,function(key,value){
            console.log(value.du=="0.00");
            // if(value.total_bulan != ""){
            if(value.du != "0.00" || value.dpmp != "0.00" || value.spp != "0.00"){    
                // row += render_row(value, num);
               
                row += '<tr contenteditable="false" class="data-row" id="row-'+value.id_siswa+'">';
                row += '<td>'+(num)+'</td>';
                row += '<td class="">'+value.ref_siswa+'</td>';
                row += '<td class="">'; 
                    row += '<b style="font-family: segoeuib;">Nama</b>: '+value.nama_lengkap_siswa+'<br/>';
                    row += '<b style="font-family: segoeuib;">Kelas</b>: '+value.kelas;
                row += '</td>';
                var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                var dump=[];
                $.each(value.total_bulan,function(key){
                    var str = this.bulan_iuran;
                    str=str.split(" ");
                    dump.push(monthNameList.indexOf(str[0])+1);
                });
                row += '<td class="">'+dump+'</td>';
                row += '<td>'+$.number(value.spp)+'</td>';
                row += '<td>'+$.number(value.dpmp)+'</td>';
                row += '<td>'+$.number(value.du)+'</td>';
                row += '<td class="">'+$.number(parseInt(value.spp)+parseInt(value.dpmp)+parseInt(value.du))+'</td>';
                row += '<td class="">'+value.tahun_ajaran+'</td>';
                row += '<td class="">';
                    row +='<p><strong>bulanan:</strong> '+value.tanggal_bulanan+'</p>';
                    row +='<p><strong>dpmp:</strong> '+value.tanggal_dpmp+'</p>';
                    row +='<p><strong>du:</strong> '+value.tanggal_du+'</p>';
                row += '</td>';
                row += '</tr>';
                //
                temp_data.push(this);
                num++;
                jumlah_iuran+=parseInt(value.spp);
                jumlah_dpmp+=parseInt(value.dpmp);
                jumlah_du+=parseInt(value.du);
                jumlah_total+=parseInt(value.spp)+parseInt(value.dpmp)+parseInt(value.du);

            }
        });
        row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="10"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
        
        $('#jumlah-iuran').text($.number(jumlah_iuran));
        $('#jumlah-dpmp').text($.number(jumlah_dpmp));
        $('#jumlah-du').text($.number(jumlah_du));
        $('#jumlah-total').text($.number(jumlah_total));
        //
        var row="";
        var body=$('#tbl-data-rekap').find('tbody');
        var data_jenis_iuran = [{"name" : "IURAN_PER_BULAN"},{"name" : "DPMP"},{"name" : "DU"}];
        var data=[{"kelas": "X", "total": 0},{"kelas": "XI", "total": 0},{"kelas": "XII", "total": 0}];

        var total_rekap=0;
        $.each(data_jenis_iuran, function(key,val_1){
            $.each(data, function(key, val_2){
                var dump=0;
                row +='<tr contenteditable="false" class="data-row-rekap">';
                  row +='<td>'+(val_1.name=='IURAN_PER_BULAN'?"BULANAN":val_1.name)+" Kelas "+val_2.kelas+'</td>';
                    var sum_besar_iuran=0;
                    $.each(response.data.iuran, function(key, val){
                        if(val_1.name==val.jenis_iuran && val_2.kelas==val.tingkat_kelas){
                          sum_besar_iuran+=parseInt(val.besar_iuran);
                        }
                    });
                    row +='<td class="">'+$.number(sum_besar_iuran)+'</td>';
                    total_rekap+=sum_besar_iuran;
                row +='</tr>';
            });
        });

        body.html(row);
        $('#total-rekap').text($.number(total_rekap));
    }

    // function render_row(value, num){
    //  console.log(value);
    //     var row = "";
    //     row += '<tr contenteditable="false" class="data-row" id="row-'+value.id_siswa+'">';
    //     row += '<td>'+(num)+'</td>';
    //     row += '<td class="">'+value.ref_siswa+'</td>';
    //     // row += '<td class="">'+value.nis_siswa+'</td>';
    //     row += '<td class="">'; 
    //         row += '<b style="font-family: segoeuib;">Nama</b>: '+value.nama_lengkap_siswa+'<br/>';
    //         row += '<b style="font-family: segoeuib;">Kelas</b>: '+value.kelas;
    //     row += '</td>';
    //     // row += '<td class="">'+value.kelas+'</td>';
        
    //     var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    //     var dump=[];
    //     $.each(value.total_bulan,function(key){
    //         var str = this.bulan_iuran;
    //         str=str.split(" ");
    //         dump.push(monthNameList.indexOf(str[0])+1);
    //     });
    //     row += '<td class="">'+dump+'</td>';

    //     // row += '<td colspan="3">';
    //         row += '<td>'+$.number(value.spp)+'</td>';
    //         row += '<td>'+$.number(value.dpmp)+'</td>';
    //         row += '<td>'+$.number(value.du)+'</td>';
    //     // row += '</td>';
    //     row += '<td class="">'+$.number(parseInt(value.spp)+parseInt(value.dpmp)+parseInt(value.du))+'</td>';
    //     row += '<td class="">'+value.tahun_ajaran+'</td>';
    //     row += '<td class="">';
    //         row +='<p><strong>bulanan:</strong> '+value.tanggal_bulanan+'</p>';
    //         row +='<p><strong>dpmp:</strong> '+value.tanggal_dpmp+'</p>';
    //         row +='<p><strong>du:</strong> '+value.tanggal_du+'</p>';
    //     row += '</td>';
    //     row += '</tr>';
    //     return row;
    // }

    function onGetListError(response) {
        alert("ERROR");
        $('#ui-view').html($('#ui-view-msg-error').html());
    }

    function doAction(id, action, confirm = 0){
        selected_action = action;
        $('#modal-form-msg').hide();
        $('#msg-confirm-alert').hide();
        if(confirm == 0) ajaxGET(path + '/get_by_id/'+id+'?action='+selected_action,'onPrepareActionSuccess','onError');
        else ajaxPOST(path + '/do_action/'+id+'/'+selected_action,{},'onActionSuccess','onError');
    }

    function onPrepareActionSuccess(response) {
        console.log(response);
        var value = response.data;
        selected_id = value.id_siswa;
        if(selected_action == 'edit'){
            fillFormValue(value);
            $('#modal-form').modal('show');
        }else{

            Swal.fire({
              title: 'Are you sure?',
              text: response.message,
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                doAction(selected_id,'delete', 1);
                Swal.fire(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                display(1);
              }
            })
        }
    }
