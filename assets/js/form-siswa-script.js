	var path = ctx + 'SiswaController';
    var path_jenis_iuran = ctx + 'JenisIuranController'; 
    var path_kelas = ctx + 'KelasController'; 
    var params='';
    var selected_action='';
    var selected_id='';
    var list_iuran = [];
    var jumlah_siswa=0;
    var kelas=0;
    var mode;
    var selected_all=true;
    var selected_key;
    var tahun_ajaran;

    function init() {
        jumlah_siswa = ($.urlParam('jumlah_siswa') == null ? 0 : $.urlParam('jumlah_siswa'));
        selected_id = ($.urlParam('id') == null ? '' : $.urlParam('id'));
        kelas = ($.urlParam('kelas') == null ? '' : $.urlParam('kelas'));
        tahun_ajaran = ($.urlParam('tahun_ajaran') == null ? '' : $.urlParam('tahun_ajaran'));
        mode = ($.urlParam('mode') == null ? '' : $.urlParam('mode'));

        if(jumlah_siswa > 0){
            var myvar="";
            for(var i=0; i < jumlah_siswa; i++){
             myvar += '<div class="card-deck">'+
            '<div class="card bg-head pill mt-1">'+
            '            <div class="card-header"><h5 class="card-title" style="font-family: segoeui;">Data Siswa Ke '+(i+1)+'</h5></div>'+
            '            <div id="card'+i+'" class="card-body">'+
            '              <div class="form-group row"> '+
            '                <div class="col-sm-6">'+
            '                  <input autocomplete="off" name="ref[]" type="text" class="form-control" id="" placeholder="NO.REF">'+
            '                </div>                         '+
            '                <div class="col-sm-6">'+
            '                  <select data-id="'+(i+1)+'" id="kelas-'+(i+1)+'" data-tingkat="" name="kelas[]" class="form-control select-styles">'+
            '                      <option value="0">Pilih Kelas</option>'+
            '                  </select>'+
            '                </div>   '+
            '              </div>'+
            '              <div class="form-group row">        '+
            '                <div class="col-sm-12">'+
            '                  <input autocomplete="off" name="nama_lengkap[]" type="text" class="form-control" id="" placeholder="Nama Siswa">'+
            '                </div>'+
            '              </div>'+
            '              <div class="form-group row">        '+
            '                <div class="col-sm-6">'+
            '                  <select name="jenis_kelamin[]" class="form-control select-style">'+
            '                      <option value="">Jenis Kelamin</option>'+
            '                      <option value="LAKI_LAKI">LAKI_LAKI</option>'+
            '                      <option value="PEREMPUAN">PEREMPUAN</option>'+
            '                  </select>'+
            '                </div>      '+
            '                <div class="col-sm-6">'+
            '                  <input name="tahun_ajaran[]" type="text" class="form-control" id="" placeholder="Tahun Ajaran" readonlys>'+
            '                </div>'+
            '              </div>'+
            '              </div>'+
            '              </div>'+
            '            </div>';
            }
            $('#form-container').html(myvar);
        }
        // if(selected_id != ''){

        //     console.log(selected_id);
        //     var arr = selected_id.split(",");
        //     $.each(arr, function(key){
        //         alert(this);
        //     })
        //     alert("RENDER FORM");
        // }
        //jika form halaman  bukan daftar maka jangan runing ini
        if(selected_id != '') {
            display(1);
        }else{
            getSelectKelas();
        }
        // getTahunAjaranAll();
        // getSelectTahun();

        $('#form').submit(function(e){
          e.preventDefault();
          console.log(path);
          save();
        });

        initFunctionKelas();

        $(document).on('click', '#tbl-data-kelas tr', function() {
           $("#tbl-data-kelas tr").removeClass("bg-info");
           $(this).toggleClass("bg-info");
        });
    };

    function initFunctionKelas(reinit){
        if(reinit){
            // alert(selected_key);
            // var arr = selected_id.split(",");
            // $.each(response.data, function(key, value){
                //ga perlu diklik satu-satu, langsung set aja di render formnya
            // $.each(arr, function(key){
            //     // if(value.id_siswa == this){
            //         // alert(key_2);
            //        $('[name="from_kelas[]"]').get(key).click();
            //     // }
            // })
            // })
            // $('[name="from_kelas[]"]').trigger('click');
            getTahunAjaranAll();
            getSelectKelas();
            getSelectTahun();
        }
        $('[name="kelas[]"]').on('change',function(){
            selected_all=false;
            // var x=$('[name="kelas[]"]').find("option[value=" + this.value +"]").data('tingkat');
            selected_key=$(this).data('id')-1;
            //alert(selected_key);
            setKey_Iuran(selected_key);
        });
        // alert("REINIT");
    }

    function check_tahun_ajaran(){
        var date = new Date();
            // date = new Date("July 10, 2019");
        var month = date.getMonth();

        if(month>=6){
            date=date.getFullYear()+"/"+(date.getFullYear()+1);
        }else{
            date=(date.getFullYear()-1)+"/"+date.getFullYear();
        }
        return date;
    }

    function getSelectTahun() {
       $('[name="tahun_ajaran[]"]').empty();
        $('[name="tahun_ajaran[]"]').append(new Option('Pilih Tahun Ajaran', ''));
        var d = new Date();
        var n = d.getFullYear();
        var tahun = n - 5;
        for (var i = n; i>= tahun; i--) {
            $('[name="tahun_ajaran[]"]').append(new Option(i+"/"+(i+1)));
        }
        $('[name="tahun_ajaran[]"]').val(check_tahun_ajaran()).trigger('change');
    }
    function getSelectKelas(){
         // if(selected_key == undefined ) {
            $('[name="kelas[]"], [name=kelas]').empty();
            $('[name="kelas[]"], [name=kelas]').append(new Option('Pilih Kelas', '0'));
        // }
        ajaxGET(path_kelas + '/list_table?page='+1+'&'+"",'onGetSelectKelas','onGetListError');
    }

    function onGetSelectKelas(response){
        console.log(response);
        var tingkat='';
        // if(selected_key != undefined ) {
            // tingkat = $('[name="from_kelas[]"]')[selected_key].dataset.tingkat;
            
            var arr = selected_id.split(",");
            console.log(arr);
            if(jumlah_siswa == 0 && mode!='edit'){
                $.each(arr, function(selected_key){
                    //
                    tingkat = $('[name="from_kelas[]"]')[selected_key].dataset.tingkat;
                    // alert(tingkat);
                    $.each(response.data, function(key_2, value) {
                        console.log(this);
                        console.log(tingkat);
                        if(tingkat != ''){
                            if(tingkat!=value.tingkat_kelas){

                                if(tingkat=="X"){
                                    if(value.tingkat_kelas=="XI"){

                                        $('[name="kelas[]"]')[selected_key].append(new Option(value.tingkat_kelas+"-"+value.jurusan_kelas+" "+value.sub_tingkat_kelas, value.id_kelas));
                                        
                                        $('[name="kelas[]"]').find("option[value=" + value.id_kelas +"]").attr('data-tingkat', value.tingkat_kelas);
                                    }
                                }

                                if(tingkat=="XI"){
                                    if(value.tingkat_kelas=="XII"){
                                        $('[name="kelas[]"]')[selected_key].append(new Option(value.tingkat_kelas+"-"+value.jurusan_kelas+" "+value.sub_tingkat_kelas, value.id_kelas));
                                        $('[name="kelas[]"]').find("option[value=" + value.id_kelas +"]").attr('data-tingkat', value.tingkat_kelas);
                                    }
                                }
                            }
                        }
                    });
                    //
                })
                // renderIuran();
            }else{
                $.each(response.data, function(key_2, value) {
                    $('[name=kelas]').append(new Option(value.tingkat_kelas+"-"+value.jurusan_kelas+" "+value.sub_tingkat_kelas, value.id_kelas));
                    if(jumlah_siswa > 0 || mode=='edit') {
                        $('[name="kelas[]"]').append(new Option(value.tingkat_kelas+"-"+value.jurusan_kelas+" "+value.sub_tingkat_kelas, value.id_kelas));
                    }
                });

                $.each($('[name="kelas[]"]'), function(key){
                    $(this).val($(this).data('kelas'));//.trigger('change');
                });
                // renderIuran();
            }
        // }
        
    }
    function setKey_Iuran(key){
        selected_all=false;
        selected_key=key;
        renderIuran();
    }
    function setKey(key){
        selected_key=key;
    }
    function renderIuran(){
        var tbody;
        var tingkat
        if(selected_all==false){
            tbody = $("#table"+selected_key).find('tbody');
            // tingkat=$('[name="kelas[]"]').find(":selected").text();
            // tingkat=tingkat.split("-")[0];
            var sel_index=$('[name="kelas[]"]')[selected_key].options.selectedIndex;
            var tingkat=$('[name="kelas[]"]')[selected_key].options[sel_index].text; //$('[name="kelas[]"]')[selected_key].find(":selected").text();
            tingkat=tingkat.split("-")[0];
            // alert("OWN");
            // alert(tingkat);
            // $('[name="header_siswa[]"]')[selected_key].val(selected_key);
        }

        if(selected_all==true){
            //
            //
            tbody = $(".tbl-data-iuran").find('tbody');
            // tingkat = $('[name="kelas[]"]').find("option[value=" + kelas +"]").data('tingkat');
            tingkat=$('[name="kelas[]"]').find("option[value=" + kelas +"]:selected")[0].text;
            //alert("ALL");
            tingkat=tingkat.split("-")[0];
        }
        tbody.text('');
        tbody.append('<tr class="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        console.log(tingkat);
        ajaxGET(path_jenis_iuran + '/list_table?page='+1+'&'+"tingkat_jenis_iuran="+tingkat,'onRenderIuran','onGetListError');
    }
    function onRenderIuran(resp){
        var row = "";
        var obj = {"nama": "nama_jenis_iuran", "besar": "besar_jenis_iuran", "cicilan": "cicilan_jenis_iuran"};

        list_iuran.push(resp.data);

        $('.loading-row').remove();
        $('.no-data-row').remove();
        var tbody;
        if(selected_all==false){
            tbody = $("#table"+selected_key).find('tbody');
            // alert('#');
            row=render_row_def2(resp, obj, selected_key);
            row == "" ? tbody.html('<tr id="" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
        
        }
        if(selected_all==true){
            // tbody = $(".tbl-data-iuran").find('tbody');
            // alert('.');
            $.each($(".tbl-data-iuran"), function(key){

                tbody = $("#table"+key).find('tbody');

                row=render_row_def2(resp, obj, key);
                
                row == "" ? tbody.html('<tr id="" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
        
            });
            
           
        }
        return row;
    }

    function render_row_def2(resp,obj, selected_key){
        var sel_index=$('[name="kelas[]"]')[selected_key].options.selectedIndex;
        var tingkat=$('[name="kelas[]"]')[selected_key].options[sel_index].text; //$('[name="kelas[]"]')[selected_key].find(":selected").text();
        tingkat=tingkat.split("-")[0];
        //
        if(mode=="edit") resp=resp;
        else resp = resp.data;
        var row='';
        $.each(resp,function(key,value){
            console.log(this);
            //if($('[name="kelas[]"]')[selected_id].find("option[value=" + value.id_kelas +"]").data('tingkat') == this.tingkat_jenis_iuran || this.tingkat_jenis_iuran=="ALL"){
            row += '<tr class="data-row" id="row-'+key+'">';
                row += '<td>'+(key+1)+'</td>';
                row += '<td class="d-none"><input name="header_siswa[]" type="text" value="'+selected_key+'"></td>';
                row += '<td class="d-none"><input name="kelas_siswa[]" type="text" value="'+tingkat+'"></td>';
                row += '<td><input class="border-radius-0 bg-light" name="jenis_iuran[]" type="text" value="'+value.nama_jenis_iuran+'" readonly="TRUE"></td>';
                row += '<td><input class="border-radius-0" name="besar_iuran[]" type="number" value="'+value.besar_jenis_iuran+'"></td>';
            row += '</tr>';
            //}
        });
        return row;
    }

    function display(page = 1, params=''){
        $('#more-button-row').remove();
        var tbody = $("#tbl-data").find('tbody');
        // if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
        if($( "#tahun_ajaran" ).val() != ''){
            tbody.text('');
        }
        
        tbody.append('<tr id="loading-row"><td colspan="7"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        console.log(selected_id != '' && selected_id !=-1);
        // params = $( "#search-form" ).serialize();
        //jgn difilter kalao mau edit
        if(selected_id != '' && selected_id !=-1){
            params += "filter_tahun_ajaran="+tahun_ajaran;
            ajaxGET(path + '/list_siswa_kelas_table_new?page='+page+'&'+params,'onGetRenderFormSuccess','onGetListError');
        }else{
            params += "filter_tahun_ajaran="+$( "#tahun_ajaran" ).val();//+"&tingkat_kelas="+$("#tbl-data-kelas").find('tbody tr.bg-info').data('tingkat');
            ajaxGET(path + '/list_siswa_kelas_table_new?page='+page+'&'+params,'onGetListSuccess','onGetListError');
        }
    }

    function onGetListSuccess(response){
        console.log(response);
        $('#loading-row').remove();
        $('#no-data-row').remove();
        
        var tbody = $("#tbl-data").find('tbody');
        
        var row = "";
        var num = $('.data-row').length+1;
        $.each(response.data,function(key,value){
            row += render_row(value, num);
            num++;
        });
        row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="7"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
    }

    function render_row(value, num){
    //  console.log(value);
        var row = "";
        row += '<tr contenteditable="false" class="data-row" id="row-'+value.id_siswa+'">';
        row+='<td align="center">';
        row+='<div class="form-check">';
            row+='<input class="form-check-input" style="position:unset" type="checkbox" value='+value.id_siswa+' name="tekan_saja" required>';
        row+='</div>';
        row+='</td>';
        // row += '<td>'+(num)+'</td>';
        row += '<td class="">'+value.ref_siswa+'</td>';
        // row += '<td class="">'+value.nis_siswa+'</td>';
        row += '<td class="">'+value.nama_lengkap_siswa+'</td>';
        row += '<td class="">'+value.tingkat_kelas+" "+value.jurusan_kelas+" "+value.sub_tingkat_kelas+'</td>';
        row += '<td class="">'+value.jenis_kelamin_siswa+'</td>';
        row += '<td class="">'+value.tahun_ajaran_siswa+'</td>';
        row += '<td class="">'+value.tahun_angkatan_siswa+'</td>';
        // row += '<td class="">'+"-"+'</td>';
        row += '<td class="">';
        row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-info pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'edit\')"><i class="fas fa-pencil-alt"></i></a> ';
        row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'delete\')"><i class="fas fa-trash-alt"></i></a>';
        row += '</td>';
        row += '</tr>';
        return row;
    }

    function onGetRenderFormSuccess(response){
        var myvar="";
        var arr = selected_id.split(",");
        var opt='';
        var tingkat='';
        if(selected_key >= 0 ) {
            tingkat = $('[name="from_kelas[]"]')[selected_key].dataset.tingkat;

        }
        $.each(response.data, function(key, value){
            
            $.each(arr, function(key_2){
                if(value.id_siswa == this){
                    console.log(value);
                   //onclick="setKey('+key_2+');" 
                 myvar += '<div class="col-md-auto">'+
                '            <div class="card bg-head pill p-1 mt-1">'+
                '            <div id="card'+key_2+'" class="card-body">'+
                '             <h4 class="card-title" style="font-family: segoeui;">Data Siswa Ke '+(key_2+1)+'</h4>'+
                '              <hr>'+
                '              <div class="form-group row"> '+
                '                <div class="col-sm-12">'+
                '                  <input name="ref[]" type="text" class="form-control" id="" placeholder="NO.REF" value="'+value.ref_siswa+'">'+
                '                </div>'+
                '              </div>'+
                '              <div class="form-group form-row">        '+
                '                <div class="col-md-10">'+
                '                  <input name="nama_lengkap[]" type="text" class="form-control" id="" placeholder="Nama Siswa" value="'+value.nama_lengkap_siswa+'">'+
                '                </div>'+
                '                <div class="col-md-2">';
                if(mode=='edit'){
                myvar +='          <select name="jenis_kelamin[]" data-jk="'+value.jenis_kelamin_siswa+'" class="form-control select-style pl-2">'+
                '                      <option value="">Jenis Kelamin</option>';
                myvar +='              <option value="LAKI_LAKI" '+(value.jenis_kelamin_siswa=="LAKI_LAKI"?"selected":"")+'>LAKI_LAKI</option>';
                myvar +='              <option value="PEREMPUAN" '+(value.jenis_kelamin_siswa=="PEREMPUAN"?"selected":"")+'>PEREMPUAN</option>';

                myvar +='          </select>';
                }else{
                myvar +='                     <input name="jenis_kelamin[]" type="text" class="form-control" id="" placeholder="" value="'+value.jenis_kelamin_siswa[0]+'" disabled>';
                }
                myvar +='                </div>      '+
                '              </div>';
                if(mode=='edit'){
                    myvar +='   <div class="form-group row"> '+
                '                <div class="col-sm-6">'+
                '                  <select data-id="'+(key_2+1)+'" data-kelas="'+value.id_kelas+'" name="kelas[]" class="form-control select-styless">'+
                '                      <option value="">Pilih Kelas</option>'+
                '                  </select>'+
                '                </div>   '+
                '                <div class="col-sm-6">'+
                '                  <select data-id="'+(key_2+1)+'" name="tahun_ajaran[]" class="form-control select-styless">'+
                '                      <option value="">Pilih Ajaran Baru</option>'+
                '                  </select>'+
                '                </div>'+
                '              </div>';
                }else{

                    myvar +='      <div class="form-group row"> '+
                    '                <div class="col-sm-6">'+
                    '                  <input data-id="'+(key_2+1)+'" onclick="setKey('+key_2+');" name="from_kelas[]" data-tingkat="'+value.tingkat_kelas+'" type="text" class="form-control" id="" value="'+value.tingkat_kelas+"-"+value.jurusan_kelas+" "+value.sub_tingkat_kelas+'" >'+
                    '                </div>'+
                    '                <div class="col-sm-6">'+
                    '                  <select data-id="'+(key_2+1)+'" name="kelas[]" class="form-control select-styless">'+
                    '                      <option value="">Pilih Kelas Baru</option>'+
                    '                  </select>'+
                    '                </div>   '+
                    '              </div>';
                }
                if(mode!='edit'){
                myvar +='        <div class="form-group row">        '+
                '                <div class="col-sm-6">'+
                '                  <input name="from_tahun_ajaran[]" type="text" class="form-control" id="" placeholder="Tahun Ajaran" value="'+value.tahun_ajaran+'" disabled>'+
                '                </div>'+
                '                <div class="col-sm-6">'+
                '                  <select data-id="'+(key_2+1)+'" name="tahun_ajaran[]" class="form-control select-styless">'+
                '                      <option value="">Pilih Ajaran Baru</option>'+
                '                  </select>'+
                '                </div>'+
                '              </div>';
                }
                myvar +='        <div class="form-group row">'+
                '                <div class="col-sm-12">'+
                '                  <div class="table-responsive">'+
                '                    <table id="table'+key_2+'" class="table table-sm table-hovered table-hover tbl-data-iuran" style="font-family: segoeui;border-radius: 20px!important;overflow: hidden;padding: 10px!important;">'+
                '                        <thead class="text-white" style="background-color: #487d95!important;">'+
                '                          <tr>'+
                '                            <th scope="col">#</th>'+
                '                            <th scope="col">Jenis Iuran</th>'+
                '                            <th scope="col">Besar</th>'+
                '                          </tr>'+
                '                        </thead>'+
                '                        <tbody>';
                //                         $.each(value.list_iuran, function(key, value_iuran){
                // myvar +='                  <tr>'+
                // '                            <td>'+(key+1)+'</td>'+
                // '                            <td>'+value_iuran.jenis_iuran_siswa_detil+'</td>'+
                // '                            <td>'+value_iuran.besar_iuran_siswa_detil+'</td>'+
                // '                          </tr>';    
                //                         });
                //myvar +=onRenderIuran(value.list_iuran);
                            $.each(value.list_iuran,function(key){
                                console.log(this);
                                //if($('[name="kelas[]"]')[selected_id].find("option[value=" + value.id_kelas +"]").data('tingkat') == this.tingkat_jenis_iuran || this.tingkat_jenis_iuran=="ALL"){
                                myvar += '<tr class="data-row" id="row-'+key+'">';
                                    myvar += '<td>'+(key+1)+'</td>';
                                    myvar += '<td class="d-none"><input name="header_siswa[]" type="text" value="'+this.header_id_siswa_detil+'"></td>';
                                    myvar += '<td class="d-none"><input name="kelas_siswa[]" type="text" value="'+this.tingkat_siswa_detil+'"></td>';
                                    myvar += '<td><input class="border-radius-0 bg-light" name="jenis_iuran[]" type="text" value="'+this.jenis_iuran_siswa_detil+'" readonly="TRUE"></td>';
                                    myvar += '<td><input class="border-radius-0" name="besar_iuran[]" type="number" value="'+this.besar_iuran_siswa_detil+'"></td>';
                                myvar += '</tr>';
                                //}
                            });
                myvar +='               </tbody>'+
                '                    </table>'+
                '                    </div>'+
                '                  </div>'+
                '              </div>  '+
                '              <hr>'+
                '              </div>'+
                '              </div>'+
                '            </div>';
                    
                }
            })
        })
        $('#form-container').html(myvar);
        initFunctionKelas(true);
    }

    function onGetListError(response) {
        alert("ERROR");
        $('#ui-view').html($('#ui-view-msg-error').html());
    }

    function save(){
        var obj = new FormData(document.querySelector('#form'));
        if(selected_id != '') obj.append('id', selected_id);
        obj.append('kelas', $('#tingkat').val()+' '+$('#jurusan').val()+' '+$('#sub_tingkat').val());
        obj.append('list_iuran', JSON.stringify(list_iuran[0]));
        
        ajaxPOST(path + '/save',obj,'onActionSuccess','onSaveError');
    }

    function onSaveSuccess(response){
        console.log(response);
        $('#msg-info-body').html('Data telah berhasil di update.');
        $('#msg-info').modal('show');
        // setTimeout(function(){ window.location.href="pengguna?vmenu=daftar"; }, 1000);
    }
    function onActionSuccess(response){
        // display();
        // $('#modal-form').modal('hide');
        // $('#msg-confirm').modal('hide');
        // selected_action = '';
        console.log(response);
        swal("Message", response.message, "success", {
          buttons: {
            cancel: "Kembali ke daftar siswa",
            go: {
              text: "Tambah lagi data siswa",
              value: "go",
            }//,
            // back: {
            //   text: "Kembali ke daftar siswa",
            //   value: "back",
            // }
          },
        })
        .then((value) => {
          switch (value) {
         
            case "go":
              window.location.href=ctx+"page/form_siswa_view";
              break;
         
            // case "back":
            //   window.location.href=ctx+"page/siswa_view";
            //   break;
         
            default:
              window.location.href=ctx+"page/siswa_view";
          }
        });
    }
    function doAction(id, action, confirm = 0){
        selected_action = action;
        $('#modal-form-msg').hide();
        $('#msg-confirm-alert').hide();
        if(confirm == 0) ajaxGET(path + '/get_by_id/'+id+'?action='+selected_action,'onPrepareActionSuccess','onError');
        // else ajaxPOST(path + '/do_action/'+id+'/'+selected_action,{},'onActionSuccess','onError');
        else ajaxPOST(path + '/delete_all/'+id,{},'onActionSuccess','onError');
    }

    function onPrepareActionSuccess(response) {
        console.log(response);
        var value = response.data;
        selected_id = value.id_siswa;
        if(selected_action == 'edit'){
            fillFormValue(value);
            $('#modal-form').modal('show');
        }else{
            Swal.fire({
              title: 'Are you sure?',
              text: response.message,
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                doAction(selected_id,'delete', 1);
                Swal.fire(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                display(1);
              }
            })
        }
    }
    

    function fillFormValue(value){
        $('#form')[0].reset();
        
        $('[name=nis_siswa]').val(value.nis_siswa);
        $('[name=nama_siswa]').val(value.nama_siswa);
        $('[name=kelas_siswa]').val(value.kelas_siswa);
    }

	function add() {
        selected_id = ''; // wajib
        selected_action = 'add'; // wajib
	    $('#form')[0].reset(); // reset form on modals
	    $('.form-group').removeClass('has-error'); // clear error class
	    $('.help-block').empty(); // clear error string
	    $('#modal-form').modal('show'); // show bootstrap modal
	    $('.modal-title').text('Tambah Siswa'); // Set Title to Bootstrap modal title
        $('#form-pass').removeClass("d-none");

	}

    function getTahunAjaranAll(){
        var strdate=[];
        var angkatan= new Date("July 10, 2017");
        // var date = new Date("December 10, 2019");//+tahun_ajaran[0]
        var date = new Date();
        var selisih=(date.getFullYear()-angkatan.getFullYear());
        for(var start_ajaran=angkatan.getFullYear(); start_ajaran < angkatan.getFullYear()+selisih; start_ajaran++){
            if(start_ajaran+"/"+(start_ajaran+1)!=date){
                strdate.push(start_ajaran+"/"+(start_ajaran+1).toString());
                $('#tahun_ajaran').append(new Option(start_ajaran+"/"+(start_ajaran+1).toString(), start_ajaran+"/"+(start_ajaran+1).toString()));
                $('[name="tahun_ajaran[]"]').append(new Option(start_ajaran+"/"+(start_ajaran+1).toString(), start_ajaran+"/"+(start_ajaran+1).toString()));
                
            }
        }
        return strdate;
    }

    function filter(params){
        display(1, params);
    }



    function goToEdit(){
        var id = $('#tbl-data').find('input:checked');
        var arr=[];
        $.each(id, function(key){
            arr.push(this.value);
        });
        if(arr=='') {
            var resp_msg={"title" : "Tidak dapat menambah data", "body" : "Karena anda belum memilih data siswa!", "icon"  : "warning"};
            showAlertMessage(resp_msg, 1500);
        }else{
            window.location.href=ctx+'page/form_siswa_view?id='+arr.toString();
        }
    }

