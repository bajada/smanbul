	var path = ctx + 'UserController';
    var params='';
    var selected_action='';
    var selected_id='';

    function init() {
        display();

        $('#search-form').submit(function(e){
            display();
            e.preventDefault();
        });
        
        $('#btn-yes-msg-confirm').click(function(e){
            if(selected_action == 'delete' ){
                doAction(selected_id, selected_action, 1);
            }
            
        });

        $('#modal-form').on('hidden.bs.modal', function (e) {
                $('#main-cont').attr('style', '');
        });

        $('#modal-form').on('shown.bs.modal', function (e) {
            $('#main-cont').attr('style', 'filter: blur(8px);-webkit-filter: blur(8px);');
        });
    };


    function display(page = 1){
        $('#more-button-row').remove();
        var tbody = $("#tbl-data").find('tbody');
        if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
            tbody.text('');
        }
        
        tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        params = $( "#search-form" ).serialize();
        
        ajaxGET(path + '/list_table?page='+page+'&'+params,'onGetListSuccess','onGetListError');
    }

    function onGetListSuccess(response){
        console.log(response);
        $('#loading-row').remove();
        $('#no-data-row').remove();
        
        var tbody = $("#tbl-data").find('tbody');
        
        var row = "";
        var num = $('.data-row').length+1;
        $.each(response.data,function(key,value){
            row += render_row(value, num);
            num++;
        });
        // if(response.next_more){
        //     row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
        //     row += '    <button class="btn waves-effect green sh" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
        //     row += '</div></td></tr>';
        // }
        row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
        
    }

    function render_row(value, num){
    //  console.log(value);
        var row = "";
        row += '<tr class="data-row" id="row-'+value.id_user+'">';
        row += '<td>'+(num)+'</td>';
        row += '<td class="">'+value.username_user+'</td>';
        row += '<td class="">'+value.firstname_user+'</td>';
        row += '<td class="">'+value.lastname_user+'</td>';
        row += '<td class="">'+value.role_user+'</td>';
        // row += '<td class="">'+value.status_user+'</td>';
        row += '<td class="">';
        row += '<a href="javascript:void(0)" class="btn btn-sm btn-square pill btn-info" type="" onclick="doAction(\''+value.id_user+'\',\'edit\')"><i class="fas fa-pencil-alt"></i></a> ';
        row += '<a href="javascript:void(0)" class="btn btn-sm btn-square pill btn-danger" type="" onclick="doAction(\''+value.id_user+'\',\'delete\')"><i class="fas fa-trash-alt"></i></a>';
        row += '</td>';
        row += '</tr>';
        return row;
    }

    function onGetListError(response) {
        $('#ui-view').html($('#ui-view-msg-error').html());
    }

    function save(){
        var obj = new FormData(document.querySelector('#form'));
        if(selected_id != '') obj.append('id', selected_id);
        ajaxPOST(path + '/save',obj,'onActionSuccess','onSaveError');
    }

    function savePassword(){
        var obj = new FormData(document.querySelector('#pass-form'));
        if(selected_id != '') obj.append('id', selected_id);
        if($('[name=password]').val() != $('[name=password_2]').val() || $('[name=password]').val() == ''){
                alert('Password tidak boleh kosong dan pengulangannya harus sama');
                return;
        }
        ajaxPOST(path + '/update-password',obj,'onSavePasswordSuccess','onError');
    }

    function onSavePasswordSuccess(response){
        $('#msg-info-body').html('Password baru telah berhasil di update.');
        $('#msg-info').modal('show');
        setTimeout(function(){ window.location.href="pengguna?vmenu=daftar"; }, 1000);
    }

    function onSaveError(response){
        console.log(response);
        $('#modal-form-msg').text(response.responseJSON.message);
        $('#modal-form-msg').show();
    }

    function onActionSuccess(response){
        display();
        $('#modal-form').modal('hide');
        $('#msg-confirm').modal('hide');
        selected_action = '';
        console.log(response);
        // swal("Message",response.message,"success");
        Swal.fire({
            timer: 2000,
            type: 'success',
            title: 'Berhasil!',
            text: response.message,
        });
    }

    function doAction(id, action, confirm = 0){
        selected_action = action;
        $('#modal-form-msg').hide();
        $('#msg-confirm-alert').hide();
        if(confirm == 0) ajaxGET(path + '/get_by_id/'+id+'?action='+selected_action,'onPrepareActionSuccess','onError');
        else ajaxPOST(path + '/do_action/'+id+'/'+selected_action,{},'onActionSuccess','onError');
    }

    function onPrepareActionSuccess(response) {
        console.log(response);
        var value = response.data;
        selected_id = value.id_user;
        if(selected_action == 'edit'){
            fillFormValue(value);
            $('#modal-form').modal('show');
        }else{
            Swal.fire({
              type: 'warning',
              title: 'Are you sure?',
              text: response.message,
              //footer: '<a href>Why do I have this issue?</a>'
              showCancelButton: true,
              showConfirmButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              // confirmButtonText: 'Tutup!'
            })
            .then((value) => {
                if(value.value){
                    doAction(selected_id,'delete', 1);
                }
            });
        }
    }

    function fillFormValue(value){
        $('#form')[0].reset();
        // $('#form-pass').addClass("d-none");
        
        $('[name=username_user]').val(value.username_user);
        $('[name=firstname_user]').val(value.firstname_user);
        $('[name=lastname_user]').val(value.lastname_user);
        $('[name=role_user]').val(value.role_user).selectric('refresh');
    }

    function clearForm(){
        $('#form')[0].reset(); // reset form on modals
        $('#modal-form-msg').hide();
    }

	function add() {
        selected_id = ''; // wajib
        selected_action = 'add'; // wajib
	    $('#form')[0].reset(); // reset form on modals
	    $('.form-group').removeClass('has-error'); // clear error class
	    $('.help-block').empty(); // clear error string
	    $('#modal-form').modal('show'); // show bootstrap modal
	    $('.modal-title').text('Tambah pengguna'); // Set Title to Bootstrap modal title
        $('#form-pass').removeClass("d-none");

        $('[name=role_user]').val(0).selectric('refresh');
        clearForm();
	}

    function editPass(id) {
        console.log(id);
        save_method = 'updatePass';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#form-karyawan').addClass("hidden");
        $('#form-username').addClass("hidden");
        $('#form-pass').removeClass("hidden");
        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('user/edit_table/')?>" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {

                $('[name="id"]').val(data.user_id);
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit User'); // Set title to Bootstrap modal title

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }