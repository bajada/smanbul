	var path = ctx + 'LaporanController';
    var path_jenis_iuran = ctx + 'JenisIuranController'; 
    var path_kelas = ctx + 'KelasController'; 
    var params='';
    var selected_action='';
    var selected_id='';
    var selected_view='';
    var list_iuran = [];
    var jumlah_siswa=0;
    var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    function init() {

        selected_view = ($.urlParam('view') == null ? '' : $.urlParam('view'));

        //tambahkan validasi, jika form halaman  bukan daftar maka jangan runing ini
        // display(1);
        $('#search-form').submit(function(e){
            if($('[name=filter_keyword]').val() != ""){
                if(selected_view=='harian'){
                    displayHarian();
                }else if(selected_view=='mingguan'){
                    display();
                }
            }else{
                var resp_msg={"title" : "Message", "body" : "Tanggal pencarian wajib diisi!", "icon"  : "warning"};
                showAlertMessage(resp_msg, 1200);
            }
            e.preventDefault();
        });

        $('#btn-export').click(function(){
            var params="";
            params=$('#search-form').serialize();
            window.location.href=ctx+"LaporanExcel/per_tanggal?"+params;
        });
        

        //format tgl mm/dd/yyyy
        console.log(date_diff_indays('07/22/2019', '07/28/2019'));
        console.log(date_diff_inweeks('07/22/2019', '07/29/2019'));

     //    $(function(){
     //     $(".datepicker").datepicker({
     //        format: 'yyyy-mm-dd',
     //        autoclose: true,
     //        todayHighlight: true,
     //    });
     //    $("#tgl_mulai").on('changeDate', function(selected) {
     //        var startDate = new Date(selected.date.valueOf());
     //        $("#tgl_akhir").datepicker('setStartDate', startDate);
     //        if($("#tgl_mulai").val() > $("#tgl_akhir").val()){
     //          $("#tgl_akhir").val($("#tgl_mulai").val());
     //        }
     //    });
     // });

        $(function() {

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                // $('#reportrangestart')
                // $('#reportrangestart').val(start.format('MM/D/YYYY'));
               
                $('#reportrangestart').val(start.format('MM/DD/YYYY'));
                $('#reportrangeend').val(end.format('MM/DD/YYYY'));
                // $('#reportrangestart').val(start.format('YYYY-MM-DD'));
                // $('#reportrangeend').val(end.format('YYYY/MM-DD'));
            }

            $('#reportrangestart').daterangepicker({
                //singleDatePicker: true,
                // "locale": {
                // "format": "MM/DD/YYYY",
                // "separator": " = ",
                // },
                startDate: start,
                endDate: end,
                ranges: {
                   'Today': [moment(), moment()],
                   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                   'This Month': [moment().startOf('month'), moment().endOf('month')],
                   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);
        });

        $('#bg-head').hide();
    };

    var date_diff_indays = function(date1, date2) {
    dt1 = new Date(date1);
    dt2 = new Date(date2);
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) / (1000 * 60 * 60 * 24));
    }

    var date_diff_inweeks = function(date1, date2) {
    dt1 = new Date(date1);
    dt2 = new Date(date2);
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) / (1000 * 60 * 60 * 24) / 7);
    }

    function exportExcel(){
        var params="";
        params=$('#search-form').serialize();
        //
            let timerInterval
            Swal.fire({
              title: 'Loading..',
              html: 'Mohon tunggu hingga popup download muncul, jika popup download telah muncul, silahkan abaikan pesan ini.',
            //   timer: 2000,
            //   onBeforeOpen: () => {
            //     Swal.showLoading()
            //     timerInterval = setInterval(() => {
            //       Swal.getContent().querySelector('strong')
            //         .textContent = Swal.getTimerLeft()
            //     }, 100)
            //   },
            //   onClose: () => {
            //     clearInterval(timerInterval)
            //   }
            // }).then((result) => {
            //   if (
            //     /* Read more about handling dismissals below */
            //     result.dismiss === Swal.DismissReason.timer
            //   ) {
            //     console.log('I was closed by the timer')
            //   }
            })
        //
        if(selected_view=='harian'){
            window.location.href=ctx+"LaporanExcel/per_tanggal?"+params;
        }else if(selected_view=='mingguan'){
            window.location.href=ctx+"LaporanExcel/per_tanggal?"+params;
        }
    }

    function trychange(){
        var fav=$('#reportrangestart').val();
        $('#reportrangestart').val(fav.split(" ")[0]);
    }


    function displayHarian(page = 1, param=''){
        $('#more-button-row').remove();
        var tbody = $("#tbl-data").find('tbody');
        var body=$('#tbl-data-rekap').find('tbody');
        // if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
        tbody.text('');
        body.text('');
        // }/
        
        tbody.append('<tr id="loading-row"><td colspan="10"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        body.append('<tr id="loading-row-2"><td colspan="10"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        params = $( "#search-form" ).serialize();
        ajaxGET(path + '/list_table?page='+page+'&'+params,'onGetListSuccess','onGetListError');
    }

    function display(page = 1, param=''){
        $('#more-button-row').remove();
        var tbody = $("#tbl-data").find('tbody');
        var body=$('#tbl-data-rekap').find('tbody');
        // if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
        tbody.text('');
        body.text('');
        // }/
        //
        var resp_msg={"title" : "Message", "body" : '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>', "icon"  : "warning"};
        Swal.fire({
          title: 'Sedang proses',
          text: 'Mohon tunggu, data sedang di proses',
          animation: true,
          customClass: 'animated tada',
          allowOutsideClick: false,
          onBeforeOpen: () => {
            Swal.showLoading()
          },
        });
        //
        tbody.append('<tr id="loading-row"><td colspan="10"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        body.append('<tr id="loading-row-2"><td colspan="10"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        if(param=='') {
            params = $( "#search-form" ).serialize();
            ajaxGET(path + '/list_table?page='+page+'&'+params,'onGetListTabSuccess','onGetListError');
        }else{
            params="filter_keyword="+param;
            ajaxGET(path + '/list_table?page='+page+'&'+params,'onGetListSuccess','onGetListError');
        } 
    }
    function onGetListTabSuccess(response){

        Swal.close();
        console.log(response);
        var row = "";
        var myvar = "";
        var wraper_sheet=$('#wrap-sheet');
        var wraper_content=$('#pills-tabContent');
        var new_key=0;
        var html = [];
        $.each(response.data.laporan_uniq, function(key) {

            var name = this.real_tanggal_iuran;
            // if(html.indexOf(name) == -1) html.push(name);
            // else html.push("");

            // console.log(html[key]);
            // alert(html[key]);
            //if(html[key]){
                console.log(this);
                if(new_key==0) active="active show";
                else active="";

                row +=  '<li class="nav-item">'+// onclick="display(\'1\', \''+this.tanggal_bulanan+'\')" 
                            '<a id="btn-date-'+new_key+'" class="nav-link '+active+'" data-toggle="pill" href="#pills-date-'+new_key+'">'+this.real_tanggal_iuran+'</a>'+
                        '</li>';

                var tanggal_bulanan = this.real_tanggal_iuran;
                //create body and table x date
                myvar += '<div class="tab-pane fade '+active+'" id="pills-date-'+new_key+'">'+
                '            <div class="row">'+
                '              <div class="col-md-12">'+
                '                  <div class="">'+
                '                    <table id="tbl-data-'+new_key+'" class="table table-striped table-bordered bg-light"  style="font-family: segoeui;opacity: 0.8;display: table;">'+
                '                      <thead class="text-white text-center" style="background-color: #487d95!important;">'+
                '                        <tr>'+
                '                          <th width="1%" rowspan="2" class="align-middle">No</th>'+
                '                          <th width="5%" rowspan="2" class="align-middle">REF</th>'+
                '                          <th width="15%" rowspan="2" class="align-middle">NAMA</th>'+
                '                          <th width="5%" rowspan="2" class="align-middle">KELAS</th>'+
                '                          <th width="10%" colspan="2">IURAN BULANAN</th>'+
                '                          <th width="15%" colspan="2">TABUNGAN BULANAN</th>'+
                '                          <th width="5%" rowspan="2" class="align-middle">DPMP</th>'+
                '                          <th width="5%" rowspan="2" class="align-middle">OSIS</th>'+
                '                          <th width="5%" rowspan="2" class="align-middle">TOTAL</th>'+
                '                          <th width="10%" rowspan="2" class="align-middle">KET</th>'+
                '                        </tr>'+
                '                        <tr>'+
                '                          <th width="5%">BULAN</th>'+
                '                          <th width="5%">JUMLAH</th>'+
                '                          <th width="5%">BULAN</th>'+
                '                          <th width="5%">JUMLAH</th>'+
                '                        </tr>'+
                '                      </thead>'+
                '                      <tbody class="text-dark small">';
                //
                var no=1;
                var sum_besar_iuran_x=0;
                var sum_besar_iuran_xi=0;
                var sum_besar_iuran_xii=0;

                var sum_besar_dpmp_x=0;
                var sum_besar_dpmp_xi=0;
                var sum_besar_dpmp_xii=0;

                var sum_besar_tabungan_x=0;

                var sum_besar_iuran=0;
                var sum_besar_tabungan=0;
                var sum_besar_dpmp=0;
                var sum_besar_total=0;
                var sum_besar_du=0;
                //
                // var row_tingkat_kelas_x=[];
                // var row_tingkat_kelas_xi=[];
                // var row_tingkat_kelas_xii=[];

                
                var real_tanggal_iuran = this.real_tanggal_iuran.split("-");
                //
                var tahun_sekarang = new Date();
                tahun_sekarang.setFullYear(real_tanggal_iuran[0]);
                tahun_sekarang.setMonth(real_tanggal_iuran[1]-1);
                tahun_sekarang.setDate(real_tanggal_iuran[2]);
                // alert(tahun_sekarang);
                tahun_sekarang=tahun_sekarang.getFullYear();
                var selisih = tahun_sekarang - 2;
                var arrayOsis=[];
                var obj_osis={};
                for(var tahun_ajaran_i=selisih; tahun_ajaran_i <= tahun_sekarang; tahun_ajaran_i++){
                    obj_osis = {
                        'tahun' : tahun_ajaran_i,
                        'tahun_ajaran' : tahun_ajaran_i+"/"+(tahun_ajaran_i+1),
                        'total_du' : 0
                    };
                    arrayOsis.push(obj_osis);
                }

                console.log(arrayOsis);
                //
                $.each(response.data.laporan,function(key, value){
                     var real_tanggal_iuran = this.real_tanggal_iuran.split("-");
                    //if(value.du != "0.00" || value.dpmp != "0.00" || value.spp != "0.00"){   
                        // row += render_row(value, num);
                        if(this.real_tanggal_iuran==tanggal_bulanan){
                            var tingkat_kelas = value.kelas;
                            tingkat_kelas = tingkat_kelas.split(" ")[0];

                            if(tingkat_kelas=="X") {
                                sum_besar_iuran_x+=parseInt(value.total_besar_iuran_bulanan);
                                sum_besar_dpmp_x+=parseInt(value.total_besar_iuran_dpmp);
                                sum_besar_tabungan_x+=parseInt(value.total_besar_iuran_tabungan);
                            }
                            if(value.keterangan_iuran=="TABUNGAN KELAS X") sum_besar_tabungan_x+=parseInt(value.total_besar_iuran_tabungan);
                            if(tingkat_kelas=="XI") {
                                sum_besar_iuran_xi+=parseInt(value.total_besar_iuran_bulanan);
                                sum_besar_dpmp_xi+=parseInt(value.total_besar_iuran_dpmp);
                            }
                            if(tingkat_kelas=="XII") {
                                sum_besar_iuran_xii+=parseInt(value.total_besar_iuran_bulanan);
                                sum_besar_dpmp_xii+=parseInt(value.total_besar_iuran_dpmp);
                            }

                            var keterangan_iuran = value.keterangan_iuran;
                            if(keterangan_iuran!=""){
                                var ta="";
                                // $ta= explode(" ", $value->keterangan_iuran)[1];
                                var ta_from_ket = keterangan_iuran.split(" ")[1];
                                var ta_from_tgl_iuran  = new Date();
                                ta_from_tgl_iuran.setFullYear(real_tanggal_iuran[0]);
                                ta_from_tgl_iuran.setMonth(real_tanggal_iuran[1]-1);
                                ta_from_tgl_iuran.setDate(real_tanggal_iuran[2]);
                                var new_ta_from_tgl_iuran;
                                if(ta_from_tgl_iuran.getMonth() >= 6) new_ta_from_tgl_iuran=ta_from_tgl_iuran.getFullYear()+"/"+(ta_from_tgl_iuran.getFullYear()+1);
                                else new_ta_from_tgl_iuran=(ta_from_tgl_iuran.getFullYear()-1)+"/"+(ta_from_tgl_iuran.getFullYear());
                                
                                if(value.keterangan_iuran!="REGISTRASI_ULANG") ta = ta_from_ket;
                                else ta = new_ta_from_tgl_iuran;

                                $.each(arrayOsis, function(key_osis,value_osis){
                                    if(value_osis.tahun_ajaran==ta) {
                                        arrayOsis[key_osis].total_du += parseInt(value.total_besar_iuran_du);
                                    }
                                    /*
                                    if($value->keterangan_iuran!=""){
                                        $ta="";
                                        if($value->keterangan_iuran!="REGISTRASI_ULANG") $ta= explode(" ", $value->keterangan_iuran)[1];
                                        else $ta= $tahun_ajaran;
                                        foreach ($arrayOsis as $key_osis => $value_osis) {
                                            if($value_osis['tahun_ajaran']==$ta) {
                                                $arrayOsis[$key_osis]['total_du'] += $value->total_besar_iuran_du;
                                            }
                                        }
                                    }
                                    */
                                })
                                
                            }
                            console.log(arrayOsis);
                            myvar += '<tr contenteditable="false" class="data-row" id="row-'+value.id_siswa+'">';
                                myvar += '<td>'+(no)+'</td>';
                                myvar += '<td class="">'+value.ref_siswa+'</td>';
                                myvar += '<td class="">'+value.nama_lengkap_siswa+'</td>';
                                myvar += '<td class="">'+value.kelas+'</td>';
                                myvar += '<td class="">'+(value.bulan_iuran_bulanan!=null?value.bulan_iuran_bulanan:"")+'</td>';
                                myvar += '<td>'+$.number(value.total_besar_iuran_bulanan)+'</td>';
                                myvar += '<td class="">'+(value.bulan_iuran_tabungan!=null?value.bulan_iuran_tabungan:"")+'</td>';
                                myvar += '<td>'+$.number(value.total_besar_iuran_tabungan)+'</td>';
                                myvar += '<td>'+$.number(value.total_besar_iuran_dpmp)+'</td>';
                                myvar += '<td>'+$.number(value.total_besar_iuran_du)+'</td>';
                                myvar += '<td>'+$.number(value.total_bulanan_tabungan_dpmp_du)+'</td>';
                                myvar += '<td class="">'+value.keterangan_iuran+'</td>';
                            myvar += '</tr>';
                            no++;
                            sum_besar_iuran+=parseInt(value.total_besar_iuran_bulanan);
                            sum_besar_tabungan+=parseInt(value.total_besar_iuran_tabungan);
                            sum_besar_dpmp+=parseInt(value.total_besar_iuran_dpmp);
                            sum_besar_du+=parseInt(value.total_besar_iuran_du);
                            sum_besar_total+=parseInt(value.total_bulanan_tabungan_dpmp_du);
                        }
                    //}
                });
                //
                myvar += '             </tbody>'+
                '                      <tfoot class="text-white" style="background-color: #487d95!important;">'+
                '                        <tr>'+
                '                          <th class="text-center" colspan="5">Jumlah</th>'+
                '                          <th id="jumlah-iuran-'+new_key+'">'+$.number(sum_besar_iuran)+'</th>'+
                '                          <th></th>'+
                '                          <th id="jumlah-tabungan-'+new_key+'">'+$.number(sum_besar_tabungan)+'</th>'+
                '                          <th id="jumlah-dpmp-'+new_key+'">'+$.number(sum_besar_dpmp)+'</th>'+
                '                          <th id="jumlah-du-'+new_key+'">'+$.number(sum_besar_du)+'</th>'+
                '                          <th id="jumlah-total-'+new_key+'">'+$.number(sum_besar_total)+'</th>'+
                '                          <th width="10%"></th>'+
                '                        </tr>'+
                '                      </tfoot>'+
                '                    </table>'+
                '                  </div>'+
                '              </div>'+
                '            </div>'+
                '           <div class="row justify-content-end">'+

                //batas col-md
                '              <div class="col-md-3">'+
                '                  <div class="">'+
                '                    <table id="tbl-data-rekap-'+new_key+'" class="table table-sm table-striped  bg-light" style="font-family: segoeui;opacity: 0.8;">'+
                '                      <thead class="text-white" style="background-color: #487d95!important;">'+
                '                        <tr>'+
                '                          <th colspan="2" class="text-center">REKAP IURAN DAN DPMP</th>'+
                '                        </tr>'+
                '                      </thead>'+
                '                      <tbody class="text-dark">';

                //
                var data_jenis_iuran = [{"jenis_iuran" : "Iuran Bulanan"},{"jenis_iuran" : "DPMP"}];
                var data_kelas=[{"kelas": "X", "total": 0},{"kelas": "XI", "total": 0},{"kelas": "XII", "total": 0}];

                var total_rekap=0;
                $.each(data_jenis_iuran, function(key,val_1){
                    $.each(data_kelas, function(key, val_2){
                        var dump=0;
                        myvar +='<tr contenteditable="false" class="data-row-rekap">';
                          myvar +='<td>'+val_1.jenis_iuran+" Kelas "+val_2.kelas+'</td>';
                            
                            if(val_1.jenis_iuran=="Iuran Bulanan"){
                               if(val_2.kelas=="X") myvar +='<td class="">'+$.number(sum_besar_iuran_x)+'</td>';
                               if(val_2.kelas=="XI") myvar +='<td class="">'+$.number(sum_besar_iuran_xi)+'</td>';
                               if(val_2.kelas=="XII") myvar +='<td class="">'+$.number(sum_besar_iuran_xii)+'</td>';
                            }
                            if(val_1.jenis_iuran=="DPMP"){
                               if(val_2.kelas=="X") myvar +='<td class="">'+$.number(sum_besar_dpmp_x)+'</td>';
                               if(val_2.kelas=="XI") myvar +='<td class="">'+$.number(sum_besar_dpmp_xi)+'</td>';
                               if(val_2.kelas=="XII") myvar +='<td class="">'+$.number(sum_besar_dpmp_xii)+'</td>';
                            }
                        myvar +='</tr>';
                    });
                });

                myvar +=               '</tbody>'+
                '                      <tfoot class="text-white" style="background-color: #487d95!important;">'+
                '                        <tr>'+
                '                          <th>Jumlah</th>'+
                '                          <th id="total-rekap-'+new_key+'">'+$.number(sum_besar_iuran+sum_besar_dpmp)+'</th>'+
                '                        </tr>'+
                '                      </tfoot>'+
                '                    </table>'+
                '                  </div>';

                myvar += '<div class="row">'+
                '                <div class="col-md-auto">'+
                '                  <button onclick="exportExcel()" id="btn-export" type="button" class="btn btn-block btn-secondary pill pl-4 pr-4 float-right"><i class="fa fa-arrow-down"></i> DOWNLOAD(.xlsx)</button>'+
                '                </div>'+
                '        </div>'+
                '</div>'+
                //
                '              <div class="col-md-3">'+
                //tabungan

                '                  <div class="">'+
                '                    <table id="tbl-data-rekap-tabungan-'+new_key+'" class="table table-sm table-striped bg-light" style="font-family: segoeui;opacity: 0.8;">'+
                '                      <thead class="text-white" style="background-color: #487d95!important;">'+
                '                        <tr>'+
                '                          <th colspan="2" class="text-center">REKAP TABUNGAN BULANAN</th>'+
                '                        </tr>'+
                '                      </thead>'+
                '                      <tbody class="text-dark">';
                //
                var data_jenis_iuran = [{"jenis_iuran" : "TABUNGAN"}];
                var data_kelas=[{"kelas": "X", "total": 0}];

                var total_rekap=0;
                $.each(data_jenis_iuran, function(key,val_1){
                    $.each(data_kelas, function(key, val_2){
                        var dump=0;
                        myvar +='<tr contenteditable="false" class="data-row-rekap-tabungan">';
                          myvar +='<td>Kelas '+val_2.kelas+'</td>';
                            if(val_1.jenis_iuran=="TABUNGAN"){
                               if(val_2.kelas=="X"){
                                myvar +='<td class="">'+$.number(sum_besar_tabungan_x)+'</td>';
                               }
                            }
                            // total_rekap+=sum_besar_tabungan_x;
                        myvar +='</tr>';
                    });
                });

                myvar +=               '</tbody>'+
                '                    </table>'+
                '                  </div>'+

                //OSIS
                '                  <div class="">'+
                '                    <table id="tbl-data-rekap-du'+new_key+'" class="table table-sm table-striped bg-light" style="font-family: segoeui;opacity: 0.8;">'+
                '                      <thead class="text-white" style="background-color: #487d95!important;">'+
                '                        <tr>'+
                '                          <th colspan="2" class="text-center">OSIS</th>'+
                '                        </tr>'+
                '                      </thead>'+
                '                      <tbody class="text-dark">';

                //

                var total_rekap=0;
                $.each(arrayOsis, function(key_osis, value_osis){
                    myvar +='<tr contenteditable="false" class="data-row-rekap">';
                        myvar +='<td>'+"OSIS "+value_osis.tahun_ajaran+'</td>';
                        myvar +='<td class="">'+$.number(value_osis.total_du)+'</td>';
                        total_rekap+=value_osis.total_du;
                    myvar +='</tr>';
                });

                myvar +=               '</tbody>'+
                '                      <tfoot class="text-white" style="background-color: #487d95!important;">'+
                '                        <tr>'+
                '                          <th>Jumlah</th>'+
                '                          <th id="total-rekap-du-'+new_key+'">'+$.number(sum_besar_du)+'</th>'+
                '                        </tr>'+
                '                      </tfoot>'+
                '                    </table>'+
                '                  </div>';



                myvar += '</div>'+
                //batas col-md      
                '           </div>'+
                '           </div>';
                    

                //
                new_key++;
            //}
            
        });
        
        wraper_content.html(myvar);
        wraper_sheet.html(row);
        if(myvar=="" || wraper_sheet==""){
            Swal.fire({
              type: 'warning',
              title: 'Hasil Pencarian',
              html: 'Mohon maaf, Tidak ada data transaksi, yang ditemukan.',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Tutup!'
            })
        } 
    }

    function renderRowTable(response){

    }

    function onGetListSuccess(response){
        console.log(response);
        $('#loading-row').remove();
        $('#loading-row-2').remove();
        $('#no-data-row').remove();
        
        var tbody = $("#tbl-data").find('tbody');
        
        var row = "";
        var num = $('.data-row').length+1;
        var temp_data=[];
        var jumlah_iuran=0;
        var jumlah_dpmp=0;
        var jumlah_du=0;

        var jumlah_total=0;
        
        $.each(response.data.laporan,function(key,value){
            console.log(value.du=="0.00");
            // if(value.total_bulan != ""){
            if(value.du != "0.00" || value.dpmp != "0.00" || value.spp != "0.00"){    
                // row += render_row(value, num);
               
                row += '<tr contenteditable="false" class="data-row" id="row-'+value.id_siswa+'">';
                row += '<td>'+(num)+'</td>';
                row += '<td class="">'+value.ref_siswa+'</td>';
                row += '<td class="">'; 
                    row += '<b style="font-family: segoeuib;">Nama</b>: '+value.nama_lengkap_siswa+'<br/>';
                    row += '<b style="font-family: segoeuib;">Kelas</b>: '+value.kelas;
                row += '</td>';
                var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                var dump=[];
                $.each(value.total_bulan,function(key){
                    var str = this.bulan_iuran;
                    str=str.split(" ");
                    dump.push(monthNameList.indexOf(str[0])+1);
                });
                row += '<td class="">'+dump+'</td>';
                row += '<td>'+$.number(value.spp)+'</td>';
                row += '<td>'+$.number(value.dpmp)+'</td>';
                row += '<td>'+$.number(value.du)+'</td>';
                row += '<td class="">'+$.number(parseInt(value.spp)+parseInt(value.dpmp)+parseInt(value.du))+'</td>';
                row += '<td class="">'+value.tahun_ajaran+'</td>';
                row += '<td class="">';
                    row +='<p><strong>bulanan:</strong> '+value.tanggal_bulanan+'</p>';
                    row +='<p><strong>dpmp:</strong> '+value.tanggal_dpmp+'</p>';
                    row +='<p><strong>du:</strong> '+value.tanggal_du+'</p>';
                row += '</td>';
                row += '</tr>';
                //
                temp_data.push(this);
                num++;
                jumlah_iuran+=parseInt(value.spp);
                jumlah_dpmp+=parseInt(value.dpmp);
                jumlah_du+=parseInt(value.du);
                jumlah_total+=parseInt(value.spp)+parseInt(value.dpmp)+parseInt(value.du);

            }
        });
        row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="10"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
        
        $('#jumlah-iuran').text($.number(jumlah_iuran));
        $('#jumlah-dpmp').text($.number(jumlah_dpmp));
        $('#jumlah-du').text($.number(jumlah_du));
        $('#jumlah-total').text($.number(jumlah_total));
        //
        var row="";
        var body=$('#tbl-data-rekap').find('tbody');
        var data_jenis_iuran = [{"name" : "IURAN_PER_BULAN"},{"name" : "DPMP"},{"name" : "DU"}];
        var data=[{"kelas": "X", "total": 0},{"kelas": "XI", "total": 0},{"kelas": "XII", "total": 0}];

        var total_rekap=0;
        $.each(data_jenis_iuran, function(key,val_1){
            $.each(data, function(key, val_2){
                var dump=0;
                row +='<tr contenteditable="false" class="data-row-rekap">';
                  row +='<td>'+(val_1.name=='IURAN_PER_BULAN'?"BULANAN":val_1.name)+" Kelas "+val_2.kelas+'</td>';
                    var sum_besar_iuran=0;
                    $.each(response.data.iuran, function(key, val){
                        if(val_1.name==val.jenis_iuran && val_2.kelas==val.tingkat_kelas){
                          sum_besar_iuran+=parseInt(val.besar_iuran);
                        }
                    });
                    row +='<td class="">'+$.number(sum_besar_iuran)+'</td>';
                    total_rekap+=sum_besar_iuran;
                row +='</tr>';
            });
        });

        body.html(row);
        $('#total-rekap').text($.number(total_rekap));
    }

    // function render_row(value, num){
    //  console.log(value);
    //     var row = "";
    //     row += '<tr contenteditable="false" class="data-row" id="row-'+value.id_siswa+'">';
    //     row += '<td>'+(num)+'</td>';
    //     row += '<td class="">'+value.ref_siswa+'</td>';
    //     // row += '<td class="">'+value.nis_siswa+'</td>';
    //     row += '<td class="">'; 
    //         row += '<b style="font-family: segoeuib;">Nama</b>: '+value.nama_lengkap_siswa+'<br/>';
    //         row += '<b style="font-family: segoeuib;">Kelas</b>: '+value.kelas;
    //     row += '</td>';
    //     // row += '<td class="">'+value.kelas+'</td>';
        
    //     var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    //     var dump=[];
    //     $.each(value.total_bulan,function(key){
    //         var str = this.bulan_iuran;
    //         str=str.split(" ");
    //         dump.push(monthNameList.indexOf(str[0])+1);
    //     });
    //     row += '<td class="">'+dump+'</td>';

    //     // row += '<td colspan="3">';
    //         row += '<td>'+$.number(value.spp)+'</td>';
    //         row += '<td>'+$.number(value.dpmp)+'</td>';
    //         row += '<td>'+$.number(value.du)+'</td>';
    //     // row += '</td>';
    //     row += '<td class="">'+$.number(parseInt(value.spp)+parseInt(value.dpmp)+parseInt(value.du))+'</td>';
    //     row += '<td class="">'+value.tahun_ajaran+'</td>';
    //     row += '<td class="">';
    //         row +='<p><strong>bulanan:</strong> '+value.tanggal_bulanan+'</p>';
    //         row +='<p><strong>dpmp:</strong> '+value.tanggal_dpmp+'</p>';
    //         row +='<p><strong>du:</strong> '+value.tanggal_du+'</p>';
    //     row += '</td>';
    //     row += '</tr>';
    //     return row;
    // }

    function onGetListError(response) {
        alert("ERROR");
        $('#ui-view').html($('#ui-view-msg-error').html());
    }

    function doAction(id, action, confirm = 0){
        selected_action = action;
        $('#modal-form-msg').hide();
        $('#msg-confirm-alert').hide();
        if(confirm == 0) ajaxGET(path + '/get_by_id/'+id+'?action='+selected_action,'onPrepareActionSuccess','onError');
        else ajaxPOST(path + '/do_action/'+id+'/'+selected_action,{},'onActionSuccess','onError');
    }

    function onPrepareActionSuccess(response) {
        console.log(response);
        var value = response.data;
        selected_id = value.id_siswa;
        if(selected_action == 'edit'){
            fillFormValue(value);
            $('#modal-form').modal('show');
        }else{

            Swal.fire({
              title: 'Are you sure?',
              text: response.message,
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                doAction(selected_id,'delete', 1);
                Swal.fire(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                display(1);
              }
            })
        }
    }
