	var path = ctx + 'SiswaController';
    var path_jenis_iuran = ctx + 'JenisIuranController'; 
    var path_kelas = ctx + 'KelasController'; 
    var params='';
    var selected_action='';
    var selected_id='';
    var list_iuran = [];
    // var jumlah_siswa=0;
    var kelas=0;
    var selected_all=true;
    var selected_key='';
    var list_obj=[];
    var list_kelas=[];

    function init() {
        
        getSelectKelas();

        //getTahunAjaranAll();
        //getSelectTahun();
        //display(1);
        

        $('#form-data').submit(function(e){
          // e.preventDefault();
          // console.log(path);
          // save();
          console.log(e);
        });

        $('#search-form').submit(function(e){
            display();
            e.preventDefault();
        });

        $('#form-siswa').submit(function(e){
            console.log(this);
            var jumlah_siswa=$('[name=jumlah_siswa]').val();
            var kelas_siswa=$('[name=kelas]').val();
            if(kelas_siswa!='') kelas_siswa='&kelas='+kelas_siswa;
            if(jumlah_siswa=='') {
                alert("Jumlah siswa wajib diisi.");
                return false;
            }
            window.location.href='form_siswa_view?jumlah_siswa='+jumlah_siswa+kelas_siswa;
            e.preventDefault();
        });

        $(document).on('click', '#tbl-data-kelas tr', function() {
           $("#tbl-data-kelas tr").removeClass("bg-info");
           $(this).toggleClass("bg-info");
        });

        $('#bg-head').hide();
    };

    function renderForm(){
            // list_obj.push(response.data);
            $('#loading-row').remove();
            $('#no-data-row').remove();
            
            var thead = $("#tbl-data").find('#sub-du');
            var thead1 = $("#tbl-data").find('#sub-du-1');
            var tbody = $("#tbl-data").find('tbody');
            
            var row = "";
            for(var i=0; i < jumlah_siswa; i++){
                row += render_body(i);
            }
            row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="9"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
            
            //render header
            row="";
            
            //var master_iuran=["BULANAN","DPMP","OSIS"];
            // if(list_kelas[0].tingkat_kelas=="X") master_iuran.push("TABUNGAN");
            var colspan_head=0;
            $.each(list_iuran, function(key){
                if(list_kelas[0].tingkat_kelas==this.tingkat_jenis_iuran || this.tingkat_jenis_iuran=="ALL"){
                    row += '<td>'+(this.nama_jenis_iuran=="DU"?"OSIS":this.nama_jenis_iuran)+'</td>';
                }
            });

            $.each(list_iuran, function(key){
                if(list_kelas[0].tingkat_kelas==this.tingkat_jenis_iuran || this.tingkat_jenis_iuran=="ALL"){
                    row += '<td>'+(this.nama_jenis_iuran=="DU"?"OSIS":this.nama_jenis_iuran)+'</td>';
                    colspan_head++;
                }
            });
            thead.html(row)

            row="";
            row += '<td width="2%" colspan="'+colspan_head+'" class="text-center">IURAN</td>';

            row += '<th rowspan="2" class="text-successs align-middle" width="2%">TAHUN ANGKATAN</th>';

            row += '<td width="2%" colspan="'+colspan_head+'" class="text-center">RINCIAN PEMBAYARAN(DU)</td>';

            row += '<td rowspan="2" class="align-middle" width="1%">'+"TANGGAL DU"+'</td>';
            row += '<td rowspan="2" class="align-middle" width="3%">'+"KETERANGAN MASUK"+'</td>';
            thead1.append(row);
    }

    function getSelectMasterIuran(tingkat=""){
        $.getJSON(path_jenis_iuran + '/list_table?page='+1, function(response){
            console.log(response);
            if(response.code==200){
                $.each(response.data, function(key_2, value) {
                    list_iuran.push({"id_jenis_iuran" : value.id_jenis_iuran, "tingkat_jenis_iuran" : value.tingkat_jenis_iuran, "nama_jenis_iuran" : value.nama_jenis_iuran, "besar_jenis_iuran" : value.besar_jenis_iuran});
                });
                renderForm();
            }
        });
    }
    function getSelectKelas(){
        $('[name="kelas[]"], [name=kelas]').empty();
        $('[name="kelas[]"], [name=kelas]').append(new Option('Pilih Kelas', ''));
        // ajaxGET(path_kelas + '/list_table?page='+1+'&'+"",'onGetSelectKelas','onGetListError');
        ajaxGET(path_kelas + '/list_table?page='+1,'onGetSelectKelas','onGetListError');
    }
    function onGetSelectKelas(response){
        $.each(response.data, function(key_2, value) {
            $('[name=kelas]').append(new Option(value.tingkat_kelas+"-"+value.jurusan_kelas+" "+value.sub_tingkat_kelas, value.id_kelas));
            if(this.tingkat_kelas==kelas_siswa)
            list_kelas.push({"id_kelas" : value.id_kelas, "tingkat_kelas" : value.tingkat_kelas, "kelas" : value.tingkat_kelas+"-"+value.jurusan_kelas+" "+value.sub_tingkat_kelas});
        });
        getSelectMasterIuran();
        // $('[name=kelas]').selectric('refresh');
    }

    function render_body(key){
        var row = "";
        row += '<tr data-id='+key+' contenteditable="false" class="data-row bg-light" id="row-'+key+'">';
        
        row += '<td>'+(key+1)+'</td>';
        row += '<td class="pr-3">'+'<input name="ref_siswa[]" type="text" class="w-100 rounded-0">'+'</td>';
        row += '<td class="pr-3">'+'<input name="nama_lengkap_siswa[]" type="text" class="w-100 rounded-0">'+'</td>';
        row += '<td class="pr-3">'+'<select name="jenis_kelamin_siswa[]" class="w-100 rounded-0"><option value="" selected>Pilih</option><option>LAKI_LAKI</option><option>PEREMPUAN</option></select>'+'</td>';
        
        row += '<td class="pr-3">';
        row += '<select name="tingkat_siswa[]" class="w-100 rounded-0"><option value="" selected>Pilih</option>';
        $.each(list_kelas, function(key_kelas, value_kelas){
            row += '<option value='+this.id_kelas+'>'+this.kelas+'</option>';
        });
        row += '</select>';
        row += '</td>';
       
        //
        $.each(list_iuran, function(key){
            if(list_kelas[0].tingkat_kelas==this.tingkat_jenis_iuran || this.tingkat_jenis_iuran=="ALL"){
                row += '<td class="pr-3">'+'<input min="0" name="iuran_'+this.nama_jenis_iuran+'[]" type="number" class="rounded-0" value="'+this.besar_jenis_iuran+'">'+'</td>';
            }
        });
        // for (var i = 0; i < 4; i++) {
        //     row += '<td class="pr-3">'+'<input type="text" class="rounded-0">'+'</td>';
        // }
        var d = new Date();
        var n = d.getFullYear();
        var tahun = n - 2;
        
        row += '<td class="pr-3"><select name="tahun_angkatan_siswa[]" class="w-100 rounded-0"><option>Pilih</option>';
        for (var i = n; i>= tahun; i--) {
            row += '<option value='+i+'>'+i+'</option>';
        }
        row +='</select></td>';
        

        $.each(list_iuran, function(key){
            if(list_kelas[0].tingkat_kelas==this.tingkat_jenis_iuran || this.tingkat_jenis_iuran=="ALL"){
                row += '<td class="pr-3">'+'<input name="rincian_'+this.nama_jenis_iuran+'[]" type="number" class="rounded-0" max="'+this.besar_jenis_iuran+'">'+'</td>';
            }
        });
        // for (var i = 0; i < 4; i++) {
        //     row += '<td class="pr-3">'+'<input type="text" class="w-100 rounded-0">'+'</td>';
        // }

        row += '<td class="">'+'<input  name="tanggal_bayar_siswa[]" value="'+moment().format('YYYY-MM-DD')+'" type="date" class="rounded-0">'+'</td>';
        row += '<td class="pr-3">'+'<input name="keterangan_masuk_siswa[]" type="text" class="w-100 rounded-0">'+'</td>';

        // row += '<td class="">'+check_tahun_ajaran_by(tahun_ajaran_baru)+'</td>';
        // row += '<td class="">'+value.tahun_angkatan_siswa+'</td>';
        // row += '<td class="">'+tahun_ajaran_baru+'</td>';
        // row += '<td class="">'+value.tingkat_kelas+" "+value.jurusan_kelas+" "+value.sub_tingkat_kelas+'</td>';
        
        // row += '<td class="text-success">'+value.tingkat_kelas_full_baru+'</td>';
        
        // var besar_iuran=0;
        // var besar_iuran_spp=0;
        // var besar_iuran_osis=0;
        // $.each(value.list_iuran, function(key_jenis_iuran){
        //     if(this.nama_jenis_iuran=="IURAN_PER_BULAN") 
        //     row += '<td class="">'+"<input data-key="+num+" onkeyup='setMy(this)' id='master-spp-"+num+"' style='border-radius:0;' type='number' value="+this.besar_jenis_iuran+">"+'</td>';
        //     if(this.nama_jenis_iuran=="DU") 
        //     row += '<td class="">'+"<input data-key="+num+" onkeyup='setMy(this)' id='master-osis-"+num+"' style='border-radius:0;' type='number' value="+this.besar_jenis_iuran+">"+'</td>';
        //     besar_iuran+=parseInt(this.besar_jenis_iuran);
        // })
        
        // row += '<td class="">'+"<input value='0' id='rincian-spp-"+num+"' data-key="+num+" onkeyup='setMy(this)' style='border-radius:0;' type='number'>"+'</td>';
        // row += '<td class="">'+"<input value='0' id='rincian-osis-"+num+"' data-key="+num+" onkeyup='setMy(this)' style='border-radius:0;' type='number'>"+'</td>';
        // row += '<td class="text-success" id="jumlah-'+num+'">'+0+'</td>';
        // row += '<td class="text-danger" id="sisa-'+num+'" data-sisa='+besar_iuran+'>'+$.number(besar_iuran)+'</td>';
        
        // row += '<td class="">'+"<input  id='tanggal-iuran-"+num+"' data-key="+num+" style='border-radius:0;' type='date'>"+'</td>';
        
        row += '</tr>';
        return row;
    }

    function check_tahun_ajaran(){
        var date = new Date();
            // date = new Date("July 10, 2019");
        var month = date.getMonth();

        if(month>=6){
            date=date.getFullYear()+"/"+(date.getFullYear()+1);
        }else{
            date=(date.getFullYear()-1)+"/"+date.getFullYear();
        }
        return date;
    }
    function check_tahun_ajaran_by(tanggal){
        var date = new Date();
        tanggal = tanggal.split("-");//0 tahun, 1 month, 2 date
        date.setDate(tanggal[2]);
        date.setMonth(tanggal[1]-1);
        // alert(tanggal[0]-1);
        date.setFullYear(tanggal[0]);
        
        if(date.getMonth()>=6){
            date=date.getFullYear()+"/"+(date.getFullYear()+1);
        }else{
            date=(date.getFullYear()-1)+"/"+date.getFullYear();
        }
        return date;
    }
    function getSelectTahun() {
        $('[name="tahun_ajaran_siswa_kelas[]"]').empty();
        $('[name="tahun_ajaran_siswa_kelas[]"]').append(new Option('Pilih', ''));
        var d = new Date();
        var n = d.getFullYear();
        var tahun = n - 5;
        for (var i = n; i>= tahun; i--) {
            $('[name="tahun_ajaran_siswa_kelas[]"]').append(new Option(i+"/"+(i+1)));
        }
        // $("#tahun_ajaran").val(check_tahun_ajaran()).trigger('change');
    }

    function onGetListError(response) {
        alert("ERROR");
        $('#ui-view').html($('#ui-view-msg-error').html());
    }

    function save(){
        // alert("ITS ");
        obj_arr = []; 
        var obj ={}
        var data = $('#tbl-data').find('tbody tr');
        var something_wrong=false;
        var data_siswa=[];
        $.each(data,function(key){
            console.log($(this).find('td'));
            console.log($(this));
            console.log($(this)[0].cells);
            //
            // if($(this)[0].cells[4].textContent.split("-")[0]=="X"){
            //     obj={ 
            //         "id_siswa": 0,
            //         "ref_siswa": $(this)[0].cells[1].children[0].value,
            //         "nama_lengkap_siswa": $(this)[0].cells[2].children[0].value,
            //         "jenis_kelamin_siswa": $(this)[0].cells[3].children[0].value,
            //         "tahun_angkatan_siswa": tahun_angkatan_siswa.split("/")[0],
            //         "keterangan_masuk_siswa": keterangan_masuk_siswa,
            //         "status_aktif_siswa": 1,
            //         "siswa_detil" : master_jenis_iuran,
            //         "siswa_kelas" : master_siswa_kelas,
            //         "siswa_iuran" : master_iuran
            //     }
            //     data_siswa.push(
            //         {
            //             'siswa' => array(
            //                 'id_siswa' =>  $utils->longNumberId(),
            //                 "ref_siswa": $(this)[0].cells[1].children[0].value,
            //                 "nama_lengkap_siswa": $(this)[0].cells[2].children[0].value,
            //                 "jenis_kelamin_siswa": $(this)[0].cells[3].children[0].value,
            //                 'tahun_angkatan_siswa' => $row['J'],
            //                 'keterangan_masuk_siswa' => $row['Q'],
            //                 'status_aktif_siswa' => 1,
            //                 'user_added_siswa' => $this->session->userdata('fullname'),
            //                 'date_added_siswa' => date("Ymd")
            //             ),
            //             'kelas_siswa' => $row['D'],
            //             'iuran' => array(
            //                 'bulanan' => $row['E'],
            //                 'dpmp' => $row['F'],
            //                 'du' => $row['G'],
            //                 'osis' => $row['G'],
            //                 'tabungan' => $row['H'],
            //             ), 
            //             'iuran_bayaran' => array(
            //                 'osis' => $row['L'],
            //                 'bulanan' => $row['N'],
            //                 'dpmp' => $row['O'],
            //                 'tabungan' => $row['M'],
            //             ),
            //             'tanggal_iuran_bayaran' => $row['P'],
            //             'tanggal_registrasi' => $row['K'],
            //             'tahun_ajaran' => $row['I']      
            //         }
            //     );
            //     array_push($data_siswa, array(
            //         'siswa' => array(
            //             'id_siswa' =>  $utils->longNumberId(),
            //             'ref_siswa' => $row['A'],
            //             'nama_lengkap_siswa' => $row['B'],
            //             'jenis_kelamin_siswa' => $row['C'],
            //             'tahun_angkatan_siswa' => $row['J'],
            //             'keterangan_masuk_siswa' => $row['Q'],
            //             'status_aktif_siswa' => 1,
            //             'user_added_siswa' => $this->session->userdata('fullname'),
            //             'date_added_siswa' => date("Ymd")
            //         ),
            //         'kelas_siswa' => $row['D'],
            //         'iuran' => array(
            //             'bulanan' => $row['E'],
            //             'dpmp' => $row['F'],
            //             'du' => $row['G'],
            //             'osis' => $row['G'],
            //             'tabungan' => $row['H'],
            //         ), 
            //         'iuran_bayaran' => array(
            //             'osis' => $row['L'],
            //             'bulanan' => $row['N'],
            //             'dpmp' => $row['O'],
            //             'tabungan' => $row['M'],
            //         ),
            //         'tanggal_iuran_bayaran' => $row['P'],
            //         'tanggal_registrasi' => $row['K'],
            //         'tahun_ajaran' => $row['I']
            //     ));// $key++;
            // }else{
            //     array_push($data_siswa, array(
            //         'siswa' => array(
            //             'id_siswa' =>  $utils->longNumberId(),
            //             'ref_siswa' => $row['A'],
            //             'nama_lengkap_siswa' => $row['B'],
            //             'jenis_kelamin_siswa' => $row['C'],
            //             'tahun_angkatan_siswa' => $row['I'],
            //             'keterangan_masuk_siswa' => $row['O'],
            //             'status_aktif_siswa' => 1,
            //             'user_added_siswa' => $this->session->userdata('fullname'),
            //             'date_added_siswa' => date("Ymd")
            //         ),
            //         'kelas_siswa' => $row['D'],
            //         'iuran' => array(
            //             'bulanan' => $row['E'],
            //             'dpmp' => $row['F'],
            //             'du' => $row['G'],
            //             'osis' => $row['G'],
            //         ),
            //         'iuran_bayaran' => array(
            //             'osis' => $row['K'],
            //             'bulanan' => $row['L'],
            //             'dpmp' => $row['M'],
            //         ),
            //         'tanggal_iuran_bayaran' => $row['N'],
            //         'tanggal_registrasi' => $row['J'],
            //         'tahun_ajaran' => $row['H']
            //     ));
            // }
            //

            if($(this)[0].cells[1].children[0].value=="" || $(this)[0].cells[2].children[0].value=="" || $(this)[0].cells[3].children[0].value==""){
                something_wrong=true;
                $(this)[0].className=($(this)[0].className).replace("bg-light","bg-danger");
            }else{
                something_wrong=false;
                $(this)[0].className=($(this)[0].className).replace("bg-danger","bg-light");
            }
            var master_jenis_iuran = [
                    {
                        "tingkat_siswa_detil": "ALL",//$(this)[0].cells[4].textContent.split("-")[0],
                        "besar_iuran_siswa_detil": $(this)[0].cells[5].children[0].value,
                        "jenis_iuran_siswa_detil": "DPMP",
                    },
                    {
                        "tingkat_siswa_detil": $(this)[0].cells[4].children[0].options[$(this)[0].cells[4].children[0].options.selectedIndex].text.split("-")[0],
                        "besar_iuran_siswa_detil": $(this)[0].cells[6].children[0].value,
                        "jenis_iuran_siswa_detil": "IURAN_PER_BULAN",
                    },
                    {
                        "tingkat_siswa_detil": $(this)[0].cells[4].children[0].options[$(this)[0].cells[4].children[0].options.selectedIndex].text.split("-")[0],
                        "besar_iuran_siswa_detil": $(this)[0].cells[7].children[0].value,
                        "jenis_iuran_siswa_detil": "DU",
                    },
            ];
            var keterangan_masuk_siswa = "";
            var tahun_angkatan_siswa = "";
            var tahun_ajaran_siswa_kelas = "";
            var tanggal_registrasi_siswa = "";
            var bulan_iuran = "";

            var master_siswa_kelas = [];
            var master_iuran = [];

            if(list_kelas[0].tingkat_kelas=="X"){
                master_jenis_iuran.push(
                    {
                        "tingkat_siswa_detil": $(this)[0].cells[4].children[0].options[$(this)[0].cells[4].children[0].options.selectedIndex].text.split("-")[0],
                        "besar_iuran_siswa_detil": $(this)[0].cells[8].children[0].value,
                        "jenis_iuran_siswa_detil": "TABUNGAN",
                    }
                );
            }

            var elem=this;
            $.each(master_jenis_iuran, function(key){
                    if(master_jenis_iuran.length==3){
                        tahun_angkatan_siswa=$(elem)[0].cells[8].children[0].value;
                        keterangan_masuk_siswa=$(elem)[0].cells[13].children[0].value;
                        tahun_ajaran_siswa_kelas=check_tahun_ajaran_by($(elem)[0].cells[12].children[0].value);
                        tanggal_registrasi_siswa=$(elem)[0].cells[12].children[0].value;
                    }else if(master_jenis_iuran.length==4){
                        tahun_angkatan_siswa=$(elem)[0].cells[9].children[0].value;
                        keterangan_masuk_siswa=$(elem)[0].cells[15].children[0].value;
                        tahun_ajaran_siswa_kelas=check_tahun_ajaran_by($(elem)[0].cells[14].children[0].value);
                        tanggal_registrasi_siswa=$(elem)[0].cells[14].children[0].value;
                    }

                    date_iuran = new Date();
                    date_iuran.setMonth(tanggal_registrasi_siswa.split("-")[1]-1);
                    bulan_iuran = date_iuran.getMonth()+" "+date_iuran.getFullYear();

                if($(elem)[0].cells[6+master_jenis_iuran.length+key].children[0].value!=""){
                    master_iuran.push(
                        {
                            "bulan_iuran": tanggal_registrasi_siswa,//"Jul "+tahun_angkatan_siswa.split("/")[0],
                            "besar_iuran" : $(elem)[0].cells[6+master_jenis_iuran.length+key].children[0].value,
                            "jenis_iuran" : this.jenis_iuran_siswa_detil,
                            "tanggal_iuran" : tanggal_registrasi_siswa,
                            "status_iuran" : ($(elem)[0].cells[5+key].children[0].value==$(elem)[0].cells[6+master_jenis_iuran.length+key].children[0].value?"LUNAS":"BELUM_LUNAS"),//"LUNAS",
                            "keterangan_iuran": "REGISTRASI_ULANG",
                        }
                    );
                }
            });

            master_siswa_kelas.push(
                {
                    "kelas_siswa_kelas": $(this)[0].cells[4].children[0].value,
                    "tahun_ajaran_siswa_kelas": tahun_ajaran_siswa_kelas,
                    "status_siswa_kelas": 1,
                    "tanggal_registrasi_siswa_kelas": tanggal_registrasi_siswa,
                }
            );
            
            obj={ 
                "id_siswa": 0,
                "ref_siswa": $(this)[0].cells[1].children[0].value,
                "nama_lengkap_siswa": $(this)[0].cells[2].children[0].value,
                "jenis_kelamin_siswa": $(this)[0].cells[3].children[0].value,
                "tahun_angkatan_siswa": tahun_angkatan_siswa,
                "keterangan_masuk_siswa": keterangan_masuk_siswa,
                "status_aktif_siswa": 1,
                "siswa_detil" : master_jenis_iuran,
                "siswa_kelas" : master_siswa_kelas,
                "siswa_iuran" : master_iuran
            }

            obj_arr.push(obj);

        });
        console.log(obj_arr);
        var obj = new FormData();
        obj.append('list_obj', JSON.stringify(obj_arr));

        if(something_wrong==false) ajaxPOST(path + '/save_naik_kelas',obj,'onActionSuccess','onSaveFailed');
    }

    function onSaveSuccess(response){
        console.log(response);
        $('#msg-info-body').html('Data telah berhasil di update.');
        $('#msg-info').modal('show');
        // setTimeout(function(){ window.location.href="pengguna?vmenu=daftar"; }, 1000);
    }
    //
    function onSaveFailed(response){
        console.log(response);
        list_duplicate=response.responseJSON.data;
        $.each($('#tbl-data').find('tbody tr'), function(key,value_form){
            // $(this).find('td').eq(1).text()
            console.log($(this).find('td input').eq(1).val());
            $.each(list_duplicate, function(key,value_data){
                console.log($(value_form).find('td input').eq(1).val()+"=="+value_data.ref_siswa);
                if($(value_form).find('td input').eq(1).val()==value_data.ref_siswa) //active="bg-danger";
                console.log($(this));
                // $(value_form).addClass("bg-danger");
                $(value_form).removeClass("bg-light").addClass( "bg-warning" );;
            });
        });
    }
    //
    function onActionSuccess(response){
        console.log(response);
        Swal.fire({
          type: 'success',
          title: 'Message',
          text: response.message,
          //footer: '<a href>Why do I have this issue?</a>'
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Oke!'
        })
        .then((value) => {
            if(value.value){
                // location.reload();
                window.location.href='siswa_view';
            }
        });
    }
    function doAction(id, action, confirm = 0){
        selected_action = action;
        $('#modal-form-msg').hide();
        $('#msg-confirm-alert').hide();
        if(confirm == 0) ajaxGET(path + '/get_by_id/'+id+'?action='+selected_action,'onPrepareActionSuccess','onError');
        // else ajaxPOST(path + '/do_action/'+id+'/'+selected_action,{},'onActionSuccess','onError');
        else ajaxPOST(path + '/delete_all/'+id,{},'onActionSuccess','onError');
    }

    function onPrepareActionSuccess(response) {
        console.log(response);
        var value = response.data;
        selected_id = value.id_siswa;
        if(selected_action == 'edit'){
            fillFormValue(value);
            $('#modal-form').modal('show');
        }else{
            Swal.fire({
              title: 'Are you sure?',
              text: response.message,
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                doAction(selected_id,'delete', 1);
                Swal.fire(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                display(1);
              }
            })
        }
    }
    


    function getTahunAjaranAll(){
        var strdate=[];
        var angkatan= new Date("July 10, 2017");
        // var date = new Date("December 10, 2019");//+tahun_ajaran[0]
        var date = new Date();
        var selisih=(date.getFullYear()-angkatan.getFullYear());
        for(var start_ajaran=angkatan.getFullYear(); start_ajaran < angkatan.getFullYear()+selisih; start_ajaran++){
            if(start_ajaran+"/"+(start_ajaran+1)!=date){
                strdate.push(start_ajaran+"/"+(start_ajaran+1).toString());
                $('#tahun_ajaran').append(new Option(start_ajaran+"/"+(start_ajaran+1).toString(), start_ajaran+"/"+(start_ajaran+1).toString()));
            }
        }
        return strdate;
    }

    function filter(params){
        display(1, params);
    }

    function setMy(e){
        var elem = $(e);
        console.log(elem.data('key'));
        var rinci_spp=$('#rincian-spp-'+elem.data('key')).val();
        var rinci_osis=$('#rincian-osis-'+elem.data('key')).val();

        $('#jumlah-'+elem.data('key')).text($.number(parseInt(rinci_spp)+parseInt(rinci_osis)));
        
        //count sisa
        var master_spp = parseInt($('#master-spp-'+elem.data('key')).val());
        var master_osis = parseInt($('#master-osis-'+elem.data('key')).val());

        $('#sisa-'+elem.data('key')).text($.number(parseInt(master_spp+master_osis)-(parseInt(rinci_spp)+parseInt(rinci_osis))));

    }
