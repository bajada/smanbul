	var path = ctx + 'SiswaController';
    var path_jenis_iuran = ctx + 'JenisIuranController'; 
    var path_kelas = ctx + 'KelasController'; 
    var params='';
    var selected_action='';
    var selected_id='';
    var list_iuran = [];
    var jumlah_siswa=0;
    var apel=false;
    function init() {
        //tambahkan validasi, jika form halaman  bukan daftar maka jangan runing ini
        

        //renderIuran();
        getSelectKelas();
        // getSelectTahun();
        getTahunAjaranAll();
        // $('#search-form').submit(function(e){
        //     display();
        //     e.preventDefault();
        // });

        $('#search-form').submit(function(e){
            if($('[name=filter_keyword]').val() != ""){
                display();
            }else{
                var resp_msg={"title" : "Message", "body" : "Tanggal pencarian wajib diisi!", "icon"  : "warning"};
                showAlertMessage(resp_msg, 1200);
            }
            e.preventDefault();
        });
        
        // $('#btn-yes-msg-confirm').click(function(e){
        //     if(selected_action == 'delete' ){
        //         doAction(selected_id, selected_action, 1);
        //     }
            
        // });

        $('#form').submit(function(e){
          e.preventDefault();
          console.log(path);
          save();
        });

        $(document).on('click', '#tbl-data-kelas tr', function() {
           $("#tbl-data-kelas tr").removeClass("bg-info");
           $(this).toggleClass("bg-info");
        });
    };
    function getSelectKelas(to){
        if(to==true) {
            ajaxGET(path_kelas + '/list_table?page='+1+'&'+"",'onGetSelectToKelas','onGetListError');
        }else{
            $('[name=filter_kelas], [name=from_kelas]').empty();
            $('[name=filter_kelas], [name=from_kelas]').append(new Option('Pilih Kelas', ''));
            $('[name=from_kelas]').append(new Option('BELUM ADA KELAS', '0'));
            $('[name=from_kelas]').attr('onchange','renderSiswa();getSelectKelas(true);');
            ajaxGET(path_kelas + '/list_table?page='+1+'&'+"",'onGetSelectKelas','onGetListError');
        }
    }

    function onGetSelectKelas(response){
        console.log(response);
        var tbody=$('#tbl-data-kelas').find('tbody');
        var row='';
        $.each(response.data, function(key, value) {
            $('[name=filter_kelas], [name=from_kelas]').append(new Option(value.tingkat_kelas+" "+value.jurusan_kelas+" "+value.sub_tingkat_kelas, value.id_kelas));
            row+='<tr onclick="chooseDo('+value.id_kelas+');">';
                row+='<td>'+(value.tingkat_kelas+" "+value.jurusan_kelas+" "+value.sub_tingkat_kelas)+' <span class="badge badge-pill badge-secondary pull-right">10</span></td>';
            row+='</tr>';

        });
        tbody.html(row);

         $('[name="filter_kelas[]"]').selectric('refresh');
    }
    function onGetSelectToKelas(response){
        $('[name=to_kelas]').empty();
        $('[name=to_kelas]').append(new Option('Pilih Kelas', ''));
        // $('[name=to_kelas]').append(new Option('ALUMNI', '-1'));
        var select_kelas_now=$('[name=from_kelas] :selected').text();
        select_kelas_now=select_kelas_now.split(" ");
        $.each(response.data, function(key, value) {
            if(this.id_kelas!=$('[name=from_kelas]').val()){
                if($('[name=from_kelas]').val()==0){
                    $('[name=to_kelas]').append(new Option(value.tingkat_kelas+" "+value.jurusan_kelas+" "+value.sub_tingkat_kelas, value.id_kelas));
                }
                if(select_kelas_now[0] == "X"){
                    if(this.tingkat_kelas=="XI"){
                        $('[name=to_kelas]').append(new Option(value.tingkat_kelas+" "+value.jurusan_kelas+" "+value.sub_tingkat_kelas, value.id_kelas));
                    }
                }
                if(select_kelas_now[0] == "XI"){
                    if(this.tingkat_kelas=="XII"){
                        $('[name=to_kelas]').append(new Option(value.tingkat_kelas+" "+value.jurusan_kelas+" "+value.sub_tingkat_kelas, value.id_kelas));
                    }
                }
                if(select_kelas_now[0] == "XII"){
                    if(this.tingkat_kelas=="XII"){
                        $('[name=to_kelas]').append(new Option(value.tingkat_kelas+" "+value.jurusan_kelas+" "+value.sub_tingkat_kelas, value.id_kelas));
                    }
                }
                
            }
        });
    }

    function getSelectTahun() {
        var d = new Date();
        var n = d.getFullYear();
        var tahun = n - 5;
        for (var i = n; i>= tahun; i--) {
            $("[name=filter_tahun]").append(new Option(i+"/"+(i+1)));
        }
    }

    function getTahunAjaranAll(){
        var strdate=[];
        var angkatan= new Date("July 10, 2017");
        // var date = new Date("December 10, 2019");//+tahun_ajaran[0]
        var date = new Date();
        var selisih=(date.getFullYear()-angkatan.getFullYear());
        for(var start_ajaran=angkatan.getFullYear(); start_ajaran < angkatan.getFullYear()+selisih; start_ajaran++){
            if(start_ajaran+"/"+(start_ajaran+1)!=date){
                strdate.push(start_ajaran+"/"+(start_ajaran+1).toString());
                var newstr=start_ajaran+"/"+(start_ajaran+1).toString();
                $("[name=filter_tahun_ajaran]").append(new Option(newstr, newstr));
                $("[name=filter_tahun_ajaran]").val(checkTahunAjaran()).trigger('change');
            }
        }
        return strdate;
    }

    function renderIuran(){
        var tbody = $(".tbl-data-iuran").find('tbody');
        tbody.text('');
        
        tbody.append('<tr class="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        
        ajaxGET(path_jenis_iuran + '/list_table?page='+1+'&'+"",'onRenderIuran','onGetListError');
    }
    function onRenderIuran(resp){
        list_iuran.push(resp.data);

        $('.loading-row').remove();
        $('.no-data-row').remove();
        var tbody = $(".tbl-data-iuran").find('tbody');
        var row = "";
        var obj = {"nama": "nama_jenis_iuran", "besar": "besar_jenis_iuran", "cicilan": "cicilan_jenis_iuran"};
        row=render_row_def2(resp, obj);
        console.log(row);
        row == "" ? tbody.html('<tr id="" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
    
        //set value
          // alert(count);
          for (var i = 0; i < count; i++) {
            $('#table'+i).find('tbody [name="header_siswa[]"]').val(i);
            console.log(i);
          }
    }

    function render_row_def2(resp,obj){
        var row='';
        $.each(resp.data,function(key,value){
            row += '<tr class="data-row" id="row-'+key+'">';
                row += '<td>'+(key+1)+'</td>';
                // row += '<td class=""><input name="header_siswa[]" type="text" value="'+value.ref+'"></td>';
                row += '<td class="d-none"><input name="header_siswa[]" type="text"></td>';
                row += '<td><input class="border-radius-0 bg-light" name="jenis_iuran[]" type="text" value="'+value.nama_jenis_iuran+'" readonly="TRUE"></td>';
                row += '<td><input class="border-radius-0" name="besar_iuran[]" type="text" value="'+value.besar_jenis_iuran+'"></td>';
            row += '</tr>';
        });
        return row;
    }

    function display(page = 1){
        $('#more-button-row').remove();
        var tbody = $("#tbl-data").find('tbody');
        if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
            tbody.text('');
        }
        
        tbody.append('<tr id="loading-row"><td colspan="6"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        
        params = $( "#search-form" ).serialize();
        console.log(path + '/list_kelas_table?page='+page+'&'+params);
        ajaxGET(path + '/list_kelas_table?page='+page+'&'+params,'onGetListSuccess','onGetListError');
    }

    function onGetListSuccess(response){
        console.log(response);
        $('#loading-row').remove();
        $('#no-data-row').remove();
        
        var tbody = $("#tbl-data").find('tbody');
        
        var row = "";
        var num = $('.data-row').length+1;
        $.each(response.data,function(key, value){
            // $.each(this.header_id_siswa_kelas, function(key,value){
                row += '<tr contenteditable="false" class="data-row" id="row-'+value.header_id_siswa_kelas.id_siswa+'">';
                // row += '<td>'+(num)+'</td>';
                row += '<td>'+'<div class="form-check"><input class="form-check-input position-static" type="checkbox" value='+value.id_siswa+'></div>'+'</td>';
                row += '<td class="">'+value.header_id_siswa_kelas.ref_siswa+'</td>';
                // row += '<td class="">'+value.nis_siswa+'</td>';
                row += '<td class="">'+value.header_id_siswa_kelas.nama_lengkap_siswa+'</td>';
                row += '<td class="">'+value.kelas+'<br/>'+value.tahun_ajaran+'</td>';
                row += '<td class="">'+value.header_id_siswa_kelas.kelas+'<br/>'+value.header_id_siswa_kelas.tahun_ajaran_siswa+'</td>';
                row += '<td class="">'+value.header_id_siswa_kelas.jenis_kelamin_siswa+'</td>';
                // row += '<td class="">'+"-"+'</td>';
                row += '<td class="">';
                row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-info pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'edit\')"><i class="fas fa-pencil-alt"></i></a> ';
                row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'delete\')"><i class="fas fa-trash-alt"></i></a>';
                row += '</td>';
                row += '</tr>';
                num++;
            // })
        });
        row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="6"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
    }

    function render_row(value, num){
    //  console.log(value);
        var row = "";
        return row;
    }

    function onGetListError(response) {
        alert("ERROR");
        $('#ui-view').html($('#ui-view-msg-error').html());
    }

    function save(){
        var obj = new FormData(document.querySelector('#form'));
        if(selected_id != '') obj.append('id', selected_id);
        obj.append('kelas', $('#tingkat').val()+' '+$('#jurusan').val()+' '+$('#sub_tingkat').val());
        obj.append('list_iuran', JSON.stringify(list_iuran[0]));
        
        ajaxPOST(path + '/save',obj,'onActionSuccess','onSaveError');
    }

    function onSaveSuccess(response){
        console.log(response);
        $('#msg-info-body').html('Data telah berhasil di update.');
        $('#msg-info').modal('show');
        // setTimeout(function(){ window.location.href="pengguna?vmenu=daftar"; }, 1000);
    }
    function onActionSuccess(response){
        // display();
        // $('#modal-form').modal('hide');
        // $('#msg-confirm').modal('hide');
        // selected_action = '';
        console.log(response);
        swal("Message", response.message, "success", {
          buttons: {
            cancel: "Kembali ke daftar siswa",
            go: {
              text: "Tambah lagi data siswa",
              value: "go",
            }//,
            // back: {
            //   text: "Kembali ke daftar siswa",
            //   value: "back",
            // }
          },
        })
        .then((value) => {
          switch (value) {
         
            case "go":
              window.location.href=ctx+"page/form_siswa_view";
              break;
         
            // case "back":
            //   window.location.href=ctx+"page/siswa_view";
            //   break;
         
            default:
              window.location.href=ctx+"page/siswa_view";
          }
        });
    }
    function retes(){
        swal("Message", "response.message", "success", {
          buttons: {
            //cancel: "Run away!",
            go: {
              text: "Tambah lagi data siswa",
              value: "go",
            },
            back: {
              text: "Kembali ke daftar siswa",
              value: "back",
            }
          },
        })
        .then((value) => {
          switch (value) {
         
            case "go":
              window.location.href=ctx+"page/form_siswa_view";
              break;
         
            case "back":
              window.location.href=ctx+"page/siswa_view";
              break;
         
            default:
              swal("Something Wrong!","error");
          }
        });       
         // alert(response.message);
    }
    function doAction(id, action, confirm = 0){
        selected_action = action;
        $('#modal-form-msg').hide();
        $('#msg-confirm-alert').hide();
        if(confirm == 0) ajaxGET(path + '/get_by_id/'+id+'?action='+selected_action,'onPrepareActionSuccess','onError');
        else ajaxPOST(path + '/do_action/'+id+'/'+selected_action,{},'onActionSuccess','onError');
    }

    function onPrepareActionSuccess(response) {
        console.log(response);
        var value = response.data;
        selected_id = value.id_siswa;
        if(selected_action == 'edit'){
            fillFormValue(value);
            $('#modal-form').modal('show');
        }else{

            Swal.fire({
              title: 'Are you sure?',
              text: response.message,
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                doAction(selected_id,'delete', 1);
                Swal.fire(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                display(1);
              }
            })
        }
    }
    

    function fillFormValue(value){
        $('#form')[0].reset();
        
        $('[name=nis_siswa]').val(value.nis_siswa);
        $('[name=nama_siswa]').val(value.nama_siswa);
        $('[name=kelas_siswa]').val(value.kelas_siswa);
    }

	function add() {
     //    selected_id = ''; // wajib
     //    selected_action = 'add'; // wajib
	    // $('#form')[0].reset(); // reset form on modals
	    // $('.form-group').removeClass('has-error'); // clear error class
	    // $('.help-block').empty(); // clear error string
	    // $('#modal-form').modal('show'); // show bootstrap modal
	    // $('.modal-title').text('Tambah Siswa'); // Set Title to Bootstrap modal title
     //    $('#form-pass').removeClass("d-none");
        readForm();
	}

    function readForm(jenis=''){
        // if(selected_id != '') {
        //     jenis_iuran=jenis;
        //     ajaxGET(path + '/get_by_id/'+selected_id,'onGetReadDaftarTunggakan','onGetError');
        // }
        onReadForm(1);
    }
    function renderSiswa(){
        //status = non kelas=-1, kelas > 0
        var select_kelas_now=$('[name=from_kelas]').val();
        var page=1;
        var params="";

        if(select_kelas_now > 0){
            // console.log(path + '/list_kelas_table?page='+page+'&'+params);
            ajaxGET(path + '/list_siswa_kelas_table_new?page='+page+'&'+params,'onRenderSiswaKelasSuccess','onGetListError');
        }else{
            // console.log(path + '/list_table?page='+page+'&'+params);
            ajaxGET(path + '/list_siswa_kelas_table_new?page='+page+'&'+params,'onRenderNonSiswaSuccess','onGetListError');
        }
         // ajaxGET(path + '/list_kelas_table?page='+page+'&'+params,'onRenderSiswaSuccess','onGetListError');
    }

    function onRenderNonSiswaSuccess(resp){
        var row = "";
        var tbody=$('#tbl-data-pengaturan-kelas').find('tbody');
        var select_kelas_now=$('[name=from_kelas]').val();
        tbody.text('');
        console.log(resp);
        $.each(resp.data,function(key1, siswa){
            console.log(this);
            console.log(siswa);
            if(this.kelas_siswa_kelas==null){
                row+='<tr>';
                    row+='<td class="pl-3">';
                        row+='<div class="form-check"><input class="form-check-input position-static" type="checkbox" value='+siswa.id_siswa+'></div>';
                    row+='</td>';
                    row+='<td class="">';
                        row+='<i class="fas fa-user-circle fa-2x"></i>';
                    row+='</td>';
                    row+='<td class="pl-3"><span style="font-family:segoeuib">'+siswa.nama_lengkap_siswa+'</span><br>'+siswa.ref_siswa;
                    row+='</td>';
                    row+='<td class="pl-3">'+siswa.tahun_angkatan_siswa+'</td>';
                row+='</tr>';
            }
        });
        row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
    }

    function onRenderSiswaKelasSuccess(resp){
        var row = "";
        var tbody=$('#tbl-data-pengaturan-kelas').find('tbody');
        var select_kelas_now=$('[name=from_kelas]').val();
        tbody.text('');
        console.log(resp);
        $.each(resp.data,function(key1, siswa){
            console.log(this);
            console.log(siswa);
            if(select_kelas_now==this.kelas_siswa_kelas){
                row+='<tr>';
                    row+='<td class="pl-3">';
                        row+='<div class="form-check"><input class="form-check-input position-static" type="checkbox" value='+siswa.id_siswa+'></div>';
                    row+='</td>';
                    row+='<td class="">';
                        row+='<i class="fas fa-user-circle fa-2x"></i>';
                    row+='</td>';
                    row+='<td class="pl-3"><span style="font-family:segoeuib">'+siswa.nama_lengkap_siswa+'</span><br>'+siswa.ref_siswa;
                    row+='</td>';
                    row+='<td class="pl-3">'+siswa.tahun_angkatan_siswa+'</td>';
                row+='</tr>';
            }
        });
        row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
    }

    function onReadForm(resp){
        var row='';
        var table="<hr/>";
            table+='<div class="row">';
                table+='<div class="col-md-12">';

                  table+='<div class="row form-group">';

                  table+='<div class="col-md-6">';
                  table+='<div class="input-group input-group-lg">';
                  table+='<div class="input-group-prepend">';
                    table+='<span class="input-group-text">Dari-Kelas</span>';
                  table+='</div>';
                      table+='<select name="from_kelas" class="form-control select-styles">';
                          table+='<option value="">Pilih Kelas</option>';
                      table+='</select>';
                  table+='</div>';
                  table+='</div>';


                  table+='<div class="col-md-6">';
                  table+='<div class="input-group input-group-lg">';
                  table+='<div class="input-group-prepend">';
                    table+='<span class="input-group-text">Ke-Kelas</span>';
                  table+='</div>';
                      table+='<select name="to_kelas" class="form-control select-styles">';
                          table+='<option value="">Pilih Kelas</option>';
                      table+='</select>';
                  table+='</div>';
                  table+='</div>';

                  table+='</div>';

                table+='<div class="table-responsive text-left">';
                table+='<table id="tbl-data-pengaturan-kelas" class="table table-sm table-hovered table-pill table-striped">';
                  table+='<thead class="text-white" style="background-color: #487d95!important;font-family: segoeui;">';
                    table+='<tr>';
                        table+='<th scope="col" class="p-3" width="10">'+
                        '<div class="form-check">'+
                            '<input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="option1">'+
                            '</div>'+
                        '</th>';
                        table+='<th scope="col" class="" width="10%"></th>';
                        table+='<th scope="col" class="p-3" width="50%">Pilih Siswa Untuk Dipindahkan</th>';
                      // table+='<th scope="col" class="p-3">SPP</th>';
                      // table+='<th scope="col" class="p-3">DPMP</th>';
                      // table+='<th scope="col" class="p-3">DU</th>';
                      // table+='<th scope="col" class="p-3">Keterangan</th>';
                      table+='<th scope="col" class="text-center p-3">#</th>';
                    table+='</tr>';
                  table+='</thead>';
                  table+='<tbody></tbody>';
                table+='</table>';
                table+='</div>';
                table+='</div>';

                // table+='<div class="col-md-6">';


                //   table+='<div class="form-group">';
                //   table+='<div class="input-group input-group-lg">';
                //   table+='<div class="input-group-prepend">';
                //     table+='<span class="input-group-text mr-1">Ke-Kelas</span>';
                //   table+='</div>';
                //   table+='<select name="filter_tahun_ajaran" class="form-control select-styles">';
                //       table+='<option value="">Pilih Kelas yang akan ditempatkan</option>';
                //   table+='</select>';
                //   table+='</div>';
                //   table+='</div>';

                // table+='<div class="table-responsive text-left">';
                // table+='<table id="tbl-data" class="table table-sm table-hovered table-pill table-striped">';
                //   table+='<thead class="text-white" style="background-color: #487d95!important;font-family: segoeui;">';
                //     table+='<tr>';
                //         table+='<th scope="col" class="p-3" width="10">'+
                //         '<div class="form-check">'+
                //             '<input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="option1">'+
                //             '</div>'+
                //         '</th>';
                //         table+='<th scope="col" class="" width="10"></th>';
                //         table+='<th scope="col" class="p-3">Daftar Siswa di kelas baru</th>';
                //       // table+='<th scope="col" class="p-3">SPP</th>';
                //       // table+='<th scope="col" class="p-3">DPMP</th>';
                //       // table+='<th scope="col" class="p-3">DU</th>';
                //       // table+='<th scope="col" class="p-3">Keterangan</th>';
                //       // table+='<th scope="col" class="text-center p-3">#</th>';
                //     table+='</tr>';
                //   table+='</thead>';
                //   table+='<tbody>'+row+'</tbody>';
                // table+='</table>';
                // table+='</div>';
                table+='</div>';
            table+='</div><hr/>';

            Swal.fire({
              title: 'Pengaturan Kelas Untuk Tahun Ajaran: '+$('[name=filter_tahun_ajaran').val(),
              width: '70%',
              html: table,
              showCloseButton: true,
              showCancelButton: true,
              focusConfirm: false,
              confirmButtonText: 'SIMPAN',
              showLoaderOnConfirm: true,
              // preConfirm: (login) => {
              //   return moveClass()
              //     .then(response => {
              //       if (!response.ok) {
              //         throw new Error(response.statusText)
              //       }
              //       return response.json()
              //     })
              //     .catch(error => {
              //       Swal.showValidationMessage(
              //         'Request failed: ${error}'
              //       )
              //     })
              // },
              allowOutsideClick: () => !Swal.isLoading()
            })
            .then((result) => {
              if (result.value) {
                // Swal.fire({
                //   title: '${result.value.login}',
                //   imageUrl: result.value.avatar_url
                // })
                moveClass();
                // Swal.fire(
                //   'Deleted!',
                //   'Your file has been deleted.',
                //   'success'
                // )
              }
            });

            getSelectKelas();  
    }

    function chooseDo(){
        Swal.fire({
          title: 'Apa yang akan dilakukkan?',
          text: "Pilih salah satu tombol!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Tambah Siswa',
          cancelButtonText: 'Lihat Siswa'
        }).then((result) => {
          if (result.value) {
                // Swal.fire(
                //   'Deleted!',
                //   'Your file has been deleted.',
                //   'success'
                // )
                readForm();
          }
        });
    }

    function moveClass(){
        var arr=[];
        var obj={};
        var from_kelas = $('[name=from_kelas]').val();
        var to_kelas = $('[name=to_kelas]').val();
        var data_siswa = $('#tbl-data-pengaturan-kelas').find('tbody input:checked');
        $.each(data_siswa, function(key){
            obj={
                header_id_siswa_kelas : this.value,
                kelas_siswa_kelas : to_kelas,
                tahun_ajaran : $('[name=filter_tahun_ajaran').val(),
                status_siswa_kelas : 1
            };
            arr.push(obj);
        });
        console.log(arr);
        var obj = new FormData();
        // obj.append()
        obj.append('list_siswa_kelas', JSON.stringify(arr));
        
        ajaxPOST(path + '/save_siswa_kelas',obj,'succMoveClass','onSaveError');
    }
    function succMoveClass(resp){
        console.log("SUCC")
        return resp;
    }
    function onSaveError(resp){
        console.log(resp);
        // alert("SINDAH");
        return resp;
    }
