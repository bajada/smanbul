
//const URL = {
//	bidang: ctx + '/bidang',
//	pemegang_izin: ctx + '/pemegang-izin/',
//	surat_izin: ctx + '/pemegang-izin/{id_pemegang_izin}/surat-izin',
//	pengguna_list_by_id_permohonan: ctx + '/pengguna/list?id_permohonan={id_permohonan}',
//	pengguna_list_by_id_pemegang_izin: ctx + '/pengguna/list?id_pemegang_izin={id_pemegang_izin}',
//	pengguna_list_by_id_bidang: ctx + '/pengguna/list?id_bidang={id_bidang}',
//	
//	detail_dokumen_by_id_permohonan: ctx + '/permohonan/{id_permohonan}/detil-dokumen',
//	detail_tsl_by_id_permohonan: ctx + '/permohonan/{id_permohonan}/detil-satwa',
//}

var btn;
$(document).ready(function(){
//	$('#error_msg').hide();
//	
	// $.ajaxSetup({
	// 	headers : {
	// 		'Authorization' : 'Bearer '+ getCookieValue(tokenCookieName)
	// 	}
	// });
	
	$('.single-date-picker').on('apply.daterangepicker', function(ev, picker) {
		$(this).val(picker.startDate.format('DD-MMM-YYYY'));
		$(this).val(picker.endDate.format('DD-MMM-YYYY'));
		picker.autoUpdateInput = true;
		$(this).change();
	});
	$('.single-date-picker').on('cancel.daterangepicker', function(ev, picker) {
	    $(this).val('');
	});
//	
    /*
     * Format Input Type Currency
     */
    $('.currency').number( true,0);
    $('.dollar').number( true,2);

//	$('.datepicker').datepicker({
//		format: "dd-mm-yyyy",
//		clearBtn: true,
//	    language: "id",
//		keyboardNavigation: false
//	});
//
//	$('.button-collapse').sideNav()
//	
//	$('.datepicker').on('changeDate', function(ev){
//	    $(this).datepicker('hide');
//	});
//
//	$(':input[readonly]').css({'background-color':'rgba(0,0,0,0.06)'});

    // $('.select').select2({
    // 		width: '100%'
    // });
    
	init();
    
    $("input[type=checkbox]").change(function() {
        clearTextSelection();
    });
    //init global btn-load
    try {
        btn = Ladda.create( document.querySelector( '.btn-load' ) );
    }
    catch(err) {
        console.log("Something wrong!");
    }


    $('.select-auto-complete').select2({
      placeholder: "Tentukan Pilihan",
    });
    
});

$(function() {
	$('.select-style').selectric();
});

$(function () {
	$('[data-toggle="popover"]').popover()
});
function showModalMessage(msg){
	$('#msg-info-body').html(msg);
	$('#msg-info').modal('show');
}

function showModalMessage(msg, timeout){
	$('#msg-info-body').html(msg);
	$('#msg-info').modal('show');
	setTimeout(function(){ $('#msg-info').modal('hide'); }, timeout);
}

//function show_image(img){
//	$('#modal-preview-title').text('Perbesar Gambar');
//	$('#modal-preview-content').html('<img src="'+img+'" width="640px" />');
//	$('#modal-preview').modal('open');
//}
//
function getURLParam(param) {
	var getUrlParameter = function getUrlParameter(sParam) {
			var sPageURL = decodeURIComponent(window.location.search.substring(1)),
				sURLVariables = sPageURL.split('&'),
				sParameterName,
				i;

			for (i = 0; i < sURLVariables.length; i++) {
				sParameterName = sURLVariables[i].split('=');

				if (sParameterName[0] === sParam) {
					return sParameterName[1] === undefined ? true : sParameterName[1];
				}
			}
	}
	return getUrlParameter(param);
}
//
function getCookieValue(a) {
    var b = document.cookie.match('(^|;)\\s*' + a + '\\s*=\\s*([^;]+)');
    return b ? b.pop() : '';
}

function url_base64_decode(str) {
	var output = str.replace('-', '+').replace('_', '/');
	switch (output.length % 4) {
		case 0:
			break;
		case 2:
			output += '==';
			break;
		case 3:
			output += '=';
			break;
		default:
			throw 'Illegal base64url string!';
	}
	var result = window.atob(output); //polifyll https://github.com/davidchambers/Base64.js
	return result;
}

function logout(){
	$( "#logout" ).text("Please wait..");
	$.ajax({
	    url: ctx + '/revoke-token',
	    method: "GET",
	    crossDomain: true,
	    contentType: "application/x-www-form-urlencoded",
	    data: '',
	    cache: false,
	    success: function (data) {
	    		clearTokenAndRedirect();
	    },
	    error: function (jqXHR, textStatus, errorThrown) {
	    		clearTokenAndRedirect();
	    }
		
	});	
}

// function clearTokenAndRedirect(){
// 	var org = jQuery.parseJSON(url_base64_decode(getCookieValue(tokenCookieName).split('\.')[1])).user_name.split('::')[1];
// 	console.log(org);
// 	if(window.location.hostname != 'localhost') {
// 		Cookies.remove(tokenCookieName,{domain: domainRoot, path: '/'});
// 		//Cookies.remove('oauth2-refresh-token',{domain: '.hijr.co.id', path: '/'});;	
// 	}else{
// 		Cookies.remove(tokenCookieName,{ path: '/'});
// 		//Cookies.remove('oauth2-refresh-token',{path: '/'});;
// 	}
// //	location.href=ctx + '/login?org='+org;
// 	location.href=ctx + '/login?org='+org;
// }

function ajaxPOST(url,obj,fnsuccess, fnerror){
	$.ajax({
	    url : url,
	    method: "POST",
	    dataType: "JSON",
	    crossDomain: true,
	    contentType: false,
	    processData: false,
	    data : obj,
	    cache: false,
	    success : function (response) {
		    	var fn = window[fnsuccess];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
	    },
	    error : function (response) {
	    		// $('#error_msg').show();
	    		// console.log(response);
			var fn = window[fnerror];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
	    },
	});
}
function ajaxGET(url, fnsuccess, fnerror){
	$.ajax({
	    url: url,
	    method: "GET",
	    dataType: "JSON",
	    success: function (response) {
		    	var fn = window[fnsuccess];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
	    },
	    error: function (jqXHR, textStatus, errorThrown) {
			var fn = window[fnerror];
	    		if(typeof fn === 'function') {
	    		    fn(jqXHR, textStatus, errorThrown);
	    		}
	    }
		
	});	
}
function ajaxGET_new(url, fnsuccess, fnerror, element){
	$.ajax({
	    url: url,
	    method: "GET",
	    success: function (response) {
		    	var fn = window[fnsuccess];
	    		if(typeof fn === 'function') {
	    		    fn(response,element);
	    		}
	    },
	    error: function (jqXHR, textStatus, errorThrown) {
			var fn = window[fnerror];
	    		if(typeof fn === 'function') {
	    		    fn();
	    		}
	    }
		
	});	
}

function ajaxGET_NEW(url, fnsuccess, fnerror, param){
    $.ajax({
        url: url,
        method: "GET",
        success: function (response) {
            if (fnsuccess instanceof Function) {
                fnsuccess(response, param);
            } else {
                var fn = window[fnsuccess];
                if(typeof fn === 'function') {
                    fn(response, param);
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (fnerror instanceof Function) {
                fnerror(jqXHR);
            } else {
                var fn = window[fnerror];
                if(typeof fn === 'function') {
                    fn(jqXHR);
                }
            }
        }
        
    }); 
}

//
function onGetRenderSucc(response,obj){
	console.log(response);
	$.each(response.data, function(key, value) {
		$('[name='+obj.element+']').append(new Option(response.data[key][obj.name], response.data[key][obj.id]));
	});
}
//
///** custom by Hadi **/

function getValue(value, replace = '') {
	return (value == null || value == 'null' ? replace : value) 
}

function getDateForList(datetime) {
	let result = moment(datetime).format('DD-MMM-YYYY').toUpperCase();
	return (result == 'INVALID DATE' ? '' : result);
}

function getDateForInput(datetime) {
	return moment(datetime).format('DD-MM-YYYY');
}


$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}


$("[data-type='currency']").on({
   keyup: function() {
     formatCurrency($(this));
   },
   blur: function() { 
     formatCurrency($(this), "blur");
   }
});
//
//
//$('#modal-form, #modal-remove, #modal-remove-new, #modal-preview, #modal-confirm').modal({
//	dismissible:false
//});
//
//
//$('#modal-alert, #modal-failed-alert').modal({
//	dismissible:false,
//	ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
//        
//        console.log(modal, trigger);
//		setTimeout(function (){modal.modal('close')}, 2000);
//      }
//});


function formatNumber(n) {
 // format number 1000000 to 1,234,567
 return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

//function toRp(angka){
//    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
//    var rev2    = '';
//    for(var i = 0; i < rev.length; i++){
//        rev2  += rev[i];
//        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
//            rev2 += '.';
//        }
//    }
//    return 'Rp. ' + rev2.split('').reverse().join('');
//}

function formatCurrency(input, blur) {
 // appends $ to value, validates decimal side
 // and puts cursor back in right position.
 
 // get input value
 var input_val = input.val();
 
 // don't validate empty input
 if (input_val === "") { return; }
 
 // original length
 var original_len = input_val.length;

 // initial caret position 
 var caret_pos = input.prop("selectionStart");
   
 // check for decimal
 if (input_val.indexOf(".") >= 0) {

   // get position of first decimal
   // this prevents multiple decimals from
   // being entered
   var decimal_pos = input_val.indexOf(".");

   // split number by decimal point
   var left_side = input_val.substring(0, decimal_pos);
   var right_side = input_val.substring(decimal_pos);

   // add commas to left side of number
   left_side = formatNumber(left_side);

   // validate right side
   right_side = formatNumber(right_side);
   
   // On blur make sure 2 numbers after decimal
   if (blur === "blur") {
     right_side += "";//right_side += "00";
   }
   
   // Limit decimal to only 2 digits
   right_side = right_side.substring(0, 2);

   // join number by .
   // input_val = "" + left_side + "." + right_side;
   // input_val = "" + left_side;

 } else {
   // no decimal entered
   // add commas to number
   // remove all non-digits
   input_val = formatNumber(input_val);
   input_val = "" + input_val;
   
   // final formatting
   if (blur === "blur") {
     input_val += "";
   }
 }
 // send updated string to input
 input.val(input_val);

 console.log(input_val);
 // put caret back in the right position
 // var updated_len = input_val.length;
 // caret_pos = updated_len - original_len + caret_pos;
 // input[0].setSelectionRange(caret_pos, caret_pos);
}

//function randomInt(min,max) {
//    return Math.floor(Math.random()*(max-min+1)+min);
//}
//
//function getAutoNumberDate() {
//	return new Date().getTime() + randomInt(100, 999);
//}


function createPagination(id, func, response) {
	console.log(response);
	var html = "";
	var activePage = (response.next_page_number-1);//activePage;
//	alert(activePage);
	if (activePage == 1) {
		html += '<li class="page-item disabled">';
		html += '<a class="page-link" href="#" aria-label="Previous">'+
			        '<span aria-hidden="true">&laquo;</span>'+
			        '<span class="sr-only">Previous</span>'+
			      '</a>';
		html += '</li>';
	} else {
		html += '<li class="page-item">';
		html += '<a class="page-link" href="javascript:'+func+'('+(activePage - 1)+')" aria-label="Previous">'+
				'<span aria-hidden="true">&laquo;</span>'+
			'</a>';
		html += '</li>';
	}
	var i;
	for ( i = 1; i <= response.count; i++) {
		if (activePage == i) {
			html += '<li class="page-item active"><a class="page-link" href="javascript:'+func+'(\'' + i + '\',\'' + 'pertanyaan_id='+response.data[0].pertanyaan_id + '\')">' + i + '</a></li>';
		} else {
			html += '<li class="page-item"><a class="page-link" href="javascript:'+func+'(\'' + i + '\',\'' + 'pertanyaan_id='+response.data[0].pertanyaan_id + '\')">' + i + '</a></li>';
		}
	}
	if (activePage == response.count) {
		html += '<li class="page-item disabled">';
		html += '<a class="page-link" href="#" aria-label="Next">'+
			        '<span aria-hidden="true">&raquo;</span>'+
			        '<span class="sr-only">Next</span>'+
			      '</a>';
		html += '</li>';
	} else {
		html += '<li class="page-item"><a class="page-link" href="javascript:'+func+'(' +(activePage + 1) + ')" aria-label="Next"> <span aria-hidden="true">&raquo;</span> </a></li>';
	}
	$('#'+id).html(html);
}

function createPagination2(id, func, response) {
	var html = "";
	if (response.limit == 0) {
		html += '<li class="disabled"><span aria-hidden="true">&laquo;</span> </li>';
		html += '<li class="disabled"><span aria-hidden="true">&raquo;</span></li>';
	} else {
		if (response.activePage == 1) {
			html += '<li class="page-item disabled">';
			html += '<a class="page-link" href="#" aria-label="Previous">'+
				        '<span aria-hidden="true">&laquo;</span>'+
				        '<span class="sr-only">Previous</span>'+
				      '</a>';
			html += '</li>';
		} else {
			html += '<li class="page-item">';
			html += '<a class="page-link" href="javascript:'+func+'('+(response.activePage - 1)+')" aria-label="Previous">'+
					'<span aria-hidden="true">&laquo;</span>'+
				'</a>';
			html += '</li>';
		}
		var i;
		for ( i = 1; i <= response.pageCount; i++) {
			if (response.activePage == i) {
				html += '<li class="page-item active"><a class="page-link" href="javascript:'+func+'(' + i + ')">' + i + '</a></li>';
			} else {
				html += '<li class="page-item"><a class="page-link" href="javascript:'+func+'(' + i + ')">' + i + '</a></li>';
			}
		}
		if (response.activePage == response.pageCount) {
			html += '<li class="page-item disabled">';
			html += '<a class="page-link" href="#" aria-label="Next">'+
				        '<span aria-hidden="true">&raquo;</span>'+
				        '<span class="sr-only">Next</span>'+
				      '</a>';
			html += '</li>';
		} else {
			html += '<li class="page-item"><a class="page-link" href="javascript:'+func+'(' +(response.activePage + 1) + ')" aria-label="Next"> <span aria-hidden="true">&raquo;</span> </a></li>';
		}
	}
	$('#'+id).html(html);
}

function clearTextSelection() {
    if (window.getSelection) {
        if (window.getSelection().empty) {  // Chrome
            window.getSelection().empty();
        } else if (window.getSelection().removeAllRanges) {  // Firefox
            window.getSelection().removeAllRanges();
        }
    } else if (document.selection) {  // IE?
        document.selection.empty();
    }
}

function render_row_def(resp,obj){
    var row='';
    $.each(resp.data,function(key,value){
        row += '<tr class="data-row" id="row-'+key+'">';
            row += '<td>'+(key+1)+'</td>';
            $.each(obj,function(key){
                row += '<td>'+value[this]+'</td>';
            });
        row += '</tr>';
    });
    return row;
}

function render_obj_data(resp){
    var data=[];
    $.each(resp.data,function(key,value){
        // $.each(obj,function(key){
        //     row += '<td>'++'</td>';
        // });
        data.push(this);
    });
    return data;
}


    function contoh_looping_date(){
        var resultList = [];
        var tahun_ajaran = $('#lbl_tahun_ajaran').text().substring(2).split("/");
        var date = new Date("July 10, "+tahun_ajaran[0]);
        var endDate = new Date("June 10, "+tahun_ajaran[1]);
        var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        while (date <= endDate)
        {
            var stringDate = monthNameList[date.getMonth()] + " " + date.getFullYear();
            resultList.push(stringDate);
            date.setMonth(date.getMonth() + 1);
        }

        return resultList;
    }


    function konfirmasi_modal(){
        swal("Message", "response.message", "success", {
          buttons: {
            //cancel: "Run away!",
            go: {
              text: "Tambah lagi data siswa",
              value: "go",
            },
            back: {
              text: "Kembali ke daftar siswa",
              value: "back",
            }
          },
        })
        .then((value) => {
          switch (value) {
         
            case "go":
              window.location.href=ctx+"page/form_siswa_view";
              break;
         
            case "back":
              window.location.href=ctx+"page/siswa_view";
              break;
         
            default:
              swal("Something Wrong!","error");
          }
        });       
    }

    function onError(response){
    	console.log(response);
    	Swal.fire({
		  type: 'error',
		  title: 'Oops...',
		  text: 'Something went wrong!',
		  // footer: '<a href>Why do I have this issue?</a>'
		})
    }

    function showAlertMessage2(msg, timeout=0){
    	Swal.fire({
          type: 'warning',
          title: 'Tidak dapat menyimpan data',
          text: 'Karena anda belum melakukan entri data apapun!',
          animation: true,
          customClass: 'animated tada'
        });
    }

    function showAlertMessage(resp_msg, timeout=0){
    	// Swal.fire({
     //      type: resp_msg.icon,
     //      title: resp_msg.title,
     //      text: resp_msg.body,
     //      animation: true,
     //      customClass: 'animated tada'
     //    });
        //
        let timerInterval
		Swal.fire({
		  type: resp_msg.icon,
		  title: resp_msg.title,
		  html: resp_msg.body,
		  timer: timeout,
		  onBeforeOpen: () => {
		    // Swal.showLoading()
		    // timerInterval = setInterval(() => {
		    //   Swal.getContent().querySelector('strong')
		    //     .textContent = Swal.getTimerLeft()
		    // }, 100)
		  },
		  onClose: () => {
		    clearInterval(timerInterval)
		  }
		}).then((result) => {
		  if (
		    // Read more about handling dismissals
		    result.dismiss === Swal.DismissReason.timer
		  ) {
		    console.log('I was closed by the timer')
		  }
		})
    }

    function checkTahunAjaran(){
      var date = new Date();
      var month = date.getMonth();
      if(month>=6){
        date=date.getFullYear()+"/"+(date.getFullYear()+1);
      }else{
        date=(date.getFullYear()-1)+"/"+date.getFullYear();
      }
      return date;
    }


