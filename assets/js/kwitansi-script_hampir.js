    var path = ctx + 'IuranSiswaController';
    var params='';
    var selected_action='';
    var selected_id='';
    var selected_id_kelas=0;
    var tahun_ajaran='';
    var jenis_iuran='';
    var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    var obj_arr = [];       
    
    var lbl_total_biaya = 0;
    var lbl_nominal_dpmp = 0;
    var lbl_nominal_du = 0;
    var lbl_nominal_iuran = 0;

    function init() {
        selected_id = ($.urlParam('id') == null ? '' : $.urlParam('id'));
        tahun_ajaran = ($.urlParam('tahun_ajaran') == null ? '' : $.urlParam('tahun_ajaran'));

        if(selected_id != '') {
            console.log(path + '/get_by_id/'+selected_id+'?tahun_ajaran='+tahun_ajaran);
            ajaxGET(path + '/get_by_id/'+selected_id+'?tahun_ajaran='+tahun_ajaran,'onGetSuccess','onGetError');
            //$('#daftar_permohonan').prop('disabled', false);
        }

        $('#search-form').submit(function(e){
            displaySiswa();
            e.preventDefault();
        });
    }
    
    function readHistoryPayment(jenis){
        if(selected_id != '') {
            jenis_iuran=jenis;
            ajaxGET(path + '/get_by_id/'+selected_id,'onGetReadHistoryPayment','onGetError');
        }
    }
    function onGetReadHistoryPayment(resp){
        if(resp.data.data_siswa){
            var rows='';
            var no=1;

            var new_month=get_tahun_ajaran_sekarang();
            $.each(new_month, function(keys, date){
            $.each(resp.data.data_iuran,function(key){
                var stringDate = monthNameList[date.getMonth()] + " " + date.getFullYear();
                if(jenis_iuran==this.jenis_iuran && stringDate==this.bulan_iuran && jenis_iuran=="DU"){
                    // if(stringDate==this.bulan_iuran){
                        rows+='<tr class="bg-light" style="opacity: 0.9;">';
                          rows+='<td>'+1+'</td>';
                          rows+='<td>'+this.jatuh_tempo_iuran+'('+this.jenis_iuran+')</td>';
                          // rows+='<td class="text-left">-</td>';
                          rows+='<td class="text-left">'+this.no_iuran+'</td>';
                          rows+='<td class="text-right">'+this.tanggal_iuran+'</td>';
                          rows+='<td class="text-right">'+$.number(this.besar_iuran)+'</td>';
                          rows+='<td>'+this.status_iuran+'</td>';
                          rows+='<td align="center"><i class="fas text-success fa-check-circle"></i></td>';
                        rows+='</tr>';
                        no++;
                    // }
                }
            });
            });

            // rows='';
            $.each(resp.data.data_iuran,function(key){
                if(this.jenis_iuran==jenis_iuran && jenis_iuran=="DPMP"){
                    rows+='<tr class="bg-light" style="opacity: 0.9;">';
                      rows+='<td>'+no+'</td>';
                      rows+='<td>'+this.jatuh_tempo_iuran+'('+this.jenis_iuran+')</td>';
                      // rows+='<td class="text-left">-</td>';
                      rows+='<td class="text-left">'+this.no_iuran+'</td>';
                      rows+='<td class="text-right">'+this.tanggal_iuran+'</td>';
                      rows+='<td class="text-right">'+$.number(this.besar_iuran)+'</td>';
                      rows+='<td>'+this.status_iuran+'</td>';
                      rows+='<td align="center"><i class="fas text-success fa-check-circle"></i></td>';
                    rows+='</tr>';
                    no++;
                }
            });

            var table="";
            table+='<table id="tbl-data" class="table table-sm table-hovered table-pill">';
              table+='<thead class="text-white" style="background-color: #487d95!important;font-family: segoeui;">';
                table+='<tr>';
                  table+='<th scope="col" class="p-3">No.</th>';
                  table+='<th scope="col" class="p-3">Jatuh Tempo</th>';
                  table+='<th scope="col" class="p-3">No.Iuran</th>';
                  table+='<th scope="col" class="p-3">Tgl.Iuran</th>';
                  table+='<th scope="col" class="p-3">Besar Iuran</th>';
                  table+='<th scope="col" class="p-3">Keterangan</th>';
                  table+='<th scope="col" class="text-center p-3">#</th>';
                table+='</tr>';
              table+='</thead>';
              table+='<tbody>'+rows+'</tbody>';
            table+='</table>';

            Swal.fire({
              title: 'Riwayat Pembayaran',
              width: '60%',
              html: table,
              showCloseButton: true,
              showCancelButton: true,
              focusConfirm: false
            });
        }
    }

    function readDaftarTunggakan(jenis=''){
        if(selected_id != '') {
            ajaxGET(path + '/check_tunggakan/'+selected_id+'?check_tunggakan='+true,'onGetReadDaftarTunggakan','onGetError');
            // ajaxGET(path + '/get_by_id/'+selected_id+'?check_tunggakan='+true,'onGetReadDaftarTunggakan','onGetError');
        }
    }
    function getTahunAjaranAll(){
        var strdate=[];
        var angkatan= new Date("July 10, 2017");
        // var date = new Date("December 10, 2019");//+tahun_ajaran[0]
        var date = new Date();
        var selisih=(date.getFullYear()-angkatan.getFullYear());
        for(var start_ajaran=angkatan.getFullYear(); start_ajaran < angkatan.getFullYear()+selisih; start_ajaran++){
            if(start_ajaran+"/"+(start_ajaran+1)!=date){
                strdate.push(start_ajaran+"/"+(start_ajaran+1).toString());
            }
        }
        return strdate;
    }
    function getTahunAjaranAllAndSet(tahun_angkatan){
        var strdate=[];
        var angkatan= new Date("July 10, "+tahun_angkatan);
        // var date = new Date("December 10, 2019");//+tahun_ajaran[0]
        var date = new Date();
        var selisih=(date.getFullYear()-angkatan.getFullYear());
        for(var start_ajaran=angkatan.getFullYear(); start_ajaran < angkatan.getFullYear()+selisih; start_ajaran++){
            if(start_ajaran+"/"+(start_ajaran+1)!=date){
                strdate.push(start_ajaran+"/"+(start_ajaran+1).toString());
            }
        }
        return strdate;
    }

    function onGetReadDaftarTunggakan(resp){
        console.log(resp);
        var tunggakan = resp.data.new_method.tunggakan;

        var rows='';var no=1;
        $.each(tunggakan, function(key){
            console.log(this);
            rows+='<tr class="bg-light" style="opacity: 0.9;">';
                rows+='<td>'+no+'</td>';
                rows+='<td>'+this.tahun_ajaran+'</td>';
                rows+='<td class="text-left">'+$.number(this.sisa_spp)+'</td>';
                // rows+='<td class="text-left">'+$.number(total_dpmp-sum_dpmp)+'</td>';
                rows+='<td class="text-left">'+$.number(this.sisa_du)+'</td>';

                if(this.sisa_spp==0 && this.sisa_du==0){
                    rows+='<td class=""><i class="fas text-success fa-check-circle"></i> LUNAS</td>';
                }else{
                    rows+='<td class=""><i class="fas text-danger fa-times-circle"></i> BELUM LUNAS</td>';
                }
                rows+='<td align="center"><a href="?id='+selected_id+'&tahun_ajaran='+this.tahun_ajaran+'"><i class="fas text-info fa-eye"></i></a></td>';
            rows+='</tr>';
            no++;
        });

        var table="<hr/>";
        table+='<div class=" table-responsive">';
        table+='<table id="tbl-data" class="table table-sm table-hovered table-pill">';
          table+='<thead class="text-white" style="background-color: #487d95!important;font-family: segoeui;">';
            table+='<tr>';
              table+='<th scope="col" class="p-3">No.</th>';
              table+='<th scope="col" class="p-3">Tahun Ajaran</th>';
              table+='<th scope="col" class="p-3">SPP</th>';
              // table+='<th scope="col" class="p-3">DPMP</th>';
              table+='<th scope="col" class="p-3">DU</th>';
              table+='<th scope="col" class="p-3">Keterangan</th>';
              table+='<th scope="col" class="text-center p-3">#</th>';
            table+='</tr>';
          table+='</thead>';
          table+='<tbody>'+rows+'</tbody>';
        table+='</table>';
        table+='</div>';

        Swal.fire({
          title: 'Riwayat Iuran',
          width: '60%',
          html: table,
          showCloseButton: true,
          showCancelButton: true,
          focusConfirm: false
        });

    }

    function onGetSuccess(resp){
        console.log(resp);
        var count_besar_dpmp=0;
        var count_sisa_dpmp=0;
        var count_besar_du=0;
        var count_sisa_du=0;
        var count_besar_iuran=0;

        if(resp.data.data_siswa){
            var value_siswa = resp.data.data_siswa;
            var value_siswa_detil = resp.data.data_siswa_detil;
            var value_siswa_kelas = resp.data.data_siswa_kelas;
            var value_iuran = resp.data.data_iuran;
            var value_iuran_old = resp.data.data_tunggakan;
            var new_method = resp.data.new_method;
            console.log(resp);

            $('[name=filter_keyword]').val(value_siswa.ref_siswa);
            $('#lbl_no_ref').html(": "+value_siswa.ref_siswa);
            $('#lbl_nama_lengkap').html(": "+value_siswa.nama_lengkap_siswa);

            $('#lbl_tahun_angkatan').html(": "+value_siswa.tahun_angkatan_siswa);
            
                if(value_siswa_kelas.length>0){
                    $.each(value_siswa_kelas, function(key){
                        console.log(this);
                        if(this.status_siswa_kelas){
                            console.log(this);
                            $('#lbl_kelas').html(": "+this.kelas);
                            $('#lbl_tahun_ajaran').html(": "+this.tahun_ajaran);

                            selected_id_kelas=this.id_siswa_kelas;
                        }
                    });
                }else{
                    $('#lbl_kelas').html(": "+value_siswa_kelas.kelas);
                    $('#lbl_tahun_ajaran').html(": "+value_siswa_kelas.tahun_ajaran);
                }
        }
    }

    function onGetError(resp){
        console.log(resp);
        //swal("Terjadi Kesalahan", "Mohon periksa koneksi internet anda!", "warning");
        Swal.fire({
          type: 'warning',
          title: 'Terjadi Kesalahan',
          text: 'Mohon periksa koneksi internet anda!',
          animation: false,
          customClass: 'animated tada'
        });
    }

    function displaySiswa(){
        //$('#more-button-row').remove();
        //var tbody = $("#tbl-data").find('tbody');
        // if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
        //     tbody.text('');
        // }
        
        //tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        
        params = $( "#search-form" ).serialize();
        ajaxGET(path + '/list_table?page='+1+'&'+params,'onDisplaySiswaSuccess','onGetListError');
    }
    function onDisplaySiswaSuccess(resp){
        console.log(resp);
        console.log(resp.data.data_siswa);

            
        var count_besar_dpmp=0;
        var count_sisa_dpmp=0;
        var count_besar_du=0;
        var count_sisa_du=0;
        var count_besar_iuran=0;

        if(resp.data.data_siswa){
            var value_siswa = resp.data.data_siswa[0];
            window.location.href="kwitansi_view?id="+value_siswa.id_siswa;
            // window.location.href="iuran_view?id="+value_siswa.id_siswa_kelas;
        }else{
            $('#lbl_no_ref').html(":");
            $('#lbl_nama_lengkap').html(":");
            $('#lbl_kelas').html(":");
            $('#lbl_tahun_ajaran').html(":");
            $('#lbl_tahun_angkatan').html(":");
            var resp_msg={"title" : "Data tidak ditemukan", "body" : "Mohon periksa kembali NO.REF anda", "icon"  : "warning"};
            showAlertMessage(resp_msg, 1200);
            // swal("Data tidak ditemukan","Mohon periksa kembali NO.REF anda","warning");
            renderDisplayIuranBulanan();
            // //dpmp
            $('#lbl_besar_dpmp').html(": "+$.number(count_besar_dpmp,2));
            $('#lbl_sisa_dpmp').html(": "+$.number((count_besar_dpmp-count_sisa_dpmp),2));       
            // //du
            $('#lbl_besar_du').html(": "+$.number(count_besar_du,2));
            $('#lbl_sisa_du').html(": "+$.number((count_besar_du-count_sisa_du),2));  
        }
    }

    function get_tahun_ajaran_sekarang(){
        var tahun_ajaran = $('#lbl_tahun_ajaran').text().substring(2).split("/");
        var date = new Date("July 10, "+tahun_ajaran[0]);
        var endDate = new Date("June 10, "+tahun_ajaran[1]);
        var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        var new_month=[];
        for (var i=date; i <= endDate; date.setMonth(date.getMonth() + 1)){
            var stringDate = monthNameList[date.getMonth()] + " " + date.getFullYear();
            var jatuh_tempo = date.getFullYear() + "-" + ((date.getMonth()+1).toString().length==1?"0"+(date.getMonth()+1):date.getMonth()+1) + "-" + date.getDate();  
            // console.log(new Date(monthNameList[date.getMonth()]+" 10, "+date.getFullYear()));
            new_month.push(new Date(monthNameList[date.getMonth()]+" 10, "+date.getFullYear()));
        }
        return new_month;
    }
    function get_tahun_ajaran(string_date){
        var tahun_ajaran = string_date.split("/");
        var date = new Date("July 10, "+tahun_ajaran[0]);
        var endDate = new Date("June 10, "+tahun_ajaran[1]);
        var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        var new_month=[];
        for (var i=date; i <= endDate; date.setMonth(date.getMonth() + 1)){
            var stringDate = monthNameList[date.getMonth()] + " " + date.getFullYear();
            var jatuh_tempo = date.getFullYear() + "-" + ((date.getMonth()+1).toString().length==1?"0"+(date.getMonth()+1):date.getMonth()+1) + "-" + date.getDate();  
            // console.log(new Date(monthNameList[date.getMonth()]+" 10, "+date.getFullYear()));
            new_month.push(new Date(monthNameList[date.getMonth()]+" 10, "+date.getFullYear()));
        }
        return new_month;
    }

    function renderDisplayIuranBulanan(res_data,count_besar_iuran){
        console.log(res_data);
        ///
        // var tahun_ajaran = $('#lbl_tahun_ajaran').text().substring(2).split("/");
        // var date = new Date("July 10, "+tahun_ajaran[0]);
        // var endDate = new Date("June 10, "+tahun_ajaran[1]);
        var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        ///

        var tbody = $("#tbl-data").find('tbody');
        var row="";
        var key=1;
        ///
        // var date = new Date("July 10, "+tahun_ajaran[0]);
        // var endDate = new Date("June 10, "+tahun_ajaran[1]);
        var new_month=get_tahun_ajaran_sekarang();
        // for (var i=date; i <= endDate; date.setMonth(date.getMonth() + 1)){
        //     var stringDate = monthNameList[date.getMonth()] + " " + date.getFullYear();
        //     var jatuh_tempo = date.getFullYear() + "-" + ((date.getMonth()+1).toString().length==1?"0"+(date.getMonth()+1):date.getMonth()+1) + "-" + date.getDate();  
            
        //     new_month.push(new Date(monthNameList[date.getMonth()]+" 10, "+date.getFullYear()));
        // }
        /// 

        var new_arr=[];
            $.each(new_month, function(key, date){
                console.log(true);
                var new_obj={
                    "id_iuran" : 0,
                    "no_iuran" : "",
                    "tanggal_iuran" : "",
                    "besar_iuran" : 0,
                    "status_iuran" : "",
                }
                stringDate = monthNameList[date.getMonth()] + " " + date.getFullYear();
                jatuh_tempo = date.getFullYear() + "-" + ((date.getMonth()+1).toString().length==1?"0"+(date.getMonth()+1):date.getMonth()+1) + "-" + date.getDate();  
                $.each(res_data,function(keys, value){
                    if(value.bulan_iuran==stringDate && value.status_iuran=="LUNAS" && value.jenis_iuran=="IURAN_PER_BULAN"){
                        row+='<tr id=row-'+value.id_iuran+' class="bg-light" style="opacity: 0.9;">';
                        // row+='<td>'+key+'</td>';
                        new_obj['id_iuran']=value.id_iuran;
                        new_obj['status_iuran']=value.status_iuran;
                        new_obj['besar_iuran']=value.besar_iuran;
                        new_obj['tanggal_iuran']=value.tanggal_iuran;
                        new_obj['no_iuran']=value.no_iuran;
                        // new_arr.push(new_obj);
                    }
                });
                if(row=="") {
                    row+='<tr id=row-'+key+'>';
                    // new_arr.push(new_obj);
                }
                // new_obj['id_iuran']=0; 
                row+='<td>'+"#"+'</td>';
                row+='<td>'+stringDate+'</td>';
                row+='<td class="text-danger">'+jatuh_tempo+'</td>';
                row+='<td>'+new_obj.no_iuran+'</td>';
                row+='<td>'+new_obj.tanggal_iuran+'</td>';
                row+='<td contenteditable="true">'+$.number(count_besar_iuran)+'</td>';
                row+='<td>'+new_obj.status_iuran+'</td>';
                if(new_obj.status_iuran=="LUNAS"){
                    row+='<td align="center"><i class="fas text-success fa-check-circle"></i></td>';
                }else{
                    row+='<td align="center">';
                    row+='<div class="form-check">';
                        row+='<input class="form-check-input" type="checkbox" value='+count_besar_iuran+' name="tekan_saja" onclick="setIuranPerBulan('+count_besar_iuran+')" required>';
                    row+='</div>';
                    row+='</td>';
                }

                row+='</tr>';
                key++;
            });

            // console.log(": ", new_obj);
            console.log(": ", new_arr);
            tbody.html(row);
    }

    // function renderDisplayIuranBulanan(res_data, count_besar_iuran){
    //     console.log(res_data);
    //     var tahun_ajaran = $('#lbl_tahun_ajaran').text().substring(2).split("/");
    //     var date = new Date("July 10, "+tahun_ajaran[0]);
    //     var endDate = new Date("June 10, "+tahun_ajaran[1]);
    //     var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    //     var monthNameList_random = ["Jul", "Aug", "Sep", "Oct", "Nov", "Dec","Jan", "Feb", "Mar", "Apr", "May", "Jun"];
        
    //     var tbody = $("#tbl-data").find('tbody');
    //     var row="";
    //         if(res_data!=undefined){
    //             if(res_data.length!=0){
    //                 var data_obj=[];
    //                 var object={};
    //                 $.each(res_data,function(keys){
    //                     // if(selected_id==this.id_siswa_iuran) {
    //                          data_obj.push(this);
    //                     // }
    //                 });
    //                 // data_obj.sort((a, b) => (a.key > b.key) ? 1 : -1)
    //                 console.log("data_objs", data_obj);

    //                 $.each(data_obj, function(key){
    //                     if(this.bulan_iuran!=""){
    //                         if(this.status_iuran=='LUNAS') row+='<tr class="bg-light" style="opacity: 0.9;" data-id='+this.id_iuran+'>';
    //                         else row+='<tr data-id='+this.id_iuran+'>';
    //                             row+='<td>'+(key+1)+'</td>';
    //                             row+='<td>'+this.bulan_iuran+'</td>';
    //                             row+='<td>'+this.jatuh_tempo_iuran+'</td>';
    //                             row+='<td>'+this.no_iuran+'</td>';
    //                             row+='<td>'+this.tanggal_iuran+'</td>';
    //                             // row+='<td>'+this.besar_iuran+'</td>';
    //                             row+='<td>'+$.number(count_besar_iuran)+'</td>';
    //                             row+='<td>'+this.status_iuran+'</td>';

    //                             if(this.status_iuran=="LUNAS"){
    //                                 row+='<td align="center"><i class="fas text-success fa-check-circle"></i></td>';
    //                             }else{
    //                                 row+='<td align="center">';
    //                                     row+='<div class="form-check">';
    //                                     row+='<input class="form-check-input" type="checkbox" value='+count_besar_iuran+' name="tekan_saja" onclick="setIuranPerBulan('+count_besar_iuran+')" required>';
    //                                     row+='</div>';
    //                                 row+='</td>';
    //                             }

    //                         row+='</tr>';
    //                     }
    //                 });
                    
    //             }
    //         }
    //     tbody.html(row);
    // }

    function setIuranPerBulan(value){
        var data = $('[name=tekan_saja]:checked');
        lbl_nominal_iuran=0;
        $.each(data,function(key){
            lbl_nominal_iuran+=parseInt(this.value);
        });
        var check = $('[name=tekan_saja]').is(':checked');
        if(check)
        $('#lbl_biaya_iuran_per_bulan').html(': '+$.number(lbl_nominal_iuran,2));
        else
        $('#lbl_biaya_iuran_per_bulan').html(': 0');

        $('#lbl_total_biaya').text(": "+($.number(lbl_nominal_iuran+lbl_nominal_dpmp+lbl_nominal_du,2)));
    }

    function renderBulanKe(val){
        var html="";
        for(var i = 0; i < val;i++){
          html+='<input type="text" class="form-control mt-1" id="" placeholder="Bulan Ke">';
        };
        $('#bulan-ke').html(html);
    }
    function setBulanIuran(){
        var val='';
        $.each($('#bulan-ke input'), function(key){
          console.log(this.value);
          val+=";"+this.value;
        })
        $('[name=bulan_iuran]').val(val.substring(1));
    }

    function display(page = 1){
        $('#more-button-row').remove();
        var tbody = $("#tbl-data").find('tbody');
        if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
            tbody.text('');
        }
        
        tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        
        params = $( "#search-form" ).serialize();
        ajaxGET(path + '/list_table?page='+page+'&'+params,'onGetListSuccess','onGetListError');
    }

    function onGetListSuccess(response){
        console.log(response);
        $('#loading-row').remove();
        $('#no-data-row').remove();
        
        var tbody = $("#tbl-data").find('tbody');
        
        var row = "";
        var num = $('.data-row').length+1;
        $.each(response.data,function(key,value){
            row += render_row(value, num);
            num++;
        });
        row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
        
    }
    function render_row(value, num){
    //  console.log(value);
        var row = "";
        row += '<tr class="data-row" id="row-'+value.id_siswa+'">';
        row += '<td>'+(num)+'</td>';
        row += '<td class="">'+value.nis_siswa+'</td>';
        row += '<td class="">'+value.nama_lengkap_siswa+'</td>';
        row += '<td class="">'+value.kelas_siswa+'</td>';
        row += '<td class="">'+value.jk_siswa+'</td>';
        row += '<td class="">';
        row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-info pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'edit\')"><i class="fas fa-pencil-alt"></i></a> ';
        row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'delete\')"><i class="fas fa-trash-alt"></i></a>';
        row += '</td>';
        row += '</tr>';
        return row;
    }
    function onGetListError(response) {
        console.log(response);
        let timerInterval
        Swal.fire({
          title: 'Auto close alert!',
          html: 'I will close in <strong></strong> seconds.',
          timer: 1000,
          onBeforeOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
              Swal.getContent().querySelector('strong')
                .textContent = Swal.getTimerLeft()
            }, 100)
          },
          onClose: () => {
            clearInterval(timerInterval)
          }
        }).then((result) => {
          if (
            // Read more about handling dismissals
            result.dismiss === Swal.DismissReason.timer
          ) {
            console.log('I was closed by the timer')
          }
        })
        $('#ui-view').html($('#ui-view-msg-error').html());
    }

    function save(){
        // var obj = new FormData(document.querySelector('#form'));
        var obj = new FormData();
        if(selected_id != '') obj.append('id', selected_id);
        obj.append("list_data_iuran_bulan", JSON.stringify(obj_arr));
        obj.append("nominal_dpmp", $('[name=nominal_dpmp]').val());
        obj.append("nominal_du", $('[name=nominal_du]').val());
        ajaxPOST(path + '/save',obj,'onActionSuccess','onGetError');
    }

    function onSaveSuccess(response){
        console.log(response);
        $('#msg-info-body').html('Data telah berhasil di update.');
        $('#msg-info').modal('show');
        // setTimeout(function(){ window.location.href="pengguna?vmenu=daftar"; }, 1000);
    }
    function onActionSuccess(response){
        // display();
        // $('#modal-form').modal('hide');
        // $('#msg-confirm').modal('hide');
        // selected_action = '';
        console.log(response);
        Swal.fire({
          type: 'success',
          title: 'Message',
          text: response.message,
          //footer: '<a href>Why do I have this issue?</a>'
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Oke!'
        })
        .then((value) => {
            if(value.value){
                location.reload();
            }
        });
    }

    function doAction(id, action, confirm = 0){
        selected_action = action;
        $('#modal-form-msg').hide();
        $('#msg-confirm-alert').hide();
        if(confirm == 0) ajaxGET(path + '/get_by_id/'+id+'?action='+selected_action,'onPrepareActionSuccess','onError');
        else ajaxPOST(path + '/do_action/'+id+'/'+selected_action,{},'onActionSuccess','onError');
    }

    function onPrepareActionSuccess(response) {
        console.log(response);
        var value = response.data;
        selected_id = value.id_siswa;
        if(selected_action == 'edit'){
            fillFormValue(value);
            $('#modal-form').modal('show');
        }else{
            swal({
              title: "Are you sure?",
              text: response.message,
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
                console.log(willDelete);
                if(willDelete)
                doAction(selected_id,'delete', 1);
            });
        }
    }

    function fillFormValue(value){
        $('#form')[0].reset();
        
        $('[name=nis_siswa]').val(value.nis_siswa);
        $('[name=nama_siswa]').val(value.nama_siswa);
        $('[name=kelas_siswa]').val(value.kelas_siswa);
    }

    function add() {
        selected_id = ''; // wajib
        selected_action = 'add'; // wajib
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal-form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Tambah Siswa'); // Set Title to Bootstrap modal title
        $('#form-pass').removeClass("d-none");

    }
