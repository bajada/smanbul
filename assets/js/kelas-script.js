	var path = ctx + 'KelasController';
    var params='';
    var selected_action='';
    var selected_id='';

    function init() {
        displaySide(1);

        $('#search-form').submit(function(e){
            display();
            e.preventDefault();
        });
        
        $('#btn-yes-msg-confirm').click(function(e){
            if(selected_action == 'delete' ){
                doAction(selected_id, selected_action, 1);
            }
        });
    };

    function displaySide(page){
        var tbody = $("#list-group-data");
        tbody.text('');
        //tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        $("#tbl-data").find('tbody').text('');
        params = "filter_jurusan_kelas="+$('[name=filter_jurusan_kelas]').val();
        ajaxGET(path + '/list_table_side?page='+page+'&'+params,'onGetDisplaySideSuccess','onGetListError');
    }

    function onGetDisplaySideSuccess(response){
        console.log(response);
        var tbody = $("#list-group-data");
        
        var row = "";
        var num = $('.data-row').length+1;
        $.each(response.data,function(key,value){
            row += '<li onclick="display(1, \''+"&filter_jurusan_kelas="+value.jurusan_kelas+'\');" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center bg-head">'+value.jurusan_kelas+' <span class="badge badge-primary badge-pill">'+value.jumlah_kelas+'</span></li>';
            num++;
        });
        row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
        
    }

    function display(page = 1, param=""){
        $('#more-button-row').remove();
        var tbody = $("#tbl-data").find('tbody');
        // if(('' != $( "#search-form" ).serialize()) || selected_action != ''){
            // alert("Parameter pencarian masih kosong.");
        // }else{

            tbody.text('');
        // }
        
        tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        
        params = $( "#search-form" ).serialize()+"&filter_kelas="+$('[name=filter_jurusan_kelas]').val()+param;
        ajaxGET(path + '/list_table?page='+page+'&'+params,'onGetListSuccess','onGetListError');
    }

    function onGetListSuccess(response){
        console.log(response);
        $('#loading-row').remove();
        $('#no-data-row').remove();
        
        var tbody = $("#tbl-data").find('tbody');
        
        var row = "";
        var num = $('.data-row').length+1;
        $.each(response.data,function(key,value){
            row += render_row(value, num);
            num++;
        });
        // if(response.next_more){
        //     row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
        //     row += '    <button class="btn waves-effect green sh" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
        //     row += '</div></td></tr>';
        // }
        row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
        
    }
    function render_row(value, num){
    //  console.log(value);
        var row = "";
        row += '<tr class="data-row " id="row-'+value.id_kelas+'">';
        row += '<td>'+(num)+'</td>';
        // row += '<td class="">'+value.tingkat_kelas+" "+value.jurusan_kelas+" "+value.sub_tingkat_kelas+'</td>';
        row += '<td class="">'+value.tingkat_kelas+'</td>';
        row += '<td class="">'+value.jurusan_kelas+'</td>';
        row += '<td class="">'+value.sub_tingkat_kelas+'</td>';
        row += '<td class="">'+(value.wali_kelas!=""?value.wali_kelas:"-")+'</td>';
        row += '<td class="">';
        row += '<a href="javascript:void(0)" class="btn btn-sm btn-square pill btn-info" type="" onclick="doAction(\''+value.id_kelas+'\',\'edit\')"><i class="fas fa-pencil-alt"></i></a> ';
        row += '<a href="javascript:void(0)" class="btn btn-sm btn-square pill btn-danger" type="" onclick="doAction(\''+value.id_kelas+'\',\'delete\')"><i class="fas fa-trash-alt"></i></a>';
        row += '</td>';
        row += '</tr>';
        return row;
    }
    function onGetListError(response) {
        $('#ui-view').html($('#ui-view-msg-error').html());
    }

    function save(){
        var obj = new FormData(document.querySelector('#form'));
        if(selected_id != '') {
            obj.append('id', selected_id);
        }
        ajaxPOST(path + '/save',obj,'onActionSuccess','onModalActionErr');
    }


    function onSaveSuccess(response){
        console.log(response);
        $('#msg-info-body').html('Data pengguna telah berhasil di update.');
        $('#msg-info').modal('show');
        // setTimeout(function(){ window.location.href="pengguna?vmenu=daftar"; }, 1000);
    }
    
    function onModalActionErr(response){
        console.log(response);
        $('#modal-form-msg').text(response.responseJSON.message);
        $('#modal-form-msg').show();
    }
    function onActionSuccess(response){
        displaySide(1);
        $('#modal-form').modal('hide');
        $('#msg-confirm').modal('hide');
        selected_action = '';
        console.log(response);
        // swal("Message",response.message,"success");
        Swal.fire({
            timer: 2000,
            type: 'success',
            title: 'Berhasil!',
            text: response.message,
        });
    }


    function doAction(id, action, confirm = 0){
        selected_action = action;
        selected_id=id;
        $('#modal-form-msg').hide();
        $('#msg-confirm-alert').hide();
        if(confirm == 0) ajaxGET(path + '/get_by_id/'+id+'?action='+selected_action,'onPrepareActionSuccess','onError');
        else ajaxPOST(path + '/do_action/'+id+'/'+selected_action,{},'onActionSuccess','onError');
    }

    function onPrepareActionSuccess(response) {
        console.log(response);
        var value = response.data;
        // selected_id = value.id_jenis_iuran;
        if(selected_action == 'edit'){
            fillFormValue(value);
            $('#modal-form').modal('show');
        }else{
            Swal.fire({
              title: 'Are you sure?',
              text: response.message,
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                doAction(selected_id,'delete', 1);
              }
            })
        }
    }

    function fillFormValue(value){
        $('#form')[0].reset();
        $('[name=tingkat]').val(value.tingkat_kelas).selectric('refresh');
        $('[name=sub_tingkat]').val(value.sub_tingkat_kelas);
        $('[name=jurusan]').val(value.jurusan_kelas);
        $('[name=wali_kelas]').val(value.wali_kelas);
    }
    function clearForm(){
        $('#form')[0].reset(); // reset form on modals
        $('#modal-form-msg').hide();
    }
	function add() {
        selected_id = ''; // wajib
        selected_action = 'add'; // wajib
	    $('#modal-form').modal('show'); // show bootstrap modal
	    $('.modal-title').text('Tambah Data'); // Set Title to Bootstrap modal title
        $('[name=tingkat]').val("").selectric('refresh');
        clearForm();
	}