	var path = ctx + 'LaporanTahunanController';
    var path_jenis_iuran = ctx + 'JenisIuranController'; 
    var path_kelas = ctx + 'KelasController'; 
    var params='';
    var selected_action='';
    var selected_id='';
    var list_iuran = [];
    var jumlah_siswa=0;

    function init() {
        //tambahkan validasi, jika form halaman  bukan daftar maka jangan runing ini
        //display(1);
        $('#search-form').submit(function(e){
            if($('[name=filter_tahun]').val() != "" && $('[name=filter_kelas]').val() != ""){
                display();
            }else{
                var resp_msg={"title" : "Message", "body" : "<strong>Tahun Ajaran</strong> dan Kelas wajib diisi!", "icon"  : "warning"};
                showAlertMessage(resp_msg, 1800);
            }
            e.preventDefault();
        });

        $('#btn-export').click(function(){
            var params="";
            params=$('#search-form').serialize();

            window.location.href=ctx+"LaporanExcel/tahunan?"+params;
        });
        // getSelectKelas();
        getSelectTahun();

    
        $(document).on('click', 'table tbody tr', function() {
           // $("table tbody tr").removeClass("bg-light");
           console.log(this);
           console.log(this.className);
           if(this.className=="data-row bg-light"){
               $(this).removeClass("bg-light").addClass("bg-info");
           }else if(this.className=="data-row bg-info"){
               $(this).removeClass("bg-info").addClass("bg-light");
           }
           // else if(this.className=="data-row"){
           //     $(this).toggleClass("bg-info");
           // }
        });

        $('#bg-head').hide();
    };

    // function getSelectKelas(){
    //     $('[name="filter_kelas"]').empty();
    //     $('[name="filter_kelas"]').append(new Option('Pilih Kelas', ''));
    //     ajaxGET(path_kelas + '/list_table?page='+1+'&'+"",'onGetSelectKelas','onGetListError');
    // }
    // function onGetSelectKelas(response){
    //     $.each(response.data, function(key, value) {
    //         $('[name="filter_kelas"]').append(new Option(value.tingkat_kelas+" "+value.jurusan_kelas+" "+value.sub_tingkat_kelas, value.id_kelas));
    //     });
    //      // $('[name="kelas[]"]').selectric('refresh');
    // }

    function getSelectTahun() {
        var d = new Date();
        var n = d.getFullYear();
        var tahun = n - 5;
        for (var i = n; i>= tahun; i--) {
            $("[name=filter_tahun]").append(new Option(i+"/"+(i+1)));
        }
    }

    function display(page = 1){
        $('#more-button-row').remove();
        var tbody = $("#tbl-data").find('tbody');
        // if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
            tbody.text('');
        // }/
        
        tbody.append('<tr id="loading-row"><td colspan="64"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        //
        var resp_msg={"title" : "Message", "body" : '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>', "icon"  : "warning"};
        Swal.fire({
          title: 'Sedang proses',
          text: 'Mohon tunggu, data sedang di proses',
          animation: true,
          customClass: 'animated tada',
          allowOutsideClick: false,
          onBeforeOpen: () => {
            Swal.showLoading()
          },
        });
        //
        params = $( "#search-form" ).serialize();
        ajaxGET(path + '/list_table?page='+page+'&'+params,'onGetListSuccess','onGetListError');
    }

    function onGetListSuccess(response){
        Swal.close();
        console.log(response);
        $('#loading-row').remove();
        $('#no-data-row').remove();
        
        var tbody = $("#tbl-data").find('tbody');
        var tfoot = $("#tbl-data").find('tfoot');
        
        var row = "";
        var num = $('.data-row').length+1;
        var temp_data=[];
        var jumlah_iuran=0;
        var jumlah_total=0;
        //
        var row_data=["NO","REF","NAMA SISWA","JK","KELAS","WALI KELAS","BESAR<br/>IURAN"];
        var thead_sub = $('#thead-sub-tunggakan');
        var row_head = '';
        var row_foot = '';
        $.each(row_data, function(key){
            var width="50";
            var dnone="";
            if(this=="NAMA SISWA") width="250";
            if(this=="KELAS" || this=="WALI KELAS") width="100";
            if(this=="WALI KELAS") dnone="d-none";
            row_head+='<th class="text-center align-middle '+dnone+'" rowspan="3" width="'+width+'">'+this+'</th>';
        });
        row_foot+='<th class="text-center align-middle" colspan='+(row_data.length-1)+'>JUMLAH</th>';

        row_head+='<th class="text-center" colspan="12">BULAN KE</th>';
        row_head+='<th class="text-center align-middle" rowspan="3" width="100">JUMLAH <br>BAYAR <br>IURAN BULANAN</th>';
        row_head+='<th class="text-center align-middle" rowspan="3" width="100">SISA <br>IURAN <br>BULANAN</th>';
        //tabungan
        row_head+='<th class="text-center bg-purple" colspan="12">TABUNGAN GTC BULANAN</th>';
        row_head+='<th class="text-center bg-purple align-middle" rowspan="3" width="100">JUMLAH <br>TABUNGAN</th>';
        
        //
        row_head+='<th class="text-center bg-danger" colspan='+(response.data.header.length*3)+'>DATA TUNGGAKAN KEUANGAN TAHUN LALU</th>';
        
        row_data=["JUMLAH <br>TUNGGAKAN <br>DAFTAR ULANG","TOTAL","KETERANGAN","DPMP"];
        if($('[name=filter_kelas]').val()=="X"){
            row_data[3]="DPMP";
        }else{
            row_data[3]="DPMP<br/>SEBELUMNYA";
        }
       
        $.each(row_data, function(key){
            width="100";
            if(this=="KETERANGAN") width="125";
            row_head+='<th class="text-center align-middle bg-danger" rowspan="3" width="'+width+'">'+this+'</th>';
        });
        //
        row_head+='<th class="text-center" colspan="24">BULAN</th>';
        row_data=["JUMLAH <br>BAYAR <br>DPMP","SISA DPMP","KETERANGAN","JUMLAH BIAYA <br/>IURAN 1 TH"];
        if($('[name=filter_kelas]').val()=="XII"){
             row_data=["JUMLAH <br>BAYAR <br>DPMP","SISA DPMP","KETERANGAN","JUMLAH BIAYA <br/>IURAN 1 TH","TUNGGAKAN <br/>IURAN KELAS XI","KELAS X"];
        }
        if($('[name=filter_kelas]').val()=="XI"){
             row_data=["JUMLAH <br>BAYAR <br>DPMP","SISA DPMP","KETERANGAN","JUMLAH BIAYA <br/>IURAN 1 TH","TUNGGAKAN <br/>IURAN KELAS X"];
        }
        
        $.each(row_data, function(key){
            width="100";
            if(this=="KETERANGAN") width="125";
            row_head+='<th class="text-center align-middle" rowspan="3" width="'+width+'">'+this+'</th>';
        });
        // $.each(response.data.header,function(key,value){
        //     row_head+='<th class="text-center bg-danger" colspan="3">DU '+this.tahun+' ('+this.kelas+')</th>';
        //     $.each(value.month_year,function(key){
        //         if(value.kelas==$('[name=filter_kelas]').val()){
        //             row_head+='<th class="text-center" colspan="2">'+this+'</th>';
        //         }
        //     });
        // });
        thead_sub.html(row_head);
        //$('#thead-tunggakan-1').attr('colspan', response.data.header.length*3);
        //
        var thead_sub = $('#thead-sub-tunggakan-1');
        row_head = '';
        for(var i=1; i <= 12; i++){
            row_head+='<th class="text-center">'+i+'</th>';
        }
        for(var i=1; i <= 12; i++){
            row_head+='<th class="text-center bg-purple">'+i+'</th>';
        }
        $.each(response.data.header,function(key,value){
            row_head+='<th class="text-center bg-danger" colspan="3">DU '+this.tahun+' ('+this.kelas+')</th>';
            $.each(value.month_year,function(key){
                if(value.kelas==$('[name=filter_kelas]').val()){
                    row_head+='<th class="text-center" colspan="2">'+this+'</th>';
                }
            });
        });
        thead_sub.html(row_head);

        thead_sub = $('#thead-sub-tunggakan-2');
        row_head='';
        var monthNameList_random = ["Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "Jan", "Feb", "Mar", "Apr", "May", "Jun"];
        $.each(monthNameList_random, function(key){
            var width="50";
            row_head+='<th class="text-center" width="'+width+'">'+this+'</th>';
        });
        $.each(monthNameList_random, function(key){
            var width="50";
            row_head+='<th class="text-center bg-purple" width="'+width+'">'+this+'</th>';
        });
        $.each(response.data.header, function(key){
            row_head+='<th class="text-center bg-danger" width="100">OSIS</th>';
            row_head+='<th class="text-center bg-danger" width="100">Komputer</th>';
            row_head+='<th class="text-center bg-danger" width="100">SPP</th>';
        });
        
        $.each(response.data.header, function(key,value){
            $.each(this.month_year,function(key){
                if(value.kelas==$('[name=filter_kelas]').val()){
                    row_head+='<th class="text-center" width="100">TGL</th>';
                    row_head+='<th class="text-center" width="100">BAYAR</th>';
                }
            });
        });
        thead_sub.html(row_head);
        var sum_total_per_bulan=[];
        var sum_total_jumlah_iuran_bulanan=0;
        var sum_total_sisa_iuran_bulanan=0;
        var sum_total=[];

        var sum_total_per_du=[];
        var sum_total_per_dpmp=[];
        var idx=0;
        var new_key=0;
        $.each(response.data.main,function(key,value){
            // if(value.total_bulan != ""){
                var tingkat_kelas = (value.kelas).split(" ");
                tingkat_kelas=tingkat_kelas[0];
                // alert(tingkat_kelas);
                
                if(value.tahun_ajaran_siswa_kelas==$('[name=filter_tahun]').val() && tingkat_kelas==$('[name=filter_kelas]').val() ){

                    // row += render_row(value, num);
                    // temp_data.push(this);
                    
                    row += '<tr contenteditable="false" class="data-row bg-light" id="row-'+value.id_siswa+'">';
                    row += '<td>'+(num)+'</td>';
                    row += '<td class="">'+value.ref_siswa+'</td>';
                    row += '<td class="">'+value.nama_lengkap_siswa+'</td>';
                    row += '<td class="">'+(value.jenis_kelamin_siswa).split("")[0]+'</td>';
                    row += '<td class="">'+value.kelas+'</td>';

                    var tgl_regis = value.tanggal_registrasi_siswa_kelas.split("-")[1]-1;
                    console.log(tgl_regis);
                    var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                    var monthNameList_random = ["Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "Jan", "Feb", "Mar", "Apr", "May", "Jun"];
                    var dump=[];
                    row += '<td class="d-none">'+value.wali_kelas+'</td>';
                    row += '<td class="text-right">'+$.number(value.besar_spp)+'</td>';
                    var bulan_iuran_bulanan=[];
                    var besar_iuran_bulanan=[];
                    var bulan_iuran_du=[];
                    var besar_iuran_du=[];
                    
                    var temp_bulan=[];
                    var temp_bulan_i=0;
                    var index_bulan=[];

                    $.each(monthNameList_random,function(key, month){
                        bulan_iuran_bulanan.push(month);
                        besar_iuran_bulanan.push(value.new_list_bulan[month]);
                        
                        var new_val=value.new_list_bulan[month];
                        console.log(new_val==undefined);
                        if(new_val==0 || new_val==undefined){
                            row += '<td class="text-center">'+"-"+'</td>';
                            temp_bulan.push(parseInt(0));
                        }
                        if(new_val>0){
                            row += '<td class="bg-success text-white">'+((''+$.number(new_val).split(",")[0]+''))+'</td>';
                            temp_bulan.push(parseInt($.number(new_val).split(",")[0]));
                            temp_bulan_i+=parseInt(new_val);
                        }
                    });

                    sum_total_per_bulan.push(temp_bulan);

                    row += '<td class="text-right">'+(temp_bulan_i)+'</td>';
                    // row += '<td class="text-right">'+(temp_bulan_i)+'-'+temp_bulan+'xx'+$.number(value.spp)+'</td>';
                    row += '<td class="text-right">'+$.number((value.besar_spp*12)-value.spp)+'</td>';
                    sum_total_jumlah_iuran_bulanan+=parseInt(value.spp);
                    sum_total_sisa_iuran_bulanan+=((value.besar_spp*12)-value.spp);
                    //set tabungan
                    var temp_tabungan=[];
                    var temp_tabungan_i=0;
                    var index_tabungan=[];
                     $.each(monthNameList_random,function(key, month){
                        // bulan_iuran_bulanan.push(month);
                        // besar_iuran_bulanan.push(value.new_list_bulan[month]);
                        
                        var new_val=value.new_list_tabungan[month];
                        // console.log(new_val==undefined);
                        if(new_val==0 || new_val==undefined){
                            row += '<td class="text-center">'+"-"+'</td>';
                            // temp_bulan.push(parseInt(0));
                        }
                        if(new_val>0){
                            row += '<td class="bg-success text-white">'+((''+$.number(new_val).split(",")[0]+''))+'</td>';
                            // temp_bulan.push(parseInt($.number(new_val).split(",")[0]));
                            temp_tabungan_i+=parseInt(new_val);
                        }
                    });

                    // sum_total_per_bulan.push(temp_bulan);

                    row += '<td class="text-right">'+($.number(temp_tabungan_i))+'</td>';
                    //
                    //rummus mengambil besaran disini
                    var temp_besar_iuran=[];
                    var temp_test=[];
                    $.each(value.besar_iuran,function(key){
                        if(this.jenis_iuran_siswa_detil=="DU") temp_besar_iuran.push(this.besar_iuran_siswa_detil);
                    });
                    //end disini
                    var sum_du=0;
                    var sum_all_du=0;
                    var temp_du=[];
                    $.each(response.data.header,function(key,value_1){
                            console.log(value_1);
                        sum_du=0;
                         $.each(value.list_du,function(key, value_2){
                            console.log(value_2);
                            if(value_1.tahun==value_2.tahun_ajaran_siswa_kelas){
                                sum_du+=parseInt(value_2.besar_iuran);
                            }
                        });
                        sum_all_du+=parseInt(temp_besar_iuran[key])-sum_du;
                        temp_test.push(temp_besar_iuran[key])
                        row += '<td class="bg-success text-white text-right">'+$.number(temp_besar_iuran[key]-sum_du)+'</td>';
                        row += '<td class="bg-success text-white text-right">'+"-"+'</td>';
                        row += '<td class="bg-success text-white text-right">'+"-"+'</td>';
                        temp_du.push(temp_besar_iuran[key]-sum_du);
                        temp_du.push(0);
                        temp_du.push(0);
                    });

                    row += '<td class="bg-success text-white text-right">'+$.number(sum_all_du)+'</td>';
                    //END DU
                    row += '<td class="text-right">'+$.number((value.besar_spp*12)-value.spp+sum_all_du)+'</td>';

                    row += '<td class="">'+value.keterangan_masuk_siswa+'</td>';
                    temp_du.push(sum_all_du);
                    temp_du.push((value.besar_spp*12)-value.spp+sum_all_du);
                    temp_du.push("-");

                    var count_sisa_dpmp_x=0;
                    var count_sisa_dpmp_xi=0;
                    $.each(value.list_dpmp,function(key, value){
                        if($('[name=filter_kelas]').val()=="XI"){
                            if(value.tingkat_kelas=="X"){
                                count_sisa_dpmp_x+=parseInt(value.besar_iuran);
                            }
                        }
                        if($('[name=filter_kelas]').val()=="XII"){
                            if(value.tingkat_kelas=="X"){
                                count_sisa_dpmp_x+=parseInt(value.besar_iuran);
                            }
                            if(value.tingkat_kelas=="XI"){
                                count_sisa_dpmp_xi+=parseInt(value.besar_iuran);
                            }
                        }
                    });
                    
                    if(tingkat_kelas=="X"){
                        row += '<td class="text-right">'+$.number((value.besar_dpmp))+'</td>';
                        temp_du.push(value.besar_dpmp);
                    }else{
                        row += '<td class="text-right">'+$.number(parseInt(value.besar_dpmp)-(parseInt(count_sisa_dpmp_x)+parseInt(count_sisa_dpmp_xi)))+'</td>';
                        temp_du.push(parseInt(value.besar_dpmp)-(parseInt(count_sisa_dpmp_x)+parseInt(count_sisa_dpmp_xi)));
                    }
                    sum_total_per_du.push(temp_du);

                    var dump_dpmp=[];var damps_month="";var damps_besar_iuran=0;var damps_tanggal_iuran="";
                    $.each(monthNameList_random,function(key, month){
                        $.each(value.list_dpmp,function(key, value_dpmp){
                            console.log(month);
                            var date = new Date(value_dpmp.tanggal_iuran);
                            if(month==monthNameList[date.getMonth()] && value_dpmp.id_siswa_kelas==value.id_siswa_kelas){
                                damps_month=monthNameList[date.getMonth()];
                                var temp_date=value_dpmp.tanggal_iuran;
                                temp_date=temp_date.split(" ");
                                //dump.push(temp_date[0]);
                                damps_tanggal_iuran=temp_date[0];
                                damps_besar_iuran+=parseInt(value_dpmp.besar_iuran);
                            }
                        });

                        //console.log(damps_month+"=="+month);
                        if(damps_month==month){
                            row += '<td class="">'+damps_tanggal_iuran+'</td>';
                            row += '<td class="text-right">'+$.number(damps_besar_iuran)+'</td>';
                            dump_dpmp.push(damps_tanggal_iuran);
                            dump_dpmp.push(damps_besar_iuran);
                        }else{
                            row += '<td class="">'+"--"+'</td>';
                            row += '<td class="text-right">'+"-"+'</td>';
                            dump_dpmp.push("-");
                            dump_dpmp.push(0);
                        }
                    });

                    var sisa_dpmp=(value.besar_dpmp)-(parseInt(count_sisa_dpmp_x)+parseInt(count_sisa_dpmp_xi))-value.dpmp;
                    row += '<td class="text-right">'+$.number(value.dpmp)+'</td>';
                    dump_dpmp.push(parseInt(value.dpmp));
                    row += '<td class="text-right">'+$.number(sisa_dpmp)+'</td>';
                    dump_dpmp.push(sisa_dpmp);
                    row += '<td class="text-center">'+(sisa_dpmp==0?"DPMP LUNAS":"DPMP MENUNGGAK")+'</td>';
                    dump_dpmp.push(sisa_dpmp==0?"DPMP LUNAS":"DPMP MENUNGGAK");
                    row += '<td class="">'+""+'</td>';
                    dump_dpmp.push("");
                    
                    sum_total_per_dpmp.push(dump_dpmp);

                    var besar_spp_x=0;
                    var besar_spp_xi=0;
                    $.each(value.besar_iuran,function(key){
                        if(this.jenis_iuran_siswa_detil=="IURAN_PER_BULAN") {
                            if($('[name=filter_kelas]').val()=="XI"){
                                if(this.tingkat_siswa_detil=="X"){
                                    besar_spp_x=this.besar_iuran_siswa_detil;
                                }
                            }
                            if($('[name=filter_kelas]').val()=="XII"){
                                if(this.tingkat_siswa_detil=="X"){
                                    besar_spp_x=this.besar_iuran_siswa_detil;
                                }
                                if(this.tingkat_siswa_detil=="XI"){
                                    besar_spp_xi=this.besar_iuran_siswa_detil;
                                }
                            }
                        }
                    });

                    var count_sisa_spp_x=0;
                    var count_sisa_spp_xi=0;
                    $.each(value.list_bulan,function(key, value){
                        if($('[name=filter_kelas]').val()=="XI"){
                            if(value.tingkat_kelas=="X"){
                                count_sisa_spp_x+=parseInt(value.besar_iuran);
                            }
                        }
                        if($('[name=filter_kelas]').val()=="XII"){
                            if(value.tingkat_kelas=="X"){
                                count_sisa_spp_x+=parseInt(value.besar_iuran);
                            }
                            if(value.tingkat_kelas=="XI"){
                                count_sisa_spp_xi+=parseInt(value.besar_iuran);
                            }
                        }
                    });
                    
                    var temp_besar_tunggakan=[];
                    if(tingkat_kelas=="XI"){
                        row += '<td class="text-right">'+$.number(parseInt(besar_spp_x*12)-count_sisa_spp_x)+'</td>';
                        temp_besar_tunggakan.push(parseInt(besar_spp_x*12)-count_sisa_spp_x);
                    }
                    if(tingkat_kelas=="XII"){
                        row += '<td class="text-right">'+$.number(parseInt(besar_spp_xi*12)-count_sisa_spp_xi)+'</td>';
                        temp_besar_tunggakan.push(parseInt(besar_spp_xi*12)-count_sisa_spp_xi);
                        row += '<td class="text-right">'+$.number(parseInt(besar_spp_x*12)-count_sisa_spp_x)+'</td>';
                        temp_besar_tunggakan.push(parseInt(besar_spp_x*12)-count_sisa_spp_x);
                    }
                        if($('[name=filter_kelas]').val()=="XI"){
                            sum_total_per_dpmp[idx].push(temp_besar_tunggakan[0]);
                        }
                        if($('[name=filter_kelas]').val()=="XII"){
                            sum_total_per_dpmp[idx].push(temp_besar_tunggakan[0]);
                            sum_total_per_dpmp[idx].push(temp_besar_tunggakan[1]);
                        }
                    row += '</tr>';

                    num++;
                    idx++;
                }
        });
        var count=0;
        for(var i=0; i < 12; i++){
            count=0;
            $.each(sum_total_per_bulan, function(key){
                count+=parseInt(this[i]);
            });
            row_foot+='<th class="text-center">'+($.number(count))+'</th>';
        }
        row_foot+='<th class="text-right">'+($.number(sum_total_jumlah_iuran_bulanan))+'</th>';
        row_foot+='<th class="text-right">'+($.number(sum_total_sisa_iuran_bulanan))+'</th>';

        count=0;
        for(var i=0; i < (response.data.header.length*3)+4; i++){
            count=0;
            $.each(sum_total_per_du, function(key){
                count+=parseInt(this[i]);
            });
            if(i!=((response.data.header.length*3)+2)){
                row_foot+='<th class="text-right">'+$.number(count)+'</th>';
            }else{
                row_foot+='<th class="text-left">-</th>';
            }
        }
        console.log(sum_total_per_dpmp);
        var dynam=0;
        count=0;
        if($('[name=filter_kelas]').val()=="XI") dynam=1;
        if($('[name=filter_kelas]').val()=="XII") dynam=2;
        for(var i=0; i < (12*2)+4+dynam; i++){
            count=0;
            $.each(sum_total_per_dpmp, function(key){
                if(typeof this[i] == "number"){
                    count+=parseInt(this[i]);
                }else{
                    count="";
                }
            });
            if(typeof count == "number") count=$.number(count);
            row_foot+='<th class="text-right">'+(count)+'</th>';
        }

        tfoot.html(row_foot);

        row == "" ? tbody.html('<tr id="no-data-row" class="nodata bg-light"><td colspan="64"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
    }

    function render_row(value, num){
     console.log(value);

        var row = "";
        row += '<tr contenteditable="false" class="data-row " id="row-'+value.id_siswa+'">';
        row += '<td>'+(num)+'</td>';
        row += '<td class="">'+value.ref_siswa+'</td>';
        row += '<td class="">'+value.nama_lengkap_siswa+'</td>';
        row += '<td class="">'+(value.jenis_kelamin_siswa).split("")[0]+'</td>';
        // row += '<td class="">'; 
        //     row += '<b style="font-family: segoeuib;">Nama</b>: '+value.nama_lengkap_siswa+'<br/>';
        //     row += '<b style="font-family: segoeuib;">Kelas</b>: '+value.kelas;
        // row += '</td>';
        row += '<td class="">'+value.kelas+'</td>';
        
        var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var monthNameList_random = ["Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "Jan", "Feb", "Mar", "Apr", "May", "Jun"];
        var dump=[];
        // $.each(value.total_bulan,function(key){
        //     var str = this.bulan_iuran;
        //     str=str.split(" ");
        //     dump.push(monthNameList.indexOf(str[0])+1);
        // });
        row += '<td class="">'+value.wali_kelas+'</td>';
        row += '<td class="text-right">'+$.number(value.besar_spp)+'</td>';
        // row += '<td colspan="3">';
        //     row += '<b style="font-family: segoeuib;">SPP</b>: '+$.number(value.spp)+'<br/>';
        //     row += '<b style="font-family: segoeuib;">DPMP</b>: '+$.number(value.dpmp)+'<br/>';
        //     row += '<b style="font-family: segoeuib;">DU</b>: '+$.number(value.du);
        // row += '</td>';
        // row += '<td class="">'+$.number(parseInt(value.spp)+parseInt(value.dpmp)+parseInt(value.du))+'</td>';
        // var dump=[];
        // $.each(value.list_bulan,function(key){
        //     var str = this.bulan_iuran;
        //     str=str.split(" ");
        //     row += '<td class="">'+(monthNameList[monthNameList.indexOf(str[0])])+'</td>';
        // });
        // $.each(value.list_bulan,function(key, value_2){
        //     var str = value_2.bulan_iuran;
        //     str=str.split(" ");
        //     console.log(str);
        //     if(value.id_siswa_kelas==value_2.id_siswa_kelas){
        //         row += '<td class="bg-successs text-whiste"><i class="fa fa-check"></i></td>';
        //     }
        //     // if(month==monthNameList[monthNameList.indexOf(str[0])]){
        //     //     dump.push(monthNameList[monthNameList.indexOf(str[0])]);
        //     // }
        // });
        var bulan_iuran_bulanan=[];
        var besar_iuran_bulanan=[];
        var bulan_iuran_du=[];
        var besar_iuran_du=[];
        var sum_du=0;
        $.each(monthNameList_random,function(key, month){
            //set BULANAN
            $.each(value.list_bulan,function(key, value_2){
                var str = value_2.bulan_iuran;
                str=str.split(" ");
                if(month==monthNameList[monthNameList.indexOf(str[0])] && value.id_siswa_kelas==value_2.id_siswa_kelas){
                    bulan_iuran_bulanan.push(monthNameList[monthNameList.indexOf(str[0])]);
                    besar_iuran_bulanan.push(value_2.besar_iuran);
                }
            });
            console.log(month);
            if(bulan_iuran_bulanan[key]==month){
                row += '<td class="bg-success text-white">'+besar_iuran_bulanan[key].substring(0,3)+'</td>';
            }else{
                row += '<td class="">'+"-"+'</td>';
            }
            //set DU
           //sudah dipindah 
        });

        row += '<td class="text-right">'+$.number(value.spp)+'</td>';
        row += '<td class="text-right">'+$.number((value.besar_spp*12)-value.spp)+'</td>';
        //START DU
        // row += '<td class="">'+"-"+'</td>';
        // row += '<td class="">'+"-"+'</td>';
        // row += '<td class="">'+"-"+'</td>';
        // row += '<td class="">'+"-"+'</td>';
        // row += '<td class="">'+"-"+'</td>';
        // row += '<td class="">'+"-"+'</td>';
        // row += '<td class="">'+"-"+'</td>';
        // row += '<td class="">'+"-"+'</td>';
        // row += '<td class="">'+"-"+'</td>';
        // row += '<td class="">'+"-"+'</td>';
        // row += '<td class="">'+"-"+'</td>';
        // row += '<td class="">'+"-"+'</td>';

        
        var du_temp=[];
        $.each(value.list_du,function(key, value_2){
            console.log("end", value_2);
        // var str = value_2.bulan_iuran;
        // str=str.split(" ");
        // if(month==monthNameList[monthNameList.indexOf(str[0])] && value.id_siswa_kelas==value_2.id_siswa_kelas){
            // bulan_iuran_du.push(monthNameList[monthNameList.indexOf(str[0])]);
            //if(value_2.id_siswa_kelas_iuran==value.id_siswa_kelas){
            // if(value_2.tahun_ajaran == value.tahun_ajaran){
                // besar_iuran_du.push(value_2.besar_iuran);
                sum_du+=parseInt(value_2.besar_iuran);
                du_temp.push(sum_du+=parseInt(value_2.besar_iuran));
                // row += '<td class="bg-success text-white">'+$.number(value_2.besar_iuran)+'</td>';
            // }
        // }
        });
        //percobaan ke-2
        // $.each(response.data.header,function(key,value){
        //     row_head+='<th class="text-center bg-danger" colspan="3">DU '+this.tahun+' ('+this.kelas+')</th>';
        //     $.each(value.month_year,function(key){
        //         if(value.kelas==$('[name=filter_kelas]').val()){
        //             row_head+='<th class="text-center" colspan="2">'+this+'</th>';
        //         }
        //     });
        // });
        // thead_sub.html(row_head);
        //end here
        // alert(sum_du);
        // console.log("final", besar_iuran_du);
        if(value.list_du.length > 0){
            row += '<td class="bg-success text-white">'+$.number(sum_du)+'</td>';
        }else{
            row += '<td class="bg-success text-white">'+"-"+'</td>';
        }
        row += '<td class="bg-success text-white">'+"-"+'</td>';
        row += '<td class="bg-success text-white">'+"-"+'</td>';

        console.log("WHAT", du_temp);
        // row += '<td class="">'+"- TOTAL DU"+'</td>';

        //END DU
        row += '<td class="text-right">'+$.number(value.besar_dpmp)+'</td>';

        var dump=[];var damps="";var damps_besar_iuran=0;var damps_tanggal_iuran="";
        $.each(monthNameList_random,function(key, month){
            $.each(value.list_dpmp,function(key, value){
                var date = new Date(value.tanggal_iuran);
                // console.log(monthNameList[date.getMonth()]);
                if(month==monthNameList[date.getMonth()]){
                    // dump.push(monthNameList[date.getMonth()]);
                    damps=monthNameList[date.getMonth()];
                    var temp_date=value.tanggal_iuran;
                    temp_date= temp_date.split(" ");
                    dump.push(temp_date[0]);
                    damps_tanggal_iuran=temp_date[0];
                    damps_besar_iuran=(value.besar_iuran);
                }
            });
            console.log(dump);
            if(damps==month){
                // row += '<td class="bg-success text-white">'+month+'</td>';
                row += '<td class="">'+damps_tanggal_iuran+'</td>';
                row += '<td class="bg-successs text-whiste">'+$.number(damps_besar_iuran)+'</td>';
            }else{
                row += '<td class="">'+"-"+'</td>';
                row += '<td class="">'+"-"+'</td>';
            }
        });

        row += '<td class="text-right">'+$.number(value.dpmp)+'</td>';
        row += '<td class="text-right">'+$.number((value.besar_dpmp)-value.dpmp)+'</td>';
        row += '<td class="text-center">'+(0==(value.besar_dpmp)-value.dpmp?"DPS LUNAS":"DPS MENUNGGAK")+'</td>';
        row += '<td class="">'+""+'</td>';     
        row += '</tr>';
        return row;
    }


    
    function onGetListError(response) {
        console.log(response);
        let timerInterval
        Swal.fire({
          title: 'Something Wrong!',
          icon: 'warning',
          html: 'I will close in <strong></strong> seconds.',
          timer: 1000,
          onBeforeOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
              Swal.getContent().querySelector('strong')
                .textContent = Swal.getTimerLeft()
            }, 100)
          },
          onClose: () => {
            clearInterval(timerInterval)
          }
        }).then((result) => {
          if (
            // Read more about handling dismissals
            result.dismiss === Swal.DismissReason.timer
          ) {
            console.log('I was closed by the timer')
          }
        })
        $('#ui-view').html($('#ui-view-msg-error').html());
    }

    function doAction(id, action, confirm = 0){
        selected_action = action;
        $('#modal-form-msg').hide();
        $('#msg-confirm-alert').hide();
        if(confirm == 0) ajaxGET(path + '/get_by_id/'+id+'?action='+selected_action,'onPrepareActionSuccess','onError');
        else ajaxPOST(path + '/do_action/'+id+'/'+selected_action,{},'onActionSuccess','onError');
    }

    function onPrepareActionSuccess(response) {
        console.log(response);
        var value = response.data;
        selected_id = value.id_siswa;
        if(selected_action == 'edit'){
            fillFormValue(value);
            $('#modal-form').modal('show');
        }else{

            Swal.fire({
              title: 'Are you sure?',
              text: response.message,
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                doAction(selected_id,'delete', 1);
                Swal.fire(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                display(1);
              }
            })
        }
    }
