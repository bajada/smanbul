	var path = ctx + 'KwitansiIuranController';
    var params='';
    var selected_action='';
    var selected_id='';

    function init() {
        display();

        $('#search-form').submit(function(e){
            display();
            e.preventDefault();
        });
        
        $('#btn-yes-msg-confirm').click(function(e){
            if(selected_action == 'delete' ){
                doAction(selected_id, selected_action, 1);
            }
            
        });

        // $(document).on('click', '#tbl-data-kelas tr', function() {
        //    $("#tbl-data-kelas tr").removeClass("bg-info");
        //    $(this).toggleClass("bg-info");
        // });
    };


    function display(page = 1){
        $('#more-button-row').remove();
        var tbody = $("#tbl-data").find('tbody');
        // if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
            tbody.text('');
        // }
        
        tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        
        params = $( "#search-form" ).serialize();
        ajaxGET(path + '/list_table?page='+page+'&'+params,'onGetListSuccess','onGetListError');
    }

    function onGetListSuccess(response){
        console.log(response);
        $('#loading-row').remove();
        $('#no-data-row').remove();
        
        var tbody = $("#tbl-data").find('tbody');
        
        var row = "";
        var num = $('.data-row').length+1;
        $.each(response.data,function(key,value){
            // if($('#tbl-data-kelas').find('.bg-info').data('id') == value.tingkat_jenis_iuran || value.tingkat_jenis_iuran=="ALL"){
            row += render_row(value, num);
            num++;
            // }

        });
        // if(response.next_more){
        //     row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
        //     row += '    <button class="btn waves-effect green sh" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
        //     row += '</div></td></tr>';
        // }
        row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
        
    }
    function render_row(value, num){
     console.log(value);
        var row = "";
        row += '<tr class="data-row" id="row-'+value.id_jenis_iuran+'">';
        row += '<td>'+(num)+'</td>';
        row += '<td class=""><a target="_blank" class="text-decoration-none text-warning" href="/kp_smanbull/IuranSiswaController/print_invoice/'+value.kode_kwitansi_iuran+'">'+value.kode_kwitansi_iuran+'<a/></td>';
        row += '<td class="">'+value.ref_siswa+'</td>';
        row += '<td class="">'+value.tanggal_kwitansi_iuran+'</td>';
        row += '<td class="">'+value.nama_lengkap_siswa+'</td>';
        row += '<td class="">'+value.kelas+'</td>';
        row += '<td class="">';
            row += '<a href="/kp_smanbull/IuranSiswaController/print_invoice/'+value.kode_kwitansi_iuran+'" target="_blank" class="btn btn-sm btn-square pill btn-info" ><i class="fas fa-print"></i></a> ';
        row += '</td>';
        row += '</tr>';
        return row;
    }
    function onGetListError(response) {
        $('#ui-view').html($('#ui-view-msg-error').html());
    }

    function save(){
        var obj = new FormData(document.querySelector('#form'));
        if(selected_id != '') {
            obj.append('id', selected_id);
        }
        ajaxPOST(path + '/save',obj,'onActionSuccess','onSaveError');
    }


    function onSaveSuccess(response){
        console.log(response);
        $('#msg-info-body').html('Data pengguna telah berhasil di update.');
        $('#msg-info').modal('show');
        // setTimeout(function(){ window.location.href="pengguna?vmenu=daftar"; }, 1000);
    }
    function onActionSuccess(response){
        display();
        $('#modal-form').modal('hide');
        $('#msg-confirm').modal('hide');
        selected_action = '';
        console.log(response);
        swal("Message",response.message,"success");
    }


    function doAction(id, action, confirm = 0){
        selected_action = action;
        $('#modal-form-msg').hide();
        $('#msg-confirm-alert').hide();
        if(confirm == 0) ajaxGET(path + '/get_by_id/'+id+'?action='+selected_action,'onPrepareActionSuccess','onError');
        else ajaxPOST(path + '/do_action/'+id+'/'+selected_action,{},'onActionSuccess','onError');
    }

    function onPrepareActionSuccess(response) {
        console.log(response);
        var value = response.data;
        selected_id = value.id_jenis_iuran;
        if(selected_action == 'edit'){
            fillFormValue(value);
            $('#modal-form').modal('show');
        }else{
            Swal.fire({
              type: 'warning',
              title: 'Are you sure?',
              text: response.message,
              //footer: '<a href>Why do I have this issue?</a>'
              showCancelButton: true,
              showConfirmButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              // confirmButtonText: 'Tutup!'
            })
            .then((value) => {
                if(value.value){
                    doAction(selected_id,'delete', 1);
                }
            });
            // swal({
            //   title: "Are you sure?",
            //   text: response.message,
            //   icon: "warning",
            //   buttons: true,
            //   dangerMode: true,
            // })
            // .then((willDelete) => {
            //     console.log(willDelete);
            //     if(willDelete)
            //     doAction(selected_id,'delete', 1);
            // });
        }
    }

    function fillFormValue(value){
        $('#form')[0].reset();
        
        $('[name=nama]').val(value.nama_jenis_iuran);
        $('[name=cicilan]').val(value.cicilan_jenis_iuran);
        $('[name=besar]').val(value.besar_jenis_iuran);
    }

	function add() {
        selected_id = ''; // wajib
        selected_action = 'add'; // wajib
	    $('#form')[0].reset(); // reset form on modals
	    $('.form-group').removeClass('has-error'); // clear error class
	    $('.help-block').empty(); // clear error string
	    $('#modal-form').modal('show'); // show bootstrap modal
	    $('.modal-title').text('Tambah Jenis Iuran'); // Set Title to Bootstrap modal title
        $('#form-pass').removeClass("d-none");

	}