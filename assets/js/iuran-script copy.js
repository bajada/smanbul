	var path = ctx + 'IuranSiswaController';
    var params='';
    var selected_action='';
    var selected_id='';
        var lbl_total_biaya = 0;
        var lbl_nominal_dpmp = 0;
        var lbl_nominal_du = 0;
        var lbl_nominal_iuran = 0;

    function init() {
        selected_id = ($.urlParam('filter_keyword') == null ? '' : $.urlParam('filter_keyword'));

        if(selected_id != '') {
            ajaxGET(path + '/'+selected_id,'onGetSuccess','onGetError');
            $('#daftar_permohonan').prop('disabled', false);
        }
        //tambahkan validasi, jika form halaman  bukan daftar maka jangan runing ini
        //display(1);

        $('#search-form').submit(function(e){
            //displaySiswa();
            //e.preventDefault();
        });
        $('[name=nominal_dpmp]').change(function(){
            lbl_nominal_dpmp=parseInt(this.value);
            $('#lbl_biaya_dpmp').html(": "+$.number(this.value,2));
            $('#lbl_total_biaya').text(": "+($.number(lbl_nominal_iuran+lbl_nominal_dpmp+lbl_nominal_du,2)));
        });
        $('[name=nominal_du]').change(function(){
            lbl_nominal_du=parseInt(this.value);
            $('#lbl_biaya_du').html(": "+$.number(this.value,2));
            $('#lbl_total_biaya').text(": "+($.number(lbl_nominal_iuran+lbl_nominal_dpmp+lbl_nominal_du,2)));

        });
        // $('[name=tekan_saja]').change(function(){
        //     alert(1);
        //     lbl_nominal_iuran=parseInt(this.value);
        //     $('#lbl_biaya_iuran_per_bulan').html(": "+$.number(this.value,2));
        //     $('#lbl_total_biaya').text(": "+($.number(lbl_nominal_iuran+lbl_nominal_dpmp+lbl_nominal_du,2)));

        // });
        $('#btn-simpan').click(function(){
            console.log(this);
            var obj ={
                "id_siswa_iuran": 0,
                "bulan_iuran": "dari tbl iuran yang dipilih",
                "jatuh_tempo_iuran": "dari tbl iuran yang dipilih",
                "no_iuran": "convert dari jatuh_tempo_iuran",
                //diambil dari tgl ketika bayar
                //"tanggal_iuran": "",
                "besar_iuran": "dari tbl iuran yang dipilih",
                "jenis_iuran": "dari tbl iuran yang dipilih",
                //belum diketahui,jadi dikosongkan saja
                //"cicilan_iuran": "dari tbl iuran yang dipilih",
                "status": "di tentukan ketika runing rest save"
            }
            var obj_arr = [];
            console.log("============");
            var data = $('#tbl-data').find('tbody tr');
            $.each(data,function(key){
                if($(this).find('td input:checked').length){
                    console.log(this);
                    console.log($(this).find('td'));
                    console.log($(this));
                    console.log($(this)[0].cells);
                    obj ={
                        "id_siswa_iuran": key,
                        "bulan_iuran": $(this)[0].cells[1].textContent,
                        "jatuh_tempo_iuran": $(this)[0].cells[2].textContent,
                        "no_iuran": $(this)[0].cells[2].textContent,
                        "besar_iuran": lbl_nominal_iuran,
                        "jenis_iuran": "IURAN_PER_BULAN",
                        "status": "LUNAS"
                    }
                    obj_arr.push(obj);
                }

            });
            console.log(obj);
        });

    };

    function displaySiswa(){
        //$('#more-button-row').remove();
        //var tbody = $("#tbl-data").find('tbody');
        // if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
        //     tbody.text('');
        // }
        
        //tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        
        params = $( "#search-form" ).serialize();
        ajaxGET(path + '/list_table?page='+1+'&'+params,'onDisplaySiswaSuccess','onGetListError');
    }
    function onDisplaySiswaSuccess(resp){
        console.log(resp);
        console.log(resp.data.data_siswa);

            
        var count_besar_dpmp=0;
        var count_sisa_dpmp=0;
        var count_besar_du=0;
        var count_sisa_du=0;
        var count_besar_iuran=0;

        if(resp.data.data_siswa){
            var value_siswa = resp.data.data_siswa[0];
            var value_siswa_detil = resp.data.data_siswa_detil;
            var value_iuran = resp.data.data_iuran;
            $('#lbl_no_ref').html(": "+value_siswa.ref_siswa);
            $('#lbl_nama_lengkap').html(": "+value_siswa.nama_lengkap_siswa);
            $('#lbl_kelas').html(": "+value_siswa.kelas_siswa);
            $('#lbl_tahun_ajaran').html(": "+value_siswa.tahun_ajaran_siswa);



            $.each(value_siswa_detil,function(key){
                console.log(this);
                if(this.jenis_iuran_siswa_detil=='DPMP') count_besar_dpmp=this.besar_iuran_siswa_detil;
                if(this.jenis_iuran_siswa_detil=='DU') count_besar_du=this.besar_iuran_siswa_detil;
                if(this.jenis_iuran_siswa_detil=='IURAN_PER_BULAN') count_besar_iuran=this.besar_iuran_siswa_detil;
            });
            $.each(value_iuran,function(key){
                if(this.jenis_iuran=='DPMP') count_sisa_dpmp+=this.besar_iuran;
                if(this.jenis_iuran=='DU') count_sisa_du+=this.besar_iuran;
            });

            renderDisplayIuranBulanan(value_iuran, count_besar_iuran);

            // //dpmp
            $('#lbl_besar_dpmp').html(": "+$.number(count_besar_dpmp,2));
            $('#lbl_sisa_dpmp').html(": "+$.number((count_besar_dpmp-count_sisa_dpmp),2));       
            // //du
            $('#lbl_besar_du').html(": "+$.number(count_besar_du,2));
            $('#lbl_sisa_du').html(": "+$.number((count_besar_du-count_sisa_du),2));  
            swal("Data ditemukan","Pastikan kembali data yang anda cari sudah sesuai","success");
        }else{
            $('#lbl_no_ref').html(":");
            $('#lbl_nama_lengkap').html(":");
            $('#lbl_kelas').html(":");
            $('#lbl_tahun_ajaran').html(":");
            swal("Data tidak ditemukan","Mohon periksa kembali NO.REF anda","warning");
            renderDisplayIuranBulanan(value_iuran);
            // //dpmp
            $('#lbl_besar_dpmp').html(": "+$.number(count_besar_dpmp,2));
            $('#lbl_sisa_dpmp').html(": "+$.number((count_besar_dpmp-count_sisa_dpmp),2));       
            // //du
            $('#lbl_besar_du').html(": "+$.number(count_besar_du,2));
            $('#lbl_sisa_du').html(": "+$.number((count_besar_du-count_sisa_du),2));  
        }
    }

    function ggbudag(){
        var resultList = [];
        var tahun_ajaran = $('#lbl_tahun_ajaran').text().substring(2).split("/");
        var date = new Date("July 10, "+tahun_ajaran[0]);
        var endDate = new Date("June 10, "+tahun_ajaran[1]);
        var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        while (date <= endDate)
        {
            var stringDate = monthNameList[date.getMonth()] + " " + date.getFullYear();
            resultList.push(stringDate);
            date.setMonth(date.getMonth() + 1);
        }

        return resultList;
    }

    function renderDisplayIuranBulanan(res_data,count_besar_iuran){
        console.log(res_data);
        var tahun_ajaran = $('#lbl_tahun_ajaran').text().substring(2).split("/");
        var date = new Date("July 10, "+tahun_ajaran[0]);
        var endDate = new Date("June 10, "+tahun_ajaran[1]);
        var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        var tbody = $("#tbl-data").find('tbody');
        var row="";
        var key=1;
            if(res_data!=undefined){

            if(res_data.length!=0){
            while (date <= endDate){
                $.each(res_data,function(keys){
                    var stringDate = monthNameList[date.getMonth()] + " " + date.getFullYear();
                    var jatuh_tempo = date.getFullYear() + "-" + ((date.getMonth()+1).toString().length==1?"0"+(date.getMonth()+1):date.getMonth()+1) + "-" + date.getDate();  
                   
                    if(this.jenis_iuran=='IURAN_PER_BULAN'){  
                        if(this.bulan_iuran == stringDate) row+='<tr class="bg-light" style="opacity: 0.9;">';
                        else row+='<tr>';
                        row+='<td>'+key+'</td>';
                        row+='<td>'+stringDate+'</td>';
                        row+='<td>'+jatuh_tempo+'</td>';
                        if(this.bulan_iuran == stringDate){
                            row+='<td>'+this.no_iuran+'</td>';
                            row+='<td>'+this.tanggal_iuran+'</td>';
                            row+='<td>'+$.number(this.besar_iuran,2)+'</td>';
                            row+='<td>'+this.status_iuran+'</td>';
                            row+='<td align="center"><i class="fas text-success fa-check-circle"></i></td>';
                        }else{
                            row+='<td></td>';
                            row+='<td></td>';
                            row+='<td></td>';
                            row+='<td></td>';
                            row+='<td align="center">';
                                row+='<div class="form-check">';
                                row+='<input class="form-check-input" type="checkbox" value='+count_besar_iuran+' name="tekan_saja" onclick="setIuranPerBulan('+count_besar_iuran+')" required>';
                                row+='</div>';
                            row+='</td>';

                        }
                            row+='</tr>';
                        key++;
                        date.setMonth(date.getMonth() + 1);
                    }
                });
            }
            }else{

                var key=1;
        var date = new Date("July 10, "+tahun_ajaran[0]);
        var endDate = new Date("June 10, "+tahun_ajaran[1]);
            while (date <= endDate){
                var stringDate = monthNameList[date.getMonth()] + " " + date.getFullYear();
                    var jatuh_tempo = date.getFullYear() + "-" + ((date.getMonth()+1).toString().length==1?"0"+(date.getMonth()+1):date.getMonth()+1) + "-" + date.getDate();  
                   
                    // row+='<tr class="bg-light" style="opacity: 0.9;">';
                    row+='<tr>';
                    row+='<td>'+key+'</td>';
                    row+='<td>'+stringDate+'</td>';
                    row+='<td>'+jatuh_tempo+'</td>';
                    row+='<td></td>';
                    row+='<td></td>';
                    row+='<td></td>';
                    row+='<td></td>';
                    row+='<td align="center">';
                        row+='<div class="form-check">';
                        row+='<input class="form-check-input" type="checkbox" value="" id="" required>';
                        row+='</div>';
                    row+='</td>';

                    row+='</tr>';
                    key++;
                    date.setMonth(date.getMonth() + 1);
            }
        }
    }
        tbody.html(row);
    }

    function setIuranPerBulan(value){
        var data = $('[name=tekan_saja]:checked');
        lbl_nominal_iuran=0;
        $.each(data,function(key){
            lbl_nominal_iuran+=parseInt(this.value);
        });
        var check = $('[name=tekan_saja]').is(':checked');
        if(check)
        $('#lbl_biaya_iuran_per_bulan').html(': '+$.number(lbl_nominal_iuran,2));
        else
        $('#lbl_biaya_iuran_per_bulan').html(': 0');

        $('#lbl_total_biaya').text(": "+($.number(lbl_nominal_iuran+lbl_nominal_dpmp+lbl_nominal_du,2)));
    }
    function getDataRiwayat(){
        var obj = [
                {
                    "bulan_iuran": "Jul 2018",
                    "no_iuran": "07102018",
                    "tanggal_iuran": "2018-07-10",
                    "besar_iuran": 250000,
                    "status_iuran": "LUNAS",
                    "jenis_iuran": "IURAN"
                },
                {
                    "bulan_iuran": "Aug 2018",
                    "no_iuran": "08102018",
                    "tanggal_iuran": "2018-08-10",
                    "besar_iuran": 250000,
                    "status_iuran": "LUNAS",
                    "jenis_iuran": "IURAN"
                },
                {
                    "bulan_iuran": "",
                    "no_iuran": "07102018",
                    "tanggal_iuran": "2018-07-10",
                    "besar_iuran": 500000,
                    "status_iuran": "LUNAS",
                    "jenis_iuran": "DPMP"
                },
                {
                    "bulan_iuran": "",
                    "no_iuran": "08102018",
                    "tanggal_iuran": "2018-08-10",
                    "besar_iuran": 250000,
                    "status_iuran": "LUNAS",
                    "jenis_iuran": "DPMP"
                },
                {
                    "bulan_iuran": "",
                    "no_iuran": "09102018",
                    "tanggal_iuran": "2018-09-10",
                    "besar_iuran": 500000,
                    "status_iuran": "LUNAS",
                    "jenis_iuran": "DPMP"
                },
                {
                    "bulan_iuran": "",
                    "no_iuran": "10102018",
                    "tanggal_iuran": "2018-10-10",
                    "besar_iuran": 250000,
                    "status_iuran": "LUNAS",
                    "jenis_iuran": "DPMP"
                },
                {
                    "bulan_iuran": "",
                    "no_iuran": "11102018",
                    "tanggal_iuran": "2018-11-10",
                    "besar_iuran": 300000,
                    "status_iuran": "LUNAS",
                    "jenis_iuran": "DU"
                },
            ];
        return obj;
    }

    function getDataMasterTagihan(){
        var obj = [
                {
                    "besar_iuran": 250000,
                    "jenis_iuran": "IURAN"
                },
                {
                    "besar_iuran": 3000000,
                    "jenis_iuran": "DPMP"
                },
                {
                    "besar_iuran": 300000,
                    "jenis_iuran": "DU"
                }
            ];
        return obj;
    }

    function renderBulanKe(val){
        var html="";
        for(var i = 0; i < val;i++){
          html+='<input type="text" class="form-control mt-1" id="" placeholder="Bulan Ke">';
        };
        $('#bulan-ke').html(html);
    }
    function setBulanIuran(){
        var val='';
        $.each($('#bulan-ke input'), function(key){
          console.log(this.value);
          val+=";"+this.value;
        })
        $('[name=bulan_iuran]').val(val.substring(1));
    }

    function display(page = 1){
        $('#more-button-row').remove();
        var tbody = $("#tbl-data").find('tbody');
        if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
            tbody.text('');
        }
        
        tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        
        params = $( "#search-form" ).serialize();
        ajaxGET(path + '/list_table?page='+page+'&'+params,'onGetListSuccess','onGetListError');
    }

    function onGetListSuccess(response){
        console.log(response);
        $('#loading-row').remove();
        $('#no-data-row').remove();
        
        var tbody = $("#tbl-data").find('tbody');
        
        var row = "";
        var num = $('.data-row').length+1;
        $.each(response.data,function(key,value){
            row += render_row(value, num);
            num++;
        });
        row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
        
    }
    function render_row(value, num){
    //  console.log(value);
        var row = "";
        row += '<tr class="data-row" id="row-'+value.id_siswa+'">';
        row += '<td>'+(num)+'</td>';
        row += '<td class="">'+value.nis_siswa+'</td>';
        row += '<td class="">'+value.nama_lengkap_siswa+'</td>';
        row += '<td class="">'+value.kelas_siswa+'</td>';
        row += '<td class="">'+value.jk_siswa+'</td>';
        row += '<td class="">';
        row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-info pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'edit\')"><i class="fas fa-pencil-alt"></i></a> ';
        row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'delete\')"><i class="fas fa-trash-alt"></i></a>';
        row += '</td>';
        row += '</tr>';
        return row;
    }
    function onGetListError(response) {
        console.log(response);
        $('#ui-view').html($('#ui-view-msg-error').html());
    }

    function save(){
        var obj = new FormData(document.querySelector('#form'));
        if(selected_id != '') obj.append('id', selected_id);
        obj.append('kelas', $('#tingkat').val()+' '+$('#jurusan').val()+' '+$('#sub_tingkat').val());
        
        ajaxPOST(path + '/save',obj,'onActionSuccess','onSaveError');
    }

    function onSaveSuccess(response){
        console.log(response);
        $('#msg-info-body').html('Data telah berhasil di update.');
        $('#msg-info').modal('show');
        // setTimeout(function(){ window.location.href="pengguna?vmenu=daftar"; }, 1000);
    }
    function onActionSuccess(response){
        // display();
        // $('#modal-form').modal('hide');
        // $('#msg-confirm').modal('hide');
        // selected_action = '';
        console.log(response);
        swal("Message", response.message, "success", {
          buttons: {
            cancel: "Kembali ke daftar siswa",
            go: {
              text: "Tambah lagi data siswa",
              value: "go",
            }//,
            // back: {
            //   text: "Kembali ke daftar siswa",
            //   value: "back",
            // }
          },
        })
        .then((value) => {
          switch (value) {
         
            case "go":
              window.location.href=ctx+"page/form_siswa_view";
              break;
         
            // case "back":
            //   window.location.href=ctx+"page/siswa_view";
            //   break;
         
            default:
              window.location.href=ctx+"page/siswa_view";
          }
        });
    }
    function retes(){
        swal("Message", "response.message", "success", {
          buttons: {
            //cancel: "Run away!",
            go: {
              text: "Tambah lagi data siswa",
              value: "go",
            },
            back: {
              text: "Kembali ke daftar siswa",
              value: "back",
            }
          },
        })
        .then((value) => {
          switch (value) {
         
            case "go":
              window.location.href=ctx+"page/form_siswa_view";
              break;
         
            case "back":
              window.location.href=ctx+"page/siswa_view";
              break;
         
            default:
              swal("Something Wrong!","error");
          }
        });       
         // alert(response.message);
    }
    function doAction(id, action, confirm = 0){
        selected_action = action;
        $('#modal-form-msg').hide();
        $('#msg-confirm-alert').hide();
        if(confirm == 0) ajaxGET(path + '/get_by_id/'+id+'?action='+selected_action,'onPrepareActionSuccess','onError');
        else ajaxPOST(path + '/do_action/'+id+'/'+selected_action,{},'onActionSuccess','onError');
    }

    function onPrepareActionSuccess(response) {
        console.log(response);
        var value = response.data;
        selected_id = value.id_siswa;
        if(selected_action == 'edit'){
            fillFormValue(value);
            $('#modal-form').modal('show');
        }else{
            swal({
              title: "Are you sure?",
              text: response.message,
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
                console.log(willDelete);
                if(willDelete)
                doAction(selected_id,'delete', 1);
            });
        }
    }
    

    function fillFormValue(value){
        $('#form')[0].reset();
        
        $('[name=nis_siswa]').val(value.nis_siswa);
        $('[name=nama_siswa]').val(value.nama_siswa);
        $('[name=kelas_siswa]').val(value.kelas_siswa);
    }

	function add() {
        selected_id = ''; // wajib
        selected_action = 'add'; // wajib
	    $('#form')[0].reset(); // reset form on modals
	    $('.form-group').removeClass('has-error'); // clear error class
	    $('.help-block').empty(); // clear error string
	    $('#modal-form').modal('show'); // show bootstrap modal
	    $('.modal-title').text('Tambah Siswa'); // Set Title to Bootstrap modal title
        $('#form-pass').removeClass("d-none");

	}
