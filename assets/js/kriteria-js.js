	var path = ctx + 'KriteriaController';
    var params='';
    var selected_action='';
    var selected_id='';

    function init() {
        display();

        $('#search-form').submit(function(e){
            display();
            e.preventDefault();
        });
        
        $('#btn-yes-msg-confirm').click(function(e){
            if(selected_action == 'delete' ){
                doAction(selected_id, selected_action, 1);
            }
            
        });
    };

    function display(page = 1){
        $('#more-button-row').remove();
        var tbody = $("#tbl-data").find('tbody');
        if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
            tbody.text('');
        }
        
        tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        
        params = $( "#search-form" ).serialize();
        ajaxGET(path + '/list_table?page='+page+'&'+params,'onGetListSuccess','onGetListError');
    }

    function onGetListSuccess(response){
        console.log(response);
        $('#loading-row').remove();
        $('#no-data-row').remove();
        
        var tbody = $("#tbl-data").find('tbody');
        
        var row = "";
        var num = $('.data-row').length+1;
        $.each(response.data,function(key,value){
            row += render_row(value, num);
            num++;
        });
        row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
        
    }
    function render_row(value, num){
    //  console.log(value);
        var row = "";
        row += '<tr class="data-row" id="row-'+value.id_kriteria+'">';
        row += '<td>'+(num)+'</td>';
        row += '<td class="">'+value.nama_kriteria+'</td>';
        row += '<td class="">'+value.bobot_kriteria+'</td>';
        row += '<td class="">';
        row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-outline-info" type="button" onclick="doAction(\''+value.id_kriteria+'\',\'edit\')"><i class="fa fa-pencil"></i></a> ';
        row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-outline-danger" type="button" onclick="doAction(\''+value.id_kriteria+'\',\'delete\')"><i class="fa fa-trash-o"></i></a>';
        row += '</td>';
        row += '</tr>';
        return row;
    }
    function onGetListError(response) {
        $('#ui-view').html($('#ui-view-msg-error').html());
    }

    function save(){
        var obj = new FormData(document.querySelector('#form'));
        if(selected_id != '') obj.append('id_kriteria', selected_id);
        
        ajaxPOST(path + '/save',obj,'onActionSuccess','onSaveError');
    }

    function onSaveSuccess(response){
        console.log(response);
        $('#msg-info-body').html('Data telah berhasil di update.');
        $('#msg-info').modal('show');
        // setTimeout(function(){ window.location.href="pengguna?vmenu=daftar"; }, 1000);
    }
    function onActionSuccess(response){
        display();
        $('#modal-form').modal('hide');
        $('#msg-confirm').modal('hide');
        selected_action = '';
        console.log(response);
        alert(response.message);
    }

    function doAction(id, action, confirm = 0){
        selected_action = action;
        $('#modal-form-msg').hide();
        $('#msg-confirm-alert').hide();
        if(confirm == 0) ajaxGET(path + '/get_by_id/'+id+'?action='+selected_action,'onPrepareActionSuccess','onError');
        else ajaxPOST(path + '/do_action/'+id+'/'+selected_action,{},'onActionSuccess','onError');
    }

    function onPrepareActionSuccess(response) {
        console.log(response);
        var value = response.data;
        selected_id = value.id_kriteria;
        if(selected_action == 'edit'){
            fillFormValue(value);
            $('#modal-form').modal('show');
        }else{
            $('#msg-confirm-body').html(response.message);
            $('#msg-confirm').modal('show');
        }
    }
    

    function fillFormValue(value){
        $('#form')[0].reset();
        $('[name=nama_kriteria]').val(value.nama_kriteria);
        $('[name=bobot_kriteria]').val(value.bobot_kriteria);
    }

	function add() {
        selected_id = ''; // wajib
        selected_action = 'add'; // wajib
	    $('#form')[0].reset(); // reset form on modals
	    $('.form-group').removeClass('has-error'); // clear error class
	    $('.help-block').empty(); // clear error string
	    $('#modal-form').modal('show'); // show bootstrap modal
	    $('.modal-title').text('Tambah Kriteria'); // Set Title to Bootstrap modal title
        $('#form-pass').removeClass("d-none");

	}
