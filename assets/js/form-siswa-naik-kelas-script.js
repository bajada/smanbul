	var path = ctx + 'SiswaController';
    var path_jenis_iuran = ctx + 'JenisIuranController'; 
    var path_kelas = ctx + 'KelasController'; 
    var params='';
    var selected_action='';
    var selected_id='';
    var list_iuran = [];
    var jumlah_siswa=0;
    var kelas=0;
    var selected_all=true;
    var selected_key='';
    var list_obj=[];
    var list_kelas=[];

    function init() {
        
        getSelectKelas();
        getSelectMasterIuran();

        //getTahunAjaranAll();
        //getSelectTahun();
        display(1);

        $('#form-data').submit(function(e){
          // e.preventDefault();
          // console.log(path);
          // save();
          console.log(e);
        });

        $('#search-form').submit(function(e){
            display();
            e.preventDefault();
        });

        $('#form-siswa').submit(function(e){
            console.log(this);
            var jumlah_siswa=$('[name=jumlah_siswa]').val();
            var kelas_siswa=$('[name=kelas]').val();
            if(kelas_siswa!='') kelas_siswa='&kelas='+kelas_siswa;
            if(jumlah_siswa=='') {
                alert("Jumlah siswa wajib diisi.");
                return false;
            }
            window.location.href='form_siswa_view?jumlah_siswa='+jumlah_siswa+kelas_siswa;
            e.preventDefault();
        });

        $(document).on('click', '#tbl-data-kelas tr', function() {
           $("#tbl-data-kelas tr").removeClass("bg-info");
           $(this).toggleClass("bg-info");
        });

        $('#bg-head').hide();
    };


    function getSelectMasterIuran(tingkat=""){
        $.getJSON(path_jenis_iuran + '/list_table?page='+1, function(response){
            console.log(response);
        });
    }

    function check_tahun_ajaran(){
        var date = new Date();
            // date = new Date("July 10, 2019");
        var month = date.getMonth();

        if(month>=6){
            date=date.getFullYear()+"/"+(date.getFullYear()+1);
        }else{
            date=(date.getFullYear()-1)+"/"+date.getFullYear();
        }
        return date;
    }
    function check_tahun_ajaran_by(tanggal){
        var date = new Date();
        tanggal = tanggal.split("-");//0 tahun, 1 month, 2 date
        date.setDate(tanggal[2]);
        date.setMonth(tanggal[1]-1);
        // alert(tanggal[0]-1);
        date.setFullYear(tanggal[0]);
        
        if(date.getMonth()>=6){
            date=date.getFullYear()+"/"+(date.getFullYear()+1);
        }else{
            date=(date.getFullYear()-1)+"/"+date.getFullYear();
        }
        return date;
    }
    function getSelectTahun() {
       $("#tahun_ajaran").empty();
        $("#tahun_ajaran").append(new Option('Semua Tahun Ajaran', ''));
        var d = new Date();
        var n = d.getFullYear();
        var tahun = n - 5;
        for (var i = n; i>= tahun; i--) {
            $("#tahun_ajaran").append(new Option(i+"/"+(i+1)));
        }
        $("#tahun_ajaran").val(check_tahun_ajaran()).trigger('change');
    }
    function getSelectKelas(){
        $('[name="kelas[]"], [name=kelas]').empty();
        $('[name="kelas[]"], [name=kelas]').append(new Option('Pilih Kelas', ''));
        // ajaxGET(path_kelas + '/list_table?page='+1+'&'+"",'onGetSelectKelas','onGetListError');
        ajaxGET(path_kelas + '/list_table?page='+1,'onGetSelectKelas','onGetListError');
    }
    function onGetSelectKelas(response){
        $.each(response.data, function(key_2, value) {
            $('[name=kelas]').append(new Option(value.tingkat_kelas+"-"+value.jurusan_kelas+" "+value.sub_tingkat_kelas, value.id_kelas));
            list_kelas.push({"id_kelas" : value.id_kelas, "tingkat_kelas" : value.tingkat_kelas, "kelas" : value.tingkat_kelas+"-"+value.jurusan_kelas+" "+value.sub_tingkat_kelas});
        });
        // $('[name=kelas]').selectric('refresh');
    }
    function display(page = 1, params=''){
        $('#more-button-row').remove();
        var tbody = $("#tbl-data").find('tbody');
        // if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
        if($( "#tahun_ajaran" ).val() != ''){
            tbody.text('');
        }
        
        tbody.append('<tr id="loading-row"><td colspan="9"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        
        // params = $( "#search-form" ).serialize();
        //jgn difilter kalao mau edit
        // params += "tahun_ajaran_siswa="+$( "#tahun_ajaran" ).val();//+"&tingkat_kelas="+$("#tbl-data-kelas").find('tbody tr.bg-info').data('tingkat');
        ajaxGET(path + '/list_siswa_kelas_table_new?page='+page+'&'+params,'onGetListSuccess','onGetListError');
    }

    function onGetListSuccess(response){
        $.getJSON(path_jenis_iuran + '/list_table?page='+1, function(response_master_jenis_iuran){
            console.log(response);
       
            console.log(response);
            // list_obj.push(response.data);
            $('#loading-row').remove();
            $('#no-data-row').remove();
            
            var thead = $("#tbl-data").find('#sub-du');
            var thead1 = $("#tbl-data").find('#sub-du-1');
            var tbody = $("#tbl-data").find('tbody');
            
            var row = "";
            var num = $('.data-row').length+1;
            $.each(response.data,function(key,value){
                $.each(arr_siswa_kelas,function(key_arr, value_siswa_kelas){

                    /*$.each(list_kelas, function(key_kelas, value_kelas){
                        if(value_siswa_kelas==value.id_siswa_kelas){
                            // if(this.id_kelas==kelas_baru){
                            if(this.tingkat_kelas==tingkat_kelas){  
                                value.list_iuran=[];
                                value['tingkat_kelas_full_baru']=value_kelas.kelas;
                                $.each(response_master_jenis_iuran.data, function(key_jenis_iuran, value_jenis_iuran){
                                    if(value_jenis_iuran.tingkat_jenis_iuran==value_kelas.tingkat_kelas){
                                        value.list_iuran.push(this);
                                    }
                                });
                                row += render_row(value, num);
                                num++;
                            }
                        }
                    });*/

                    if(value_siswa_kelas==value.id_siswa_kelas){
                        // if(this.id_kelas==kelas_baru){
                        // if(this.tingkat_kelas==tingkat_kelas){  
                            value.list_iuran=[];
                        //     value['tingkat_kelas_full_baru']=value_kelas.kelas;
                            $.each(response_master_jenis_iuran.data, function(key_jenis_iuran, value_jenis_iuran){
                                if(value_jenis_iuran.tingkat_jenis_iuran==tingkat_kelas){
                                    value.list_iuran.push(this);
                                }
                            });
                        //     row += render_row(value, num);
                        //     num++;
                        // }
                        row += render_row(value, num);
                        num++;
                    }

                });
            });
            row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="9"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
            $('#info-tabel').html("Total data : <span>"+num+"</span>");
            //render header
            row="";
            var colspan_head=0;
            $.each(response_master_jenis_iuran.data, function(key_jenis_iuran, value_jenis_iuran){

                //$.each(list_kelas, function(key_kelas, value_kelas){
                    // if(value_kelas.tingkat_kelas==tingkat_kelas){
                        if(value_jenis_iuran.tingkat_jenis_iuran==tingkat_kelas){
                            if(value_jenis_iuran.nama_jenis_iuran=="IURAN_PER_BULAN"){
                                row += '<td width="1%">'+"SPP/BULAN"+'</td>';
                            }
                            if(value_jenis_iuran.nama_jenis_iuran=="DU"){
                                row += '<td width="1%">'+"OSIS/TAHUN"+'</td>';
                            }
                            colspan_head++;
                        }
                    // }
                //});
            });


            row += '<td width="1%">SPP JULI</td>';
            row += '<td width="1%">OSIS</td>';
            thead.html(row);

            row="";
            row += '<td colspan="'+colspan_head+'" class="text-center">Alokasi DU</td>';
            row += '<td colspan="'+colspan_head+'" class="text-center">Rincian DU</td>';

            row += '<th rowspan="2" class="text-successs align-middle">Jumlah</th>';
            row += '<th rowspan="2" class="text-successs align-middle">Sisa</th>';
            row += '<th rowspan="2" class=" align-middle">Tanggal Registrasi DU</th>';
            row += '<th rowspan="2" class=" align-middle">Keterangan</th>';

            thead1.append(row);
        });
    }

    function render_row(value, num){
     console.log(value);
        var row = "";
        row += '<tr data-id='+value.id_siswa+' contenteditable="false" class="data-row bg-light" id="row-'+value.id_siswa+'">';
        // row+='<td align="center">';
        // row+='<div class="form-check">';
        //     row+='<input class="form-check-input" style="position:unset" type="checkbox" value='+value.id_siswa_kelas+' name="id[]" data-tahun_ajaran='+value.tahun_ajaran+'>';
        // row+='</div>';
        // row+='</td>';
        row += '<td>'+(num)+'</td>';
        row += '<td class="">'+value.ref_siswa+'</td>';
        // row += '<td class="">'+value.nis_siswa+'</td>';
        row += '<td class="">'+value.nama_lengkap_siswa+'</td>';
        row += '<td class="">'+value.jenis_kelamin_siswa+'</td>';
        // row += '<td class="">'+value.tahun_ajaran_siswa_kelas+'</td>';
        // var tahun_ajaran_baru = "07/01/2020";
        row += '<td class="">'+check_tahun_ajaran_by(tahun_ajaran_baru)+'</td>';
        row += '<td class="">'+value.tahun_angkatan_siswa+'</td>';
        row += '<td class="">'+value.tingkat_kelas+" "+value.jurusan_kelas+" "+value.sub_tingkat_kelas+'</td>';
        // $.each(list_kelas, function(key_kelas){
        //     if(this.id_kelas==kelas_baru){
        //         row += '<td class="text-success">'+this.kelas+'</td>';
        //     }
        // })


        /*if(kelas_baru=="null"){
            row += '<td class="pr-3">';
            row += '<select name="tingkat_siswa[]" class="w-100 rounded-0"><option value="" selected>Pilih</option>';
            $.each(list_kelas, function(key_kelas, value_kelas){
                if(value_kelas.tingkat_kelas==tingkat_kelas){
                    row += '<option value='+this.id_kelas+'>'+this.kelas+'</option>';
                }
            });
            row += '</select>';
            row += '</td>';
        }else{
             $.each(list_kelas, function(key_kelas, value_kelas){
                if(value_kelas.id_kelas==kelas_baru){
                row += '<td class="text-success">'+this.kelas+'</td>';
                }
            });
            // row += '<td class="text-success">'+kelas_baru+'</td>';
        }*/

        row += '<td class="pr-3">';
        row += '<select name="tingkat_siswa[]" class="w-100 rounded-0"><option value="" selected>Pilih</option>';
            $.each(list_kelas, function(key_kelas, value_kelas){
                if(value_kelas.tingkat_kelas==tingkat_kelas){
                    // (kelas_baru!="null"?kelas_baru:this.kelas)
                    if(this.id_kelas == kelas_baru){
                        row += '<option selected value='+this.id_kelas+'>'+this.kelas+'</option>';
                    }else{
                        row += '<option value='+this.id_kelas+'>'+this.kelas+'</option>';
                    }
                }
            });
        row += '</select>';
        row += '</td>';

        var besar_iuran=0;
        var besar_iuran_spp=0;
        var besar_iuran_osis=0;
        $.each(value.list_iuran, function(key_jenis_iuran){
            if(this.nama_jenis_iuran=="IURAN_PER_BULAN") 
            row += '<td class="">'+"<input data-key="+num+" onkeyup='setMy(this)' id='master-spp-"+num+"' style='border-radius:0;' type='number' value="+this.besar_jenis_iuran+">"+'</td>';
            if(this.nama_jenis_iuran=="DU") 
            row += '<td class="">'+"<input data-key="+num+" onkeyup='setMy(this)' id='master-osis-"+num+"' style='border-radius:0;' type='number' value="+this.besar_jenis_iuran+">"+'</td>';
            besar_iuran+=parseInt(this.besar_jenis_iuran);
        })
        
        row += '<td class="">'+"<input value='0' id='rincian-spp-"+num+"' data-key="+num+" onkeyup='setMy(this)' style='border-radius:0;' type='number'>"+'</td>';
        row += '<td class="">'+"<input value='0' id='rincian-osis-"+num+"' data-key="+num+" onkeyup='setMy(this)' style='border-radius:0;' type='number'>"+'</td>';
        row += '<td class="text-success" id="jumlah-'+num+'">'+0+'</td>';
        row += '<td class="text-danger" id="sisa-'+num+'" data-sisa='+besar_iuran+'>'+$.number(besar_iuran)+'</td>';
        
        // row += '<td class="">'+tahun_ajaran_baru+'</td>';
        row += '<td class="">'+"<input value='"+tahun_ajaran_baru+"' id='tanggal-iuran-"+num+"' data-key="+num+" style='border-radius:0;' type='date'>"+'</td>';
        row += '<td class="">'+"<input value='' id='keterangan-iuran-"+num+"' data-key="+num+" style='border-radius:0;' type='text'>"+'</td>';
        // row += '<td class="">';
        // // row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-info pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'edit\')"><i class="fas fa-pencil-alt"></i></a> ';
        // row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-primary pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'delete\')"><i class="fas fa-search"></i></a>';
        // row += '</td>';
        row += '</tr>';
        return row;
    }

    

    function onGetListError(response) {
        alert("ERROR");
        $('#ui-view').html($('#ui-view-msg-error').html());
    }

    function save(){
        // var obj = new FormData(document.querySelector('#form'));
        // if(selected_id != '') obj.append('id', selected_id);
        // obj.append('kelas', $('#tingkat').val()+' '+$('#jurusan').val()+' '+$('#sub_tingkat').val());
        // obj.append('list_iuran', JSON.stringify(list_iuran[0]));
        //
        /*var obj = {
            "siswa_kelas" : {
                "header_id_siswa_kelas" : value.id_siswa,
                "kelas_siswa_kelas" : kelas_baru,
                "tahun_ajaran_siswa_kelas" : null,
                "status_siswa_kelas" : null,
                "tanggal_registrasi_siswa_kelas" : null,
            },
            "siswa_detil" : {
                "header_id_siswa_detil" : value.id_siswa,
                "tingkat_siswa_detil" : value.tingkat_kelas_full_baru,
                "tahun_ajaran_siswa_kelas" : null,
                "status_siswa_kelas" : null,
                "tanggal_registrasi_siswa_kelas" : null,
            }
        }
        list_obj.push(value);*/
        obj_arr = []; 
        var obj ={}
        // var obj_arr = [];
        console.log("============");
        var data = $('#tbl-data').find('tbody tr');
        $.each(data,function(key){
                // console.log($(this).data('id'));
                console.log($(this).find('td'));
                console.log($(this));
                console.log($(this)[0].cells);

                obj={ 
                    "id_siswa": $(this).data('id'),
                    "siswa_kelas" : [
                        {
                            "kelas_siswa_kelas": $(this)[0].cells[7].children[0].value,
                            "tahun_ajaran_siswa_kelas": check_tahun_ajaran_by($(this)[0].cells[14].children[0].value),
                            "status_siswa_kelas": 1,
                            "tanggal_registrasi_siswa_kelas": $(this)[0].cells[14].children[0].value,
                        }
                    ],
                    "siswa_detil" : [
                        {
                            "tingkat_siswa_detil": $(this)[0].cells[7].children[0].options[$(this)[0].cells[7].children[0].options.selectedIndex].text.split("-")[0],
                            "besar_iuran_siswa_detil": $(this)[0].cells[8].children[0].value,
                            "jenis_iuran_siswa_detil": "IURAN_PER_BULAN",
                        },
                        {
                            "tingkat_siswa_detil": $(this)[0].cells[7].children[0].options[$(this)[0].cells[7].children[0].options.selectedIndex].text.split("-")[0],
                            "besar_iuran_siswa_detil": $(this)[0].cells[9].children[0].value,
                            "jenis_iuran_siswa_detil": "DU",
                        }
                    ],
                    "siswa_iuran" : []
                }
                obj_arr.push(obj);

                if($(this)[0].cells[10].children[0].value != 0){
                    obj_arr[key].siswa_iuran.push(
                        {
                            "bulan_iuran": $(this)[0].cells[14].children[0].value,
                            "besar_iuran": $(this)[0].cells[10].children[0].value,
                            "tanggal_iuran": $(this)[0].cells[14].children[0].value,
                            "jenis_iuran": "IURAN_PER_BULAN",
                            "status_iuran": ($(this)[0].cells[8].children[0].value==$(this)[0].cells[10].children[0].value?"LUNAS":"BELUM_LUNAS"),
                            "keterangan_iuran": $(this)[0].cells[15].children[0].value
                        }
                    );   
                }
                if($(this)[0].cells[11].children[0].value != 0){
                    obj_arr[key].siswa_iuran.push(
                        {
                            "bulan_iuran": "",
                            "besar_iuran": $(this)[0].cells[11].children[0].value,
                            "tanggal_iuran": $(this)[0].cells[14].children[0].value,
                            "jenis_iuran": "DU",
                            "status_iuran": ($(this)[0].cells[9].children[0].value==$(this)[0].cells[11].children[0].value?"LUNAS":"BELUM_LUNAS"),
                            "keterangan_iuran": $(this)[0].cells[15].children[0].value
                        }
                    );   
                }
        });
        console.log(obj_arr);

        var obj = new FormData();
        obj.append('list_obj', JSON.stringify(obj_arr));

        ajaxPOST(path + '/save_naik_kelas',obj,'onActionSuccess','onSaveError');
    }

    function onSaveSuccess(response){
        console.log(response);
        $('#msg-info-body').html('Data telah berhasil di update.');
        $('#msg-info').modal('show');
        // setTimeout(function(){ window.location.href="pengguna?vmenu=daftar"; }, 1000);
    }
    //

    //
    function onActionSuccess(response){
        Swal.fire({
          type: 'success',
          title: 'Message',
          text: response.message,
          //footer: '<a href>Why do I have this issue?</a>'
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Oke!'
        })
        .then((value) => {
            if(value.value){
                window.location.href='siswa_view';
            }
        });
    }

    function doAction(id, action, confirm = 0){
        selected_action = action;
        $('#modal-form-msg').hide();
        $('#msg-confirm-alert').hide();
        if(confirm == 0) ajaxGET(path + '/get_by_id/'+id+'?action='+selected_action,'onPrepareActionSuccess','onError');
        // else ajaxPOST(path + '/do_action/'+id+'/'+selected_action,{},'onActionSuccess','onError');
        else ajaxPOST(path + '/delete_all/'+id,{},'onActionSuccess','onError');
    }

    function onPrepareActionSuccess(response) {
        console.log(response);
        var value = response.data;
        selected_id = value.id_siswa;
        if(selected_action == 'edit'){
            fillFormValue(value);
            $('#modal-form').modal('show');
        }else{
            Swal.fire({
              title: 'Are you sure?',
              text: response.message,
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                doAction(selected_id,'delete', 1);
                Swal.fire(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                display(1);
              }
            })
        }
    }
    

    function fillFormValue(value){
        $('#form')[0].reset();
        
        $('[name=nis_siswa]').val(value.nis_siswa);
        $('[name=nama_siswa]').val(value.nama_siswa);
        $('[name=kelas_siswa]').val(value.kelas_siswa);
    }

	function add() {
        selected_id = ''; // wajib
        selected_action = 'add'; // wajib
	    $('#form')[0].reset(); // reset form on modals
	    $('.form-group').removeClass('has-error'); // clear error class
	    $('.help-block').empty(); // clear error string
	    $('#modal-form').modal('show'); // show bootstrap modal
	    $('.modal-title').text('Tambah Siswa'); // Set Title to Bootstrap modal title
        $('#form-pass').removeClass("d-none");

	}

    function getTahunAjaranAll(){
        var strdate=[];
        var angkatan= new Date("July 10, 2017");
        // var date = new Date("December 10, 2019");//+tahun_ajaran[0]
        var date = new Date();
        var selisih=(date.getFullYear()-angkatan.getFullYear());
        for(var start_ajaran=angkatan.getFullYear(); start_ajaran < angkatan.getFullYear()+selisih; start_ajaran++){
            if(start_ajaran+"/"+(start_ajaran+1)!=date){
                strdate.push(start_ajaran+"/"+(start_ajaran+1).toString());
                $('#tahun_ajaran').append(new Option(start_ajaran+"/"+(start_ajaran+1).toString(), start_ajaran+"/"+(start_ajaran+1).toString()));
            }
        }
        return strdate;
    }

    function filter(params){
        display(1, params);
    }



    function goToEdit(mode=""){
        // var id = $('#tbl-data').find('input:checked');
        // var arr=[];
        // $.each(id, function(key){
        //     arr.push(this.value);
        // });
        // if(arr=='') {
        //     var resp_msg={"title" : "Tidak dapat menambah data", "body" : "Karena anda belum memilih data siswa!", "icon"  : "warning"};
        //     showAlertMessage(resp_msg, 1500);
        // }else{
        //     var tahun_ajaran=id.data('tahun_ajaran');
        //     if(mode=='edit'){
        //         mode="&mode=edit";
        //     }
        //     window.location.href=ctx+'page/form_siswa_view?id='+arr.toString()+"&tahun_ajaran="+tahun_ajaran+mode;
        // }
        // var obj = new FormData(document.querySelector('#form-data'));
        // obj.append("list_obj", obj);
        $('#form-data').attr('action', ctx+'page/form-siswa-naik-kelas_view');
        // $('#form-data').attr('list_obj', JSON.stringify(list_obj));
        $('#form-data').trigger('submit');
    }

    function goToDelete(){
        var obj = new FormData(document.querySelector('#form-data'));
        ajaxPOST(path + '/delete_all/',obj,'onActionSuccess','onError');
    }

    function setMy(e){
        var elem = $(e);
        console.log(elem.data('key'));
        var rinci_spp=$('#rincian-spp-'+elem.data('key')).val();
        var rinci_osis=$('#rincian-osis-'+elem.data('key')).val();

        $('#jumlah-'+elem.data('key')).text($.number(parseInt(rinci_spp)+parseInt(rinci_osis)));
        
        //count sisa
        var master_spp = parseInt($('#master-spp-'+elem.data('key')).val());
        var master_osis = parseInt($('#master-osis-'+elem.data('key')).val());

        $('#sisa-'+elem.data('key')).text($.number(parseInt(master_spp+master_osis)-(parseInt(rinci_spp)+parseInt(rinci_osis))));

    }
