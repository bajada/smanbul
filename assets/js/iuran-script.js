	var path = ctx + 'IuranSiswaController';
    var params='';
    var selected_action='';
    var selected_id='';
    var selected_id_kelas=0;
    var tahun_ajaran='';
    var jenis_iuran='';
    var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    var obj_arr = [];       
    
    var lbl_total_biaya = 0;
    var lbl_nominal_dpmp = 0;
    var lbl_nominal_du = 0;
    var lbl_nominal_iuran = 0;

    function init() {
        selected_id = ($.urlParam('id') == null ? '' : $.urlParam('id'));
        tahun_ajaran = ($.urlParam('tahun_ajaran') == null ? '' : $.urlParam('tahun_ajaran'));

        if(selected_id != '') {
            ajaxGET(path + '/get_by_id/'+selected_id+'?tahun_ajaran='+tahun_ajaran,'onGetSuccess','onGetError');
        }

        $('#search-form').submit(function(e){
            displaySiswa();
            e.preventDefault();
        });
        $('[name=nominal_dpmp]').change(function(){
            lbl_nominal_dpmp=parseInt($(this).val().replace(/,/g, ''));
            $('#lbl_biaya_dpmp').html(": "+$.number(this.value,2));
            $('#lbl_total_biaya').text(": "+($.number(lbl_nominal_iuran+lbl_nominal_dpmp+lbl_nominal_du,2)));
        });
        $('[name=nominal_du]').change(function(){
            lbl_nominal_du=parseInt($(this).val().replace(/,/g, ''));
            $('#lbl_biaya_du').html(": "+$.number(this.value,2));
            $('#lbl_total_biaya').text(": "+($.number(lbl_nominal_iuran+lbl_nominal_dpmp+lbl_nominal_du,2)));

        });

        $('#btn-simpan').click(function(){
            obj_arr = []; 
            var obj ={}
            $.each($('#tbl-data').find('tbody tr'),function(key){
                if($(this).find('td input:checked').length){
                    console.log($(this).data('id'));
                    console.log($(this).find('td'));
                    console.log($(this));
                    console.log($(this)[0].cells);
                    obj ={
                        "id_siswa_iuran": selected_id,
                        // "id_siswa_kelas_iuran": selected_id_kelas,
                        "bulan_iuran": $(this)[0].cells[1].textContent,
                        "jumlah_iuran": $(this)[0].cells[2].textContent.replace(",",""),
                        "besar_iuran": this.cells[3].textContent.replace(",",""),//$(this)[0].cells[0].children[0].children[0].value,
                        "jenis_iuran": "IURAN_PER_BULAN",
                        "tanggal_iuran": $(this)[0].cells[4].textContent,
                        "keterangan_iuran": $(this)[0].cells[5].textContent,
                        "status_iuran": parseInt($(this)[0].cells[2].textContent.replace(",",""))==(parseInt(this.cells[3].textContent.replace(",",""))+parseInt($(this)[0].cells[0].children[0].children[0].dataset.iuranTelahBayar))?"LUNAS":"BELUM_LUNAS",
                   }
                    obj_arr.push(obj);
                }
            });

            $.each($('#tbl-tabungan').find('tbody tr'),function(key){
                if($(this).find('td input:checked').length){
                console.log(this.cells[3]);
                    obj ={
                        "id_siswa_iuran": selected_id,
                        "bulan_iuran": $(this)[0].cells[1].textContent,
                        "jumlah_iuran": $(this)[0].cells[2].textContent.replace(",",""),
                        "besar_iuran": this.cells[3].textContent.replace(",",""), //$(this)[0].cells[0].children[0].children[0].value,
                        "jenis_iuran": "TABUNGAN",
                        "tanggal_iuran": $(this)[0].cells[4].textContent,
                        "keterangan_iuran": $(this)[0].cells[5].textContent,
                        "status_iuran": parseInt($(this)[0].cells[2].textContent.replace(",",""))==(parseInt(this.cells[3].textContent.replace(",",""))+parseInt($(this)[0].cells[0].children[0].children[0].dataset.iuranTelahBayar))?"LUNAS":"BELUM_LUNAS"
                    }
                    obj_arr.push(obj);
                }
            });

            //save dpmp
            if($('[name=nominal_dpmp]').val() != "") {  
                obj ={
                    "id_siswa_iuran": selected_id,
                    // "id_siswa_kelas_iuran": selected_id_kelas,
                    "bulan_iuran": "",
                    "jumlah_iuran": "",
                    "besar_iuran": $('[name=nominal_dpmp]').val().replace(/,/g, ''),
                    "jenis_iuran": "DPMP",
                    "tanggal_iuran": null,
                    "keterangan_iuran": "",
                    "status_iuran": (parseInt($('[name=nominal_dpmp]').val().replace(/,/g, ''))+parseInt($('[name=nominal_dpmp]').data("telahDibayar")))==$('[name=nominal_dpmp]').data("harusDibayar")?"LUNAS":"BELUM_LUNAS"
                }
                if($('[name=nominal_dpmp]').val() != "") obj_arr.push(obj);
            }
            //save du
            var data_du = $('#form-du');
            $.each(data_du, function(key){
                console.log($(this).data('bulan_iuran'));
                obj ={
                    "id_siswa_iuran": selected_id,
                    // "id_siswa_kelas_iuran": selected_id_kelas,
                    "bulan_iuran": "", //$(this).data('bulan_iuran'),
                    "jumlah_iuran": "",
                    "besar_iuran": $('[name=nominal_du]').val().replace(/,/g, ''),
                    "jenis_iuran": $(this).data('jenis_iuran'),
                    "tanggal_iuran": null,
                    "keterangan_iuran": $(this).data('keterangan_iuran'),
                    "status_iuran": (parseInt($('[name=nominal_du]').val().replace(/,/g, ''))+parseInt($('[name=nominal_du]').data("telahDibayar")))==$('[name=nominal_du]').data("harusDibayar")?"LUNAS":"BELUM_LUNAS"
                }
                if($('[name=nominal_du]').val() != "") obj_arr.push(obj);
            });

            if(obj_arr.length>0) {
               save();
            }else{
                var resp_msg={"title" : "Tidak dapat menyimpan data", "body" : "Karena anda belum melakukan entri data apapun!", "icon"  : "warning"};
                showAlertMessage(resp_msg, 1200);
            } 
        });

       renderDatePicker();
    }

    function onGetSuccess(resp){
        var count_besar_dpmp=0;
        var count_sisa_dpmp=0;
        var count_besar_du=0;
        var count_sisa_du=0;
        var count_besar_iuran=0;
        var count_besar_tabungan=0;

        if(resp.data.data_siswa){
            var value_siswa = resp.data.data_siswa;
            var value_siswa_detil = resp.data.data_siswa_detil;
            var value_siswa_kelas = resp.data.data_siswa_kelas;
            var value_iuran = resp.data.data_iuran;
            var value_iuran_old = resp.data.data_tunggakan;
            var new_method = resp.data.new_method;

            var content="Karena siswa masih memiliki tunggakan iuran, harap siswa untuk melunasi terlebih dahulu, <a href='javascript:void(0)' onclick='readDaftarTunggakan();'>klik untuk melihat detaillnya.</a>";
            
            if(resp.data.data_tunggakan=="BELUM LUNAS" && tahun_ajaran=="" || resp.data.data_tunggakan=="DATA SISWA BARU BELUM ADA"){
                $('#lbl_tunggakan').removeClass('d-none');
                if(resp.data.data_tunggakan=="DATA SISWA BARU BELUM ADA"){
                    content="Karena data siswa belum lengkap, silahkan cek data siswa terkait, pastikan data pada tahun ajaran tersebut sudah ada. Pada menu Manajemen <a href='siswa_view'>Siswa</a>"
                }
                Swal.fire({
                  type: 'warning',
                  title: 'Tidak dapat melakukan Pembayaran.',
                  html: content,
                  //footer: '<a href>Why do I have this issue?</a>'
                  showCancelButton: false,
                  // showConfirmButton: false,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Tutup!'
                })
            }
            $('[name=filter_keyword]').val(value_siswa.ref_siswa);
            $('#lbl_no_ref').html(": "+value_siswa.ref_siswa);
            $('#lbl_nama_lengkap').html(": "+value_siswa.nama_lengkap_siswa);
            $('#lbl_tahun_angkatan').html(": "+value_siswa.tahun_angkatan_siswa);

            if(value_siswa_kelas.length>0){
                $.each(value_siswa_kelas, function(key){
                    console.log(this);
                    if(this.status_siswa_kelas){
                        console.log(this);
                        $('#lbl_kelas').html(": "+this.kelas);
                        $('#lbl_kelas').data('tingkat', this.tingkat_kelas);
                        $('#lbl_tahun_ajaran').html(": "+this.tahun_ajaran_siswa_kelas);
                        $('#lbl_tanggal_registrasi').html(": "+this.tanggal_registrasi_siswa_kelas);


                        selected_id_kelas=this.id_siswa_kelas;
                        
                        var date_now = new Date();
                        var tahun_ajaran = new Date();
                        //
                        $('#form-du').data('bulan_iuran', monthNameList[date_now.getMonth()]+" "+date_now.getFullYear());
                        $('#form-du').data('jenis_iuran', 'DU');
                        $('#form-du').data('keterangan_iuran', "OSIS"+" "+this.tahun_ajaran_siswa_kelas);
                    }
                });
            }else{
                $('#lbl_kelas').html(": "+value_siswa_kelas.kelas);
                $('#lbl_kelas').data('tingkat', value_siswa_kelas.tingkat_kelas);
                $('#lbl_tahun_ajaran').html(": "+value_siswa_kelas.tahun_ajaran_siswa_kelas);
                $('#lbl_tanggal_registrasi').html(": "+value_siswa_kelas.tanggal_registrasi_siswa_kelas);

                $('#form-du').data('bulan_iuran', value_siswa_kelas.tahun_ajaran+" - "+value_siswa_kelas.tingkat_kelas);
                $('#form-du').data('jenis_iuran', 'DU');
                $('#form-du').data('keterangan_iuran', "OSIS"+" "+value_siswa_kelas.tahun_ajaran_siswa_kelas);
            }


            if(resp.data.data_tunggakan=="BELUM LUNAS" && value_siswa_kelas[0].tahun_ajaran_siswa_kelas==tahun_ajaran && value_siswa_kelas[0].status_siswa_kelas=="1"){
                var tbody = $("#tbl-data").find('tbody');
                var value_data_granted=content;              
                tbody.html('<tr class="text-danger"><td colspan="10"><div class="list-group-item text-center"><span class="fas fa-warning"></span> <strong>'+value_data_granted+'</strong></div></td></tr>');
                $('[name=nominal_dpmp]').attr('disabled',true);
                 $('[name=nominal_du]').attr('disabled',true);
            }else{
                if(resp.data.data_tunggakan=="LUNAS" || resp.data.data_tunggakan==undefined || tahun_ajaran==value_siswa_kelas[0].tahun_ajaran_siswa_kelas){
                    $('#lbl_tunggakan').addClass('d-none');

                    $.each(value_siswa_detil,function(key){
                        if(this.jenis_iuran_siswa_detil=='DPMP') count_besar_dpmp=parseInt(this.besar_iuran_siswa_detil);
                        if(this.jenis_iuran_siswa_detil=='DU') count_besar_du=parseInt(this.besar_iuran_siswa_detil);
                        if(this.jenis_iuran_siswa_detil=='IURAN_PER_BULAN') count_besar_iuran=this.besar_iuran_siswa_detil;
                        if(this.jenis_iuran_siswa_detil=='TABUNGAN') count_besar_tabungan=this.besar_iuran_siswa_detil;
                    });

                    count_besar_iuran=new_method.besar.iuran_bulanan[0];
                    count_besar_du=new_method.besar.du[0];

                    $.each(value_iuran,function(key){
                        if(this.jenis_iuran=='DPMP') count_sisa_dpmp+=parseInt(this.besar_iuran);

                        if(this.id_siswa_kelas==value_siswa_kelas[0].id_siswa_kelas){
                            if(this.jenis_iuran=='DU') count_sisa_du+=parseInt(this.besar_iuran);
                        }
                    });

                    $('#tbl-data').find('tbody').html(renderDisplayIuran(value_iuran, count_besar_iuran, {"html" : $('#tbl-data'), "jenis_iuran" : "IURAN_PER_BULAN"}));
                    $('#tbl-tabungan').find('tbody').html(renderDisplayIuran(value_iuran, count_besar_tabungan, {"html" : $('#tbl-tabungan'), "jenis_iuran" : "TABUNGAN"}));
                    renderDatePicker();

                    // //dpmp
                    $('#lbl_besar_dpmp').html(": "+$.number(count_besar_dpmp,2));
                    $('#lbl_sisa_dpmp').html(": "+$.number((count_besar_dpmp-count_sisa_dpmp),2));     
                    $('[name=nominal_dpmp]').data("telahDibayar", count_sisa_dpmp);      
                    $('[name=nominal_dpmp]').data("harusDibayar", count_besar_dpmp);
                    // //du
                    $('#lbl_besar_du').html(": "+$.number(count_besar_du,2));
                    $('#lbl_sisa_du').html(": "+$.number((count_besar_du-count_sisa_du),2));    
                    $('[name=nominal_du]').data("telahDibayar", count_sisa_du);      
                    $('[name=nominal_du]').data("harusDibayar", count_besar_du);
                    
                    if(count_besar_du-count_sisa_du == 0) {
                        $('#lbl_du_status').html("LUNAS "+'<i class="fas text-dark fa-check-circle"></i>');
                        $('#lbl_du_status').addClass("text-dark");
                        $('[name=nominal_du]').attr('disabled',true);
                    }else{
                        $('#lbl_du_status').html("PROSES "+'<i class="fas text-dark fa-clock"></i>');
                        $('#lbl_du_status').addClass("text-dark");
                    }
                    if(count_besar_dpmp-count_sisa_dpmp == 0) {
                        $('#lbl_dpmp_status').html("LUNAS "+'<i class="fas text-dark fa-check-circle"></i>');
                        $('#lbl_dpmp_status').addClass("text-dark");
                        $('[name=nominal_dpmp]').attr('disabled',true);
                    }else{
                        $('#lbl_dpmp_status').html("PROSES "+'<i style="color:rgb(69, 125, 157)!important" class="fas fa-clock"></i>');
                        $('#lbl_dpmp_status').attr("style","color:rgb(69, 125, 157)!important");
                    }

                }
                $('#collapse-form-dpmp').collapse('show');
                $('#form-du').collapse('show');
            }
        }
    }

    function renderButton(){
        $(function() {
            $('#btn-save-row-modal').click(function(){
                saveRowModal();
            });
        });
    }
    function renderDatePicker(){
         $(function() {
          $('.my-picker').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: parseInt(moment().format('YYYY'),10)-4,
            maxYear: parseInt(moment().format('YYYY'),10),
            "locale": {
                "format": "YYYY-MM-DD",
            }
          }, function(start, end, label) {
            // var years = moment().diff(start, 'years');
            // alert("You are " + years + " years old!");
            $(this).val(start.format('DD-MMM-YYYY'));
            // picker.autoUpdateInput = true;
            // $(this).change();
          });
        });
    }
    function readHistoryPayment(elem){
        if(selected_id != '') {
            var param = "";
            if(elem.parentElement.dataset.jenisIuran != "IURAN_PER_BULAN" && elem.parentElement.dataset.jenisIuran != "TABUNGAN"){
                // alert(elem.parentElement.dataset.jenisIuran);
                param = "?filter_jenis_iuran="+elem.parentElement.dataset.jenisIuran;
            }else{
                // alert(elem.parentElement.dataset.jenisIuran);
                // alert(elem.parentElement.parentElement.dataset.bulan_iuran);
                param = "?filter_jenis_iuran="+elem.parentElement.dataset.jenisIuran+"&filter_bulan_iuran="+elem.parentElement.parentElement.dataset.bulan_iuran;
            }
            // alert(param);
            ajaxGET(path + '/get_by_id/'+selected_id+param,'onGetReadHistoryPayment','onGetError');
        }
    }
    function onGetReadHistoryPayment(resp){
        if(resp.data.data_iuran.length==0) {
            alert("Tidak ditemukan Riwayat Iuran");
            return;
        }
        console.log(resp);
        if(resp.data.data_siswa){
            var rows='';
            var no=1;

            var sum_besar_iuran=0;
            var besar_iuran=resp.data.new_method.besar.iuran_bulanan;
            $.each(resp.data.data_iuran,function(key){
                //if(this.jenis_iuran==jenis_iuran && jenis_iuran=="DPMP"){
                    rows+='<tr data-key="'+no+'" data-id="'+this.id_iuran+'" data-id-siswa="'+this.id_siswa_iuran+'" data-tingkat-kelas="'+this.tingkat_kelas+'" data-jenis-iuran="'+this.jenis_iuran+'" class="bg-light" style="opacity: 0.9;">';
                      rows+='<td width="5%">'+no+'</td>';
                      // rows+='<td>'+this.jatuh_tempo_iuran+'('+this.jenis_iuran+')</td>';
                      // rows+='<td class="text-left">-</td>';
                      rows+='<td class="text-left" width="15%">'+this.no_iuran+'</td>';
                      rows+='<td data-old="'+this.tanggal_iuran+'" class="text-left" width="20%">'+this.tanggal_iuran+'</td>';
                      rows+='<td data-old="'+parseInt(this.besar_iuran)+'" class="text-left" width="15%">'+$.number(this.besar_iuran)+'</td>';
                      rows+='<td data-old="'+this.catatan_iuran+'" class="text-left" width="30%">'+this.catatan_iuran+'</td>';
                      rows+='<td align="center" width="5%"><a href="javascript:void(0)" onclick="editRowModal(this);"><i class="fas text-warning fa-pencil-alt"></i></a></td>';
                    rows+='</tr>';
                    no++;
                //}
                sum_besar_iuran+=parseInt(this.besar_iuran);
            });



            // rows='';
            var title_iuran="";
            if(resp.data.data_iuran.length > 0){
                if(resp.data.data_iuran[0].jenis_iuran=="IURAN_PER_BULAN")
                    title_iuran=resp.data.data_iuran[0].bulan_iuran;
                else
                    title_iuran=resp.data.data_iuran[0].jenis_iuran;
            }
            //besar_iuran-sum_besar_iuran
            var icon='<i class="fas fa-check-circle text-success"></i>';
            if(besar_iuran-sum_besar_iuran>0){
                icon='<i class="fas fa-clock text-warning"></i>';
            }
            var table="";
            table+='<h4 class="text-left" style="font-family: segoeui;">'+icon+' Detail Riwayat Iuran : '+title_iuran+' </h4>';
            table+='<div id="alert-msg" class="alert alert-danger text-left" role="alert" style="font-family: segoeuib; display:none"></div>';
            //
            table+='<table class="table table-sm table-hovered table-pills m-0">';
              table+='<thead class="text-white" style="background-color: #487d95!important;font-family: segoeui;">';
                table+='<tr>';
                  table+='<th scope="col" class="" width="5%">#</th>';
                  table+='<th scope="col" class="text-left" width="15%">No.Iuran</th>';
                  table+='<th scope="col" class="text-left" width="20%">Tgl.Iuran</th>';
                  table+='<th scope="col" class="text-left" width="15%">Besar Iuran</th>';
                  table+='<th scope="col" class="text-left" width="30%">Keterangan</th>';
                  table+='<th scope="col" class="text-center" width="5%">#</th>';
                table+='</tr>';
              table+='</thead>';
            table+='</table>';
            //
            table+='<div style="max-height:250px;overflow-x: scroll;">';
            table+='<table id="tbl-data-modal" class="table table-sm table-hovered table-pills">';
                table+='<tbody>'+rows+'</tbody>';
            table+='</table></div>';
            //table+='<p class="text-left" style="font-family: segoeui;">Sisa Iuran : <span style="font-family: segoeuib;">'+$.number(250000)+'</span></p>'
            //table+='<hr/>';
            table+='<div class="row">';
                table+='<div class="col-md-6">';
                    table+='<div class="row">';
                    table+='<div class="col-md-12">';
                        table+='<p class="text-left" style="font-family: segoeui;">Besar Iuran : <span style="font-family: segoeuib;">'+$.number(besar_iuran)+'</span></p>';
                        table+='<p class="text-left" style="font-family: segoeui;">Sisa Iuran : <span style="font-family: segoeuib;">'+$.number(besar_iuran-sum_besar_iuran)+'</span></p>';
                    table+='</div>';
                    table+='</div>';
                table+='</div>';
                table+='<div class="col-md-6">';
                    table+='<button id="btn-save-row-modal" class="btn btn-primary float-right shadow-none m-1" type="button">SUBMIT</button>';
                    table+='<button id="btn-reset-row-modal" class="btn btn-danger float-right shadow-none m-1" type="button">RESET</button>';
                table+='</div>';
            table+='</div>';
            // table+='<div class="row">';
            //     table+='<div class="col-md-12">';
            //         table+='<button class="btn btn-primary">OK</button>';
            //     table+='</div>';
            // table+='</div>';
            
            Swal.fire({
              // title: 'Riwayat Pembayaran',
              width: '60%',
              html: table,
              showConfirmButton: false,
              showCloseButton: true,
            });
            //
            renderButton();
            //
        }
    }



    function readDaftarTunggakan(jenis=''){
        if(selected_id != '') {
            ajaxGET(path + '/check_tunggakan/'+selected_id+'?check_tunggakan='+true,'onGetReadDaftarTunggakan','onGetError');
            // ajaxGET(path + '/get_by_id/'+selected_id+'?check_tunggakan='+true,'onGetReadDaftarTunggakan','onGetError');
        }
    }
    function getTahunAjaranAll(){
        var strdate=[];
        var angkatan= new Date("July 10, 2017");
        // var date = new Date("December 10, 2019");//+tahun_ajaran[0]
        var date = new Date();
        var selisih=(date.getFullYear()-angkatan.getFullYear());
        for(var start_ajaran=angkatan.getFullYear(); start_ajaran < angkatan.getFullYear()+selisih; start_ajaran++){
            if(start_ajaran+"/"+(start_ajaran+1)!=date){
                strdate.push(start_ajaran+"/"+(start_ajaran+1).toString());
            }
        }
        return strdate;
    }
    function getTahunAjaranAllAndSet(tahun_angkatan){
        var strdate=[];
        var angkatan= new Date("July 10, "+tahun_angkatan);
        // var date = new Date("December 10, 2019");//+tahun_ajaran[0]
        var date = new Date();
        var selisih=(date.getFullYear()-angkatan.getFullYear());
        for(var start_ajaran=angkatan.getFullYear(); start_ajaran < angkatan.getFullYear()+selisih; start_ajaran++){
            if(start_ajaran+"/"+(start_ajaran+1)!=date){
                strdate.push(start_ajaran+"/"+(start_ajaran+1).toString());
            }
        }
        return strdate;
    }

    function onGetReadDaftarTunggakan(resp){
        console.log(resp);
        var tunggakan = resp.data.new_method.tunggakan;

        var rows='';var no=1;
        $.each(tunggakan, function(key){
            console.log(this);
            rows+='<tr class="bg-light" style="opacity: 0.9;">';
                rows+='<td>'+no+'</td>';
                rows+='<td>'+this.tahun_ajaran+'</td>';
                rows+='<td class="text-left">'+$.number(this.sisa_spp)+'</td>';
                // rows+='<td class="text-left">'+$.number(total_dpmp-sum_dpmp)+'</td>';
                rows+='<td class="text-left">'+$.number(this.sisa_du)+'</td>';

                if(this.sisa_spp==0 && this.sisa_du==0){
                    rows+='<td class=""><i class="fas text-success fa-check-circle"></i> LUNAS</td>';
                }else{
                    rows+='<td class=""><i class="fas text-danger fa-times-circle"></i> BELUM LUNAS</td>';
                }
                rows+='<td align="center"><a href="?id='+selected_id+'&tahun_ajaran='+this.tahun_ajaran+'"><i class="fas text-info fa-eye"></i></a></td>';
            rows+='</tr>';
            no++;
        });

        var table="<hr/>";
        table+='<div class=" table-responsive">';
        table+='<table id="tbl-data" class="table table-sm table-hovered table-pills">';
          table+='<thead class="text-white" style="background-color: #487d95!important;font-family: segoeui;">';
            table+='<tr>';
              table+='<th scope="col" class="p-3">No.</th>';
              table+='<th scope="col" class="p-3">Tahun Ajaran</th>';
              table+='<th scope="col" class="p-3">IURAN BULANAN</th>';
              // table+='<th scope="col" class="p-3">DPMP</th>';
              table+='<th scope="col" class="p-3">OSIS</th>';
              table+='<th scope="col" class="p-3">Keterangan</th>';
              table+='<th scope="col" class="text-center p-3">#</th>';
            table+='</tr>';
          table+='</thead>';
          table+='<tbody>'+rows+'</tbody>';
        table+='</table>';
        table+='</div>';

        Swal.fire({
          title: 'Riwayat Iuran',
          width: '60%',
          html: table,
          showCloseButton: true,
          showCancelButton: true,
          focusConfirm: false
        });
        // }

    }

    function onGetError(resp){
        console.log(resp);
        //swal("Terjadi Kesalahan", "Mohon periksa koneksi internet anda!", "warning");
        var title="Terjadi Kesalahan";
        var text="Mohon periksa koneksi internet anda!";
        if(resp.responseJSON.code==400){
            title="Mohon Maaf";
            text=resp.responseJSON.message;
        }

        Swal.fire({
          type: 'warning',
          title: title,
          text: text,//'Mohon periksa koneksi internet anda!',
          animation: false,
          customClass: 'animated tada'
        });
    }

    // function renderIuran(mode){
    //     ajaxGET_NEW(path + '/get_by_id/'+selected_id+'?tahun_ajaran='+tahun_ajaran, function(resp){

    //     },'onGetError');
    // }

    function displaySiswa(){
        params = $( "#search-form" ).serialize();
        ajaxGET(path + '/list_table?page='+1+'&'+params,'onDisplaySiswaSuccess','onGetListError');
    }
    function onDisplaySiswaSuccess(resp){
        console.log(resp);
        console.log(resp.data.data_siswa);

            
        var count_besar_dpmp=0;
        var count_sisa_dpmp=0;
        var count_besar_du=0;
        var count_sisa_du=0;
        var count_besar_iuran=0;

        if(resp.data.data_siswa){
            var value_siswa = resp.data.data_siswa[0];
            window.location.href="iuran_view?id="+value_siswa.id_siswa;
            // window.location.href="iuran_view?id="+value_siswa.id_siswa_kelas;
        }else{
            $('#lbl_no_ref').html(":");
            $('#lbl_nama_lengkap').html(":");
            $('#lbl_kelas').html(":");
            $('#lbl_tahun_ajaran').html(":");
            $('#lbl_tahun_angkatan').html(":");
            var resp_msg={"title" : "Data tidak ditemukan", "body" : "Mohon periksa kembali NO.REF anda", "icon"  : "warning"};
            showAlertMessage(resp_msg, 1200);
            // swal("Data tidak ditemukan","Mohon periksa kembali NO.REF anda","warning");
            // renderDisplayIuranBulanan();
            renderDisplayIuran();
            // //dpmp
            $('#lbl_besar_dpmp').html(": "+$.number(count_besar_dpmp,2));
            $('#lbl_sisa_dpmp').html(": "+$.number((count_besar_dpmp-count_sisa_dpmp),2));       
            // //du
            $('#lbl_besar_du').html(": "+$.number(count_besar_du,2));
            $('#lbl_sisa_du').html(": "+$.number((count_besar_du-count_sisa_du),2));  
        }
    }

    function get_tahun_ajaran_sekarang(){
        var tahun_ajaran = $('#lbl_tahun_ajaran').text().substring(2).split("/");
        var date = new Date("July 10, "+tahun_ajaran[0]);
        var endDate = new Date("June 10, "+tahun_ajaran[1]);
        var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        var new_month=[];
        for (var i=date; i <= endDate; date.setMonth(date.getMonth() + 1)){
            var stringDate = monthNameList[date.getMonth()] + " " + date.getFullYear();
            var jatuh_tempo = date.getFullYear() + "-" + ((date.getMonth()+1).toString().length==1?"0"+(date.getMonth()+1):date.getMonth()+1) + "-" + date.getDate();  
            // console.log(new Date(monthNameList[date.getMonth()]+" 10, "+date.getFullYear()));
            new_month.push(new Date(monthNameList[date.getMonth()]+" 10, "+date.getFullYear()));
        }
        return new_month;
    }
    function get_tahun_ajaran(string_date){
        var tahun_ajaran = string_date.split("/");
        var date = new Date("July 10, "+tahun_ajaran[0]);
        var endDate = new Date("June 10, "+tahun_ajaran[1]);
        var monthNameList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        var new_month=[];
        for (var i=date; i <= endDate; date.setMonth(date.getMonth() + 1)){
            var stringDate = monthNameList[date.getMonth()] + " " + date.getFullYear();
            var jatuh_tempo = date.getFullYear() + "-" + ((date.getMonth()+1).toString().length==1?"0"+(date.getMonth()+1):date.getMonth()+1) + "-" + date.getDate();  
            // console.log(new Date(monthNameList[date.getMonth()]+" 10, "+date.getFullYear()));
            new_month.push(new Date(monthNameList[date.getMonth()]+" 10, "+date.getFullYear()));
        }
        return new_month;
    }

    function renderDisplayIuranBulanan(res_data,count_besar_iuran){
        console.log(res_data);
        var tbody = $("#tbl-data").find('tbody');
        var new_month=get_tahun_ajaran_sekarang();
        var tgl_regis= $('#lbl_tanggal_registrasi').text().split("-")[1]-1;
        var row="";
        var new_arr=[];
        var key=1;
        $.each(new_month, function(key_month, date){
            new_arr.push(date.getMonth());
            stringDate = monthNameList[date.getMonth()] + " " + date.getFullYear();
            jatuh_tempo = date.getFullYear() + "-" + ((date.getMonth()+1).toString().length==1?"0"+(date.getMonth()+1):date.getMonth()+1) + "-" + date.getDate();  
            var month = monthNameList[date.getMonth()];
            var count_same=0;

            var active="";
            var besar_iuran=0;
            var jumlah_iuran=count_besar_iuran;
            var tanggal_iuran="-";
            var keterangan_iuran="";
            var user_added="";
            var status_iuran=0;

            $.each(res_data,function(keys, value){
                if(value.bulan_iuran==stringDate && value.jenis_iuran=="IURAN_PER_BULAN"){
                    active="bg-light";
                    tanggal_iuran=value.tanggal_iuran;
                    keterangan_iuran=value.keterangan_iuran;
                    user_added=value.user_added;

                    if(count_same>0){
                        besar_iuran+=parseInt(value.besar_iuran);
                        jumlah_iuran=parseInt(value.jumlah_iuran);
                    }else{
                        besar_iuran=parseInt(value.besar_iuran);
                        jumlah_iuran=parseInt(value.jumlah_iuran);
                    }
                    console.log(count_same);
                    count_same++;
                }
            });

            var status_iuran = parseInt(jumlah_iuran) - parseInt(besar_iuran);
            row+='<tr id=row-'+key_month+' data-bulan_iuran="'+stringDate+'" class="'+active+'">';
                if(new_arr.indexOf(tgl_regis) == -1){
                    row+='<td align="center">';
                        row+='<span class="small">-</span>';
                    row+='</td>';
                }else{
                    if(status_iuran==0){
                        row+='<td align="center"><i class="fas text-success fa-check-circle"></i></td>';
                    }
                    else{
                        row+='<td align="center">';
                            row+='<div class="form-check">';
                                row+='<input id="besar_iuran_'+key_month+'" class="form-check-input" type="checkbox" value='+(parseInt((jumlah_iuran-besar_iuran)))+' name="tekan_saja" onclick="setIuranPerBulan(this,\''+key_month+'\')" required>';
                            row+='</div>';
                        row+='</td>';
                    }
                }

                row+='<td>'+stringDate+'</td>';
                row+='<td>'+$.number(jumlah_iuran)+'</td>';
                
                if(status_iuran==0) row+='<td>'+$.number(besar_iuran)+'</td>';
                else{
                    if(new_arr.indexOf(tgl_regis) == -1){
                        row+='<td><span class="">0</span></td>';
                    }else{
                        row+='<td id="val_besar_iuran_'+key_month+'" contenteditable="'+false+'" onkeyup="javascript:setIuranPerBulans(this,\''+key_month+'\');"><input style="border-radius:0px!important;" type="number" max='+jumlah_iuran+' value='+(jumlah_iuran-besar_iuran)+'></td>';
                    }
                }

                if(new_arr.indexOf(tgl_regis) == -1){
                    row+='<td><span class="">-</span></td>';
                    row+='<td><span class="">Tidak dipungut biaya</span></td>';
                    row+='<td><span class=""><strong>By</strong>: Sistem</span></td>';
                }else{
                    row+='<td>'+tanggal_iuran+'</td>';
                    row+='<td>'+keterangan_iuran+'</td>';
                    row+='<td>'+user_added+'</td>';
                }
            row+='<tr>';
        });
        tbody.html(row);
    }

    function renderDisplayIuran(res_data,count_besar_iuran, elem){
        console.log(elem);
        console.log(res_data);
        var tbody = elem.html.find('tbody');
        var new_month=get_tahun_ajaran_sekarang();
        var tgl_regis= $('#lbl_tanggal_registrasi').text().split("-")[1]-1;
        var row="";
        var new_arr=[];
        var new_data=[];

        var key=1;
        $.each(new_month, function(key_month, date){
            new_arr.push(date.getMonth());
            new_data.push(
                {
                    "string_date" : monthNameList[date.getMonth()] + " " + date.getFullYear(),
                    "jatuh_tempo" :date.getFullYear() + "-" + ((date.getMonth()+1).toString().length==1?"0"+(date.getMonth()+1):date.getMonth()+1) + "-" + date.getDate(),
                    "active" : "",
                    "besar_iuran" : 0,
                    "jumlah_iuran" : count_besar_iuran,
                    "tanggal_iuran" : "",
                    "keterangan_iuran" : "",
                    "user_added" : "",
                    "status_iuran" : 0,
                    "kelas_iuran" : "",
                    "content_editable" : false,
                    "action" : "",
                }
            );

            // var stringDate = monthNameList[date.getMonth()] + " " + date.getFullYear();
            // var jatuh_tempo = date.getFullYear() + "-" + ((date.getMonth()+1).toString().length==1?"0"+(date.getMonth()+1):date.getMonth()+1) + "-" + date.getDate();  
            // var active="";
            // var besar_iuran=0;
            // var jumlah_iuran=count_besar_iuran;
            // var tanggal_iuran="-";
            // var keterangan_iuran="";
            // var user_added="";
            // var status_iuran=0;
            // var kelas_iuran="";
            // var contenteditable=false;
            // var action="";

            // var count_same=0;
            // $.each(res_data,function(keys, value){
            //     if(value.bulan_iuran==stringDate && value.jenis_iuran==elem.jenis_iuran){
                    
            //         active="bg-light";
            //         tanggal_iuran=value.tanggal_iuran;
            //         keterangan_iuran=value.keterangan_iuran;
            //         user_added=value.user_added_iuran;
            //         status_iuran=value.status_iuran;
            //         contenteditable=true;
            //         if(count_same>0){
            //             besar_iuran+=parseInt(value.besar_iuran);
            //             // jumlah_iuran=parseInt(value.jumlah_iuran);
            //         }else{
            //             besar_iuran=parseInt(value.besar_iuran);
            //             // jumlah_iuran=parseInt(value.jumlah_iuran);
            //         }
            //         console.log(count_same);
            //         count_same++;
            //         action='<i onclick="editRow(this);" class="fas text-warning fa-eye"></i>';
                    
            //     }
            // });
            
            /*
            var status_iuran = (status_iuran=="LUNAS"?0:1);
            row+='<tr id=row-'+key_month+' data-bulan_iuran="'+stringDate+'" class="'+active+'">';

                if(new_arr.indexOf(tgl_regis) == -1){
                    row+='<td align="center"><span class="small">-</span></td>';
                }else{
                    if(status_iuran==0){
                        row+='<td align="center"><i class="fas text-success fa-check-circle"></i></td>';
                    }else{
                        row+='<td align="center">';
                            row+='<div class="form-check">';
                                row+='<input data-jenis-iuran="'+elem.jenis_iuran+'" data-iuran-telah-bayar="'+besar_iuran+'" id="'+elem.jenis_iuran+'_'+key_month+'" class="form-check-input" type="checkbox" value='+(parseInt((jumlah_iuran-besar_iuran)))+' name="'+elem.jenis_iuran+'_tekan_saja" onclick="setIuranPerBulan(this,\''+key_month+'\')" required>';
                            row+='</div>';
                        row+='</td>';
                    }
                }

                row+='<td>'+stringDate+'</td>';

                if(status_iuran==1)
                    row+='<td data-value="'+jumlah_iuran+'">'+$.number(jumlah_iuran)+'</td>';
                else 
                    row+='<td>'+$.number(besar_iuran)+'</td>';
                
                if(status_iuran==0) row+='<td>'+$.number(besar_iuran)+'</td>';
                else{
                    if(new_arr.indexOf(tgl_regis) == -1){
                        row+='<td><span class="">0</span></td>';
                    }else{
                        row+='<td id="val_'+elem.jenis_iuran+'_'+key_month+'" contenteditable="'+false+'" onkeyup="javascript:setIuranPerBulans(this,\''+key_month+'\');"><input style="border-radius:0px!important;" type="number" max='+(jumlah_iuran-besar_iuran)+' value='+(jumlah_iuran-besar_iuran)+'></td>';
                    }
                }

                if(new_arr.indexOf(tgl_regis) == -1){
                    row+='<td><span class="">-</span></td>';
                    row+='<td><span class="">Tidak dipungut biaya</span></td>';
                    row+='<td><span class=""><strong>By</strong>: Sistem</span></td>';
                }else{
                    if(status_iuran==0){
                        row+='<td>'+tanggal_iuran+'</td>';
                        row+='<td contenteditable="'+(status_iuran==1?true:false)+'" id="val_'+elem.jenis_iuran+'_'+'keterangan_iuran_'+key_month+'">'+keterangan_iuran+'</td>';
                        
                    }
                    else{
                        row+="<td><input class='rounded-0 my-picker' type='text' ></td>";
                        row+='<td contenteditable="'+(status_iuran==1?true:false)+'" id="val_'+elem.jenis_iuran+'_'+'keterangan_iuran_'+key_month+'">'+"<input style='border-radius:0px!important;' type='text' value=''>"+'</td>';
                    }
                    row+='<td>'+user_added+'</td>';
                }

                if(status_iuran==0){
                    row+='<td data-jenis-iuran="'+elem.jenis_iuran+'"><i onclick="editRow(this);" class="fas text-warning fa-eye"></i></td>';
                }else{
                    row+='<td data-jenis-iuran="'+elem.jenis_iuran+'">'+action+'</td>';
                }

            row+='<tr>';
            */

        });

        $.each(new_data,function(keys, value){
            var count_same=0;
            $.each(res_data,function(){
                if(value.string_date==this.bulan_iuran && elem.jenis_iuran==this.jenis_iuran){
                    
                    value.active="bg-light";
                    value.tanggal_iuran=this.tanggal_iuran;
                    value.keterangan_iuran=this.keterangan_iuran;
                    value.user_added=this.user_added_iuran;
                    value.status_iuran=this.status_iuran;
                    value.contenteditable=true;
                    
                    if(count_same>0){
                        value.besar_iuran+=parseInt(this.besar_iuran);
                    }else{
                        value.besar_iuran=parseInt(this.besar_iuran);
                    }

                    count_same++;
                    
                    value.action='<i onclick="editRow(this);" class="fas text-warning fa-eye"></i>';
                    
                }
            });

            var status_iuran = 1;
            if(parseInt(value.jumlah_iuran)-value.besar_iuran==0 || value.status_iuran=="LUNAS"){
                status_iuran=0
            }else{
                status_iuran=1;
            }

            row+='<tr id=row-'+keys+' data-bulan_iuran="'+value.string_date+'" class="'+value.active+'">';

                if(new_arr.indexOf(tgl_regis) == -1){
                    row+='<td align="center"><span class="small">-</span></td>';
                }else{
                    if(status_iuran==0){
                        row+='<td align="center"><i class="fas text-success fa-check-circle"></i></td>';
                    }else{
                        row+='<td align="center">';
                            row+='<div class="form-check">';
                                row+='<input data-jenis-iuran="'+elem.jenis_iuran+'" data-iuran-telah-bayar="'+value.besar_iuran+'" id="'+elem.jenis_iuran+'_'+keys+'" class="form-check-input" type="checkbox" value='+(parseInt((value.jumlah_iuran-value.besar_iuran)))+' name="'+elem.jenis_iuran+'_tekan_saja" onclick="setIuranPerBulan(this,\''+keys+'\')" required>';
                            row+='</div>';
                        row+='</td>';
                    }
                }

                row+='<td>'+value.string_date+'</td>';

                if(value.status_iuran==1)
                    row+='<td data-value="'+value.jumlah_iuran+'">'+$.number(value.jumlah_iuran)+'</td>';
                else 
                    row+='<td>'+$.number(value.jumlah_iuran)+'</td>';
                
                if(status_iuran==0) row+='<td>'+$.number(value.besar_iuran)+'</td>';
                else{
                    if(new_arr.indexOf(tgl_regis) == -1){
                        row+='<td><span class="">0</span></td>';
                    }else{
                        row+='<td id="val_'+elem.jenis_iuran+'_'+keys+'" contenteditable="'+false+'" onkeyup="javascript:setIuranPerBulans(this,\''+keys+'\');"><input style="border-radius:0px!important;" type="number" max='+(parseInt(value.jumlah_iuran)-value.besar_iuran)+' value='+(parseInt(value.jumlah_iuran)-value.besar_iuran)+'></td>';
                    }
                }

                if(new_arr.indexOf(tgl_regis) == -1){
                    row+='<td><span class="">-</span></td>';
                    row+='<td><span class="">Tidak dipungut biaya</span></td>';
                    row+='<td><span class=""><strong>By</strong>: Sistem</span></td>';
                }else{
                    if(status_iuran==0){
                        row+='<td>'+value.tanggal_iuran+'</td>';
                        row+='<td contenteditable="'+(status_iuran==1?true:false)+'" id="val_'+elem.jenis_iuran+'_'+'keterangan_iuran_'+keys+'">'+value.keterangan_iuran+'</td>';
                    }else{
                        row+="<td><input class='rounded-0 my-picker' type='text' ></td>";
                        row+='<td contenteditable="'+(status_iuran==1?true:false)+'" id="val_'+elem.jenis_iuran+'_'+'keterangan_iuran_'+keys+'">'+"<input style='border-radius:0px!important;' type='text' value=''>"+'</td>';
                    }
                    row+='<td>'+value.user_added+'</td>';
                }

                if(status_iuran==0){
                    row+='<td data-jenis-iuran="'+elem.jenis_iuran+'"><i onclick="editRow(this);" class="fas text-warning fa-eye"></i></td>';
                }else{
                    row+='<td data-jenis-iuran="'+elem.jenis_iuran+'">'+value.action+'</td>';
                }

            row+='<tr>';
        });

        console.log(new_data);
        return row;
    }

    function editRow(elem){
        /*console.log(elem.parentElement.parentElement.cells[3]);
        var new_elem = elem.parentElement.parentElement;
        var new_date = new_elem.cells[4].innerText.split(" ");
        new_date = new_date[0].split("-");
        var row="";
        row+='<div class="form-check">';
            row+='<input data-jumlah-iuran="'+new_elem.cells[3].innerText+'" data-tanggal-iuran="'+new_elem.cells[4].innerText+'" data-keterangan-iuran="'+new_elem.cells[5].innerText+'" id="'+(new_elem.id).replace("row-","IURAN_PER_BULAN_")+'" class="form-check-input" type="checkbox" value='+(new_elem.cells[3].innerText.replace(",",""))+' name="IURAN_PER_BULAN_tekan_saja" onclick="setIuranPerBulan(this,\''+(new_elem.id).replace("row-","")+'\')" required>';
        row+='</div>';
        new_elem.cells[0].innerHTML=row;
        new_elem.cells[3].outerHTML="<td id='"+(new_elem.id).replace("row-","val_IURAN_PER_BULAN_")+"'><input  style='border-radius:0px!important;' type='number' value='"+new_elem.cells[3].innerText.replace(",","")+"'>";
        new_elem.cells[4].innerHTML="<input class='rounded-0 my-pickers' type='text' value='"+(new_date[1]+"/"+new_date[2]+"/"+new_date[0])+"'>";
        new_elem.cells[5].innerHTML="<input style='border-radius:0px!important;' type='text' value='"+new_elem.cells[5].innerText+"'>";
        new_elem.cells[7].innerHTML='<i onclick="editRow(this);" class="fa fa-times-circle text-danger"></i>';
        
        renderDatePicker();*/
        readHistoryPayment(elem);
    }

    function editRowModal(elem){
        console.log(elem.parentElement.parentElement.cells[3]);
        var new_elem = elem.parentElement.parentElement;
        // console.log(new_elem);
        var new_date = new_elem.cells[2].dataset.old.split(" ");
        // new_date = new_date[0].split("-");
        var row="";
        row+='<div class="form-check">';
            // row+='<input data-jumlah-iuran="'+new_elem.cells[3].innerText+'" data-tanggal-iuran="'+new_elem.cells[2].innerText+'" data-keterangan-iuran="'+new_elem.cells[4].innerText+'" id="'+(new_elem.id).replace("row-","IURAN_PER_BULAN_")+'" class="form-check-input" type="checkbox" value='+(new_elem.cells[3].innerText.replace(",",""))+' name="IURAN_PER_BULAN_tekan_saja" onclick="setIuranPerBulan(this,\''+(new_elem.id).replace("row-","")+'\')" required>';
            row+='<input checked data-jumlah-iuran="'+new_elem.cells[3].innerText+'" data-tanggal-iuran="'+new_elem.cells[2].innerText+'" data-keterangan-iuran="'+new_elem.cells[4].innerText+'" id="'+(new_elem.id).replace("row-","IURAN_PER_BULAN_")+'" class="form-check-input" type="checkbox" value='+(new_elem.cells[3].innerText.replace(",",""))+' name="IURAN_PER_BULAN_tekan_saja" onclick="setIuranPerBulan(this,\''+(new_elem.id).replace("row-","")+'\')" required>';
        row+='</div>';
        new_elem.cells[0].innerHTML=row;
        new_elem.cells[2].innerHTML="<input data-value='"+(new_date[0])+"' class='rounded-0 w-100 my-picker' type='text' value='"+(new_date[0])+"'>";
        // new_elem.cells[3].outerHTML="<td class='text-left' id='"+(new_elem.id).replace("row-","val_IURAN_PER_BULAN_")+"'><input data-value="+(new_elem.cells[3].innerText)+" class='rounded-0 w-100' type='number' value='"+new_elem.cells[3].innerText.replace(",","")+"'>";
        new_elem.cells[3].innerHTML="<input class='rounded-0 w-100' type='number' value='"+new_elem.cells[3].dataset.old.replace(",","")+"'>";
        new_elem.cells[4].innerHTML="<input class='rounded-0 w-100' type='text' value='"+new_elem.cells[4].dataset.old+"'>";
        new_elem.cells[5].innerHTML='<a href="javascript:void(0)" onclick="resetRowModal(this);"><i class="fa fa-times-circle text-danger"></i></a>';
        
        renderDatePicker();
    }
    function resetRowModal(elem){
        var new_elem = elem.parentElement.parentElement;
        var new_date = new_elem.cells[2].dataset.old;//.split(" ");

        new_elem.cells[0].innerHTML=new_elem.dataset.key;
        new_elem.cells[2].innerHTML=new_date;
        new_elem.cells[3].innerHTML=$.number(new_elem.cells[3].dataset.old);//.replace(",","");
        new_elem.cells[4].innerHTML=new_elem.cells[4].dataset.old;
        new_elem.cells[5].innerHTML='<a href="javascript:void(0)" onclick="editRowModal(this);"><i class="fa fa-pencil-alt text-warning"></i></a>';
        // alert("Try Save");
    }
    function saveRowModal(){
        var obj_new_arr = []; 
        var obj ={}
        var total_iuran=0;
        $.each($('#tbl-data-modal').find('tbody tr'),function(key){
            if($(this).find('td input:checked').length){
                obj ={
                    "id_iuran": this.dataset.id,
                    "jenis_iuran": this.dataset.jenisIuran,
                    "besar_iuran": this.cells[3].children[0].value.replace(",",""),//$(this)[0].cells[0].children[0].children[0].value,
                    "tanggal_iuran": this.cells[2].children[0].value,
                    //"keterangan_iuran": this.cells[4].children[0].value,
                    "catatan_iuran": this.cells[4].children[0].value,
                    "status_iuran": ''//$(this)[0].cells[2].textContent.replace(",","")==(parseInt(this.cells[3].textContent.replace(",",""))+parseInt($(this)[0].cells[0].children[0].children[0].dataset.iuranTelahBayar))?"LUNAS":"BELUM_LUNAS"
                }
                obj_new_arr.push(obj);
                total_iuran+=parseInt(obj.besar_iuran);
            }else{
                obj ={
                    "id_iuran": this.dataset.id,
                    "jenis_iuran": this.dataset.jenisIuran,
                    "besar_iuran": this.cells[3].innerText.replace(",",""),//$(this)[0].cells[0].children[0].children[0].value,
                    "tanggal_iuran": this.cells[2].innerText.split(" ")[0],
                    //"keterangan_iuran": this.cells[4].innerText,
                    "catatan_iuran": this.cells[4].innerText,
                    "status_iuran": ''//$(this)[0].cells[2].textContent.replace(",","")==(parseInt(this.cells[3].textContent.replace(",",""))+parseInt($(this)[0].cells[0].children[0].children[0].dataset.iuranTelahBayar))?"LUNAS":"BELUM_LUNAS"
                }
                obj_new_arr.push(obj);
                total_iuran+=parseInt(obj.besar_iuran);
            }
        });
        obj = {
                "id_siswa": selected_id,
                "id_siswa_kelas": selected_id_kelas,
                "iuran" : obj_new_arr,
                "total_iuran" : total_iuran
        }
        console.log(obj);
        // console.log(obj_new_arr);
        if(obj_new_arr.length > 0) {
            var formData = new FormData();
            formData.append("list_data_iuran_bulan", JSON.stringify(obj));
            ajaxPOST(path + '/update_iuran',formData,'onActionSuccess','onUpdateError');
        }
    }
    function onUpdateSuccess(response){
        console.log(response);
    }
    function onUpdateError(response){
        console.log(response);
        $('#alert-msg').show();
        $('#alert-msg').html(response.responseJSON.message);
    }
    function setIuranPerBulans(elem, id_key){
        $('#besar_iuran_'+id_key).val($(elem).find('input').val());
        console.log(elem);
        console.log($(elem).find('input').val());
    }

    function setIuranPerBulan(elem, id_key){
        console.log("Try.."+elem.dataset.jenisIuran);
        console.log(elem);
        console.log(elem.id);

        if(elem.parentElement.parentElement.parentElement.cells[3].innerText=="")
            console.log(elem.parentElement.parentElement.parentElement.cells[3].children[0].value);
        else console.log(elem.parentElement.parentElement.parentElement.cells[3].innerText);
       
        if($(elem).is(':checked')){
            $('#val_'+elem.id).html($.number(elem.parentElement.parentElement.parentElement.cells[3].children[0].value));
            if(tahun_ajaran!='') $('#val_'+elem.dataset.jenisIuran+'_keterangan_iuran_'+id_key).html("SPP KELAS "+$('#lbl_kelas').data('tingkat'));
            else
                $('#val_'+elem.dataset.jenisIuran+'_keterangan_iuran_'+id_key).html(elem.parentElement.parentElement.parentElement.cells[5].children[0].value);
            // $('#val_tanggal_iuran_'+id_key).html(elem.parentElement.parentElement.parentElement.cells[6].children[0].value);
            elem.parentElement.parentElement.parentElement.cells[4].innerHTML=elem.parentElement.parentElement.parentElement.cells[4].children[0].value;
            console.log(elem.parentElement.parentElement.parentElement.cells[4]);
        }else{
            $('#val_'+elem.id).html("<input style='border-radius:0px!important;' type='number' value="+$('#val_'+elem.id).text().replace(",","")+">");
            $('#val_'+elem.dataset.jenisIuran+'_keterangan_iuran_'+id_key).html("<input style='border-radius:0px!important;' type='text' value=\""+elem.parentElement.parentElement.parentElement.cells[5].innerText+"\">");

            elem.parentElement.parentElement.parentElement.cells[4].innerHTML="<input class='rounded-0 my-picker' type='text' value="+elem.parentElement.parentElement.parentElement.cells[4].innerText+">";
            // elem.parentElement.parentElement.parentElement.cells[4].innerHTML=elem.parentElement.parentElement.parentElement.cells[4].children[0].value;
            // $('#val_tanggal_iuran_'+id_key).html("<input style='border-radius:0px!important;' type='date' value=\""+elem.parentElement.parentElement.parentElement.cells[6].innerText+"\">");
            renderDatePicker();
        }

        var lbl_nominal_tabungan=0;
        $.each($('#tbl-tabungan').find('input:checkbox'), function(key){
            if($(this).is(':checked')){
                lbl_nominal_tabungan+=parseInt(this.parentElement.parentElement.parentElement.cells[3].innerText.replace(",",""));
            }
        });

        var lbl_nominal_data=0;
        $.each($('#tbl-data').find('input:checkbox'), function(key){
            if($(this).is(':checked')){
                lbl_nominal_data+=parseInt(this.parentElement.parentElement.parentElement.cells[3].innerText.replace(",",""));
            }
        });

        $('#lbl_biaya_tabungan').html(': '+$.number(lbl_nominal_tabungan,2));
        $('#lbl_biaya_iuran_per_bulan').html(': '+$.number(lbl_nominal_data,2));

        $('#lbl_total_biaya').text(": "+($.number(lbl_nominal_tabungan+lbl_nominal_data+lbl_nominal_dpmp+lbl_nominal_du,2)));
    
    }

    function renderBulanKe(val){
        var html="";
        for(var i = 0; i < val;i++){
          html+='<input type="text" class="form-control mt-1" id="" placeholder="Bulan Ke">';
        };
        $('#bulan-ke').html(html);
    }
    function setBulanIuran(){
        var val='';
        $.each($('#bulan-ke input'), function(key){
          console.log(this.value);
          val+=";"+this.value;
        })
        $('[name=bulan_iuran]').val(val.substring(1));
    }

    function display(page = 1){
        $('#more-button-row').remove();
        var tbody = $("#tbl-data").find('tbody');
        if((params !='' && params != $( "#search-form" ).serialize()) || selected_action != ''){
            tbody.text('');
        }
        
        tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'assets/img/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
        
        params = $( "#search-form" ).serialize();
        ajaxGET(path + '/list_table?page='+page+'&'+params,'onGetListSuccess','onGetListError');
    }

    function onGetListSuccess(response){
        console.log(response);
        $('#loading-row').remove();
        $('#no-data-row').remove();
        
        var tbody = $("#tbl-data").find('tbody');
        
        var row = "";
        var num = $('.data-row').length+1;
        $.each(response.data,function(key,value){
            row += render_row(value, num);
            num++;
        });
        row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
        
    }
    function render_row(value, num){
    //  console.log(value);
        var row = "";
        row += '<tr class="data-row" id="row-'+value.id_siswa+'">';
        row += '<td>'+(num)+'</td>';
        row += '<td class="">'+value.nis_siswa+'</td>';
        row += '<td class="">'+value.nama_lengkap_siswa+'</td>';
        row += '<td class="">'+value.kelas_siswa+'</td>';
        row += '<td class="">'+value.jk_siswa+'</td>';
        row += '<td class="">';
        row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-info pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'edit\')"><i class="fas fa-pencil-alt"></i></a> ';
        row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger pill" type="" onclick="doAction(\''+value.id_siswa+'\',\'delete\')"><i class="fas fa-trash-alt"></i></a>';
        row += '</td>';
        row += '</tr>';
        return row;
    }
    function onGetListError(response) {
        console.log(response);
        let timerInterval
        Swal.fire({
          title: 'Auto close alert!',
          html: 'I will close in <strong></strong> seconds.',
          timer: 1000,
          onBeforeOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
              Swal.getContent().querySelector('strong')
                .textContent = Swal.getTimerLeft()
            }, 100)
          },
          onClose: () => {
            clearInterval(timerInterval)
          }
        }).then((result) => {
          if (
            // Read more about handling dismissals
            result.dismiss === Swal.DismissReason.timer
          ) {
            console.log('I was closed by the timer')
          }
        })
        $('#ui-view').html($('#ui-view-msg-error').html());
    }

    function save(){
        // var obj = new FormData(document.querySelector('#form'));
        var obj = new FormData();
        if(selected_id != '') obj.append('id', selected_id);
        obj.append("id_siswa", selected_id);  
        obj.append("list_data_iuran_bulan", JSON.stringify(obj_arr));
        obj.append("created_kwitansi", true);
        ajaxPOST(path + '/save',obj,'onActionSuccess','onGetError');
    }

    function onSaveSuccess(response){
        console.log(response);
        $('#msg-info-body').html('Data telah berhasil di update.');
        $('#msg-info').modal('show');
        // setTimeout(function(){ window.location.href="pengguna?vmenu=daftar"; }, 1000);
    }
    function onActionSuccess(response){
        // display();
        // $('#modal-form').modal('hide');
        // $('#msg-confirm').modal('hide');
        // selected_action = '';
        console.log(response);
        Swal.fire({
          type: 'success',
          title: 'Message',
          text: response.message,
          //footer: '<a href>Why do I have this issue?</a>'
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Oke!'
        })
        .then((value) => {
            if(value.value){
                location.reload();
            }
        });
    }

    function doAction(id, action, confirm = 0){
        selected_action = action;
        $('#modal-form-msg').hide();
        $('#msg-confirm-alert').hide();
        if(confirm == 0) ajaxGET(path + '/get_by_id/'+id+'?action='+selected_action,'onPrepareActionSuccess','onError');
        else ajaxPOST(path + '/do_action/'+id+'/'+selected_action,{},'onActionSuccess','onError');
    }

    function onPrepareActionSuccess(response) {
        console.log(response);
        var value = response.data;
        selected_id = value.id_siswa;
        if(selected_action == 'edit'){
            fillFormValue(value);
            $('#modal-form').modal('show');
        }else{
            swal({
              title: "Are you sure?",
              text: response.message,
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
                console.log(willDelete);
                if(willDelete)
                doAction(selected_id,'delete', 1);
            });
        }
    }

    function fillFormValue(value){
        $('#form')[0].reset();
        
        $('[name=nis_siswa]').val(value.nis_siswa);
        $('[name=nama_siswa]').val(value.nama_siswa);
        $('[name=kelas_siswa]').val(value.kelas_siswa);
    }

	function add() {
        selected_id = ''; // wajib
        selected_action = 'add'; // wajib
	    $('#form')[0].reset(); // reset form on modals
	    $('.form-group').removeClass('has-error'); // clear error class
	    $('.help-block').empty(); // clear error string
	    $('#modal-form').modal('show'); // show bootstrap modal
	    $('.modal-title').text('Tambah Siswa'); // Set Title to Bootstrap modal title
        $('#form-pass').removeClass("d-none");

	}
